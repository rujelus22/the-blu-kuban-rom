.class Lcom/google/android/location/g/q;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field private final b:D

.field private final c:D


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 57
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "0.0"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "1.1"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "1.5"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "1.6"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "2.0"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "2.0"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "2.1"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "2.2"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "2.3"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "2.3"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "3.0"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "3.1"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "3.2"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "4.0"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "4.0"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "4.1"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/location/g/q;->a:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .registers 3

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    const-wide/high16 v0, 0x3ff0

    iput-wide v0, p0, Lcom/google/android/location/g/q;->b:D

    .line 93
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/g/q;->c:D

    .line 94
    return-void
.end method

.method constructor <init>(DD)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-wide p1, p0, Lcom/google/android/location/g/q;->b:D

    .line 87
    iput-wide p3, p0, Lcom/google/android/location/g/q;->c:D

    .line 88
    return-void
.end method

.method public static a(Lcom/google/android/location/os/i;)Lcom/google/android/location/g/q;
    .registers 9
    .parameter

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 97
    invoke-interface {p0}, Lcom/google/android/location/os/i;->q()Lcom/google/android/location/os/i$a;

    move-result-object v1

    .line 98
    iget-object v0, v1, Lcom/google/android/location/os/i$a;->c:Ljava/lang/String;

    if-nez v0, :cond_11

    .line 101
    new-instance v0, Lcom/google/android/location/g/q;

    invoke-direct {v0}, Lcom/google/android/location/g/q;-><init>()V

    .line 129
    :goto_10
    return-object v0

    .line 103
    :cond_11
    iget-object v0, v1, Lcom/google/android/location/os/i$a;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 104
    array-length v0, v2

    if-ge v0, v7, :cond_26

    .line 107
    new-instance v0, Lcom/google/android/location/g/q;

    invoke-direct {v0}, Lcom/google/android/location/g/q;-><init>()V

    goto :goto_10

    .line 109
    :cond_26
    sget-object v0, Lcom/google/android/location/g/q;->a:[Ljava/lang/String;

    aget-object v0, v0, v5

    .line 110
    iget v3, v1, Lcom/google/android/location/os/i$a;->d:I

    sget-object v4, Lcom/google/android/location/g/q;->a:[Ljava/lang/String;

    array-length v4, v4

    if-ge v3, v4, :cond_37

    .line 111
    sget-object v0, Lcom/google/android/location/g/q;->a:[Ljava/lang/String;

    iget v1, v1, Lcom/google/android/location/os/i$a;->d:I

    aget-object v0, v0, v1

    .line 115
    :cond_37
    const-string v1, "%s/%s/%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aget-object v4, v2, v5

    aput-object v4, v3, v5

    aget-object v4, v2, v6

    aput-object v4, v3, v6

    aput-object v0, v3, v7

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 116
    const-string v1, "%s/%s/"

    new-array v3, v7, [Ljava/lang/Object;

    aget-object v4, v2, v5

    aput-object v4, v3, v5

    aget-object v2, v2, v6

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 117
    const-string v2, "models.txt"

    invoke-interface {p0, v2}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 118
    if-nez v2, :cond_68

    .line 120
    new-instance v0, Lcom/google/android/location/g/q;

    invoke-direct {v0}, Lcom/google/android/location/g/q;-><init>()V

    goto :goto_10

    .line 122
    :cond_68
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 123
    invoke-static {v3, v0, v1}, Lcom/google/android/location/g/q;->a(Ljava/io/BufferedReader;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/g/q;

    move-result-object v0

    .line 125
    :try_start_76
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_79
    .catch Ljava/io/IOException; {:try_start_76 .. :try_end_79} :catch_7a

    goto :goto_10

    .line 126
    :catch_7a
    move-exception v1

    goto :goto_10
.end method

.method private static a(Ljava/io/BufferedReader;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/location/g/q;
    .registers 20
    .parameter
    .parameter
    .parameter

    .prologue
    .line 142
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 143
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v14

    .line 144
    const-wide/high16 v12, 0x3ff0

    .line 145
    const-wide/16 v8, 0x0

    .line 146
    const-wide v4, 0x7fefffffffffffffL

    .line 147
    const/4 v0, 0x0

    .line 149
    :cond_12
    :goto_12
    :try_start_12
    invoke-virtual/range {p0 .. p0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_cd

    .line 150
    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_12

    .line 154
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_79

    .line 155
    const-string v1, ","

    invoke-virtual {v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 156
    array-length v2, v1

    const/4 v3, 0x3

    if-ge v2, v3, :cond_3b

    .line 158
    new-instance v1, Lcom/google/android/location/g/q;

    invoke-direct {v1}, Lcom/google/android/location/g/q;-><init>()V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_39} :catch_cc

    move-object v0, v1

    .line 213
    :goto_3a
    return-object v0

    .line 161
    :cond_3b
    const/4 v2, 0x1

    :try_start_3c
    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 162
    const/4 v4, 0x2

    aget-object v1, v1, v4

    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 165
    const-wide/16 v6, 0x0

    cmpg-double v1, v2, v6

    if-ltz v1, :cond_63

    const-wide/high16 v6, -0x3fa7

    cmpg-double v1, v4, v6

    if-ltz v1, :cond_63

    const-wide/high16 v6, 0x4059

    cmpl-double v1, v4, v6

    if-lez v1, :cond_6a

    .line 167
    :cond_63
    new-instance v1, Lcom/google/android/location/g/q;

    invoke-direct {v1}, Lcom/google/android/location/g/q;-><init>()V

    move-object v0, v1

    goto :goto_3a

    .line 170
    :cond_6a
    new-instance v1, Lcom/google/android/location/g/q;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/location/g/q;-><init>(DD)V
    :try_end_6f
    .catch Ljava/lang/NumberFormatException; {:try_start_3c .. :try_end_6f} :catch_71
    .catch Ljava/io/IOException; {:try_start_3c .. :try_end_6f} :catch_cc

    move-object v0, v1

    goto :goto_3a

    .line 171
    :catch_71
    move-exception v1

    .line 173
    :try_start_72
    new-instance v1, Lcom/google/android/location/g/q;

    invoke-direct {v1}, Lcom/google/android/location/g/q;-><init>()V

    move-object v0, v1

    goto :goto_3a

    .line 176
    :cond_79
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 177
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 178
    array-length v3, v2
    :try_end_8a
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_8a} :catch_cc

    const/4 v6, 0x3

    if-lt v3, v6, :cond_12

    .line 183
    const/4 v3, 0x1

    :try_start_8e
    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    .line 184
    const/4 v3, 0x2

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 185
    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D
    :try_end_ad
    .catch Ljava/lang/NumberFormatException; {:try_start_8e .. :try_end_ad} :catch_dd
    .catch Ljava/io/IOException; {:try_start_8e .. :try_end_ad} :catch_cc

    move-result-wide v2

    .line 188
    const-wide/16 v15, 0x0

    cmpg-double v15, v10, v15

    if-ltz v15, :cond_12

    const-wide/high16 v15, -0x3fa7

    cmpg-double v15, v6, v15

    if-ltz v15, :cond_12

    const-wide/high16 v15, 0x4059

    cmpl-double v15, v6, v15

    if-gtz v15, :cond_12

    .line 192
    cmpg-double v15, v2, v4

    if-gez v15, :cond_e0

    move-object v0, v1

    move-wide v4, v6

    move-wide v6, v10

    :goto_c7
    move-wide v8, v4

    move-wide v12, v6

    move-wide v4, v2

    .line 202
    goto/16 :goto_12

    .line 204
    :catch_cc
    move-exception v1

    .line 207
    :cond_cd
    if-eqz v0, :cond_d6

    .line 209
    new-instance v0, Lcom/google/android/location/g/q;

    invoke-direct {v0, v12, v13, v8, v9}, Lcom/google/android/location/g/q;-><init>(DD)V

    goto/16 :goto_3a

    .line 213
    :cond_d6
    new-instance v0, Lcom/google/android/location/g/q;

    invoke-direct {v0}, Lcom/google/android/location/g/q;-><init>()V

    goto/16 :goto_3a

    .line 198
    :catch_dd
    move-exception v2

    goto/16 :goto_12

    :cond_e0
    move-wide v2, v4

    move-wide v6, v12

    move-wide v4, v8

    goto :goto_c7
.end method


# virtual methods
.method public a(I)I
    .registers 6
    .parameter

    .prologue
    .line 223
    iget-wide v0, p0, Lcom/google/android/location/g/q;->b:D

    int-to-double v2, p1

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/location/g/q;->c:D

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public a(Ljava/util/Map;)Ljava/util/Map;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 236
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 237
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/g/q;->a(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_d

    .line 239
    :cond_33
    return-object v1
.end method
