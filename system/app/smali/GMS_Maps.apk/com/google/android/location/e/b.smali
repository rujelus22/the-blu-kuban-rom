.class public Lcom/google/android/location/e/b;
.super Lcom/google/android/location/e/e;
.source "SourceFile"


# instance fields
.field private final o:I

.field private final p:I


# direct methods
.method public constructor <init>(JIIIIIIILjava/util/List;Ljava/lang/String;IIII)V
    .registers 31
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JIIIIIII",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/e$a;",
            ">;",
            "Ljava/lang/String;",
            "IIII)V"
        }
    .end annotation

    .prologue
    .line 22
    move-object v1, p0

    move-wide/from16 v2, p1

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p11

    move-object/from16 v9, p10

    move/from16 v10, p12

    move/from16 v11, p13

    move/from16 v12, p14

    move/from16 v13, p15

    move/from16 v14, p9

    invoke-direct/range {v1 .. v14}, Lcom/google/android/location/e/e;-><init>(JIIIILjava/lang/String;Ljava/util/List;IIIII)V

    .line 24
    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/location/e/b;->o:I

    .line 25
    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/location/e/b;->p:I

    .line 26
    return-void
.end method


# virtual methods
.method public a(JI)Lcom/google/android/location/e/e;
    .registers 21
    .parameter
    .parameter

    .prologue
    .line 33
    new-instance v1, Lcom/google/android/location/e/b;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/location/e/b;->l:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/location/e/b;->b:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/location/e/b;->c:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/location/e/b;->d:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/location/e/b;->p:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/location/e/b;->o:I

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/location/e/b;->k:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/location/e/b;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/location/e/b;->e:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/location/e/b;->f:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/location/e/b;->g:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/location/e/b;->h:I

    move/from16 v16, v0

    move-wide/from16 v2, p1

    move/from16 v10, p3

    invoke-direct/range {v1 .. v16}, Lcom/google/android/location/e/b;-><init>(JIIIIIIILjava/util/List;Ljava/lang/String;IIII)V

    return-object v1
.end method

.method public a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/location/e/b;->n:Ljava/lang/String;

    if-nez v0, :cond_47

    .line 52
    invoke-virtual {p0}, Lcom/google/android/location/e/b;->j()I

    move-result v0

    .line 56
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/e/b;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/e/b;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/e/b;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/e/b;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e/b;->n:Ljava/lang/String;

    .line 59
    :cond_47
    iget-object v0, p0, Lcom/google/android/location/e/b;->n:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 40
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/location/e/b;->p:I

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 42
    iget v0, p0, Lcom/google/android/location/e/b;->o:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_12

    .line 43
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/location/e/b;->o:I

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 45
    :cond_12
    return-void
.end method

.method public a(Lcom/google/android/location/e/e;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 70
    instance-of v1, p1, Lcom/google/android/location/e/b;

    if-eqz v1, :cond_14

    .line 71
    check-cast p1, Lcom/google/android/location/e/b;

    .line 72
    iget v1, p0, Lcom/google/android/location/e/b;->p:I

    iget v2, p1, Lcom/google/android/location/e/b;->p:I

    if-ne v1, v2, :cond_14

    iget v1, p0, Lcom/google/android/location/e/b;->o:I

    iget v2, p1, Lcom/google/android/location/e/b;->o:I

    if-ne v1, v2, :cond_14

    const/4 v0, 0x1

    .line 74
    :cond_14
    return v0
.end method

.method b()Z
    .registers 3

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/location/e/b;->p:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " lac: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/e/b;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
