.class Lcom/google/android/location/internal/server/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/internal/server/b$b;,
        Lcom/google/android/location/internal/server/b$a;,
        Lcom/google/android/location/internal/server/b$c;
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/location/internal/server/b$c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/internal/server/b$b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/internal/server/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:I


# direct methods
.method constructor <init>(I)V
    .registers 6
    .parameter

    .prologue
    const v3, 0x7fffffff

    const/4 v2, 0x1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/b;->b:Ljava/util/Map;

    .line 37
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/b;->c:Ljava/util/Set;

    .line 39
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Set;

    .line 40
    iput v3, p0, Lcom/google/android/location/internal/server/b;->e:I

    .line 41
    iput v3, p0, Lcom/google/android/location/internal/server/b;->f:I

    .line 44
    iput p1, p0, Lcom/google/android/location/internal/server/b;->a:I

    .line 45
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/location/Location;Z)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 185
    const/4 v0, 0x0

    .line 186
    const/4 v2, 0x0

    .line 188
    iget-object v1, p0, Lcom/google/android/location/internal/server/b;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_31

    .line 189
    if-nez v0, :cond_19

    .line 190
    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->d()Landroid/content/Intent;

    move-result-object v0

    .line 191
    const-string v1, "location"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_19
    move-object v1, v0

    .line 193
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$b;

    .line 194
    iget-boolean v4, v0, Lcom/google/android/location/internal/server/b$b;->d:Z

    if-ne p3, v4, :cond_32

    .line 195
    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/internal/server/b$b;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_32

    .line 197
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 198
    const/4 v0, 0x1

    :goto_2e
    move v2, v0

    move-object v0, v1

    .line 201
    goto :goto_8

    .line 202
    :cond_31
    return v2

    :cond_32
    move v0, v2

    goto :goto_2e
.end method

.method private d()Landroid/content/Intent;
    .registers 4

    .prologue
    .line 141
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 142
    const-string v1, "com.google.android.location.internal.EXTRA_RELEASE_VERSION"

    iget v2, p0, Lcom/google/android/location/internal/server/b;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 143
    return-object v0
.end method

.method private e()V
    .registers 4

    .prologue
    .line 282
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/location/internal/server/b;->e:I

    .line 283
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 284
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$c;

    .line 286
    iget v2, p0, Lcom/google/android/location/internal/server/b;->e:I

    iget v0, v0, Lcom/google/android/location/internal/server/b$c;->c:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/internal/server/b;->e:I

    goto :goto_f

    .line 288
    :cond_2c
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_32
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_49

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$b;

    .line 290
    iget v2, p0, Lcom/google/android/location/internal/server/b;->e:I

    iget v0, v0, Lcom/google/android/location/internal/server/b$b;->c:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/internal/server/b;->e:I

    goto :goto_32

    .line 292
    :cond_49
    return-void
.end method

.method private f()V
    .registers 4

    .prologue
    .line 296
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/location/internal/server/b;->f:I

    .line 297
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$a;

    .line 299
    iget v2, p0, Lcom/google/android/location/internal/server/b;->f:I

    iget v0, v0, Lcom/google/android/location/internal/server/b$a;->c:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/internal/server/b;->f:I

    goto :goto_b

    .line 301
    :cond_22
    return-void
.end method


# virtual methods
.method a()I
    .registers 2

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/location/internal/server/b;->e:I

    return v0
.end method

.method a(Landroid/content/Context;Landroid/location/Location;Landroid/location/Location;)Ljava/util/List;
    .registers 11
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/location/Location;",
            "Landroid/location/Location;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 159
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 162
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    .line 163
    :goto_12
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 164
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$c;

    .line 166
    :try_start_24
    iget-object v6, v0, Lcom/google/android/location/internal/server/b$c;->a:Lcom/google/android/location/internal/ILocationListener;

    invoke-interface {v6, p2}, Lcom/google/android/location/internal/ILocationListener;->onLocationChanged(Landroid/location/Location;)V
    :try_end_29
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_29} :catch_2c

    move v0, v1

    :goto_2a
    move v1, v0

    .line 173
    goto :goto_12

    .line 167
    :catch_2c
    move-exception v1

    .line 169
    iget-object v0, v0, Lcom/google/android/location/internal/server/b$c;->b:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    move v0, v3

    .line 171
    goto :goto_2a

    .line 175
    :cond_37
    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Landroid/location/Location;Z)Z

    move-result v0

    or-int/2addr v0, v1

    .line 176
    invoke-direct {p0, p1, p3, v3}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Landroid/location/Location;Z)Z

    move-result v1

    or-int/2addr v0, v1

    .line 177
    if-eqz v0, :cond_46

    .line 178
    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->e()V

    .line 180
    :cond_46
    return-object v4
.end method

.method a(Landroid/content/Context;Landroid/app/PendingIntent;I)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 124
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 125
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NLP ActivityPendingIntent client in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 128
    new-instance v1, Lcom/google/android/location/internal/server/b$a;

    invoke-direct {v1, p2, p3, v0}, Lcom/google/android/location/internal/server/b$a;-><init>(Landroid/app/PendingIntent;ILandroid/os/PowerManager$WakeLock;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 130
    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->f()V

    .line 131
    return-void
.end method

.method a(Landroid/content/Context;Landroid/app/PendingIntent;ILandroid/location/Location;Z)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 97
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 98
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NLP PendingIntent client in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 101
    new-instance v1, Lcom/google/android/location/internal/server/b$b;

    invoke-direct {v1, p2, p3, v0, p5}, Lcom/google/android/location/internal/server/b$b;-><init>(Landroid/app/PendingIntent;ILandroid/os/PowerManager$WakeLock;Z)V

    .line 103
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->c:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 104
    if-eqz p4, :cond_3c

    .line 105
    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->d()Landroid/content/Intent;

    move-result-object v0

    .line 106
    const-string v2, "location"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 107
    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/internal/server/b$b;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 109
    :cond_3c
    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->e()V

    .line 110
    return-void
.end method

.method a(Landroid/content/Context;Lcom/google/android/location/clientlib/NlpActivity;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 214
    const/4 v0, 0x0

    .line 215
    const/4 v2, 0x0

    .line 216
    iget-object v1, p0, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 217
    if-nez v0, :cond_19

    .line 218
    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->d()Landroid/content/Intent;

    move-result-object v0

    .line 219
    const-string v1, "activity"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_19
    move-object v1, v0

    .line 221
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$a;

    .line 222
    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/internal/server/b$a;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_33

    .line 224
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 225
    const/4 v0, 0x1

    :goto_2a
    move v2, v0

    move-object v0, v1

    .line 227
    goto :goto_8

    .line 228
    :cond_2d
    if-eqz v2, :cond_32

    .line 229
    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->f()V

    .line 231
    :cond_32
    return-void

    :cond_33
    move v0, v2

    goto :goto_2a
.end method

.method a(Landroid/content/Context;Z)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 242
    const/4 v0, 0x0

    .line 245
    iget-object v1, p0, Lcom/google/android/location/internal/server/b;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    .line 246
    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 247
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$c;

    .line 249
    if-eqz p2, :cond_29

    .line 250
    :try_start_21
    iget-object v0, v0, Lcom/google/android/location/internal/server/b$c;->a:Lcom/google/android/location/internal/ILocationListener;

    invoke-interface {v0}, Lcom/google/android/location/internal/ILocationListener;->onProviderEnabled()V

    :goto_26
    move v0, v1

    :goto_27
    move v1, v0

    .line 259
    goto :goto_d

    .line 252
    :cond_29
    iget-object v0, v0, Lcom/google/android/location/internal/server/b$c;->a:Lcom/google/android/location/internal/ILocationListener;

    invoke-interface {v0}, Lcom/google/android/location/internal/ILocationListener;->onProviderDisabled()V
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_2e} :catch_2f

    goto :goto_26

    .line 254
    :catch_2f
    move-exception v0

    .line 256
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    move v0, v2

    .line 257
    goto :goto_27

    .line 262
    :cond_35
    const/4 v0, 0x0

    .line 263
    iget-object v3, p0, Lcom/google/android/location/internal/server/b;->c:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v1

    :goto_3d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_62

    .line 264
    if-nez v0, :cond_4e

    .line 265
    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->d()Landroid/content/Intent;

    move-result-object v0

    .line 266
    const-string v1, "providerEnabled"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_4e
    move-object v1, v0

    .line 268
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/internal/server/b$a;

    .line 269
    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/internal/server/b$a;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_68

    .line 271
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    move v0, v2

    :goto_5f
    move v3, v0

    move-object v0, v1

    .line 274
    goto :goto_3d

    .line 276
    :cond_62
    if-eqz v3, :cond_67

    .line 277
    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->e()V

    .line 279
    :cond_67
    return-void

    :cond_68
    move v0, v3

    goto :goto_5f
.end method

.method a(Lcom/google/android/location/internal/ILocationListener;)V
    .registers 4
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->b:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/location/internal/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->e()V

    .line 82
    return-void
.end method

.method a(Lcom/google/android/location/internal/ILocationListener;Ljava/lang/String;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->b:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/location/internal/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/internal/server/b$c;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/location/internal/server/b$c;-><init>(Lcom/google/android/location/internal/ILocationListener;Ljava/lang/String;I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    invoke-direct {p0}, Lcom/google/android/location/internal/server/b;->e()V

    .line 74
    return-void
.end method

.method b()I
    .registers 2

    .prologue
    .line 59
    iget v0, p0, Lcom/google/android/location/internal/server/b;->f:I

    return v0
.end method

.method c()I
    .registers 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/location/internal/server/b;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
