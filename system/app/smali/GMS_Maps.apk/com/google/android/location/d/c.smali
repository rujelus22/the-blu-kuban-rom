.class public Lcom/google/android/location/d/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/location/d/c;


# instance fields
.field private b:Lcom/google/android/location/h/h;


# direct methods
.method private constructor <init>(Lcom/google/android/location/h/h;)V
    .registers 2
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/google/android/location/d/c;->b:Lcom/google/android/location/h/h;

    .line 57
    return-void
.end method

.method public static declared-synchronized a()Lcom/google/android/location/d/c;
    .registers 3

    .prologue
    .line 49
    const-class v1, Lcom/google/android/location/d/c;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/location/d/c;->a:Lcom/google/android/location/d/c;

    if-nez v0, :cond_12

    .line 50
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Please call init() before calling getInstance."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_f

    .line 49
    :catchall_f
    move-exception v0

    monitor-exit v1

    throw v0

    .line 52
    :cond_12
    :try_start_12
    sget-object v0, Lcom/google/android/location/d/c;->a:Lcom/google/android/location/d/c;
    :try_end_14
    .catchall {:try_start_12 .. :try_end_14} :catchall_f

    monitor-exit v1

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/google/android/location/h/h$a;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 35
    const-class v1, Lcom/google/android/location/d/c;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/location/d/c;->a:Lcom/google/android/location/d/c;

    if-nez v0, :cond_1d

    .line 36
    invoke-static {p0}, Lcom/google/googlenav/common/Config;->getOrCreateInstance(Landroid/content/Context;)Lcom/google/googlenav/common/Config;

    .line 37
    new-instance v0, Lcom/google/android/location/h/h$a;

    invoke-direct {v0}, Lcom/google/android/location/h/h$a;-><init>()V

    .line 38
    invoke-static {p1}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/h$a;)V

    .line 39
    new-instance v0, Lcom/google/android/location/d/c;

    invoke-static {}, Lcom/google/android/location/h/h;->g()Lcom/google/android/location/h/h;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/location/d/c;-><init>(Lcom/google/android/location/h/h;)V

    sput-object v0, Lcom/google/android/location/d/c;->a:Lcom/google/android/location/d/c;
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1f

    .line 41
    :cond_1d
    monitor-exit v1

    return-void

    .line 35
    :catchall_1f
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(Lcom/google/android/location/h/b/m;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 63
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/d/c;->b:Lcom/google/android/location/h/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;Z)V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 64
    monitor-exit p0

    return-void

    .line 63
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method
