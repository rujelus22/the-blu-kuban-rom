.class Lcom/google/android/location/c/i;
.super Lcom/google/android/location/c/E;
.source "SourceFile"


# instance fields
.field private final c:Landroid/telephony/TelephonyManager;

.field private final d:Landroid/telephony/PhoneStateListener;

.field private volatile e:I

.field private final f:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1, p3, p4, p5}, Lcom/google/android/location/c/E;-><init>(Landroid/content/Context;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    .line 23
    new-instance v0, Lcom/google/android/location/c/i$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/i$1;-><init>(Lcom/google/android/location/c/i;)V

    iput-object v0, p0, Lcom/google/android/location/c/i;->d:Landroid/telephony/PhoneStateListener;

    .line 46
    const/16 v0, -0x270f

    iput v0, p0, Lcom/google/android/location/c/i;->e:I

    .line 49
    new-instance v0, Lcom/google/android/location/c/i$2;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/i$2;-><init>(Lcom/google/android/location/c/i;)V

    iput-object v0, p0, Lcom/google/android/location/c/i;->f:Ljava/lang/Runnable;

    .line 65
    invoke-static {p2}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iput-object p2, p0, Lcom/google/android/location/c/i;->c:Landroid/telephony/TelephonyManager;

    .line 67
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/i;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 20
    iput p1, p0, Lcom/google/android/location/c/i;->e:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/location/c/i;)Landroid/telephony/TelephonyManager;
    .registers 2
    .parameter

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/location/c/i;->c:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method private a(Landroid/telephony/CellLocation;)V
    .registers 10
    .parameter

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/location/c/i;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/i;->c:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/c/i;->c:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v2

    iget v4, p0, Lcom/google/android/location/c/i;->e:I

    iget-object v3, p0, Lcom/google/android/location/c/i;->c:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNeighboringCellInfo()Ljava/util/List;

    move-result-object v5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    move-object v3, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/c/k;->a(ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V

    .line 94
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/i;Landroid/telephony/CellLocation;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/location/c/i;->a(Landroid/telephony/CellLocation;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .registers 5

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/location/c/i;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/i;->f:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/c/k;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 73
    iget-object v0, p0, Lcom/google/android/location/c/i;->c:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/android/location/c/i;->d:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x111

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/location/c/i;->b:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_1d

    .line 77
    iget-object v0, p0, Lcom/google/android/location/c/i;->b:Lcom/google/android/location/c/l;

    invoke-interface {v0}, Lcom/google/android/location/c/l;->c()V

    .line 79
    :cond_1d
    return-void
.end method

.method protected b()V
    .registers 4

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/location/c/i;->c:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/android/location/c/i;->d:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 84
    invoke-virtual {p0}, Lcom/google/android/location/c/i;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/i;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/k;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/location/c/i;->b:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_1a

    .line 86
    iget-object v0, p0, Lcom/google/android/location/c/i;->b:Lcom/google/android/location/c/l;

    invoke-interface {v0}, Lcom/google/android/location/c/l;->b()V

    .line 88
    :cond_1a
    return-void
.end method
