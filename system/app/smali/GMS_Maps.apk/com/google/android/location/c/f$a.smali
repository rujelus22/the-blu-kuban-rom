.class Lcom/google/android/location/c/f$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/f;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/c/f$b;",
            ">;"
        }
    .end annotation
.end field

.field private volatile d:Ljava/lang/String;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/location/c/f;Ljava/util/List;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 673
    iput-object p1, p0, Lcom/google/android/location/c/f$a;->a:Lcom/google/android/location/c/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 654
    invoke-static {}, Lcom/google/android/location/c/L;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/f$a;->e:Ljava/util/List;

    .line 662
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/f$a;->f:Ljava/util/Map;

    .line 664
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/f$a;->g:Ljava/util/Map;

    .line 670
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/f$a;->h:Ljava/util/Map;

    .line 674
    iput-object p2, p0, Lcom/google/android/location/c/f$a;->b:Ljava/util/List;

    .line 675
    iput-object p3, p0, Lcom/google/android/location/c/f$a;->d:Ljava/lang/String;

    .line 676
    invoke-static {}, Lcom/google/android/location/c/L;->c()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/f$a;->c:Ljava/util/List;

    .line 677
    const/4 v0, 0x0

    :goto_37
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_47

    .line 678
    iget-object v1, p0, Lcom/google/android/location/c/f$a;->c:Ljava/util/List;

    sget-object v2, Lcom/google/android/location/c/f$b;->a:Lcom/google/android/location/c/f$b;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 677
    add-int/lit8 v0, v0, 0x1

    goto :goto_37

    .line 680
    :cond_47
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/f$a;Ljava/lang/String;)Lcom/google/android/location/c/K;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 641
    invoke-direct {p0, p1}, Lcom/google/android/location/c/f$a;->c(Ljava/lang/String;)Lcom/google/android/location/c/K;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(IZ)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 767
    monitor-enter p0

    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 768
    const-string v1, "Need to call corresponding addGLocRequest before calling this."

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 770
    iget-object v1, p0, Lcom/google/android/location/c/f$a;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 771
    iget-object v4, p0, Lcom/google/android/location/c/f$a;->g:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 772
    if-nez v1, :cond_28

    if-eqz v0, :cond_57

    :cond_28
    move v4, v2

    :goto_29
    const-string v5, "Need to call corresponding addGLocRequest before calling this."

    invoke-static {v4, v5}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    .line 774
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ne v4, v5, :cond_59

    move v4, v2

    :goto_39
    const-string v5, "Inconsistent state."

    invoke-static {v4, v5}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    .line 775
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 776
    if-ltz v4, :cond_5b

    move v1, v2

    :goto_49
    const-string v2, "Need to call corresponding addGLocRequest before calling this."

    invoke-static {v1, v2}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    .line 778
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_55
    .catchall {:try_start_3 .. :try_end_55} :catchall_5d

    .line 779
    monitor-exit p0

    return-void

    :cond_57
    move v4, v3

    .line 772
    goto :goto_29

    :cond_59
    move v4, v3

    .line 774
    goto :goto_39

    :cond_5b
    move v1, v3

    .line 776
    goto :goto_49

    .line 767
    :catchall_5d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/c/f$a;IZ)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 641
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c/f$a;->a(IZ)V

    return-void
.end method

.method private declared-synchronized c(Ljava/lang/String;)Lcom/google/android/location/c/K;
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 813
    monitor-enter p0

    :try_start_2
    new-instance v2, Lcom/google/android/location/c/K;

    invoke-direct {v2}, Lcom/google/android/location/c/K;-><init>()V

    .line 814
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 815
    if-ltz v3, :cond_24

    const/4 v0, 0x1

    :goto_10
    const-string v4, "File not found."

    invoke-static {v0, v4}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    .line 816
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/c/f$b;

    .line 817
    sget-object v3, Lcom/google/android/location/c/f$b;->a:Lcom/google/android/location/c/f$b;
    :try_end_1f
    .catchall {:try_start_2 .. :try_end_1f} :catchall_44

    if-ne v0, v3, :cond_26

    move-object v0, v1

    .line 849
    :goto_22
    monitor-exit p0

    return-object v0

    .line 815
    :cond_24
    const/4 v0, 0x0

    goto :goto_10

    .line 820
    :cond_26
    :try_start_26
    sget-object v3, Lcom/google/android/location/c/f$3;->a:[I

    invoke-virtual {v0}, Lcom/google/android/location/c/f$b;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_86

    .line 834
    :goto_31
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 835
    if-nez v0, :cond_5c

    move-object v0, v2

    .line 837
    goto :goto_22

    .line 822
    :pswitch_3d
    iget v0, v2, Lcom/google/android/location/c/K;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/android/location/c/K;->a:I
    :try_end_43
    .catchall {:try_start_26 .. :try_end_43} :catchall_44

    goto :goto_31

    .line 813
    :catchall_44
    move-exception v0

    monitor-exit p0

    throw v0

    .line 825
    :pswitch_47
    :try_start_47
    iget v0, v2, Lcom/google/android/location/c/K;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/android/location/c/K;->c:I

    goto :goto_31

    .line 828
    :pswitch_4e
    iget v0, v2, Lcom/google/android/location/c/K;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/android/location/c/K;->b:I

    goto :goto_31

    .line 831
    :pswitch_55
    iget v0, v2, Lcom/google/android/location/c/K;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/android/location/c/K;->d:I

    goto :goto_31

    .line 839
    :cond_5c
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_60
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_84

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 840
    if-nez v0, :cond_70

    move-object v0, v1

    .line 841
    goto :goto_22

    .line 843
    :cond_70
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7d

    .line 844
    iget v0, v2, Lcom/google/android/location/c/K;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/android/location/c/K;->e:I

    goto :goto_60

    .line 846
    :cond_7d
    iget v0, v2, Lcom/google/android/location/c/K;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/android/location/c/K;->f:I
    :try_end_83
    .catchall {:try_start_47 .. :try_end_83} :catchall_44

    goto :goto_60

    :cond_84
    move-object v0, v2

    .line 849
    goto :goto_22

    .line 820
    :pswitch_data_86
    .packed-switch 0x1
        :pswitch_3d
        :pswitch_47
        :pswitch_4e
        :pswitch_55
    .end packed-switch
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 886
    monitor-enter p0

    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/c/f$a;->a:Lcom/google/android/location/c/f;

    invoke-static {v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_1a

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_1a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .registers 5

    .prologue
    .line 695
    monitor-enter p0

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_33

    .line 696
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/google/android/location/c/f$b;->a:Lcom/google/android/location/c/f$b;

    if-ne v0, v2, :cond_2f

    .line 697
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->c:Ljava/util/List;

    sget-object v2, Lcom/google/android/location/c/f$b;->d:Lcom/google/android/location/c/f$b;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 701
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->f(Lcom/google/android/location/c/f;)Lcom/google/android/location/c/f$c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/c/f$a;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/location/c/f$a;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/location/c/f$c;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2f
    .catchall {:try_start_3 .. :try_end_2f} :catchall_35

    .line 695
    :cond_2f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 704
    :cond_33
    monitor-exit p0

    return-void

    .line 695
    :catchall_35
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .registers 4
    .parameter

    .prologue
    .line 683
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->e:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 684
    monitor-exit p0

    return-void

    .line 683
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;I)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 740
    monitor-enter p0

    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 741
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->f:Ljava/util/Map;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 742
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->g:Ljava/util/Map;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 744
    :cond_1f
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 745
    iget-object v1, p0, Lcom/google/android/location/c/f$a;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 747
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v5

    :goto_34
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_49

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 748
    if-ne v2, p2, :cond_77

    move v2, v3

    :goto_47
    move v4, v2

    .line 749
    goto :goto_34

    .line 752
    :cond_49
    if-nez v4, :cond_56

    .line 753
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 754
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 756
    :cond_56
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->h:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_72

    :goto_62
    const-string v0, "Duplicated seqNum (the same seqNum exists in more than one file)!"

    invoke-static {v3, v0}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    .line 758
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->h:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_70
    .catchall {:try_start_3 .. :try_end_70} :catchall_74

    .line 759
    monitor-exit p0

    return-void

    :cond_72
    move v3, v5

    .line 756
    goto :goto_62

    .line 740
    :catchall_74
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_77
    move v2, v4

    goto :goto_47
.end method

.method public declared-synchronized a(Ljava/lang/String;Lcom/google/android/location/c/f$b;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 728
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 729
    if-ltz v1, :cond_27

    const/4 v0, 0x1

    :goto_a
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not in upload list."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    .line 730
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->c:Ljava/util/List;

    invoke-interface {v0, v1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_25
    .catchall {:try_start_1 .. :try_end_25} :catchall_29

    .line 731
    monitor-exit p0

    return-void

    .line 729
    :cond_27
    const/4 v0, 0x0

    goto :goto_a

    .line 728
    :catchall_29
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 710
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->b:Ljava/util/List;

    return-object v0
.end method

.method public declared-synchronized b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 890
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/location/c/f$a;->d:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 891
    monitor-exit p0

    return-void

    .line 890
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(I)Z
    .registers 4
    .parameter

    .prologue
    .line 687
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->e:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_d

    move-result v0

    monitor-exit p0

    return v0

    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 717
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized c(I)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 875
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 876
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "seqNum #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not exist. addGLocRequest need to be called before this."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_29
    .catchall {:try_start_1 .. :try_end_29} :catchall_2b

    .line 878
    monitor-exit p0

    return-object v0

    .line 875
    :catchall_2b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()Lcom/google/android/location/c/K;
    .registers 4

    .prologue
    .line 790
    monitor-enter p0

    :try_start_1
    new-instance v1, Lcom/google/android/location/c/K;

    invoke-direct {v1}, Lcom/google/android/location/c/K;-><init>()V

    .line 791
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 792
    invoke-direct {p0, v0}, Lcom/google/android/location/c/f$a;->c(Ljava/lang/String;)Lcom/google/android/location/c/K;
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_25

    move-result-object v0

    .line 793
    if-nez v0, :cond_21

    .line 794
    const/4 v0, 0x0

    .line 799
    :goto_1f
    monitor-exit p0

    return-object v0

    .line 796
    :cond_21
    :try_start_21
    invoke-virtual {v1, v0}, Lcom/google/android/location/c/K;->a(Lcom/google/android/location/c/K;)V
    :try_end_24
    .catchall {:try_start_21 .. :try_end_24} :catchall_25

    goto :goto_c

    .line 790
    :catchall_25
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_28
    move-object v0, v1

    .line 799
    goto :goto_1f
.end method

.method public declared-synchronized e()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 857
    monitor-enter p0

    :try_start_1
    invoke-static {}, Lcom/google/android/location/c/L;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 858
    iget-object v0, p0, Lcom/google/android/location/c/f$a;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_f
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_52

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 859
    iget-object v1, p0, Lcom/google/android/location/c/f$a;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 860
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 861
    const/4 v2, 0x0

    move v3, v2

    :goto_2f
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_f

    .line 862
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4e

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4e

    .line 863
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4e
    .catchall {:try_start_1 .. :try_end_4e} :catchall_54

    .line 861
    :cond_4e
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2f

    .line 867
    :cond_52
    monitor-exit p0

    return-object v4

    .line 857
    :catchall_54
    move-exception v0

    monitor-exit p0

    throw v0
.end method
