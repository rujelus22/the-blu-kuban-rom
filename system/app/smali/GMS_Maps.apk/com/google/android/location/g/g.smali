.class Lcom/google/android/location/g/g;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/g/g$a;
    }
.end annotation


# static fields
.field private static final d:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/location/g/g$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/location/g/g$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:I

.field private final c:[[D


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 232
    new-instance v0, Lcom/google/android/location/g/g$1;

    invoke-direct {v0}, Lcom/google/android/location/g/g$1;-><init>()V

    sput-object v0, Lcom/google/android/location/g/g;->d:Ljava/util/Comparator;

    .line 240
    new-instance v0, Lcom/google/android/location/g/g$2;

    invoke-direct {v0}, Lcom/google/android/location/g/g$2;-><init>()V

    sput-object v0, Lcom/google/android/location/g/g;->e:Ljava/util/Comparator;

    return-void
.end method

.method constructor <init>(I)V
    .registers 4
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lcom/google/android/location/g/g;->b:I

    .line 44
    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/g/g;->a:I

    .line 45
    iget v0, p0, Lcom/google/android/location/g/g;->a:I

    iget v1, p0, Lcom/google/android/location/g/g;->a:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[D

    iput-object v0, p0, Lcom/google/android/location/g/g;->c:[[D

    .line 46
    invoke-virtual {p0}, Lcom/google/android/location/g/g;->a()V

    .line 47
    return-void
.end method

.method private a(II)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 59
    iget v0, p0, Lcom/google/android/location/g/g;->b:I

    if-gt p1, v0, :cond_14

    iget v0, p0, Lcom/google/android/location/g/g;->b:I

    neg-int v0, v0

    if-lt p1, v0, :cond_14

    iget v0, p0, Lcom/google/android/location/g/g;->b:I

    if-gt p2, v0, :cond_14

    iget v0, p0, Lcom/google/android/location/g/g;->b:I

    neg-int v0, v0

    if-lt p2, v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private b()V
    .registers 9

    .prologue
    const/4 v1, 0x0

    .line 195
    const-wide/16 v2, 0x0

    move v0, v1

    .line 196
    :goto_4
    iget v4, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v0, v4, :cond_1c

    move v4, v1

    .line 197
    :goto_9
    iget v5, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v4, v5, :cond_19

    .line 198
    iget-object v5, p0, Lcom/google/android/location/g/g;->c:[[D

    aget-object v5, v5, v0

    aget-wide v5, v5, v4

    add-double/2addr v5, v2

    .line 197
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v5

    goto :goto_9

    .line 196
    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_1c
    move v0, v1

    .line 202
    :goto_1d
    iget v4, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v0, v4, :cond_35

    move v4, v1

    .line 203
    :goto_22
    iget v5, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v4, v5, :cond_32

    .line 204
    iget-object v5, p0, Lcom/google/android/location/g/g;->c:[[D

    aget-object v5, v5, v0

    aget-wide v6, v5, v4

    div-double/2addr v6, v2

    aput-wide v6, v5, v4

    .line 203
    add-int/lit8 v4, v4, 0x1

    goto :goto_22

    .line 202
    :cond_32
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d

    .line 207
    :cond_35
    return-void
.end method


# virtual methods
.method public a(DLcom/google/android/location/e/w;D)Lcom/google/android/location/e/w$a;
    .registers 17
    .parameter
    .parameter
    .parameter

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/location/g/g;->b()V

    .line 96
    new-instance v3, Ljava/util/PriorityQueue;

    const/16 v0, 0x64

    sget-object v1, Lcom/google/android/location/g/g;->e:Ljava/util/Comparator;

    invoke-direct {v3, v0, v1}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    .line 100
    const/4 v0, 0x0

    move v1, v0

    :goto_e
    iget v0, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v1, v0, :cond_4f

    .line 101
    const/4 v0, 0x0

    move v2, v0

    :goto_14
    iget v0, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v2, v0, :cond_4b

    .line 102
    new-instance v4, Lcom/google/android/location/g/g$a;

    iget-object v0, p0, Lcom/google/android/location/g/g;->c:[[D

    aget-object v0, v0, v1

    aget-wide v5, v0, v2

    invoke-direct {v4, v1, v2, v5, v6}, Lcom/google/android/location/g/g$a;-><init>(IID)V

    .line 103
    invoke-virtual {v3}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    const/16 v5, 0x64

    if-ge v0, v5, :cond_32

    .line 104
    invoke-virtual {v3, v4}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 101
    :cond_2e
    :goto_2e
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_14

    .line 105
    :cond_32
    invoke-virtual {v3}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/g$a;

    invoke-static {v0}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v5

    invoke-static {v4}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v7

    cmpg-double v0, v5, v7

    if-gez v0, :cond_2e

    .line 106
    invoke-virtual {v3}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    .line 107
    invoke-virtual {v3, v4}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_2e

    .line 100
    :cond_4b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_e

    .line 112
    :cond_4f
    const/16 v0, 0x64

    new-array v0, v0, [Lcom/google/android/location/g/g$a;

    .line 113
    invoke-virtual {v3, v0}, Ljava/util/PriorityQueue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/g/g$a;

    .line 114
    sget-object v1, Lcom/google/android/location/g/g;->d:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 117
    const-wide/16 v2, 0x0

    .line 119
    const/4 v1, 0x0

    :goto_61
    array-length v4, v0

    if-ge v1, v4, :cond_72

    cmpg-double v4, v2, p1

    if-gez v4, :cond_72

    .line 120
    aget-object v4, v0, v1

    invoke-static {v4}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v4

    add-double/2addr v2, v4

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_61

    .line 126
    :cond_72
    array-length v2, v0

    const/4 v3, 0x1

    if-le v2, v3, :cond_aa

    const/4 v2, 0x2

    if-lt v1, v2, :cond_aa

    add-int/lit8 v2, v1, -0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v2

    add-int/lit8 v4, v1, -0x2

    aget-object v4, v0, v4

    invoke-static {v4}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-nez v2, :cond_aa

    .line 128
    add-int/lit8 v2, v1, -0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v3

    .line 130
    add-int/lit8 v2, v1, -0x1

    :goto_99
    if-ltz v2, :cond_a7

    .line 131
    aget-object v5, v0, v2

    invoke-static {v5}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Double;->compare(DD)I

    move-result v5

    if-eqz v5, :cond_de

    .line 135
    :cond_a7
    if-ltz v2, :cond_aa

    move v1, v2

    .line 141
    :cond_aa
    new-array v3, v1, [D

    .line 142
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 143
    const/4 v2, 0x0

    :goto_b2
    if-ge v2, v1, :cond_e1

    .line 144
    aget-object v5, v0, v2

    .line 145
    invoke-virtual {v5}, Lcom/google/android/location/g/g$a;->b()I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v6, p4

    iget v8, p0, Lcom/google/android/location/g/g;->b:I

    int-to-double v8, v8

    mul-double/2addr v8, p4

    sub-double/2addr v6, v8

    double-to-int v6, v6

    .line 146
    invoke-virtual {v5}, Lcom/google/android/location/g/g$a;->a()I

    move-result v7

    int-to-double v7, v7

    mul-double/2addr v7, p4

    iget v9, p0, Lcom/google/android/location/g/g;->b:I

    int-to-double v9, v9

    mul-double/2addr v9, p4

    sub-double/2addr v7, v9

    double-to-int v7, v7

    .line 147
    invoke-static {v6, v7, p3}, Lcom/google/android/location/g/c;->a(IILcom/google/android/location/e/w;)Lcom/google/android/location/e/h;

    move-result-object v6

    .line 148
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    invoke-static {v5}, Lcom/google/android/location/g/g$a;->a(Lcom/google/android/location/g/g$a;)D

    move-result-wide v5

    aput-wide v5, v3, v2

    .line 143
    add-int/lit8 v2, v2, 0x1

    goto :goto_b2

    .line 130
    :cond_de
    add-int/lit8 v2, v2, -0x1

    goto :goto_99

    .line 152
    :cond_e1
    new-instance v2, Lcom/google/android/location/g/m;

    invoke-direct {v2}, Lcom/google/android/location/g/m;-><init>()V

    .line 153
    invoke-virtual {v2, v4, v3}, Lcom/google/android/location/g/m;->a(Ljava/util/List;[D)Lcom/google/android/location/e/h;

    move-result-object v5

    .line 154
    if-nez v5, :cond_ee

    .line 155
    const/4 v0, 0x0

    .line 188
    :goto_ed
    return-object v0

    .line 159
    :cond_ee
    invoke-static {v5, p3}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/h;Lcom/google/android/location/e/w;)D

    move-result-wide v2

    const-wide/high16 v6, 0x400e

    div-double/2addr v2, v6

    iget v4, p0, Lcom/google/android/location/g/g;->b:I

    int-to-double v6, v4

    add-double/2addr v2, v6

    double-to-int v6, v2

    .line 163
    invoke-static {v5, p3}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/h;Lcom/google/android/location/e/w;)D

    move-result-wide v2

    const-wide/high16 v7, 0x400e

    div-double/2addr v2, v7

    iget v4, p0, Lcom/google/android/location/g/g;->b:I

    int-to-double v7, v4

    add-double/2addr v2, v7

    double-to-int v7, v2

    .line 167
    const/high16 v3, -0x8000

    .line 168
    const/4 v2, 0x0

    move v4, v2

    :goto_10a
    if-ge v4, v1, :cond_13a

    .line 169
    aget-object v2, v0, v4

    .line 170
    invoke-static {v2}, Lcom/google/android/location/g/g$a;->b(Lcom/google/android/location/g/g$a;)I

    move-result v8

    sub-int/2addr v8, v7

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    .line 171
    invoke-static {v2}, Lcom/google/android/location/g/g$a;->c(Lcom/google/android/location/g/g$a;)I

    move-result v2

    sub-int/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 173
    const/16 v9, 0x78

    if-ge v8, v9, :cond_128

    const/16 v9, 0x78

    if-lt v2, v9, :cond_133

    .line 175
    :cond_128
    invoke-static {v2, v8}, Lcom/google/android/location/g/f;->a(II)I

    move-result v2

    .line 180
    :goto_12c
    if-le v2, v3, :cond_14d

    .line 168
    :goto_12e
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    goto :goto_10a

    .line 177
    :cond_133
    sget-object v9, Lcom/google/android/location/g/f;->a:[[I

    aget-object v2, v9, v2

    aget v2, v2, v8

    goto :goto_12c

    .line 184
    :cond_13a
    const/16 v0, 0x14

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 185
    new-instance v0, Lcom/google/android/location/e/w$a;

    invoke-direct {v0}, Lcom/google/android/location/e/w$a;-><init>()V

    .line 186
    invoke-virtual {v0, v5}, Lcom/google/android/location/e/w$a;->a(Lcom/google/android/location/e/h;)Lcom/google/android/location/e/w$a;

    .line 187
    mul-int/lit16 v1, v1, 0x3e8

    iput v1, v0, Lcom/google/android/location/e/w$a;->c:I

    goto :goto_ed

    :cond_14d
    move v2, v3

    goto :goto_12e
.end method

.method a()V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 51
    move v0, v1

    :goto_2
    iget v2, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v0, v2, :cond_19

    move v2, v1

    .line 52
    :goto_7
    iget v3, p0, Lcom/google/android/location/g/g;->a:I

    if-ge v2, v3, :cond_16

    .line 53
    iget-object v3, p0, Lcom/google/android/location/g/g;->c:[[D

    aget-object v3, v3, v0

    const-wide/high16 v4, 0x3ff0

    aput-wide v4, v3, v2

    .line 52
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 51
    :cond_16
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 56
    :cond_19
    return-void
.end method

.method public a(IID)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/g/g;->a(II)Z

    move-result v0

    if-nez v0, :cond_7

    .line 78
    :goto_6
    return-void

    .line 75
    :cond_7
    iget v0, p0, Lcom/google/android/location/g/g;->b:I

    add-int/2addr v0, p1

    .line 76
    iget v1, p0, Lcom/google/android/location/g/g;->b:I

    add-int/2addr v1, p2

    .line 77
    iget-object v2, p0, Lcom/google/android/location/g/g;->c:[[D

    aget-object v0, v2, v0

    aget-wide v2, v0, v1

    const-wide/high16 v4, 0x4059

    mul-double/2addr v4, p3

    mul-double/2addr v2, v4

    aput-wide v2, v0, v1

    goto :goto_6
.end method
