.class final Lcom/google/android/location/i/c$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/i/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation


# instance fields
.field private a:I

.field private b:I


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367
    iput v0, p0, Lcom/google/android/location/i/c$b;->a:I

    .line 369
    iput v0, p0, Lcom/google/android/location/i/c$b;->b:I

    return-void
.end method

.method private declared-synchronized b()V
    .registers 2

    .prologue
    .line 375
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput v0, p0, Lcom/google/android/location/i/c$b;->a:I

    .line 376
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/i/c$b;->b:I
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_9

    .line 377
    monitor-exit p0

    return-void

    .line 375
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4

    .prologue
    .line 401
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/location/i/c$b;->a:I

    if-nez v0, :cond_b

    .line 402
    const/4 v0, 0x0

    .line 408
    :goto_6
    invoke-direct {p0}, Lcom/google/android/location/i/c$b;->b()V
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_1f

    .line 409
    monitor-exit p0

    return-object v0

    .line 404
    :cond_b
    :try_start_b
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->F:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 405
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/location/i/c$b;->a:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 406
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/location/i/c$b;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_1e
    .catchall {:try_start_b .. :try_end_1e} :catchall_1f

    goto :goto_6

    .line 401
    :catchall_1f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 386
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/location/i/c$b;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/i/c$b;->a:I

    .line 387
    if-eqz p1, :cond_f

    .line 388
    iget v0, p0, Lcom/google/android/location/i/c$b;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/i/c$b;->b:I
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    .line 390
    :cond_f
    monitor-exit p0

    return-void

    .line 386
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method
