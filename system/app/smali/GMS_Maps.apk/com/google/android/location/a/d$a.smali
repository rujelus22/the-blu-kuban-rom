.class Lcom/google/android/location/a/d$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/location/a/d$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:D


# direct methods
.method private constructor <init>(ID)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    iput p1, p0, Lcom/google/android/location/a/d$a;->a:I

    .line 211
    iput-wide p2, p0, Lcom/google/android/location/a/d$a;->b:D

    .line 212
    return-void
.end method

.method synthetic constructor <init>(IDLcom/google/android/location/a/d$1;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 205
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/a/d$a;-><init>(ID)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/a/d$a;)I
    .registers 2
    .parameter

    .prologue
    .line 205
    iget v0, p0, Lcom/google/android/location/a/d$a;->a:I

    return v0
.end method


# virtual methods
.method public a(Lcom/google/android/location/a/d$a;)I
    .registers 6
    .parameter

    .prologue
    .line 216
    iget-wide v0, p0, Lcom/google/android/location/a/d$a;->b:D

    iget-wide v2, p1, Lcom/google/android/location/a/d$a;->b:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 205
    check-cast p1, Lcom/google/android/location/a/d$a;

    invoke-virtual {p0, p1}, Lcom/google/android/location/a/d$a;->a(Lcom/google/android/location/a/d$a;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BucketIndexPowerPair [bucketIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/a/d$a;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", power="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/a/d$a;->b:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
