.class public Lcom/google/android/location/i;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/i$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240
    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)F
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    const/high16 v2, 0x43b4

    const/4 v6, 0x2

    .line 140
    invoke-virtual {p1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-nez v0, :cond_c

    move v1, v2

    .line 163
    :cond_b
    return v1

    .line 143
    :cond_c
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 144
    :goto_12
    invoke-virtual {p1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-ge v0, v3, :cond_2b

    .line 145
    invoke-virtual {p1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 146
    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 148
    :cond_2b
    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 149
    const/4 v0, 0x0

    .line 150
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v3, v1

    move v1, v0

    .line 151
    :goto_35
    if-ge v3, v5, :cond_b

    .line 153
    if-nez v3, :cond_5c

    .line 155
    add-int/lit8 v0, v5, -0x1

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float v0, v6, v0

    sub-float v0, v2, v0

    .line 159
    :goto_53
    cmpl-float v6, v0, v1

    if-lez v6, :cond_75

    .line 151
    :goto_57
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_35

    .line 157
    :cond_5c
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v6

    add-int/lit8 v0, v3, -0x1

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float v0, v6, v0

    goto :goto_53

    :cond_75
    move v0, v1

    goto :goto_57
.end method


# virtual methods
.method a(Ljava/util/List;Z)F
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;Z)F"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 67
    const/4 v2, 0x0

    .line 68
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v0

    move v1, v0

    .line 70
    :goto_8
    if-ge v3, v4, :cond_23

    .line 71
    if-eqz p2, :cond_f

    const/4 v0, 0x3

    if-lt v3, v0, :cond_2c

    .line 72
    :cond_f
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v0}, Lcom/google/android/location/i;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)F

    move-result v0

    add-float/2addr v2, v0

    .line 73
    add-int/lit8 v0, v1, 0x1

    move v1, v2

    .line 70
    :goto_1d
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_8

    .line 76
    :cond_23
    if-nez v1, :cond_28

    .line 77
    const/high16 v0, 0x43b4

    .line 79
    :goto_27
    return v0

    :cond_28
    int-to-float v0, v1

    div-float v0, v2, v0

    goto :goto_27

    :cond_2c
    move v0, v1

    move v1, v2

    goto :goto_1d
.end method

.method public a(Ljava/util/List;)Lcom/google/android/location/i$a;
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)",
            "Lcom/google/android/location/i$a;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/location/i$a;

    invoke-virtual {p0, p1}, Lcom/google/android/location/i;->b(Ljava/util/List;)F

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {p0, p1, v2}, Lcom/google/android/location/i;->a(Ljava/util/List;Z)F

    move-result v2

    invoke-virtual {p0, p1}, Lcom/google/android/location/i;->c(Ljava/util/List;)F

    move-result v3

    invoke-virtual {p0, p1}, Lcom/google/android/location/i;->d(Ljava/util/List;)F

    move-result v4

    invoke-virtual {p0, p1}, Lcom/google/android/location/i;->e(Ljava/util/List;)F

    move-result v5

    invoke-virtual {p0, p1}, Lcom/google/android/location/i;->f(Ljava/util/List;)F

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/i$a;-><init>(FFFFFF)V

    return-object v0
.end method

.method b(Ljava/util/List;)F
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)F"
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    const/4 v0, 0x0

    .line 45
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 46
    if-gt v3, v5, :cond_a

    .line 47
    const/4 v0, 0x0

    .line 55
    :goto_9
    return v0

    :cond_a
    move v2, v0

    move v1, v0

    .line 50
    :goto_c
    if-ge v2, v3, :cond_21

    .line 51
    if-lt v2, v5, :cond_27

    .line 52
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 50
    :goto_1c
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_c

    .line 55
    :cond_21
    int-to-float v0, v1

    add-int/lit8 v1, v3, -0x3

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_9

    :cond_27
    move v0, v1

    goto :goto_1c
.end method

.method c(Ljava/util/List;)F
    .registers 11
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)F"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 90
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 91
    const/4 v0, 0x5

    if-ge v2, v0, :cond_b

    .line 93
    const/high16 v0, 0x43b4

    .line 105
    :goto_a
    return v0

    .line 95
    :cond_b
    add-int/lit8 v0, v2, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    .line 96
    const-wide/16 v3, 0x2

    div-long v3, v0, v3

    .line 97
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 98
    const/4 v0, 0x0

    move v1, v0

    :goto_23
    if-ge v1, v2, :cond_3f

    .line 99
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v6, v0

    cmp-long v0, v6, v3

    if-gtz v0, :cond_3f

    .line 100
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_23

    .line 105
    :cond_3f
    invoke-virtual {p0, v5, v8}, Lcom/google/android/location/i;->a(Ljava/util/List;Z)F

    move-result v0

    goto :goto_a
.end method

.method d(Ljava/util/List;)F
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)F"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 116
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 117
    const/4 v0, 0x5

    if-ge v1, v0, :cond_b

    .line 119
    const/high16 v0, 0x43b4

    .line 132
    :goto_a
    return v0

    .line 121
    :cond_b
    add-int/lit8 v0, v1, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v2, v0

    .line 122
    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    .line 123
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 124
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    :goto_23
    if-ltz v1, :cond_3f

    .line 125
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v5, v0

    cmp-long v0, v5, v2

    if-lez v0, :cond_3f

    .line 126
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_23

    .line 131
    :cond_3f
    invoke-static {v4}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 132
    const/4 v0, 0x0

    invoke-virtual {p0, v4, v0}, Lcom/google/android/location/i;->a(Ljava/util/List;Z)F

    move-result v0

    goto :goto_a
.end method

.method e(Ljava/util/List;)F
    .registers 16
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)F"
        }
    .end annotation

    .prologue
    const/4 v13, 0x3

    const/4 v2, 0x0

    const/high16 v12, 0x4190

    const/4 v11, 0x2

    const/4 v4, 0x0

    .line 176
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    .line 177
    if-gt v8, v13, :cond_d

    .line 202
    :cond_c
    :goto_c
    return v4

    :cond_d
    move v7, v2

    move v5, v4

    move v6, v4

    .line 182
    :goto_10
    if-ge v7, v8, :cond_42

    .line 183
    if-lt v7, v13, :cond_49

    .line 185
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v1, v2

    move v3, v4

    .line 186
    :goto_1c
    invoke-virtual {v0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v9

    if-ge v1, v9, :cond_33

    .line 187
    invoke-virtual {v0, v11, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v9

    .line 188
    invoke-virtual {v9, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v9

    .line 189
    cmpl-float v10, v9, v12

    if-lez v10, :cond_30

    .line 190
    sub-float/2addr v9, v12

    add-float/2addr v3, v9

    .line 186
    :cond_30
    add-int/lit8 v1, v1, 0x1

    goto :goto_1c

    .line 193
    :cond_33
    cmpl-float v0, v3, v4

    if-lez v0, :cond_49

    .line 194
    add-float v1, v6, v3

    .line 195
    const/high16 v0, 0x3f80

    add-float/2addr v0, v5

    .line 182
    :goto_3c
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v5, v0

    move v6, v1

    goto :goto_10

    .line 199
    :cond_42
    cmpl-float v0, v5, v4

    if-eqz v0, :cond_c

    .line 202
    div-float v4, v6, v5

    goto :goto_c

    :cond_49
    move v0, v5

    move v1, v6

    goto :goto_3c
.end method

.method f(Ljava/util/List;)F
    .registers 16
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)F"
        }
    .end annotation

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 213
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    .line 214
    if-gt v9, v13, :cond_b

    .line 234
    :goto_a
    return v4

    :cond_b
    move v8, v2

    move v6, v2

    move v7, v4

    .line 219
    :goto_e
    if-ge v8, v9, :cond_44

    .line 220
    if-lt v8, v13, :cond_48

    .line 221
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v1, v2

    move v3, v4

    .line 223
    :goto_1a
    invoke-virtual {v0, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    if-ge v1, v5, :cond_3a

    .line 224
    invoke-virtual {v0, v12, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v10

    .line 225
    invoke-virtual {v10, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v5

    .line 226
    invoke-virtual {v10, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v10

    const/high16 v11, 0x4190

    cmpl-float v10, v10, v11

    if-lez v10, :cond_37

    cmpl-float v10, v5, v3

    if-lez v10, :cond_37

    move v3, v5

    .line 223
    :cond_37
    add-int/lit8 v1, v1, 0x1

    goto :goto_1a

    .line 230
    :cond_3a
    add-float v1, v7, v3

    .line 231
    add-int/lit8 v0, v6, 0x1

    .line 219
    :goto_3e
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    move v6, v0

    move v7, v1

    goto :goto_e

    .line 234
    :cond_44
    int-to-float v0, v6

    div-float v4, v7, v0

    goto :goto_a

    :cond_48
    move v0, v6

    move v1, v7

    goto :goto_3e
.end method
