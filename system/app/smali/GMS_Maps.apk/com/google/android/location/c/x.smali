.class Lcom/google/android/location/c/x;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/location/c/m;

.field private c:Lcom/google/android/location/c/k;

.field private d:Lcom/google/android/location/k/a/c;


# direct methods
.method constructor <init>(Landroid/os/Handler;Lcom/google/android/location/k/a/c;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/location/c/x;->a:Landroid/os/Handler;

    .line 29
    iput-object p2, p0, Lcom/google/android/location/c/x;->d:Lcom/google/android/location/k/a/c;

    .line 30
    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/location/c/x;->b:Lcom/google/android/location/c/m;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/location/c/x;->c:Lcom/google/android/location/c/k;

    if-eqz v0, :cond_f

    .line 63
    iget-object v0, p0, Lcom/google/android/location/c/x;->c:Lcom/google/android/location/c/k;

    iget-object v1, p0, Lcom/google/android/location/c/x;->b:Lcom/google/android/location/c/m;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/k;->a(Lcom/google/android/location/c/E;)V

    .line 65
    :cond_f
    return-void
.end method

.method a(Landroid/content/Context;Lcom/google/android/location/c/y;Lcom/google/android/location/d/a;Lcom/google/android/location/c/l;)Z
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 42
    new-instance v1, Lcom/google/android/location/c/u;

    iget-object v0, p0, Lcom/google/android/location/c/x;->d:Lcom/google/android/location/k/a/c;

    invoke-direct {v1, p4, v0}, Lcom/google/android/location/c/u;-><init>(Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    .line 43
    const/4 v10, 0x0

    .line 44
    new-instance v0, Lcom/google/android/location/c/o;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/location/c/x;->d:Lcom/google/android/location/k/a/c;

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/c/o;-><init>(Lcom/google/android/location/c/D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/k/a/c;)V

    .line 46
    new-instance v1, Lcom/google/android/location/c/k;

    iget-object v2, p0, Lcom/google/android/location/c/x;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/location/c/x;->d:Lcom/google/android/location/k/a/c;

    invoke-direct {v1, v0, v2, v7, v3}, Lcom/google/android/location/c/k;-><init>(Lcom/google/android/location/c/o;Landroid/os/Handler;ILcom/google/android/location/k/a/c;)V

    iput-object v1, p0, Lcom/google/android/location/c/x;->c:Lcom/google/android/location/c/k;

    .line 48
    new-instance v0, Lcom/google/android/location/c/m;

    invoke-interface {p2}, Lcom/google/android/location/c/y;->b()Ljava/util/Set;

    move-result-object v2

    invoke-interface {p2}, Lcom/google/android/location/c/y;->d()Ljava/util/Map;

    move-result-object v3

    invoke-interface {p2}, Lcom/google/android/location/c/y;->c()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/location/c/x;->c:Lcom/google/android/location/c/k;

    iget-object v9, p0, Lcom/google/android/location/c/x;->d:Lcom/google/android/location/k/a/c;

    move-object v1, p1

    move-object v4, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/c/m;-><init>(Landroid/content/Context;Ljava/util/Set;Ljava/util/Map;Lcom/google/android/location/d/a;ZLcom/google/android/location/c/k;ZLcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    iput-object v0, p0, Lcom/google/android/location/c/x;->b:Lcom/google/android/location/c/m;

    .line 53
    iget-object v0, p0, Lcom/google/android/location/c/x;->b:Lcom/google/android/location/c/m;

    invoke-virtual {v0}, Lcom/google/android/location/c/m;->c()I

    move-result v0

    if-lez v0, :cond_4e

    .line 54
    iget-object v0, p0, Lcom/google/android/location/c/x;->b:Lcom/google/android/location/c/m;

    invoke-interface {p2}, Lcom/google/android/location/c/y;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/c/m;->a(J)V

    .line 57
    :goto_4d
    return v7

    :cond_4e
    move v7, v10

    goto :goto_4d
.end method
