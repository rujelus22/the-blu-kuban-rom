.class public Lcom/google/android/location/b/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/b/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/concurrent/ExecutorService;

.field private final b:Ljava/io/File;

.field private final c:Lcom/google/android/location/c/a;

.field private final d:[B

.field private final e:Lcom/google/android/location/os/f;

.field private final f:Lcom/google/android/location/b/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/j",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final g:Lcom/google/android/location/b/c$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/c$a",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final h:Lcom/google/android/location/b/k$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/k$b",
            "<TK;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/google/android/location/e/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/e/x",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IILjava/util/concurrent/ExecutorService;Lcom/google/android/location/e/x;Lcom/google/android/location/b/k$b;Ljava/io/File;[BLcom/google/android/location/os/f;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/location/e/x",
            "<TV;>;",
            "Lcom/google/android/location/b/k$b",
            "<TK;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            "[B",
            "Lcom/google/android/location/os/f;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    if-lt p1, p2, :cond_e

    .line 150
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Memory capacity is expected to be larger than disk capacity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_e
    iput-object p4, p0, Lcom/google/android/location/b/c;->i:Lcom/google/android/location/e/x;

    .line 156
    iput-object p6, p0, Lcom/google/android/location/b/c;->b:Ljava/io/File;

    .line 157
    iput-object p7, p0, Lcom/google/android/location/b/c;->d:[B

    .line 158
    if-nez p7, :cond_31

    .line 159
    iput-object v1, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    .line 163
    :goto_18
    iput-object p3, p0, Lcom/google/android/location/b/c;->a:Ljava/util/concurrent/ExecutorService;

    .line 165
    new-instance v0, Lcom/google/android/location/b/j;

    invoke-direct {v0, p1}, Lcom/google/android/location/b/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    .line 167
    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-eqz v0, :cond_38

    .line 168
    new-instance v0, Lcom/google/android/location/b/c$a;

    invoke-direct {v0, p2, p3, p6}, Lcom/google/android/location/b/c$a;-><init>(ILjava/util/concurrent/ExecutorService;Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    .line 173
    :goto_2c
    iput-object p5, p0, Lcom/google/android/location/b/c;->h:Lcom/google/android/location/b/k$b;

    .line 174
    iput-object p8, p0, Lcom/google/android/location/b/c;->e:Lcom/google/android/location/os/f;

    .line 175
    return-void

    .line 161
    :cond_31
    invoke-static {p7, v1}, Lcom/google/android/location/c/a;->b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    goto :goto_18

    .line 170
    :cond_38
    iput-object v1, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    goto :goto_2c
.end method

.method static synthetic a(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/j;
    .registers 2
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    return-object v0
.end method

.method static synthetic a(Ljava/io/Closeable;)V
    .registers 1
    .parameter

    .prologue
    .line 91
    invoke-static {p0}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/b/c;)Ljava/io/File;
    .registers 2
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/location/b/c;->b:Ljava/io/File;

    return-object v0
.end method

.method private static b(Ljava/io/Closeable;)V
    .registers 2
    .parameter

    .prologue
    .line 618
    if-eqz p0, :cond_5

    .line 620
    :try_start_2
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_5} :catch_6

    .line 626
    :cond_5
    :goto_5
    return-void

    .line 621
    :catch_6
    move-exception v0

    goto :goto_5
.end method

.method static synthetic c(Lcom/google/android/location/b/c;)[B
    .registers 2
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/location/b/c;->d:[B

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/b/c;)Lcom/google/android/location/e/x;
    .registers 2
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/location/b/c;->i:Lcom/google/android/location/e/x;

    return-object v0
.end method

.method private d()V
    .registers 5

    .prologue
    .line 606
    iget-object v0, p0, Lcom/google/android/location/b/c;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 607
    if-eqz v1, :cond_14

    .line 608
    array-length v2, v1

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v2, :cond_14

    aget-object v3, v1, v0

    .line 609
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 608
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 612
    :cond_14
    return-void
.end method

.method static synthetic e(Lcom/google/android/location/b/c;)Lcom/google/android/location/os/f;
    .registers 2
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/location/b/c;->e:Lcom/google/android/location/os/f;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/c$a;
    .registers 2
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;J)Ljava/lang/Object;
    .registers 9
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;J)TV;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 278
    monitor-enter p0

    .line 279
    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/j;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/A;

    .line 280
    iget-object v1, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-eqz v1, :cond_25

    .line 281
    iget-object v1, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v1, p1}, Lcom/google/android/location/b/c$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/A;

    .line 285
    :goto_16
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_2 .. :try_end_17} :catchall_27

    .line 287
    if-eqz v0, :cond_2a

    .line 290
    iget-object v2, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-eqz v2, :cond_20

    .line 292
    invoke-virtual {v1, p2, p3}, Lcom/google/android/location/e/A;->b(J)V

    .line 294
    :cond_20
    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/e/A;->a(J)Ljava/lang/Object;

    move-result-object v0

    .line 369
    :goto_24
    return-object v0

    :cond_25
    move-object v1, v2

    .line 283
    goto :goto_16

    .line 285
    :catchall_27
    move-exception v0

    :try_start_28
    monitor-exit p0
    :try_end_29
    .catchall {:try_start_28 .. :try_end_29} :catchall_27

    throw v0

    .line 295
    :cond_2a
    if-eqz v1, :cond_59

    .line 299
    const-string v0, ""

    invoke-virtual {v1}, Lcom/google/android/location/e/A;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 300
    new-instance v0, Lcom/google/android/location/e/A;

    invoke-virtual {v1}, Lcom/google/android/location/e/A;->b()J

    move-result-wide v3

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/location/e/A;-><init>(Ljava/lang/Object;J)V

    .line 303
    monitor-enter p0

    .line 304
    :try_start_42
    iget-object v1, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/b/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    monitor-exit p0

    move-object v0, v2

    .line 307
    goto :goto_24

    .line 305
    :catchall_4a
    move-exception v0

    monitor-exit p0
    :try_end_4c
    .catchall {:try_start_42 .. :try_end_4c} :catchall_4a

    throw v0

    .line 311
    :cond_4d
    iget-object v0, p0, Lcom/google/android/location/b/c;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/google/android/location/b/c$1;

    invoke-direct {v3, p0, p1, v1}, Lcom/google/android/location/b/c$1;-><init>(Lcom/google/android/location/b/c;Ljava/lang/Object;Lcom/google/android/location/e/A;)V

    invoke-interface {v0, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-object v0, v2

    .line 366
    goto :goto_24

    :cond_59
    move-object v0, v2

    .line 369
    goto :goto_24
.end method

.method public declared-synchronized a()V
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 484
    monitor-enter p0

    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_8a

    if-nez v0, :cond_8

    .line 547
    :cond_6
    monitor-exit p0

    return-void

    .line 489
    :cond_8
    :try_start_8
    new-instance v3, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/b/c;->b:Ljava/io/File;

    const-string v1, "lru.cache"

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_11
    .catchall {:try_start_8 .. :try_end_11} :catchall_8a

    .line 490
    const/4 v0, 0x0

    .line 493
    :try_start_12
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1c
    .catchall {:try_start_12 .. :try_end_1c} :catchall_9b
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_1c} :catch_c4
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_1c} :catch_8d

    .line 494
    :try_start_1c
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    .line 496
    const/4 v3, 0x1

    if-eq v0, v3, :cond_6a

    .line 499
    invoke-direct {p0}, Lcom/google/android/location/b/c;->d()V
    :try_end_26
    .catchall {:try_start_1c .. :try_end_26} :catchall_bb
    .catch Ljava/io/FileNotFoundException; {:try_start_1c .. :try_end_26} :catch_81
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_26} :catch_c2

    .line 518
    :goto_26
    :try_start_26
    invoke-static {v1}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    .line 523
    :goto_29
    iget-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0}, Lcom/google/android/location/b/j;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v1}, Lcom/google/android/location/b/c$a;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 524
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 526
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v3}, Lcom/google/android/location/b/c$a;->size()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v4}, Lcom/google/android/location/b/j;->a()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 533
    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/c$a;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v0, v2

    .line 534
    :goto_5b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a7

    .line 535
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_64
    .catchall {:try_start_26 .. :try_end_64} :catchall_8a

    move-result-object v2

    .line 536
    if-ge v0, v3, :cond_a3

    .line 537
    add-int/lit8 v0, v0, 0x1

    goto :goto_5b

    .line 501
    :cond_6a
    :try_start_6a
    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/e/u;

    move-result-object v0

    .line 502
    iget-object v3, p0, Lcom/google/android/location/b/c;->h:Lcom/google/android/location/b/k$b;

    iget-object v4, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, [B

    invoke-direct {v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-interface {v3, v4, v5}, Lcom/google/android/location/b/k$b;->a(Lcom/google/android/location/b/j;Ljava/io/InputStream;)V
    :try_end_80
    .catchall {:try_start_6a .. :try_end_80} :catchall_bb
    .catch Ljava/io/FileNotFoundException; {:try_start_6a .. :try_end_80} :catch_81
    .catch Ljava/io/IOException; {:try_start_6a .. :try_end_80} :catch_c2

    goto :goto_26

    .line 506
    :catch_81
    move-exception v0

    move-object v0, v1

    .line 510
    :goto_83
    :try_start_83
    invoke-direct {p0}, Lcom/google/android/location/b/c;->d()V
    :try_end_86
    .catchall {:try_start_83 .. :try_end_86} :catchall_bd

    .line 518
    :try_start_86
    invoke-static {v0}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V
    :try_end_89
    .catchall {:try_start_86 .. :try_end_89} :catchall_8a

    goto :goto_29

    .line 484
    :catchall_8a
    move-exception v0

    monitor-exit p0

    throw v0

    .line 511
    :catch_8d
    move-exception v1

    move-object v1, v0

    .line 514
    :goto_8f
    :try_start_8f
    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/c$a;->clear()V

    .line 516
    invoke-direct {p0}, Lcom/google/android/location/b/c;->d()V
    :try_end_97
    .catchall {:try_start_8f .. :try_end_97} :catchall_bb

    .line 518
    :try_start_97
    invoke-static {v1}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    goto :goto_29

    :catchall_9b
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_9f
    invoke-static {v1}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    throw v0

    .line 539
    :cond_a3
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5b

    .line 544
    :cond_a7
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_ab
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 545
    const-wide/16 v2, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/location/b/c;->a(Ljava/lang/Object;J)Ljava/lang/Object;
    :try_end_ba
    .catchall {:try_start_97 .. :try_end_ba} :catchall_8a

    goto :goto_ab

    .line 518
    :catchall_bb
    move-exception v0

    goto :goto_9f

    :catchall_bd
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_9f

    .line 511
    :catch_c2
    move-exception v0

    goto :goto_8f

    .line 506
    :catch_c4
    move-exception v1

    goto :goto_83
.end method

.method public declared-synchronized a(JJ)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 471
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/b/j;->a(JJ)V

    .line 473
    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-eqz v0, :cond_f

    .line 476
    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/b/c$a;->a(JJ)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    .line 478
    :cond_f
    monitor-exit p0

    return-void

    .line 471
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/Object;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-nez v0, :cond_15

    .line 383
    new-instance v0, Lcom/google/android/location/e/A;

    iget-object v1, p0, Lcom/google/android/location/b/c;->i:Lcom/google/android/location/e/x;

    invoke-virtual {v1, p2}, Lcom/google/android/location/e/x;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1, p3, p4}, Lcom/google/android/location/e/A;-><init>(Ljava/lang/Object;J)V

    .line 384
    iget-object v1, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/b/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    :cond_14
    :goto_14
    return-void

    .line 388
    :cond_15
    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/c$a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 394
    new-instance v1, Lcom/google/android/location/e/A;

    iget-object v0, p0, Lcom/google/android/location/b/c;->i:Lcom/google/android/location/e/x;

    invoke-virtual {v0, p2}, Lcom/google/android/location/e/x;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v1, v0, p3, p4}, Lcom/google/android/location/e/A;-><init>(Ljava/lang/Object;J)V

    .line 398
    new-instance v2, Lcom/google/android/location/e/A;

    if-nez p2, :cond_43

    const-string v0, ""

    :goto_2e
    invoke-direct {v2, v0, p3, p4}, Lcom/google/android/location/e/A;-><init>(Ljava/lang/Object;J)V

    .line 405
    monitor-enter p0

    .line 406
    :try_start_32
    iget-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/b/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v0, p1, v2}, Lcom/google/android/location/b/c$a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    if-nez p2, :cond_5b

    .line 411
    monitor-exit p0

    goto :goto_14

    .line 458
    :catchall_40
    move-exception v0

    monitor-exit p0
    :try_end_42
    .catchall {:try_start_32 .. :try_end_42} :catchall_40

    throw v0

    .line 398
    :cond_43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3, p4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".cache"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2e

    .line 420
    :cond_5b
    :try_start_5b
    iget-object v0, p0, Lcom/google/android/location/b/c;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/location/b/c$2;

    invoke-direct {v1, p0, v2, p2, p1}, Lcom/google/android/location/b/c$2;-><init>(Lcom/google/android/location/b/c;Lcom/google/android/location/e/A;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 458
    monitor-exit p0
    :try_end_66
    .catchall {:try_start_5b .. :try_end_66} :catchall_40

    goto :goto_14
.end method

.method public declared-synchronized a(Ljava/lang/Object;)Z
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)Z"
        }
    .end annotation

    .prologue
    .line 251
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-eqz v0, :cond_d

    .line 252
    iget-object v0, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/c$a;->containsKey(Ljava/lang/Object;)Z
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_14

    move-result v0

    .line 254
    :goto_b
    monitor-exit p0

    return v0

    :cond_d
    :try_start_d
    iget-object v0, p0, Lcom/google/android/location/b/c;->f:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/j;->containsKey(Ljava/lang/Object;)Z
    :try_end_12
    .catchall {:try_start_d .. :try_end_12} :catchall_14

    move-result v0

    goto :goto_b

    .line 251
    :catchall_14
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()V
    .registers 7

    .prologue
    .line 553
    iget-object v0, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    if-nez v0, :cond_5

    .line 591
    :goto_4
    return-void

    .line 557
    :cond_5
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/b/c;->b:Ljava/io/File;

    const-string v1, "lru.cache"

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 558
    const/4 v0, 0x0

    .line 560
    :try_start_f
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 561
    iget-object v1, p0, Lcom/google/android/location/b/c;->e:Lcom/google/android/location/os/f;

    invoke-interface {v1, v2}, Lcom/google/android/location/os/f;->a(Ljava/io/File;)V

    .line 563
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_21
    .catchall {:try_start_f .. :try_end_21} :catchall_55
    .catch Ljava/io/FileNotFoundException; {:try_start_f .. :try_end_21} :catch_61
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_21} :catch_4c

    .line 565
    const/4 v0, 0x1

    :try_start_22
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 568
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 569
    monitor-enter p0
    :try_end_2b
    .catchall {:try_start_22 .. :try_end_2b} :catchall_5d
    .catch Ljava/io/FileNotFoundException; {:try_start_22 .. :try_end_2b} :catch_46
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_2b} :catch_5f

    .line 570
    :try_start_2b
    iget-object v3, p0, Lcom/google/android/location/b/c;->h:Lcom/google/android/location/b/k$b;

    iget-object v4, p0, Lcom/google/android/location/b/c;->g:Lcom/google/android/location/b/c$a;

    invoke-interface {v3, v4, v0}, Lcom/google/android/location/b/k$b;->a(Lcom/google/android/location/b/j;Ljava/io/OutputStream;)V

    .line 571
    monitor-exit p0
    :try_end_33
    .catchall {:try_start_2b .. :try_end_33} :catchall_43

    .line 572
    :try_start_33
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 575
    iget-object v3, p0, Lcom/google/android/location/b/c;->c:Lcom/google/android/location/c/a;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/google/android/location/c/a;->a(Ljava/io/DataOutputStream;[B)V
    :try_end_3f
    .catchall {:try_start_33 .. :try_end_3f} :catchall_5d
    .catch Ljava/io/FileNotFoundException; {:try_start_33 .. :try_end_3f} :catch_46
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_3f} :catch_5f

    .line 589
    invoke-static {v1}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    goto :goto_4

    .line 571
    :catchall_43
    move-exception v0

    :try_start_44
    monitor-exit p0
    :try_end_45
    .catchall {:try_start_44 .. :try_end_45} :catchall_43

    :try_start_45
    throw v0
    :try_end_46
    .catchall {:try_start_45 .. :try_end_46} :catchall_5d
    .catch Ljava/io/FileNotFoundException; {:try_start_45 .. :try_end_46} :catch_46
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_46} :catch_5f

    .line 576
    :catch_46
    move-exception v0

    move-object v0, v1

    .line 589
    :goto_48
    invoke-static {v0}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    goto :goto_4

    .line 581
    :catch_4c
    move-exception v1

    move-object v1, v0

    .line 584
    :goto_4e
    :try_start_4e
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_51
    .catchall {:try_start_4e .. :try_end_51} :catchall_5d

    .line 589
    invoke-static {v1}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    goto :goto_4

    :catchall_55
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_59
    invoke-static {v1}, Lcom/google/android/location/b/c;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_5d
    move-exception v0

    goto :goto_59

    .line 581
    :catch_5f
    move-exception v0

    goto :goto_4e

    .line 576
    :catch_61
    move-exception v1

    goto :goto_48
.end method

.method public c()V
    .registers 2

    .prologue
    .line 597
    invoke-direct {p0}, Lcom/google/android/location/b/c;->d()V

    .line 598
    iget-object v0, p0, Lcom/google/android/location/b/c;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 599
    return-void
.end method
