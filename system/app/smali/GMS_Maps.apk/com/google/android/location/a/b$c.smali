.class Lcom/google/android/location/a/b$c;
.super Lcom/google/android/location/a/b$d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/a/b;


# direct methods
.method private constructor <init>(Lcom/google/android/location/a/b;)V
    .registers 3
    .parameter

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/b$d;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;)V

    return-void
.end method

.method private b(Z)V
    .registers 9
    .parameter

    .prologue
    .line 137
    if-nez p1, :cond_8

    .line 140
    iget-object v0, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->d(Lcom/google/android/location/a/b;)V

    .line 148
    :cond_7
    :goto_7
    return-void

    .line 142
    :cond_8
    iget-object v0, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->e(Lcom/google/android/location/a/b;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_7

    .line 145
    iget-object v0, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    iget-object v1, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v1}, Lcom/google/android/location/a/b;->b(Lcom/google/android/location/a/b;)Lcom/google/android/location/os/i;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    const-wide/32 v3, 0xafc8

    iget-object v5, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v5}, Lcom/google/android/location/a/b;->c(Lcom/google/android/location/a/b;)J

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    add-long/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;J)V

    goto :goto_7
.end method

.method private g()V
    .registers 5

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/google/android/location/a/b$c;->f()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 185
    iget-object v0, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$a;

    iget-object v2, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/a/b$a;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    .line 187
    :cond_13
    return-void
.end method


# virtual methods
.method protected a()V
    .registers 6

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/android/location/a/b$c;->f()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 112
    iget-object v0, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    iget-object v1, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v1}, Lcom/google/android/location/a/b;->b(Lcom/google/android/location/a/b;)Lcom/google/android/location/os/i;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v3}, Lcom/google/android/location/a/b;->c(Lcom/google/android/location/a/b;)J

    move-result-wide v3

    add-long/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;J)V

    .line 114
    :cond_1c
    return-void
.end method

.method protected a(Lcom/google/android/location/a/n$b;)V
    .registers 8
    .parameter

    .prologue
    const-wide/16 v4, -0x1

    .line 154
    iget-object v0, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->f(Lcom/google/android/location/a/b;)Lcom/google/android/location/a/n$b;

    move-result-object v0

    if-eqz v0, :cond_55

    iget-object v0, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->f(Lcom/google/android/location/a/b;)Lcom/google/android/location/a/n$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/a/n$b;->a()Lcom/google/android/location/e/B;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/e/B;->c:Lcom/google/android/location/e/B;

    if-eq v0, v1, :cond_55

    invoke-virtual {p1}, Lcom/google/android/location/a/n$b;->a()Lcom/google/android/location/e/B;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/e/B;->c:Lcom/google/android/location/e/B;

    if-eq v0, v1, :cond_55

    iget-object v0, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->f(Lcom/google/android/location/a/b;)Lcom/google/android/location/a/n$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/a/n$b;->a()Lcom/google/android/location/e/B;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/location/a/n$b;->a()Lcom/google/android/location/e/B;

    move-result-object v1

    if-eq v0, v1, :cond_55

    .line 160
    iget-object v0, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->g(Lcom/google/android/location/a/b;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_52

    iget-object v0, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->b(Lcom/google/android/location/a/b;)Lcom/google/android/location/os/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v2}, Lcom/google/android/location/a/b;->g(Lcom/google/android/location/a/b;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xafc8

    cmp-long v0, v0, v2

    if-lez v0, :cond_56

    .line 163
    :cond_52
    invoke-direct {p0}, Lcom/google/android/location/a/b$c;->g()V

    .line 175
    :cond_55
    :goto_55
    return-void

    .line 169
    :cond_56
    iget-object v0, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->b(Lcom/google/android/location/a/b;)Lcom/google/android/location/os/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    .line 170
    iget-object v2, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v2}, Lcom/google/android/location/a/b;->e(Lcom/google/android/location/a/b;)J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_78

    iget-object v2, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v2}, Lcom/google/android/location/a/b;->e(Lcom/google/android/location/a/b;)J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-lez v2, :cond_55

    .line 171
    :cond_78
    iget-object v2, p0, Lcom/google/android/location/a/b$c;->a:Lcom/google/android/location/a/b;

    invoke-static {v2, v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;J)V

    goto :goto_55
.end method

.method protected a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 123
    invoke-super {p0, p1}, Lcom/google/android/location/a/b$d;->a(Z)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/location/a/b$c;->f()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/location/a/b$c;->b(Z)V

    .line 125
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 129
    invoke-super {p0, p1, p2}, Lcom/google/android/location/a/b$d;->a(ZLjava/lang/String;)V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/location/a/b$c;->f()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/location/a/b$c;->b(Z)V

    .line 131
    return-void
.end method

.method protected c()V
    .registers 1

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/google/android/location/a/b$c;->g()V

    .line 119
    return-void
.end method

.method protected d()V
    .registers 1

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/google/android/location/a/b$c;->g()V

    .line 181
    return-void
.end method
