.class Lcom/google/android/location/a/g$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field final a:D

.field final b:D


# direct methods
.method constructor <init>(DD)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-wide p1, p0, Lcom/google/android/location/a/g$a;->a:D

    .line 89
    iput-wide p3, p0, Lcom/google/android/location/a/g$a;->b:D

    .line 90
    return-void
.end method


# virtual methods
.method a(Lcom/google/android/location/a/g$a;)Lcom/google/android/location/a/g$a;
    .registers 9
    .parameter

    .prologue
    .line 93
    new-instance v0, Lcom/google/android/location/a/g$a;

    iget-wide v1, p0, Lcom/google/android/location/a/g$a;->a:D

    iget-wide v3, p1, Lcom/google/android/location/a/g$a;->a:D

    add-double/2addr v1, v3

    iget-wide v3, p0, Lcom/google/android/location/a/g$a;->b:D

    iget-wide v5, p1, Lcom/google/android/location/a/g$a;->b:D

    add-double/2addr v3, v5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/a/g$a;-><init>(DD)V

    return-object v0
.end method

.method b(Lcom/google/android/location/a/g$a;)Lcom/google/android/location/a/g$a;
    .registers 9
    .parameter

    .prologue
    .line 97
    new-instance v0, Lcom/google/android/location/a/g$a;

    iget-wide v1, p0, Lcom/google/android/location/a/g$a;->a:D

    iget-wide v3, p1, Lcom/google/android/location/a/g$a;->a:D

    sub-double/2addr v1, v3

    iget-wide v3, p0, Lcom/google/android/location/a/g$a;->b:D

    iget-wide v5, p1, Lcom/google/android/location/a/g$a;->b:D

    sub-double/2addr v3, v5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/a/g$a;-><init>(DD)V

    return-object v0
.end method

.method c(Lcom/google/android/location/a/g$a;)Lcom/google/android/location/a/g$a;
    .registers 10
    .parameter

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/google/android/location/a/g$a;->a:D

    iget-wide v2, p1, Lcom/google/android/location/a/g$a;->a:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/location/a/g$a;->b:D

    iget-wide v4, p1, Lcom/google/android/location/a/g$a;->b:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    .line 102
    iget-wide v2, p0, Lcom/google/android/location/a/g$a;->a:D

    iget-wide v4, p1, Lcom/google/android/location/a/g$a;->b:D

    mul-double/2addr v2, v4

    iget-wide v4, p0, Lcom/google/android/location/a/g$a;->b:D

    iget-wide v6, p1, Lcom/google/android/location/a/g$a;->a:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 103
    new-instance v4, Lcom/google/android/location/a/g$a;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/location/a/g$a;-><init>(DD)V

    return-object v4
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ComplexNumber [real="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/a/g$a;->a:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", imaginary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/a/g$a;->b:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
