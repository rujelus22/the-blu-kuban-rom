.class Lcom/google/android/location/c/s;
.super Lcom/google/android/location/c/D;
.source "SourceFile"


# instance fields
.field private volatile e:Z

.field private final f:Ljava/util/concurrent/ThreadPoolExecutor;

.field private final g:Landroid/os/PowerManager;

.field private volatile h:Ljava/lang/String;

.field private final i:[B

.field private volatile j:Z

.field private volatile k:Ljava/lang/String;

.field private l:Ljava/lang/Object;

.field private volatile m:Lcom/google/android/location/c/c;


# direct methods
.method constructor <init>(Landroid/os/PowerManager;Ljava/lang/String;[BLcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;Lcom/google/android/location/c/H;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 87
    invoke-direct {p0, p4, p5, p6}, Lcom/google/android/location/c/D;-><init>(Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;Lcom/google/android/location/c/H;)V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c/s;->e:Z

    .line 46
    new-instance v0, Lcom/google/android/location/c/s$1;

    const/4 v2, 0x1

    const/4 v3, 0x1

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v1, 0x32

    invoke-direct {v7, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    new-instance v8, Ljava/util/concurrent/ThreadPoolExecutor$AbortPolicy;

    invoke-direct {v8}, Ljava/util/concurrent/ThreadPoolExecutor$AbortPolicy;-><init>()V

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/c/s$1;-><init>(Lcom/google/android/location/c/s;IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/RejectedExecutionHandler;)V

    iput-object v0, p0, Lcom/google/android/location/c/s;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c/s;->j:Z

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/s;->k:Ljava/lang/String;

    .line 81
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/c/s;->l:Ljava/lang/Object;

    .line 88
    iput-object p1, p0, Lcom/google/android/location/c/s;->g:Landroid/os/PowerManager;

    .line 89
    iput-object p2, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_5a

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    .line 93
    :cond_5a
    iput-object p3, p0, Lcom/google/android/location/c/s;->i:[B

    .line 94
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/c/D$a;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/location/c/s;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/c/D$a;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;[B)Lcom/google/android/location/c/D$a;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 272
    invoke-direct {p0}, Lcom/google/android/location/c/s;->c()Lcom/google/android/location/c/D$a;

    .line 275
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 276
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    :try_start_24
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_29
    .catchall {:try_start_24 .. :try_end_29} :catchall_59
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_29} :catch_39

    .line 280
    :try_start_29
    invoke-virtual {v1, p2}, Ljava/io/FileOutputStream;->write([B)V

    .line 281
    new-instance v0, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_33
    .catchall {:try_start_29 .. :try_end_33} :catchall_65
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_33} :catch_67

    .line 286
    if-eqz v1, :cond_38

    .line 288
    :try_start_35
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_38} :catch_61

    .line 290
    :cond_38
    :goto_38
    return-object v0

    .line 282
    :catch_39
    move-exception v0

    move-object v1, v2

    .line 283
    :goto_3b
    :try_start_3b
    const-string v2, "Failed to save data: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 284
    new-instance v0, Lcom/google/android/location/c/D$a;

    const/4 v4, 0x0

    invoke-direct {v0, v4, v3, v2}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_51
    .catchall {:try_start_3b .. :try_end_51} :catchall_65

    .line 286
    if-eqz v1, :cond_38

    .line 288
    :try_start_53
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_56
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_56} :catch_57

    goto :goto_38

    .line 289
    :catch_57
    move-exception v1

    goto :goto_38

    .line 286
    :catchall_59
    move-exception v0

    move-object v1, v2

    :goto_5b
    if-eqz v1, :cond_60

    .line 288
    :try_start_5d
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_60
    .catch Ljava/io/IOException; {:try_start_5d .. :try_end_60} :catch_63

    .line 290
    :cond_60
    :goto_60
    throw v0

    .line 289
    :catch_61
    move-exception v1

    goto :goto_38

    :catch_63
    move-exception v1

    goto :goto_60

    .line 286
    :catchall_65
    move-exception v0

    goto :goto_5b

    .line 282
    :catch_67
    move-exception v0

    goto :goto_3b
.end method

.method static synthetic a(Lcom/google/android/location/c/s;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Lcom/google/android/location/c/D$a;
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 130
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 132
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_27

    .line 134
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v1

    .line 136
    :goto_12
    if-nez v1, :cond_26

    .line 137
    const-string v1, "Failed to create dir: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 139
    new-instance v1, Lcom/google/android/location/c/D$a;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v4, v0, v2}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 141
    :cond_26
    return-object v0

    :cond_27
    move v1, v2

    goto :goto_12
.end method

.method private b()Z
    .registers 3

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/google/android/location/c/s;->e:Z

    if-eqz v0, :cond_6

    .line 152
    const/4 v0, 0x1

    .line 163
    :cond_5
    :goto_5
    return v0

    .line 154
    :cond_6
    invoke-direct {p0}, Lcom/google/android/location/c/s;->c()Lcom/google/android/location/c/D$a;

    move-result-object v0

    .line 155
    if-eqz v0, :cond_e

    .line 156
    const/4 v0, 0x0

    goto :goto_5

    .line 158
    :cond_e
    invoke-static {}, Lcom/google/android/location/c/C;->a()Lcom/google/android/location/c/C;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/C;->a(Ljava/lang/String;)Z

    move-result v0

    .line 159
    iput-boolean v0, p0, Lcom/google/android/location/c/s;->e:Z

    .line 160
    if-nez v0, :cond_5

    goto :goto_5
.end method

.method static synthetic b(Lcom/google/android/location/c/s;)Z
    .registers 2
    .parameter

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/android/location/c/s;->j:Z

    return v0
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x6

    .line 167
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2d

    move v0, v1

    :goto_15
    const-string v3, "No sequence number specified!"

    invoke-static {v0, v3}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    .line 171
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 174
    :try_start_22
    iget-object v3, p0, Lcom/google/android/location/c/s;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v4, Lcom/google/android/location/c/s$2;

    invoke-direct {v4, p0, v0, p1}, Lcom/google/android/location/c/s$2;-><init>(Lcom/google/android/location/c/s;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V
    :try_end_2c
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_22 .. :try_end_2c} :catch_2f

    .line 208
    :goto_2c
    return v1

    :cond_2d
    move v0, v2

    .line 167
    goto :goto_15

    .line 204
    :catch_2f
    move-exception v1

    .line 205
    const-string v1, "Failed to write to file: work queue full."

    .line 207
    iget-object v3, p0, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4, v1}, Lcom/google/android/location/c/l;->a(ILjava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 208
    goto :goto_2c
.end method

.method static synthetic c(Lcom/google/android/location/c/s;)Landroid/os/PowerManager;
    .registers 2
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/location/c/s;->g:Landroid/os/PowerManager;

    return-object v0
.end method

.method private c()Lcom/google/android/location/c/D$a;
    .registers 10

    .prologue
    const/4 v0, 0x0

    .line 300
    iget-object v2, p0, Lcom/google/android/location/c/s;->l:Ljava/lang/Object;

    monitor-enter v2

    .line 301
    :try_start_4
    iget-boolean v1, p0, Lcom/google/android/location/c/s;->j:Z

    if-eqz v1, :cond_a

    .line 302
    monitor-exit v2

    .line 319
    :goto_9
    return-object v0

    .line 305
    :cond_a
    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/location/c/s;->b(Ljava/lang/String;)Lcom/google/android/location/c/D$a;

    move-result-object v1

    .line 306
    if-eqz v1, :cond_15

    .line 307
    monitor-exit v2

    move-object v0, v1

    goto :goto_9

    .line 311
    :cond_15
    const-string v1, "%d-%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v5

    const-wide v7, 0x412e848000000000L

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 313
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    .line 314
    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/location/c/s;->b(Ljava/lang/String;)Lcom/google/android/location/c/D$a;

    move-result-object v1

    .line 315
    if-eqz v1, :cond_5b

    .line 316
    monitor-exit v2

    move-object v0, v1

    goto :goto_9

    .line 318
    :cond_5b
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/c/s;->j:Z

    .line 319
    monitor-exit v2

    goto :goto_9

    .line 321
    :catchall_60
    move-exception v0

    monitor-exit v2
    :try_end_62
    .catchall {:try_start_4 .. :try_end_62} :catchall_60

    throw v0
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/c/D$a;
    .registers 7
    .parameter

    .prologue
    .line 250
    invoke-direct {p0}, Lcom/google/android/location/c/s;->c()Lcom/google/android/location/c/D$a;

    .line 251
    monitor-enter p0

    .line 252
    :try_start_4
    iget-object v0, p0, Lcom/google/android/location/c/s;->m:Lcom/google/android/location/c/c;

    if-nez v0, :cond_34

    .line 253
    iget-object v0, p0, Lcom/google/android/location/c/s;->i:[B

    if-nez v0, :cond_21

    .line 254
    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x0

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    const-string v3, "Encryption Key invalid."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    monitor-exit p0

    move-object v0, v1

    .line 264
    :goto_20
    return-object v0

    .line 259
    :cond_21
    new-instance v0, Lcom/google/android/location/c/c;

    iget-object v1, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/location/c/s;->i:[B

    iget-object v3, p0, Lcom/google/android/location/c/s;->b:Lcom/google/android/location/k/a/c;

    invoke-static {v2, v3}, Lcom/google/android/location/c/a;->b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/c/s;->b:Lcom/google/android/location/k/a/c;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/c/c;-><init>(Ljava/lang/String;Lcom/google/android/location/c/a;Lcom/google/android/location/k/a/c;)V

    iput-object v0, p0, Lcom/google/android/location/c/s;->m:Lcom/google/android/location/c/c;

    .line 262
    :cond_34
    monitor-exit p0
    :try_end_35
    .catchall {:try_start_4 .. :try_end_35} :catchall_3c

    .line 264
    iget-object v0, p0, Lcom/google/android/location/c/s;->m:Lcom/google/android/location/c/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/c/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/c/D$a;

    move-result-object v0

    goto :goto_20

    .line 262
    :catchall_3c
    move-exception v0

    :try_start_3d
    monitor-exit p0
    :try_end_3e
    .catchall {:try_start_3d .. :try_end_3e} :catchall_3c

    throw v0
.end method

.method static synthetic d(Lcom/google/android/location/c/s;)Z
    .registers 2
    .parameter

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/location/c/s;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)Lcom/google/android/location/c/D$a;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 103
    monitor-enter p0

    :try_start_2
    iget-object v1, p0, Lcom/google/android/location/c/s;->k:Ljava/lang/String;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/google/android/location/c/s;->k:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_24

    :cond_e
    :goto_e
    const-string v1, "sessionId in two writes should be consistent."

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/location/c/s;->k:Ljava/lang/String;

    if-eqz v0, :cond_26

    .line 106
    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x1

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_21
    .catchall {:try_start_2 .. :try_end_21} :catchall_44

    move-object v0, v1

    .line 113
    :cond_22
    :goto_22
    monitor-exit p0

    return-object v0

    .line 103
    :cond_24
    const/4 v0, 0x0

    goto :goto_e

    .line 108
    :cond_26
    :try_start_26
    const-string v0, "sessionId"

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/c/s;->a(Ljava/lang/String;[B)Lcom/google/android/location/c/D$a;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/google/android/location/c/D$a;->a()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 110
    iput-object p1, p0, Lcom/google/android/location/c/s;->k:Ljava/lang/String;

    .line 111
    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x1

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_42
    .catchall {:try_start_26 .. :try_end_42} :catchall_44

    move-object v0, v1

    goto :goto_22

    .line 103
    :catchall_44
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a()V
    .registers 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/location/c/s;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    .line 219
    return-void
.end method

.method a(Lcom/google/android/location/c/H;)V
    .registers 3
    .parameter

    .prologue
    .line 340
    if-nez p1, :cond_3

    .line 348
    :goto_2
    return-void

    .line 344
    :cond_3
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/c/s;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/location/c/H;->b(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_8} :catch_9

    goto :goto_2

    .line 345
    :catch_9
    move-exception v0

    goto :goto_2
.end method

.method declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 3
    .parameter

    .prologue
    .line 227
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/s;->c:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_d

    if-eqz v0, :cond_8

    .line 229
    const/4 v0, 0x0

    .line 231
    :goto_6
    monitor-exit p0

    return v0

    :cond_8
    :try_start_8
    invoke-direct {p0, p1}, Lcom/google/android/location/c/s;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    :try_end_b
    .catchall {:try_start_8 .. :try_end_b} :catchall_d

    move-result v0

    goto :goto_6

    .line 227
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 241
    const/4 v0, 0x6

    invoke-virtual {p1, v0, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 242
    invoke-direct {p0, p1}, Lcom/google/android/location/c/s;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method protected finalize()V
    .registers 2

    .prologue
    .line 327
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/s;->f:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->isShutdown()Z
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_c

    move-result v0

    if-nez v0, :cond_8

    .line 331
    :cond_8
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 333
    return-void

    .line 331
    :catchall_c
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
