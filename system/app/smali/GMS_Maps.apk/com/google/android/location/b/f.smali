.class public Lcom/google/android/location/b/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/location/e/o;

.field public static final f:Lcom/google/android/location/e/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/e/v",
            "<",
            "Lcom/google/android/location/b/a",
            "<",
            "Lcom/google/android/location/e/w;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final g:Lcom/google/android/location/e/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/e/v",
            "<",
            "Lcom/google/android/location/b/a",
            "<",
            "Lcom/google/android/location/e/C;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public b:Lcom/google/android/location/e/o;

.field public final c:Lcom/google/android/location/os/h;

.field public final d:Lcom/google/android/location/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/w;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/google/android/location/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;"
        }
    .end annotation
.end field

.field private h:J

.field private i:Z

.field private j:Lcom/google/android/location/c/a;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    .line 104
    new-instance v0, Lcom/google/android/location/e/o;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/location/e/o$a;->b:Lcom/google/android/location/e/o$a;

    const-wide/16 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/e/o;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;J)V

    sput-object v0, Lcom/google/android/location/b/f;->a:Lcom/google/android/location/e/o;

    .line 477
    new-instance v0, Lcom/google/android/location/b/f$1;

    invoke-direct {v0}, Lcom/google/android/location/b/f$1;-><init>()V

    sput-object v0, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/e/v;

    .line 491
    new-instance v0, Lcom/google/android/location/b/f$2;

    invoke-direct {v0}, Lcom/google/android/location/b/f$2;-><init>()V

    sput-object v0, Lcom/google/android/location/b/f;->g:Lcom/google/android/location/e/v;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/h;J)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    sget-object v0, Lcom/google/android/location/b/f;->a:Lcom/google/android/location/e/o;

    iput-object v0, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    .line 126
    iput-object p1, p0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    .line 127
    new-instance v0, Lcom/google/android/location/b/i;

    const/16 v1, 0x32

    sget-object v2, Lcom/google/android/location/e/y;->a:Lcom/google/android/location/e/v;

    sget-object v3, Lcom/google/android/location/b/f;->f:Lcom/google/android/location/e/v;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/b/i;-><init>(ILcom/google/android/location/e/v;Lcom/google/android/location/e/v;)V

    iput-object v0, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    .line 129
    new-instance v0, Lcom/google/android/location/b/i;

    const/16 v1, 0x3e8

    sget-object v2, Lcom/google/android/location/e/y;->b:Lcom/google/android/location/e/v;

    sget-object v3, Lcom/google/android/location/b/f;->g:Lcom/google/android/location/e/v;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/b/i;-><init>(ILcom/google/android/location/e/v;Lcom/google/android/location/e/v;)V

    iput-object v0, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    .line 131
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/b/f;->b(J)V

    .line 132
    return-void
.end method

.method public static a(JJJJ)J
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 252
    add-long v0, p2, p0

    sub-long/2addr v0, p4

    invoke-static {p6, p7, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(ILjava/io/DataInputStream;Ljavax/crypto/SecretKey;[B)Ljava/io/DataInput;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 259
    const/16 v0, 0xa

    if-ne p1, v0, :cond_e

    .line 263
    new-instance v0, Ljava/io/DataInputStream;

    invoke-static {p2, p3}, Lcom/google/android/location/os/b;->a(Ljava/io/InputStream;Ljavax/crypto/SecretKey;)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 271
    :goto_d
    return-object v0

    .line 264
    :cond_e
    const/16 v0, 0xb

    if-ne p1, v0, :cond_33

    .line 267
    iget-object v0, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/c/a;

    if-nez v0, :cond_1d

    .line 268
    const/4 v0, 0x0

    invoke-static {p4, v0}, Lcom/google/android/location/c/a;->b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/c/a;

    .line 270
    :cond_1d
    iget-object v0, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/c/a;

    invoke-virtual {v0, p2}, Lcom/google/android/location/c/a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/e/u;

    move-result-object v0

    .line 271
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, [B

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v0, v1

    goto :goto_d

    .line 273
    :cond_33
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incompatible version."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(J)V
    .registers 9
    .parameter

    .prologue
    const-wide/high16 v4, 0x404e

    .line 278
    sget-object v0, Lcom/google/android/location/b/f;->a:Lcom/google/android/location/e/o;

    iput-object v0, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    .line 281
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide/high16 v2, 0x4038

    mul-double/2addr v0, v2

    mul-double/2addr v0, v4

    mul-double/2addr v0, v4

    const-wide v2, 0x408f400000000000L

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    .line 282
    sub-long v0, p1, v0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/location/b/f;->a(JZ)V

    .line 283
    iget-object v0, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    invoke-virtual {v0}, Lcom/google/android/location/b/i;->b()V

    .line 284
    iget-object v0, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v0}, Lcom/google/android/location/b/i;->b()V

    .line 285
    return-void
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 448
    iget-wide v0, p0, Lcom/google/android/location/b/f;->h:J

    return-wide v0
.end method

.method public a(J)V
    .registers 8
    .parameter

    .prologue
    .line 140
    const-wide/32 v0, 0x240c8400

    sub-long v0, p1, v0

    .line 141
    iget-object v2, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    iget-object v3, p0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-virtual {v3}, Lcom/google/android/location/os/h;->p()J

    move-result-wide v3

    sub-long v3, p1, v3

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/google/android/location/b/i;->a(JJ)V

    .line 143
    iget-object v2, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    iget-object v3, p0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-virtual {v3}, Lcom/google/android/location/os/h;->o()J

    move-result-wide v3

    sub-long v3, p1, v3

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/google/android/location/b/i;->a(JJ)V

    .line 145
    return-void
.end method

.method public a(JZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 465
    iput-wide p1, p0, Lcom/google/android/location/b/f;->h:J

    .line 466
    iput-boolean p3, p0, Lcom/google/android/location/b/f;->i:Z

    .line 467
    return-void
.end method

.method public a(Lcom/google/android/location/os/i;)V
    .registers 10
    .parameter

    .prologue
    .line 155
    new-instance v0, Ljava/io/File;

    invoke-interface {p1}, Lcom/google/android/location/os/i;->d()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_state"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 156
    invoke-interface {p1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v4

    .line 157
    invoke-interface {p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v6

    .line 159
    :try_start_13
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 160
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 161
    invoke-interface {p1}, Lcom/google/android/location/os/i;->j()Ljavax/crypto/SecretKey;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/location/os/i;->l()[B

    move-result-object v3

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/b/f;->a(Ljava/io/InputStream;Ljavax/crypto/SecretKey;[BJJ)V

    .line 162
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2c
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_2c} :catch_2d
    .catch Ljava/lang/SecurityException; {:try_start_13 .. :try_end_2c} :catch_32
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_2c} :catch_37

    .line 175
    :goto_2c
    return-void

    .line 163
    :catch_2d
    move-exception v0

    .line 164
    invoke-direct {p0, v6, v7}, Lcom/google/android/location/b/f;->b(J)V

    goto :goto_2c

    .line 168
    :catch_32
    move-exception v0

    .line 169
    invoke-direct {p0, v6, v7}, Lcom/google/android/location/b/f;->b(J)V

    goto :goto_2c

    .line 171
    :catch_37
    move-exception v0

    .line 172
    invoke-direct {p0, v6, v7}, Lcom/google/android/location/b/f;->b(J)V

    goto :goto_2c
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZJ)V
    .registers 22
    .parameter
    .parameter
    .parameter

    .prologue
    .line 360
    invoke-static/range {p1 .. p1}, Lcom/google/android/location/k/c;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 441
    :cond_6
    return-void

    .line 363
    :cond_7
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v14

    .line 364
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-virtual {v1}, Lcom/google/android/location/os/h;->d()I

    move-result v4

    .line 365
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-virtual {v1}, Lcom/google/android/location/os/h;->e()I

    move-result v15

    .line 366
    const/4 v1, 0x0

    move v13, v1

    :goto_20
    const/4 v1, 0x3

    invoke-virtual {v14, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-ge v13, v1, :cond_6

    .line 367
    const/4 v1, 0x3

    invoke-virtual {v14, v1, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v16

    .line 368
    const/4 v5, 0x0

    .line 369
    const/4 v3, 0x0

    .line 370
    const/4 v2, -0x1

    .line 371
    const/4 v1, -0x1

    .line 372
    const/4 v6, -0x1

    .line 373
    const v10, 0x9c40

    .line 374
    const/4 v7, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_134

    .line 375
    const/4 v7, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    .line 376
    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_79

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_79

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_79

    .line 378
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    .line 379
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    .line 380
    const/4 v1, 0x3

    invoke-virtual {v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    mul-int/lit16 v2, v1, 0x3e8

    .line 381
    const/4 v1, 0x4

    invoke-virtual {v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 383
    :cond_79
    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_87

    .line 384
    const/16 v6, 0x8

    invoke-virtual {v7, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    .line 386
    :cond_87
    const/16 v8, 0x14

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_134

    .line 387
    const/16 v8, 0x14

    invoke-virtual {v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v7

    mul-int/lit16 v10, v7, 0x3e8

    move v9, v1

    move v8, v2

    move v11, v3

    move v12, v5

    .line 392
    :goto_9b
    const/4 v1, 0x2

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_c2

    const/4 v1, 0x3

    if-eq v6, v1, :cond_c2

    .line 394
    const/4 v1, 0x2

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 395
    invoke-static {v1}, Lcom/google/android/location/e/e;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v3

    .line 396
    new-instance v5, Lcom/google/android/location/e/w;

    invoke-direct {v5, v12, v11, v8, v9}, Lcom/google/android/location/e/w;-><init>(IIII)V

    .line 397
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    move/from16 v2, p2

    move-wide/from16 v6, p3

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/location/b/i;->a(ZLjava/lang/Object;ILjava/lang/Object;J)V

    .line 402
    :cond_c2
    const/4 v1, 0x3

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_11f

    .line 403
    const/4 v1, 0x3

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 406
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_124

    .line 407
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    move-wide v2, v1

    .line 415
    :goto_e1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/location/b/i;->a(Ljava/lang/Object;)Lcom/google/android/location/b/a;

    move-result-object v6

    .line 421
    if-nez v6, :cond_12f

    if-eqz p2, :cond_12f

    const/4 v1, 0x1

    move v5, v1

    .line 431
    :goto_f3
    if-eqz v6, :cond_132

    if-eqz p2, :cond_103

    invoke-virtual {v6}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/C;

    invoke-virtual {v1}, Lcom/google/android/location/e/C;->e()Z

    move-result v1

    if-nez v1, :cond_132

    :cond_103
    const/4 v1, 0x1

    .line 434
    :goto_104
    if-nez v5, :cond_108

    if-eqz v1, :cond_11f

    .line 435
    :cond_108
    new-instance v5, Lcom/google/android/location/e/C;

    move v6, v12

    move v7, v11

    invoke-direct/range {v5 .. v10}, Lcom/google/android/location/e/C;-><init>(IIIII)V

    .line 437
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    const/4 v7, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move v9, v15

    move-object v10, v5

    move-wide/from16 v11, p3

    invoke-virtual/range {v6 .. v12}, Lcom/google/android/location/b/i;->a(ZLjava/lang/Object;ILjava/lang/Object;J)V

    .line 366
    :cond_11f
    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto/16 :goto_20

    .line 409
    :cond_124
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/e/F;->a(Ljava/lang/String;)J

    move-result-wide v1

    move-wide v2, v1

    goto :goto_e1

    .line 421
    :cond_12f
    const/4 v1, 0x0

    move v5, v1

    goto :goto_f3

    .line 431
    :cond_132
    const/4 v1, 0x0

    goto :goto_104

    :cond_134
    move v9, v1

    move v8, v2

    move v11, v3

    move v12, v5

    goto/16 :goto_9b
.end method

.method a(Ljava/io/InputStream;Ljavax/crypto/SecretKey;[BJJ)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 197
    :try_start_0
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 198
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v3

    .line 201
    const/16 v4, 0xb

    if-eq v3, v4, :cond_11

    const/16 v4, 0xa

    if-ne v3, v4, :cond_60

    .line 203
    :cond_11
    invoke-direct {p0, v3, v2, p2, p3}, Lcom/google/android/location/b/f;->a(ILjava/io/DataInputStream;Ljavax/crypto/SecretKey;[B)Ljava/io/DataInput;

    move-result-object v10

    .line 212
    invoke-interface {v10}, Ljava/io/DataInput;->readLong()J

    move-result-wide v2

    .line 213
    invoke-interface {v10}, Ljava/io/DataInput;->readLong()J

    move-result-wide v4

    move-wide v6, p4

    move-wide/from16 v8, p6

    .line 214
    invoke-static/range {v2 .. v9}, Lcom/google/android/location/b/f;->a(JJJJ)J

    move-result-wide v4

    invoke-interface {v10}, Ljava/io/DataInput;->readBoolean()Z

    move-result v6

    invoke-virtual {p0, v4, v5, v6}, Lcom/google/android/location/b/f;->a(JZ)V

    .line 217
    invoke-interface {v10}, Ljava/io/DataInput;->readBoolean()Z

    move-result v4

    .line 218
    if-eqz v4, :cond_55

    .line 219
    sget-object v4, Lcom/google/android/location/e/o;->f:Lcom/google/android/location/e/v;

    invoke-interface {v4, v10}, Lcom/google/android/location/e/v;->b(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/e/o;

    iput-object v4, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    .line 225
    iget-object v4, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    iget-wide v4, v4, Lcom/google/android/location/e/o;->e:J

    move-wide v6, p4

    move-wide/from16 v8, p6

    invoke-static/range {v2 .. v9}, Lcom/google/android/location/b/f;->a(JJJJ)J

    move-result-wide v2

    .line 227
    new-instance v4, Lcom/google/android/location/e/o;

    iget-object v5, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    iget-object v5, v5, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    iget-object v6, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    iget-object v6, v6, Lcom/google/android/location/e/o;->d:Lcom/google/android/location/e/o$a;

    invoke-direct {v4, v5, v6, v2, v3}, Lcom/google/android/location/e/o;-><init>(Lcom/google/android/location/e/w;Lcom/google/android/location/e/o$a;J)V

    iput-object v4, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    .line 229
    :cond_55
    iget-object v2, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    invoke-virtual {v2, v10}, Lcom/google/android/location/b/i;->a(Ljava/io/DataInput;)Lcom/google/android/location/b/i;

    .line 230
    iget-object v2, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v2, v10}, Lcom/google/android/location/b/i;->a(Ljava/io/DataInput;)Lcom/google/android/location/b/i;

    .line 237
    :goto_5f
    return-void

    .line 209
    :cond_60
    move-wide/from16 v0, p6

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/b/f;->b(J)V
    :try_end_65
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_65} :catch_66

    goto :goto_5f

    .line 231
    :catch_66
    move-exception v2

    .line 232
    move-wide/from16 v0, p6

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/b/f;->b(J)V

    .line 233
    throw v2
.end method

.method a(Ljava/io/OutputStream;[BJ)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 320
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 321
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 322
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    .line 324
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 325
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 326
    invoke-virtual {v2, p3, p4}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 327
    iget-wide v3, p0, Lcom/google/android/location/b/f;->h:J

    invoke-virtual {v2, v3, v4}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 328
    iget-boolean v3, p0, Lcom/google/android/location/b/f;->i:Z

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 329
    iget-object v3, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    sget-object v4, Lcom/google/android/location/b/f;->a:Lcom/google/android/location/e/o;

    if-eq v3, v4, :cond_61

    .line 330
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 331
    sget-object v3, Lcom/google/android/location/e/o;->f:Lcom/google/android/location/e/v;

    iget-object v4, p0, Lcom/google/android/location/b/f;->b:Lcom/google/android/location/e/o;

    invoke-interface {v3, v4, v2}, Lcom/google/android/location/e/v;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    .line 335
    :goto_35
    iget-object v3, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    iget-object v4, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    invoke-virtual {v3, v4, v2}, Lcom/google/android/location/b/i;->a(Lcom/google/android/location/b/i;Ljava/io/DataOutput;)V

    .line 336
    iget-object v3, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    iget-object v4, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v3, v4, v2}, Lcom/google/android/location/b/i;->a(Lcom/google/android/location/b/i;Ljava/io/DataOutput;)V

    .line 338
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    .line 339
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 340
    iget-object v2, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/c/a;

    if-nez v2, :cond_54

    .line 341
    const/4 v2, 0x0

    invoke-static {p2, v2}, Lcom/google/android/location/c/a;->b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/c/a;

    .line 343
    :cond_54
    iget-object v2, p0, Lcom/google/android/location/b/f;->j:Lcom/google/android/location/c/a;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/location/c/a;->a(Ljava/io/DataOutputStream;[B)V

    .line 344
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 345
    return-void

    .line 333
    :cond_61
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    goto :goto_35
.end method

.method public b(Lcom/google/android/location/os/i;)V
    .registers 6
    .parameter

    .prologue
    .line 293
    invoke-interface {p1}, Lcom/google/android/location/os/i;->d()Ljava/io/File;

    move-result-object v0

    .line 294
    if-nez v0, :cond_7

    .line 310
    :goto_6
    return-void

    .line 297
    :cond_7
    new-instance v1, Ljava/io/File;

    const-string v2, "nlp_state"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 299
    :try_start_e
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 300
    invoke-interface {p1, v1}, Lcom/google/android/location/os/i;->a(Ljava/io/File;)V

    .line 301
    new-instance v1, Ljava/io/BufferedOutputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 302
    invoke-interface {p1}, Lcom/google/android/location/os/i;->l()[B

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/location/b/f;->a(Ljava/io/OutputStream;[BJ)V
    :try_end_26
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_26} :catch_27
    .catch Ljava/lang/SecurityException; {:try_start_e .. :try_end_26} :catch_2b
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_26} :catch_29

    goto :goto_6

    .line 303
    :catch_27
    move-exception v0

    goto :goto_6

    .line 307
    :catch_29
    move-exception v0

    goto :goto_6

    .line 305
    :catch_2b
    move-exception v0

    goto :goto_6
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 455
    iget-boolean v0, p0, Lcom/google/android/location/b/f;->i:Z

    return v0
.end method

.method public c()Lcom/google/android/location/b/i;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;"
        }
    .end annotation

    .prologue
    .line 470
    iget-object v0, p0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    return-object v0
.end method

.method public d()Lcom/google/android/location/b/i;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/w;",
            ">;"
        }
    .end annotation

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    return-object v0
.end method
