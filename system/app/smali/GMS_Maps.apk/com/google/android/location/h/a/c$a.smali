.class public Lcom/google/android/location/h/a/c$a;
.super Lcom/google/android/location/h/a/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/a/b;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/h/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "a"
.end annotation


# instance fields
.field private a:J

.field private b:Lcom/google/android/location/h/a/c;

.field private c:Lcom/google/googlenav/common/io/GoogleHttpConnection;

.field private d:Ljava/io/InputStream;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Ljava/io/DataInputStream;

.field private k:Ljava/lang/String;

.field private l:J

.field private m:I

.field private n:Lcom/google/android/location/h/g;

.field private o:[B

.field private p:I

.field private q:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/h/a/c;Ljava/lang/String;I)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 279
    invoke-direct {p0}, Lcom/google/android/location/h/a/a;-><init>()V

    .line 237
    const-wide/16 v0, 0x4e20

    iput-wide v0, p0, Lcom/google/android/location/h/a/c$a;->a:J

    .line 244
    const-string v0, "GET"

    iput-object v0, p0, Lcom/google/android/location/h/a/c$a;->e:Ljava/lang/String;

    .line 282
    iput-object p1, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    .line 283
    iput-object p2, p0, Lcom/google/android/location/h/a/c$a;->g:Ljava/lang/String;

    .line 284
    iput p3, p0, Lcom/google/android/location/h/a/c$a;->p:I

    .line 285
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/a/c$a;->h:Ljava/util/Vector;

    .line 286
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/h/a/c$a;->i:Z

    .line 287
    return-void
.end method

.method private static a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V
    .registers 1
    .parameter

    .prologue
    .line 488
    return-void
.end method

.method private a(Ljava/io/OutputStream;)V
    .registers 3
    .parameter

    .prologue
    .line 557
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->o:[B

    if-eqz v0, :cond_a

    .line 558
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->o:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 571
    :cond_9
    :goto_9
    return-void

    .line 562
    :cond_a
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->n:Lcom/google/android/location/h/g;

    if-eqz v0, :cond_18

    .line 563
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->n:Lcom/google/android/location/h/g;

    invoke-interface {v0}, Lcom/google/android/location/h/g;->c_()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    goto :goto_9

    .line 567
    :cond_18
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;

    if-eqz v0, :cond_9

    .line 568
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    goto :goto_9
.end method

.method private b(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 620
    move v1, v2

    :goto_2
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1e

    .line 621
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->h:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 622
    aget-object v3, v0, v2

    const/4 v4, 0x1

    aget-object v0, v0, v4

    invoke-interface {p1, v3, v0}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 624
    :cond_1e
    return-void
.end method

.method private p()Z
    .registers 2

    .prologue
    .line 536
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->o:[B

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->n:Lcom/google/android/location/h/g;

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private q()I
    .registers 2

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;

    if-eqz v0, :cond_f

    .line 541
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/a/c$a;->o:[B

    .line 542
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;

    .line 545
    :cond_f
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->o:[B

    if-eqz v0, :cond_17

    .line 546
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->o:[B

    array-length v0, v0

    .line 553
    :goto_16
    return v0

    .line 549
    :cond_17
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->n:Lcom/google/android/location/h/g;

    if-eqz v0, :cond_22

    .line 550
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->n:Lcom/google/android/location/h/g;

    invoke-interface {v0}, Lcom/google/android/location/h/g;->b_()I

    move-result v0

    goto :goto_16

    .line 553
    :cond_22
    const/4 v0, 0x0

    goto :goto_16
.end method


# virtual methods
.method public declared-synchronized a(J)V
    .registers 4
    .parameter

    .prologue
    .line 575
    monitor-enter p0

    :try_start_1
    iput-wide p1, p0, Lcom/google/android/location/h/a/c$a;->a:J
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 576
    monitor-exit p0

    return-void

    .line 575
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/io/InputStream;)V
    .registers 3
    .parameter

    .prologue
    .line 518
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/location/h/a/c$a;->d:Ljava/io/InputStream;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 519
    monitor-exit p0

    return-void

    .line 518
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 506
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/location/h/a/c$a;->e:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 507
    monitor-exit p0

    return-void

    .line 506
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .registers 2

    .prologue
    .line 291
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->g()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 292
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->j:Ljava/io/DataInputStream;

    invoke-static {v0}, Lcom/google/googlenav/common/io/i;->b(Ljava/io/InputStream;)V

    .line 293
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->c:Lcom/google/googlenav/common/io/GoogleHttpConnection;

    invoke-static {v0}, Lcom/google/googlenav/common/io/i;->a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    .line 294
    invoke-super {p0}, Lcom/google/android/location/h/a/a;->b()V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_16

    .line 296
    :cond_14
    monitor-exit p0

    return-void

    .line 291
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Las/a;)V
    .registers 3
    .parameter

    .prologue
    .line 495
    invoke-virtual {p0, p1}, Lcom/google/android/location/h/a/c$a;->a(Las/a;)V

    .line 496
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    invoke-static {v0, p0}, Lcom/google/android/location/h/a/c;->a(Lcom/google/android/location/h/a/c;Lcom/google/android/location/h/a/c$a;)Z

    .line 497
    return-void
.end method

.method public declared-synchronized b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 511
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/location/h/a/c$a;->f:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 512
    monitor-exit p0

    return-void

    .line 511
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d_()I
    .registers 2

    .prologue
    .line 305
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->a()V

    .line 306
    iget v0, p0, Lcom/google/android/location/h/a/c$a;->m:I
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    monitor-exit p0

    return v0

    .line 305
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()J
    .registers 3

    .prologue
    .line 311
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->a()V

    .line 312
    iget-wide v0, p0, Lcom/google/android/location/h/a/c$a;->l:J
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    monitor-exit p0

    return-wide v0

    .line 311
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized h()Ljava/lang/String;
    .registers 2

    .prologue
    .line 317
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->a()V

    .line 318
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->k:Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    monitor-exit p0

    return-object v0

    .line 317
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected i()V
    .registers 3

    .prologue
    .line 479
    iget v0, p0, Lcom/google/android/location/h/a/c$a;->p:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_17

    .line 480
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/h/a/c$a;->q:J

    .line 481
    invoke-super {p0}, Lcom/google/android/location/h/a/a;->i()V

    .line 483
    :cond_17
    return-void
.end method

.method public declared-synchronized j()Ljava/io/DataInputStream;
    .registers 2

    .prologue
    .line 323
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->a()V

    .line 324
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->j:Ljava/io/DataInputStream;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    monitor-exit p0

    return-object v0

    .line 323
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public k()J
    .registers 3

    .prologue
    .line 300
    iget-wide v0, p0, Lcom/google/android/location/h/a/c$a;->q:J

    return-wide v0
.end method

.method public declared-synchronized n()J
    .registers 3

    .prologue
    .line 501
    monitor-enter p0

    :try_start_1
    iget-wide v0, p0, Lcom/google/android/location/h/a/c$a;->a:J
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-wide v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized o()V
    .registers 3

    .prologue
    .line 580
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->e_()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_19

    .line 582
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->c:Lcom/google/googlenav/common/io/GoogleHttpConnection;

    if-eqz v0, :cond_11

    .line 583
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->c:Lcom/google/googlenav/common/io/GoogleHttpConnection;

    invoke-interface {v0}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->notifyTimeout()V

    .line 585
    :cond_11
    new-instance v0, Lcom/google/android/location/h/a/e;

    invoke-direct {v0}, Lcom/google/android/location/h/a/e;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/location/h/a/c$a;->a(Ljava/lang/Exception;)V
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_1b

    .line 587
    :cond_19
    monitor-exit p0

    return-void

    .line 580
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .registers 12

    .prologue
    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 337
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    invoke-static {v0}, Lcom/google/android/location/h/a/c;->a(Lcom/google/android/location/h/a/c;)Las/c;

    move-result-object v0

    .line 338
    new-instance v4, Lcom/google/android/location/h/a/c$b;

    invoke-direct {v4, v0}, Lcom/google/android/location/h/a/c$b;-><init>(Las/c;)V

    .line 339
    invoke-virtual {v4, p0}, Lcom/google/android/location/h/a/c$b;->a(Lcom/google/android/location/h/a/c$a;)V

    .line 340
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->n()J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, Lcom/google/android/location/h/a/c$b;->a(J)V

    .line 341
    invoke-virtual {v4}, Lcom/google/android/location/h/a/c$b;->g()V

    .line 370
    :try_start_1a
    monitor-enter p0
    :try_end_1b
    .catchall {:try_start_1a .. :try_end_1b} :catchall_136
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1b} :catch_4a

    .line 371
    :try_start_1b
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->f_()V

    .line 373
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->i()V

    .line 374
    monitor-exit p0
    :try_end_22
    .catchall {:try_start_1b .. :try_end_22} :catchall_47

    .line 376
    :try_start_22
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->e:Ljava/lang/String;

    const-string v2, "POST"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 377
    iget-object v2, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    iget-object v3, p0, Lcom/google/android/location/h/a/c$a;->g:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lcom/google/android/location/h/a/c;->a(Lcom/google/android/location/h/a/c;Ljava/lang/String;Z)Lcom/google/googlenav/common/io/GoogleHttpConnection;
    :try_end_31
    .catchall {:try_start_22 .. :try_end_31} :catchall_136
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_31} :catch_4a

    move-result-object v3

    .line 379
    :try_start_32
    monitor-enter p0
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_fc
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_33} :catch_f0

    .line 380
    :try_start_33
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->f()Z

    move-result v0

    if-nez v0, :cond_78

    .line 381
    monitor-exit p0
    :try_end_3a
    .catchall {:try_start_33 .. :try_end_3a} :catchall_ed

    .line 468
    invoke-virtual {v4}, Lcom/google/android/location/h/a/c$b;->c()I

    .line 471
    invoke-static {v1}, Lcom/google/googlenav/common/io/i;->b(Ljava/io/InputStream;)V

    .line 472
    invoke-static {v1}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/OutputStream;)V

    .line 473
    invoke-static {v3}, Lcom/google/googlenav/common/io/i;->a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    .line 476
    :goto_46
    return-void

    .line 374
    :catchall_47
    move-exception v0

    :try_start_48
    monitor-exit p0
    :try_end_49
    .catchall {:try_start_48 .. :try_end_49} :catchall_47

    :try_start_49
    throw v0
    :try_end_4a
    .catchall {:try_start_49 .. :try_end_4a} :catchall_136
    .catch Ljava/lang/Exception; {:try_start_49 .. :try_end_4a} :catch_4a

    .line 454
    :catch_4a
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    .line 457
    :goto_4d
    :try_start_4d
    iget-object v5, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    invoke-static {v5}, Lcom/google/android/location/h/a/c;->b(Lcom/google/android/location/h/a/c;)Lcom/google/googlenav/common/io/g;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/googlenav/common/io/g;->c()V

    .line 458
    monitor-enter p0
    :try_end_57
    .catchall {:try_start_4d .. :try_end_57} :catchall_11d

    .line 459
    :try_start_57
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->e_()I

    move-result v5

    if-ne v5, v10, :cond_60

    .line 460
    invoke-virtual {p0, v0}, Lcom/google/android/location/h/a/c$a;->a(Ljava/lang/Exception;)V

    .line 462
    :cond_60
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->e_()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_6a

    .line 463
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 465
    :cond_6a
    monitor-exit p0
    :try_end_6b
    .catchall {:try_start_57 .. :try_end_6b} :catchall_133

    .line 468
    invoke-virtual {v4}, Lcom/google/android/location/h/a/c$b;->c()I

    .line 471
    invoke-static {v1}, Lcom/google/googlenav/common/io/i;->b(Ljava/io/InputStream;)V

    .line 472
    invoke-static {v2}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/OutputStream;)V

    .line 473
    invoke-static {v3}, Lcom/google/googlenav/common/io/i;->a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    goto :goto_46

    .line 385
    :cond_78
    :try_start_78
    iput-object v3, p0, Lcom/google/android/location/h/a/c$a;->c:Lcom/google/googlenav/common/io/GoogleHttpConnection;

    .line 387
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->i()V

    .line 388
    monitor-exit p0
    :try_end_7e
    .catchall {:try_start_78 .. :try_end_7e} :catchall_ed

    .line 390
    :try_start_7e
    iget-object v0, p0, Lcom/google/android/location/h/a/c$a;->f:Ljava/lang/String;

    if-eqz v0, :cond_f4

    .line 391
    const-string v0, "Content-Type"

    iget-object v2, p0, Lcom/google/android/location/h/a/c$a;->f:Ljava/lang/String;

    invoke-interface {v3, v0, v2}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :goto_89
    invoke-direct {p0, v3}, Lcom/google/android/location/h/a/c$a;->b(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    .line 403
    invoke-direct {p0}, Lcom/google/android/location/h/a/c$a;->p()Z

    move-result v0

    if-eqz v0, :cond_13a

    .line 404
    iget-boolean v0, p0, Lcom/google/android/location/h/a/c$a;->i:Z

    if-eqz v0, :cond_a3

    .line 405
    const-string v0, "Content-Length"

    invoke-direct {p0}, Lcom/google/android/location/h/a/c$a;->q()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v0, v2}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_a3
    invoke-interface {v3}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->a()Ljava/io/DataOutputStream;
    :try_end_a6
    .catchall {:try_start_7e .. :try_end_a6} :catchall_fc
    .catch Ljava/lang/Exception; {:try_start_7e .. :try_end_a6} :catch_f0

    move-result-object v2

    .line 411
    :goto_a7
    :try_start_a7
    invoke-direct {p0}, Lcom/google/android/location/h/a/c$a;->p()Z
    :try_end_aa
    .catchall {:try_start_a7 .. :try_end_aa} :catchall_11d
    .catch Ljava/lang/Exception; {:try_start_a7 .. :try_end_aa} :catch_110

    move-result v0

    if-eqz v0, :cond_b3

    .line 413
    :try_start_ad
    invoke-direct {p0, v2}, Lcom/google/android/location/h/a/c$a;->a(Ljava/io/OutputStream;)V
    :try_end_b0
    .catchall {:try_start_ad .. :try_end_b0} :catchall_10b

    .line 415
    :try_start_b0
    invoke-static {v2}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/OutputStream;)V

    .line 419
    :cond_b3
    invoke-interface {v3}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->c()I

    move-result v0

    .line 420
    invoke-interface {v3}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->e()J

    move-result-wide v5

    .line 421
    invoke-interface {v3}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->d()Ljava/lang/String;

    move-result-object v7

    .line 422
    invoke-interface {v3}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->b()Ljava/io/DataInputStream;

    move-result-object v1

    .line 424
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->i()V

    .line 426
    invoke-static {v3}, Lcom/google/android/location/h/a/c$a;->a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    .line 433
    const/16 v8, 0xc8

    if-ne v0, v8, :cond_113

    .line 434
    iget-object v8, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    invoke-static {v8}, Lcom/google/android/location/h/a/c;->b(Lcom/google/android/location/h/a/c;)Lcom/google/googlenav/common/io/g;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Lcom/google/googlenav/common/io/g;->a(Z)Z

    .line 439
    :goto_d7
    monitor-enter p0
    :try_end_d8
    .catchall {:try_start_b0 .. :try_end_d8} :catchall_11d
    .catch Ljava/lang/Exception; {:try_start_b0 .. :try_end_d8} :catch_110

    .line 440
    :try_start_d8
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->f()Z

    move-result v8

    if-nez v8, :cond_11f

    .line 441
    monitor-exit p0
    :try_end_df
    .catchall {:try_start_d8 .. :try_end_df} :catchall_130

    .line 468
    invoke-virtual {v4}, Lcom/google/android/location/h/a/c$b;->c()I

    .line 471
    invoke-static {v1}, Lcom/google/googlenav/common/io/i;->b(Ljava/io/InputStream;)V

    .line 472
    invoke-static {v2}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/OutputStream;)V

    .line 473
    invoke-static {v3}, Lcom/google/googlenav/common/io/i;->a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    goto/16 :goto_46

    .line 388
    :catchall_ed
    move-exception v0

    :try_start_ee
    monitor-exit p0
    :try_end_ef
    .catchall {:try_start_ee .. :try_end_ef} :catchall_ed

    :try_start_ef
    throw v0

    .line 454
    :catch_f0
    move-exception v0

    move-object v2, v1

    goto/16 :goto_4d

    .line 397
    :cond_f4
    const-string v0, "Content-Type"

    const-string v2, "application/binary"

    invoke-interface {v3, v0, v2}, Lcom/google/googlenav/common/io/GoogleHttpConnection;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_fb
    .catchall {:try_start_ef .. :try_end_fb} :catchall_fc
    .catch Ljava/lang/Exception; {:try_start_ef .. :try_end_fb} :catch_f0

    goto :goto_89

    .line 468
    :catchall_fc
    move-exception v0

    move-object v2, v1

    :goto_fe
    invoke-virtual {v4}, Lcom/google/android/location/h/a/c$b;->c()I

    .line 471
    invoke-static {v1}, Lcom/google/googlenav/common/io/i;->b(Ljava/io/InputStream;)V

    .line 472
    invoke-static {v2}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/OutputStream;)V

    .line 473
    invoke-static {v3}, Lcom/google/googlenav/common/io/i;->a(Lcom/google/googlenav/common/io/GoogleHttpConnection;)V

    throw v0

    .line 415
    :catchall_10b
    move-exception v0

    :try_start_10c
    invoke-static {v2}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/OutputStream;)V

    throw v0

    .line 454
    :catch_110
    move-exception v0

    goto/16 :goto_4d

    .line 436
    :cond_113
    iget-object v8, p0, Lcom/google/android/location/h/a/c$a;->b:Lcom/google/android/location/h/a/c;

    invoke-static {v8}, Lcom/google/android/location/h/a/c;->b(Lcom/google/android/location/h/a/c;)Lcom/google/googlenav/common/io/g;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/googlenav/common/io/g;->c()V
    :try_end_11c
    .catchall {:try_start_10c .. :try_end_11c} :catchall_11d
    .catch Ljava/lang/Exception; {:try_start_10c .. :try_end_11c} :catch_110

    goto :goto_d7

    .line 468
    :catchall_11d
    move-exception v0

    goto :goto_fe

    .line 443
    :cond_11f
    :try_start_11f
    iput v0, p0, Lcom/google/android/location/h/a/c$a;->m:I

    .line 444
    iput-wide v5, p0, Lcom/google/android/location/h/a/c$a;->l:J

    .line 445
    iput-object v7, p0, Lcom/google/android/location/h/a/c$a;->k:Ljava/lang/String;

    .line 446
    iput-object v1, p0, Lcom/google/android/location/h/a/c$a;->j:Ljava/io/DataInputStream;

    .line 447
    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->g_()V

    .line 448
    monitor-exit p0
    :try_end_12b
    .catchall {:try_start_11f .. :try_end_12b} :catchall_130

    .line 468
    invoke-virtual {v4}, Lcom/google/android/location/h/a/c$b;->c()I

    goto/16 :goto_46

    .line 448
    :catchall_130
    move-exception v0

    :try_start_131
    monitor-exit p0
    :try_end_132
    .catchall {:try_start_131 .. :try_end_132} :catchall_130

    :try_start_132
    throw v0
    :try_end_133
    .catchall {:try_start_132 .. :try_end_133} :catchall_11d
    .catch Ljava/lang/Exception; {:try_start_132 .. :try_end_133} :catch_110

    .line 465
    :catchall_133
    move-exception v0

    :try_start_134
    monitor-exit p0
    :try_end_135
    .catchall {:try_start_134 .. :try_end_135} :catchall_133

    :try_start_135
    throw v0
    :try_end_136
    .catchall {:try_start_135 .. :try_end_136} :catchall_11d

    .line 468
    :catchall_136
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    goto :goto_fe

    :cond_13a
    move-object v2, v1

    goto/16 :goto_a7
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 627
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/location/h/a/c$a;->e_()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
