.class public Lcom/google/android/location/h/b/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/16 v3, 0x9

    .line 24
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "application/atom+xml"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "application/binary"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "image/jpg"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "image/png"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "multipart/alternative"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "multipart/mixed"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "multipart/related"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "text/plain"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "text/xml"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/location/h/b/g;->a:[Ljava/lang/String;

    .line 36
    new-array v0, v3, [I

    fill-array-data v0, :array_3c

    sput-object v0, Lcom/google/android/location/h/b/g;->b:[I

    .line 51
    return-void

    .line 36
    :array_3c
    .array-data 0x4
        0x61t 0x61t 0x0t 0x0t
        0x62t 0x61t 0x0t 0x0t
        0x6at 0x69t 0x0t 0x0t
        0x70t 0x69t 0x0t 0x0t
        0x61t 0x6dt 0x0t 0x0t
        0x6dt 0x6dt 0x0t 0x0t
        0x72t 0x6dt 0x0t 0x0t
        0x70t 0x74t 0x0t 0x0t
        0x78t 0x74t 0x0t 0x0t
    .end array-data
.end method

.method public static a(Ljava/lang/String;)I
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 60
    move v0, v1

    :goto_2
    sget-object v2, Lcom/google/android/location/h/b/g;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_15

    .line 61
    sget-object v2, Lcom/google/android/location/h/b/g;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 62
    sget-object v1, Lcom/google/android/location/h/b/g;->b:[I

    aget v1, v1, v0

    .line 66
    :cond_15
    return v1

    .line 60
    :cond_16
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public static a(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 76
    const/4 v0, 0x0

    :goto_1
    sget-object v1, Lcom/google/android/location/h/b/g;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_14

    .line 77
    sget-object v1, Lcom/google/android/location/h/b/g;->b:[I

    aget v1, v1, v0

    if-ne v1, p0, :cond_11

    .line 78
    sget-object v1, Lcom/google/android/location/h/b/g;->a:[Ljava/lang/String;

    aget-object v0, v1, v0

    .line 82
    :goto_10
    return-object v0

    .line 76
    :cond_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 82
    :cond_14
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public static a(Ljava/io/DataInputStream;)Ljava/util/Hashtable;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInputStream;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    .line 172
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    .line 174
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v0

    and-int/lit16 v2, v0, 0xff

    .line 176
    const/4 v0, 0x0

    :goto_c
    if-ge v0, v2, :cond_1c

    .line 177
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    .line 178
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v4

    .line 179
    invoke-virtual {v1, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 182
    :cond_1c
    return-object v1
.end method

.method public static a(Ljava/io/DataOutputStream;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 96
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 98
    if-ltz v0, :cond_20

    .line 102
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 103
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 110
    :goto_13
    invoke-static {v1}, Lcom/google/android/location/h/b/g;->a(Ljava/lang/String;)I

    move-result v1

    .line 112
    if-lez v1, :cond_24

    .line 114
    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 115
    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 122
    :goto_1f
    return-void

    .line 107
    :cond_20
    const-string v0, ""

    move-object v1, p1

    goto :goto_13

    .line 119
    :cond_24
    invoke-virtual {p0, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 120
    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    goto :goto_1f
.end method

.method public static a(Ljava/io/DataOutputStream;Ljava/util/Hashtable;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 133
    .line 136
    if-eqz p1, :cond_38

    .line 137
    invoke-virtual {p1}, Ljava/util/Hashtable;->size()I

    move-result v1

    .line 139
    :goto_7
    if-eqz p2, :cond_a

    .line 140
    const/4 v0, 0x1

    .line 146
    :cond_a
    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 147
    if-eqz p2, :cond_18

    .line 148
    const-string v0, "Content-Type"

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 149
    invoke-virtual {p0, p2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 151
    :cond_18
    if-eqz p1, :cond_37

    .line 152
    invoke-virtual {p1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    .line 153
    :goto_1e
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 154
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 155
    invoke-virtual {p1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 156
    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    goto :goto_1e

    .line 160
    :cond_37
    return-void

    :cond_38
    move v1, v0

    goto :goto_7
.end method
