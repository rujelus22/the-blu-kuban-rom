.class public Lcom/google/android/location/h/f;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field protected final a:Ljava/io/InputStream;

.field protected b:Lcom/google/android/location/h/c;

.field protected c:I

.field protected d:Ljava/lang/Object;

.field protected volatile e:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/f;->d:Ljava/lang/Object;

    .line 49
    iput-object p1, p0, Lcom/google/android/location/h/f;->a:Ljava/io/InputStream;

    .line 50
    iput p2, p0, Lcom/google/android/location/h/f;->c:I

    .line 51
    return-void
.end method

.method private a([BII)I
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 185
    const/4 v0, -0x1

    .line 186
    iget v1, p0, Lcom/google/android/location/h/f;->c:I

    if-lez v1, :cond_21

    .line 188
    :try_start_5
    iget-object v0, p0, Lcom/google/android/location/h/f;->a:Ljava/io/InputStream;

    iget v1, p0, Lcom/google/android/location/h/f;->c:I

    invoke-static {v1, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 189
    if-lez v0, :cond_18

    .line 190
    iget v1, p0, Lcom/google/android/location/h/f;->c:I

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/location/h/f;->c:I

    .line 193
    :cond_18
    if-lez v0, :cond_1e

    iget v1, p0, Lcom/google/android/location/h/f;->c:I

    if-nez v1, :cond_21

    .line 194
    :cond_1e
    invoke-direct {p0}, Lcom/google/android/location/h/f;->c()V
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_21} :catch_22

    .line 201
    :cond_21
    return v0

    .line 196
    :catch_22
    move-exception v0

    .line 197
    invoke-direct {p0}, Lcom/google/android/location/h/f;->c()V

    .line 198
    throw v0
.end method

.method private c()V
    .registers 3

    .prologue
    .line 71
    iget-object v1, p0, Lcom/google/android/location/h/f;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/location/h/f;->e:Z

    if-nez v0, :cond_14

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/h/f;->e:Z

    .line 74
    iget-object v0, p0, Lcom/google/android/location/h/f;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_16

    .line 80
    :try_start_f
    iget-object v0, p0, Lcom/google/android/location/h/f;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_14
    .catchall {:try_start_f .. :try_end_14} :catchall_16
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_14} :catch_19

    .line 85
    :cond_14
    :goto_14
    :try_start_14
    monitor-exit v1

    .line 86
    return-void

    .line 85
    :catchall_16
    move-exception v0

    monitor-exit v1
    :try_end_18
    .catchall {:try_start_14 .. :try_end_18} :catchall_16

    throw v0

    .line 81
    :catch_19
    move-exception v0

    goto :goto_14
.end method

.method private d()I
    .registers 3

    .prologue
    .line 168
    const/4 v0, -0x1

    .line 169
    iget v1, p0, Lcom/google/android/location/h/f;->c:I

    if-lez v1, :cond_18

    .line 171
    :try_start_5
    iget-object v0, p0, Lcom/google/android/location/h/f;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 172
    if-ltz v0, :cond_15

    iget v1, p0, Lcom/google/android/location/h/f;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/location/h/f;->c:I

    if-nez v1, :cond_18

    .line 173
    :cond_15
    invoke-direct {p0}, Lcom/google/android/location/h/f;->c()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_18} :catch_19

    .line 180
    :cond_18
    return v0

    .line 175
    :catch_19
    move-exception v0

    .line 176
    invoke-direct {p0}, Lcom/google/android/location/h/f;->c()V

    .line 177
    throw v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 59
    iget-object v1, p0, Lcom/google/android/location/h/f;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 60
    :goto_3
    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/location/h/f;->e:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_11

    if-nez v0, :cond_f

    .line 62
    :try_start_7
    iget-object v0, p0, Lcom/google/android/location/h/f;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_11
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_c} :catch_d

    goto :goto_3

    .line 63
    :catch_d
    move-exception v0

    goto :goto_3

    .line 67
    :cond_f
    :try_start_f
    monitor-exit v1

    .line 68
    return-void

    .line 67
    :catchall_11
    move-exception v0

    monitor-exit v1
    :try_end_13
    .catchall {:try_start_f .. :try_end_13} :catchall_11

    throw v0
.end method

.method public b()V
    .registers 4

    .prologue
    .line 107
    monitor-enter p0

    .line 108
    :try_start_1
    iget v0, p0, Lcom/google/android/location/h/f;->c:I

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/location/h/f;->b:Lcom/google/android/location/h/c;

    if-eqz v0, :cond_b

    .line 109
    :cond_9
    monitor-exit p0

    .line 134
    :goto_a
    return-void

    .line 112
    :cond_b
    new-instance v0, Lcom/google/android/location/h/c;

    const/high16 v1, 0x1

    iget v2, p0, Lcom/google/android/location/h/f;->c:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/location/h/c;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/h/f;->b:Lcom/google/android/location/h/c;

    .line 114
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_38

    .line 116
    const/16 v0, 0x400

    new-array v0, v0, [B

    .line 119
    :goto_1f
    :try_start_1f
    iget v1, p0, Lcom/google/android/location/h/f;->c:I

    if-lez v1, :cond_4a

    .line 120
    const/4 v1, 0x0

    array-length v2, v0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/location/h/f;->a([BII)I

    move-result v1

    .line 122
    if-lez v1, :cond_3b

    .line 123
    iget-object v2, p0, Lcom/google/android/location/h/f;->b:Lcom/google/android/location/h/c;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/location/h/c;->a([BI)V
    :try_end_30
    .catchall {:try_start_1f .. :try_end_30} :catchall_43
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_30} :catch_31

    goto :goto_1f

    .line 128
    :catch_31
    move-exception v0

    .line 132
    iget-object v0, p0, Lcom/google/android/location/h/f;->b:Lcom/google/android/location/h/c;

    invoke-virtual {v0}, Lcom/google/android/location/h/c;->b()V

    goto :goto_a

    .line 114
    :catchall_38
    move-exception v0

    :try_start_39
    monitor-exit p0
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_38

    throw v0

    .line 125
    :cond_3b
    :try_start_3b
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Premature EOF"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_43
    .catchall {:try_start_3b .. :try_end_43} :catchall_43
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_43} :catch_31

    .line 132
    :catchall_43
    move-exception v0

    iget-object v1, p0, Lcom/google/android/location/h/f;->b:Lcom/google/android/location/h/c;

    invoke-virtual {v1}, Lcom/google/android/location/h/c;->b()V

    throw v0

    :cond_4a
    iget-object v0, p0, Lcom/google/android/location/h/f;->b:Lcom/google/android/location/h/c;

    invoke-virtual {v0}, Lcom/google/android/location/h/c;->b()V

    goto :goto_a
.end method

.method public declared-synchronized close()V
    .registers 2

    .prologue
    .line 95
    monitor-enter p0

    :try_start_1
    invoke-super {p0}, Ljava/io/InputStream;->close()V

    .line 97
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/location/h/f;->read()I
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_c

    move-result v0

    if-gez v0, :cond_4

    .line 100
    monitor-exit p0

    return-void

    .line 95
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized read()I
    .registers 2

    .prologue
    .line 153
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/h/f;->b:Lcom/google/android/location/h/c;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/location/h/f;->b:Lcom/google/android/location/h/c;

    invoke-virtual {v0}, Lcom/google/android/location/h/c;->a()I
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_12

    move-result v0

    :goto_b
    monitor-exit p0

    return v0

    :cond_d
    :try_start_d
    invoke-direct {p0}, Lcom/google/android/location/h/f;->d()I
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_12

    move-result v0

    goto :goto_b

    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public read([B)I
    .registers 4
    .parameter

    .prologue
    .line 163
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/location/h/f;->read([BII)I

    move-result v0

    return v0
.end method

.method public declared-synchronized read([BII)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 158
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/h/f;->b:Lcom/google/android/location/h/c;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/location/h/f;->b:Lcom/google/android/location/h/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/h/c;->a([BII)I
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_12

    move-result v0

    :goto_b
    monitor-exit p0

    return v0

    :cond_d
    :try_start_d
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/h/f;->a([BII)I
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_12

    move-result v0

    goto :goto_b

    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method
