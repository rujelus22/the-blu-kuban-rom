.class public Lcom/google/android/location/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/k/a;
.implements Lcom/google/android/location/os/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/location/k/a",
        "<",
        "Lcom/google/android/location/t;",
        ">;",
        "Lcom/google/android/location/os/a;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/android/location/os/i;

.field final b:Lcom/google/android/location/b/f;

.field final c:Lcom/google/android/location/b/g;

.field final d:Lcom/google/android/location/b/b;

.field final e:Lcom/google/android/location/i/a;

.field final f:Lcom/google/android/location/p;

.field final g:Lcom/google/android/location/o;

.field final h:Lcom/google/android/location/x;

.field final i:Lcom/google/android/location/b/e;

.field final j:Lcom/google/android/location/t;

.field final k:Lcom/google/android/location/g;

.field final l:Lcom/google/android/location/w;

.field final m:Lcom/google/android/location/a/b;

.field final n:Lcom/google/android/location/e/d;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;Lcom/google/android/location/i/a;Lcom/google/android/location/b/e;)V
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 60
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/q;->a:Lcom/google/android/location/os/i;

    .line 61
    new-instance v2, Lcom/google/android/location/b/f;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v3

    move-object/from16 v0, p2

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/location/b/f;-><init>(Lcom/google/android/location/os/h;J)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    .line 62
    new-instance v2, Lcom/google/android/location/b/g;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v1}, Lcom/google/android/location/b/g;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    .line 63
    new-instance v2, Lcom/google/android/location/b/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v3, v4, v1}, Lcom/google/android/location/b/b;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/os/h;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    .line 64
    invoke-static/range {p1 .. p1}, Lcom/google/android/location/t;->a(Lcom/google/android/location/os/i;)Lcom/google/android/location/t;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->j:Lcom/google/android/location/t;

    .line 65
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/q;->i:Lcom/google/android/location/b/e;

    .line 66
    new-instance v12, Lcom/google/android/location/g/l;

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-direct {v12, v0, v1}, Lcom/google/android/location/g/l;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/e;)V

    .line 67
    new-instance v7, Lcom/google/android/location/e/n;

    invoke-direct {v7}, Lcom/google/android/location/e/n;-><init>()V

    .line 69
    new-instance v2, Lcom/google/android/location/g;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/i;->k()Ljavax/crypto/SecretKey;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/i;->l()[B

    move-result-object v4

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/location/g;-><init>(Lcom/google/android/location/os/f;Ljavax/crypto/SecretKey;[B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->k:Lcom/google/android/location/g;

    .line 71
    new-instance v2, Lcom/google/android/location/x;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/q;->k:Lcom/google/android/location/g;

    move-object/from16 v3, p1

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v8}, Lcom/google/android/location/x;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/g;Lcom/google/android/location/e/n;Lcom/google/android/location/i/a;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->h:Lcom/google/android/location/x;

    .line 72
    new-instance v2, Lcom/google/android/location/e/d;

    invoke-direct {v2}, Lcom/google/android/location/e/d;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->n:Lcom/google/android/location/e/d;

    .line 77
    new-instance v8, Lcom/google/android/location/p;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/location/q;->k:Lcom/google/android/location/g;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/location/q;->h:Lcom/google/android/location/x;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/q;->n:Lcom/google/android/location/e/d;

    move-object/from16 v16, v0

    move-object/from16 v9, p1

    move-object v14, v7

    invoke-direct/range {v8 .. v16}, Lcom/google/android/location/p;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/g/l;Lcom/google/android/location/g;Lcom/google/android/location/e/n;Lcom/google/android/location/x;Lcom/google/android/location/e/d;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    .line 79
    new-instance v2, Lcom/google/android/location/a/b;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lcom/google/android/location/a/b;-><init>(Lcom/google/android/location/os/i;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    .line 80
    new-instance v2, Lcom/google/android/location/o;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/q;->k:Lcom/google/android/location/g;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/q;->h:Lcom/google/android/location/x;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/location/q;->j:Lcom/google/android/location/t;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/location/q;->n:Lcom/google/android/location/e/d;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    move-object/from16 v3, p1

    move-object/from16 v9, p3

    invoke-direct/range {v2 .. v12}, Lcom/google/android/location/o;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/g;Lcom/google/android/location/x;Lcom/google/android/location/t;Lcom/google/android/location/i/a;Lcom/google/android/location/e/d;Lcom/google/android/location/a/b;Lcom/google/android/location/g/l;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    .line 82
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/q;->e:Lcom/google/android/location/i/a;

    .line 83
    new-instance v2, Lcom/google/android/location/w;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/q;->k:Lcom/google/android/location/g;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/q;->j:Lcom/google/android/location/t;

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/location/w;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/g;Lcom/google/android/location/t;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/location/q;->l:Lcom/google/android/location/w;

    .line 84
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/location/q;->j:Lcom/google/android/location/t;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/location/t;->a(Lcom/google/android/location/k/a;)V

    .line 85
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(Lcom/google/android/location/os/a;)V

    .line 86
    return-void
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    iget-object v1, p0, Lcom/google/android/location/q;->a:Lcom/google/android/location/os/i;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/f;->a(Lcom/google/android/location/os/i;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/t;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/t;->c(Z)V

    .line 118
    iget-object v0, p0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    if-eqz v0, :cond_16

    .line 119
    iget-object v0, p0, Lcom/google/android/location/q;->c:Lcom/google/android/location/b/g;

    invoke-virtual {v0}, Lcom/google/android/location/b/g;->a()V

    .line 121
    :cond_16
    iget-object v0, p0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    if-eqz v0, :cond_1f

    .line 123
    iget-object v0, p0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    invoke-virtual {v0}, Lcom/google/android/location/b/b;->a()V

    .line 126
    :cond_1f
    iget-object v0, p0, Lcom/google/android/location/q;->h:Lcom/google/android/location/x;

    iget-object v1, p0, Lcom/google/android/location/q;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/x;->a(J)V

    .line 127
    iget-object v0, p0, Lcom/google/android/location/q;->i:Lcom/google/android/location/b/e;

    if-eqz v0, :cond_33

    .line 128
    iget-object v0, p0, Lcom/google/android/location/q;->i:Lcom/google/android/location/b/e;

    invoke-virtual {v0}, Lcom/google/android/location/b/e;->a()V

    .line 131
    :cond_33
    iget-object v0, p0, Lcom/google/android/location/q;->k:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->a()V

    .line 132
    return-void
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 153
    packed-switch p1, :pswitch_data_26

    .line 184
    :cond_3
    :goto_3
    return-void

    .line 155
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0}, Lcom/google/android/location/p;->b()V

    goto :goto_3

    .line 170
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    invoke-virtual {v0, p1}, Lcom/google/android/location/o;->a(I)V

    goto :goto_3

    .line 173
    :pswitch_10
    iget-object v0, p0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    if-eqz v0, :cond_3

    .line 174
    iget-object v0, p0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/b;->a(I)V

    goto :goto_3

    .line 178
    :pswitch_1a
    iget-object v0, p0, Lcom/google/android/location/q;->l:Lcom/google/android/location/w;

    invoke-virtual {v0, p1}, Lcom/google/android/location/w;->a(I)V

    goto :goto_3

    .line 181
    :pswitch_20
    iget-object v0, p0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a/b;->a(I)V

    goto :goto_3

    .line 153
    :pswitch_data_26
    .packed-switch 0x0
        :pswitch_4
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_10
        :pswitch_a
        :pswitch_a
        :pswitch_1a
        :pswitch_20
        :pswitch_a
        :pswitch_a
    .end packed-switch
.end method

.method public a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/a/b;->a(II)V

    .line 306
    return-void
.end method

.method public a(IIZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/t;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/t;->a(IIZ)V

    .line 230
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/o;->a(IIZ)V

    .line 231
    return-void
.end method

.method public a(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/p;->a(IZ)V

    .line 149
    return-void
.end method

.method public a(Lcom/google/android/location/a/n$b;)V
    .registers 3
    .parameter

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/n$b;)V

    .line 296
    return-void
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    invoke-virtual {v0, p1}, Lcom/google/android/location/o;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    .line 301
    return-void
.end method

.method public a(Lcom/google/android/location/e/E;)V
    .registers 3
    .parameter

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->a(Lcom/google/android/location/e/E;)V

    .line 217
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    invoke-virtual {v0, p1}, Lcom/google/android/location/o;->a(Lcom/google/android/location/e/E;)V

    .line 218
    return-void
.end method

.method public a(Lcom/google/android/location/e/e;)V
    .registers 5
    .parameter

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/t;

    invoke-virtual {v0, p1}, Lcom/google/android/location/t;->a(Lcom/google/android/location/e/e;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/location/q;->n:Lcom/google/android/location/e/d;

    iget-object v1, p0, Lcom/google/android/location/q;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/e/d;->a(Lcom/google/android/location/e/e;J)V

    .line 190
    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->a(Lcom/google/android/location/e/e;)V

    .line 191
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    invoke-virtual {v0, p1}, Lcom/google/android/location/o;->a(Lcom/google/android/location/e/e;)V

    .line 192
    return-void
.end method

.method public a(Lcom/google/android/location/e/t;)V
    .registers 4
    .parameter

    .prologue
    .line 288
    iget-object v0, p1, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-eqz v0, :cond_13

    iget-object v0, p1, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v0, v0, Lcom/google/android/location/e/D;->a:Lcom/google/android/location/e/E;

    if-eqz v0, :cond_13

    .line 289
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    iget-object v1, p1, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v1, v1, Lcom/google/android/location/e/D;->a:Lcom/google/android/location/e/E;

    invoke-virtual {v0, v1}, Lcom/google/android/location/o;->b(Lcom/google/android/location/e/E;)V

    .line 291
    :cond_13
    return-void
.end method

.method public a(Lcom/google/android/location/os/g;)V
    .registers 4
    .parameter

    .prologue
    .line 206
    invoke-interface {p1}, Lcom/google/android/location/os/g;->h()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {p1}, Lcom/google/android/location/os/g;->d()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_e

    .line 212
    :goto_d
    return-void

    .line 210
    :cond_e
    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->a(Lcom/google/android/location/os/g;)V

    .line 211
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    invoke-virtual {v0, p1}, Lcom/google/android/location/o;->a(Lcom/google/android/location/os/g;)V

    goto :goto_d
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .registers 3
    .parameter

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/location/q;->e:Lcom/google/android/location/i/a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/i/a;->a(Lcom/google/android/location/os/h;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/t;

    invoke-virtual {v0, p1}, Lcom/google/android/location/t;->a(Lcom/google/android/location/os/h;)V

    .line 275
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    invoke-virtual {v0, p1}, Lcom/google/android/location/o;->a(Lcom/google/android/location/os/h;)V

    .line 276
    return-void
.end method

.method public a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/o;->a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V

    .line 317
    return-void
.end method

.method public a(Lcom/google/android/location/t;)V
    .registers 3
    .parameter

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    invoke-virtual {v0}, Lcom/google/android/location/o;->c()V

    .line 281
    iget-object v0, p0, Lcom/google/android/location/q;->l:Lcom/google/android/location/w;

    invoke-virtual {v0}, Lcom/google/android/location/w;->a()V

    .line 282
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 197
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 34
    check-cast p1, Lcom/google/android/location/t;

    invoke-virtual {p0, p1}, Lcom/google/android/location/q;->a(Lcom/google/android/location/t;)V

    return-void
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/t;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/location/t;->c(Z)V

    .line 137
    if-eqz p1, :cond_1a

    .line 138
    iget-object v0, p0, Lcom/google/android/location/q;->b:Lcom/google/android/location/b/f;

    iget-object v1, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    iget-object v1, v1, Lcom/google/android/location/p;->a:Lcom/google/android/location/os/i;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/f;->b(Lcom/google/android/location/os/i;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/location/q;->i:Lcom/google/android/location/b/e;

    if-eqz v0, :cond_1a

    .line 141
    iget-object v0, p0, Lcom/google/android/location/q;->i:Lcom/google/android/location/b/e;

    invoke-virtual {v0}, Lcom/google/android/location/b/e;->b()V

    .line 144
    :cond_1a
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/t;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/t;->a(ZLjava/lang/String;)V

    .line 311
    iget-object v0, p0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/a/b;->a(ZLjava/lang/String;)V

    .line 312
    return-void
.end method

.method public a(ZZI)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/location/q;->e:Lcom/google/android/location/i/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/i/a;->a(ZZI)V

    .line 267
    iget-object v0, p0, Lcom/google/android/location/q;->l:Lcom/google/android/location/w;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/w;->a(ZZI)V

    .line 268
    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/p;->a(ZZI)V

    .line 269
    return-void
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    invoke-virtual {v0, p1}, Lcom/google/android/location/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 202
    return-void
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->b(Z)V

    .line 223
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    invoke-virtual {v0, p1}, Lcom/google/android/location/o;->a(Z)V

    .line 224
    iget-object v0, p0, Lcom/google/android/location/q;->l:Lcom/google/android/location/w;

    invoke-virtual {v0, p1}, Lcom/google/android/location/w;->a(Z)V

    .line 225
    return-void
.end method

.method public c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 255
    return-void
.end method

.method public c(Z)V
    .registers 5
    .parameter

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/location/q;->n:Lcom/google/android/location/e/d;

    iget-object v1, p0, Lcom/google/android/location/q;->a:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/e/d;->a(ZJ)V

    .line 236
    iget-object v0, p0, Lcom/google/android/location/q;->j:Lcom/google/android/location/t;

    invoke-virtual {v0, p1}, Lcom/google/android/location/t;->b(Z)V

    .line 237
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    invoke-virtual {v0, p1}, Lcom/google/android/location/o;->b(Z)V

    .line 238
    iget-object v0, p0, Lcom/google/android/location/q;->m:Lcom/google/android/location/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a/b;->a(Z)V

    .line 239
    return-void
.end method

.method public d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    if-eqz v0, :cond_9

    .line 260
    iget-object v0, p0, Lcom/google/android/location/q;->d:Lcom/google/android/location/b/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 262
    :cond_9
    return-void
.end method

.method public d(Z)V
    .registers 3
    .parameter

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/location/q;->f:Lcom/google/android/location/p;

    invoke-virtual {v0, p1}, Lcom/google/android/location/p;->a(Z)V

    .line 244
    return-void
.end method

.method public e(Z)V
    .registers 3
    .parameter

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/location/q;->g:Lcom/google/android/location/o;

    invoke-virtual {v0, p1}, Lcom/google/android/location/o;->c(Z)V

    .line 250
    return-void
.end method
