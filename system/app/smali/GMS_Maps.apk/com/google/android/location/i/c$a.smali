.class public Lcom/google/android/location/i/c$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/i/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:J

.field private final b:Lcom/google/android/location/i/c;

.field private c:J


# direct methods
.method private constructor <init>(Lcom/google/android/location/i/c;J)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424
    iput-object p1, p0, Lcom/google/android/location/i/c$a;->b:Lcom/google/android/location/i/c;

    .line 425
    iput-wide p2, p0, Lcom/google/android/location/i/c$a;->a:J

    .line 426
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/i/c$a;->c:J

    .line 427
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/i/c;JLcom/google/android/location/i/c$1;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 418
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/i/c$a;-><init>(Lcom/google/android/location/i/c;J)V

    return-void
.end method


# virtual methods
.method declared-synchronized a(J)J
    .registers 9
    .parameter

    .prologue
    const-wide/16 v0, 0x0

    .line 437
    monitor-enter p0

    cmp-long v2, p1, v0

    if-gez v2, :cond_9

    .line 442
    :goto_7
    monitor-exit p0

    return-wide v0

    .line 440
    :cond_9
    :try_start_9
    iget-wide v0, p0, Lcom/google/android/location/i/c$a;->a:J

    iget-wide v2, p0, Lcom/google/android/location/i/c$a;->c:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 441
    iget-wide v2, p0, Lcom/google/android/location/i/c$a;->c:J

    add-long/2addr v2, v0

    iget-wide v4, p0, Lcom/google/android/location/i/c$a;->a:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/location/i/c$a;->c:J

    .line 442
    iget-object v2, p0, Lcom/google/android/location/i/c$a;->b:Lcom/google/android/location/i/c;

    invoke-static {v2, v0, v1}, Lcom/google/android/location/i/c;->a(Lcom/google/android/location/i/c;J)J
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_24

    move-result-wide v0

    goto :goto_7

    .line 437
    :catchall_24
    move-exception v0

    monitor-exit p0

    throw v0
.end method
