.class public abstract Lcom/google/android/location/h/b/m;
.super Lcom/google/android/location/h/b/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/h/b/m$a;
    }
.end annotation


# instance fields
.field private a:J

.field private b:J

.field protected c:Lcom/google/android/location/h/b/m$a;

.field protected d:J

.field protected e:I

.field private f:Z

.field private g:J

.field private h:J

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, -0x1

    .line 46
    invoke-direct {p0}, Lcom/google/android/location/h/b/a;-><init>()V

    .line 70
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/google/android/location/h/b/m;->d:J

    .line 71
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/location/h/b/m;->e:I

    .line 74
    iput-wide v2, p0, Lcom/google/android/location/h/b/m;->a:J

    .line 77
    iput-wide v2, p0, Lcom/google/android/location/h/b/m;->b:J

    .line 85
    iput-wide v2, p0, Lcom/google/android/location/h/b/m;->g:J

    .line 92
    iput-wide v2, p0, Lcom/google/android/location/h/b/m;->h:J

    .line 99
    iput-boolean v4, p0, Lcom/google/android/location/h/b/m;->i:Z

    .line 104
    iput-boolean v4, p0, Lcom/google/android/location/h/b/m;->j:Z

    .line 363
    return-void
.end method

.method private f()V
    .registers 5

    .prologue
    .line 203
    iget-wide v0, p0, Lcom/google/android/location/h/b/m;->b:J

    iget-wide v2, p0, Lcom/google/android/location/h/b/m;->a:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/h/b/m;->b:J

    .line 204
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public declared-synchronized a(Lcom/google/android/location/h/b/m$a;)V
    .registers 3
    .parameter

    .prologue
    .line 145
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/location/h/b/m;->c:Lcom/google/android/location/h/b/m$a;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 146
    monitor-exit p0

    return-void

    .line 145
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(J)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 177
    monitor-enter p0

    :try_start_2
    iget-wide v1, p0, Lcom/google/android/location/h/b/m;->h:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_15

    iget-wide v1, p0, Lcom/google/android/location/h/b/m;->g:J

    iget-wide v3, p0, Lcom/google/android/location/h/b/m;->h:J
    :try_end_e
    .catchall {:try_start_2 .. :try_end_e} :catchall_1b

    add-long/2addr v1, v3

    cmp-long v1, v1, p1

    if-gez v1, :cond_15

    .line 182
    :cond_13
    :goto_13
    monitor-exit p0

    return v0

    :cond_15
    :try_start_15
    iget v1, p0, Lcom/google/android/location/h/b/m;->e:I
    :try_end_17
    .catchall {:try_start_15 .. :try_end_17} :catchall_1b

    if-lez v1, :cond_13

    const/4 v0, 0x1

    goto :goto_13

    .line 177
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(I)V
    .registers 3
    .parameter

    .prologue
    .line 268
    monitor-enter p0

    :try_start_1
    iput p1, p0, Lcom/google/android/location/h/b/m;->e:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 269
    monitor-exit p0

    return-void

    .line 268
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(J)V
    .registers 7
    .parameter

    .prologue
    .line 210
    monitor-enter p0

    :try_start_1
    iget-wide v0, p0, Lcom/google/android/location/h/b/m;->d:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/location/h/b/m;->a:J

    .line 211
    iget-boolean v0, p0, Lcom/google/android/location/h/b/m;->f:Z

    if-eqz v0, :cond_1a

    .line 212
    iget-wide v0, p0, Lcom/google/android/location/h/b/m;->a:J

    iput-wide v0, p0, Lcom/google/android/location/h/b/m;->b:J

    .line 216
    :goto_e
    invoke-direct {p0}, Lcom/google/android/location/h/b/m;->f()V

    .line 217
    iget-wide v0, p0, Lcom/google/android/location/h/b/m;->d:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/h/b/m;->d:J
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_21

    .line 218
    monitor-exit p0

    return-void

    .line 214
    :cond_1a
    const-wide/32 v0, 0x36ee80

    add-long/2addr v0, p1

    :try_start_1e
    iput-wide v0, p0, Lcom/google/android/location/h/b/m;->b:J
    :try_end_20
    .catchall {:try_start_1e .. :try_end_20} :catchall_21

    goto :goto_e

    .line 210
    :catchall_21
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract b_()I
.end method

.method public declared-synchronized c(J)V
    .registers 5
    .parameter

    .prologue
    .line 226
    monitor-enter p0

    const-wide/16 v0, 0xbb8

    add-long/2addr v0, p1

    :try_start_4
    iput-wide v0, p0, Lcom/google/android/location/h/b/m;->a:J

    .line 227
    const-wide/16 v0, 0x1388

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/location/h/b/m;->b:J

    .line 228
    invoke-direct {p0}, Lcom/google/android/location/h/b/m;->f()V
    :try_end_e
    .catchall {:try_start_4 .. :try_end_e} :catchall_10

    .line 229
    monitor-exit p0

    return-void

    .line 226
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract c_()Ljava/io/InputStream;
.end method

.method public d(J)V
    .registers 5
    .parameter

    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/google/android/location/h/b/m;->f:Z

    if-eqz v0, :cond_e

    .line 304
    const-wide/16 v0, 0xbb8

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/location/h/b/m;->a:J

    .line 305
    const-wide/16 v0, 0x1388

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/location/h/b/m;->b:J

    .line 307
    :cond_e
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    .line 160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/h/b/m;->j:Z

    .line 161
    return-void
.end method

.method public declared-synchronized i()Lcom/google/android/location/h/b/m$a;
    .registers 2

    .prologue
    .line 138
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/h/b/m;->c:Lcom/google/android/location/h/b/m$a;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-object v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/google/android/location/h/b/m;->f:Z

    return v0
.end method

.method public k()V
    .registers 2

    .prologue
    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/h/b/m;->f:Z

    .line 156
    return-void
.end method

.method public l()Z
    .registers 2

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/google/android/location/h/b/m;->j:Z

    return v0
.end method

.method public declared-synchronized m()V
    .registers 2

    .prologue
    .line 235
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/location/h/b/m;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/location/h/b/m;->e:I
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 236
    monitor-exit p0

    return-void

    .line 235
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public n()J
    .registers 3

    .prologue
    .line 242
    iget-wide v0, p0, Lcom/google/android/location/h/b/m;->a:J

    return-wide v0
.end method

.method public o()J
    .registers 3

    .prologue
    .line 249
    iget-wide v0, p0, Lcom/google/android/location/h/b/m;->b:J

    return-wide v0
.end method

.method public p()J
    .registers 3

    .prologue
    .line 283
    iget-wide v0, p0, Lcom/google/android/location/h/b/m;->h:J

    return-wide v0
.end method

.method public q()Ljava/lang/String;
    .registers 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/location/h/b/m;->k:Ljava/lang/String;

    return-object v0
.end method

.method public r()V
    .registers 5

    .prologue
    const-wide/16 v2, -0x1

    .line 315
    iget-wide v0, p0, Lcom/google/android/location/h/b/m;->g:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_16

    .line 316
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/h/b/m;->g:J

    .line 321
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/location/h/b/m;->j()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 322
    invoke-virtual {p0}, Lcom/google/android/location/h/b/m;->p()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_28

    .line 323
    const-wide/16 v0, 0x4e20

    iput-wide v0, p0, Lcom/google/android/location/h/b/m;->h:J

    .line 326
    :cond_28
    return-void
.end method

.method public declared-synchronized s()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 336
    monitor-enter p0

    :try_start_2
    iget-boolean v1, p0, Lcom/google/android/location/h/b/m;->i:Z

    if-nez v1, :cond_b

    .line 337
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/h/b/m;->i:Z
    :try_end_9
    .catchall {:try_start_2 .. :try_end_9} :catchall_d

    .line 340
    :goto_9
    monitor-exit p0

    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_9

    .line 336
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public t()Z
    .registers 2

    .prologue
    .line 348
    iget-boolean v0, p0, Lcom/google/android/location/h/b/m;->i:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 352
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "request[id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/location/h/b/m;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",retrySoft="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/location/h/b/m;->n()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",retryDeadline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/location/h/b/m;->o()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",sendCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/h/b/m;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",secure="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/h/b/m;->j:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",now="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",obj="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
