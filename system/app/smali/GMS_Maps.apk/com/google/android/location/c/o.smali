.class Lcom/google/android/location/c/o;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/o$a;
    }
.end annotation


# instance fields
.field private A:Lcom/google/android/location/c/o$a;

.field private final B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/Integer;

.field private d:Z

.field private e:J

.field private f:I

.field private g:Z

.field private h:J

.field private i:J

.field private j:J

.field private k:J

.field private final l:Lcom/google/android/location/c/D;

.field private m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:J

.field private v:J

.field private w:J

.field private x:J

.field private y:J

.field private z:Lcom/google/android/location/k/a/c;


# direct methods
.method public constructor <init>(Lcom/google/android/location/c/D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/k/a/c;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const-wide/16 v1, 0x0

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-boolean v4, p0, Lcom/google/android/location/c/o;->d:Z

    .line 84
    iput-wide v1, p0, Lcom/google/android/location/c/o;->e:J

    .line 86
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/location/c/o;->f:I

    .line 92
    iput-boolean v4, p0, Lcom/google/android/location/c/o;->g:Z

    .line 97
    iput-wide v1, p0, Lcom/google/android/location/c/o;->h:J

    .line 102
    iput-wide v1, p0, Lcom/google/android/location/c/o;->i:J

    .line 107
    iput-wide v1, p0, Lcom/google/android/location/c/o;->j:J

    .line 112
    iput-wide v1, p0, Lcom/google/android/location/c/o;->k:J

    .line 118
    iput v3, p0, Lcom/google/android/location/c/o;->p:I

    .line 119
    iput v3, p0, Lcom/google/android/location/c/o;->q:I

    .line 120
    iput v3, p0, Lcom/google/android/location/c/o;->r:I

    .line 121
    iput v3, p0, Lcom/google/android/location/c/o;->s:I

    .line 122
    iput v3, p0, Lcom/google/android/location/c/o;->t:I

    .line 123
    iput-wide v1, p0, Lcom/google/android/location/c/o;->u:J

    .line 124
    iput-wide v1, p0, Lcom/google/android/location/c/o;->v:J

    .line 125
    iput-wide v1, p0, Lcom/google/android/location/c/o;->w:J

    .line 126
    iput-wide v1, p0, Lcom/google/android/location/c/o;->x:J

    .line 127
    iput-wide v1, p0, Lcom/google/android/location/c/o;->y:J

    .line 130
    new-instance v0, Lcom/google/android/location/c/o$a;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/o$a;-><init>(Lcom/google/android/location/c/o;)V

    iput-object v0, p0, Lcom/google/android/location/c/o;->A:Lcom/google/android/location/c/o$a;

    .line 147
    iput-object p1, p0, Lcom/google/android/location/c/o;->l:Lcom/google/android/location/c/D;

    .line 148
    invoke-static {p6}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/o;->z:Lcom/google/android/location/k/a/c;

    .line 149
    invoke-static {p2}, Lcom/google/android/location/c/L;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/o;->a:Ljava/lang/String;

    .line 150
    iput-object p3, p0, Lcom/google/android/location/c/o;->b:Ljava/lang/String;

    .line 151
    iput-object p4, p0, Lcom/google/android/location/c/o;->c:Ljava/lang/Integer;

    .line 152
    if-eqz p5, :cond_4c

    .line 153
    sget-object v0, Lcom/google/android/location/j/a;->m:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {p0, p5, v0}, Lcom/google/android/location/c/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 155
    :cond_4c
    iput-object p5, p0, Lcom/google/android/location/c/o;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 156
    invoke-direct {p0}, Lcom/google/android/location/c/o;->d()V

    .line 157
    invoke-direct {p0}, Lcom/google/android/location/c/o;->e()V

    .line 158
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/c/o;->h:J

    .line 159
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/c/o;->i:J

    .line 160
    return-void
.end method

.method private a(J)J
    .registers 9
    .parameter

    .prologue
    .line 631
    iget-wide v0, p0, Lcom/google/android/location/c/o;->k:J

    sub-long v0, p1, v0

    iget-wide v2, p0, Lcom/google/android/location/c/o;->j:J

    iget-wide v4, p0, Lcom/google/android/location/c/o;->i:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xf4240

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/location/c/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/location/c/o;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method private a(JJ)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 620
    iget-wide v0, p0, Lcom/google/android/location/c/o;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_c

    .line 621
    iput-wide p1, p0, Lcom/google/android/location/c/o;->k:J

    .line 622
    iput-wide p3, p0, Lcom/google/android/location/c/o;->j:J

    .line 624
    :cond_c
    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 963
    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getType()Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-result-object v0

    if-eq v0, p2, :cond_2d

    .line 964
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wront protocol buffer type. Expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getType()Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 967
    :cond_2d
    return-void
.end method

.method private declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 382
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/location/c/o;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/c/o;->e:J

    .line 383
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->ag:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 384
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/location/c/o;->e:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 385
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/location/c/o;->h:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 386
    if-eqz p2, :cond_33

    .line 387
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 392
    :goto_21
    iget-object v1, p0, Lcom/google/android/location/c/o;->l:Lcom/google/android/location/c/D;

    if-eqz v1, :cond_31

    .line 393
    iget-object v1, p0, Lcom/google/android/location/c/o;->l:Lcom/google/android/location/c/D;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/c/D;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    .line 394
    if-eqz p2, :cond_31

    .line 395
    iget-object v0, p0, Lcom/google/android/location/c/o;->l:Lcom/google/android/location/c/D;

    invoke-virtual {v0}, Lcom/google/android/location/c/D;->d()V
    :try_end_31
    .catchall {:try_start_1 .. :try_end_31} :catchall_40

    .line 398
    :cond_31
    monitor-exit p0

    return-void

    .line 389
    :cond_33
    const/4 v1, 0x3

    :try_start_34
    iget v2, p0, Lcom/google/android/location/c/o;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 390
    iget v1, p0, Lcom/google/android/location/c/o;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/location/c/o;->f:I
    :try_end_3f
    .catchall {:try_start_34 .. :try_end_3f} :catchall_40

    goto :goto_21

    .line 382
    :catchall_40
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(JJ)I
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 644
    sub-long v0, p3, p1

    .line 645
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_8

    .line 648
    :cond_8
    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/location/c/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/location/c/o;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method private c(JJ)I
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 660
    sub-long v0, p3, p1

    .line 661
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_8

    .line 664
    :cond_8
    const-wide/16 v2, 0x3e8

    rem-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method static synthetic c(Lcom/google/android/location/c/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method private declared-synchronized c()V
    .registers 3

    .prologue
    .line 309
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/o;->A:Lcom/google/android/location/c/o$a;

    invoke-virtual {v0}, Lcom/google/android/location/c/o$a;->a()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 310
    invoke-virtual {p0}, Lcom/google/android/location/c/o;->b()V

    .line 311
    invoke-direct {p0}, Lcom/google/android/location/c/o;->e()V

    .line 312
    iget-object v0, p0, Lcom/google/android/location/c/o;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/c/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    .line 313
    invoke-direct {p0}, Lcom/google/android/location/c/o;->d()V
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_1a

    .line 315
    :cond_18
    monitor-exit p0

    return-void

    .line 309
    :catchall_1a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()V
    .registers 5

    .prologue
    .line 321
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/o;->A:Lcom/google/android/location/c/o$a;

    invoke-virtual {v0}, Lcom/google/android/location/c/o$a;->b()V

    .line 322
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lcom/google/android/location/c/o;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 323
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->W:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 324
    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/location/c/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 325
    const/4 v1, 0x5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 326
    const/4 v1, 0x1

    const-string v2, "1.0"

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 327
    iget-object v1, p0, Lcom/google/android/location/c/o;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_39

    .line 328
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/location/c/o;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 330
    :cond_39
    iget-object v1, p0, Lcom/google/android/location/c/o;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 331
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->Y:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 332
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/c/o;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 333
    iget-object v1, p0, Lcom/google/android/location/c/o;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/location/c/L;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_67

    .line 334
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->o:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 335
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/c/o;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 336
    iget-object v2, p0, Lcom/google/android/location/c/o;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 338
    :cond_67
    iget-object v1, p0, Lcom/google/android/location/c/o;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_6d
    .catchall {:try_start_1 .. :try_end_6d} :catchall_6f

    .line 339
    monitor-exit p0

    return-void

    .line 321
    :catchall_6f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()V
    .registers 4

    .prologue
    .line 345
    monitor-enter p0

    :try_start_1
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lcom/google/android/location/c/o;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 346
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->j:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 347
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/c/o;->u:J

    .line 348
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/c/o;->v:J

    .line 349
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/c/o;->w:J

    .line 350
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/c/o;->x:J

    .line 351
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/c/o;->y:J

    .line 352
    iget-object v0, p0, Lcom/google/android/location/c/o;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_4a

    iget-boolean v0, p0, Lcom/google/android/location/c/o;->d:Z

    if-nez v0, :cond_4a

    .line 353
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/o;->d:Z

    .line 354
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->U:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 355
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/c/o;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 356
    iget-object v1, p0, Lcom/google/android/location/c/o;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x63

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_4a
    .catchall {:try_start_1 .. :try_end_4a} :catchall_4c

    .line 358
    :cond_4a
    monitor-exit p0

    return-void

    .line 345
    :catchall_4c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()J
    .registers 5

    .prologue
    .line 364
    monitor-enter p0

    :try_start_1
    iget-wide v0, p0, Lcom/google/android/location/c/o;->h:J

    iget-wide v2, p0, Lcom/google/android/location/c/o;->i:J

    sub-long/2addr v0, v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 366
    iget-wide v2, p0, Lcom/google/android/location/c/o;->e:J

    cmp-long v2, v0, v2

    if-gtz v2, :cond_16

    .line 369
    iget-wide v0, p0, Lcom/google/android/location/c/o;->e:J
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_18

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 371
    :cond_16
    monitor-exit p0

    return-wide v0

    .line 364
    :catchall_18
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method a(I)I
    .registers 3
    .parameter

    .prologue
    .line 594
    packed-switch p1, :pswitch_data_c

    .line 611
    :pswitch_3
    const/4 v0, -0x1

    :goto_4
    return v0

    .line 597
    :pswitch_5
    const/4 v0, 0x3

    goto :goto_4

    .line 603
    :pswitch_7
    const/4 v0, 0x5

    goto :goto_4

    .line 609
    :pswitch_9
    const/4 v0, 0x4

    goto :goto_4

    .line 594
    nop

    :pswitch_data_c
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_3
        :pswitch_9
        :pswitch_3
        :pswitch_3
        :pswitch_7
    .end packed-switch
.end method

.method public declared-synchronized a()V
    .registers 3

    .prologue
    .line 268
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/o;->g:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_1d

    if-eqz v0, :cond_7

    .line 278
    :goto_5
    monitor-exit p0

    return-void

    .line 271
    :cond_7
    const/4 v0, 0x1

    :try_start_8
    iput-boolean v0, p0, Lcom/google/android/location/c/o;->g:Z

    .line 273
    invoke-virtual {p0}, Lcom/google/android/location/c/o;->b()V

    .line 274
    iget-object v0, p0, Lcom/google/android/location/c/o;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/c/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    .line 275
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/o;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 276
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/o;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 277
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_1d

    goto :goto_5

    .line 268
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(FFFF)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 933
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->D:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 934
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 935
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 936
    const/4 v1, 0x3

    invoke-virtual {v0, v1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 937
    const/4 v1, 0x7

    invoke-virtual {v0, v1, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 938
    iget-object v1, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x9

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 939
    invoke-direct {p0}, Lcom/google/android/location/c/o;->c()V

    .line 940
    return-void
.end method

.method public declared-synchronized a(FFFIJJ)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 678
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/o;->g:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_79

    if-eqz v0, :cond_7

    .line 718
    :goto_5
    monitor-exit p0

    return-void

    .line 682
    :cond_7
    :try_start_7
    invoke-direct {p0, p5, p6, p7, p8}, Lcom/google/android/location/c/o;->a(JJ)V

    .line 683
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->au:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    const/16 v1, 0x8

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;I)V

    .line 684
    const/4 v0, 0x1

    invoke-virtual {v2, v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 685
    const/4 v0, 0x3

    invoke-virtual {v2, v0, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 686
    const/4 v0, 0x2

    invoke-virtual {v2, v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 687
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_32

    .line 689
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-direct {p0, p5, p6}, Lcom/google/android/location/c/o;->a(J)J

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 693
    :cond_32
    iget-wide v0, p0, Lcom/google/android/location/c/o;->v:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-nez v0, :cond_7c

    .line 694
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    .line 695
    invoke-direct {p0, p5, p6}, Lcom/google/android/location/c/o;->a(J)J

    move-result-wide v5

    .line 696
    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/location/c/o;->b(JJ)I

    move-result v1

    .line 697
    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/location/c/o;->c(JJ)I

    move-result v0

    .line 702
    :goto_4d
    if-eqz v1, :cond_53

    .line 703
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 705
    :cond_53
    if-eqz v0, :cond_5a

    .line 706
    const/16 v1, 0x8

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 708
    :cond_5a
    iput-wide p5, p0, Lcom/google/android/location/c/o;->v:J

    .line 711
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-eqz v0, :cond_69

    iget v0, p0, Lcom/google/android/location/c/o;->q:I

    if-eq v0, p4, :cond_6f

    .line 713
    :cond_69
    const/4 v0, 0x4

    invoke-virtual {v2, v0, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 714
    iput p4, p0, Lcom/google/android/location/c/o;->q:I

    .line 716
    :cond_6f
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 717
    invoke-direct {p0}, Lcom/google/android/location/c/o;->c()V
    :try_end_78
    .catchall {:try_start_7 .. :try_end_78} :catchall_79

    goto :goto_5

    .line 678
    :catchall_79
    move-exception v0

    monitor-exit p0

    throw v0

    .line 699
    :cond_7c
    :try_start_7c
    iget-wide v0, p0, Lcom/google/android/location/c/o;->v:J

    invoke-direct {p0, v0, v1, p5, p6}, Lcom/google/android/location/c/o;->b(JJ)I

    move-result v1

    .line 700
    iget-wide v3, p0, Lcom/google/android/location/c/o;->v:J

    invoke-direct {p0, v3, v4, p5, p6}, Lcom/google/android/location/c/o;->c(JJ)I
    :try_end_87
    .catchall {:try_start_7c .. :try_end_87} :catchall_79

    move-result v0

    goto :goto_4d
.end method

.method public a(FIJJ)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v8, 0xb

    const/4 v7, 0x4

    const/4 v5, 0x1

    .line 891
    iget-boolean v0, p0, Lcom/google/android/location/c/o;->g:Z

    if-eqz v0, :cond_9

    .line 929
    :goto_8
    return-void

    .line 895
    :cond_9
    invoke-direct {p0, p3, p4, p5, p6}, Lcom/google/android/location/c/o;->a(JJ)V

    .line 896
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->at:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;I)V

    .line 897
    invoke-virtual {v2, v5, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 898
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_27

    .line 900
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, p3, p4}, Lcom/google/android/location/c/o;->a(J)J

    move-result-wide v3

    invoke-virtual {v0, v5, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 904
    :cond_27
    iget-wide v0, p0, Lcom/google/android/location/c/o;->y:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-nez v0, :cond_69

    .line 905
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    .line 906
    invoke-direct {p0, p3, p4}, Lcom/google/android/location/c/o;->a(J)J

    move-result-wide v5

    .line 907
    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/location/c/o;->b(JJ)I

    move-result v1

    .line 908
    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/location/c/o;->c(JJ)I

    move-result v0

    .line 913
    :goto_41
    if-eqz v1, :cond_47

    .line 914
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 916
    :cond_47
    if-eqz v0, :cond_4c

    .line 917
    invoke-virtual {v2, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 919
    :cond_4c
    iput-wide p3, p0, Lcom/google/android/location/c/o;->y:J

    .line 922
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-eqz v0, :cond_5a

    iget v0, p0, Lcom/google/android/location/c/o;->t:I

    if-eq v0, p2, :cond_60

    .line 924
    :cond_5a
    const/4 v0, 0x2

    invoke-virtual {v2, v0, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 925
    iput p2, p0, Lcom/google/android/location/c/o;->t:I

    .line 927
    :cond_60
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 928
    invoke-direct {p0}, Lcom/google/android/location/c/o;->c()V

    goto :goto_8

    .line 910
    :cond_69
    iget-wide v0, p0, Lcom/google/android/location/c/o;->y:J

    invoke-direct {p0, v0, v1, p3, p4}, Lcom/google/android/location/c/o;->b(JJ)I

    move-result v1

    .line 911
    iget-wide v3, p0, Lcom/google/android/location/c/o;->y:J

    invoke-direct {p0, v3, v4, p3, p4}, Lcom/google/android/location/c/o;->c(JJ)I

    move-result v0

    goto :goto_41
.end method

.method public declared-synchronized a(ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Landroid/telephony/CellLocation;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/NeighboringCellInfo;",
            ">;J)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x3

    .line 535
    monitor-enter p0

    :try_start_3
    iget-boolean v2, p0, Lcom/google/android/location/c/o;->g:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_a8

    if-eqz v2, :cond_9

    .line 584
    :goto_7
    monitor-exit p0

    return-void

    .line 543
    :cond_9
    if-eqz p2, :cond_e9

    :try_start_b
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v3, :cond_e9

    .line 544
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 545
    const/4 v3, 0x3

    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move v4, v2

    .line 547
    :goto_2d
    if-nez p3, :cond_ab

    move v2, v1

    .line 557
    :goto_30
    new-instance v5, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/android/location/j/a;->an:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 558
    new-instance v6, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v7, Lcom/google/android/location/j/a;->ae:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 559
    const/4 v7, 0x1

    invoke-virtual {v6, v7, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 560
    const/4 v2, 0x2

    invoke-virtual {v6, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 561
    const/4 v1, 0x3

    invoke-virtual {v6, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 562
    const/4 v1, 0x4

    invoke-virtual {v6, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 563
    const/4 v1, 0x5

    invoke-virtual {v6, v1, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 564
    invoke-virtual {p0, p1}, Lcom/google/android/location/c/o;->a(I)I

    move-result v1

    .line 565
    if-ltz v1, :cond_5d

    .line 566
    const/16 v2, 0xa

    invoke-virtual {v6, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 568
    :cond_5d
    const/4 v1, 0x1

    invoke-virtual {v5, v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 569
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/location/c/o;->i:J

    sub-long v2, p6, v2

    invoke-virtual {v5, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 571
    if-eqz p5, :cond_db

    .line 572
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_db

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/NeighboringCellInfo;

    .line 573
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/android/location/j/a;->ae:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 574
    const/4 v4, 0x1

    invoke-virtual {v1}, Landroid/telephony/NeighboringCellInfo;->getLac()I

    move-result v6

    invoke-virtual {v3, v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 575
    const/4 v4, 0x2

    invoke-virtual {v1}, Landroid/telephony/NeighboringCellInfo;->getCid()I

    move-result v6

    invoke-virtual {v3, v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 576
    const/16 v4, 0x8

    invoke-virtual {v1}, Landroid/telephony/NeighboringCellInfo;->getPsc()I

    move-result v6

    invoke-virtual {v3, v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 577
    const/4 v4, 0x5

    invoke-virtual {v1}, Landroid/telephony/NeighboringCellInfo;->getRssi()I

    move-result v1

    invoke-virtual {v3, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 578
    const/4 v1, 0x3

    invoke-virtual {v5, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_a7
    .catchall {:try_start_b .. :try_end_a7} :catchall_a8

    goto :goto_6f

    .line 535
    :catchall_a8
    move-exception v1

    monitor-exit p0

    throw v1

    .line 549
    :cond_ab
    :try_start_ab
    instance-of v2, p3, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v2, :cond_bf

    .line 550
    move-object v0, p3

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v2

    .line 551
    check-cast p3, Landroid/telephony/gsm/GsmCellLocation;

    invoke-virtual {p3}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v1

    goto/16 :goto_30

    .line 552
    :cond_bf
    instance-of v2, p3, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v2, :cond_e6

    .line 553
    move-object v0, p3

    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/telephony/cdma/CdmaCellLocation;->getSystemId()I

    move-result v3

    .line 554
    move-object v0, p3

    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v2

    .line 555
    check-cast p3, Landroid/telephony/cdma/CdmaCellLocation;

    invoke-virtual {p3}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    move-result v1

    goto/16 :goto_30

    .line 581
    :cond_db
    iget-object v1, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 583
    invoke-direct {p0}, Lcom/google/android/location/c/o;->c()V
    :try_end_e4
    .catchall {:try_start_ab .. :try_end_e4} :catchall_a8

    goto/16 :goto_7

    :cond_e6
    move v2, v1

    goto/16 :goto_30

    :cond_e9
    move v3, v1

    move v4, v1

    goto/16 :goto_2d
.end method

.method public declared-synchronized a(Landroid/location/GpsStatus;J)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 468
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/o;->g:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_59

    if-eqz v0, :cond_7

    .line 491
    :cond_5
    :goto_5
    monitor-exit p0

    return-void

    .line 472
    :cond_7
    if-eqz p1, :cond_5

    .line 477
    :try_start_9
    invoke-virtual {p1}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v0

    .line 478
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->av:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 479
    const/4 v2, 0x1

    iget-wide v3, p0, Lcom/google/android/location/c/o;->i:J

    sub-long v3, p2, v3

    long-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 481
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_21
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/GpsSatellite;

    .line 482
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/android/location/j/a;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 483
    const/4 v4, 0x4

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->getAzimuth()F

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 484
    const/4 v4, 0x3

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->getElevation()F

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 485
    const/4 v4, 0x1

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->getPrn()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 486
    const/4 v4, 0x2

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->getSnr()F

    move-result v0

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 487
    const/4 v0, 0x2

    invoke-virtual {v1, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_58
    .catchall {:try_start_9 .. :try_end_58} :catchall_59

    goto :goto_21

    .line 468
    :catchall_59
    move-exception v0

    monitor-exit p0

    throw v0

    .line 489
    :cond_5c
    :try_start_5c
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0xa

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 490
    invoke-direct {p0}, Lcom/google/android/location/c/o;->c()V
    :try_end_66
    .catchall {:try_start_5c .. :try_end_66} :catchall_59

    goto :goto_5
.end method

.method public declared-synchronized a(Ljava/util/List;J)V
    .registers 12
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 501
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/o;->g:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_55

    if-eqz v0, :cond_7

    .line 524
    :cond_5
    :goto_5
    monitor-exit p0

    return-void

    .line 505
    :cond_7
    if-eqz p1, :cond_5

    :try_start_9
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 508
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->L:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 509
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1a
    :goto_1a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_58

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    .line 510
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/android/location/j/a;->ac:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 511
    iget-object v4, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/location/c/L;->c(Ljava/lang/String;)J

    move-result-wide v4

    .line 512
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-eqz v6, :cond_1a

    .line 515
    const/4 v6, 0x1

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 516
    const/16 v6, 0x8

    invoke-virtual {v3, v6, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 517
    const/16 v4, 0x9

    iget v5, v0, Landroid/net/wifi/ScanResult;->level:I

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 518
    iget v0, v0, Landroid/net/wifi/ScanResult;->frequency:I

    invoke-static {v3, v0}, Lcom/google/android/location/c/L;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 519
    const/4 v0, 0x2

    invoke-virtual {v1, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_54
    .catchall {:try_start_9 .. :try_end_54} :catchall_55

    goto :goto_1a

    .line 501
    :catchall_55
    move-exception v0

    monitor-exit p0

    throw v0

    .line 521
    :cond_58
    const/4 v0, 0x1

    :try_start_59
    iget-wide v2, p0, Lcom/google/android/location/c/o;->i:J

    sub-long v2, p2, v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 522
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x7

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 523
    invoke-direct {p0}, Lcom/google/android/location/c/o;->c()V
    :try_end_69
    .catchall {:try_start_59 .. :try_end_69} :catchall_55

    goto :goto_5
.end method

.method public declared-synchronized a(Landroid/location/Location;J)Z
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const-wide v5, 0x416312d000000000L

    .line 436
    monitor-enter p0

    if-nez p1, :cond_b

    .line 457
    :goto_9
    monitor-exit p0

    return v0

    .line 439
    :cond_b
    :try_start_b
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->Q:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 440
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->x:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 441
    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 442
    const/4 v2, 0x2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 443
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 444
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_40

    .line 445
    const/4 v1, 0x3

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 447
    :cond_40
    invoke-virtual {p1}, Landroid/location/Location;->hasAltitude()Z

    move-result v1

    if-eqz v1, :cond_50

    .line 448
    const/16 v1, 0xa

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 450
    :cond_50
    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    if-eqz v1, :cond_60

    .line 451
    const/16 v1, 0xd

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 453
    :cond_60
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v1

    if-eqz v1, :cond_6f

    .line 454
    const/16 v1, 0x10

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 456
    :cond_6f
    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 457
    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/location/c/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z
    :try_end_78
    .catchall {:try_start_b .. :try_end_78} :catchall_7a

    move-result v0

    goto :goto_9

    .line 436
    :catchall_7a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 4
    .parameter

    .prologue
    .line 409
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/o;->g:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_13

    if-eqz v0, :cond_8

    .line 411
    const/4 v0, 0x0

    .line 415
    :goto_6
    monitor-exit p0

    return v0

    .line 413
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/google/android/location/c/o;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 414
    invoke-direct {p0}, Lcom/google/android/location/c/o;->c()V
    :try_end_11
    .catchall {:try_start_8 .. :try_end_11} :catchall_13

    .line 415
    const/4 v0, 0x1

    goto :goto_6

    .line 409
    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 425
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/o;->g:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_1c

    if-eqz v0, :cond_8

    .line 427
    const/4 v0, 0x0

    .line 432
    :goto_6
    monitor-exit p0

    return v0

    .line 429
    :cond_8
    const/4 v0, 0x6

    :try_start_9
    iget-wide v1, p0, Lcom/google/android/location/c/o;->i:J

    sub-long v1, p2, v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 430
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 431
    invoke-direct {p0}, Lcom/google/android/location/c/o;->c()V
    :try_end_1a
    .catchall {:try_start_9 .. :try_end_1a} :catchall_1c

    .line 432
    const/4 v0, 0x1

    goto :goto_6

    .line 425
    :catchall_1c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized b()V
    .registers 4

    .prologue
    .line 285
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-gtz v0, :cond_55

    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-gtz v0, :cond_55

    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-gtz v0, :cond_55

    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-gtz v0, :cond_55

    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-gtz v0, :cond_55

    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-gtz v0, :cond_55

    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-gtz v0, :cond_55

    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-gtz v0, :cond_55

    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lez v0, :cond_5e

    .line 294
    :cond_55
    iget-object v0, p0, Lcom/google/android/location/c/o;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 297
    :cond_5e
    iget-object v0, p0, Lcom/google/android/location/c/o;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDataSize()I

    move-result v0

    if-eqz v0, :cond_6e

    .line 298
    iget-object v0, p0, Lcom/google/android/location/c/o;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/c/o;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_6e
    .catchall {:try_start_1 .. :try_end_6e} :catchall_70

    .line 300
    :cond_6e
    monitor-exit p0

    return-void

    .line 285
    :catchall_70
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(FFFIJJ)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 731
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/o;->g:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_79

    if-eqz v0, :cond_7

    .line 771
    :goto_5
    monitor-exit p0

    return-void

    .line 735
    :cond_7
    :try_start_7
    invoke-direct {p0, p5, p6, p7, p8}, Lcom/google/android/location/c/o;->a(JJ)V

    .line 736
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->af:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    const/16 v1, 0x8

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;I)V

    .line 737
    const/4 v0, 0x1

    invoke-virtual {v2, v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 738
    const/4 v0, 0x2

    invoke-virtual {v2, v0, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 739
    const/4 v0, 0x3

    invoke-virtual {v2, v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 740
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_32

    .line 742
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-direct {p0, p5, p6}, Lcom/google/android/location/c/o;->a(J)J

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 746
    :cond_32
    iget-wide v0, p0, Lcom/google/android/location/c/o;->u:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-nez v0, :cond_7c

    .line 747
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    .line 748
    invoke-direct {p0, p5, p6}, Lcom/google/android/location/c/o;->a(J)J

    move-result-wide v5

    .line 749
    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/location/c/o;->b(JJ)I

    move-result v1

    .line 750
    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/location/c/o;->c(JJ)I

    move-result v0

    .line 755
    :goto_4d
    if-eqz v1, :cond_53

    .line 756
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 758
    :cond_53
    if-eqz v0, :cond_5a

    .line 759
    const/16 v1, 0x8

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 761
    :cond_5a
    iput-wide p5, p0, Lcom/google/android/location/c/o;->u:J

    .line 764
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-eqz v0, :cond_69

    iget v0, p0, Lcom/google/android/location/c/o;->p:I

    if-eq v0, p4, :cond_6f

    .line 766
    :cond_69
    const/4 v0, 0x4

    invoke-virtual {v2, v0, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 767
    iput p4, p0, Lcom/google/android/location/c/o;->p:I

    .line 769
    :cond_6f
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 770
    invoke-direct {p0}, Lcom/google/android/location/c/o;->c()V
    :try_end_78
    .catchall {:try_start_7 .. :try_end_78} :catchall_79

    goto :goto_5

    .line 731
    :catchall_79
    move-exception v0

    monitor-exit p0

    throw v0

    .line 752
    :cond_7c
    :try_start_7c
    iget-wide v0, p0, Lcom/google/android/location/c/o;->u:J

    invoke-direct {p0, v0, v1, p5, p6}, Lcom/google/android/location/c/o;->b(JJ)I

    move-result v1

    .line 753
    iget-wide v3, p0, Lcom/google/android/location/c/o;->u:J

    invoke-direct {p0, v3, v4, p5, p6}, Lcom/google/android/location/c/o;->c(JJ)I
    :try_end_87
    .catchall {:try_start_7c .. :try_end_87} :catchall_79

    move-result v0

    goto :goto_4d
.end method

.method public declared-synchronized c(FFFIJJ)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 784
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/o;->g:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_7a

    if-eqz v0, :cond_7

    .line 824
    :goto_5
    monitor-exit p0

    return-void

    .line 788
    :cond_7
    :try_start_7
    invoke-direct {p0, p5, p6, p7, p8}, Lcom/google/android/location/c/o;->a(JJ)V

    .line 789
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->D:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    const/16 v1, 0x9

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;I)V

    .line 790
    const/4 v0, 0x1

    invoke-virtual {v2, v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 791
    const/4 v0, 0x2

    invoke-virtual {v2, v0, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 792
    const/4 v0, 0x3

    invoke-virtual {v2, v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 793
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_32

    .line 795
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-direct {p0, p5, p6}, Lcom/google/android/location/c/o;->a(J)J

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 799
    :cond_32
    iget-wide v0, p0, Lcom/google/android/location/c/o;->w:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-nez v0, :cond_7d

    .line 800
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    .line 801
    invoke-direct {p0, p5, p6}, Lcom/google/android/location/c/o;->a(J)J

    move-result-wide v5

    .line 802
    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/location/c/o;->b(JJ)I

    move-result v1

    .line 803
    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/location/c/o;->c(JJ)I

    move-result v0

    .line 808
    :goto_4d
    if-eqz v1, :cond_54

    .line 809
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 811
    :cond_54
    if-eqz v0, :cond_5b

    .line 812
    const/16 v1, 0x9

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 814
    :cond_5b
    iput-wide p5, p0, Lcom/google/android/location/c/o;->w:J

    .line 817
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-eqz v0, :cond_6a

    iget v0, p0, Lcom/google/android/location/c/o;->r:I

    if-eq v0, p4, :cond_70

    .line 819
    :cond_6a
    const/4 v0, 0x4

    invoke-virtual {v2, v0, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 820
    iput p4, p0, Lcom/google/android/location/c/o;->r:I

    .line 822
    :cond_70
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 823
    invoke-direct {p0}, Lcom/google/android/location/c/o;->c()V
    :try_end_79
    .catchall {:try_start_7 .. :try_end_79} :catchall_7a

    goto :goto_5

    .line 784
    :catchall_7a
    move-exception v0

    monitor-exit p0

    throw v0

    .line 805
    :cond_7d
    :try_start_7d
    iget-wide v0, p0, Lcom/google/android/location/c/o;->w:J

    invoke-direct {p0, v0, v1, p5, p6}, Lcom/google/android/location/c/o;->b(JJ)I

    move-result v1

    .line 806
    iget-wide v3, p0, Lcom/google/android/location/c/o;->w:J

    invoke-direct {p0, v3, v4, p5, p6}, Lcom/google/android/location/c/o;->c(JJ)I
    :try_end_88
    .catchall {:try_start_7d .. :try_end_88} :catchall_7a

    move-result v0

    goto :goto_4d
.end method

.method public declared-synchronized d(FFFIJJ)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 837
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/o;->g:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_79

    if-eqz v0, :cond_7

    .line 877
    :goto_5
    monitor-exit p0

    return-void

    .line 841
    :cond_7
    :try_start_7
    invoke-direct {p0, p5, p6, p7, p8}, Lcom/google/android/location/c/o;->a(JJ)V

    .line 842
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->aa:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    const/16 v1, 0x8

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;I)V

    .line 843
    const/4 v0, 0x1

    invoke-virtual {v2, v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 844
    const/4 v0, 0x2

    invoke-virtual {v2, v0, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 845
    const/4 v0, 0x3

    invoke-virtual {v2, v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 846
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_32

    .line 848
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-direct {p0, p5, p6}, Lcom/google/android/location/c/o;->a(J)J

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 852
    :cond_32
    iget-wide v0, p0, Lcom/google/android/location/c/o;->x:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-nez v0, :cond_7c

    .line 853
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    .line 854
    invoke-direct {p0, p5, p6}, Lcom/google/android/location/c/o;->a(J)J

    move-result-wide v5

    .line 855
    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/location/c/o;->b(JJ)I

    move-result v1

    .line 856
    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/location/c/o;->c(JJ)I

    move-result v0

    .line 861
    :goto_4d
    if-eqz v1, :cond_53

    .line 862
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 864
    :cond_53
    if-eqz v0, :cond_5a

    .line 865
    const/16 v1, 0x8

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 867
    :cond_5a
    iput-wide p5, p0, Lcom/google/android/location/c/o;->x:J

    .line 870
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-eqz v0, :cond_69

    iget v0, p0, Lcom/google/android/location/c/o;->s:I

    if-eq v0, p4, :cond_6f

    .line 872
    :cond_69
    const/4 v0, 0x4

    invoke-virtual {v2, v0, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 873
    iput p4, p0, Lcom/google/android/location/c/o;->s:I

    .line 875
    :cond_6f
    iget-object v0, p0, Lcom/google/android/location/c/o;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 876
    invoke-direct {p0}, Lcom/google/android/location/c/o;->c()V
    :try_end_78
    .catchall {:try_start_7 .. :try_end_78} :catchall_79

    goto :goto_5

    .line 837
    :catchall_79
    move-exception v0

    monitor-exit p0

    throw v0

    .line 858
    :cond_7c
    :try_start_7c
    iget-wide v0, p0, Lcom/google/android/location/c/o;->x:J

    invoke-direct {p0, v0, v1, p5, p6}, Lcom/google/android/location/c/o;->b(JJ)I

    move-result v1

    .line 859
    iget-wide v3, p0, Lcom/google/android/location/c/o;->x:J

    invoke-direct {p0, v3, v4, p5, p6}, Lcom/google/android/location/c/o;->c(JJ)I
    :try_end_87
    .catchall {:try_start_7c .. :try_end_87} :catchall_79

    move-result v0

    goto :goto_4d
.end method
