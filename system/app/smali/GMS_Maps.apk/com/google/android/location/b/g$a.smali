.class public Lcom/google/android/location/b/g$a;
.super Ljava/util/LinkedHashMap;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/b/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedHashMap",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/location/b/h;


# direct methods
.method public constructor <init>(ILcom/google/android/location/b/h;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 700
    const/high16 v0, 0x3f40

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    .line 701
    iput p1, p0, Lcom/google/android/location/b/g$a;->a:I

    .line 702
    iput-object p2, p0, Lcom/google/android/location/b/g$a;->b:Lcom/google/android/location/b/h;

    .line 703
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/b/g$a;)I
    .registers 2
    .parameter

    .prologue
    .line 695
    iget v0, p0, Lcom/google/android/location/b/g$a;->a:I

    return v0
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 707
    invoke-virtual {p0}, Lcom/google/android/location/b/g$a;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/location/b/g$a;->a:I

    if-le v0, v1, :cond_11

    const/4 v0, 0x1

    .line 708
    :goto_9
    if-eqz v0, :cond_10

    .line 709
    iget-object v1, p0, Lcom/google/android/location/b/g$a;->b:Lcom/google/android/location/b/h;

    invoke-virtual {v1}, Lcom/google/android/location/b/h;->a()V

    .line 711
    :cond_10
    return v0

    .line 707
    :cond_11
    const/4 v0, 0x0

    goto :goto_9
.end method
