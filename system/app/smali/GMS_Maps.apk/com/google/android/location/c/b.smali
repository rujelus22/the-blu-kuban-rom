.class public Lcom/google/android/location/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/b$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:J

.field private c:Ljava/io/DataInputStream;

.field private d:I

.field private e:Z

.field private f:Ljava/lang/Integer;

.field private final g:Lcom/google/android/location/c/a;

.field private final h:Lcom/google/android/location/c/a;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/location/c/a;Lcom/google/android/location/c/a;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/c/b;->b:J

    .line 27
    iput v2, p0, Lcom/google/android/location/c/b;->d:I

    .line 28
    iput-boolean v2, p0, Lcom/google/android/location/c/b;->e:Z

    .line 46
    iput-object p1, p0, Lcom/google/android/location/c/b;->a:Ljava/lang/String;

    .line 47
    iput-object p2, p0, Lcom/google/android/location/c/b;->g:Lcom/google/android/location/c/a;

    .line 48
    iput-object p3, p0, Lcom/google/android/location/c/b;->h:Lcom/google/android/location/c/a;

    .line 49
    return-void
.end method

.method private a(Ljava/io/DataInputStream;I)[B
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 132
    new-array v1, p2, [B

    .line 133
    const/4 v0, 0x0

    .line 135
    :cond_3
    sub-int v2, p2, v0

    invoke-virtual {p1, v1, v0, v2}, Ljava/io/DataInputStream;->read([BII)I

    move-result v2

    if-ltz v2, :cond_e

    .line 136
    add-int/2addr v0, v2

    .line 137
    if-ne v0, p2, :cond_3

    .line 141
    :cond_e
    if-eq v0, p2, :cond_1b

    .line 142
    invoke-direct {p0}, Lcom/google/android/location/c/b;->d()V

    .line 143
    new-instance v0, Lcom/google/android/location/c/b$a;

    const-string v1, "Unexpected end of file."

    invoke-direct {v0, v1}, Lcom/google/android/location/c/b$a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_1b
    return-object v1
.end method

.method private c()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 154
    iget-wide v0, p0, Lcom/google/android/location/c/b;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_3d

    .line 155
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/c/b;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 156
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_29

    .line 157
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Could not found file %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/location/c/b;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_29
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 160
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v2, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    .line 161
    iput-boolean v4, p0, Lcom/google/android/location/c/b;->e:Z

    .line 162
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/c/b;->b:J

    .line 164
    :cond_3d
    return-void
.end method

.method private d()V
    .registers 2

    .prologue
    .line 183
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/c/b;->b()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_3} :catch_4

    .line 186
    :goto_3
    return-void

    .line 184
    :catch_4
    move-exception v0

    goto :goto_3
.end method


# virtual methods
.method public declared-synchronized a()[B
    .registers 8

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x0

    .line 67
    monitor-enter p0

    :try_start_3
    invoke-direct {p0}, Lcom/google/android/location/c/b;->c()V

    .line 68
    iget-boolean v1, p0, Lcom/google/android/location/c/b;->e:Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_4e

    if-eqz v1, :cond_c

    .line 111
    :cond_a
    :goto_a
    monitor-exit p0

    return-object v0

    .line 73
    :cond_c
    :try_start_c
    iget-object v1, p0, Lcom/google/android/location/c/b;->f:Ljava/lang/Integer;

    if-nez v1, :cond_51

    .line 74
    iget-object v1, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/c/b;->f:Ljava/lang/Integer;

    .line 75
    iget v1, p0, Lcom/google/android/location/c/b;->d:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/android/location/c/b;->d:I

    .line 76
    iget v1, p0, Lcom/google/android/location/c/b;->d:I

    int-to-long v1, v1

    iget-wide v3, p0, Lcom/google/android/location/c/b;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_51

    .line 78
    invoke-direct {p0}, Lcom/google/android/location/c/b;->d()V
    :try_end_2e
    .catchall {:try_start_c .. :try_end_2e} :catchall_4e
    .catch Ljava/io/EOFException; {:try_start_c .. :try_end_2e} :catch_2f
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_2e} :catch_7d

    goto :goto_a

    .line 112
    :catch_2f
    move-exception v0

    .line 113
    :try_start_30
    invoke-direct {p0}, Lcom/google/android/location/c/b;->d()V

    .line 114
    new-instance v0, Lcom/google/android/location/c/b$a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected end of file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/c/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/location/c/b$a;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4e
    .catchall {:try_start_30 .. :try_end_4e} :catchall_4e

    .line 67
    :catchall_4e
    move-exception v0

    monitor-exit p0

    throw v0

    .line 83
    :cond_51
    const/4 v1, 0x0

    .line 84
    :try_start_52
    iget-object v2, p0, Lcom/google/android/location/c/b;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v5, :cond_82

    .line 86
    iget-object v0, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    .line 87
    iget-object v1, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    invoke-direct {p0, v1, v0}, Lcom/google/android/location/c/b;->a(Ljava/io/DataInputStream;I)[B

    move-result-object v1

    move v6, v0

    move-object v0, v1

    move v1, v6

    .line 106
    :cond_6b
    :goto_6b
    iget v2, p0, Lcom/google/android/location/c/b;->d:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/location/c/b;->d:I

    .line 107
    iget v1, p0, Lcom/google/android/location/c/b;->d:I

    int-to-long v1, v1

    iget-wide v3, p0, Lcom/google/android/location/c/b;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_a

    .line 109
    invoke-direct {p0}, Lcom/google/android/location/c/b;->d()V
    :try_end_7c
    .catchall {:try_start_52 .. :try_end_7c} :catchall_4e
    .catch Ljava/io/EOFException; {:try_start_52 .. :try_end_7c} :catch_2f
    .catch Ljava/io/IOException; {:try_start_52 .. :try_end_7c} :catch_7d

    goto :goto_a

    .line 115
    :catch_7d
    move-exception v0

    .line 116
    :try_start_7e
    invoke-direct {p0}, Lcom/google/android/location/c/b;->d()V

    .line 117
    throw v0
    :try_end_82
    .catchall {:try_start_7e .. :try_end_82} :catchall_4e

    .line 88
    :cond_82
    :try_start_82
    iget-object v2, p0, Lcom/google/android/location/c/b;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt v2, v5, :cond_6b

    .line 91
    iget-object v0, p0, Lcom/google/android/location/c/b;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_b2

    iget-object v0, p0, Lcom/google/android/location/c/b;->h:Lcom/google/android/location/c/a;

    .line 93
    :goto_95
    if-nez v0, :cond_b5

    .line 94
    new-instance v0, Lcom/google/android/location/c/b$a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to decrypt GLocRequest: no cipher for version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/c/b;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/location/c/b$a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_b2
    iget-object v0, p0, Lcom/google/android/location/c/b;->g:Lcom/google/android/location/c/a;
    :try_end_b4
    .catchall {:try_start_82 .. :try_end_b4} :catchall_4e
    .catch Ljava/io/EOFException; {:try_start_82 .. :try_end_b4} :catch_2f
    .catch Ljava/io/IOException; {:try_start_82 .. :try_end_b4} :catch_7d

    goto :goto_95

    .line 98
    :cond_b5
    :try_start_b5
    iget-object v1, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/e/u;

    move-result-object v1

    .line 99
    iget-object v0, v1, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, [B

    .line 100
    iget-object v1, v1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 101
    invoke-static {v0}, Lcom/google/android/location/c/L;->b([B)[B
    :try_end_ca
    .catchall {:try_start_b5 .. :try_end_ca} :catchall_4e
    .catch Ljava/io/IOException; {:try_start_b5 .. :try_end_ca} :catch_cc
    .catch Ljava/io/EOFException; {:try_start_b5 .. :try_end_ca} :catch_2f

    move-result-object v0

    goto :goto_6b

    .line 102
    :catch_cc
    move-exception v0

    .line 103
    :try_start_cd
    new-instance v1, Lcom/google/android/location/c/b$a;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/location/c/b$a;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_d7
    .catchall {:try_start_cd .. :try_end_d7} :catchall_4e
    .catch Ljava/io/EOFException; {:try_start_cd .. :try_end_d7} :catch_2f
    .catch Ljava/io/IOException; {:try_start_cd .. :try_end_d7} :catch_7d
.end method

.method public b()V
    .registers 2

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/google/android/location/c/b;->e:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    if-eqz v0, :cond_10

    .line 173
    iget-object v0, p0, Lcom/google/android/location/c/b;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/b;->e:Z

    .line 176
    :cond_10
    return-void
.end method
