.class public Lcom/google/android/location/b/k;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/b/k$a;,
        Lcom/google/android/location/b/k$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/location/b/k$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/k$b",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/android/location/b/k$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/k$b",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 31
    invoke-static {}, Lcom/google/android/location/b/k;->a()Lcom/google/android/location/b/k$b;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/b/k;->a:Lcom/google/android/location/b/k$b;

    .line 34
    invoke-static {}, Lcom/google/android/location/b/k;->b()Lcom/google/android/location/b/k$b;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/b/k;->b:Lcom/google/android/location/b/k$b;

    return-void
.end method

.method private static a()Lcom/google/android/location/b/k$b;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/b/k$b",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 168
    new-instance v0, Lcom/google/android/location/b/k$1;

    sget-object v2, Lcom/google/android/location/j/a;->ao:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    sget-object v3, Lcom/google/android/location/j/a;->aj:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    const/4 v5, 0x3

    const/4 v6, 0x4

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/b/k$1;-><init>(ILcom/google/googlenav/common/io/protocol/ProtoBufType;Lcom/google/googlenav/common/io/protocol/ProtoBufType;III)V

    return-object v0
.end method

.method private static b()Lcom/google/android/location/b/k$b;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/b/k$b",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 199
    new-instance v0, Lcom/google/android/location/b/k$2;

    sget-object v2, Lcom/google/android/location/j/a;->T:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    sget-object v3, Lcom/google/android/location/j/a;->am:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    const/4 v5, 0x3

    const/4 v6, 0x4

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/b/k$2;-><init>(ILcom/google/googlenav/common/io/protocol/ProtoBufType;Lcom/google/googlenav/common/io/protocol/ProtoBufType;III)V

    return-object v0
.end method
