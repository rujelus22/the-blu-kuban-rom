.class Lcom/google/android/location/a/m;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/a/m$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 5

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 42
    sget-object v1, Lcom/google/android/location/a/m$a;->c:Lcom/google/android/location/a/m$a;

    const-wide v2, 0x4061c4fdf3b645a2L

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v1, Lcom/google/android/location/a/m$a;->e:Lcom/google/android/location/a/m$a;

    const-wide v2, 0x40591cfdf3b645a2L

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v1, Lcom/google/android/location/a/m$a;->f:Lcom/google/android/location/a/m$a;

    const-wide v2, 0x4065e7e76c8b4396L

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v1, Lcom/google/android/location/a/m$a;->g:Lcom/google/android/location/a/m$a;

    const-wide v2, 0x405b5428f5c28f5cL

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v1, Lcom/google/android/location/a/m$a;->h:Lcom/google/android/location/a/m$a;

    const-wide v2, 0x40709e9374bc6a7fL

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/a/m;->a:Ljava/util/Map;

    .line 48
    return-void
.end method

.method private a(Ljava/util/Map;Lcom/google/android/location/a/m$a;)D
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ">;",
            "Lcom/google/android/location/a/m$a;",
            ")D"
        }
    .end annotation

    .prologue
    .line 91
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 92
    if-nez v0, :cond_10

    .line 93
    iget-object v0, p0, Lcom/google/android/location/a/m;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 95
    :cond_10
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method private b(Ljava/util/Map;)Z
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 69
    sget-object v0, Lcom/google/android/location/a/m$a;->d:Lcom/google/android/location/a/m$a;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_20

    sget-object v0, Lcom/google/android/location/a/m$a;->c:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/m;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;)D

    move-result-wide v0

    const-wide/high16 v2, 0x4079

    cmpl-double v0, v0, v2

    if-lez v0, :cond_20

    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method private c(Ljava/util/Map;)D
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ">;)D"
        }
    .end annotation

    .prologue
    const-wide/high16 v6, 0x3ff0

    .line 74
    const-wide v0, -0x3fff4504816f0069L

    const-wide v2, 0x3ffffe5c91d14e3cL

    sget-object v4, Lcom/google/android/location/a/m$a;->a:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, p1, v4}, Lcom/google/android/location/a/m;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, 0x3fd1013a92a30553L

    sget-object v4, Lcom/google/android/location/a/m$a;->b:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, p1, v4}, Lcom/google/android/location/a/m;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, -0x409c56d5cfaacd9fL

    sget-object v4, Lcom/google/android/location/a/m$a;->c:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, p1, v4}, Lcom/google/android/location/a/m;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, 0x40158b0f27bb2fecL

    sget-object v4, Lcom/google/android/location/a/m$a;->d:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, p1, v4}, Lcom/google/android/location/a/m;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, -0x40876c8b43958106L

    sget-object v4, Lcom/google/android/location/a/m$a;->e:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, p1, v4}, Lcom/google/android/location/a/m;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, -0x408353f7ced91687L

    sget-object v4, Lcom/google/android/location/a/m$a;->f:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, p1, v4}, Lcom/google/android/location/a/m;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, -0x406978d4fdf3b646L

    sget-object v4, Lcom/google/android/location/a/m$a;->g:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, p1, v4}, Lcom/google/android/location/a/m;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, -0x40bf9db22d0e5604L

    sget-object v4, Lcom/google/android/location/a/m$a;->h:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, p1, v4}, Lcom/google/android/location/a/m;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    .line 83
    const-wide v2, 0x4005bf0a8b145769L

    neg-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    add-double/2addr v0, v6

    div-double v0, v6, v0

    return-wide v0
.end method


# virtual methods
.method public a(Ljava/util/Map;)Lcom/google/android/location/a/n$b;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ">;)",
            "Lcom/google/android/location/a/n$b;"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/location/a/m;->b(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 53
    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    .line 59
    :goto_8
    return-object v0

    .line 55
    :cond_9
    invoke-direct {p0, p1}, Lcom/google/android/location/a/m;->c(Ljava/util/Map;)D

    move-result-wide v1

    .line 56
    const-wide/high16 v3, 0x3fe0

    cmpg-double v0, v1, v3

    if-gez v0, :cond_1f

    .line 57
    new-instance v0, Lcom/google/android/location/a/n$b;

    sget-object v3, Lcom/google/android/location/e/B;->b:Lcom/google/android/location/e/B;

    const-wide/high16 v4, 0x3ff0

    sub-double v1, v4, v1

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/location/a/n$b;-><init>(Lcom/google/android/location/e/B;D)V

    goto :goto_8

    .line 59
    :cond_1f
    new-instance v0, Lcom/google/android/location/a/n$b;

    sget-object v3, Lcom/google/android/location/e/B;->a:Lcom/google/android/location/e/B;

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/location/a/n$b;-><init>(Lcom/google/android/location/e/B;D)V

    goto :goto_8
.end method
