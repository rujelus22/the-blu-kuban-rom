.class public Lcom/google/android/location/f;
.super Lcom/google/android/location/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/f$1;,
        Lcom/google/android/location/f$a;
    }
.end annotation


# instance fields
.field g:J

.field h:J

.field i:J

.field private final j:Lcom/google/android/location/t;

.field private final k:Lcom/google/android/location/g;

.field private l:Lcom/google/android/location/c/r;

.field private m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private n:I

.field private o:I


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/t;Lcom/google/android/location/g;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 118
    const-string v1, "CalibrationCollector"

    sget-object v6, Lcom/google/android/location/a$c;->a:Lcom/google/android/location/a$c;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V

    .line 89
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/f;->g:J

    .line 93
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/f;->i:J

    .line 119
    iput-object p6, p0, Lcom/google/android/location/f;->j:Lcom/google/android/location/t;

    .line 120
    iput-object p5, p0, Lcom/google/android/location/f;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 121
    iput-object p7, p0, Lcom/google/android/location/f;->k:Lcom/google/android/location/g;

    .line 122
    invoke-direct {p0, p5}, Lcom/google/android/location/f;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/f;->g:J

    .line 123
    invoke-direct {p0}, Lcom/google/android/location/f;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/f;->h:J

    .line 124
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/f;)I
    .registers 2
    .parameter

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/location/f;->n:I

    return v0
.end method

.method static a(JLjava/util/List;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 13
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x2

    const-wide v7, 0x3ef4f8b588e368f1L

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 587
    invoke-static {p2}, Lcom/google/android/location/f;->a(Ljava/util/List;)[[D

    move-result-object v1

    .line 588
    if-nez v1, :cond_10

    .line 606
    :cond_f
    :goto_f
    return-object v0

    .line 592
    :cond_10
    aget-object v2, v1, v5

    aget-wide v2, v2, v6

    cmpg-double v2, v2, v7

    if-gez v2, :cond_f

    aget-object v2, v1, v5

    aget-wide v2, v2, v5

    cmpg-double v2, v2, v7

    if-gez v2, :cond_f

    aget-object v2, v1, v5

    aget-wide v2, v2, v9

    cmpg-double v2, v2, v7

    if-gez v2, :cond_f

    .line 596
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->X:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 597
    aget-object v0, v1, v6

    aget-wide v3, v0, v6

    double-to-float v0, v3

    invoke-virtual {v2, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 598
    aget-object v0, v1, v6

    aget-wide v3, v0, v5

    double-to-float v0, v3

    invoke-virtual {v2, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 599
    const/4 v0, 0x3

    aget-object v1, v1, v6

    aget-wide v3, v1, v9

    double-to-float v1, v3

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 600
    const/4 v0, 0x4

    invoke-virtual {v2, v0, p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 601
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->m:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 602
    invoke-virtual {v0, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_f
.end method

.method static synthetic a(Lcom/google/android/location/f;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/location/f;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p1
.end method

.method static a(Lcom/google/android/location/os/i;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 131
    invoke-interface {p0}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v1

    .line 132
    new-instance v2, Ljava/io/File;

    const-string v3, "calibration"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 133
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_13

    .line 144
    :goto_12
    return-object v0

    .line 138
    :cond_13
    :try_start_13
    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 139
    sget-object v1, Lcom/google/android/location/j/a;->m:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v3, v1}, Lcom/google/android/location/k/c;->a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 140
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_26} :catch_28

    move-object v0, v1

    .line 141
    goto :goto_12

    .line 142
    :catch_28
    move-exception v1

    goto :goto_12
.end method

.method private a(II)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 228
    iput p1, p0, Lcom/google/android/location/f;->n:I

    .line 229
    iput p2, p0, Lcom/google/android/location/f;->o:I

    .line 231
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 232
    sget-object v0, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    sget-object v0, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    iget-object v0, p0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/location/c/F;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    aput-object v4, v1, v3

    invoke-static {v1}, Lcom/google/android/location/c/F;->a([Lcom/google/android/location/c/F;)Ljava/util/Set;

    move-result-object v1

    const-wide/16 v3, 0x3a98

    new-instance v5, Lcom/google/android/location/f$a;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/google/android/location/f$a;-><init>(Lcom/google/android/location/f;Lcom/google/android/location/f$1;)V

    iget-object v6, p0, Lcom/google/android/location/f;->a:Ljava/lang/String;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/i;->a(Ljava/util/Set;Ljava/util/Map;JLcom/google/android/location/c/l;Ljava/lang/String;)Lcom/google/android/location/c/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/f;->l:Lcom/google/android/location/c/r;

    .line 237
    iget-object v0, p0, Lcom/google/android/location/f;->l:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->a()V

    .line 238
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/f;J)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/f;->j(J)V

    return-void
.end method

.method public static a(Ljava/util/List;)[[D
    .registers 34
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)[[D"
        }
    .end annotation

    .prologue
    .line 535
    if-eqz p0, :cond_d

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0xc8

    cmp-long v2, v2, v4

    if-gez v2, :cond_11

    .line 537
    :cond_d
    const/4 v2, 0x0

    check-cast v2, [[D

    .line 558
    :goto_10
    return-object v2

    .line 540
    :cond_11
    const-wide/16 v12, 0x0

    .line 541
    const-wide/16 v10, 0x0

    .line 542
    const-wide/16 v8, 0x0

    .line 543
    const-wide/16 v6, 0x0

    .line 544
    const-wide/16 v4, 0x0

    .line 545
    const-wide/16 v2, 0x0

    .line 546
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v15

    .line 547
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move-wide/from16 v21, v2

    move-wide/from16 v23, v4

    move-wide/from16 v3, v21

    move-wide/from16 v25, v6

    move-wide/from16 v5, v23

    move-wide/from16 v27, v8

    move-wide/from16 v7, v25

    move-wide/from16 v29, v10

    move-wide/from16 v9, v27

    move-wide/from16 v31, v12

    move-wide/from16 v13, v31

    move-wide/from16 v11, v29

    :goto_3d
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b5

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 548
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    add-double v13, v13, v17

    .line 549
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x4000

    invoke-static/range {v17 .. v20}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v17

    add-double v11, v11, v17

    .line 550
    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    add-double v9, v9, v17

    .line 551
    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x4000

    invoke-static/range {v17 .. v20}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v17

    add-double v7, v7, v17

    .line 552
    const/16 v17, 0x3

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    add-double v5, v5, v17

    .line 553
    const/16 v17, 0x3

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v2

    float-to-double v0, v2

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x4000

    invoke-static/range {v17 .. v20}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v17

    add-double v2, v3, v17

    move-wide v3, v2

    goto :goto_3d

    .line 555
    :cond_b5
    int-to-double v0, v15

    move-wide/from16 v16, v0

    div-double v13, v13, v16

    .line 556
    int-to-double v0, v15

    move-wide/from16 v16, v0

    div-double v9, v9, v16

    .line 557
    int-to-double v0, v15

    move-wide/from16 v16, v0

    div-double v5, v5, v16

    .line 558
    const/4 v2, 0x2

    new-array v2, v2, [[D

    const/16 v16, 0x0

    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [D

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-wide v13, v17, v18

    const/16 v18, 0x1

    aput-wide v9, v17, v18

    const/16 v18, 0x2

    aput-wide v5, v17, v18

    aput-object v17, v2, v16

    const/16 v16, 0x1

    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [D

    move-object/from16 v17, v0

    const/16 v18, 0x0

    int-to-double v0, v15

    move-wide/from16 v19, v0

    div-double v11, v11, v19

    const-wide/high16 v19, 0x4000

    move-wide/from16 v0, v19

    invoke-static {v13, v14, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v13

    sub-double/2addr v11, v13

    aput-wide v11, v17, v18

    const/4 v11, 0x1

    int-to-double v12, v15

    div-double/2addr v7, v12

    const-wide/high16 v12, 0x4000

    invoke-static {v9, v10, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    sub-double/2addr v7, v9

    aput-wide v7, v17, v11

    const/4 v7, 0x2

    int-to-double v8, v15

    div-double/2addr v3, v8

    const-wide/high16 v8, 0x4000

    invoke-static {v5, v6, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    sub-double/2addr v3, v5

    aput-wide v3, v17, v7

    aput-object v17, v2, v16

    goto/16 :goto_10
.end method

.method static synthetic b(Lcom/google/android/location/f;)I
    .registers 2
    .parameter

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/location/f;->o:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/location/f;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)J
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/location/f;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)J

    move-result-wide v0

    return-wide v0
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)J
    .registers 8
    .parameter

    .prologue
    const-wide/16 v0, -0x1

    const/4 v3, 0x1

    .line 162
    .line 163
    if-eqz p1, :cond_1e

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 165
    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v2

    .line 171
    iget-object v4, p0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-lez v4, :cond_1f

    .line 177
    :cond_1e
    :goto_1e
    return-wide v0

    :cond_1f
    move-wide v0, v2

    goto :goto_1e
.end method

.method static b(Ljava/util/List;)Ljava/lang/Boolean;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-wide v5, 0x3ef4f8b588e368f1L

    const/4 v0, 0x1

    .line 571
    invoke-static {p0}, Lcom/google/android/location/f;->a(Ljava/util/List;)[[D

    move-result-object v2

    .line 572
    if-nez v2, :cond_f

    .line 575
    const/4 v0, 0x0

    .line 579
    :goto_e
    return-object v0

    :cond_f
    aget-object v3, v2, v0

    aget-wide v3, v3, v1

    cmpg-double v3, v3, v5

    if-gez v3, :cond_2d

    aget-object v3, v2, v0

    aget-wide v3, v3, v0

    cmpg-double v3, v3, v5

    if-gez v3, :cond_2d

    aget-object v2, v2, v0

    const/4 v3, 0x2

    aget-wide v2, v2, v3

    cmpg-double v2, v2, v5

    if-gez v2, :cond_2d

    :goto_28
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_e

    :cond_2d
    move v0, v1

    goto :goto_28
.end method

.method static synthetic c(Lcom/google/android/location/f;)Lcom/google/android/location/g;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/location/f;->k:Lcom/google/android/location/g;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/f;)Lcom/google/android/location/c/r;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/location/f;->l:Lcom/google/android/location/c/r;

    return-object v0
.end method

.method private e()J
    .registers 8

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x0

    .line 265
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 269
    iget-wide v0, p0, Lcom/google/android/location/f;->g:J

    const-wide/16 v3, -0x1

    cmp-long v0, v0, v3

    if-eqz v0, :cond_1a

    iget-wide v0, p0, Lcom/google/android/location/f;->g:J

    iget-object v3, p0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    invoke-interface {v3}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v3

    cmp-long v0, v0, v3

    if-lez v0, :cond_68

    :cond_1a
    const-wide/16 v0, 0x0

    :goto_1c
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 272
    const/4 v0, 0x7

    invoke-virtual {v2, v6, v0}, Ljava/util/Calendar;->add(II)V

    .line 274
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 275
    iget-object v1, p0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 276
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3d

    .line 278
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 280
    :cond_3d
    const/16 v1, 0xb

    const/4 v3, 0x3

    invoke-virtual {v2, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 281
    const/16 v1, 0xc

    invoke-virtual {v2, v1, v5}, Ljava/util/Calendar;->set(II)V

    .line 282
    const/16 v1, 0xd

    invoke-virtual {v2, v1, v5}, Ljava/util/Calendar;->set(II)V

    .line 283
    const/16 v1, 0xe

    invoke-virtual {v2, v1, v5}, Ljava/util/Calendar;->set(II)V

    .line 285
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 287
    const/4 v0, 0x1

    invoke-virtual {v2, v6, v0}, Ljava/util/Calendar;->add(II)V

    .line 290
    :cond_5c
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 293
    return-wide v0

    .line 269
    :cond_68
    iget-wide v0, p0, Lcom/google/android/location/f;->g:J

    goto :goto_1c
.end method

.method static synthetic e(Lcom/google/android/location/f;)J
    .registers 3
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/location/f;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method private j(J)V
    .registers 5
    .parameter

    .prologue
    .line 252
    iget-wide v0, p0, Lcom/google/android/location/f;->i:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_e

    .line 253
    iput-wide p1, p0, Lcom/google/android/location/f;->i:J

    .line 255
    iget-object v0, p0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x5

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 257
    :cond_e
    return-void
.end method

.method private k(J)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 297
    iget-object v0, p0, Lcom/google/android/location/f;->j:Lcom/google/android/location/t;

    invoke-virtual {v0, v1}, Lcom/google/android/location/t;->a(Z)Lcom/google/android/location/e/u;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 298
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/f;->l(J)Z

    move-result v2

    .line 299
    if-eqz v0, :cond_19

    if-eqz v2, :cond_19

    const/4 v0, 0x1

    :goto_18
    return v0

    :cond_19
    move v0, v1

    goto :goto_18
.end method

.method private l(J)Z
    .registers 7
    .parameter

    .prologue
    .line 303
    iget-wide v0, p0, Lcom/google/android/location/f;->h:J

    sub-long v0, p1, v0

    .line 304
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_13

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-gez v0, :cond_13

    const/4 v0, 0x1

    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method


# virtual methods
.method a(I)V
    .registers 4
    .parameter

    .prologue
    .line 312
    const/4 v0, 0x5

    if-ne p1, v0, :cond_7

    .line 313
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/f;->i:J

    .line 315
    :cond_7
    return-void
.end method

.method a(IIZ)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 517
    return-void
.end method

.method a(Lcom/google/android/location/e/E;)V
    .registers 2
    .parameter

    .prologue
    .line 492
    return-void
.end method

.method a(Lcom/google/android/location/e/e;)V
    .registers 2
    .parameter

    .prologue
    .line 502
    return-void
.end method

.method a(Lcom/google/android/location/os/g;)V
    .registers 2
    .parameter

    .prologue
    .line 487
    return-void
.end method

.method a(Lcom/google/android/location/os/h;)V
    .registers 2
    .parameter

    .prologue
    .line 512
    return-void
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 482
    return-void
.end method

.method a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 497
    return-void
.end method

.method protected a(J)Z
    .registers 4
    .parameter

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/location/f;->j:Lcom/google/android/location/t;

    invoke-virtual {v0}, Lcom/google/android/location/t;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 184
    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/f;->f:Lcom/google/android/location/a$c;

    .line 185
    const/4 v0, 0x1

    .line 187
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 522
    return-void
.end method

.method protected b(J)Z
    .registers 9
    .parameter

    .prologue
    const-wide/32 v4, 0x36ee80

    const/4 v1, 0x0

    .line 192
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/f;->k(J)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 194
    sget-object v0, Lcom/google/android/location/a$c;->g:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/f;->f:Lcom/google/android/location/a$c;

    .line 197
    iget-object v0, p0, Lcom/google/android/location/f;->k:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->h()Z

    move-result v0

    if-eqz v0, :cond_2f

    iget-object v0, p0, Lcom/google/android/location/f;->k:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->f()I

    move-result v0

    :goto_1c
    iget-object v2, p0, Lcom/google/android/location/f;->k:Lcom/google/android/location/g;

    invoke-virtual {v2}, Lcom/google/android/location/g;->i()Z

    move-result v2

    if-eqz v2, :cond_2a

    iget-object v1, p0, Lcom/google/android/location/f;->k:Lcom/google/android/location/g;

    invoke-virtual {v1}, Lcom/google/android/location/g;->g()I

    move-result v1

    :cond_2a
    invoke-direct {p0, v0, v1}, Lcom/google/android/location/f;->a(II)V

    .line 201
    const/4 v1, 0x1

    .line 217
    :goto_2e
    return v1

    :cond_2f
    move v0, v1

    .line 197
    goto :goto_1c

    .line 203
    :cond_31
    iget-wide v2, p0, Lcom/google/android/location/f;->h:J

    sub-long v2, p1, v2

    cmp-long v0, v2, v4

    if-ltz v0, :cond_45

    .line 205
    invoke-direct {p0}, Lcom/google/android/location/f;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/location/f;->h:J

    .line 206
    iget-wide v2, p0, Lcom/google/android/location/f;->h:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/f;->j(J)V

    goto :goto_2e

    .line 207
    :cond_45
    iget-wide v2, p0, Lcom/google/android/location/f;->h:J

    cmp-long v0, p1, v2

    if-ltz v0, :cond_52

    .line 211
    iget-wide v2, p0, Lcom/google/android/location/f;->h:J

    add-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/f;->j(J)V

    goto :goto_2e

    .line 214
    :cond_52
    iget-wide v2, p0, Lcom/google/android/location/f;->h:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/f;->j(J)V

    goto :goto_2e
.end method

.method public bridge synthetic c()V
    .registers 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/google/android/location/a;->c()V

    return-void
.end method

.method c(Z)V
    .registers 2
    .parameter

    .prologue
    .line 507
    return-void
.end method

.method public d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/location/f;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method protected g(J)Z
    .registers 4
    .parameter

    .prologue
    .line 245
    const/4 v0, 0x0

    return v0
.end method
