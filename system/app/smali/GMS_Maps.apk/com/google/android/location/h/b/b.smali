.class public Lcom/google/android/location/h/b/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/g;


# instance fields
.field private final a:Lcom/google/android/location/h/g;

.field private b:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/location/h/g;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/google/android/location/h/b/b;->c:Ljava/lang/String;

    .line 85
    iput-object p2, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    .line 86
    return-void
.end method

.method public static a(Ljava/io/InputStream;)Lcom/google/android/location/h/b/b;
    .registers 10
    .parameter

    .prologue
    .line 117
    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, p0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 119
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readShort()S

    move-result v1

    .line 120
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_73

    .line 123
    invoke-static {v1}, Lcom/google/android/location/h/b/g;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 126
    :goto_18
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v4

    .line 127
    invoke-static {v3}, Lcom/google/android/location/h/b/g;->a(Ljava/io/DataInputStream;)Ljava/util/Hashtable;

    move-result-object v5

    .line 129
    invoke-virtual {v5}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v6

    :goto_24
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_53

    .line 130
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 131
    invoke-virtual {v5, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 132
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " => "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_24

    .line 135
    :cond_53
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 136
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 138
    new-instance v6, Lcom/google/android/location/h/f;

    invoke-direct {v6, v3, v0}, Lcom/google/android/location/h/f;-><init>(Ljava/io/InputStream;I)V

    .line 139
    new-instance v0, Lcom/google/android/location/h/b/c;

    const/4 v3, -0x1

    invoke-direct {v0, v1, v3, v6}, Lcom/google/android/location/h/b/c;-><init>(Ljava/lang/String;ILcom/google/android/location/h/f;)V

    .line 140
    invoke-virtual {v6}, Lcom/google/android/location/h/f;->b()V

    .line 141
    new-instance v1, Lcom/google/android/location/h/b/b;

    invoke-direct {v1, v2, v0}, Lcom/google/android/location/h/b/b;-><init>(Ljava/lang/String;Lcom/google/android/location/h/g;)V

    .line 142
    iput-object v5, v1, Lcom/google/android/location/h/b/b;->b:Ljava/util/Hashtable;

    .line 143
    iput-object v4, v1, Lcom/google/android/location/h/b/b;->d:Ljava/lang/String;

    .line 145
    return-object v1

    :cond_73
    move-object v2, v0

    goto :goto_18
.end method

.method private declared-synchronized h()V
    .registers 5

    .prologue
    .line 92
    monitor-enter p0

    :try_start_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 93
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/location/h/b/b;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/h/b/g;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/location/h/b/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/location/h/b/b;->g()Ljava/util/Hashtable;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/location/h/b/g;->a(Ljava/io/DataOutputStream;Ljava/util/Hashtable;Ljava/lang/String;)V

    .line 98
    iget-object v2, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->b_()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 99
    iget-object v2, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->b_()I

    move-result v2

    if-lez v2, :cond_37

    .line 100
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 103
    :cond_37
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 104
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 106
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/b/b;->e:[B
    :try_end_43
    .catchall {:try_start_1 .. :try_end_43} :catchall_45

    .line 107
    monitor-exit p0

    return-void

    .line 92
    :catchall_45
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    invoke-interface {v0}, Lcom/google/android/location/h/g;->a()V

    .line 153
    return-void
.end method

.method public b_()I
    .registers 3

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/google/android/location/h/b/b;->h()V

    .line 160
    iget-object v0, p0, Lcom/google/android/location/h/b/b;->e:[B

    array-length v0, v0

    iget-object v1, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    invoke-interface {v1}, Lcom/google/android/location/h/g;->b_()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public c_()Ljava/io/InputStream;
    .registers 4

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/google/android/location/h/b/b;->h()V

    .line 168
    new-instance v0, Lcom/google/googlenav/common/io/SequenceInputStream;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/google/android/location/h/b/b;->e:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget-object v2, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->c_()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/common/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    return-object v0
.end method

.method public declared-synchronized d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 176
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/h/b/b;->d:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/location/h/b/b;->d:Ljava/lang/String;
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_c

    :goto_7
    monitor-exit p0

    return-object v0

    :cond_9
    :try_start_9
    const-string v0, ""
    :try_end_b
    .catchall {:try_start_9 .. :try_end_b} :catchall_c

    goto :goto_7

    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/location/h/b/b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public f()Lcom/google/android/location/h/g;
    .registers 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/location/h/b/b;->a:Lcom/google/android/location/h/g;

    return-object v0
.end method

.method public declared-synchronized g()Ljava/util/Hashtable;
    .registers 2

    .prologue
    .line 197
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/h/b/b;->b:Ljava/util/Hashtable;

    if-nez v0, :cond_c

    .line 198
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/b/b;->b:Ljava/util/Hashtable;

    .line 201
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/h/b/b;->b:Ljava/util/Hashtable;
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    monitor-exit p0

    return-object v0

    .line 197
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method
