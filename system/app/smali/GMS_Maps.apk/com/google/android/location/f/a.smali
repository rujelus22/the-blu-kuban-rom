.class public Lcom/google/android/location/f/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/f/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput p1, p0, Lcom/google/android/location/f/a;->a:I

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/f/a;->b:Ljava/util/ArrayList;

    .line 59
    return-void
.end method

.method public static a(Ljava/io/DataInputStream;)Lcom/google/android/location/f/a;
    .registers 5
    .parameter

    .prologue
    .line 119
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 120
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 121
    new-instance v2, Lcom/google/android/location/f/a;

    invoke-direct {v2, v0}, Lcom/google/android/location/f/a;-><init>(I)V

    .line 122
    const/4 v0, 0x0

    :goto_e
    if-ge v0, v1, :cond_1a

    .line 123
    invoke-static {p0}, Lcom/google/android/location/f/b;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/f/b;

    move-result-object v3

    .line 124
    invoke-virtual {v2, v3}, Lcom/google/android/location/f/a;->a(Lcom/google/android/location/f/b;)V

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 126
    :cond_1a
    return-object v2
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/location/f/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public a(Lcom/google/android/location/f/b;)V
    .registers 3
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/location/f/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    return-void
.end method

.method public a([F)[F
    .registers 5
    .parameter

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/location/f/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [F

    .line 89
    const/4 v0, 0x0

    move v1, v0

    :goto_a
    array-length v0, v2

    if-ge v1, v0, :cond_1f

    .line 90
    iget-object v0, p0, Lcom/google/android/location/f/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/f/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/f/b;->a([F)F

    move-result v0

    aput v0, v2, v1

    .line 89
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 92
    :cond_1f
    return-object v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 131
    if-ne p0, p1, :cond_5

    .line 141
    :cond_4
    :goto_4
    return v0

    .line 135
    :cond_5
    instance-of v2, p1, Lcom/google/android/location/f/a;

    if-nez v2, :cond_b

    move v0, v1

    .line 136
    goto :goto_4

    .line 139
    :cond_b
    check-cast p1, Lcom/google/android/location/f/a;

    .line 141
    iget v2, p0, Lcom/google/android/location/f/a;->a:I

    iget v3, p1, Lcom/google/android/location/f/a;->a:I

    if-ne v2, v3, :cond_1d

    iget-object v2, p0, Lcom/google/android/location/f/a;->b:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/google/android/location/f/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_1d
    move v0, v1

    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 147
    .line 148
    iget v0, p0, Lcom/google/android/location/f/a;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 149
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/f/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 100
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    const/4 v0, 0x0

    :goto_6
    iget-object v2, p0, Lcom/google/android/location/f/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_36

    .line 102
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Tree "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    iget-object v2, p0, Lcom/google/android/location/f/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 105
    :cond_36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
