.class Lcom/google/android/location/a/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:J

.field final b:[F


# direct methods
.method constructor <init>(J[F)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-wide p1, p0, Lcom/google/android/location/a/j;->a:J

    .line 20
    iput-object p3, p0, Lcom/google/android/location/a/j;->b:[F

    .line 21
    return-void
.end method


# virtual methods
.method a()D
    .registers 11

    .prologue
    .line 27
    const-wide/16 v1, 0x0

    .line 28
    iget-object v3, p0, Lcom/google/android/location/a/j;->b:[F

    array-length v4, v3

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v4, :cond_12

    aget v5, v3, v0

    .line 29
    float-to-double v6, v5

    float-to-double v8, v5

    mul-double v5, v6, v8

    add-double/2addr v1, v5

    .line 28
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 31
    :cond_12
    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SensorEvent [timestampNano="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/a/j;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/j;->b:[F

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
