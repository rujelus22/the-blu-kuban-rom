.class public Lcom/google/android/location/e/m;
.super Lcom/google/android/location/e/x;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/location/e/x",
        "<",
        "Lcom/google/android/location/e/l;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/location/j/a;->v:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {p0, v0}, Lcom/google/android/location/e/x;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 28
    return-void
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Map;
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 72
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 74
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 76
    invoke-virtual {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    .line 78
    const/4 v0, 0x0

    :goto_10
    if-ge v0, v3, :cond_25

    .line 79
    invoke-virtual {v2, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v4

    int-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v2, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 84
    :cond_25
    return-object v1
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/e/l;
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 44
    if-nez p1, :cond_5

    .line 67
    :cond_4
    :goto_4
    return-object v0

    .line 48
    :cond_5
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 52
    const/4 v2, 0x3

    :try_start_a
    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/e/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/f/a;

    move-result-object v2

    .line 55
    if-eqz v2, :cond_4

    .line 59
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/e/r;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Map;

    move-result-object v3

    .line 61
    invoke-static {v1}, Lcom/google/android/location/e/m;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Map;

    move-result-object v4

    .line 63
    new-instance v5, Lcom/google/android/location/e/z;

    invoke-direct {v5, v3, v2}, Lcom/google/android/location/e/z;-><init>(Ljava/util/Map;Lcom/google/android/location/f/a;)V

    .line 64
    new-instance v1, Lcom/google/android/location/e/l;

    invoke-direct {v1, v5, v4}, Lcom/google/android/location/e/l;-><init>(Lcom/google/android/location/e/z;Ljava/util/Map;)V
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_2b} :catch_2d

    move-object v0, v1

    goto :goto_4

    .line 65
    :catch_2d
    move-exception v1

    goto :goto_4
.end method

.method public synthetic b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/google/android/location/e/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/e/l;

    move-result-object v0

    return-object v0
.end method
