.class final enum Lcom/google/android/location/d/a$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/location/d/a$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/location/d/a$a;

.field public static final enum b:Lcom/google/android/location/d/a$a;

.field public static final enum c:Lcom/google/android/location/d/a$a;

.field public static final enum d:Lcom/google/android/location/d/a$a;

.field private static final synthetic e:[Lcom/google/android/location/d/a$a;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 128
    new-instance v0, Lcom/google/android/location/d/a$a;

    const-string v1, "LOCATION_ACTIVE_ON"

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/d/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d/a$a;->a:Lcom/google/android/location/d/a$a;

    new-instance v0, Lcom/google/android/location/d/a$a;

    const-string v1, "LOCATION_ACTIVE_OFF"

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/d/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d/a$a;->b:Lcom/google/android/location/d/a$a;

    new-instance v0, Lcom/google/android/location/d/a$a;

    const-string v1, "SATELLITE_STATUS_ON"

    invoke-direct {v0, v1, v4}, Lcom/google/android/location/d/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d/a$a;->c:Lcom/google/android/location/d/a$a;

    new-instance v0, Lcom/google/android/location/d/a$a;

    const-string v1, "SATELLITE_STATUS_OFF"

    invoke-direct {v0, v1, v5}, Lcom/google/android/location/d/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d/a$a;->d:Lcom/google/android/location/d/a$a;

    .line 126
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/location/d/a$a;

    sget-object v1, Lcom/google/android/location/d/a$a;->a:Lcom/google/android/location/d/a$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/location/d/a$a;->b:Lcom/google/android/location/d/a$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/d/a$a;->c:Lcom/google/android/location/d/a$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/location/d/a$a;->d:Lcom/google/android/location/d/a$a;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/location/d/a$a;->e:[Lcom/google/android/location/d/a$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/d/a$a;
    .registers 2
    .parameter

    .prologue
    .line 126
    const-class v0, Lcom/google/android/location/d/a$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/d/a$a;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/d/a$a;
    .registers 1

    .prologue
    .line 126
    sget-object v0, Lcom/google/android/location/d/a$a;->e:[Lcom/google/android/location/d/a$a;

    invoke-virtual {v0}, [Lcom/google/android/location/d/a$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/d/a$a;

    return-object v0
.end method
