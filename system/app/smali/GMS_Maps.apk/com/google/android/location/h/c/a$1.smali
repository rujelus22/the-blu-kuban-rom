.class Lcom/google/android/location/h/c/a$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/b/m$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/h/c/a;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/h/c/a;


# direct methods
.method constructor <init>(Lcom/google/android/location/h/c/a;)V
    .registers 2
    .parameter

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 119
    invoke-static {}, Lcom/google/android/location/h/c/a;->a()Lcom/google/android/location/h/c/a;

    move-result-object v1

    monitor-enter v1

    .line 121
    :try_start_5
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->g()I

    move-result v0

    .line 122
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->b_()I

    .line 124
    const/16 v2, 0xc8

    if-ne v0, v2, :cond_45

    .line 128
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->c_()Ljava/io/InputStream;

    move-result-object v0

    .line 129
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;J)J

    .line 131
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;Z)Z
    :try_end_28
    .catchall {:try_start_5 .. :try_end_28} :catchall_74
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_28} :catch_4d

    .line 138
    :goto_28
    :try_start_28
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;Z)Z

    .line 139
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-static {v0}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    new-instance v2, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-static {v3}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;)J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v2}, Lcom/google/android/location/h/c/a;->notifyObservers(Ljava/lang/Object;)V

    .line 142
    :goto_43
    monitor-exit v1
    :try_end_44
    .catchall {:try_start_28 .. :try_end_44} :catchall_71

    .line 143
    return-void

    .line 133
    :cond_45
    :try_start_45
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;J)J
    :try_end_4c
    .catchall {:try_start_45 .. :try_end_4c} :catchall_74
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_4c} :catch_4d

    goto :goto_28

    .line 135
    :catch_4d
    move-exception v0

    .line 136
    :try_start_4e
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;J)J
    :try_end_55
    .catchall {:try_start_4e .. :try_end_55} :catchall_74

    .line 138
    :try_start_55
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;Z)Z

    .line 139
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-static {v0}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    new-instance v2, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-static {v3}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;)J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v2}, Lcom/google/android/location/h/c/a;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_43

    .line 142
    :catchall_71
    move-exception v0

    monitor-exit v1
    :try_end_73
    .catchall {:try_start_55 .. :try_end_73} :catchall_71

    throw v0

    .line 138
    :catchall_74
    move-exception v0

    :try_start_75
    iget-object v2, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;Z)Z

    .line 139
    iget-object v2, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-static {v2}, Lcom/google/android/location/h/c/a;->a(Lcom/google/android/location/h/c/a;)V

    .line 140
    iget-object v2, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    new-instance v3, Ljava/lang/Long;

    iget-object v4, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    invoke-static {v4}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;)J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v2, v3}, Lcom/google/android/location/h/c/a;->notifyObservers(Ljava/lang/Object;)V

    throw v0
    :try_end_91
    .catchall {:try_start_75 .. :try_end_91} :catchall_71
.end method

.method public a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 146
    invoke-static {}, Lcom/google/android/location/h/c/a;->a()Lcom/google/android/location/h/c/a;

    move-result-object v1

    monitor-enter v1

    .line 147
    :try_start_5
    iget-object v0, p0, Lcom/google/android/location/h/c/a$1;->a:Lcom/google/android/location/h/c/a;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/location/h/c/a;->b(Lcom/google/android/location/h/c/a;Z)Z

    .line 148
    monitor-exit v1

    .line 149
    return-void

    .line 148
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_5 .. :try_end_f} :catchall_d

    throw v0
.end method
