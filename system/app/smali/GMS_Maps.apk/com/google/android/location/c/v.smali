.class public Lcom/google/android/location/c/v;
.super Lcom/google/android/location/h/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/v$a;
    }
.end annotation


# static fields
.field private static final a:[B


# instance fields
.field private b:Lcom/google/android/location/h/g;

.field private f:[B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_a

    sput-object v0, Lcom/google/android/location/c/v;->a:[B

    return-void

    nop

    :array_a
    .array-data 0x1
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;I[B)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/b/o;-><init>(Ljava/lang/String;I)V

    .line 64
    const/16 v0, 0x101

    invoke-virtual {p0, v0}, Lcom/google/android/location/c/v;->c(I)V

    .line 65
    invoke-virtual {p0, p3}, Lcom/google/android/location/c/v;->a([B)V

    .line 66
    return-void
.end method

.method private f()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 104
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 105
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 107
    invoke-virtual {p0}, Lcom/google/android/location/c/v;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/location/c/v;->v()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lcom/google/android/location/c/v;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 110
    const-string v2, "POST"

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 111
    const/16 v2, 0x6d72

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 112
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 113
    const-string v2, "ROOT"

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 114
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 116
    iget-object v2, p0, Lcom/google/android/location/c/v;->b:Lcom/google/android/location/h/g;

    if-eqz v2, :cond_5e

    iget-object v2, p0, Lcom/google/android/location/c/v;->b:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->b_()I

    move-result v2

    if-lez v2, :cond_5e

    .line 118
    iget-object v2, p0, Lcom/google/android/location/c/v;->b:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->b_()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 119
    const-string v2, "g"

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 124
    :goto_51
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 125
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 127
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/v;->f:[B

    .line 128
    return-void

    .line 121
    :cond_5e
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_51
.end method

.method private g()V
    .registers 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/location/c/v;->f:[B

    if-nez v0, :cond_7

    .line 145
    invoke-direct {p0}, Lcom/google/android/location/c/v;->f()V

    .line 147
    :cond_7
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 2

    .prologue
    .line 135
    monitor-enter p0

    :try_start_1
    invoke-super {p0}, Lcom/google/android/location/h/b/o;->a()V

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/v;->f:[B

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/v;->b:Lcom/google/android/location/h/g;
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 138
    monitor-exit p0

    return-void

    .line 135
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .registers 3
    .parameter

    .prologue
    .line 75
    monitor-enter p0

    :try_start_1
    invoke-super {p0, p1}, Lcom/google/android/location/h/b/o;->a(I)V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/v;->f:[B
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 80
    monitor-exit p0

    return-void

    .line 75
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a([B)V
    .registers 3
    .parameter

    .prologue
    .line 88
    monitor-enter p0

    if-eqz p1, :cond_f

    :try_start_3
    array-length v0, p1

    if-lez v0, :cond_f

    .line 91
    new-instance v0, Lcom/google/android/location/c/v$a;

    invoke-direct {v0, p1}, Lcom/google/android/location/c/v$a;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/location/c/v;->b:Lcom/google/android/location/h/g;
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_13

    .line 95
    :goto_d
    monitor-exit p0

    return-void

    .line 93
    :cond_f
    const/4 v0, 0x0

    :try_start_10
    iput-object v0, p0, Lcom/google/android/location/c/v;->b:Lcom/google/android/location/h/g;
    :try_end_12
    .catchall {:try_start_10 .. :try_end_12} :catchall_13

    goto :goto_d

    .line 88
    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected b()Ljava/io/InputStream;
    .registers 6

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/google/android/location/c/v;->g()V

    .line 152
    iget-object v0, p0, Lcom/google/android/location/c/v;->b:Lcom/google/android/location/h/g;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/location/c/v;->b:Lcom/google/android/location/h/g;

    invoke-interface {v0}, Lcom/google/android/location/h/g;->b_()I

    move-result v0

    if-nez v0, :cond_23

    .line 154
    :cond_f
    new-instance v0, Lcom/google/googlenav/common/io/SequenceInputStream;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/google/android/location/c/v;->f:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v2, Ljava/io/ByteArrayInputStream;

    sget-object v3, Lcom/google/android/location/c/v;->a:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/common/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    .line 159
    :goto_22
    return-object v0

    :cond_23
    new-instance v0, Lcom/google/googlenav/common/io/SequenceInputStream;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/google/android/location/c/v;->f:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget-object v2, p0, Lcom/google/android/location/c/v;->b:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->c_()Ljava/io/InputStream;

    move-result-object v2

    new-instance v3, Ljava/io/ByteArrayInputStream;

    sget-object v4, Lcom/google/android/location/c/v;->a:[B

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;Ljava/io/InputStream;)V

    goto :goto_22
.end method

.method protected c()I
    .registers 3

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/location/c/v;->g()V

    .line 167
    iget-object v0, p0, Lcom/google/android/location/c/v;->f:[B

    array-length v0, v0

    sget-object v1, Lcom/google/android/location/c/v;->a:[B

    array-length v1, v1

    add-int/2addr v0, v1

    .line 168
    iget-object v1, p0, Lcom/google/android/location/c/v;->b:Lcom/google/android/location/h/g;

    if-eqz v1, :cond_15

    .line 169
    iget-object v1, p0, Lcom/google/android/location/c/v;->b:Lcom/google/android/location/h/g;

    invoke-interface {v1}, Lcom/google/android/location/h/g;->b_()I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_15
    return v0
.end method
