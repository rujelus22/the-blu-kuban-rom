.class public Lcom/google/android/location/e/l;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/e/l$a;
    }
.end annotation


# instance fields
.field final a:Lcom/google/android/location/e/z;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/location/e/z;Ljava/util/Map;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/z;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p2, p0, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    .line 81
    iput-object p1, p0, Lcom/google/android/location/e/l;->a:Lcom/google/android/location/e/z;

    .line 82
    return-void
.end method


# virtual methods
.method public a(Ljava/util/Map;)Lcom/google/android/location/e/l$a;
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/location/e/l$a;"
        }
    .end annotation

    .prologue
    const/high16 v2, 0x3f80

    .line 99
    iget-object v0, p0, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2c

    .line 100
    new-instance v1, Lcom/google/android/location/e/l$a;

    invoke-direct {v1}, Lcom/google/android/location/e/l$a;-><init>()V

    .line 102
    iget-object v0, p0, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/location/e/l$a;->a(Lcom/google/android/location/e/l$a;Ljava/lang/String;)Ljava/lang/String;

    .line 104
    invoke-static {v1}, Lcom/google/android/location/e/l$a;->a(Lcom/google/android/location/e/l$a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/location/e/l$a;->a(Ljava/lang/String;F)V

    move-object v0, v1

    .line 124
    :goto_2b
    return-object v0

    .line 108
    :cond_2c
    new-instance v1, Lcom/google/android/location/e/l$a;

    invoke-direct {v1}, Lcom/google/android/location/e/l$a;-><init>()V

    .line 111
    iget-object v0, p0, Lcom/google/android/location/e/l;->a:Lcom/google/android/location/e/z;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e/z;->a(Ljava/util/Map;)[F

    move-result-object v3

    .line 115
    array-length v0, v3

    int-to-float v0, v0

    div-float v4, v2, v0

    .line 116
    array-length v5, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_3e
    if-ge v2, v5, :cond_58

    aget v0, v3, v2

    .line 117
    iget-object v6, p0, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 118
    if-nez v0, :cond_54

    .line 116
    :goto_50
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3e

    .line 120
    :cond_54
    invoke-virtual {v1, v0, v4}, Lcom/google/android/location/e/l$a;->a(Ljava/lang/String;F)V

    goto :goto_50

    .line 123
    :cond_58
    invoke-virtual {v1}, Lcom/google/android/location/e/l$a;->c()V

    move-object v0, v1

    .line 124
    goto :goto_2b
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 129
    if-ne p0, p1, :cond_5

    .line 139
    :cond_4
    :goto_4
    return v0

    .line 133
    :cond_5
    instance-of v2, p1, Lcom/google/android/location/e/l;

    if-nez v2, :cond_b

    move v0, v1

    .line 134
    goto :goto_4

    .line 137
    :cond_b
    check-cast p1, Lcom/google/android/location/e/l;

    .line 139
    iget-object v2, p0, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_21

    iget-object v2, p0, Lcom/google/android/location/e/l;->a:Lcom/google/android/location/e/z;

    iget-object v3, p1, Lcom/google/android/location/e/l;->a:Lcom/google/android/location/e/z;

    invoke-virtual {v2, v3}, Lcom/google/android/location/e/z;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_21
    move v0, v1

    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 145
    .line 146
    iget-object v0, p0, Lcom/google/android/location/e/l;->b:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 147
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/e/l;->a:Lcom/google/android/location/e/z;

    invoke-virtual {v1}, Lcom/google/android/location/e/z;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    return v0
.end method
