.class public final enum Lcom/google/android/location/os/i$b;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/location/os/i$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/location/os/i$b;

.field private static final synthetic b:[Lcom/google/android/location/os/i$b;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 106
    new-instance v0, Lcom/google/android/location/os/i$b;

    const-string v1, "TRIGGER_ACTIVE_BURST_COLLECTION"

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/os/i$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/os/i$b;->a:Lcom/google/android/location/os/i$b;

    .line 105
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/location/os/i$b;

    sget-object v1, Lcom/google/android/location/os/i$b;->a:Lcom/google/android/location/os/i$b;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/location/os/i$b;->b:[Lcom/google/android/location/os/i$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/os/i$b;
    .registers 2
    .parameter

    .prologue
    .line 105
    const-class v0, Lcom/google/android/location/os/i$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/i$b;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/os/i$b;
    .registers 1

    .prologue
    .line 105
    sget-object v0, Lcom/google/android/location/os/i$b;->b:[Lcom/google/android/location/os/i$b;

    invoke-virtual {v0}, [Lcom/google/android/location/os/i$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/os/i$b;

    return-object v0
.end method
