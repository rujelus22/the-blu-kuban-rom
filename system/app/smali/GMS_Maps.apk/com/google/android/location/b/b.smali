.class public Lcom/google/android/location/b/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/b/b$1;,
        Lcom/google/android/location/b/b$a;
    }
.end annotation


# static fields
.field static final a:J

.field static final b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# instance fields
.field final c:Lcom/google/android/location/os/i;

.field final d:Lcom/google/android/location/b/f;

.field final e:Lcom/google/android/location/b/g;

.field final f:Lcom/google/android/location/os/h;

.field final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;"
        }
    .end annotation
.end field

.field final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;"
        }
    .end annotation
.end field

.field i:I

.field j:I

.field k:I

.field l:Z

.field m:Lcom/google/android/location/b/b$a;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const-wide/32 v2, 0x2932e00

    const/4 v4, -0x1

    .line 55
    const-wide/32 v0, 0x5265c00

    invoke-static {v2, v3, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/b/b;->a:J

    .line 127
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->ae:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    sput-object v0, Lcom/google/android/location/b/b;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 128
    sget-object v0, Lcom/google/android/location/b/b;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 129
    sget-object v0, Lcom/google/android/location/b/b;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 130
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/os/h;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    .line 99
    iput v1, p0, Lcom/google/android/location/b/b;->i:I

    .line 105
    iput v1, p0, Lcom/google/android/location/b/b;->j:I

    .line 111
    iput v1, p0, Lcom/google/android/location/b/b;->k:I

    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/b/b;->l:Z

    .line 118
    sget-object v0, Lcom/google/android/location/b/b$a;->a:Lcom/google/android/location/b/b$a;

    iput-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    .line 143
    iput-object p1, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    .line 144
    iput-object p2, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    .line 145
    iput-object p3, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    .line 146
    iput-object p4, p0, Lcom/google/android/location/b/b;->f:Lcom/google/android/location/os/h;

    .line 147
    return-void
.end method

.method private a(JI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 525
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->ac:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 526
    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 529
    const/4 v1, 0x1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 531
    if-lez p3, :cond_19

    .line 532
    const/16 v1, 0xa

    invoke-virtual {v0, v1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 535
    :cond_19
    return-object v0
.end method

.method private a(Z)V
    .registers 5
    .parameter

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    .line 328
    iget-object v2, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    invoke-virtual {v2, v0, v1, p1}, Lcom/google/android/location/b/f;->a(JZ)V

    .line 329
    iget-object v2, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v2, v0, v1, p1}, Lcom/google/android/location/b/g;->a(JZ)V

    .line 330
    return-void
.end method

.method private c()V
    .registers 2

    .prologue
    .line 182
    sget-object v0, Lcom/google/android/location/b/b$a;->a:Lcom/google/android/location/b/b$a;

    iput-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    .line 183
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/b/b;->a(Z)V

    .line 184
    invoke-direct {p0}, Lcom/google/android/location/b/b;->k()V

    .line 186
    invoke-direct {p0}, Lcom/google/android/location/b/b;->d()V

    .line 187
    return-void
.end method

.method private d()V
    .registers 3

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/google/android/location/b/b;->l:Z

    if-eqz v0, :cond_d

    .line 194
    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->c(I)V

    .line 195
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/b/b;->l:Z

    .line 197
    :cond_d
    return-void
.end method

.method private e()V
    .registers 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/location/b/b;->f:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->b()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 235
    invoke-direct {p0}, Lcom/google/android/location/b/b;->f()V

    .line 239
    :goto_b
    return-void

    .line 237
    :cond_c
    invoke-direct {p0}, Lcom/google/android/location/b/b;->g()V

    goto :goto_b
.end method

.method private f()V
    .registers 7

    .prologue
    .line 246
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 247
    iget-object v1, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/i;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 250
    invoke-direct {p0}, Lcom/google/android/location/b/b;->h()V

    .line 251
    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x2710

    add-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 252
    sget-object v0, Lcom/google/android/location/b/b$a;->b:Lcom/google/android/location/b/b$a;

    iput-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    .line 253
    return-void
.end method

.method private g()V
    .registers 9

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    invoke-virtual {v0}, Lcom/google/android/location/b/f;->d()Lcom/google/android/location/b/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    invoke-virtual {v1}, Lcom/google/android/location/b/f;->c()Lcom/google/android/location/b/i;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/b/b;->a(Lcom/google/android/location/b/i;Lcom/google/android/location/b/i;)V

    .line 261
    invoke-direct {p0}, Lcom/google/android/location/b/b;->i()Z

    move-result v0

    .line 262
    if-eqz v0, :cond_2f

    .line 266
    invoke-direct {p0}, Lcom/google/android/location/b/b;->h()V

    .line 267
    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    iget v4, p0, Lcom/google/android/location/b/b;->j:I

    int-to-long v4, v4

    const-wide/16 v6, 0x2710

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 269
    sget-object v0, Lcom/google/android/location/b/b$a;->c:Lcom/google/android/location/b/b$a;

    iput-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    .line 271
    :cond_2f
    return-void
.end method

.method private h()V
    .registers 3

    .prologue
    .line 277
    iget-boolean v0, p0, Lcom/google/android/location/b/b;->l:Z

    if-nez v0, :cond_d

    .line 278
    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(I)V

    .line 279
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/b/b;->l:Z

    .line 281
    :cond_d
    return-void
.end method

.method private i()Z
    .registers 2

    .prologue
    .line 290
    invoke-direct {p0}, Lcom/google/android/location/b/b;->l()Z

    move-result v0

    .line 291
    if-nez v0, :cond_9

    .line 293
    invoke-direct {p0}, Lcom/google/android/location/b/b;->j()V

    .line 296
    :cond_9
    return v0
.end method

.method private j()V
    .registers 3

    .prologue
    .line 300
    iget v0, p0, Lcom/google/android/location/b/b;->j:I

    if-eqz v0, :cond_12

    iget v0, p0, Lcom/google/android/location/b/b;->k:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/location/b/b;->j:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const v1, 0x3f333333

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_23

    :cond_12
    const/4 v0, 0x1

    .line 302
    :goto_13
    if-eqz v0, :cond_19

    .line 304
    iget v1, p0, Lcom/google/android/location/b/b;->j:I

    if-nez v1, :cond_19

    .line 320
    :cond_19
    invoke-direct {p0, v0}, Lcom/google/android/location/b/b;->a(Z)V

    .line 322
    invoke-direct {p0}, Lcom/google/android/location/b/b;->k()V

    .line 323
    invoke-direct {p0}, Lcom/google/android/location/b/b;->d()V

    .line 324
    return-void

    .line 300
    :cond_23
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private k()V
    .registers 5

    .prologue
    const/4 v1, -0x1

    .line 345
    iget-object v0, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 346
    iget-object v0, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 347
    iput v1, p0, Lcom/google/android/location/b/b;->i:I

    .line 348
    iput v1, p0, Lcom/google/android/location/b/b;->j:I

    .line 349
    iput v1, p0, Lcom/google/android/location/b/b;->k:I

    .line 350
    sget-object v0, Lcom/google/android/location/b/b$a;->a:Lcom/google/android/location/b/b$a;

    iput-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    .line 351
    invoke-virtual {p0}, Lcom/google/android/location/b/b;->b()J

    move-result-wide v0

    .line 352
    iget-object v2, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    const/4 v3, 0x4

    invoke-interface {v2, v3, v0, v1}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 354
    return-void
.end method

.method private l()Z
    .registers 12

    .prologue
    const/4 v4, 0x0

    const/4 v10, 0x4

    const/4 v2, 0x0

    const/4 v9, 0x2

    const/4 v5, 0x1

    .line 395
    .line 396
    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v6

    .line 399
    iget-object v0, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_a3

    .line 400
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->an:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 401
    sget-object v0, Lcom/google/android/location/b/b;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 402
    invoke-virtual {v3, v9, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 403
    iget-object v0, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    :goto_29
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 404
    iget v0, p0, Lcom/google/android/location/b/b;->i:I

    if-ge v1, v0, :cond_43

    .line 405
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 406
    invoke-interface {v8}, Ljava/util/Iterator;->remove()V

    .line 407
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_29

    :cond_43
    move v0, v1

    move-object v1, v3

    .line 413
    :goto_45
    iget v3, p0, Lcom/google/android/location/b/b;->i:I

    if-ge v0, v3, :cond_7c

    iget-object v3, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_7c

    .line 414
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/j/a;->L:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 415
    invoke-virtual {v4, v5, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 416
    iget-object v3, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v0

    :goto_62
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7d

    .line 417
    iget v0, p0, Lcom/google/android/location/b/b;->i:I

    if-ge v3, v0, :cond_7d

    .line 418
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v4, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 419
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    .line 420
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_62

    :cond_7c
    move v3, v0

    .line 426
    :cond_7d
    if-lez v3, :cond_80

    move v2, v5

    .line 427
    :cond_80
    if-eqz v2, :cond_a2

    .line 428
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 429
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/android/location/j/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 430
    if-eqz v1, :cond_95

    .line 431
    invoke-virtual {v3, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 433
    :cond_95
    if-eqz v4, :cond_9a

    .line 434
    invoke-virtual {v3, v9, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 436
    :cond_9a
    invoke-virtual {v0, v10, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 437
    iget-object v1, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/i;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 440
    :cond_a2
    return v2

    :cond_a3
    move-object v1, v4

    move v0, v2

    goto :goto_45
.end method


# virtual methods
.method a(JZ)J
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 380
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide/high16 v2, 0x3fe0

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000

    mul-double/2addr v0, v2

    const-wide v2, 0x41224f8000000000L

    mul-double/2addr v0, v2

    double-to-long v2, v0

    .line 381
    if-eqz p3, :cond_19

    const-wide/32 v0, 0x5265c00

    .line 382
    :goto_16
    add-long/2addr v0, p1

    add-long/2addr v0, v2

    .line 383
    return-wide v0

    .line 381
    :cond_19
    sget-wide v0, Lcom/google/android/location/b/b;->a:J

    goto :goto_16
.end method

.method public a()V
    .registers 1

    .prologue
    .line 338
    invoke-direct {p0}, Lcom/google/android/location/b/b;->k()V

    .line 339
    return-void
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 205
    const/4 v0, 0x4

    if-eq p1, v0, :cond_4

    .line 229
    :goto_3
    return-void

    .line 208
    :cond_4
    sget-object v0, Lcom/google/android/location/b/b$1;->a:[I

    iget-object v1, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    invoke-virtual {v1}, Lcom/google/android/location/b/b$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1e

    goto :goto_3

    .line 212
    :pswitch_12
    invoke-direct {p0}, Lcom/google/android/location/b/b;->e()V

    goto :goto_3

    .line 217
    :pswitch_16
    invoke-direct {p0}, Lcom/google/android/location/b/b;->c()V

    goto :goto_3

    .line 223
    :pswitch_1a
    invoke-direct {p0}, Lcom/google/android/location/b/b;->j()V

    goto :goto_3

    .line 208
    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_12
        :pswitch_16
        :pswitch_1a
    .end packed-switch
.end method

.method a(Lcom/google/android/location/b/i;Lcom/google/android/location/b/i;)V
    .registers 14
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/e/w;",
            ">;",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 452
    iget-object v0, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 453
    iget-object v0, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 454
    iput v3, p0, Lcom/google/android/location/b/b;->k:I

    .line 456
    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v4

    .line 458
    if-eqz p1, :cond_57

    .line 459
    iget-object v0, p0, Lcom/google/android/location/b/b;->f:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->d()I

    move-result v2

    .line 460
    invoke-virtual {p1}, Lcom/google/android/location/b/i;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_23
    :goto_23
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_57

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 461
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/b/a;

    invoke-virtual {v1}, Lcom/google/android/location/b/a;->c()I

    move-result v1

    if-ge v1, v2, :cond_4d

    .line 463
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/location/e/e;->a(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 464
    if-eqz v0, :cond_23

    .line 465
    iget-object v1, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_23

    .line 470
    :cond_4d
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/location/b/a;->b(J)V

    goto :goto_23

    .line 475
    :cond_57
    iget-object v0, p0, Lcom/google/android/location/b/b;->f:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->e()I

    move-result v6

    .line 476
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 477
    if-eqz p2, :cond_b9

    .line 480
    invoke-virtual {p2}, Lcom/google/android/location/b/i;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_6c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 481
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/b/a;

    .line 482
    invoke-virtual {v1}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/e/C;

    .line 483
    invoke-virtual {v1}, Lcom/google/android/location/b/a;->c()I

    move-result v1

    if-lt v1, v6, :cond_90

    invoke-virtual {v2}, Lcom/google/android/location/e/C;->e()Z

    move-result v1

    if-eqz v1, :cond_af

    .line 485
    :cond_90
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 486
    invoke-virtual {v2}, Lcom/google/android/location/e/C;->d()I

    move-result v2

    .line 487
    iget-object v9, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/location/b/b;->a(JI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 489
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_6c

    .line 493
    :cond_af
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/location/b/a;->b(J)V

    goto :goto_6c

    .line 499
    :cond_b9
    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v4

    .line 500
    iget-object v0, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    if-eqz v0, :cond_11b

    .line 501
    iget-object v0, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v0}, Lcom/google/android/location/b/g;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_cd
    :goto_cd
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 502
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 503
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/b/g$b;

    invoke-virtual {v1}, Lcom/google/android/location/b/g$b;->b()I

    move-result v1

    if-ge v1, v6, :cond_10d

    const/4 v1, 0x1

    .line 504
    :goto_f0
    if-eqz v1, :cond_10f

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_10f

    .line 505
    iget-object v0, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-direct {p0, v8, v9, v3}, Lcom/google/android/location/b/b;->a(JI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 506
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_cd

    :cond_10d
    move v1, v3

    .line 503
    goto :goto_f0

    .line 507
    :cond_10f
    if-nez v1, :cond_cd

    .line 510
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/location/b/g$b;->a(J)V

    goto :goto_cd

    .line 515
    :cond_11b
    iget-object v0, p0, Lcom/google/android/location/b/b;->f:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->j()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/b/b;->i:I

    .line 516
    iget-object v0, p0, Lcom/google/android/location/b/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/location/b/b;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 517
    int-to-double v1, v0

    const-wide/high16 v3, 0x3ff0

    mul-double/2addr v1, v3

    iget v3, p0, Lcom/google/android/location/b/b;->i:I

    int-to-double v3, v3

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    iput v1, p0, Lcom/google/android/location/b/b;->j:I

    .line 518
    if-lez v0, :cond_141

    .line 522
    :cond_141
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x5

    .line 156
    iget-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    sget-object v1, Lcom/google/android/location/b/b$a;->b:Lcom/google/android/location/b/b$a;

    if-ne v0, v1, :cond_2a

    .line 158
    iget-object v0, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(I)V

    .line 159
    invoke-static {p1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 160
    iget-object v0, p0, Lcom/google/android/location/b/b;->f:Lcom/google/android/location/os/h;

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 161
    invoke-direct {p0}, Lcom/google/android/location/b/b;->g()V

    .line 174
    :cond_25
    :goto_25
    return-void

    .line 164
    :cond_26
    invoke-direct {p0}, Lcom/google/android/location/b/b;->c()V

    goto :goto_25

    .line 166
    :cond_2a
    iget-object v0, p0, Lcom/google/android/location/b/b;->m:Lcom/google/android/location/b/b$a;

    sget-object v1, Lcom/google/android/location/b/b$a;->c:Lcom/google/android/location/b/b$a;

    if-ne v0, v1, :cond_25

    .line 167
    iget-object v0, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/b/b;->c:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v2

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/location/b/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZJ)V

    .line 168
    iget-object v0, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 169
    invoke-static {p1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 170
    iget v0, p0, Lcom/google/android/location/b/b;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/b/b;->k:I

    .line 172
    :cond_4d
    invoke-direct {p0}, Lcom/google/android/location/b/b;->i()Z

    goto :goto_25
.end method

.method b()J
    .registers 6

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    invoke-virtual {v0}, Lcom/google/android/location/b/f;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/b/b;->d:Lcom/google/android/location/b/f;

    invoke-virtual {v2}, Lcom/google/android/location/b/f;->b()Z

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/location/b/b;->a(JZ)J

    move-result-wide v0

    .line 364
    iget-object v2, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v2}, Lcom/google/android/location/b/g;->d()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/location/b/b;->e:Lcom/google/android/location/b/g;

    invoke-virtual {v4}, Lcom/google/android/location/b/g;->e()Z

    move-result v4

    invoke-virtual {p0, v2, v3, v4}, Lcom/google/android/location/b/b;->a(JZ)J

    move-result-wide v2

    .line 367
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method
