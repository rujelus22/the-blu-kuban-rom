.class Lcom/google/android/location/a/d$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field private final a:D

.field private final b:D


# direct methods
.method constructor <init>(DD)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    iput-wide p1, p0, Lcom/google/android/location/a/d$c;->a:D

    .line 231
    iput-wide p3, p0, Lcom/google/android/location/a/d$c;->b:D

    .line 232
    return-void
.end method

.method private a()D
    .registers 3

    .prologue
    .line 235
    iget-wide v0, p0, Lcom/google/android/location/a/d$c;->b:D

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/location/a/d$c;)D
    .registers 3
    .parameter

    .prologue
    .line 225
    iget-wide v0, p0, Lcom/google/android/location/a/d$c;->a:D

    return-wide v0
.end method

.method static synthetic b(Lcom/google/android/location/a/d$c;)D
    .registers 3
    .parameter

    .prologue
    .line 225
    invoke-direct {p0}, Lcom/google/android/location/a/d$c;->a()D

    move-result-wide v0

    return-wide v0
.end method
