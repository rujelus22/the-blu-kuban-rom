.class final Lcom/google/android/location/i/b$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/i/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/i/b;

.field private final b:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/i/b;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 258
    iput-object p1, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 259
    iput-wide p2, p0, Lcom/google/android/location/i/b$a;->b:J

    .line 260
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    .prologue
    .line 264
    const/4 v0, 0x0

    .line 265
    new-instance v2, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v1}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;)Lcom/google/android/location/os/i;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/os/i;->e()Ljava/io/File;

    move-result-object v1

    const-string v3, "cp_state"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 267
    :try_start_12
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1b

    .line 268
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 270
    :cond_1b
    iget-object v1, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v1}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;)Lcom/google/android/location/os/i;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/google/android/location/os/i;->a(Ljava/io/File;)V

    .line 271
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_29
    .catchall {:try_start_12 .. :try_end_29} :catchall_5f
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_29} :catch_48
    .catch Ljava/lang/SecurityException; {:try_start_12 .. :try_end_29} :catch_4f
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_29} :catch_57

    .line 275
    :try_start_29
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 276
    iget-object v2, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    iget-wide v3, p0, Lcom/google/android/location/i/b$a;->b:J

    invoke-static {v2, v3, v4, v0}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;JLjava/io/ByteArrayOutputStream;)V

    .line 277
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 278
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 279
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_42
    .catchall {:try_start_29 .. :try_end_42} :catchall_69
    .catch Ljava/io/FileNotFoundException; {:try_start_29 .. :try_end_42} :catch_6f
    .catch Ljava/lang/SecurityException; {:try_start_29 .. :try_end_42} :catch_6d
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_42} :catch_6b

    .line 287
    iget-object v0, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v0, v1}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;Ljava/io/Closeable;)V

    .line 289
    :goto_47
    return-void

    .line 280
    :catch_48
    move-exception v1

    .line 287
    :goto_49
    iget-object v1, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v1, v0}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;Ljava/io/Closeable;)V

    goto :goto_47

    .line 282
    :catch_4f
    move-exception v1

    move-object v1, v0

    .line 287
    :goto_51
    iget-object v0, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v0, v1}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;Ljava/io/Closeable;)V

    goto :goto_47

    .line 284
    :catch_57
    move-exception v1

    move-object v1, v0

    .line 287
    :goto_59
    iget-object v0, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v0, v1}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;Ljava/io/Closeable;)V

    goto :goto_47

    :catchall_5f
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_63
    iget-object v2, p0, Lcom/google/android/location/i/b$a;->a:Lcom/google/android/location/i/b;

    invoke-static {v2, v1}, Lcom/google/android/location/i/b;->a(Lcom/google/android/location/i/b;Ljava/io/Closeable;)V

    throw v0

    :catchall_69
    move-exception v0

    goto :goto_63

    .line 284
    :catch_6b
    move-exception v0

    goto :goto_59

    .line 282
    :catch_6d
    move-exception v0

    goto :goto_51

    .line 280
    :catch_6f
    move-exception v0

    move-object v0, v1

    goto :goto_49
.end method
