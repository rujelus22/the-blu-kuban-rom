.class public Lcom/google/android/location/h/h;
.super Lcom/google/googlenav/common/io/BaseHttpConnectionFactory;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/h/h$a;,
        Lcom/google/android/location/h/h$b;
    }
.end annotation


# static fields
.field protected static b:Lcom/google/android/location/h/h;


# instance fields
.field protected c:Las/c;

.field protected d:Lcom/google/android/location/h/b/d;

.field protected e:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/google/android/location/h/b/m;",
            ">;"
        }
    .end annotation
.end field

.field protected f:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Lcom/google/android/location/h/b/m;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected g:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/h/b/m;",
            ">;"
        }
    .end annotation
.end field

.field protected h:J

.field protected i:J

.field protected j:Las/d;

.field protected k:Las/d;

.field private l:Lcom/google/android/location/h/a/c;

.field private m:Lcom/google/googlenav/common/io/g;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/google/android/location/h/h$b;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/lang/Object;

.field private r:I

.field private s:I

.field private t:I

.field private final u:J

.field private v:Z

.field private w:Lcom/google/android/location/h/j;


# direct methods
.method protected constructor <init>(Lcom/google/android/location/h/h$a;)V
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 237
    invoke-direct {p0}, Lcom/google/googlenav/common/io/BaseHttpConnectionFactory;-><init>()V

    .line 103
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/h;->p:Ljava/util/Vector;

    .line 112
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    .line 122
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    .line 128
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/h;->g:Ljava/util/Hashtable;

    .line 133
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    .line 135
    iput v1, p0, Lcom/google/android/location/h/h;->r:I

    .line 136
    iput v1, p0, Lcom/google/android/location/h/h;->s:I

    .line 137
    iput v1, p0, Lcom/google/android/location/h/h;->t:I

    .line 163
    iput-boolean v1, p0, Lcom/google/android/location/h/h;->v:Z

    .line 240
    invoke-static {p1}, Lcom/google/android/location/h/h$a;->a(Lcom/google/android/location/h/h$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/h/h;->u:J

    .line 242
    new-instance v0, Lcom/google/android/location/h/b/d;

    invoke-static {p1}, Lcom/google/android/location/h/h$a;->b(Lcom/google/android/location/h/h$a;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/location/h/h$a;->c(Lcom/google/android/location/h/h$a;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/location/h/h$a;->d(Lcom/google/android/location/h/h$a;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/location/h/h$a;->e(Lcom/google/android/location/h/h$a;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "g"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/h/b/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/h/h;->d:Lcom/google/android/location/h/b/d;

    .line 248
    invoke-virtual {p0}, Lcom/google/android/location/h/h;->h()Las/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/h;->c:Las/c;

    .line 249
    iget-object v0, p0, Lcom/google/android/location/h/h;->c:Las/c;

    invoke-virtual {v0}, Las/c;->d()V

    .line 250
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->l()Lcom/google/googlenav/common/io/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/h;->m:Lcom/google/googlenav/common/io/g;

    .line 251
    invoke-static {p1}, Lcom/google/android/location/h/h$a;->f(Lcom/google/android/location/h/h$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/h;->n:Ljava/lang/String;

    .line 252
    invoke-static {p1}, Lcom/google/android/location/h/h$a;->g(Lcom/google/android/location/h/h$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/h;->o:Ljava/lang/String;

    .line 253
    invoke-virtual {p0}, Lcom/google/android/location/h/h;->i()Lcom/google/android/location/h/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/h;->l:Lcom/google/android/location/h/a/c;

    .line 254
    iget-object v0, p0, Lcom/google/android/location/h/h;->l:Lcom/google/android/location/h/a/c;

    invoke-virtual {v0}, Lcom/google/android/location/h/a/c;->a()V

    .line 255
    new-instance v0, Lcom/google/android/location/h/j;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/location/h/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/h/h;->w:Lcom/google/android/location/h/j;

    .line 257
    new-instance v0, Las/d;

    iget-object v1, p0, Lcom/google/android/location/h/h;->c:Las/c;

    new-instance v2, Lcom/google/android/location/h/h$1;

    invoke-direct {v2, p0}, Lcom/google/android/location/h/h$1;-><init>(Lcom/google/android/location/h/h;)V

    invoke-direct {v0, v1, v2}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/location/h/h;->j:Las/d;

    .line 274
    new-instance v0, Lcom/google/android/location/h/h$2;

    iget-object v1, p0, Lcom/google/android/location/h/h;->c:Las/c;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/h/h$2;-><init>(Lcom/google/android/location/h/h;Las/c;)V

    iput-object v0, p0, Lcom/google/android/location/h/h;->k:Las/d;

    .line 281
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/h/h;I)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/location/h/h;->s:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/location/h/h;->s:I

    return v0
.end method

.method private a(Lcom/google/android/location/h/b/o;Lcom/google/android/location/h/b/h;)Lcom/google/android/location/h/b/n;
    .registers 13
    .parameter
    .parameter

    .prologue
    .line 1104
    invoke-virtual {p1}, Lcom/google/android/location/h/b/o;->v()Ljava/lang/String;

    move-result-object v3

    .line 1105
    new-instance v4, Lcom/google/android/location/h/b/i;

    invoke-virtual {p2}, Lcom/google/android/location/h/b/h;->d()I

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/location/h/b/h;->g()I

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/location/h/b/h;->e()Lcom/google/android/location/h/b/b;

    move-result-object v2

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/location/h/b/i;-><init>(IILcom/google/android/location/h/b/b;)V

    .line 1108
    new-instance v5, Ljava/util/Hashtable;

    invoke-direct {v5}, Ljava/util/Hashtable;-><init>()V

    .line 1110
    invoke-virtual {p2}, Lcom/google/android/location/h/b/h;->f()[Lcom/google/android/location/h/b/b;

    move-result-object v6

    .line 1112
    const/4 v0, 0x0

    move v2, v0

    :goto_20
    array-length v0, v6

    if-ge v2, v0, :cond_69

    .line 1113
    aget-object v7, v6, v2

    .line 1114
    invoke-virtual {v7}, Lcom/google/android/location/h/b/b;->g()Ljava/util/Hashtable;

    move-result-object v0

    const-string v1, "Content-Location"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1116
    if-eqz v0, :cond_65

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_65

    .line 1119
    invoke-virtual {v5, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/h/b/i;

    .line 1121
    if-nez v1, :cond_61

    .line 1122
    invoke-virtual {v7}, Lcom/google/android/location/h/b/b;->g()Ljava/util/Hashtable;

    move-result-object v1

    const-string v8, "X-Masf-Response-Code"

    invoke-virtual {v1, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1123
    new-instance v8, Lcom/google/android/location/h/b/i;

    invoke-virtual {p2}, Lcom/google/android/location/h/b/h;->d()I

    move-result v9

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v8, v9, v1, v7}, Lcom/google/android/location/h/b/i;-><init>(IILcom/google/android/location/h/b/b;)V

    .line 1125
    invoke-virtual {v5, v0, v8}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112
    :goto_5d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_20

    .line 1127
    :cond_61
    invoke-virtual {v1, v7}, Lcom/google/android/location/h/b/i;->a(Lcom/google/android/location/h/b/b;)V

    goto :goto_5d

    .line 1132
    :cond_65
    invoke-virtual {v4, v7}, Lcom/google/android/location/h/b/i;->a(Lcom/google/android/location/h/b/b;)V

    goto :goto_5d

    .line 1137
    :cond_69
    invoke-virtual {v5}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    .line 1139
    iget-object v2, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    monitor-enter v2

    .line 1140
    :goto_70
    :try_start_70
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_8d

    .line 1141
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    .line 1142
    invoke-virtual {v5, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/b/i;

    .line 1143
    iget-object v6, p0, Lcom/google/android/location/h/h;->w:Lcom/google/android/location/h/j;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/i;->a()Lcom/google/android/location/h/b/h;

    move-result-object v0

    invoke-virtual {v6, v3, v0}, Lcom/google/android/location/h/j;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_70

    .line 1145
    :catchall_8a
    move-exception v0

    monitor-exit v2
    :try_end_8c
    .catchall {:try_start_70 .. :try_end_8c} :catchall_8a

    throw v0

    :cond_8d
    :try_start_8d
    monitor-exit v2
    :try_end_8e
    .catchall {:try_start_8d .. :try_end_8e} :catchall_8a

    .line 1147
    invoke-virtual {v4}, Lcom/google/android/location/h/b/i;->a()Lcom/google/android/location/h/b/h;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/h/h;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    return-object v0
.end method

.method private a(I)V
    .registers 5
    .parameter

    .prologue
    .line 516
    invoke-direct {p0}, Lcom/google/android/location/h/h;->q()[Lcom/google/android/location/h/h$b;

    move-result-object v1

    .line 518
    const/4 v0, 0x0

    :goto_5
    array-length v2, v1

    if-ge v0, v2, :cond_10

    .line 519
    aget-object v2, v1, v0

    invoke-interface {v2, p1}, Lcom/google/android/location/h/h$b;->a(I)V

    .line 518
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 521
    :cond_10
    return-void
.end method

.method private a(J)V
    .registers 7
    .parameter

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    .line 402
    :cond_6
    :goto_6
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 403
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/b/m;

    .line 404
    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->n()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-gtz v2, :cond_6

    .line 405
    iget-object v2, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    invoke-virtual {v2, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->q()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2e

    .line 407
    iget-object v2, p0, Lcom/google/android/location/h/h;->g:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    :cond_2e
    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/h/b/m;->a(J)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 415
    iget-object v2, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_6

    .line 420
    :cond_3a
    return-void
.end method

.method private a(Lcom/google/android/location/h/a/b;[Lcom/google/android/location/h/b/m;)V
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1000
    invoke-interface {p1}, Lcom/google/android/location/h/a/b;->d_()I

    move-result v1

    .line 1001
    invoke-interface {p1}, Lcom/google/android/location/h/a/b;->h()Ljava/lang/String;

    move-result-object v2

    .line 1003
    const/16 v3, 0x1f6

    if-ne v1, v3, :cond_17

    .line 1007
    new-instance v2, Lcom/google/android/location/h/e;

    invoke-direct {v2, v1}, Lcom/google/android/location/h/e;-><init>(I)V

    invoke-direct {p0, p2, v2}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    .line 1101
    :goto_16
    return-void

    .line 1008
    :cond_17
    const/16 v3, 0xc8

    if-eq v1, v3, :cond_28

    .line 1013
    new-instance v2, Lcom/google/android/location/h/e;

    invoke-direct {v2, v1}, Lcom/google/android/location/h/e;-><init>(I)V

    invoke-direct {p0, p2, v2}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    .line 1014
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/android/location/h/h;->a(I)V

    goto :goto_16

    .line 1015
    :cond_28
    if-eqz v2, :cond_32

    const-string v1, "application/binary"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3d

    .line 1020
    :cond_32
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Bad content-type"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    goto :goto_16

    .line 1023
    :cond_3d
    :try_start_3d
    iget v1, p0, Lcom/google/android/location/h/h;->t:I

    int-to-long v1, v1

    invoke-interface {p1}, Lcom/google/android/location/h/a/b;->e()J

    move-result-wide v6

    add-long/2addr v1, v6

    long-to-int v1, v1

    iput v1, p0, Lcom/google/android/location/h/h;->t:I

    .line 1027
    invoke-interface {p1}, Lcom/google/android/location/h/a/b;->j()Ljava/io/DataInputStream;

    move-result-object v1

    .line 1028
    new-instance v7, Lcom/google/android/location/h/b/l;

    invoke-direct {v7, v1}, Lcom/google/android/location/h/b/l;-><init>(Ljava/io/DataInputStream;)V

    move v6, v5

    .line 1030
    :goto_52
    array-length v1, p2

    if-ge v6, v1, :cond_d9

    .line 1031
    invoke-virtual {v7}, Lcom/google/android/location/h/b/l;->b()Lcom/google/android/location/h/b/n;

    move-result-object v3

    .line 1033
    if-eqz v3, :cond_d9

    move v1, v5

    .line 1046
    :goto_5c
    array-length v2, p2

    if-ge v1, v2, :cond_f6

    .line 1047
    aget-object v2, p2, v1

    .line 1048
    if-eqz v2, :cond_a7

    invoke-virtual {v2}, Lcom/google/android/location/h/b/m;->d()I

    move-result v8

    invoke-virtual {v3}, Lcom/google/android/location/h/b/n;->d()I

    move-result v9

    if-ne v8, v9, :cond_a7

    .line 1049
    const/4 v8, 0x0

    aput-object v8, p2, v1

    .line 1055
    :goto_70
    if-eqz v2, :cond_bb

    .line 1058
    instance-of v1, v3, Lcom/google/android/location/h/b/h;

    if-eqz v1, :cond_84

    instance-of v1, v2, Lcom/google/android/location/h/b/o;

    if-eqz v1, :cond_84

    .line 1059
    move-object v0, v2

    check-cast v0, Lcom/google/android/location/h/b/o;

    move-object v1, v0

    check-cast v3, Lcom/google/android/location/h/b/h;

    invoke-direct {p0, v1, v3}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/o;Lcom/google/android/location/h/b/h;)Lcom/google/android/location/h/b/n;

    move-result-object v3

    .line 1065
    :cond_84
    invoke-virtual {v3}, Lcom/google/android/location/h/b/n;->g()I

    move-result v1

    .line 1066
    const/16 v8, 0x226

    if-ne v1, v8, :cond_aa

    .line 1067
    invoke-virtual {v3}, Lcom/google/android/location/h/b/n;->h()V

    .line 1068
    new-instance v3, Lcom/google/android/location/h/e;

    invoke-direct {v3, v1}, Lcom/google/android/location/h/e;-><init>(I)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v8

    invoke-direct {p0, v2, v3, v8, v9}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;J)Z

    .line 1030
    :goto_a3
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_52

    .line 1046
    :cond_a7
    add-int/lit8 v1, v1, 0x1

    goto :goto_5c

    .line 1071
    :cond_aa
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V
    :try_end_ad
    .catchall {:try_start_3d .. :try_end_ad} :catchall_e8
    .catch Ljava/io/IOException; {:try_start_3d .. :try_end_ad} :catch_ae
    .catch Ljava/lang/Throwable; {:try_start_3d .. :try_end_ad} :catch_cc

    goto :goto_a3

    .line 1093
    :catch_ae
    move-exception v1

    .line 1098
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Request didn\'t complete"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    goto/16 :goto_16

    .line 1079
    :cond_bb
    :try_start_bb
    invoke-virtual {v3}, Lcom/google/android/location/h/b/n;->b_()I

    move-result v1

    new-array v1, v1, [B

    .line 1080
    invoke-virtual {v3}, Lcom/google/android/location/h/b/n;->c_()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I
    :try_end_c8
    .catchall {:try_start_bb .. :try_end_c8} :catchall_e8
    .catch Ljava/io/IOException; {:try_start_bb .. :try_end_c8} :catch_f4
    .catch Ljava/lang/Throwable; {:try_start_bb .. :try_end_c8} :catch_cc

    .line 1085
    :goto_c8
    :try_start_c8
    invoke-virtual {v3}, Lcom/google/android/location/h/b/n;->h()V
    :try_end_cb
    .catchall {:try_start_c8 .. :try_end_cb} :catchall_e8
    .catch Ljava/io/IOException; {:try_start_c8 .. :try_end_cb} :catch_ae
    .catch Ljava/lang/Throwable; {:try_start_c8 .. :try_end_cb} :catch_cc

    goto :goto_a3

    .line 1095
    :catch_cc
    move-exception v1

    .line 1098
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Request didn\'t complete"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    goto/16 :goto_16

    .line 1092
    :cond_d9
    :try_start_d9
    invoke-virtual {v7}, Lcom/google/android/location/h/b/l;->a()V
    :try_end_dc
    .catchall {:try_start_d9 .. :try_end_dc} :catchall_e8
    .catch Ljava/io/IOException; {:try_start_d9 .. :try_end_dc} :catch_ae
    .catch Ljava/lang/Throwable; {:try_start_d9 .. :try_end_dc} :catch_cc

    .line 1098
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Request didn\'t complete"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    goto/16 :goto_16

    :catchall_e8
    move-exception v1

    new-instance v2, Ljava/io/IOException;

    const-string v3, "Request didn\'t complete"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v2}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    throw v1

    .line 1081
    :catch_f4
    move-exception v1

    goto :goto_c8

    :cond_f6
    move-object v2, v4

    goto/16 :goto_70
.end method

.method private a(Lcom/google/android/location/h/b/m;)V
    .registers 5
    .parameter

    .prologue
    .line 527
    invoke-direct {p0}, Lcom/google/android/location/h/h;->q()[Lcom/google/android/location/h/h$b;

    move-result-object v1

    .line 529
    const/4 v0, 0x0

    :goto_5
    array-length v2, v1

    if-ge v0, v2, :cond_10

    .line 530
    aget-object v2, v1, v0

    invoke-interface {v2, p1}, Lcom/google/android/location/h/h$b;->a(Lcom/google/android/location/h/b/m;)V

    .line 529
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 532
    :cond_10
    return-void
.end method

.method private a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1156
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/location/h/b/m;->s()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1158
    invoke-virtual {p1}, Lcom/google/android/location/h/b/m;->i()Lcom/google/android/location/h/b/m$a;

    move-result-object v0

    .line 1159
    if-eqz v0, :cond_f

    .line 1160
    invoke-interface {v0, p1, p2}, Lcom/google/android/location/h/b/m$a;->a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V

    .line 1162
    :cond_f
    invoke-direct {p0, p1}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;)V
    :try_end_12
    .catchall {:try_start_0 .. :try_end_12} :catchall_1e
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_12} :catch_16

    .line 1170
    :cond_12
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->h()V

    .line 1172
    :goto_15
    return-void

    .line 1164
    :catch_16
    move-exception v0

    .line 1165
    :try_start_17
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_1a
    .catchall {:try_start_17 .. :try_end_1a} :catchall_1e

    .line 1170
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->h()V

    goto :goto_15

    :catchall_1e
    move-exception v0

    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->h()V

    throw v0
.end method

.method public static declared-synchronized a(Lcom/google/android/location/h/h$a;)V
    .registers 3
    .parameter

    .prologue
    .line 204
    const-class v1, Lcom/google/android/location/h/h;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/location/h/h;->b:Lcom/google/android/location/h/h;

    if-nez v0, :cond_e

    .line 205
    new-instance v0, Lcom/google/android/location/h/h;

    invoke-direct {v0, p0}, Lcom/google/android/location/h/h;-><init>(Lcom/google/android/location/h/h$a;)V

    sput-object v0, Lcom/google/android/location/h/h;->b:Lcom/google/android/location/h/h;
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_10

    .line 207
    :cond_e
    monitor-exit v1

    return-void

    .line 204
    :catchall_10
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/h/h;Lcom/google/android/location/h/a/b;[Lcom/google/android/location/h/b/m;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/a/b;[Lcom/google/android/location/h/b/m;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/h/h;[Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/h/h;[Ljava/lang/Object;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/location/h/h;->a([Ljava/lang/Object;)V

    return-void
.end method

.method private a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 760
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    .line 761
    iget-object v4, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    monitor-enter v4

    move v1, v0

    .line 764
    :goto_11
    :try_start_11
    array-length v5, p1

    if-ge v1, v5, :cond_26

    .line 765
    aget-object v5, p1, v1

    if-eqz v5, :cond_23

    .line 766
    aget-object v5, p1, v1

    invoke-direct {p0, v5, p2, v2, v3}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;J)Z

    move-result v5

    if-eqz v5, :cond_23

    .line 769
    const/4 v5, 0x0

    aput-object v5, p1, v1

    .line 764
    :cond_23
    add-int/lit8 v1, v1, 0x1

    goto :goto_11

    .line 773
    :cond_26
    invoke-direct {p0}, Lcom/google/android/location/h/h;->p()V

    .line 774
    monitor-exit v4
    :try_end_2a
    .catchall {:try_start_11 .. :try_end_2a} :catchall_49

    .line 779
    :goto_2a
    array-length v1, p1

    if-ge v0, v1, :cond_4c

    .line 780
    aget-object v1, p1, v0

    if-eqz v1, :cond_46

    .line 781
    aget-object v1, p1, v0

    invoke-virtual {v1}, Lcom/google/android/location/h/b/m;->s()Z

    move-result v1

    if-eqz v1, :cond_46

    .line 783
    aget-object v1, p1, v0

    invoke-virtual {v1}, Lcom/google/android/location/h/b/m;->i()Lcom/google/android/location/h/b/m$a;

    move-result-object v1

    .line 784
    if-eqz v1, :cond_46

    .line 785
    aget-object v2, p1, v0

    invoke-interface {v1, v2, p2}, Lcom/google/android/location/h/b/m$a;->a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    .line 779
    :cond_46
    add-int/lit8 v0, v0, 0x1

    goto :goto_2a

    .line 774
    :catchall_49
    move-exception v0

    :try_start_4a
    monitor-exit v4
    :try_end_4b
    .catchall {:try_start_4a .. :try_end_4b} :catchall_49

    throw v0

    .line 790
    :cond_4c
    return-void
.end method

.method private a([Lcom/google/android/location/h/b/m;Z)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 819
    :try_start_0
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 820
    iget-object v1, p0, Lcom/google/android/location/h/h;->d:Lcom/google/android/location/h/b/d;

    invoke-virtual {v1}, Lcom/google/android/location/h/b/d;->b_()I

    move-result v3

    .line 822
    const/4 v1, 0x0

    move v4, v1

    :goto_d
    array-length v1, p1

    if-ge v4, v1, :cond_68

    .line 823
    aget-object v2, p1, v4

    .line 824
    if-nez v2, :cond_1a

    move v1, v3

    .line 822
    :goto_15
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v1

    goto :goto_d

    .line 832
    :cond_1a
    instance-of v1, v2, Lcom/google/android/location/h/b/o;

    if-eqz v1, :cond_46

    .line 833
    move-object v0, v2

    check-cast v0, Lcom/google/android/location/h/b/o;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/google/android/location/h/b/o;->v()Ljava/lang/String;

    move-result-object v1

    .line 836
    if-eqz v1, :cond_46

    .line 838
    iget-object v6, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    monitor-enter v6
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_2b} :catch_41

    .line 839
    :try_start_2b
    iget-object v7, p0, Lcom/google/android/location/h/h;->w:Lcom/google/android/location/h/j;

    invoke-virtual {v7, v1}, Lcom/google/android/location/h/j;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 840
    monitor-exit v6
    :try_end_32
    .catchall {:try_start_2b .. :try_end_32} :catchall_3e

    .line 842
    if-eqz v1, :cond_46

    .line 843
    const/4 v6, 0x0

    :try_start_35
    aput-object v6, p1, v4

    .line 844
    check-cast v1, Lcom/google/android/location/h/b/n;

    invoke-direct {p0, v2, v1}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V
    :try_end_3c
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_3c} :catch_41

    move v1, v3

    .line 845
    goto :goto_15

    .line 840
    :catchall_3e
    move-exception v1

    :try_start_3f
    monitor-exit v6
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_3e

    :try_start_40
    throw v1
    :try_end_41
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_41} :catch_41

    .line 868
    :catch_41
    move-exception v1

    .line 869
    invoke-direct {p0, p1, v1}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V

    .line 871
    :cond_45
    :goto_45
    return-void

    .line 851
    :cond_46
    :try_start_46
    invoke-virtual {v2}, Lcom/google/android/location/h/b/m;->b_()I

    move-result v1

    add-int/2addr v1, v3

    const v6, 0x8000

    if-le v1, v6, :cond_5c

    .line 852
    invoke-virtual {p0, v5, p2}, Lcom/google/android/location/h/h;->a(Ljava/util/Vector;Z)V

    .line 853
    iget-object v1, p0, Lcom/google/android/location/h/h;->d:Lcom/google/android/location/h/b/d;

    invoke-virtual {v1}, Lcom/google/android/location/h/b/d;->b_()I

    move-result v3

    .line 854
    invoke-virtual {v5}, Ljava/util/Vector;->removeAllElements()V

    .line 856
    :cond_5c
    const/4 v1, 0x0

    aput-object v1, p1, v4

    .line 857
    invoke-virtual {v5, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 858
    invoke-virtual {v2}, Lcom/google/android/location/h/b/m;->b_()I

    move-result v1

    add-int/2addr v1, v3

    goto :goto_15

    .line 863
    :cond_68
    invoke-direct {p0}, Lcom/google/android/location/h/h;->p()V

    .line 865
    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_45

    .line 866
    invoke-virtual {p0, v5, p2}, Lcom/google/android/location/h/h;->a(Ljava/util/Vector;Z)V
    :try_end_74
    .catch Ljava/io/IOException; {:try_start_46 .. :try_end_74} :catch_41

    goto :goto_45
.end method

.method private a([Ljava/lang/Object;)V
    .registers 4
    .parameter

    .prologue
    .line 740
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    array-length v0, p1

    if-ge v1, v0, :cond_12

    .line 741
    aget-object v0, p1, v1

    check-cast v0, Lcom/google/android/location/h/b/m;

    .line 742
    if-eqz v0, :cond_e

    .line 743
    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->a()V

    .line 740
    :cond_e
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 746
    :cond_12
    return-void
.end method

.method private a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;J)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 804
    invoke-virtual {p1, p3, p4}, Lcom/google/android/location/h/b/m;->a(J)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 805
    invoke-virtual {p1, p3, p4}, Lcom/google/android/location/h/b/m;->b(J)V

    .line 806
    iget-object v0, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    const-string v1, ""

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 807
    const/4 v0, 0x1

    .line 809
    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private a([Lcom/google/android/location/h/b/m;)[Lcom/google/android/location/h/b/m;
    .registers 9
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 361
    .line 362
    const/4 v1, 0x1

    move v3, v2

    move-object v0, v4

    .line 363
    :goto_5
    array-length v5, p1

    if-ge v3, v5, :cond_2f

    .line 364
    aget-object v5, p1, v3

    invoke-virtual {v5}, Lcom/google/android/location/h/b/m;->l()Z

    move-result v5

    if-eqz v5, :cond_24

    .line 365
    if-nez v0, :cond_15

    .line 366
    array-length v0, p1

    new-array v0, v0, [Lcom/google/android/location/h/b/m;

    .line 368
    :cond_15
    aget-object v5, p1, v3

    aput-object v5, v0, v3

    .line 369
    aput-object v4, p1, v3

    move v6, v1

    move-object v1, v0

    move v0, v6

    .line 363
    :goto_1e
    add-int/lit8 v3, v3, 0x1

    move v6, v0

    move-object v0, v1

    move v1, v6

    goto :goto_5

    .line 370
    :cond_24
    aget-object v5, p1, v3

    invoke-virtual {v5}, Lcom/google/android/location/h/b/m;->j()Z

    move-result v5

    if-eqz v5, :cond_44

    move-object v1, v0

    move v0, v2

    .line 374
    goto :goto_1e

    .line 378
    :cond_2f
    if-eqz v1, :cond_43

    if-eqz v0, :cond_43

    .line 379
    :goto_33
    array-length v1, p1

    if-ge v2, v1, :cond_43

    .line 380
    aget-object v1, p1, v2

    if-eqz v1, :cond_40

    .line 381
    aget-object v1, p1, v2

    aput-object v1, v0, v2

    .line 382
    aput-object v4, p1, v2

    .line 379
    :cond_40
    add-int/lit8 v2, v2, 0x1

    goto :goto_33

    .line 387
    :cond_43
    return-object v0

    :cond_44
    move v6, v1

    move-object v1, v0

    move v0, v6

    goto :goto_1e
.end method

.method private b([Lcom/google/android/location/h/b/m;)Ljava/io/InputStream;
    .registers 6
    .parameter

    .prologue
    .line 981
    array-length v0, p1

    new-array v1, v0, [Ljava/io/InputStream;

    .line 983
    const/4 v0, 0x0

    :goto_4
    array-length v2, p1

    if-ge v0, v2, :cond_12

    .line 984
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/google/android/location/h/b/m;->c_()Ljava/io/InputStream;

    move-result-object v2

    aput-object v2, v1, v0

    .line 983
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 987
    :cond_12
    new-instance v0, Lcom/google/googlenav/common/io/SequenceInputStream;

    iget-object v2, p0, Lcom/google/android/location/h/h;->d:Lcom/google/android/location/h/b/d;

    invoke-virtual {v2}, Lcom/google/android/location/h/b/d;->c_()Ljava/io/InputStream;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/common/io/SequenceInputStream;

    invoke-direct {v3, v1}, Lcom/google/googlenav/common/io/SequenceInputStream;-><init>([Ljava/io/InputStream;)V

    invoke-direct {v0, v2, v3}, Lcom/google/googlenav/common/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    .line 990
    return-object v0
.end method

.method private b(Z)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 880
    if-eqz p1, :cond_b

    invoke-direct {p0}, Lcom/google/android/location/h/h;->n()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 881
    iget-object v0, p0, Lcom/google/android/location/h/h;->o:Ljava/lang/String;

    .line 883
    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, Lcom/google/android/location/h/h;->n:Ljava/lang/String;

    goto :goto_a
.end method

.method private b(J)V
    .registers 7
    .parameter

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_2d

    .line 430
    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/b/m;

    .line 432
    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->j()Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/h/b/m;->a(J)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 433
    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/h/b/m;->c(J)V

    .line 434
    iget-object v2, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    const-string v3, ""

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    :cond_29
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 437
    :cond_2d
    return-void
.end method

.method static synthetic b(Lcom/google/android/location/h/h;)V
    .registers 1
    .parameter

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/location/h/h;->p()V

    return-void
.end method

.method public static declared-synchronized g()Lcom/google/android/location/h/h;
    .registers 2

    .prologue
    .line 230
    const-class v0, Lcom/google/android/location/h/h;

    monitor-enter v0

    :try_start_3
    sget-object v1, Lcom/google/android/location/h/h;->b:Lcom/google/android/location/h/h;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v0

    return-object v1

    :catchall_7
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private n()Z
    .registers 2

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/location/h/h;->o:Ljava/lang/String;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private o()V
    .registers 3

    .prologue
    .line 440
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 441
    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/b/m;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->m()V

    .line 440
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 443
    :cond_19
    return-void
.end method

.method private p()V
    .registers 10

    .prologue
    const-wide/16 v4, -0x1

    .line 446
    iget-object v6, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    monitor-enter v6

    .line 447
    :try_start_5
    iget-object v0, p0, Lcom/google/android/location/h/h;->k:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    .line 450
    iget-object v0, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v7

    move-wide v2, v4

    .line 451
    :goto_11
    invoke-interface {v7}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 452
    invoke-interface {v7}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/b/m;

    .line 453
    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->t()Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 454
    iget-object v1, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->q()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4a

    .line 456
    iget-object v1, p0, Lcom/google/android/location/h/h;->g:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-wide v0, v2

    :cond_38
    :goto_38
    move-wide v2, v0

    .line 466
    goto :goto_11

    .line 459
    :cond_3a
    invoke-virtual {v0}, Lcom/google/android/location/h/b/m;->o()J

    move-result-wide v0

    .line 460
    cmp-long v8, v0, v4

    if-eqz v8, :cond_4a

    cmp-long v8, v2, v4

    if-eqz v8, :cond_38

    cmp-long v8, v2, v0

    if-gtz v8, :cond_38

    :cond_4a
    move-wide v0, v2

    goto :goto_38

    .line 467
    :cond_4c
    cmp-long v0, v2, v4

    if-nez v0, :cond_52

    .line 468
    monitor-exit v6

    .line 474
    :goto_51
    return-void

    .line 471
    :cond_52
    iget-object v0, p0, Lcom/google/android/location/h/h;->k:Las/d;

    invoke-virtual {v0, v2, v3}, Las/d;->b(J)V

    .line 472
    iget-object v0, p0, Lcom/google/android/location/h/h;->k:Las/d;

    invoke-virtual {v0}, Las/d;->g()V

    .line 473
    monitor-exit v6

    goto :goto_51

    :catchall_5e
    move-exception v0

    monitor-exit v6
    :try_end_60
    .catchall {:try_start_5 .. :try_end_60} :catchall_5e

    throw v0
.end method

.method private q()[Lcom/google/android/location/h/h$b;
    .registers 4

    .prologue
    .line 504
    iget-object v1, p0, Lcom/google/android/location/h/h;->p:Ljava/util/Vector;

    monitor-enter v1

    .line 505
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/h/h;->p:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/location/h/h$b;

    .line 506
    iget-object v2, p0, Lcom/google/android/location/h/h;->p:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    .line 507
    monitor-exit v1

    .line 509
    return-object v0

    .line 507
    :catchall_12
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    throw v0
.end method

.method private declared-synchronized r()I
    .registers 2

    .prologue
    .line 727
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/location/h/h;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/h/h;->r:I
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    monitor-exit p0

    return v0

    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Z)Lcom/google/googlenav/common/io/GoogleHttpConnection;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 545
    new-instance v0, Lcom/google/android/location/h/i;

    invoke-direct {v0, p1, p2}, Lcom/google/android/location/h/i;-><init>(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public a(Lcom/google/android/location/h/b/m;Z)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 599
    iget-object v1, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    monitor-enter v1

    .line 600
    :try_start_3
    invoke-virtual {p1}, Lcom/google/android/location/h/b/m;->r()V

    .line 602
    invoke-direct {p0}, Lcom/google/android/location/h/h;->r()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/location/h/b/m;->a(I)V

    .line 604
    if-eqz p2, :cond_26

    invoke-virtual {p1}, Lcom/google/android/location/h/b/m;->n()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-nez v0, :cond_26

    .line 605
    invoke-virtual {p1}, Lcom/google/android/location/h/b/m;->k()V

    .line 606
    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 607
    invoke-virtual {p0}, Lcom/google/android/location/h/h;->m()V

    .line 612
    :goto_24
    monitor-exit v1

    .line 613
    return-void

    .line 609
    :cond_26
    iget-object v0, p0, Lcom/google/android/location/h/h;->f:Ljava/util/Hashtable;

    const-string v2, ""

    invoke-virtual {v0, p1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 610
    invoke-direct {p0}, Lcom/google/android/location/h/h;->p()V

    goto :goto_24

    .line 612
    :catchall_31
    move-exception v0

    monitor-exit v1
    :try_end_33
    .catchall {:try_start_3 .. :try_end_33} :catchall_31

    throw v0
.end method

.method protected a(Ljava/util/Vector;Z)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 925
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v4, v0, [Lcom/google/android/location/h/b/m;

    .line 927
    invoke-virtual {p1, v4}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    .line 929
    invoke-direct {p0, v4}, Lcom/google/android/location/h/h;->b([Lcom/google/android/location/h/b/m;)Ljava/io/InputStream;

    move-result-object v0

    .line 931
    invoke-direct {p0, p2}, Lcom/google/android/location/h/h;->b(Z)Ljava/lang/String;

    move-result-object v1

    .line 933
    iget-object v2, p0, Lcom/google/android/location/h/h;->l:Lcom/google/android/location/h/a/c;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Lcom/google/android/location/h/a/c;->a(Ljava/lang/String;I)Lcom/google/android/location/h/a/b;

    move-result-object v3

    .line 935
    const-string v1, "POST"

    invoke-interface {v3, v1}, Lcom/google/android/location/h/a/b;->a(Ljava/lang/String;)V

    .line 936
    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v5

    .line 937
    invoke-interface {v3, v0}, Lcom/google/android/location/h/a/b;->a(Ljava/io/InputStream;)V

    .line 938
    iget-wide v0, p0, Lcom/google/android/location/h/h;->u:J

    invoke-interface {v3, v0, v1}, Lcom/google/android/location/h/a/b;->a(J)V

    .line 939
    const-string v0, "application/binary"

    invoke-interface {v3, v0}, Lcom/google/android/location/h/a/b;->b(Ljava/lang/String;)V

    .line 943
    new-instance v0, Lcom/google/android/location/h/h$3;

    iget-object v2, p0, Lcom/google/android/location/h/h;->c:Las/c;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/h/h$3;-><init>(Lcom/google/android/location/h/h;Las/c;Lcom/google/android/location/h/a/b;[Lcom/google/android/location/h/b/m;I)V

    .line 973
    invoke-interface {v3, v0}, Lcom/google/android/location/h/a/b;->b(Las/a;)V

    .line 974
    return-void
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 1182
    iget-object v0, p0, Lcom/google/android/location/h/h;->m:Lcom/google/googlenav/common/io/g;

    invoke-interface {v0}, Lcom/google/googlenav/common/io/g;->e()Z

    move-result v0

    return v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 1178
    iget-object v0, p0, Lcom/google/android/location/h/h;->m:Lcom/google/googlenav/common/io/g;

    invoke-interface {v0}, Lcom/google/googlenav/common/io/g;->f()I

    move-result v0

    return v0
.end method

.method protected h()Las/c;
    .registers 5

    .prologue
    .line 285
    new-instance v0, Las/c;

    new-instance v1, Lar/b;

    invoke-direct {v1}, Lar/b;-><init>()V

    const-string v2, "MobileServiceMux TaskRunner"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Las/c;-><init>(Lar/d;Ljava/lang/String;I)V

    return-object v0
.end method

.method protected i()Lcom/google/android/location/h/a/c;
    .registers 7

    .prologue
    .line 291
    new-instance v0, Lcom/google/android/location/h/a/c;

    iget-object v1, p0, Lcom/google/android/location/h/h;->c:Las/c;

    new-instance v2, Lar/b;

    invoke-direct {v2}, Lar/b;-><init>()V

    iget-object v3, p0, Lcom/google/android/location/h/h;->m:Lcom/google/googlenav/common/io/g;

    const-string v4, "MobileServiceMux AsyncHttpRequestFactory"

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/h/a/c;-><init>(Las/c;Lar/d;Lcom/google/googlenav/common/io/g;Ljava/lang/String;I)V

    return-object v0
.end method

.method protected j()V
    .registers 5

    .prologue
    .line 319
    iget-object v1, p0, Lcom/google/android/location/h/h;->q:Ljava/lang/Object;

    monitor-enter v1

    .line 320
    :try_start_3
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    .line 322
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/h/h;->a(J)V

    .line 324
    invoke-direct {p0, v2, v3}, Lcom/google/android/location/h/h;->b(J)V

    .line 329
    invoke-direct {p0}, Lcom/google/android/location/h/h;->o()V

    .line 331
    invoke-direct {p0}, Lcom/google/android/location/h/h;->p()V

    .line 333
    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_25

    .line 334
    monitor-exit v1

    .line 350
    :goto_24
    return-void

    .line 337
    :cond_25
    iget-object v0, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/location/h/b/m;

    .line 338
    iget-object v2, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    .line 340
    iget-object v2, p0, Lcom/google/android/location/h/h;->e:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->removeAllElements()V

    .line 341
    monitor-exit v1
    :try_end_38
    .catchall {:try_start_3 .. :try_end_38} :catchall_4d

    .line 343
    invoke-direct {p0}, Lcom/google/android/location/h/h;->n()Z

    move-result v1

    if-eqz v1, :cond_48

    .line 344
    invoke-direct {p0, v0}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;)[Lcom/google/android/location/h/b/m;

    move-result-object v1

    .line 345
    if-eqz v1, :cond_48

    .line 346
    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Z)V

    .line 349
    :cond_48
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/h/h;->a([Lcom/google/android/location/h/b/m;Z)V

    goto :goto_24

    .line 341
    :catchall_4d
    move-exception v0

    :try_start_4e
    monitor-exit v1
    :try_end_4f
    .catchall {:try_start_4e .. :try_end_4f} :catchall_4d

    throw v0
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 572
    invoke-virtual {p0}, Lcom/google/android/location/h/h;->l()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 573
    iget-boolean v0, p0, Lcom/google/android/location/h/h;->v:Z

    .line 575
    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public l()Z
    .registers 3

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/android/location/h/h;->n:Ljava/lang/String;

    const-string v1, "https:/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected m()V
    .registers 9

    .prologue
    .line 669
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    .line 670
    const-wide/16 v2, 0xa

    add-long/2addr v2, v0

    .line 677
    iget-wide v4, p0, Lcom/google/android/location/h/h;->i:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_2b

    .line 680
    const-wide/16 v4, 0x64

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/location/h/h;->h:J

    .line 681
    iput-wide v2, p0, Lcom/google/android/location/h/h;->i:J

    .line 684
    iget-object v0, p0, Lcom/google/android/location/h/h;->j:Las/d;

    iget-wide v1, p0, Lcom/google/android/location/h/h;->i:J

    invoke-virtual {v0, v1, v2}, Las/d;->b(J)V

    .line 685
    iget-object v0, p0, Lcom/google/android/location/h/h;->j:Las/d;

    invoke-virtual {v0}, Las/d;->g()V

    .line 698
    :cond_2a
    :goto_2a
    return-void

    .line 686
    :cond_2b
    iget-wide v0, p0, Lcom/google/android/location/h/h;->h:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_34

    .line 689
    iput-wide v2, p0, Lcom/google/android/location/h/h;->i:J

    goto :goto_2a

    .line 690
    :cond_34
    iget-wide v0, p0, Lcom/google/android/location/h/h;->i:J

    iget-wide v2, p0, Lcom/google/android/location/h/h;->h:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_2a

    .line 693
    iget-wide v0, p0, Lcom/google/android/location/h/h;->h:J

    iput-wide v0, p0, Lcom/google/android/location/h/h;->i:J

    goto :goto_2a
.end method
