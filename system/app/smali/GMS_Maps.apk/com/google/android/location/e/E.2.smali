.class public Lcom/google/android/location/e/E;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/e/E$a;
    }
.end annotation


# instance fields
.field public final a:J

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/e/E$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/util/ArrayList;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/e/E$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    iput-wide p1, p0, Lcom/google/android/location/e/E;->a:J

    .line 164
    iput-object p3, p0, Lcom/google/android/location/e/E;->b:Ljava/util/ArrayList;

    .line 165
    return-void
.end method

.method public static a(Ljava/io/PrintWriter;Lcom/google/android/location/e/E;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 211
    if-nez p1, :cond_8

    .line 212
    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 223
    :goto_7
    return-void

    .line 215
    :cond_8
    const-string v0, "WifiScan [deliveryTime="

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 216
    iget-wide v0, p1, Lcom/google/android/location/e/E;->a:J

    invoke-virtual {p0, v0, v1}, Ljava/io/PrintWriter;->print(J)V

    .line 217
    const-string v0, ", devices=["

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 218
    iget-object v0, p1, Lcom/google/android/location/e/E;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/E$a;

    .line 219
    invoke-static {p0, v0}, Lcom/google/android/location/e/E$a;->a(Ljava/io/PrintWriter;Lcom/google/android/location/e/E$a;)V

    .line 220
    const-string v0, ", "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_1d

    .line 222
    :cond_32
    const-string v0, "]]"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_7
.end method

.method public static a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/E;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 196
    if-nez p1, :cond_8

    .line 197
    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    :goto_7
    return-void

    .line 200
    :cond_8
    const-string v0, "WifiScan [deliveryTime="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    iget-wide v0, p1, Lcom/google/android/location/e/E;->a:J

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 202
    const-string v0, ", devices=["

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    iget-object v0, p1, Lcom/google/android/location/e/E;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/E$a;

    .line 204
    invoke-static {p0, v0}, Lcom/google/android/location/e/E$a;->a(Ljava/lang/StringBuilder;Lcom/google/android/location/e/E$a;)V

    .line 205
    const-string v0, ", "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1d

    .line 207
    :cond_32
    const-string v0, "]]"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/location/e/E;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/google/android/location/e/E$a;
    .registers 3
    .parameter

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/location/e/E;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/E$a;

    return-object v0
.end method

.method public final a(JZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/location/e/E;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_a

    .line 176
    const/4 v0, 0x0

    .line 187
    :goto_9
    return-object v0

    .line 179
    :cond_a
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->L:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 180
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/location/e/E;->a:J

    add-long/2addr v2, p1

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 182
    const/16 v0, 0x19

    iget-object v2, p0, Lcom/google/android/location/e/E;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 183
    const/4 v0, 0x0

    move v2, v0

    :goto_26
    if-ge v2, v3, :cond_3c

    .line 184
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/android/location/e/E;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/E$a;

    invoke-virtual {v0, p3}, Lcom/google/android/location/e/E$a;->a(Z)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 183
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_26

    :cond_3c
    move-object v0, v1

    .line 187
    goto :goto_9
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 227
    if-ne p0, p1, :cond_5

    .line 234
    :cond_4
    :goto_4
    return v0

    .line 230
    :cond_5
    instance-of v2, p1, Lcom/google/android/location/e/E;

    if-nez v2, :cond_b

    move v0, v1

    .line 231
    goto :goto_4

    .line 233
    :cond_b
    check-cast p1, Lcom/google/android/location/e/E;

    .line 234
    iget-wide v2, p0, Lcom/google/android/location/e/E;->a:J

    iget-wide v4, p1, Lcom/google/android/location/e/E;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1f

    iget-object v2, p0, Lcom/google/android/location/e/E;->b:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/google/android/location/e/E;->b:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_1f
    move v0, v1

    goto :goto_4
.end method

.method public hashCode()I
    .registers 5

    .prologue
    .line 240
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/location/e/E;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/e/E;->b:Ljava/util/ArrayList;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/E;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WifiScan [deliveryTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/e/E;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", devices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/e/E;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
