.class public Lcom/google/android/location/x;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:J

.field private final b:Lcom/google/android/location/os/i;

.field private final c:Lcom/google/android/location/b/f;

.field private final d:Lcom/google/android/location/b/g;

.field private final e:Lcom/google/android/location/g;

.field private final f:Lcom/google/android/location/i/a;

.field private final g:Lcom/google/android/location/e/n;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/b/g;Lcom/google/android/location/g;Lcom/google/android/location/e/n;Lcom/google/android/location/i/a;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/x;->a:J

    .line 47
    iput-object p1, p0, Lcom/google/android/location/x;->b:Lcom/google/android/location/os/i;

    .line 48
    iput-object p2, p0, Lcom/google/android/location/x;->c:Lcom/google/android/location/b/f;

    .line 49
    iput-object p3, p0, Lcom/google/android/location/x;->d:Lcom/google/android/location/b/g;

    .line 50
    iput-object p4, p0, Lcom/google/android/location/x;->e:Lcom/google/android/location/g;

    .line 51
    iput-object p5, p0, Lcom/google/android/location/x;->g:Lcom/google/android/location/e/n;

    .line 52
    iput-object p6, p0, Lcom/google/android/location/x;->f:Lcom/google/android/location/i/a;

    .line 53
    return-void
.end method

.method private a(JJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 11
    .parameter
    .parameter

    .prologue
    const-wide/16 v4, 0x3e8

    .line 92
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->k:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 93
    const/4 v1, 0x1

    div-long v2, p3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 94
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/location/x;->a:J

    sub-long v2, p1, v2

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 95
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/location/x;->c:Lcom/google/android/location/b/f;

    iget-object v2, v2, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    invoke-virtual {v2}, Lcom/google/android/location/b/i;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 97
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/location/x;->c:Lcom/google/android/location/b/f;

    iget-object v2, v2, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v2}, Lcom/google/android/location/b/i;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 99
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/location/x;->g:Lcom/google/android/location/e/n;

    invoke-virtual {v2}, Lcom/google/android/location/e/n;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 101
    sub-long v1, p3, p1

    .line 102
    iget-object v3, p0, Lcom/google/android/location/x;->e:Lcom/google/android/location/g;

    iget-wide v4, p0, Lcom/google/android/location/x;->a:J

    add-long/2addr v1, v4

    invoke-virtual {v3, v1, v2}, Lcom/google/android/location/g;->c(J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 103
    if-eqz v1, :cond_4e

    .line 104
    const/16 v2, 0xc

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 106
    :cond_4e
    iget-object v1, p0, Lcom/google/android/location/x;->f:Lcom/google/android/location/i/a;

    invoke-virtual {v1}, Lcom/google/android/location/i/a;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 107
    if-eqz v1, :cond_5b

    .line 108
    const/16 v2, 0xd

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 110
    :cond_5b
    iget-object v1, p0, Lcom/google/android/location/x;->d:Lcom/google/android/location/b/g;

    if-eqz v1, :cond_6a

    .line 111
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/location/x;->d:Lcom/google/android/location/b/g;

    invoke-virtual {v2}, Lcom/google/android/location/b/g;->f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 114
    :cond_6a
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/location/x;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->y()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 115
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/location/x;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->z()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 116
    return-object v0
.end method


# virtual methods
.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/location/x;->c:Lcom/google/android/location/b/f;

    iget-object v0, v0, Lcom/google/android/location/b/f;->d:Lcom/google/android/location/b/i;

    invoke-virtual {v0}, Lcom/google/android/location/b/i;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 62
    iget-object v0, p0, Lcom/google/android/location/x;->c:Lcom/google/android/location/b/f;

    iget-object v0, v0, Lcom/google/android/location/b/f;->e:Lcom/google/android/location/b/i;

    invoke-virtual {v0}, Lcom/google/android/location/b/i;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 63
    iget-object v0, p0, Lcom/google/android/location/x;->g:Lcom/google/android/location/e/n;

    invoke-virtual {v0}, Lcom/google/android/location/e/n;->b()V

    .line 64
    iput-wide p1, p0, Lcom/google/android/location/x;->a:J

    .line 65
    return-void
.end method

.method public a(Lcom/google/android/location/os/i;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-interface {p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    .line 74
    iget-wide v2, p0, Lcom/google/android/location/x;->a:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_10

    .line 75
    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/x;->a(J)V

    .line 81
    :cond_f
    :goto_f
    return-void

    .line 76
    :cond_10
    iget-wide v2, p0, Lcom/google/android/location/x;->a:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x6ddd00

    cmp-long v2, v2, v4

    if-ltz v2, :cond_f

    .line 77
    const/4 v2, 0x5

    invoke-interface {p1}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v3

    invoke-direct {p0, v0, v1, v3, v4}, Lcom/google/android/location/x;->a(JJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 79
    iput-wide v0, p0, Lcom/google/android/location/x;->a:J

    goto :goto_f
.end method
