.class public Lcom/google/android/location/t;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final l:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final m:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<[",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static o:Lcom/google/android/location/t;


# instance fields
.field a:Z

.field final b:Z

.field c:Z

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Z

.field i:Lcom/google/android/location/e/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field j:Lcom/google/android/location/e/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field k:Z

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/a",
            "<",
            "Lcom/google/android/location/t;",
            ">;>;"
        }
    .end annotation
.end field

.field private final q:Lcom/google/android/location/os/i;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x7

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 53
    new-array v0, v7, [Ljava/lang/Integer;

    const/16 v1, 0x136

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const/16 v1, 0x137

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const/16 v1, 0x138

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const/16 v1, 0x139

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const/16 v2, 0x13a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x13b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x13c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a([Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/t;->l:Lcom/google/common/collect/ImmutableList;

    .line 58
    new-array v0, v6, [[Ljava/lang/Integer;

    new-array v1, v4, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x87f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v1, v0, v5

    new-array v1, v4, [Ljava/lang/Integer;

    const/16 v2, 0x900

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x1dff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v1, v0, v3

    new-array v1, v4, [Ljava/lang/Integer;

    const/16 v2, 0x543f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x547e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a([Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/t;->m:Lcom/google/common/collect/ImmutableList;

    .line 62
    const/16 v0, 0x50

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x1b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/16 v1, 0x8

    const/16 v2, 0x21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x2b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x2f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0x3d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x41

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const/16 v2, 0x43

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0x45

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x47

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x6f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x83

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x88

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0xd3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const/16 v2, 0x101

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const/16 v2, 0x103

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const/16 v2, 0x105

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const/16 v2, 0x107

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const/16 v2, 0x137

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const/16 v2, 0x140

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const/16 v2, 0x142

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const/16 v2, 0x144

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const/16 v2, 0x146

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const/16 v2, 0x148

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const/16 v2, 0x14a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const/16 v2, 0x183

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const/16 v2, 0x185

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const/16 v2, 0x187

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const/16 v2, 0x189

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const/16 v2, 0x18b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const/16 v2, 0x240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const/16 v2, 0x242

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const/16 v2, 0x246

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const/16 v2, 0x2bc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const/16 v2, 0x2bf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const/16 v2, 0x2e3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const/16 v2, 0x2e5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const/16 v2, 0x457

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const/16 v2, 0x458

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const/16 v2, 0x459

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const/16 v2, 0x4c2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const/16 v2, 0x4c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const/16 v2, 0x4c6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const/16 v2, 0x502

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const/16 v2, 0x504

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const/16 v2, 0x521

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const/16 v2, 0x523

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const/16 v2, 0x531

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const/16 v2, 0x581

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const/16 v2, 0x583

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const/16 v2, 0x5a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const/16 v2, 0x5f1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const/16 v2, 0x5f5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const/16 v2, 0x602

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const/16 v2, 0x621

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const/16 v2, 0x62d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const/16 v2, 0x662

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const/16 v2, 0x682

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const/16 v2, 0x684

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const/16 v2, 0x6a4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const/16 v2, 0x76d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const/16 v2, 0x1004

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const/16 v2, 0x1005

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const/16 v2, 0x100e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const/16 v2, 0x1018

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const/16 v2, 0x1022

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const/16 v2, 0x102c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const/16 v2, 0x1036

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const/16 v2, 0x1111

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const/16 v2, 0x1112

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const/16 v2, 0x1113

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const/16 v2, 0x1664

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const/16 v2, 0x1666

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const/16 v2, 0x168c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->copyOf([Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/t;->n:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/location/os/i;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x63

    const/4 v2, 0x0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/t;->p:Ljava/util/List;

    .line 85
    iput-boolean v2, p0, Lcom/google/android/location/t;->c:Z

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/t;->d:Z

    .line 89
    iput-boolean v2, p0, Lcom/google/android/location/t;->e:Z

    .line 91
    iput-boolean v2, p0, Lcom/google/android/location/t;->f:Z

    .line 93
    iput-boolean v2, p0, Lcom/google/android/location/t;->g:Z

    .line 97
    iput-boolean v2, p0, Lcom/google/android/location/t;->h:Z

    .line 99
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/t;->i:Lcom/google/android/location/e/u;

    .line 102
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/t;->j:Lcom/google/android/location/e/u;

    .line 105
    iput-boolean v2, p0, Lcom/google/android/location/t;->k:Z

    .line 109
    iput-object p1, p0, Lcom/google/android/location/t;->q:Lcom/google/android/location/os/i;

    .line 110
    invoke-interface {p1}, Lcom/google/android/location/os/i;->u()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/t;->a:Z

    .line 111
    iput-boolean p2, p0, Lcom/google/android/location/t;->b:Z

    .line 112
    invoke-direct {p0}, Lcom/google/android/location/t;->d()V

    .line 114
    return-void
.end method

.method public static declared-synchronized a()Lcom/google/android/location/t;
    .registers 3

    .prologue
    .line 139
    const-class v1, Lcom/google/android/location/t;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/location/t;->o:Lcom/google/android/location/t;

    const-string v2, "Class not initialized, please call init()."

    invoke-static {v0, v2}, Lcom/google/common/base/J;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/google/android/location/t;->o:Lcom/google/android/location/t;
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_e

    monitor-exit v1

    return-object v0

    .line 139
    :catchall_e
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Lcom/google/android/location/os/i;)Lcom/google/android/location/t;
    .registers 4
    .parameter

    .prologue
    .line 121
    const-class v1, Lcom/google/android/location/t;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/location/t;->o:Lcom/google/android/location/t;

    if-nez v0, :cond_f

    .line 122
    new-instance v0, Lcom/google/android/location/t;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v2}, Lcom/google/android/location/t;-><init>(Lcom/google/android/location/os/i;Z)V

    sput-object v0, Lcom/google/android/location/t;->o:Lcom/google/android/location/t;

    .line 124
    :cond_f
    sget-object v0, Lcom/google/android/location/t;->o:Lcom/google/android/location/t;
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_13

    monitor-exit v1

    return-object v0

    .line 121
    :catchall_13
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(I)Ljava/lang/Boolean;
    .registers 6
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 414
    if-gez p0, :cond_6

    .line 415
    const/4 v0, 0x0

    .line 425
    :goto_5
    return-object v0

    .line 417
    :cond_6
    sget-object v0, Lcom/google/android/location/t;->m:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    .line 420
    aget-object v2, v0, v3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gt v2, p0, :cond_c

    aget-object v0, v0, v4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt p0, v0, :cond_c

    sget-object v0, Lcom/google/android/location/t;->n:Ljava/util/Set;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 422
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_5

    .line 425
    :cond_39
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_5
.end method

.method private a(II)Ljava/lang/Boolean;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 389
    packed-switch p1, :pswitch_data_22

    .line 401
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 404
    :goto_8
    return-object v0

    .line 393
    :pswitch_9
    if-gez p2, :cond_d

    const/4 v0, 0x0

    goto :goto_8

    :cond_d
    sget-object v0, Lcom/google/android/location/t;->l:Lcom/google/common/collect/ImmutableList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_8

    .line 396
    :pswitch_1c
    invoke-static {p2}, Lcom/google/android/location/t;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_8

    .line 389
    nop

    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_9
        :pswitch_1c
        :pswitch_9
    .end packed-switch
.end method

.method private a(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 358
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-ne p1, v0, :cond_9

    .line 359
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    .line 361
    :cond_9
    return-object p2
.end method

.method private b(Lcom/google/android/location/e/e;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 330
    if-nez p1, :cond_4

    .line 354
    :cond_3
    :goto_3
    return v0

    .line 336
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/location/e/e;->g()I

    move-result v1

    .line 340
    const/4 v2, 0x4

    if-eq v1, v2, :cond_3

    .line 344
    invoke-virtual {p1}, Lcom/google/android/location/e/e;->d()I

    move-result v2

    .line 345
    invoke-virtual {p1}, Lcom/google/android/location/e/e;->e()I

    move-result v3

    .line 346
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/google/android/location/t;->f(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/t;->a(II)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {p0, v4, v2}, Lcom/google/android/location/t;->a(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v2

    .line 348
    invoke-direct {p0, v0}, Lcom/google/android/location/t;->f(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {p0, v1, v3}, Lcom/google/android/location/t;->a(II)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v4, v1}, Lcom/google/android/location/t;->a(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    .line 350
    if-nez v2, :cond_2e

    .line 353
    :cond_2e
    invoke-direct {p0, v2, v1}, Lcom/google/android/location/t;->a(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v1

    .line 354
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_3
.end method

.method private d(Z)Lcom/google/android/location/e/u;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v4, 0x63

    .line 231
    invoke-virtual {p0}, Lcom/google/android/location/t;->c()Z

    move-result v2

    .line 232
    invoke-direct {p0, p1}, Lcom/google/android/location/t;->e(Z)Z

    move-result v3

    .line 233
    if-eqz v2, :cond_26

    if-nez v3, :cond_26

    iget-boolean v0, p0, Lcom/google/android/location/t;->c:Z

    if-eqz v0, :cond_26

    const/4 v0, 0x1

    move v1, v0

    .line 234
    :goto_14
    const/4 v0, 0x0

    .line 235
    if-nez v1, :cond_1d

    .line 236
    if-nez v2, :cond_29

    .line 237
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 246
    :cond_1d
    :goto_1d
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    return-object v0

    .line 233
    :cond_26
    const/4 v0, 0x0

    move v1, v0

    goto :goto_14

    .line 238
    :cond_29
    if-eqz v3, :cond_32

    .line 239
    const/16 v0, 0x16

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1d

    .line 240
    :cond_32
    iget-boolean v0, p0, Lcom/google/android/location/t;->c:Z

    if-nez v0, :cond_3d

    .line 241
    const/16 v0, 0x17

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1d

    .line 243
    :cond_3d
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1d
.end method

.method private d()V
    .registers 3

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/google/android/location/t;->e()Z

    move-result v0

    .line 266
    invoke-direct {p0}, Lcom/google/android/location/t;->f()V

    .line 267
    if-eqz v0, :cond_1f

    .line 268
    iget-object v0, p0, Lcom/google/android/location/t;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/a;

    .line 269
    invoke-interface {v0, p0}, Lcom/google/android/location/k/a;->a(Ljava/lang/Object;)V

    goto :goto_f

    .line 273
    :cond_1f
    return-void
.end method

.method private e()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 279
    invoke-virtual {p0}, Lcom/google/android/location/t;->c()Z

    move-result v2

    iget-boolean v3, p0, Lcom/google/android/location/t;->h:Z

    if-ne v2, v3, :cond_22

    invoke-direct {p0, v0}, Lcom/google/android/location/t;->d(Z)Lcom/google/android/location/e/u;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/location/t;->i:Lcom/google/android/location/e/u;

    iget-object v3, v3, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    if-ne v2, v3, :cond_22

    invoke-direct {p0, v1}, Lcom/google/android/location/t;->d(Z)Lcom/google/android/location/e/u;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/location/t;->j:Lcom/google/android/location/e/u;

    iget-object v3, v3, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    if-eq v2, v3, :cond_23

    :cond_22
    move v0, v1

    :cond_23
    return v0
.end method

.method private e(Z)Z
    .registers 3
    .parameter

    .prologue
    .line 250
    if-eqz p1, :cond_e

    .line 251
    iget-boolean v0, p0, Lcom/google/android/location/t;->d:Z

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/location/t;->k:Z

    if-nez v0, :cond_c

    const/4 v0, 0x1

    .line 253
    :goto_b
    return v0

    .line 251
    :cond_c
    const/4 v0, 0x0

    goto :goto_b

    .line 253
    :cond_e
    iget-boolean v0, p0, Lcom/google/android/location/t;->d:Z

    goto :goto_b
.end method

.method private f(Z)Ljava/lang/Boolean;
    .registers 4
    .parameter

    .prologue
    .line 371
    if-eqz p1, :cond_14

    iget-object v0, p0, Lcom/google/android/location/t;->q:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->t()Ljava/lang/String;

    move-result-object v0

    .line 372
    :goto_8
    if-eqz v0, :cond_12

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 373
    :cond_12
    const/4 v0, 0x0

    .line 375
    :goto_13
    return-object v0

    .line 371
    :cond_14
    iget-object v0, p0, Lcom/google/android/location/t;->q:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->s()Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    .line 375
    :cond_1b
    const-string v1, "us"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_13
.end method

.method private f()V
    .registers 2

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/google/android/location/t;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/t;->h:Z

    .line 458
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/t;->d(Z)Lcom/google/android/location/e/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/t;->i:Lcom/google/android/location/e/u;

    .line 459
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/t;->d(Z)Lcom/google/android/location/e/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/t;->j:Lcom/google/android/location/e/u;

    .line 460
    return-void
.end method


# virtual methods
.method public a(Z)Lcom/google/android/location/e/u;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    if-eqz p1, :cond_5

    iget-object v0, p0, Lcom/google/android/location/t;->j:Lcom/google/android/location/e/u;

    :goto_4
    return-object v0

    :cond_5
    iget-object v0, p0, Lcom/google/android/location/t;->i:Lcom/google/android/location/e/u;

    goto :goto_4
.end method

.method public declared-synchronized a(IIZ)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 309
    monitor-enter p0

    :try_start_1
    invoke-static {p1, p2}, Lcom/google/android/location/k/c;->a(II)F

    move-result v1

    if-eqz p3, :cond_16

    const v0, 0x3e4ccccd

    :goto_a
    cmpl-float v0, v1, v0

    if-ltz v0, :cond_1a

    const/4 v0, 0x1

    :goto_f
    iput-boolean v0, p0, Lcom/google/android/location/t;->c:Z

    .line 311
    invoke-direct {p0}, Lcom/google/android/location/t;->d()V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_1c

    .line 312
    monitor-exit p0

    return-void

    .line 309
    :cond_16
    const v0, 0x3f19999a

    goto :goto_a

    :cond_1a
    const/4 v0, 0x0

    goto :goto_f

    :catchall_1c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/location/e/e;)V
    .registers 3
    .parameter

    .prologue
    .line 321
    monitor-enter p0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/location/t;->b(Lcom/google/android/location/e/e;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/t;->e:Z

    .line 322
    invoke-direct {p0}, Lcom/google/android/location/t;->d()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 323
    monitor-exit p0

    return-void

    .line 321
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/location/k/a;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/k/a",
            "<",
            "Lcom/google/android/location/t;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    if-nez p1, :cond_3

    .line 155
    :goto_2
    return-void

    .line 154
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/t;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .registers 3
    .parameter

    .prologue
    .line 429
    invoke-virtual {p1}, Lcom/google/android/location/os/h;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/t;->f:Z

    .line 430
    invoke-direct {p0}, Lcom/google/android/location/t;->d()V

    .line 431
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 439
    if-nez p2, :cond_3

    .line 450
    :cond_2
    :goto_2
    return-void

    .line 444
    :cond_3
    sget-object v0, Lcom/google/android/location/os/i;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 448
    iput-boolean p1, p0, Lcom/google/android/location/t;->k:Z

    .line 449
    invoke-direct {p0}, Lcom/google/android/location/t;->d()V

    goto :goto_2
.end method

.method public declared-synchronized b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 301
    monitor-enter p0

    :try_start_1
    iput-boolean p1, p0, Lcom/google/android/location/t;->d:Z

    .line 302
    invoke-direct {p0}, Lcom/google/android/location/t;->d()V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 303
    monitor-exit p0

    return-void

    .line 301
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/google/android/location/t;->h:Z

    return v0
.end method

.method public c(Z)V
    .registers 2
    .parameter

    .prologue
    .line 434
    iput-boolean p1, p0, Lcom/google/android/location/t;->g:Z

    .line 435
    invoke-direct {p0}, Lcom/google/android/location/t;->d()V

    .line 436
    return-void
.end method

.method c()Z
    .registers 2

    .prologue
    .line 187
    iget-boolean v0, p0, Lcom/google/android/location/t;->a:Z

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/location/t;->b:Z

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/location/t;->f:Z

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/location/t;->e:Z

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/location/t;->g:Z

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 286
    const-string v0, "[canEnableForScheduler: %s, canEnableForCollector: %s, canEnableForSensorCollector: %s, enabledInClient: %s, enabledInServer: %s, hasRequiredSensors: %s, screenOn: %s, isBatteryHealthy: %s, isInUsAccordingToNetwork: %s, nlpEnabled: %s, gmmInForeground: %s]"

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/location/t;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/t;->i:Lcom/google/android/location/e/u;

    iget-object v3, v3, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/location/t;->j:Lcom/google/android/location/e/u;

    iget-object v3, v3, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/android/location/t;->b:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/google/android/location/t;->f:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/android/location/t;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/android/location/t;->d:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/android/location/t;->c:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/google/android/location/t;->e:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-boolean v3, p0, Lcom/google/android/location/t;->g:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xa

    iget-boolean v3, p0, Lcom/google/android/location/t;->k:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
