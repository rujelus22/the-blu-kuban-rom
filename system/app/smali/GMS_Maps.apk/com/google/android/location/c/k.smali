.class Lcom/google/android/location/c/k;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/location/k/a/c;

.field private final b:Lcom/google/android/location/c/o;

.field private final c:Landroid/os/Handler;

.field private final d:I

.field private e:Z

.field private final f:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/o;Landroid/os/Handler;ILcom/google/android/location/k/a/c;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c/k;->e:Z

    .line 40
    new-instance v0, Lcom/google/android/location/c/k$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/k$1;-><init>(Lcom/google/android/location/c/k;)V

    iput-object v0, p0, Lcom/google/android/location/c/k;->f:Ljava/lang/Runnable;

    .line 53
    iput-object p1, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    .line 54
    iput-object p2, p0, Lcom/google/android/location/c/k;->c:Landroid/os/Handler;

    .line 55
    iput p3, p0, Lcom/google/android/location/c/k;->d:I

    .line 56
    invoke-static {p4}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/k;->a:Lcom/google/android/location/k/a/c;

    .line 57
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/k;)Z
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/location/c/k;->e:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/location/c/k;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/location/c/k;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/location/c/k;)Lcom/google/android/location/c/o;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/c/k;)I
    .registers 2
    .parameter

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/location/c/k;->d:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/location/c/k;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/location/c/k;->c:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method a()V
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/location/c/k;->f:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/location/c/k;->post(Ljava/lang/Runnable;)Z

    .line 76
    return-void
.end method

.method a(FFFF)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/c/o;->a(FFFF)V

    .line 141
    return-void
.end method

.method a(IFFFIJJ)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 112
    const/4 v0, 0x3

    if-ne p1, v0, :cond_10

    .line 113
    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-wide v5, p6

    move-wide/from16 v7, p8

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/location/c/o;->a(FFFIJJ)V

    .line 127
    :cond_f
    :goto_f
    return-void

    .line 115
    :cond_10
    const/4 v0, 0x1

    if-ne p1, v0, :cond_20

    .line 116
    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-wide v5, p6

    move-wide/from16 v7, p8

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/location/c/o;->b(FFFIJJ)V

    goto :goto_f

    .line 118
    :cond_20
    const/4 v0, 0x2

    if-ne p1, v0, :cond_30

    .line 119
    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-wide v5, p6

    move-wide/from16 v7, p8

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/location/c/o;->c(FFFIJJ)V

    goto :goto_f

    .line 121
    :cond_30
    const/4 v0, 0x4

    if-ne p1, v0, :cond_40

    .line 122
    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-wide v5, p6

    move-wide/from16 v7, p8

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/location/c/o;->d(FFFIJJ)V

    goto :goto_f

    .line 124
    :cond_40
    const/4 v0, 0x6

    if-ne p1, v0, :cond_f

    .line 125
    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    move v1, p2

    move v2, p5

    move-wide v3, p6

    move-wide/from16 v5, p8

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/c/o;->a(FIJJ)V

    goto :goto_f
.end method

.method a(ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Landroid/telephony/CellLocation;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/NeighboringCellInfo;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-wide v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/c/o;->a(ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V

    .line 106
    return-void
.end method

.method public a(Landroid/location/GpsStatus;J)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 135
    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/c/o;->a(Landroid/location/GpsStatus;J)V

    .line 137
    return-void
.end method

.method a(Landroid/location/Location;J)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 130
    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    iget-object v0, p0, Lcom/google/android/location/c/k;->b:Lcom/google/android/location/c/o;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/c/o;->a(Landroid/location/Location;J)Z

    .line 132
    return-void
.end method

.method a(Lcom/google/android/location/c/E;)V
    .registers 3
    .parameter

    .prologue
    .line 60
    new-instance v0, Lcom/google/android/location/c/k$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/location/c/k$2;-><init>(Lcom/google/android/location/c/k;Lcom/google/android/location/c/E;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/c/k;->post(Ljava/lang/Runnable;)Z

    .line 68
    return-void
.end method

.method a(Ljava/util/List;J)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 86
    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    new-instance v0, Lcom/google/android/location/c/k$3;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/location/c/k$3;-><init>(Lcom/google/android/location/c/k;Ljava/util/List;J)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/c/k;->post(Ljava/lang/Runnable;)Z

    .line 93
    return-void
.end method
