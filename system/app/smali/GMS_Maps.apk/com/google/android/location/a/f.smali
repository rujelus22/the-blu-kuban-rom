.class public Lcom/google/android/location/a/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x7

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput v1, p0, Lcom/google/android/location/a/f;->a:I

    .line 30
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/location/a/f;->b:I

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    return-void
.end method

.method private a(Lcom/google/android/location/e/c;Lcom/google/android/location/e/c;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 87
    iget-object v0, p1, Lcom/google/android/location/e/c;->c:Lcom/google/android/location/e/w;

    iget-object v1, p2, Lcom/google/android/location/e/c;->c:Lcom/google/android/location/e/w;

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v0

    .line 88
    iget-object v1, p1, Lcom/google/android/location/e/c;->c:Lcom/google/android/location/e/w;

    iget v1, v1, Lcom/google/android/location/e/w;->c:I

    div-int/lit16 v1, v1, 0x3e8

    const/16 v2, 0xc8

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 89
    if-gt v0, v1, :cond_18

    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method private a(Lcom/google/android/location/e/c;Ljava/util/List;)Z
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/c;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/c;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 81
    iget-wide v1, p1, Lcom/google/android/location/e/c;->e:J

    .line 82
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/c;

    iget-wide v3, v0, Lcom/google/android/location/e/c;->e:J

    .line 83
    sub-long v0, v1, v3

    const-wide/32 v2, 0xd6d8

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1b

    const/4 v0, 0x1

    :goto_1a
    return v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method private b(Lcom/google/android/location/e/c;)V
    .registers 4
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/f;->a(Lcom/google/android/location/e/c;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 72
    :cond_10
    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x7

    if-le v0, v1, :cond_24

    .line 74
    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 77
    :cond_24
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/location/e/c;)Lcom/google/android/location/a/n$b;
    .registers 7
    .parameter

    .prologue
    const-wide/high16 v3, 0x3fe0

    .line 37
    if-eqz p1, :cond_8

    iget-object v0, p1, Lcom/google/android/location/e/c;->c:Lcom/google/android/location/e/w;

    if-nez v0, :cond_b

    .line 38
    :cond_8
    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    .line 58
    :goto_a
    return-object v0

    .line 39
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_1a

    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/location/a/f;->b(Lcom/google/android/location/e/c;)V

    .line 41
    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    goto :goto_a

    .line 43
    :cond_1a
    const/4 v1, 0x0

    .line 44
    iget-object v0, p0, Lcom/google/android/location/a/f;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_21
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_49

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/c;

    .line 45
    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/f;->a(Lcom/google/android/location/e/c;Lcom/google/android/location/e/c;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 46
    const/4 v0, 0x1

    .line 50
    :goto_34
    invoke-direct {p0, p1}, Lcom/google/android/location/a/f;->b(Lcom/google/android/location/e/c;)V

    .line 51
    if-eqz v0, :cond_41

    .line 52
    new-instance v0, Lcom/google/android/location/a/n$b;

    sget-object v1, Lcom/google/android/location/e/B;->b:Lcom/google/android/location/e/B;

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/location/a/n$b;-><init>(Lcom/google/android/location/e/B;D)V

    goto :goto_a

    .line 54
    :cond_41
    new-instance v0, Lcom/google/android/location/a/n$b;

    sget-object v1, Lcom/google/android/location/e/B;->a:Lcom/google/android/location/e/B;

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/location/a/n$b;-><init>(Lcom/google/android/location/e/B;D)V

    goto :goto_a

    :cond_49
    move v0, v1

    goto :goto_34
.end method
