.class public Lcom/google/android/location/c/F;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/location/c/F;

.field public static final b:Lcom/google/android/location/c/F;

.field public static final c:Lcom/google/android/location/c/F;

.field public static final d:Lcom/google/android/location/c/F;

.field public static final e:Lcom/google/android/location/c/F;

.field public static final f:Lcom/google/android/location/c/F;

.field public static final g:Lcom/google/android/location/c/F;

.field public static final h:Lcom/google/android/location/c/F;

.field public static final i:Lcom/google/android/location/c/F;

.field public static final j:Lcom/google/android/location/c/F;

.field public static final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final m:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 19
    new-instance v0, Lcom/google/android/location/c/F;

    invoke-direct {v0, v3}, Lcom/google/android/location/c/F;-><init>(I)V

    sput-object v0, Lcom/google/android/location/c/F;->a:Lcom/google/android/location/c/F;

    .line 20
    new-instance v0, Lcom/google/android/location/c/F;

    invoke-direct {v0, v4}, Lcom/google/android/location/c/F;-><init>(I)V

    sput-object v0, Lcom/google/android/location/c/F;->b:Lcom/google/android/location/c/F;

    .line 24
    new-instance v0, Lcom/google/android/location/c/F;

    invoke-direct {v0, v5}, Lcom/google/android/location/c/F;-><init>(I)V

    sput-object v0, Lcom/google/android/location/c/F;->c:Lcom/google/android/location/c/F;

    .line 25
    new-instance v0, Lcom/google/android/location/c/F;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/location/c/F;-><init>(I)V

    sput-object v0, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    .line 26
    new-instance v0, Lcom/google/android/location/c/F;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lcom/google/android/location/c/F;-><init>(I)V

    sput-object v0, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    .line 27
    new-instance v0, Lcom/google/android/location/c/F;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lcom/google/android/location/c/F;-><init>(I)V

    sput-object v0, Lcom/google/android/location/c/F;->f:Lcom/google/android/location/c/F;

    .line 29
    new-instance v0, Lcom/google/android/location/c/F;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lcom/google/android/location/c/F;-><init>(I)V

    sput-object v0, Lcom/google/android/location/c/F;->g:Lcom/google/android/location/c/F;

    .line 30
    new-instance v0, Lcom/google/android/location/c/F;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Lcom/google/android/location/c/F;-><init>(I)V

    sput-object v0, Lcom/google/android/location/c/F;->h:Lcom/google/android/location/c/F;

    .line 32
    new-instance v0, Lcom/google/android/location/c/F;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Lcom/google/android/location/c/F;-><init>(I)V

    sput-object v0, Lcom/google/android/location/c/F;->i:Lcom/google/android/location/c/F;

    .line 34
    new-instance v0, Lcom/google/android/location/c/F;

    const/high16 v1, -0x8000

    invoke-direct {v0, v1}, Lcom/google/android/location/c/F;-><init>(I)V

    sput-object v0, Lcom/google/android/location/c/F;->j:Lcom/google/android/location/c/F;

    .line 40
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/location/c/F;

    sget-object v1, Lcom/google/android/location/c/F;->a:Lcom/google/android/location/c/F;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/location/c/F;->b:Lcom/google/android/location/c/F;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/c/F;->c:Lcom/google/android/location/c/F;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    aput-object v1, v0, v5

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/location/c/F;->f:Lcom/google/android/location/c/F;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/location/c/F;->g:Lcom/google/android/location/c/F;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/location/c/F;->h:Lcom/google/android/location/c/F;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/location/c/F;->i:Lcom/google/android/location/c/F;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/location/c/F;->a([Lcom/google/android/location/c/F;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/c/F;->k:Ljava/util/Set;

    .line 42
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/location/c/F;

    sget-object v1, Lcom/google/android/location/c/F;->c:Lcom/google/android/location/c/F;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/location/c/F;->f:Lcom/google/android/location/c/F;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/location/c/F;->i:Lcom/google/android/location/c/F;

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/google/android/location/c/F;->a([Lcom/google/android/location/c/F;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/c/F;->l:Ljava/util/Set;

    .line 44
    return-void
.end method

.method private constructor <init>(I)V
    .registers 2
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput p1, p0, Lcom/google/android/location/c/F;->m:I

    .line 49
    return-void
.end method

.method static a(Ljava/util/Set;)I
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 73
    const/4 v0, 0x0

    .line 74
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/c/F;

    .line 75
    iget v0, v0, Lcom/google/android/location/c/F;->m:I

    or-int/2addr v0, v1

    move v1, v0

    goto :goto_6

    .line 77
    :cond_17
    return v1
.end method

.method static a(I)Ljava/util/Set;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 85
    sget-object v0, Lcom/google/android/location/c/F;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b
    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/c/F;

    .line 86
    iget v3, v0, Lcom/google/android/location/c/F;->m:I

    and-int/2addr v3, p0

    if-eqz v3, :cond_b

    .line 87
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 90
    :cond_20
    return-object v1
.end method

.method public static varargs a([Lcom/google/android/location/c/F;)Ljava/util/Set;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/location/c/F;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    new-instance v1, Ljava/util/HashSet;

    array-length v0, p0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 56
    array-length v2, p0

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_12

    aget-object v3, p0, v0

    .line 57
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 59
    :cond_12
    return-object v1
.end method

.method public static b(I)Lcom/google/android/location/c/F;
    .registers 8
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 99
    .line 100
    const/4 v1, 0x0

    .line 101
    sget-object v0, Lcom/google/android/location/c/F;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v3

    :goto_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/c/F;

    .line 102
    iget v5, v0, Lcom/google/android/location/c/F;->m:I

    and-int/2addr v5, p0

    if-eqz v5, :cond_28

    .line 104
    add-int/lit8 v1, v1, 0x1

    move v6, v1

    move-object v1, v0

    move v0, v6

    :goto_1f
    move-object v2, v1

    move v1, v0

    goto :goto_9

    .line 107
    :cond_22
    const/4 v0, 0x1

    if-ne v1, v0, :cond_26

    :goto_25
    return-object v2

    :cond_26
    move-object v2, v3

    goto :goto_25

    :cond_28
    move v0, v1

    move-object v1, v2

    goto :goto_1f
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/location/c/F;->m:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 112
    iget v0, p0, Lcom/google/android/location/c/F;->m:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
