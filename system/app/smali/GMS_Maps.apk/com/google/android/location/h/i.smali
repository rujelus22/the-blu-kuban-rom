.class public Lcom/google/android/location/h/i;
.super Lcom/google/android/location/h/b;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/b;-><init>(Ljava/lang/String;Z)V

    .line 18
    return-void
.end method

.method private j()V
    .registers 3

    .prologue
    .line 69
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 71
    monitor-enter v1

    .line 72
    :try_start_6
    invoke-virtual {p0}, Lcom/google/android/location/h/i;->g()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 73
    new-instance v0, Lcom/google/android/location/h/i$1;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/h/i$1;-><init>(Lcom/google/android/location/h/i;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/h/i;->addObserver(Lcom/google/googlenav/common/util/Observer;)V

    .line 81
    invoke-virtual {p0}, Lcom/google/android/location/h/i;->submitRequest()V

    .line 84
    :cond_17
    :goto_17
    invoke-virtual {p0}, Lcom/google/android/location/h/i;->h()Z

    move-result v0

    if-nez v0, :cond_29

    invoke-virtual {p0}, Lcom/google/android/location/h/i;->i()Z
    :try_end_20
    .catchall {:try_start_6 .. :try_end_20} :catchall_2b

    move-result v0

    if-nez v0, :cond_29

    .line 86
    :try_start_23
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_2b
    .catch Ljava/lang/InterruptedException; {:try_start_23 .. :try_end_26} :catch_27

    goto :goto_17

    .line 87
    :catch_27
    move-exception v0

    goto :goto_17

    .line 91
    :cond_29
    :try_start_29
    monitor-exit v1

    .line 92
    return-void

    .line 91
    :catchall_2b
    move-exception v0

    monitor-exit v1
    :try_end_2d
    .catchall {:try_start_29 .. :try_end_2d} :catchall_2b

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    .line 29
    invoke-super {p0, p1}, Lcom/google/android/location/h/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/io/DataInputStream;
    .registers 2

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    .line 65
    invoke-super {p0}, Lcom/google/android/location/h/b;->b()Ljava/io/DataInputStream;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    .line 53
    invoke-super {p0}, Lcom/google/android/location/h/b;->c()I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    .line 23
    invoke-super {p0}, Lcom/google/android/location/h/b;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()J
    .registers 3

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    .line 47
    invoke-super {p0}, Lcom/google/android/location/h/b;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public getHeaderField(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    .line 35
    invoke-super {p0, p1}, Lcom/google/android/location/h/b;->getHeaderField(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderFieldKey(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    .line 41
    invoke-super {p0, p1}, Lcom/google/android/location/h/b;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponseMessage()Ljava/lang/String;
    .registers 2

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/location/h/i;->j()V

    .line 59
    invoke-super {p0}, Lcom/google/android/location/h/b;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
