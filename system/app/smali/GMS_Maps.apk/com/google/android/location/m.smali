.class public Lcom/google/android/location/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/base/K;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/K",
        "<",
        "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/j;

.field private final b:Lcom/google/android/location/os/i;

.field private final c:Lcom/google/android/location/g;


# direct methods
.method public constructor <init>(Lcom/google/android/location/g;Lcom/google/android/location/os/i;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Lcom/google/android/location/j;

    invoke-direct {v0}, Lcom/google/android/location/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/m;->a:Lcom/google/android/location/j;

    .line 72
    iput-object p2, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    .line 73
    iput-object p1, p0, Lcom/google/android/location/m;->c:Lcom/google/android/location/g;

    .line 74
    return-void
.end method

.method private a(Ljava/util/TimeZone;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 219
    invoke-virtual {p1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    .line 220
    if-nez v3, :cond_b

    .line 221
    const/4 v0, -0x1

    .line 245
    :goto_a
    return v0

    .line 224
    :cond_b
    invoke-virtual {p2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_3f

    move v1, v0

    .line 225
    :goto_12
    invoke-virtual {p2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-ge v0, v2, :cond_40

    .line 226
    invoke-virtual {p2, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 227
    invoke-virtual {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_3c

    invoke-virtual {v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_3c

    .line 228
    invoke-virtual {v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    .line 229
    invoke-virtual {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 230
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_38

    move v0, v2

    .line 231
    goto :goto_a

    .line 233
    :cond_38
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 225
    :cond_3c
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    :cond_3f
    move v1, v0

    .line 239
    :cond_40
    add-int/lit8 v0, v1, 0x1

    .line 240
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->E:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 241
    invoke-virtual {v1, v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 242
    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 243
    invoke-virtual {p2, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_a
.end method

.method private a(JILcom/google/android/location/e/u;Ljava/lang/Boolean;Ljava/lang/Integer;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Double;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 188
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->O:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 189
    invoke-virtual {v1, v7, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 190
    const/4 v0, 0x2

    invoke-virtual {v1, v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 193
    iget-object v0, p4, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_25

    .line 194
    const/4 v2, 0x3

    iget-object v0, p4, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 196
    :cond_25
    const/4 v2, 0x4

    iget-object v0, p4, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const-wide/high16 v5, 0x4059

    mul-double/2addr v3, v5

    double-to-int v0, v3

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 199
    if-eqz p5, :cond_41

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 200
    const/4 v0, 0x5

    invoke-virtual {v1, v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 202
    :cond_41
    if-eqz p6, :cond_4b

    .line 203
    const/4 v0, 0x6

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 205
    :cond_4b
    const/4 v0, 0x7

    invoke-virtual {v1, v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 206
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/location/m;Ljava/util/Calendar;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/m;->b(Ljava/util/Calendar;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private a(Ljava/util/Calendar;Lcom/google/android/location/e/u;Ljava/lang/Boolean;Ljava/lang/Integer;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Double;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 119
    new-instance v0, Lcom/google/android/location/os/j;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->k()Ljavax/crypto/SecretKey;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->l()[B

    move-result-object v4

    sget-object v5, Lcom/google/android/location/j/a;->y:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iget-object v6, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    invoke-interface {v6}, Lcom/google/android/location/os/i;->w()Ljava/io/File;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    move-object v7, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/os/j;-><init>(ILjavax/crypto/SecretKey;I[BLcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/File;Lcom/google/common/base/K;Lcom/google/android/location/os/f;)V

    .line 129
    :try_start_1e
    invoke-virtual {v0}, Lcom/google/android/location/os/j;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_21} :catch_2d

    move-result-object v1

    .line 135
    :goto_22
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/location/m;->a(Ljava/util/TimeZone;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v7

    .line 136
    if-gez v7, :cond_36

    .line 155
    :goto_2c
    return-void

    .line 131
    :catch_2d
    move-exception v1

    .line 133
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->y:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    goto :goto_22

    .line 140
    :cond_36
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    move-object v4, p0

    move-object v8, p2

    move-object v9, p3

    move-object v10, p4

    invoke-direct/range {v4 .. v10}, Lcom/google/android/location/m;->a(JILcom/google/android/location/e/u;Ljava/lang/Boolean;Ljava/lang/Integer;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 145
    if-eqz v2, :cond_4a

    .line 146
    invoke-direct {p0, v1}, Lcom/google/android/location/m;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 147
    invoke-virtual {v1, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 150
    :cond_4a
    :try_start_4a
    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_4d
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4d} :catch_51

    .line 154
    :goto_4d
    invoke-direct {p0, v1}, Lcom/google/android/location/m;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_2c

    .line 151
    :catch_51
    move-exception v0

    goto :goto_4d
.end method

.method private b(Ljava/util/Calendar;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x9

    const/4 v2, 0x4

    .line 94
    if-eqz p2, :cond_b

    invoke-virtual {p2, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-nez v0, :cond_c

    .line 114
    :cond_b
    :goto_b
    return-void

    .line 98
    :cond_c
    const/4 v0, 0x0

    invoke-virtual {p2, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 99
    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 103
    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 104
    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/util/List;

    move-result-object v1

    .line 106
    invoke-static {v0, v2}, Lcom/google/android/location/k/c;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/util/List;

    move-result-object v2

    .line 108
    iget-object v3, p0, Lcom/google/android/location/m;->a:Lcom/google/android/location/j;

    invoke-virtual {v3, v1}, Lcom/google/android/location/j;->a(Ljava/util/List;)Lcom/google/android/location/e/u;

    move-result-object v1

    .line 109
    invoke-static {v2}, Lcom/google/android/location/f;->b(Ljava/util/List;)Ljava/lang/Boolean;

    move-result-object v2

    .line 110
    invoke-virtual {p0, v0}, Lcom/google/android/location/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Integer;

    move-result-object v0

    .line 113
    invoke-direct {p0, p1, v1, v2, v0}, Lcom/google/android/location/m;->a(Ljava/util/Calendar;Lcom/google/android/location/e/u;Ljava/lang/Boolean;Ljava/lang/Integer;)V

    goto :goto_b
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter

    .prologue
    .line 163
    new-instance v1, Lcom/google/android/location/n;

    sget-object v0, Lcom/google/android/location/v;->i:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-direct {v1, v0}, Lcom/google/android/location/n;-><init>(Lcom/google/android/location/k/b;)V

    .line 165
    invoke-virtual {v1, p1}, Lcom/google/android/location/n;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Map;

    move-result-object v0

    .line 166
    iget-object v1, p0, Lcom/google/android/location/m;->c:Lcom/google/android/location/g;

    iget-object v2, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/location/g;->a(JLjava/util/Map;)V

    .line 167
    return-void
.end method

.method private d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 174
    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    .line 175
    const/16 v2, 0x3e8

    if-le v0, v2, :cond_19

    .line 178
    const/16 v2, 0xc

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v0, v1

    .line 179
    :goto_11
    if-ge v0, v2, :cond_19

    .line 180
    invoke-virtual {p1, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 183
    :cond_19
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Integer;
    .registers 11
    .parameter

    .prologue
    const/4 v5, 0x0

    const/16 v8, 0x10

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/16 v7, 0x8

    .line 256
    if-eqz p1, :cond_f

    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-nez v2, :cond_11

    :cond_f
    move-object v0, v5

    .line 281
    :goto_10
    return-object v0

    :cond_11
    move v2, v1

    move v3, v0

    .line 261
    :goto_13
    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v4

    if-ge v0, v4, :cond_3f

    .line 262
    invoke-virtual {p1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    .line 263
    invoke-virtual {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_3c

    invoke-virtual {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    if-nez v4, :cond_3c

    invoke-virtual {v6, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_3c

    .line 266
    add-int/lit8 v4, v3, 0x1

    .line 267
    invoke-virtual {v6, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v3

    .line 270
    cmpl-float v6, v3, v2

    if-ltz v6, :cond_52

    move v1, v2

    move v2, v3

    move v3, v4

    .line 261
    :cond_3c
    :goto_3c
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 278
    :cond_3f
    const/4 v0, 0x2

    if-lt v3, v0, :cond_50

    .line 279
    add-float v0, v2, v1

    const/high16 v1, 0x4000

    div-float/2addr v0, v1

    const/high16 v1, 0x4120

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_10

    :cond_50
    move-object v0, v5

    .line 281
    goto :goto_10

    :cond_52
    move v3, v4

    goto :goto_3c
.end method

.method public a(Ljava/util/Calendar;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/location/m;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/m$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/location/m$1;-><init>(Lcom/google/android/location/m;Ljava/util/Calendar;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 91
    return-void
.end method

.method public synthetic a(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 44
    check-cast p1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, p1}, Lcom/google/android/location/m;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 3
    .parameter

    .prologue
    .line 290
    if-eqz p1, :cond_4

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method
