.class public Lcom/google/android/location/os/real/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/k/a;
.implements Lcom/google/android/location/os/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/real/h$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/location/k/a",
        "<",
        "Lcom/google/android/location/os/h;",
        ">;",
        "Lcom/google/android/location/os/i;"
    }
.end annotation


# static fields
.field private static final l:[Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Lcom/google/android/location/os/e;

.field private final d:Lcom/google/android/location/os/real/c;

.field private final e:Lcom/google/android/location/os/real/h$a;

.field private final f:Z

.field private final g:Landroid/app/AlarmManager;

.field private final h:[Landroid/app/PendingIntent;

.field private final i:Lcom/google/android/location/os/real/d;

.field private final j:Lcom/google/android/location/i/a;

.field private final k:Lcom/google/android/location/os/h;

.field private final m:[Landroid/os/PowerManager$WakeLock;

.field private n:[Z

.field private o:[Landroid/net/wifi/WifiManager$WifiLock;

.field private final p:Landroid/net/wifi/WifiManager;

.field private final q:Landroid/hardware/SensorManager;

.field private final r:Landroid/location/LocationManager;

.field private final s:Lcom/google/android/location/d/a;

.field private final t:Ljava/util/concurrent/ExecutorService;

.field private final u:[B

.field private final v:J


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 114
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NetworkLocationLocator"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "NetworkLocationActiveCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "NetworkLocationBurstCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "NetworkLocationPassiveCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "NetworkLocationCacheUpdater"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "NetworkLocationCalibrationCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "NetworkLocationSCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "NetworkLocationSensorUploader"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "NetworkLocationActivityDetection"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "NetworkLocationInOutCollector"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "NetworkLocationBurstCollectionTrigger"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/location/os/real/h;->l:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/os/e;Lcom/google/android/location/os/real/h$a;Z)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/16 v6, 0xb

    const/4 v2, 0x0

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    new-array v0, v6, [Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    .line 122
    new-array v0, v6, [Landroid/os/PowerManager$WakeLock;

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->m:[Landroid/os/PowerManager$WakeLock;

    .line 123
    new-array v0, v6, [Z

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->n:[Z

    .line 124
    new-array v0, v6, [Landroid/net/wifi/WifiManager$WifiLock;

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->o:[Landroid/net/wifi/WifiManager$WifiLock;

    .line 151
    iput-object p1, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    .line 152
    iput-object p2, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    .line 153
    iput-object p3, p0, Lcom/google/android/location/os/real/h;->e:Lcom/google/android/location/os/real/h$a;

    .line 154
    iput-boolean p4, p0, Lcom/google/android/location/os/real/h;->f:Z

    .line 155
    new-instance v0, Lcom/google/android/location/d/a;

    invoke-direct {v0, p1, v7}, Lcom/google/android/location/d/a;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->s:Lcom/google/android/location/d/a;

    .line 156
    new-instance v0, Lcom/google/android/location/os/h;

    invoke-direct {v0, p0, p0, p0}, Lcom/google/android/location/os/h;-><init>(Lcom/google/android/location/os/c;Lcom/google/android/location/os/f;Lcom/google/android/location/k/a;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->k:Lcom/google/android/location/os/h;

    .line 157
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->k:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->a()V

    .line 158
    new-instance v0, Lcom/google/android/location/os/real/c;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->k:Lcom/google/android/location/os/h;

    invoke-direct {v0, p1, v1, p2}, Lcom/google/android/location/os/real/c;-><init>(Landroid/content/Context;Lcom/google/android/location/os/h;Lcom/google/android/location/os/e;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    .line 159
    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->g:Landroid/app/AlarmManager;

    .line 160
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->a:Ljava/lang/String;

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    aput-object v1, v0, v2

    .line 162
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->b:Ljava/lang/String;

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    aput-object v1, v0, v7

    .line 164
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v3, v3, Lcom/google/android/location/os/real/c;->c:Ljava/lang/String;

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    aput-object v1, v0, v8

    .line 166
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/4 v1, 0x3

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->d:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 168
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/4 v1, 0x4

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->e:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 170
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/4 v1, 0x5

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->f:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 172
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/4 v1, 0x6

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->g:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 174
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/4 v1, 0x7

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->h:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 176
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/16 v1, 0x8

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->i:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 178
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/16 v1, 0x9

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->j:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 180
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    const/16 v1, 0xa

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v4, v4, Lcom/google/android/location/os/real/c;->k:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v1

    .line 182
    new-instance v0, Lcom/google/android/location/os/real/h$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/os/real/h$1;-><init>(Lcom/google/android/location/os/real/h;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->t:Ljava/util/concurrent/ExecutorService;

    .line 191
    new-instance v0, Lcom/google/android/location/i/a;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->k:Lcom/google/android/location/os/h;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/i/a;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->j:Lcom/google/android/location/i/a;

    .line 192
    new-instance v0, Lcom/google/android/location/os/real/d;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->k:Lcom/google/android/location/os/h;

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->j:Lcom/google/android/location/i/a;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-direct {v0, p1, v1, v3, v4}, Lcom/google/android/location/os/real/d;-><init>(Landroid/content/Context;Lcom/google/android/location/os/h;Lcom/google/android/location/i/a;Lcom/google/android/location/os/real/c;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->i:Lcom/google/android/location/os/real/d;

    .line 193
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 194
    const-string v1, "wifi"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iput-object v1, p0, Lcom/google/android/location/os/real/h;->p:Landroid/net/wifi/WifiManager;

    move v1, v2

    .line 195
    :goto_140
    if-ge v1, v6, :cond_16d

    .line 196
    iget-object v3, p0, Lcom/google/android/location/os/real/h;->m:[Landroid/os/PowerManager$WakeLock;

    sget-object v4, Lcom/google/android/location/os/real/h;->l:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v0, v7, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    aput-object v4, v3, v1

    .line 197
    iget-object v3, p0, Lcom/google/android/location/os/real/h;->m:[Landroid/os/PowerManager$WakeLock;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 198
    iget-object v3, p0, Lcom/google/android/location/os/real/h;->o:[Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->p:Landroid/net/wifi/WifiManager;

    sget-object v5, Lcom/google/android/location/os/real/h;->l:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {v4, v8, v5}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v4

    aput-object v4, v3, v1

    .line 199
    iget-object v3, p0, Lcom/google/android/location/os/real/h;->o:[Landroid/net/wifi/WifiManager$WifiLock;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 195
    add-int/lit8 v1, v1, 0x1

    goto :goto_140

    .line 201
    :cond_16d
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->q:Landroid/hardware/SensorManager;

    .line 202
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/h;->r:Landroid/location/LocationManager;

    .line 203
    invoke-direct {p0}, Lcom/google/android/location/os/real/h;->J()[B

    move-result-object v0

    .line 204
    if-eqz v0, :cond_18c

    array-length v1, v0

    const/16 v2, 0x20

    if-eq v1, v2, :cond_190

    .line 205
    :cond_18c
    invoke-direct {p0}, Lcom/google/android/location/os/real/h;->I()[B

    move-result-object v0

    .line 207
    :cond_190
    iput-object v0, p0, Lcom/google/android/location/os/real/h;->u:[B

    .line 208
    sget-boolean v0, Lcom/google/android/location/os/real/d;->a:Z

    if-eqz v0, :cond_1a5

    .line 209
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/os/real/h;->v:J

    .line 216
    :goto_1a4
    return-void

    .line 214
    :cond_1a5
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/google/android/location/os/real/h;->v:J

    goto :goto_1a4
.end method

.method public static F()J
    .registers 2

    .prologue
    .line 527
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public static G()J
    .registers 2

    .prologue
    .line 531
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public static H()J
    .registers 4

    .prologue
    .line 535
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private I()[B
    .registers 7

    .prologue
    .line 219
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    .line 220
    const/16 v1, 0x20

    new-array v2, v1, [B

    .line 221
    invoke-virtual {v0, v2}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 222
    const/4 v1, 0x0

    .line 224
    :try_start_d
    new-instance v0, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/location/os/real/h;->g(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1d
    .catchall {:try_start_d .. :try_end_1d} :catchall_33
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_1d} :catch_28
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_1d} :catch_2e

    .line 225
    const/4 v1, 0x1

    :try_start_1e
    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 226
    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_24
    .catchall {:try_start_1e .. :try_end_24} :catchall_38
    .catch Ljava/io/FileNotFoundException; {:try_start_1e .. :try_end_24} :catch_40
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_24} :catch_3d

    .line 232
    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    .line 234
    :goto_27
    return-object v2

    .line 227
    :catch_28
    move-exception v0

    move-object v0, v1

    .line 232
    :goto_2a
    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    goto :goto_27

    .line 229
    :catch_2e
    move-exception v0

    .line 232
    :goto_2f
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    goto :goto_27

    :catchall_33
    move-exception v0

    :goto_34
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_38
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_34

    .line 229
    :catch_3d
    move-exception v1

    move-object v1, v0

    goto :goto_2f

    .line 227
    :catch_40
    move-exception v1

    goto :goto_2a
.end method

.method private J()[B
    .registers 7

    .prologue
    .line 242
    const/4 v1, 0x0

    .line 243
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 245
    :try_start_6
    new-instance v0, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/location/os/real/h;->g(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_16
    .catchall {:try_start_6 .. :try_end_16} :catchall_3a
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_16} :catch_47
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_16} :catch_35

    .line 247
    :try_start_16
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedShort()I

    .line 248
    const/16 v1, 0x20

    new-array v1, v1, [B

    .line 250
    :goto_1d
    invoke-virtual {v0, v1}, Ljava/io/DataInputStream;->read([B)I

    move-result v3

    if-ltz v3, :cond_31

    .line 251
    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_27
    .catchall {:try_start_16 .. :try_end_27} :catchall_3f
    .catch Ljava/io/FileNotFoundException; {:try_start_16 .. :try_end_27} :catch_28
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_27} :catch_44

    goto :goto_1d

    .line 253
    :catch_28
    move-exception v1

    .line 258
    :goto_29
    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    .line 260
    :goto_2c
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    .line 258
    :cond_31
    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    goto :goto_2c

    .line 255
    :catch_35
    move-exception v0

    .line 258
    :goto_36
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    goto :goto_2c

    :catchall_3a
    move-exception v0

    :goto_3b
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_3f
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_3b

    .line 255
    :catch_44
    move-exception v1

    move-object v1, v0

    goto :goto_36

    .line 253
    :catch_47
    move-exception v0

    move-object v0, v1

    goto :goto_29
.end method

.method public static a(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 343
    :try_start_0
    invoke-static {p0}, Lcom/google/android/location/os/real/h;->e(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 344
    invoke-static {p0}, Lcom/google/android/location/os/real/h;->f(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 345
    invoke-static {p0}, Lcom/google/android/location/os/real/h;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_15
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_15} :catch_22

    .line 352
    :goto_15
    :try_start_15
    invoke-static {p0}, Lcom/google/android/location/os/real/h;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/io/File;)Z
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_1c} :catch_20

    .line 357
    :goto_1c
    invoke-static {}, Lcom/google/android/location/os/real/d;->a()V

    .line 358
    return-void

    .line 353
    :catch_20
    move-exception v0

    goto :goto_1c

    .line 346
    :catch_22
    move-exception v0

    goto :goto_15
.end method

.method public static b(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 376
    const-string v0, "cache.cell"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 377
    const-string v0, "cache.wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 378
    const-string v0, "gls.platform.key"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 381
    const-string v0, "nlp_GlsPlatformKey"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 385
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "nlp_state"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 386
    invoke-static {p0}, Lcom/google/android/location/os/real/h;->e(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    .line 387
    const/4 v0, 0x0

    .line 389
    :try_start_24
    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_27
    .catch Ljava/lang/SecurityException; {:try_start_24 .. :try_end_27} :catch_38

    move-result v0

    .line 393
    :goto_28
    if-eqz v0, :cond_32

    .line 395
    invoke-static {}, Lcom/google/android/location/os/real/j;->a()Lcom/google/android/location/os/real/j;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/j;->a(Ljava/io/File;)V

    .line 400
    :goto_31
    return-void

    .line 398
    :cond_32
    const-string v0, "nlp_state"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    goto :goto_31

    .line 390
    :catch_38
    move-exception v1

    goto :goto_28
.end method

.method public static c(Landroid/content/Context;)Ljava/io/File;
    .registers 4
    .parameter

    .prologue
    .line 808
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_s"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static d(Landroid/content/Context;)Ljava/io/File;
    .registers 4
    .parameter

    .prologue
    .line 817
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_ioh"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static e(Landroid/content/Context;)Ljava/io/File;
    .registers 4
    .parameter

    .prologue
    .line 403
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_state"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static f(Landroid/content/Context;)Ljava/io/File;
    .registers 4
    .parameter

    .prologue
    .line 407
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/google/android/location/os/real/h;->h(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_devices"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static g(Landroid/content/Context;)Ljava/io/File;
    .registers 4
    .parameter

    .prologue
    .line 411
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_ck"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static h(Landroid/content/Context;)Ljava/io/File;
    .registers 2
    .parameter

    .prologue
    .line 559
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public A()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 865
    iget-object v1, p0, Lcom/google/android/location/os/real/h;->q:Landroid/hardware/SensorManager;

    invoke-virtual {v1, v0}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v1

    .line 866
    if-eqz v1, :cond_10

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_10

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public B()J
    .registers 3

    .prologue
    .line 317
    iget-wide v0, p0, Lcom/google/android/location/os/real/h;->v:J

    return-wide v0
.end method

.method public C()Lcom/google/android/location/i/a;
    .registers 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->j:Lcom/google/android/location/i/a;

    return-object v0
.end method

.method public D()Lcom/google/android/location/os/h;
    .registers 2

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->k:Lcom/google/android/location/os/h;

    return-object v0
.end method

.method public E()V
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 444
    iget-object v1, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/c;->a()V

    .line 448
    iget-object v1, p0, Lcom/google/android/location/os/real/h;->t:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 450
    :try_start_b
    iget-object v1, p0, Lcom/google/android/location/os/real/h;->t:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_14
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_14} :catch_34

    .line 454
    :goto_14
    invoke-virtual {p0, v0}, Lcom/google/android/location/os/real/h;->a(I)V

    .line 455
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/location/os/real/h;->a(I)V

    .line 456
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/google/android/location/os/real/h;->a(I)V

    .line 457
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/location/os/real/h;->a(I)V

    .line 458
    :goto_23
    const/16 v1, 0xb

    if-ge v0, v1, :cond_33

    .line 459
    iget-object v1, p0, Lcom/google/android/location/os/real/h;->n:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_30

    .line 460
    invoke-virtual {p0, v0}, Lcom/google/android/location/os/real/h;->c(I)V

    .line 458
    :cond_30
    add-int/lit8 v0, v0, 0x1

    goto :goto_23

    .line 463
    :cond_33
    return-void

    .line 451
    :catch_34
    move-exception v1

    goto :goto_14
.end method

.method public a()J
    .registers 3

    .prologue
    .line 513
    invoke-static {}, Lcom/google/android/location/os/real/h;->F()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/String;Lcom/google/android/location/c/g;Ljava/lang/String;)Lcom/google/android/location/c/q;
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x0

    .line 828
    invoke-static {}, Lcom/google/android/location/os/real/d;->b()Ljava/lang/String;

    move-result-object v3

    .line 829
    if-nez v3, :cond_9

    move-object v0, v10

    .line 840
    :goto_8
    return-object v0

    .line 834
    :cond_9
    :try_start_9
    new-instance v0, Lcom/google/android/location/c/f;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/location/os/real/h;->j()Ljavax/crypto/SecretKey;

    move-result-object v2

    invoke-interface {v2}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/location/os/real/h;->l()[B

    move-result-object v5

    const/4 v6, 0x1

    iget-object v2, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v2}, Lcom/google/android/location/os/real/c;->c()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v8

    new-instance v9, Lcom/google/android/location/k/a/c;

    invoke-static {}, Lcom/google/android/location/k/a/a;->a()Lcom/google/android/location/k/a/b;

    move-result-object v2

    invoke-direct {v9, p3, v2}, Lcom/google/android/location/k/a/c;-><init>(Ljava/lang/String;Lcom/google/android/location/k/a/b;)V

    move-object v2, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/c/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[B[BZLcom/google/android/location/c/g;Landroid/os/Looper;Lcom/google/android/location/k/a/c;)V
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_32} :catch_33

    goto :goto_8

    .line 837
    :catch_33
    move-exception v0

    move-object v0, v10

    .line 840
    goto :goto_8
.end method

.method public a(Ljava/util/Set;Ljava/util/Map;JLcom/google/android/location/c/l;Ljava/lang/String;)Lcom/google/android/location/c/r;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/c/F;",
            "Ljava/lang/Integer;",
            ">;J",
            "Lcom/google/android/location/c/l;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/location/c/r;"
        }
    .end annotation

    .prologue
    .line 716
    new-instance v7, Lcom/google/android/location/os/real/b;

    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0}, Lcom/google/android/location/os/real/c;->c()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-direct {v7, v0, p5, v1}, Lcom/google/android/location/os/real/b;-><init>(Landroid/os/Handler;Lcom/google/android/location/c/l;Landroid/content/Context;)V

    .line 718
    new-instance v0, Lcom/google/android/location/c/A;

    sget-object v4, Lcom/google/android/location/c/j$a;->a:Lcom/google/android/location/c/j$a;

    const/4 v5, 0x1

    move-object v1, p1

    move-wide v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/c/A;-><init>(Ljava/util/Set;JLcom/google/android/location/c/j$a;Z)V

    .line 720
    if-eqz p2, :cond_41

    .line 721
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_21
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_41

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 722
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/c/F;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v2, v1}, Lcom/google/android/location/c/j;->a(Lcom/google/android/location/c/F;I)V

    goto :goto_21

    .line 725
    :cond_41
    new-instance v1, Lcom/google/android/location/c/I;

    iget-object v2, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/location/os/real/h;->s:Lcom/google/android/location/d/a;

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v8, Lcom/google/android/location/k/a/c;

    invoke-static {}, Lcom/google/android/location/k/a/a;->a()Lcom/google/android/location/k/a/b;

    move-result-object v3

    invoke-direct {v8, p6, v3}, Lcom/google/android/location/k/a/c;-><init>(Ljava/lang/String;Lcom/google/android/location/k/a/b;)V

    move-object v3, v0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/location/c/I;-><init>(Landroid/content/Context;Lcom/google/android/location/c/j;Lcom/google/android/location/d/a;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    return-object v1
.end method

.method public a(Ljava/util/Set;Ljava/util/Map;JLjava/lang/String;Ljava/lang/Integer;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;ZLcom/google/android/location/c/l;Ljava/lang/String;)Lcom/google/android/location/c/r;
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/c/F;",
            "Ljava/lang/Integer;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Z",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Z",
            "Lcom/google/android/location/c/l;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/location/c/r;"
        }
    .end annotation

    .prologue
    .line 742
    new-instance v10, Lcom/google/android/location/os/real/b;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/c;->c()Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    move-object/from16 v0, p10

    invoke-direct {v10, v1, v0, v2}, Lcom/google/android/location/os/real/b;-><init>(Landroid/os/Handler;Lcom/google/android/location/c/l;Landroid/content/Context;)V

    .line 744
    new-instance v1, Lcom/google/android/location/c/A;

    sget-object v5, Lcom/google/android/location/c/j$a;->b:Lcom/google/android/location/c/j$a;

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/location/os/real/h;->l()[B

    move-result-object v8

    move-object v2, p1

    move-wide v3, p3

    move-object/from16 v6, p5

    move/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/google/android/location/c/A;-><init>(Ljava/util/Set;JLcom/google/android/location/c/j$a;Ljava/lang/String;Ljava/lang/String;[BZ)V

    .line 751
    move/from16 v0, p7

    invoke-interface {v1, v0}, Lcom/google/android/location/c/j;->a(Z)V

    .line 752
    if-eqz p2, :cond_50

    .line 753
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_30
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_50

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 754
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/location/c/F;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v3, v2}, Lcom/google/android/location/c/j;->a(Lcom/google/android/location/c/F;I)V

    goto :goto_30

    .line 757
    :cond_50
    new-instance v2, Lcom/google/android/location/c/I;

    iget-object v3, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/location/os/real/h;->s:Lcom/google/android/location/d/a;

    new-instance v9, Lcom/google/android/location/k/a/c;

    invoke-static {}, Lcom/google/android/location/k/a/a;->a()Lcom/google/android/location/k/a/b;

    move-result-object v4

    move-object/from16 v0, p11

    invoke-direct {v9, v0, v4}, Lcom/google/android/location/k/a/c;-><init>(Ljava/lang/String;Lcom/google/android/location/k/a/b;)V

    move-object v4, v1

    move-object/from16 v6, p6

    move-object/from16 v7, p8

    move-object v8, v10

    invoke-direct/range {v2 .. v9}, Lcom/google/android/location/c/I;-><init>(Landroid/content/Context;Lcom/google/android/location/c/j;Lcom/google/android/location/d/a;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    return-object v2
.end method

.method public a(Ljava/lang/String;)Ljava/io/InputStream;
    .registers 4
    .parameter

    .prologue
    .line 689
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_a} :catch_c

    move-result-object v0

    .line 692
    :goto_b
    return-object v0

    .line 690
    :catch_c
    move-exception v0

    .line 692
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->u:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 499
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->g:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 500
    return-void
.end method

.method public a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 871
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/c;->a(II)V

    .line 872
    return-void
.end method

.method public a(IJ)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/os/e;->a(IJ)V

    .line 493
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->g:Landroid/app/AlarmManager;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/os/real/h;->h:[Landroid/app/PendingIntent;

    aget-object v2, v2, p1

    invoke-virtual {v0, v1, p2, p3, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 494
    return-void
.end method

.method public a(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 421
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/c;->a(IZ)V

    .line 422
    return-void
.end method

.method public a(Lcom/google/android/location/a/n$b;)V
    .registers 3
    .parameter

    .prologue
    .line 777
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/a/n$b;)V

    .line 778
    return-void
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .registers 4
    .parameter

    .prologue
    .line 782
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    .line 783
    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    if-eq v0, v1, :cond_12

    .line 784
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->e:Lcom/google/android/location/os/real/h$a;

    invoke-interface {v0, p1}, Lcom/google/android/location/os/real/h$a;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    .line 786
    :cond_12
    return-void
.end method

.method public a(Lcom/google/android/location/e/t;)V
    .registers 3
    .parameter

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/e/t;)V

    .line 431
    return-void
.end method

.method public a(Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 634
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/e/t;)V

    .line 635
    if-eqz p1, :cond_19

    iget-object v0, p1, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    if-eqz v0, :cond_19

    iget-object v0, p1, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v0, v0, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    if-eqz v0, :cond_19

    iget-object v0, p1, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v0, v0, Lcom/google/android/location/e/o;->d:Lcom/google/android/location/e/o$a;

    sget-object v1, Lcom/google/android/location/e/o$a;->a:Lcom/google/android/location/e/o$a;

    if-eq v0, v1, :cond_1a

    .line 641
    :cond_19
    :goto_19
    return-void

    .line 640
    :cond_1a
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->e:Lcom/google/android/location/os/real/h$a;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/real/h$a;->a(Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;)V

    goto :goto_19
.end method

.method public a(Lcom/google/android/location/os/a;)V
    .registers 4
    .parameter

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/a;)V

    .line 485
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-boolean v1, p0, Lcom/google/android/location/os/real/h;->f:Z

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/real/c;->b(Z)V

    .line 486
    return-void
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .registers 3
    .parameter

    .prologue
    .line 767
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    if-eqz v0, :cond_9

    .line 771
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/h;)V

    .line 773
    :cond_9
    return-void
.end method

.method public a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 894
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V

    .line 895
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 584
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->x:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 585
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->i:Lcom/google/android/location/os/real/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 586
    return-void
.end method

.method public a(Ljava/io/File;)V
    .registers 4
    .parameter

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->B:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 577
    invoke-static {}, Lcom/google/android/location/os/real/j;->a()Lcom/google/android/location/os/real/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/j;->a(Ljava/io/File;)V

    .line 578
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 89
    check-cast p1, Lcom/google/android/location/os/h;

    invoke-virtual {p0, p1}, Lcom/google/android/location/os/real/h;->a(Lcom/google/android/location/os/h;)V

    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    invoke-virtual {v0, p2}, Lcom/google/android/location/os/e;->e(Z)V

    .line 614
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->s:Lcom/google/android/location/d/a;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/d/a;Ljava/lang/String;Z)V

    .line 615
    return-void
.end method

.method public a(Ljava/text/Format;Ljava/io/PrintWriter;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 878
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->s:Lcom/google/android/location/d/a;

    invoke-virtual {p0}, Lcom/google/android/location/os/real/h;->c()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2, p2}, Lcom/google/android/location/d/a;->a(Ljava/text/Format;JLjava/io/PrintWriter;)V

    .line 879
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/c;->a(Z)V

    .line 440
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 889
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/c;->a(ZLjava/lang/String;)V

    .line 890
    return-void
.end method

.method public b()J
    .registers 3

    .prologue
    .line 518
    invoke-static {}, Lcom/google/android/location/os/real/h;->G()J

    move-result-wide v0

    return-wide v0
.end method

.method public b(I)V
    .registers 5
    .parameter

    .prologue
    .line 647
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/e;->c(I)V

    .line 648
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->n:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_2a

    .line 649
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wakeLock "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already acquired"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 651
    :cond_2a
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->n:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    .line 652
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->m:[Landroid/os/PowerManager$WakeLock;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 653
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->o:[Landroid/net/wifi/WifiManager$WifiLock;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 654
    return-void
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 590
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->y:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 591
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->i:Lcom/google/android/location/os/real/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 592
    return-void
.end method

.method public c()J
    .registers 3

    .prologue
    .line 523
    invoke-static {}, Lcom/google/android/location/os/real/h;->H()J

    move-result-wide v0

    return-wide v0
.end method

.method public c(I)V
    .registers 5
    .parameter

    .prologue
    .line 658
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/e;->d(I)V

    .line 659
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->n:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_2a

    .line 660
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wakeLock "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already released"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 662
    :cond_2a
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->o:[Landroid/net/wifi/WifiManager$WifiLock;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 663
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->n:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    .line 664
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->m:[Landroid/os/PowerManager$WakeLock;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 665
    return-void
.end method

.method public c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 596
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->z:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 597
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->i:Lcom/google/android/location/os/real/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 598
    return-void
.end method

.method public d()Ljava/io/File;
    .registers 3

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->A:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 543
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->w:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 603
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->i:Lcom/google/android/location/os/real/d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/d;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 604
    return-void
.end method

.method public e()Ljava/io/File;
    .registers 3

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->C:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 549
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/io/File;
    .registers 3

    .prologue
    .line 554
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->D:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 555
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/h;->h(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/io/File;
    .registers 3

    .prologue
    .line 564
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->E:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 565
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/io/File;
    .registers 3

    .prologue
    .line 570
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->F:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 571
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public i()V
    .registers 3

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->v:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 506
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->d:Lcom/google/android/location/os/real/c;

    invoke-virtual {v0}, Lcom/google/android/location/os/real/c;->b()V

    .line 507
    return-void
.end method

.method public j()Ljavax/crypto/SecretKey;
    .registers 13

    .prologue
    const/16 v11, 0x18

    const/16 v10, 0x10

    const/16 v9, 0x8

    const/4 v8, 0x0

    const-wide/16 v6, 0xff

    .line 271
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->G:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 272
    invoke-virtual {p0}, Lcom/google/android/location/os/real/h;->B()J

    move-result-wide v0

    .line 274
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_22

    .line 275
    new-instance v0, Ljava/io/IOException;

    const-string v1, "no android ID; can\'t access encrypted cache"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :cond_22
    const/16 v2, 0x20

    new-array v2, v2, [B

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v6

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v8

    const/4 v3, 0x1

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x2

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x3

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x4

    ushr-long v4, v0, v11

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x5

    ushr-long v4, v0, v10

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x6

    ushr-long v4, v0, v9

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x7

    ushr-long v4, v0, v8

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v6

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v9

    const/16 v3, 0x9

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xa

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xb

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xc

    ushr-long v4, v0, v11

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xd

    ushr-long v4, v0, v10

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xe

    ushr-long v4, v0, v9

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xf

    ushr-long v4, v0, v8

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v6

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v10

    const/16 v3, 0x11

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x12

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x13

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x14

    ushr-long v4, v0, v11

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x15

    ushr-long v4, v0, v10

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x16

    ushr-long v4, v0, v9

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x17

    ushr-long v4, v0, v8

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v6

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v11

    const/16 v3, 0x19

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1a

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1b

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1c

    ushr-long v4, v0, v11

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1d

    ushr-long v4, v0, v10

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1e

    ushr-long v4, v0, v9

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1f

    ushr-long/2addr v0, v8

    and-long/2addr v0, v6

    long-to-int v0, v0

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    .line 312
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "AES"

    invoke-direct {v0, v2, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v0
.end method

.method public k()Ljavax/crypto/SecretKey;
    .registers 2

    .prologue
    .line 324
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/os/real/h;->j()Ljavax/crypto/SecretKey;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    .line 327
    :goto_4
    return-object v0

    .line 325
    :catch_5
    move-exception v0

    .line 327
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public l()[B
    .registers 3

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->u:[B

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->u:[B

    array-length v1, v1

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a([BI)[B

    move-result-object v0

    return-object v0
.end method

.method public m()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 619
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 621
    :try_start_b
    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
    :try_end_10
    .catch Ljava/lang/SecurityException; {:try_start_b .. :try_end_10} :catch_12
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_10} :catch_15

    move-result v0

    .line 626
    :goto_11
    return v0

    .line 622
    :catch_12
    move-exception v0

    move v0, v1

    .line 624
    goto :goto_11

    .line 625
    :catch_15
    move-exception v0

    move v0, v1

    .line 626
    goto :goto_11
.end method

.method public n()V
    .registers 3

    .prologue
    .line 671
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->c:Lcom/google/android/location/os/e;

    sget-object v1, Lcom/google/android/location/os/d;->N:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 672
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->p:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    .line 673
    return-void
.end method

.method public o()Ljava/io/InputStream;
    .registers 4

    .prologue
    .line 677
    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method public p()Lcom/google/android/location/os/g;
    .registers 2

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->e:Lcom/google/android/location/os/real/h$a;

    invoke-interface {v0}, Lcom/google/android/location/os/real/h$a;->d()Lcom/google/android/location/os/g;

    move-result-object v0

    return-object v0
.end method

.method public q()Lcom/google/android/location/os/i$a;
    .registers 6

    .prologue
    .line 698
    new-instance v0, Lcom/google/android/location/os/i$a;

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v3, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/os/i$a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public r()Ljava/util/concurrent/ExecutorService;
    .registers 2

    .prologue
    .line 707
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->t:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .registers 3

    .prologue
    .line 790
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 792
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public t()Ljava/lang/String;
    .registers 3

    .prologue
    .line 797
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 799
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Z
    .registers 3

    .prologue
    .line 822
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->q:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/h;->r:Landroid/location/LocationManager;

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/k;->a(Landroid/hardware/SensorManager;Landroid/location/LocationManager;)Z

    move-result v0

    return v0
.end method

.method public v()Ljava/io/File;
    .registers 2

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/h;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public w()Ljava/io/File;
    .registers 2

    .prologue
    .line 813
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/h;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public x()Z
    .registers 2

    .prologue
    .line 845
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->p:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->reconnect()Z

    move-result v0

    return v0
.end method

.method public y()I
    .registers 2

    .prologue
    .line 850
    new-instance v0, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v0}, Landroid/os/Debug$MemoryInfo;-><init>()V

    .line 851
    invoke-static {v0}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    .line 852
    invoke-virtual {v0}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    move-result v0

    return v0
.end method

.method public z()I
    .registers 5

    .prologue
    .line 857
    new-instance v1, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v1}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 858
    iget-object v0, p0, Lcom/google/android/location/os/real/h;->b:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 859
    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 860
    iget-wide v0, v1, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
