.class public interface abstract Lcom/google/android/location/os/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/os/c;
.implements Lcom/google/android/location/os/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/i$b;,
        Lcom/google/android/location/os/i$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 75
    const-string v0, "com\\.google\\.android\\.apps\\.maps\\w*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/i;->a:Ljava/util/regex/Pattern;

    return-void
.end method


# virtual methods
.method public abstract A()Z
.end method

.method public abstract B()J
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/location/c/g;Ljava/lang/String;)Lcom/google/android/location/c/q;
.end method

.method public abstract a(Ljava/util/Set;Ljava/util/Map;JLcom/google/android/location/c/l;Ljava/lang/String;)Lcom/google/android/location/c/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/c/F;",
            "Ljava/lang/Integer;",
            ">;J",
            "Lcom/google/android/location/c/l;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/location/c/r;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/Set;Ljava/util/Map;JLjava/lang/String;Ljava/lang/Integer;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;ZLcom/google/android/location/c/l;Ljava/lang/String;)Lcom/google/android/location/c/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/c/F;",
            "Ljava/lang/Integer;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Z",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Z",
            "Lcom/google/android/location/c/l;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/location/c/r;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;)Ljava/io/InputStream;
.end method

.method public abstract a(I)V
.end method

.method public abstract a(IJ)V
.end method

.method public abstract a(Lcom/google/android/location/a/n$b;)V
.end method

.method public abstract a(Lcom/google/android/location/clientlib/NlpActivity;)V
.end method

.method public abstract a(Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;)V
.end method

.method public abstract a(Lcom/google/android/location/os/a;)V
.end method

.method public abstract a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V
.end method

.method public abstract a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
.end method

.method public abstract a(Ljava/lang/String;Z)V
.end method

.method public abstract b(I)V
.end method

.method public abstract b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
.end method

.method public abstract c(I)V
.end method

.method public abstract c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
.end method

.method public abstract d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
.end method

.method public abstract i()V
.end method

.method public abstract j()Ljavax/crypto/SecretKey;
.end method

.method public abstract k()Ljavax/crypto/SecretKey;
.end method

.method public abstract l()[B
.end method

.method public abstract m()Z
.end method

.method public abstract n()V
.end method

.method public abstract o()Ljava/io/InputStream;
.end method

.method public abstract p()Lcom/google/android/location/os/g;
.end method

.method public abstract q()Lcom/google/android/location/os/i$a;
.end method

.method public abstract r()Ljava/util/concurrent/ExecutorService;
.end method

.method public abstract s()Ljava/lang/String;
.end method

.method public abstract t()Ljava/lang/String;
.end method

.method public abstract u()Z
.end method

.method public abstract v()Ljava/io/File;
.end method

.method public abstract w()Ljava/io/File;
.end method

.method public abstract x()Z
.end method

.method public abstract y()I
.end method

.method public abstract z()I
.end method
