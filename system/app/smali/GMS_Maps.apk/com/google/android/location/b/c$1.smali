.class Lcom/google/android/location/b/c$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/b/c;->a(Ljava/lang/Object;J)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Object;

.field final synthetic b:Lcom/google/android/location/e/A;

.field final synthetic c:Lcom/google/android/location/b/c;


# direct methods
.method constructor <init>(Lcom/google/android/location/b/c;Ljava/lang/Object;Lcom/google/android/location/e/A;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 311
    iput-object p1, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    iput-object p2, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/location/b/c$1;->b:Lcom/google/android/location/e/A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 11

    .prologue
    const/4 v9, 0x0

    .line 317
    monitor-enter p0

    .line 318
    :try_start_2
    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->a(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/j;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 319
    monitor-exit p0

    .line 364
    :goto_11
    return-void

    .line 321
    :cond_12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_2 .. :try_end_13} :catchall_72

    .line 326
    new-instance v6, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->b(Lcom/google/android/location/b/c;)Ljava/io/File;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/location/b/c$1;->b:Lcom/google/android/location/e/A;

    invoke-virtual {v0}, Lcom/google/android/location/e/A;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v6, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 329
    :try_start_26
    new-instance v0, Lcom/google/android/location/os/j;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v4}, Lcom/google/android/location/b/c;->c(Lcom/google/android/location/b/c;)[B

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v5}, Lcom/google/android/location/b/c;->d(Lcom/google/android/location/b/c;)Lcom/google/android/location/e/x;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/location/e/x;->a()Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-result-object v5

    invoke-static {}, Lcom/google/common/base/Predicates;->alwaysTrue()Lcom/google/common/base/K;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v8}, Lcom/google/android/location/b/c;->e(Lcom/google/android/location/b/c;)Lcom/google/android/location/os/f;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/os/j;-><init>(ILjavax/crypto/SecretKey;I[BLcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/File;Lcom/google/common/base/K;Lcom/google/android/location/os/f;)V

    .line 334
    invoke-virtual {v0}, Lcom/google/android/location/os/j;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 335
    new-instance v1, Lcom/google/android/location/e/A;

    iget-object v2, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v2}, Lcom/google/android/location/b/c;->d(Lcom/google/android/location/b/c;)Lcom/google/android/location/e/x;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/location/e/x;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/b/c$1;->b:Lcom/google/android/location/e/A;

    invoke-virtual {v2}, Lcom/google/android/location/e/A;->b()J

    move-result-wide v2

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/location/e/A;-><init>(Ljava/lang/Object;J)V

    .line 338
    monitor-enter p0
    :try_end_62
    .catchall {:try_start_26 .. :try_end_62} :catchall_9c
    .catch Ljava/io/FileNotFoundException; {:try_start_26 .. :try_end_62} :catch_78
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_62} :catch_a1

    .line 339
    :try_start_62
    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->a(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/j;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/b/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    monitor-exit p0
    :try_end_6e
    .catchall {:try_start_62 .. :try_end_6e} :catchall_75

    .line 362
    invoke-static {v9}, Lcom/google/android/location/b/c;->a(Ljava/io/Closeable;)V

    goto :goto_11

    .line 321
    :catchall_72
    move-exception v0

    :try_start_73
    monitor-exit p0
    :try_end_74
    .catchall {:try_start_73 .. :try_end_74} :catchall_72

    throw v0

    .line 340
    :catchall_75
    move-exception v0

    :try_start_76
    monitor-exit p0
    :try_end_77
    .catchall {:try_start_76 .. :try_end_77} :catchall_75

    :try_start_77
    throw v0
    :try_end_78
    .catchall {:try_start_77 .. :try_end_78} :catchall_9c
    .catch Ljava/io/FileNotFoundException; {:try_start_77 .. :try_end_78} :catch_78
    .catch Ljava/io/IOException; {:try_start_77 .. :try_end_78} :catch_a1

    .line 342
    :catch_78
    move-exception v0

    .line 347
    :try_start_79
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 348
    monitor-enter p0
    :try_end_7d
    .catchall {:try_start_79 .. :try_end_7d} :catchall_9c

    .line 349
    :try_start_7d
    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->a(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/j;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->f(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/c$a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    monitor-exit p0
    :try_end_94
    .catchall {:try_start_7d .. :try_end_94} :catchall_99

    .line 362
    invoke-static {v9}, Lcom/google/android/location/b/c;->a(Ljava/io/Closeable;)V

    goto/16 :goto_11

    .line 351
    :catchall_99
    move-exception v0

    :try_start_9a
    monitor-exit p0
    :try_end_9b
    .catchall {:try_start_9a .. :try_end_9b} :catchall_99

    :try_start_9b
    throw v0
    :try_end_9c
    .catchall {:try_start_9b .. :try_end_9c} :catchall_9c

    .line 362
    :catchall_9c
    move-exception v0

    invoke-static {v9}, Lcom/google/android/location/b/c;->a(Ljava/io/Closeable;)V

    throw v0

    .line 352
    :catch_a1
    move-exception v0

    .line 356
    :try_start_a2
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 357
    monitor-enter p0
    :try_end_a6
    .catchall {:try_start_a2 .. :try_end_a6} :catchall_9c

    .line 358
    :try_start_a6
    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->a(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/j;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    iget-object v0, p0, Lcom/google/android/location/b/c$1;->c:Lcom/google/android/location/b/c;

    invoke-static {v0}, Lcom/google/android/location/b/c;->f(Lcom/google/android/location/b/c;)Lcom/google/android/location/b/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/c$1;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/c$a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    monitor-exit p0
    :try_end_bd
    .catchall {:try_start_a6 .. :try_end_bd} :catchall_c2

    .line 362
    invoke-static {v9}, Lcom/google/android/location/b/c;->a(Ljava/io/Closeable;)V

    goto/16 :goto_11

    .line 360
    :catchall_c2
    move-exception v0

    :try_start_c3
    monitor-exit p0
    :try_end_c4
    .catchall {:try_start_c3 .. :try_end_c4} :catchall_c2

    :try_start_c4
    throw v0
    :try_end_c5
    .catchall {:try_start_c4 .. :try_end_c5} :catchall_9c
.end method
