.class Lcom/google/android/location/c/I$a;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/I;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/I$a$a;
    }
.end annotation


# instance fields
.field private volatile a:Lcom/google/android/location/c/l;

.field private volatile b:Landroid/content/Context;

.field private volatile c:Lcom/google/android/location/c/j;

.field private volatile d:Lcom/google/android/location/c/y;

.field private e:Lcom/google/android/location/c/x;

.field private volatile f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private g:Lcom/google/android/location/c/r$a;

.field private h:Lcom/google/android/location/c/z;

.field private final i:Ljava/lang/Integer;

.field private final j:Lcom/google/android/location/k/a/c;

.field private final k:Lcom/google/android/location/d/a;

.field private final l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final m:Ljava/lang/Object;

.field private volatile n:Z

.field private final o:Ljava/util/concurrent/CountDownLatch;

.field private p:Lcom/google/android/location/c/l;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/c/j;Ljava/util/concurrent/CountDownLatch;Lcom/google/android/location/d/a;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/Integer;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 157
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/c/I$a;->m:Ljava/lang/Object;

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c/I$a;->n:Z

    .line 284
    new-instance v0, Lcom/google/android/location/c/I$a$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/I$a$1;-><init>(Lcom/google/android/location/c/I$a;)V

    iput-object v0, p0, Lcom/google/android/location/c/I$a;->p:Lcom/google/android/location/c/l;

    .line 167
    const-string v0, "SignalCollector.ScannerThread"

    invoke-virtual {p0, v0}, Lcom/google/android/location/c/I$a;->setName(Ljava/lang/String;)V

    .line 168
    iput-object p2, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    .line 169
    iput-object p1, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    .line 170
    iput-object p7, p0, Lcom/google/android/location/c/I$a;->a:Lcom/google/android/location/c/l;

    .line 171
    iput-object p6, p0, Lcom/google/android/location/c/I$a;->i:Ljava/lang/Integer;

    .line 172
    iput-object p8, p0, Lcom/google/android/location/c/I$a;->j:Lcom/google/android/location/k/a/c;

    .line 173
    iput-object p3, p0, Lcom/google/android/location/c/I$a;->o:Ljava/util/concurrent/CountDownLatch;

    .line 174
    iput-object p5, p0, Lcom/google/android/location/c/I$a;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 175
    iput-object p4, p0, Lcom/google/android/location/c/I$a;->k:Lcom/google/android/location/d/a;

    .line 176
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/I$a;Lcom/google/android/location/c/z;)Lcom/google/android/location/c/z;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/location/c/I$a;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/location/c/I$a;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/location/c/I$a;)V
    .registers 1
    .parameter

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/location/c/I$a;->b()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/c/I$a;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->m:Ljava/lang/Object;

    return-object v0
.end method

.method private b()V
    .registers 3

    .prologue
    .line 381
    iget-object v1, p0, Lcom/google/android/location/c/I$a;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 382
    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Lcom/google/android/location/c/I$a;->n:Z

    .line 383
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->e:Lcom/google/android/location/c/x;

    if-eqz v0, :cond_15

    invoke-virtual {p0}, Lcom/google/android/location/c/I$a;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 384
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->e:Lcom/google/android/location/c/x;

    invoke-virtual {v0}, Lcom/google/android/location/c/x;->a()V

    .line 386
    :cond_15
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    if-eqz v0, :cond_24

    invoke-virtual {p0}, Lcom/google/android/location/c/I$a;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 387
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    invoke-virtual {v0}, Lcom/google/android/location/c/z;->a()V

    .line 389
    :cond_24
    monitor-exit v1

    .line 390
    return-void

    .line 389
    :catchall_26
    move-exception v0

    monitor-exit v1
    :try_end_28
    .catchall {:try_start_4 .. :try_end_28} :catchall_26

    throw v0
.end method

.method static synthetic c(Lcom/google/android/location/c/I$a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/r$a;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->g:Lcom/google/android/location/c/r$a;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/c/I$a;)Z
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/google/android/location/c/I$a;->n:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/l;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->a:Lcom/google/android/location/c/l;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/k/a/c;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->j:Lcom/google/android/location/k/a/c;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/location/c/I$a;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/j;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/d/a;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->k:Lcom/google/android/location/d/a;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/location/c/I$a;)Ljava/lang/Integer;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->i:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/location/c/I$a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/location/c/I$a;)Lcom/google/android/location/c/z;
    .registers 2
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 393
    iget-object v1, p0, Lcom/google/android/location/c/I$a;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 394
    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Lcom/google/android/location/c/I$a;->n:Z

    .line 395
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    if-eqz v0, :cond_15

    invoke-virtual {p0}, Lcom/google/android/location/c/I$a;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 396
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    invoke-virtual {v0}, Lcom/google/android/location/c/z;->b()V

    .line 398
    :cond_15
    monitor-exit v1

    .line 399
    return-void

    .line 398
    :catchall_17
    move-exception v0

    monitor-exit v1
    :try_end_19
    .catchall {:try_start_4 .. :try_end_19} :catchall_17

    throw v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 3
    .parameter

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    if-eqz v0, :cond_b

    .line 410
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    invoke-virtual {v0, p1}, Lcom/google/android/location/c/z;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    .line 413
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public run()V
    .registers 11

    .prologue
    const/4 v2, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 185
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 186
    invoke-virtual {p0}, Lcom/google/android/location/c/I$a;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v9

    .line 187
    invoke-virtual {v9, v8}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 188
    invoke-virtual {v9}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 190
    :try_start_1b
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 191
    new-instance v0, Lcom/google/android/location/c/I$a$a;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/I$a$a;-><init>(Lcom/google/android/location/c/I$a;)V
    :try_end_23
    .catchall {:try_start_1b .. :try_end_23} :catchall_a0

    .line 195
    :try_start_23
    iget-object v1, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    invoke-interface {v1}, Lcom/google/android/location/c/j;->i()Lcom/google/android/location/c/j;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    .line 196
    iget-object v1, p0, Lcom/google/android/location/c/I$a;->d:Lcom/google/android/location/c/y;

    if-eqz v1, :cond_6b

    .line 198
    new-instance v1, Lcom/google/android/location/c/x;

    iget-object v2, p0, Lcom/google/android/location/c/I$a;->j:Lcom/google/android/location/k/a/c;

    invoke-direct {v1, v0, v2}, Lcom/google/android/location/c/x;-><init>(Landroid/os/Handler;Lcom/google/android/location/k/a/c;)V

    iput-object v1, p0, Lcom/google/android/location/c/I$a;->e:Lcom/google/android/location/c/x;

    .line 199
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->e:Lcom/google/android/location/c/x;

    iget-object v1, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/c/I$a;->d:Lcom/google/android/location/c/y;

    iget-object v3, p0, Lcom/google/android/location/c/I$a;->k:Lcom/google/android/location/d/a;

    iget-object v4, p0, Lcom/google/android/location/c/I$a;->p:Lcom/google/android/location/c/l;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/location/c/x;->a(Landroid/content/Context;Lcom/google/android/location/c/y;Lcom/google/android/location/d/a;Lcom/google/android/location/c/l;)Z
    :try_end_45
    .catchall {:try_start_23 .. :try_end_45} :catchall_a0
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_45} :catch_8d

    move-result v1

    .line 200
    if-eqz v1, :cond_68

    move-object v0, v7

    .line 219
    :goto_49
    if-nez v1, :cond_54

    .line 221
    :try_start_4b
    iget-object v2, p0, Lcom/google/android/location/c/I$a;->a:Lcom/google/android/location/c/l;

    if-eqz v2, :cond_54

    .line 222
    iget-object v2, p0, Lcom/google/android/location/c/I$a;->a:Lcom/google/android/location/c/l;

    invoke-interface {v2, v0}, Lcom/google/android/location/c/l;->a(Ljava/lang/String;)V

    .line 228
    :cond_54
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->o:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 229
    if-eqz v1, :cond_5e

    .line 230
    invoke-static {}, Landroid/os/Looper;->loop()V
    :try_end_5e
    .catchall {:try_start_4b .. :try_end_5e} :catchall_a0

    .line 234
    :cond_5e
    invoke-virtual {v9}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 235
    iput-object v7, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    .line 236
    iput-object v7, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    .line 237
    iput-object v7, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    .line 239
    return-void

    .line 203
    :cond_68
    :try_start_68
    const-string v0, "PreScanner: Nothing to scan."
    :try_end_6a
    .catchall {:try_start_68 .. :try_end_6a} :catchall_a0
    .catch Ljava/io/IOException; {:try_start_68 .. :try_end_6a} :catch_ab

    goto :goto_49

    .line 206
    :cond_6b
    :try_start_6b
    new-instance v1, Lcom/google/android/location/c/z;

    iget-object v2, p0, Lcom/google/android/location/c/I$a;->j:Lcom/google/android/location/k/a/c;

    invoke-direct {v1, v0, v2}, Lcom/google/android/location/c/z;-><init>(Landroid/os/Handler;Lcom/google/android/location/k/a/c;)V

    iput-object v1, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    .line 207
    iget-object v0, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    iget-object v1, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    iget-object v3, p0, Lcom/google/android/location/c/I$a;->k:Lcom/google/android/location/d/a;

    iget-object v4, p0, Lcom/google/android/location/c/I$a;->i:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/google/android/location/c/I$a;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v6, p0, Lcom/google/android/location/c/I$a;->a:Lcom/google/android/location/c/l;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/c/z;->a(Landroid/content/Context;Lcom/google/android/location/c/j;Lcom/google/android/location/d/a;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/l;)Z
    :try_end_85
    .catchall {:try_start_6b .. :try_end_85} :catchall_a0
    .catch Ljava/io/IOException; {:try_start_6b .. :try_end_85} :catch_8d

    move-result v1

    .line 209
    if-eqz v1, :cond_8a

    move-object v0, v7

    goto :goto_49

    .line 212
    :cond_8a
    :try_start_8a
    const-string v0, "RealCollector: Nothing to scan."
    :try_end_8c
    .catchall {:try_start_8a .. :try_end_8c} :catchall_a0
    .catch Ljava/io/IOException; {:try_start_8a .. :try_end_8c} :catch_ab

    goto :goto_49

    .line 215
    :catch_8d
    move-exception v0

    move v1, v8

    .line 216
    :goto_8f
    :try_start_8f
    const-string v2, "Failed normalize configuration: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_9e
    .catchall {:try_start_8f .. :try_end_9e} :catchall_a0

    move-result-object v0

    goto :goto_49

    .line 234
    :catchall_a0
    move-exception v0

    invoke-virtual {v9}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 235
    iput-object v7, p0, Lcom/google/android/location/c/I$a;->b:Landroid/content/Context;

    .line 236
    iput-object v7, p0, Lcom/google/android/location/c/I$a;->c:Lcom/google/android/location/c/j;

    .line 237
    iput-object v7, p0, Lcom/google/android/location/c/I$a;->h:Lcom/google/android/location/c/z;

    throw v0

    .line 215
    :catch_ab
    move-exception v0

    goto :goto_8f
.end method
