.class Lcom/google/android/location/c/s$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/c/s;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field final synthetic c:Lcom/google/android/location/c/s;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/s;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iput p2, p0, Lcom/google/android/location/c/s$2;->a:I

    iput-object p3, p0, Lcom/google/android/location/c/s$2;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    invoke-static {v0}, Lcom/google/android/location/c/s;->c(Lcom/google/android/location/c/s;)Landroid/os/PowerManager;

    move-result-object v0

    const/4 v1, 0x1

    const-class v2, Lcom/google/android/location/c/s;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    .line 179
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 180
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 182
    :try_start_18
    iget-object v0, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    invoke-static {v0}, Lcom/google/android/location/c/s;->d(Lcom/google/android/location/c/s;)Z

    move-result v0

    .line 183
    if-nez v0, :cond_36

    .line 184
    iget-object v0, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iget-object v0, v0, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_32

    .line 185
    iget-object v0, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iget-object v0, v0, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    iget v2, p0, Lcom/google/android/location/c/s$2;->a:I

    const/4 v3, 0x0

    const-string v4, "Failed to create lock file."

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/location/c/l;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_32
    .catchall {:try_start_18 .. :try_end_32} :catchall_6d

    .line 199
    :cond_32
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 201
    :goto_35
    return-void

    .line 189
    :cond_36
    :try_start_36
    iget-object v0, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iget-object v2, p0, Lcom/google/android/location/c/s$2;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v2}, Lcom/google/android/location/c/s;->a(Lcom/google/android/location/c/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/c/D$a;

    move-result-object v0

    .line 190
    iget-object v2, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iget-object v2, v2, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    if-eqz v2, :cond_57

    .line 191
    invoke-virtual {v0}, Lcom/google/android/location/c/D$a;->a()Z

    move-result v2

    if-eqz v2, :cond_5b

    .line 192
    iget-object v2, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iget-object v2, v2, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    iget v3, p0, Lcom/google/android/location/c/s$2;->a:I

    invoke-virtual {v0}, Lcom/google/android/location/c/D$a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Lcom/google/android/location/c/l;->a(ILjava/lang/String;)V
    :try_end_57
    .catchall {:try_start_36 .. :try_end_57} :catchall_6d

    .line 199
    :cond_57
    :goto_57
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_35

    .line 194
    :cond_5b
    :try_start_5b
    iget-object v2, p0, Lcom/google/android/location/c/s$2;->c:Lcom/google/android/location/c/s;

    iget-object v2, v2, Lcom/google/android/location/c/s;->a:Lcom/google/android/location/c/l;

    iget v3, p0, Lcom/google/android/location/c/s$2;->a:I

    invoke-virtual {v0}, Lcom/google/android/location/c/D$a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/location/c/D$a;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v4, v0}, Lcom/google/android/location/c/l;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_6c
    .catchall {:try_start_5b .. :try_end_6c} :catchall_6d

    goto :goto_57

    .line 199
    :catchall_6d
    move-exception v0

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
