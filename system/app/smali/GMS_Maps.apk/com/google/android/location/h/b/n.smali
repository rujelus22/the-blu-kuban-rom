.class public abstract Lcom/google/android/location/h/b/n;
.super Lcom/google/android/location/h/b/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/g;


# instance fields
.field protected a:Lcom/google/android/location/h/f;

.field private b:I


# direct methods
.method public constructor <init>(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/location/h/b/a;-><init>()V

    .line 26
    invoke-virtual {p0, p1}, Lcom/google/android/location/h/b/n;->a(I)V

    .line 27
    iput p2, p0, Lcom/google/android/location/h/b/n;->b:I

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/h/f;)V
    .registers 4
    .parameter

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/location/h/b/a;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/location/h/b/n;->a:Lcom/google/android/location/h/f;

    .line 41
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 44
    :try_start_a
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/location/h/b/n;->a(I)V

    .line 45
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/h/b/n;->b:I
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_17} :catch_18

    .line 49
    return-void

    .line 46
    :catch_18
    move-exception v0

    .line 47
    throw v0
.end method


# virtual methods
.method public a()V
    .registers 1

    .prologue
    .line 56
    return-void
.end method

.method public abstract b_()I
.end method

.method public abstract c_()Ljava/io/InputStream;
.end method

.method public g()I
    .registers 2

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/location/h/b/n;->b:I

    return v0
.end method

.method public h()V
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/location/h/b/n;->a:Lcom/google/android/location/h/f;

    if-eqz v0, :cond_9

    .line 86
    iget-object v0, p0, Lcom/google/android/location/h/b/n;->a:Lcom/google/android/location/h/f;

    invoke-virtual {v0}, Lcom/google/android/location/h/f;->b()V

    .line 88
    :cond_9
    return-void
.end method
