.class public final enum Lcom/google/android/location/b$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/location/b$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/location/b$a;

.field public static final enum b:Lcom/google/android/location/b$a;

.field private static final synthetic c:[Lcom/google/android/location/b$a;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/google/android/location/b$a;

    const-string v1, "USING_FULL_TIME_SPANS"

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/b$a;->a:Lcom/google/android/location/b$a;

    .line 46
    new-instance v0, Lcom/google/android/location/b$a;

    const-string v1, "USING_IN_OUTDOOR_HINTS"

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/b$a;->b:Lcom/google/android/location/b$a;

    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/location/b$a;

    sget-object v1, Lcom/google/android/location/b$a;->a:Lcom/google/android/location/b$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/location/b$a;->b:Lcom/google/android/location/b$a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/location/b$a;->c:[Lcom/google/android/location/b$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/b$a;
    .registers 2
    .parameter

    .prologue
    .line 36
    const-class v0, Lcom/google/android/location/b$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b$a;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/b$a;
    .registers 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/location/b$a;->c:[Lcom/google/android/location/b$a;

    invoke-virtual {v0}, [Lcom/google/android/location/b$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/b$a;

    return-object v0
.end method
