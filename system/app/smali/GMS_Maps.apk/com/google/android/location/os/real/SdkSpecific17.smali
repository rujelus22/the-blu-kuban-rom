.class public Lcom/google/android/location/os/real/SdkSpecific17;
.super Lcom/google/android/location/os/real/SdkSpecific11;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/location/os/real/SdkSpecific11;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/location/Location;)J
    .registers 6
    .parameter

    .prologue
    .line 84
    :try_start_0
    invoke-virtual {p1}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2
    :try_end_8
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_8} :catch_9

    .line 86
    :goto_8
    return-wide v0

    .line 85
    :catch_9
    move-exception v0

    .line 86
    invoke-super {p0, p1}, Lcom/google/android/location/os/real/SdkSpecific11;->a(Landroid/location/Location;)J

    move-result-wide v0

    goto :goto_8
.end method

.method public a(Landroid/net/wifi/ScanResult;)J
    .registers 6
    .parameter

    .prologue
    .line 74
    :try_start_0
    iget-wide v0, p1, Landroid/net/wifi/ScanResult;->timestamp:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_5} :catch_6

    .line 76
    :goto_5
    return-wide v0

    .line 75
    :catch_6
    move-exception v0

    .line 76
    const-wide/16 v0, 0x0

    goto :goto_5
.end method

.method public a(Landroid/telephony/TelephonyManager;J)Lcom/google/android/location/e/e;
    .registers 15
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const v10, 0x7fffffff

    .line 28
    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getAllCellInfo()Ljava/util/List;

    move-result-object v1

    .line 35
    if-eqz v1, :cond_11

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x1

    if-eq v0, v3, :cond_13

    :cond_11
    move-object v0, v2

    .line 65
    :goto_12
    return-object v0

    .line 39
    :cond_13
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CellInfo;

    .line 42
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/CellInfo;

    goto :goto_1e

    .line 46
    :cond_2b
    instance-of v1, v0, Landroid/telephony/CellInfoLte;

    if-eqz v1, :cond_62

    .line 47
    check-cast v0, Landroid/telephony/CellInfoLte;

    .line 48
    invoke-virtual {v0}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;

    move-result-object v1

    .line 49
    invoke-virtual {v0}, Landroid/telephony/CellInfoLte;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthLte;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthLte;->getDbm()I

    move-result v9

    .line 51
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthLte;->getTimingAdvance()I

    move-result v8

    .line 52
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getMcc()I

    move-result v3

    .line 53
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getMnc()I

    move-result v4

    .line 54
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getCi()I

    move-result v5

    .line 55
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getPci()I

    move-result v6

    .line 56
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getTac()I

    move-result v7

    .line 59
    if-eq v5, v10, :cond_62

    if-eq v6, v10, :cond_62

    if-eq v7, v10, :cond_62

    .line 60
    new-instance v0, Lcom/google/android/location/e/p;

    move-wide v1, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/e/p;-><init>(JIIIIIII)V

    goto :goto_12

    :cond_62
    move-object v0, v2

    goto :goto_12
.end method
