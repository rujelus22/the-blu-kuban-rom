.class public Lcom/google/android/location/b/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/e/v;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/b/i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "P:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/location/e/v",
        "<",
        "Lcom/google/android/location/b/i",
        "<TK;TP;>;>;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/android/location/b/i$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/i$a",
            "<TK;",
            "Lcom/google/android/location/b/a",
            "<TP;>;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/location/e/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/e/v",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/location/e/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/e/v",
            "<",
            "Lcom/google/android/location/b/a",
            "<TP;>;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/location/b/h;


# direct methods
.method public constructor <init>(ILcom/google/android/location/e/v;Lcom/google/android/location/e/v;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/location/e/v",
            "<TK;>;",
            "Lcom/google/android/location/e/v",
            "<",
            "Lcom/google/android/location/b/a",
            "<TP;>;>;)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p2, p0, Lcom/google/android/location/b/i;->b:Lcom/google/android/location/e/v;

    .line 50
    iput-object p3, p0, Lcom/google/android/location/b/i;->c:Lcom/google/android/location/e/v;

    .line 51
    new-instance v0, Lcom/google/android/location/b/h;

    invoke-direct {v0}, Lcom/google/android/location/b/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    .line 52
    new-instance v0, Lcom/google/android/location/b/i$a;

    iget-object v1, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/b/i$a;-><init>(ILcom/google/android/location/b/h;)V

    iput-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    .line 53
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lcom/google/android/location/b/a;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lcom/google/android/location/b/a",
            "<TP;>;"
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/i$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    .line 154
    iget-object v2, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    if-eqz v0, :cond_11

    const/4 v1, 0x1

    :goto_d
    invoke-virtual {v2, v1}, Lcom/google/android/location/b/h;->a(Z)V

    .line 155
    return-object v0

    .line 154
    :cond_11
    const/4 v1, 0x0

    goto :goto_d
.end method

.method public a(Ljava/lang/Object;J)Lcom/google/android/location/b/a;
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;J)",
            "Lcom/google/android/location/b/a",
            "<TP;>;"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/i$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    .line 137
    if-eqz v0, :cond_d

    .line 138
    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/b/a;->a(J)V

    .line 140
    :cond_d
    iget-object v2, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    if-eqz v0, :cond_16

    const/4 v1, 0x1

    :goto_12
    invoke-virtual {v2, v1}, Lcom/google/android/location/b/h;->a(Z)V

    .line 141
    return-object v0

    .line 140
    :cond_16
    const/4 v1, 0x0

    goto :goto_12
.end method

.method public a(Ljava/io/DataInput;)Lcom/google/android/location/b/i;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInput;",
            ")",
            "Lcom/google/android/location/b/i",
            "<TK;TP;>;"
        }
    .end annotation

    .prologue
    .line 205
    :try_start_0
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v2

    .line 206
    invoke-virtual {p0}, Lcom/google/android/location/b/i;->b()V

    .line 207
    const/4 v0, 0x0

    move v1, v0

    :goto_9
    if-ge v1, v2, :cond_27

    .line 208
    iget-object v0, p0, Lcom/google/android/location/b/i;->b:Lcom/google/android/location/e/v;

    invoke-interface {v0, p1}, Lcom/google/android/location/e/v;->b(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v3

    .line 209
    iget-object v0, p0, Lcom/google/android/location/b/i;->c:Lcom/google/android/location/e/v;

    invoke-interface {v0, p1}, Lcom/google/android/location/e/v;->b(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    .line 210
    iget-object v4, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v4, v3, v0}, Lcom/google/android/location/b/i$a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_1e} :catch_22

    .line 207
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    .line 214
    :catch_22
    move-exception v0

    .line 215
    invoke-virtual {p0}, Lcom/google/android/location/b/i;->b()V

    .line 216
    throw v0

    .line 213
    :cond_27
    return-object p0
.end method

.method a()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Lcom/google/android/location/b/a",
            "<TP;>;>;>;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/i$a;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(JJ)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/i$a;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 74
    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 75
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 76
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    .line 77
    invoke-virtual {v0}, Lcom/google/android/location/b/a;->b()J

    move-result-wide v2

    cmp-long v2, v2, p3

    if-gez v2, :cond_2d

    .line 78
    iget-object v0, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    invoke-virtual {v0}, Lcom/google/android/location/b/h;->b()V

    .line 80
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_a

    .line 81
    :cond_2d
    invoke-virtual {v0}, Lcom/google/android/location/b/a;->a()J

    move-result-wide v2

    cmp-long v0, v2, p1

    if-gez v0, :cond_a

    .line 82
    iget-object v0, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    invoke-virtual {v0}, Lcom/google/android/location/b/h;->b()V

    .line 86
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_a

    .line 89
    :cond_3e
    return-void
.end method

.method public a(Lcom/google/android/location/b/i;Ljava/io/DataOutput;)V
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/i",
            "<TK;TP;>;",
            "Ljava/io/DataOutput;",
            ")V"
        }
    .end annotation

    .prologue
    .line 222
    iget-object v0, p1, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/i$a;->size()I

    move-result v0

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 223
    iget-object v0, p1, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/i$a;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 224
    iget-object v2, p0, Lcom/google/android/location/b/i;->b:Lcom/google/android/location/e/v;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3, p2}, Lcom/google/android/location/e/v;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    .line 225
    iget-object v2, p0, Lcom/google/android/location/b/i;->c:Lcom/google/android/location/e/v;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0, p2}, Lcom/google/android/location/e/v;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    goto :goto_13

    .line 227
    :cond_32
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 28
    check-cast p1, Lcom/google/android/location/b/i;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/b/i;->a(Lcom/google/android/location/b/i;Ljava/io/DataOutput;)V

    return-void
.end method

.method public a(ZLjava/lang/Object;ILjava/lang/Object;J)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTK;ITP;J)V"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0, p2}, Lcom/google/android/location/b/i$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/a;

    .line 114
    if-nez v0, :cond_1c

    .line 115
    if-eqz p1, :cond_1b

    .line 116
    new-instance v0, Lcom/google/android/location/b/a;

    invoke-direct {v0, p3, p4, p5, p6}, Lcom/google/android/location/b/a;-><init>(ILjava/lang/Object;J)V

    .line 117
    iget-object v1, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v1, p2, v0}, Lcom/google/android/location/b/i$a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    iget-object v0, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    invoke-virtual {v0}, Lcom/google/android/location/b/h;->c()V

    .line 123
    :cond_1b
    :goto_1b
    return-void

    .line 121
    :cond_1c
    invoke-virtual {v0, p3, p4, p5, p6}, Lcom/google/android/location/b/a;->a(ILjava/lang/Object;J)V

    goto :goto_1b
.end method

.method public synthetic b(Ljava/io/DataInput;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/google/android/location/b/i;->a(Ljava/io/DataInput;)Lcom/google/android/location/b/i;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/i$a;->clear()V

    .line 96
    return-void
.end method

.method public c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/location/b/i;->d:Lcom/google/android/location/b/h;

    iget-object v1, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    iget v1, v1, Lcom/google/android/location/b/i$a;->a:I

    iget-object v2, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v2}, Lcom/google/android/location/b/i$a;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/b/h;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/location/b/i;->a:Lcom/google/android/location/b/i$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/i$a;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
