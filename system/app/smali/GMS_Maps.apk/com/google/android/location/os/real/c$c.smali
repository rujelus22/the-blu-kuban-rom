.class final Lcom/google/android/location/os/real/c$c;
.super Landroid/os/Handler;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/os/real/c;

.field private final b:Landroid/telephony/TelephonyManager;

.field private final c:Landroid/net/ConnectivityManager;

.field private final d:Landroid/net/wifi/WifiManager;

.field private e:I

.field private f:Lcom/google/android/location/e/e;


# direct methods
.method private constructor <init>(Lcom/google/android/location/os/real/c;)V
    .registers 4
    .parameter

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 139
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/c$c;->b:Landroid/telephony/TelephonyManager;

    .line 141
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/c$c;->c:Landroid/net/ConnectivityManager;

    .line 143
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/c$c;->d:Landroid/net/wifi/WifiManager;

    .line 145
    const/16 v0, -0x270f

    iput v0, p0, Lcom/google/android/location/os/real/c$c;->e:I

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/os/real/c;Lcom/google/android/location/os/real/c$1;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/c$c;-><init>(Lcom/google/android/location/os/real/c;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 150
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_4d4

    .line 421
    :cond_a
    :goto_a
    return-void

    .line 152
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v3, Lcom/google/android/location/os/d;->b:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v3}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v3

    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_5f

    move v0, v1

    :goto_21
    invoke-interface {v3, v0}, Lcom/google/android/location/os/a;->a(Z)V

    .line 154
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->b:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->d(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$e;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 155
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 157
    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->e(Lcom/google/android/location/os/real/c;)Z

    move-result v1

    if-eqz v1, :cond_61

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->f(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$d;

    move-result-object v1

    :goto_4b
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 158
    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->h(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    .line 159
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto :goto_a

    :cond_5f
    move v0, v2

    .line 153
    goto :goto_21

    .line 157
    :cond_61
    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->g(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$d;

    move-result-object v1

    goto :goto_4b

    .line 163
    :pswitch_68
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/d;->l:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_a

    .line 168
    :pswitch_81
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/d;->m:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 169
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_a

    .line 173
    :pswitch_9b
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v0, p1, Landroid/os/Message;->arg2:I

    if-eqz v0, :cond_bc

    move v0, v1

    :goto_a8
    invoke-virtual {v3, v4, v0}, Lcom/google/android/location/os/e;->a(IZ)V

    .line 174
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    if-eqz v4, :cond_be

    :goto_b7
    invoke-interface {v0, v3, v1}, Lcom/google/android/location/os/a;->a(IZ)V

    goto/16 :goto_a

    :cond_bc
    move v0, v2

    .line 173
    goto :goto_a8

    :cond_be
    move v1, v2

    .line 174
    goto :goto_b7

    .line 178
    :pswitch_c0
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v1, :cond_c8

    .line 179
    const/16 v0, -0x270f

    iput v0, p0, Lcom/google/android/location/os/real/c$c;->e:I

    .line 182
    :cond_c8
    const/4 v0, 0x0

    .line 183
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 185
    iget-object v3, p0, Lcom/google/android/location/os/real/c$c;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v3

    .line 186
    if-eqz v3, :cond_117

    .line 187
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->b:Landroid/telephony/TelephonyManager;

    iget v4, p0, Lcom/google/android/location/os/real/c$c;->e:I

    invoke-static {v0, v3, v4, v1, v2}, Lcom/google/android/location/os/real/f;->a(Landroid/telephony/TelephonyManager;Landroid/telephony/CellLocation;IJ)Lcom/google/android/location/e/e;

    move-result-object v0

    .line 190
    iput-object v0, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    .line 196
    :goto_df
    invoke-static {}, Lcom/google/android/location/os/real/j;->a()Lcom/google/android/location/os/real/j;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/os/real/c$c;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3, v4, v1, v2}, Lcom/google/android/location/os/real/j;->a(Landroid/telephony/TelephonyManager;J)Lcom/google/android/location/e/e;

    move-result-object v1

    .line 199
    if-eqz v1, :cond_103

    invoke-virtual {v1, v0}, Lcom/google/android/location/e/e;->b(Lcom/google/android/location/e/e;)Z

    move-result v2

    if-nez v2, :cond_103

    .line 208
    iget-object v2, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v2}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/e/e;)V

    .line 209
    iget-object v2, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v2}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/e/e;)V

    .line 216
    :cond_103
    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/e/e;)V

    .line 217
    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/e/e;)V

    goto/16 :goto_a

    .line 192
    :cond_117
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    goto :goto_df

    .line 221
    :pswitch_11b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/telephony/SignalStrength;

    .line 224
    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    instance-of v1, v1, Lcom/google/android/location/e/b;

    if-eqz v1, :cond_a

    .line 225
    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v1

    if-eqz v1, :cond_15b

    .line 226
    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/os/real/c$c;->e:I

    .line 230
    :goto_135
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/os/real/c$c;->e:I

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->b(I)V

    .line 231
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget v3, p0, Lcom/google/android/location/os/real/c$c;->e:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/e/e;->a(JI)Lcom/google/android/location/e/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    .line 233
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->f:Lcom/google/android/location/e/e;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/e/e;)V

    goto/16 :goto_a

    .line 228
    :cond_15b
    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/os/real/c$c;->e:I

    goto :goto_135

    .line 238
    :pswitch_162
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/e;->a(I)V

    .line 239
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/location/os/a;->a(I)V

    .line 240
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 244
    :pswitch_17f
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    .line 245
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    .line 246
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 250
    :pswitch_19c
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/location/os/e;->a(I)V

    .line 251
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/location/os/a;->a(I)V

    .line 252
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 256
    :pswitch_1b9
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/location/os/e;->a(I)V

    .line 257
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/google/android/location/os/a;->a(I)V

    .line 258
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 262
    :pswitch_1d6
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/location/os/e;->a(I)V

    .line 263
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/google/android/location/os/a;->a(I)V

    .line 264
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 268
    :pswitch_1f3
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    .line 269
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    .line 270
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 274
    :pswitch_212
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    .line 275
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    .line 276
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 280
    :pswitch_231
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    .line 281
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    .line 282
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 286
    :pswitch_250
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    .line 287
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    .line 288
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 292
    :pswitch_271
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    .line 293
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    .line 294
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 298
    :pswitch_292
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(I)V

    .line 299
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->a(I)V

    .line 300
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 304
    :pswitch_2b3
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->e(Lcom/google/android/location/os/real/c;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 306
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/u;

    .line 307
    new-instance v2, Lcom/google/android/location/os/real/g;

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Landroid/location/Location;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->h(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/os/real/c$b;->a()I

    move-result v0

    invoke-direct {v2, v1, v3, v4, v0}, Lcom/google/android/location/os/real/g;-><init>(Landroid/location/Location;JI)V

    .line 309
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/g;)V

    .line 310
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/os/g;)V

    goto/16 :goto_a

    .line 315
    :pswitch_2ee
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->e(Lcom/google/android/location/os/real/c;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 317
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/u;

    .line 318
    new-instance v2, Lcom/google/android/location/os/real/g;

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Landroid/location/Location;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->h(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/real/c$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/os/real/c$b;->a()I

    move-result v0

    invoke-direct {v2, v1, v3, v4, v0}, Lcom/google/android/location/os/real/g;-><init>(Landroid/location/Location;JI)V

    .line 320
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/g;)V

    .line 321
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/os/g;)V

    goto/16 :goto_a

    .line 326
    :pswitch_329
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/os/real/i;

    .line 327
    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/e/E;)V

    .line 328
    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/e/E;)V

    goto/16 :goto_a

    .line 332
    :pswitch_341
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_359

    .line 333
    :goto_345
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->d(Z)V

    .line 334
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->b(Z)V

    goto/16 :goto_a

    :cond_359
    move v1, v2

    .line 332
    goto :goto_345

    .line 338
    :pswitch_35b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    .line 339
    const-string v3, "scale"

    const/16 v4, 0x64

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 340
    const-string v4, "level"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 341
    const-string v5, "plugged"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_389

    .line 342
    :goto_375
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v3, v4, v1}, Lcom/google/android/location/os/e;->a(IIZ)V

    .line 343
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v3, v4, v1}, Lcom/google/android/location/os/a;->a(IIZ)V

    goto/16 :goto_a

    :cond_389
    move v1, v2

    .line 341
    goto :goto_375

    .line 347
    :pswitch_38b
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_3a3

    .line 348
    :goto_38f
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->c(Z)V

    .line 349
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->c(Z)V

    goto/16 :goto_a

    :cond_3a3
    move v1, v2

    .line 347
    goto :goto_38f

    .line 353
    :pswitch_3a5
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_3bd

    .line 354
    :goto_3a9
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Z)V

    .line 355
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->d(Z)V

    goto/16 :goto_a

    :cond_3bd
    move v1, v2

    .line 353
    goto :goto_3a9

    .line 359
    :pswitch_3bf
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_3d7

    .line 360
    :goto_3c3
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->b(Z)V

    .line 361
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/a;->e(Z)V

    goto/16 :goto_a

    :cond_3d7
    move v1, v2

    .line 359
    goto :goto_3c3

    .line 365
    :pswitch_3d9
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/d;->k:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 366
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_a

    .line 370
    :pswitch_3f3
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/d;->j:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 371
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_a

    .line 376
    :pswitch_40d
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->c:Landroid/net/ConnectivityManager;

    iget-object v2, p0, Lcom/google/android/location/os/real/c$c;->d:Landroid/net/wifi/WifiManager;

    iget-object v3, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v3}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/location/os/real/c;->a(Lcom/google/android/location/os/real/c;Landroid/net/ConnectivityManager;Landroid/net/wifi/WifiManager;Lcom/google/android/location/os/a;)V

    goto/16 :goto_a

    .line 380
    :pswitch_41e
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/d;->p:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 381
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/os/h;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/os/h;)V

    goto/16 :goto_a

    .line 385
    :pswitch_438
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->b(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/os/d;->O:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/d;)V

    .line 386
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/t;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/e/t;)V

    goto/16 :goto_a

    .line 390
    :pswitch_452
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/a/n$b;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/a/n$b;)V

    goto/16 :goto_a

    .line 394
    :pswitch_461
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/clientlib/NlpActivity;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    .line 395
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 399
    :pswitch_479
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/location/os/a;->a(II)V

    .line 400
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 405
    :pswitch_491
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/u;

    .line 406
    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v2

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Lcom/google/android/location/os/a;->a(ZLjava/lang/String;)V

    .line 407
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 412
    :pswitch_4b5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/e/u;

    .line 413
    iget-object v1, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v1}, Lcom/google/android/location/os/real/c;->c(Lcom/google/android/location/os/real/c;)Lcom/google/android/location/os/a;

    move-result-object v2

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/os/i$b;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    invoke-interface {v2, v1, v0}, Lcom/google/android/location/os/a;->a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V

    .line 414
    iget-object v0, p0, Lcom/google/android/location/os/real/c$c;->a:Lcom/google/android/location/os/real/c;

    invoke-static {v0}, Lcom/google/android/location/os/real/c;->i(Lcom/google/android/location/os/real/c;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_a

    .line 150
    nop

    :pswitch_data_4d4
    .packed-switch 0x1
        :pswitch_b
        :pswitch_68
        :pswitch_9b
        :pswitch_c0
        :pswitch_11b
        :pswitch_162
        :pswitch_17f
        :pswitch_19c
        :pswitch_1b9
        :pswitch_1d6
        :pswitch_1f3
        :pswitch_212
        :pswitch_231
        :pswitch_250
        :pswitch_271
        :pswitch_292
        :pswitch_2b3
        :pswitch_329
        :pswitch_341
        :pswitch_35b
        :pswitch_2ee
        :pswitch_38b
        :pswitch_81
        :pswitch_3a5
        :pswitch_3bf
        :pswitch_3d9
        :pswitch_40d
        :pswitch_3f3
        :pswitch_41e
        :pswitch_438
        :pswitch_461
        :pswitch_479
        :pswitch_452
        :pswitch_491
        :pswitch_4b5
    .end packed-switch
.end method
