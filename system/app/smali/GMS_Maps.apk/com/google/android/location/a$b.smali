.class Lcom/google/android/location/a$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field private a:Lcom/google/android/location/os/g;

.field private b:J

.field private c:J

.field private final d:Lcom/google/android/location/b/g;

.field private e:Lcom/google/android/location/a$a;


# direct methods
.method public constructor <init>(Lcom/google/android/location/b/g;)V
    .registers 6
    .parameter

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373
    iput-object v1, p0, Lcom/google/android/location/a$b;->a:Lcom/google/android/location/os/g;

    .line 376
    iput-wide v2, p0, Lcom/google/android/location/a$b;->b:J

    .line 379
    iput-wide v2, p0, Lcom/google/android/location/a$b;->c:J

    .line 385
    new-instance v0, Lcom/google/android/location/a$a;

    invoke-direct {v0, v1}, Lcom/google/android/location/a$a;-><init>(Lcom/google/android/location/a$1;)V

    iput-object v0, p0, Lcom/google/android/location/a$b;->e:Lcom/google/android/location/a$a;

    .line 388
    iput-object p1, p0, Lcom/google/android/location/a$b;->d:Lcom/google/android/location/b/g;

    .line 389
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/location/b/g;
    .registers 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/location/a$b;->d:Lcom/google/android/location/b/g;

    return-object v0
.end method

.method a(Lcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 396
    if-eqz p1, :cond_4

    .line 397
    iput-object p1, p0, Lcom/google/android/location/a$b;->a:Lcom/google/android/location/os/g;

    .line 399
    :cond_4
    if-eqz p2, :cond_13

    .line 400
    invoke-virtual {p2}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/a$b;->c:J

    .line 401
    if-eqz p1, :cond_13

    .line 402
    iget-object v0, p0, Lcom/google/android/location/a$b;->e:Lcom/google/android/location/a$a;

    invoke-static {v0, p2, p1}, Lcom/google/android/location/a$a;->a(Lcom/google/android/location/a$a;Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)V

    .line 405
    :cond_13
    if-eqz p3, :cond_22

    .line 406
    iget-wide v0, p3, Lcom/google/android/location/e/E;->a:J

    iput-wide v0, p0, Lcom/google/android/location/a$b;->b:J

    .line 407
    iget-object v0, p0, Lcom/google/android/location/a$b;->d:Lcom/google/android/location/b/g;

    if-eqz v0, :cond_22

    .line 408
    iget-object v0, p0, Lcom/google/android/location/a$b;->d:Lcom/google/android/location/b/g;

    invoke-virtual {v0, p3}, Lcom/google/android/location/b/g;->c(Lcom/google/android/location/e/E;)V

    .line 411
    :cond_22
    return-void
.end method

.method a(Lcom/google/android/location/e/E;JJ)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 442
    invoke-virtual/range {p0 .. p5}, Lcom/google/android/location/a$b;->b(Lcom/google/android/location/e/E;JJ)I

    move-result v0

    if-lez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method a(Lcom/google/android/location/e/E;JJD)Z
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 458
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v1

    if-nez v1, :cond_8

    .line 462
    :cond_7
    :goto_7
    return v0

    .line 461
    :cond_8
    invoke-virtual/range {p0 .. p5}, Lcom/google/android/location/a$b;->b(Lcom/google/android/location/e/E;JJ)I

    move-result v1

    .line 462
    int-to-double v1, v1

    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v1, v3

    cmpl-double v1, v1, p6

    if-lez v1, :cond_7

    const/4 v0, 0x1

    goto :goto_7
.end method

.method a(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/location/a$b;->e:Lcom/google/android/location/a$a;

    invoke-static {v0, p1, p2}, Lcom/google/android/location/a$a;->b(Lcom/google/android/location/a$a;Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z

    move-result v0

    return v0
.end method

.method b(Lcom/google/android/location/e/E;JJ)I
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 471
    move v1, v0

    .line 472
    :goto_2
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v2

    if-ge v0, v2, :cond_25

    .line 473
    invoke-virtual {p1, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    .line 474
    iget-object v3, p0, Lcom/google/android/location/a$b;->d:Lcom/google/android/location/b/g;

    invoke-virtual {v3, v2}, Lcom/google/android/location/b/g;->c(Ljava/lang/Long;)J

    move-result-wide v2

    .line 476
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_20

    sub-long v2, p2, v2

    cmp-long v2, v2, p4

    if-lez v2, :cond_22

    .line 478
    :cond_20
    add-int/lit8 v1, v1, 0x1

    .line 472
    :cond_22
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 481
    :cond_25
    return v1
.end method

.method b()Lcom/google/android/location/os/g;
    .registers 2

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/location/a$b;->a:Lcom/google/android/location/os/g;

    return-object v0
.end method

.method c()J
    .registers 3

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/location/a$b;->a:Lcom/google/android/location/os/g;

    if-nez v0, :cond_7

    const-wide/16 v0, -0x1

    :goto_6
    return-wide v0

    :cond_7
    iget-object v0, p0, Lcom/google/android/location/a$b;->a:Lcom/google/android/location/os/g;

    invoke-interface {v0}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v0

    goto :goto_6
.end method

.method d()J
    .registers 3

    .prologue
    .line 426
    iget-wide v0, p0, Lcom/google/android/location/a$b;->c:J

    return-wide v0
.end method

.method e()J
    .registers 3

    .prologue
    .line 430
    iget-wide v0, p0, Lcom/google/android/location/a$b;->b:J

    return-wide v0
.end method
