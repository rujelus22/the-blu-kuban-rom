.class Lcom/google/android/location/c/G;
.super Lcom/google/android/location/c/E;
.source "SourceFile"


# instance fields
.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/hardware/SensorManager;

.field private final e:Landroid/hardware/SensorEventListener;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/Map;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/location/c/k;",
            "Lcom/google/android/location/c/l;",
            "Lcom/google/android/location/k/a/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p3, p4, p5}, Lcom/google/android/location/c/E;-><init>(Landroid/content/Context;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    .line 30
    new-instance v0, Lcom/google/android/location/c/G$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/G$1;-><init>(Lcom/google/android/location/c/G;)V

    iput-object v0, p0, Lcom/google/android/location/c/G;->e:Landroid/hardware/SensorEventListener;

    .line 76
    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    invoke-static {p2}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/google/android/location/c/G;->c:Ljava/util/Map;

    .line 79
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/location/c/G;->d:Landroid/hardware/SensorManager;

    .line 80
    return-void
.end method

.method private a(Ljava/util/Map$Entry;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v1, p0, Lcom/google/android/location/c/G;->d:Landroid/hardware/SensorManager;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_34

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_34

    .line 101
    iget-object v2, p0, Lcom/google/android/location/c/G;->d:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/google/android/location/c/G;->e:Landroid/hardware/SensorEventListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Sensor;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/location/c/G;->f()Lcom/google/android/location/c/k;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 104
    :cond_34
    return-void
.end method


# virtual methods
.method protected a()V
    .registers 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/location/c/G;->d:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1e

    .line 85
    iget-object v0, p0, Lcom/google/android/location/c/G;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 86
    invoke-direct {p0, v0}, Lcom/google/android/location/c/G;->a(Ljava/util/Map$Entry;)V

    goto :goto_e

    .line 89
    :cond_1e
    iget-object v0, p0, Lcom/google/android/location/c/G;->b:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_27

    .line 90
    iget-object v0, p0, Lcom/google/android/location/c/G;->b:Lcom/google/android/location/c/l;

    invoke-interface {v0}, Lcom/google/android/location/c/l;->e()V

    .line 92
    :cond_27
    return-void
.end method

.method protected b()V
    .registers 3

    .prologue
    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/G;->d:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/location/c/G;->e:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_7} :catch_11

    .line 114
    :goto_7
    iget-object v0, p0, Lcom/google/android/location/c/G;->b:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_10

    .line 115
    iget-object v0, p0, Lcom/google/android/location/c/G;->b:Lcom/google/android/location/c/l;

    invoke-interface {v0}, Lcom/google/android/location/c/l;->d()V

    .line 117
    :cond_10
    return-void

    .line 110
    :catch_11
    move-exception v0

    goto :goto_7
.end method
