.class Lcom/google/android/location/c/p;
.super Lcom/google/android/location/c/E;
.source "SourceFile"

# interfaces
.implements Landroid/location/GpsStatus$Listener;
.implements Landroid/location/LocationListener;


# instance fields
.field private final c:Lcom/google/android/location/d/a;

.field private final d:Ljava/lang/String;

.field private e:Z

.field private final f:Z

.field private final g:Z

.field private h:Landroid/location/GpsStatus;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZZLcom/google/android/location/d/a;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0, p1, p5, p6, p7}, Lcom/google/android/location/c/E;-><init>(Landroid/content/Context;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    .line 36
    iput-boolean v1, p0, Lcom/google/android/location/c/p;->e:Z

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/p;->h:Landroid/location/GpsStatus;

    .line 55
    iput-boolean p2, p0, Lcom/google/android/location/c/p;->f:Z

    .line 56
    iput-boolean p3, p0, Lcom/google/android/location/c/p;->g:Z

    .line 57
    if-nez p4, :cond_1f

    .line 58
    new-instance v0, Lcom/google/android/location/d/a;

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/d/a;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    .line 62
    :goto_16
    iget-object v0, p0, Lcom/google/android/location/c/p;->a:Lcom/google/android/location/k/a/c;

    invoke-virtual {v0}, Lcom/google/android/location/k/a/c;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/p;->d:Ljava/lang/String;

    .line 63
    return-void

    .line 60
    :cond_1f
    iput-object p4, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    goto :goto_16
.end method


# virtual methods
.method protected a()V
    .registers 9

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/location/c/p;->g:Z

    if-eqz v0, :cond_b

    .line 68
    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    iget-object v1, p0, Lcom/google/android/location/c/p;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;Landroid/location/GpsStatus$Listener;)Z

    .line 70
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    if-eqz v0, :cond_24

    .line 71
    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    iget-object v1, p0, Lcom/google/android/location/c/p;->d:Ljava/lang/String;

    const-string v2, "gps"

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/location/c/p;->f()Lcom/google/android/location/c/k;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/location/c/k;->getLooper()Landroid/os/Looper;

    move-result-object v7

    move-object v6, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 74
    :cond_24
    iget-object v0, p0, Lcom/google/android/location/c/p;->b:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_2d

    .line 75
    iget-object v0, p0, Lcom/google/android/location/c/p;->b:Lcom/google/android/location/c/l;

    invoke-interface {v0}, Lcom/google/android/location/c/l;->g()V

    .line 77
    :cond_2d
    return-void
.end method

.method protected b()V
    .registers 4

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/location/c/p;->g:Z

    if-eqz v0, :cond_b

    .line 82
    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    iget-object v1, p0, Lcom/google/android/location/c/p;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/location/d/a;->b(Ljava/lang/String;Landroid/location/GpsStatus$Listener;)V

    .line 84
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    if-eqz v0, :cond_17

    .line 85
    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    iget-object v1, p0, Lcom/google/android/location/c/p;->d:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/location/d/a;->a(Ljava/lang/String;ZLandroid/location/LocationListener;)V

    .line 87
    :cond_17
    iget-object v0, p0, Lcom/google/android/location/c/p;->b:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_20

    .line 88
    iget-object v0, p0, Lcom/google/android/location/c/p;->b:Lcom/google/android/location/c/l;

    invoke-interface {v0}, Lcom/google/android/location/c/l;->h()V

    .line 90
    :cond_20
    return-void
.end method

.method public onGpsStatusChanged(I)V
    .registers 6
    .parameter

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/google/android/location/c/p;->g:Z

    if-nez v0, :cond_5

    .line 143
    :cond_4
    :goto_4
    return-void

    .line 132
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/location/c/p;->h()V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/location/c/p;->g()Z

    move-result v0

    if-nez v0, :cond_4

    .line 136
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    .line 139
    iget-object v0, p0, Lcom/google/android/location/c/p;->c:Lcom/google/android/location/d/a;

    iget-object v1, p0, Lcom/google/android/location/c/p;->h:Landroid/location/GpsStatus;

    invoke-virtual {v0, v1}, Lcom/google/android/location/d/a;->a(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/p;->h:Landroid/location/GpsStatus;

    .line 141
    invoke-virtual {p0}, Lcom/google/android/location/c/p;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/p;->h:Landroid/location/GpsStatus;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/c/k;->a(Landroid/location/GpsStatus;J)V

    goto :goto_4
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .registers 8
    .parameter

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/location/c/p;->f:Z

    if-nez v0, :cond_5

    .line 111
    :cond_4
    :goto_4
    return-void

    .line 97
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/location/c/p;->h()V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/location/c/p;->g()Z

    move-result v0

    if-nez v0, :cond_4

    .line 101
    invoke-virtual {p0}, Lcom/google/android/location/c/p;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/c/k;->a(Landroid/location/Location;J)V

    .line 103
    iget-boolean v0, p0, Lcom/google/android/location/c/p;->e:Z

    if-nez v0, :cond_4

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/p;->e:Z

    .line 105
    new-instance v0, Landroid/hardware/GeomagneticField;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v3

    double-to-float v3, v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Landroid/hardware/GeomagneticField;-><init>(FFFJ)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/location/c/p;->f()Lcom/google/android/location/c/k;

    move-result-object v1

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getX()F

    move-result v2

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getY()F

    move-result v3

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getZ()F

    move-result v4

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getDeclination()F

    move-result v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/location/c/k;->a(FFFF)V

    goto :goto_4
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 116
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 121
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 125
    return-void
.end method
