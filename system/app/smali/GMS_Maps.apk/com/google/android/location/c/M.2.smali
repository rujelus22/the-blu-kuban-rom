.class Lcom/google/android/location/c/M;
.super Lcom/google/android/location/c/E;
.source "SourceFile"


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Landroid/net/wifi/WifiManager;

.field private final e:Lcom/google/android/location/d/e;

.field private f:Landroid/net/wifi/WifiManager$WifiLock;

.field private final g:Landroid/content/BroadcastReceiver;

.field private volatile h:I

.field private i:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/c/k;Lcom/google/android/location/d/e;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/google/android/location/c/E;-><init>(Landroid/content/Context;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V

    .line 32
    new-instance v0, Lcom/google/android/location/c/M$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/M$1;-><init>(Lcom/google/android/location/c/M;)V

    iput-object v0, p0, Lcom/google/android/location/c/M;->g:Landroid/content/BroadcastReceiver;

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/c/M;->h:I

    .line 77
    new-instance v0, Lcom/google/android/location/c/M$2;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/M$2;-><init>(Lcom/google/android/location/c/M;)V

    iput-object v0, p0, Lcom/google/android/location/c/M;->i:Ljava/lang/Runnable;

    .line 60
    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    iput-object p1, p0, Lcom/google/android/location/c/M;->c:Landroid/content/Context;

    .line 62
    iput-object p3, p0, Lcom/google/android/location/c/M;->e:Lcom/google/android/location/d/e;

    .line 63
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/location/c/M;->d:Landroid/net/wifi/WifiManager;

    .line 64
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/M;)V
    .registers 1
    .parameter

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/location/c/M;->c()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/c/M;)Landroid/net/wifi/WifiManager;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/location/c/M;->d:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/c/M;)Lcom/google/android/location/d/e;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/location/c/M;->e:Lcom/google/android/location/d/e;

    return-object v0
.end method

.method private c()V
    .registers 5

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/google/android/location/c/M;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/M;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/k;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 91
    invoke-direct {p0}, Lcom/google/android/location/c/M;->i()V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/location/c/M;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/M;->i:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/c/k;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 97
    return-void
.end method

.method private i()V
    .registers 3

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/location/c/M;->d:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    .line 101
    iget-object v0, p0, Lcom/google/android/location/c/M;->b:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_10

    .line 102
    iget-object v0, p0, Lcom/google/android/location/c/M;->b:Lcom/google/android/location/c/l;

    iget v1, p0, Lcom/google/android/location/c/M;->h:I

    invoke-interface {v0, v1}, Lcom/google/android/location/c/l;->a_(I)V

    .line 104
    :cond_10
    iget v0, p0, Lcom/google/android/location/c/M;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/c/M;->h:I

    .line 105
    return-void
.end method


# virtual methods
.method protected a()V
    .registers 5

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/location/c/M;->d:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x2

    const-string v2, "WifiScanner"

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/M;->f:Landroid/net/wifi/WifiManager$WifiLock;

    .line 69
    iget-object v0, p0, Lcom/google/android/location/c/M;->f:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 70
    iget-object v0, p0, Lcom/google/android/location/c/M;->f:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 72
    iget-object v0, p0, Lcom/google/android/location/c/M;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/c/M;->g:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.wifi.SCAN_RESULTS"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 74
    invoke-direct {p0}, Lcom/google/android/location/c/M;->c()V

    .line 75
    return-void
.end method

.method protected b()V
    .registers 3

    .prologue
    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/c/M;->f:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/location/c/M;->f:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 111
    iget-object v0, p0, Lcom/google/android/location/c/M;->f:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 113
    :cond_11
    iget-object v0, p0, Lcom/google/android/location/c/M;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/c/M;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_18} :catch_22

    .line 118
    :goto_18
    iget-object v0, p0, Lcom/google/android/location/c/M;->b:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_21

    .line 119
    iget-object v0, p0, Lcom/google/android/location/c/M;->b:Lcom/google/android/location/c/l;

    invoke-interface {v0}, Lcom/google/android/location/c/l;->f()V

    .line 121
    :cond_21
    return-void

    .line 114
    :catch_22
    move-exception v0

    goto :goto_18
.end method
