.class Lcom/google/android/location/g/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/g/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/g/o$1;,
        Lcom/google/android/location/g/o$b;,
        Lcom/google/android/location/g/o$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/logging/Logger;

.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 57
    const-class v0, Lcom/google/android/location/g/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/g/o;->a:Ljava/util/logging/Logger;

    .line 77
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/g/o;->b:Ljava/util/Set;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 592
    return-void
.end method

.method private a(IILcom/google/android/location/e/w;Lcom/google/android/location/e/w;ILcom/google/android/location/e/u;)D
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/google/android/location/e/w;",
            "Lcom/google/android/location/e/w;",
            "I",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;)D"
        }
    .end annotation

    .prologue
    .line 492
    mul-int v9, p5, p5

    .line 493
    mul-int v10, p1, p1

    .line 494
    mul-int v11, p2, p2

    .line 496
    invoke-static {p3}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;)D

    move-result-wide v1

    .line 497
    invoke-static {p3}, Lcom/google/android/location/g/c;->b(Lcom/google/android/location/e/w;)D

    move-result-wide v3

    .line 499
    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    move-object/from16 v0, p6

    iget-object v7, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-static/range {v1 .. v8}, Lcom/google/android/location/g/c;->b(DDDD)D

    move-result-wide v1

    double-to-int v1, v1

    .line 501
    mul-int v2, v1, v1

    .line 503
    add-int v3, v9, v10

    sub-int/2addr v3, v11

    int-to-double v3, v3

    mul-int/lit8 v5, p1, 0x2

    mul-int v5, v5, p5

    int-to-double v5, v5

    div-double/2addr v3, v5

    .line 505
    add-int/2addr v2, v10

    int-to-double v5, v2

    mul-int/lit8 v2, p1, 0x2

    mul-int/2addr v1, v2

    int-to-double v1, v1

    mul-double/2addr v1, v3

    sub-double v1, v5, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    .line 508
    return-wide v1
.end method

.method private a(Lcom/google/android/location/e/u;Lcom/google/android/location/e/u;)Lcom/google/android/location/e/u;
    .registers 19
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 521
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 522
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 523
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 524
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    .line 525
    sub-double/2addr v8, v6

    .line 527
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    .line 528
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v8, v12

    .line 529
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    add-double/2addr v4, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    add-double/2addr v12, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    add-double/2addr v14, v10

    mul-double/2addr v12, v14

    mul-double v14, v8, v8

    add-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    invoke-static {v4, v5, v12, v13}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    .line 532
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    add-double/2addr v1, v10

    invoke-static {v8, v9, v1, v2}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v1

    add-double/2addr v1, v6

    .line 535
    new-instance v3, Lcom/google/android/location/e/u;

    invoke-static {v4, v5}, Lcom/google/android/location/g/c;->c(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-static {v1, v2}, Lcom/google/android/location/g/c;->d(D)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-direct {v3, v4, v1}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3
.end method

.method private a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/w;
    .registers 15
    .parameter
    .parameter

    .prologue
    .line 416
    invoke-static {p1, p2}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v6

    .line 417
    iget v0, p1, Lcom/google/android/location/e/w;->c:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->b(I)I

    move-result v7

    .line 418
    iget v0, p2, Lcom/google/android/location/e/w;->c:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->b(I)I

    move-result v8

    .line 420
    sub-int v0, v7, v8

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gt v6, v0, :cond_1d

    .line 422
    if-ge v7, v8, :cond_1b

    .line 469
    :goto_1a
    return-object p1

    :cond_1b
    move-object p1, p2

    .line 425
    goto :goto_1a

    .line 430
    :cond_1d
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/g/o;->b(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D

    move-result-wide v4

    .line 431
    add-int v0, v6, v8

    invoke-static {v7, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 434
    iget v1, p1, Lcom/google/android/location/e/w;->a:I

    .line 435
    iget v9, p2, Lcom/google/android/location/e/w;->a:I

    .line 436
    iget v2, p1, Lcom/google/android/location/e/w;->b:I

    .line 437
    iget v10, p2, Lcom/google/android/location/e/w;->b:I

    move-object v0, p0

    .line 438
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/g/o;->a(IIID)Lcom/google/android/location/e/u;

    move-result-object v11

    .line 442
    const-wide v0, 0x400921fb54442d18L

    add-double/2addr v4, v0

    .line 443
    add-int v0, v6, v7

    invoke-static {v8, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    move-object v0, p0

    move v1, v9

    move v2, v10

    .line 446
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/g/o;->a(IIID)Lcom/google/android/location/e/u;

    move-result-object v9

    .line 450
    invoke-direct {p0, v11, v9}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/e/u;Lcom/google/android/location/e/u;)Lcom/google/android/location/e/u;

    move-result-object v10

    .line 453
    add-int v0, v7, v8

    if-le v6, v0, :cond_97

    .line 458
    iget-object v0, v11, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iget-object v2, v11, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, v9, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v6, v9, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->b(DDDD)D

    move-result-wide v0

    move-wide v1, v0

    .line 469
    :goto_74
    new-instance p1, Lcom/google/android/location/e/w;

    iget-object v0, v10, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/google/android/location/g/c;->e(D)I

    move-result v3

    iget-object v0, v10, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/location/g/c;->e(D)I

    move-result v0

    double-to-int v1, v1

    invoke-static {v1}, Lcom/google/android/location/g/c;->c(I)I

    move-result v1

    invoke-direct {p1, v3, v0, v1}, Lcom/google/android/location/e/w;-><init>(III)V

    goto :goto_1a

    :cond_97
    move-object v0, p0

    move v1, v7

    move v2, v8

    move-object v3, p1

    move-object v4, p2

    move v5, v6

    move-object v6, v10

    .line 464
    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/g/o;->a(IILcom/google/android/location/e/w;Lcom/google/android/location/e/w;ILcom/google/android/location/e/u;)D

    move-result-wide v0

    move-wide v1, v0

    goto :goto_74
.end method

.method private a()Lcom/google/android/location/g/n$a;
    .registers 5

    .prologue
    .line 161
    new-instance v0, Lcom/google/android/location/g/n$a;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/location/g/o;->b:Ljava/util/Set;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/g/n$a;-><init>(Lcom/google/android/location/e/w;ILjava/util/Set;)V

    return-object v0
.end method

.method private a(Ljava/util/Set;Lcom/google/android/location/g/o$b;)Ljava/util/List;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;>;",
            "Lcom/google/android/location/g/o$b;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 401
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 402
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/g/o$b;

    invoke-direct {p0, v1, p2}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/g/o$b;Lcom/google/android/location/g/o$b;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 406
    :goto_1d
    return-object v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method private a(Lcom/google/android/location/e/w;Ljava/util/Map;)Ljava/util/Set;
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/w;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 242
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_d
    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 243
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/w;

    .line 244
    invoke-static {v1, p1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v1

    const/16 v4, 0xfa

    if-le v1, v4, :cond_d

    .line 245
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 248
    :cond_2f
    return-object v2
.end method

.method private a(Ljava/util/List;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 173
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    .line 174
    invoke-virtual {v0, v1}, Lcom/google/android/location/g/o$b;->a(I)V

    goto :goto_5

    :cond_15
    move v2, v1

    .line 177
    :goto_16
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_45

    .line 178
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    .line 179
    add-int/lit8 v1, v2, 0x1

    move v3, v1

    :goto_25
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_41

    .line 180
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/g/o$b;

    .line 181
    invoke-direct {p0, v0, v1}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/g/o$b;Lcom/google/android/location/g/o$b;)Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 182
    invoke-virtual {v0}, Lcom/google/android/location/g/o$b;->a()V

    .line 183
    invoke-virtual {v1}, Lcom/google/android/location/g/o$b;->a()V

    .line 179
    :cond_3d
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_25

    .line 177
    :cond_41
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_16

    .line 187
    :cond_45
    return-void
.end method

.method private a(Ljava/util/List;I)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 227
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 228
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    .line 229
    invoke-virtual {v0}, Lcom/google/android/location/g/o$b;->b()I

    move-result v0

    if-ne v0, p2, :cond_4

    .line 230
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 233
    :cond_1a
    return-void
.end method

.method private a(Lcom/google/android/location/g/o$b;Lcom/google/android/location/g/o$b;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 214
    invoke-virtual {p1}, Lcom/google/android/location/g/o$b;->c()Lcom/google/android/location/e/w;

    move-result-object v0

    .line 215
    invoke-virtual {p2}, Lcom/google/android/location/g/o$b;->c()Lcom/google/android/location/e/w;

    move-result-object v1

    .line 217
    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v2

    .line 218
    iget v0, v0, Lcom/google/android/location/e/w;->c:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->b(I)I

    move-result v0

    iget v1, v1, Lcom/google/android/location/e/w;->c:I

    invoke-static {v1}, Lcom/google/android/location/g/c;->b(I)I

    move-result v1

    add-int/2addr v0, v1

    if-gt v2, v0, :cond_1d

    const/4 v0, 0x1

    :goto_1c
    return v0

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method private b(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D
    .registers 15
    .parameter
    .parameter

    .prologue
    const-wide v8, 0x416312d000000000L

    .line 544
    iget v0, p1, Lcom/google/android/location/e/w;->a:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->d(I)D

    move-result-wide v0

    .line 545
    iget v2, p2, Lcom/google/android/location/e/w;->a:I

    invoke-static {v2}, Lcom/google/android/location/g/c;->d(I)D

    move-result-wide v2

    .line 547
    iget v4, p2, Lcom/google/android/location/e/w;->b:I

    int-to-double v4, v4

    div-double/2addr v4, v8

    iget v6, p1, Lcom/google/android/location/e/w;->b:I

    int-to-double v6, v6

    div-double/2addr v6, v8

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    .line 548
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    .line 549
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    .line 552
    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method private b(Ljava/util/List;)Lcom/google/android/location/e/u;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    const v1, 0x7fffffff

    .line 199
    const/high16 v0, -0x8000

    .line 200
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    move v1, v0

    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    .line 201
    invoke-virtual {v0}, Lcom/google/android/location/g/o$b;->b()I

    move-result v4

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 202
    invoke-virtual {v0}, Lcom/google/android/location/g/o$b;->b()I

    move-result v0

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v2, v0

    goto :goto_b

    .line 205
    :cond_29
    new-instance v0, Lcom/google/android/location/e/u;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private c(Ljava/util/List;)Lcom/google/android/location/e/w;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;)",
            "Lcom/google/android/location/e/w;"
        }
    .end annotation

    .prologue
    const v4, 0x249f0

    .line 262
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 264
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    .line 265
    invoke-direct {p0, v2, v0}, Lcom/google/android/location/g/o;->a(Ljava/util/Set;Lcom/google/android/location/g/o$b;)Ljava/util/List;

    move-result-object v1

    .line 266
    if-nez v1, :cond_26

    .line 267
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 268
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 270
    :cond_26
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 275
    :cond_2a
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 276
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_33
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_47

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 277
    invoke-direct {p0, v0}, Lcom/google/android/location/g/o;->e(Ljava/util/List;)Lcom/google/android/location/e/w;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_33

    .line 281
    :cond_47
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_65

    .line 285
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    .line 292
    :goto_55
    iget v1, v0, Lcom/google/android/location/e/w;->c:I

    if-ge v1, v4, :cond_64

    .line 294
    new-instance v1, Lcom/google/android/location/e/w$a;

    invoke-direct {v1, v0}, Lcom/google/android/location/e/w$a;-><init>(Lcom/google/android/location/e/w;)V

    .line 295
    iput v4, v1, Lcom/google/android/location/e/w$a;->c:I

    .line 296
    invoke-virtual {v1}, Lcom/google/android/location/e/w$a;->a()Lcom/google/android/location/e/w;

    move-result-object v0

    .line 298
    :cond_64
    return-object v0

    .line 289
    :cond_65
    invoke-direct {p0, v1}, Lcom/google/android/location/g/o;->d(Ljava/util/List;)Lcom/google/android/location/e/w;

    move-result-object v0

    goto :goto_55
.end method

.method private d(Ljava/util/List;)Lcom/google/android/location/e/w;
    .registers 14
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/e/w;",
            ">;)",
            "Lcom/google/android/location/e/w;"
        }
    .end annotation

    .prologue
    .line 327
    const/4 v1, 0x0

    .line 331
    const/4 v0, 0x0

    move v7, v0

    move-object v8, v1

    :goto_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v7, v0, :cond_53

    .line 332
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    .line 333
    add-int/lit8 v1, v7, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/google/android/location/e/w;

    .line 334
    invoke-direct {p0, v0, v6}, Lcom/google/android/location/g/o;->b(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)D

    move-result-wide v4

    .line 336
    iget v1, v0, Lcom/google/android/location/e/w;->a:I

    iget v2, v0, Lcom/google/android/location/e/w;->b:I

    iget v0, v0, Lcom/google/android/location/e/w;->c:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->b(I)I

    move-result v3

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/g/o;->a(IIID)Lcom/google/android/location/e/u;

    move-result-object v9

    .line 339
    iget v1, v6, Lcom/google/android/location/e/w;->a:I

    iget v2, v6, Lcom/google/android/location/e/w;->b:I

    iget v0, v6, Lcom/google/android/location/e/w;->c:I

    invoke-static {v0}, Lcom/google/android/location/g/c;->b(I)I

    move-result v3

    const-wide v10, 0x400921fb54442d18L

    add-double/2addr v4, v10

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/g/o;->a(IIID)Lcom/google/android/location/e/u;

    move-result-object v0

    .line 343
    invoke-direct {p0, v9, v0}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/e/u;Lcom/google/android/location/e/u;)Lcom/google/android/location/e/u;

    move-result-object v0

    .line 345
    if-nez v8, :cond_4e

    .line 331
    :goto_49
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move-object v8, v0

    goto :goto_4

    .line 348
    :cond_4e
    invoke-direct {p0, v8, v0}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/e/u;Lcom/google/android/location/e/u;)Lcom/google/android/location/e/u;

    move-result-object v0

    goto :goto_49

    .line 351
    :cond_53
    new-instance v1, Lcom/google/android/location/e/w$a;

    invoke-direct {v1}, Lcom/google/android/location/e/w$a;-><init>()V

    iget-object v0, v8, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/location/g/c;->e(D)I

    move-result v2

    iget-object v0, v8, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/google/android/location/g/c;->e(D)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/location/e/w$a;->a(II)Lcom/google/android/location/e/w$a;

    move-result-object v2

    .line 357
    const v0, 0x7fffffff

    .line 358
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_7c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_90

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    .line 359
    invoke-static {v2, v0}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w$a;Lcom/google/android/location/e/w;)I

    move-result v0

    .line 360
    if-ge v0, v1, :cond_9b

    :goto_8e
    move v1, v0

    .line 363
    goto :goto_7c

    .line 364
    :cond_90
    invoke-static {v1}, Lcom/google/android/location/g/c;->c(I)I

    move-result v0

    iput v0, v2, Lcom/google/android/location/e/w$a;->c:I

    .line 365
    invoke-virtual {v2}, Lcom/google/android/location/e/w$a;->a()Lcom/google/android/location/e/w;

    move-result-object v0

    return-object v0

    :cond_9b
    move v0, v1

    goto :goto_8e
.end method

.method private e(Ljava/util/List;)Lcom/google/android/location/e/w;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/g/o$b;",
            ">;)",
            "Lcom/google/android/location/e/w;"
        }
    .end annotation

    .prologue
    .line 383
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    invoke-virtual {v0}, Lcom/google/android/location/g/o$b;->c()Lcom/google/android/location/e/w;

    move-result-object v1

    .line 385
    const/4 v0, 0x1

    move-object v2, v1

    move v1, v0

    :goto_e
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_26

    .line 386
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/g/o$b;

    invoke-virtual {v0}, Lcom/google/android/location/g/o$b;->c()Lcom/google/android/location/e/w;

    move-result-object v0

    .line 387
    invoke-direct {p0, v2, v0}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)Lcom/google/android/location/e/w;

    move-result-object v2

    .line 385
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_e

    .line 389
    :cond_26
    return-object v2
.end method


# virtual methods
.method a(IIID)Lcom/google/android/location/e/u;
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIID)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 568
    int-to-double v0, p3

    const-wide v2, 0x4158554c00000000L

    div-double/2addr v0, v2

    .line 570
    invoke-static {p1}, Lcom/google/android/location/g/c;->d(I)D

    move-result-wide v2

    .line 571
    invoke-static {p2}, Lcom/google/android/location/g/c;->d(I)D

    move-result-wide v4

    .line 574
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->asin(D)D

    move-result-wide v6

    .line 578
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v2, v10

    sub-double/2addr v0, v2

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    add-double/2addr v0, v4

    .line 582
    const-wide v2, 0x400921fb54442d18L

    add-double/2addr v0, v2

    const-wide v2, 0x401921fb54442d18L

    rem-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L

    sub-double/2addr v0, v2

    .line 584
    new-instance v2, Lcom/google/android/location/e/u;

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/google/android/location/e/u;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method public a(Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/location/g/n$a;
    .registers 12
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/location/g/n$a;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 90
    if-eqz p1, :cond_a

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 91
    :cond_a
    invoke-direct {p0}, Lcom/google/android/location/g/o;->a()Lcom/google/android/location/g/n$a;

    move-result-object v0

    .line 156
    :goto_e
    return-object v0

    .line 96
    :cond_f
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_17
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/C;

    .line 97
    iget-object v2, v0, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    sget-object v5, Lcom/google/android/location/e/C$a;->c:Lcom/google/android/location/e/C$a;

    if-eq v2, v5, :cond_2f

    iget-object v0, v0, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    sget-object v2, Lcom/google/android/location/e/C$a;->d:Lcom/google/android/location/e/C$a;

    if-ne v0, v2, :cond_17

    :cond_2f
    move v2, v3

    .line 104
    :goto_30
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 107
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3d
    :goto_3d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_72

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 108
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/C;

    .line 109
    if-nez v2, :cond_5d

    iget-object v7, v1, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    sget-object v8, Lcom/google/android/location/e/C$a;->c:Lcom/google/android/location/e/C$a;

    if-eq v7, v8, :cond_5d

    iget-object v1, v1, Lcom/google/android/location/e/C;->j:Lcom/google/android/location/e/C$a;

    sget-object v7, Lcom/google/android/location/e/C$a;->d:Lcom/google/android/location/e/C$a;

    if-ne v1, v7, :cond_3d

    .line 111
    :cond_5d
    new-instance v7, Lcom/google/android/location/g/o$b;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/w;

    invoke-direct {v7, v1, v0}, Lcom/google/android/location/g/o$b;-><init>(Ljava/lang/Long;Lcom/google/android/location/e/w;)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3d

    .line 114
    :cond_72
    new-instance v0, Lcom/google/android/location/g/o$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/g/o$a;-><init>(Lcom/google/android/location/g/o;Lcom/google/android/location/g/o$1;)V

    invoke-static {v5, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 123
    :cond_7b
    invoke-direct {p0, v5}, Lcom/google/android/location/g/o;->a(Ljava/util/List;)V

    .line 126
    invoke-direct {p0, v5}, Lcom/google/android/location/g/o;->b(Ljava/util/List;)Lcom/google/android/location/e/u;

    move-result-object v0

    .line 128
    iget-object v1, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    iget-object v2, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    if-eq v1, v2, :cond_c9

    move v1, v4

    .line 129
    :goto_89
    if-eqz v1, :cond_96

    .line 131
    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v5, v0}, Lcom/google/android/location/g/o;->a(Ljava/util/List;I)V

    .line 133
    :cond_96
    if-nez v1, :cond_7b

    .line 136
    invoke-direct {p0, v5}, Lcom/google/android/location/g/o;->c(Ljava/util/List;)Lcom/google/android/location/e/w;

    move-result-object v1

    .line 138
    iget v0, v1, Lcom/google/android/location/e/w;->c:I

    const v2, 0x16e360

    if-le v0, v2, :cond_cb

    .line 140
    sget-object v0, Lcom/google/android/location/g/o;->a:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignoring computed location since accuracy too high: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v1, v1, Lcom/google/android/location/e/w;->c:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mm."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 142
    invoke-direct {p0}, Lcom/google/android/location/g/o;->a()Lcom/google/android/location/g/n$a;

    move-result-object v0

    goto/16 :goto_e

    :cond_c9
    move v1, v3

    .line 128
    goto :goto_89

    .line 146
    :cond_cb
    const/16 v2, 0x50

    .line 152
    invoke-direct {p0, v1, p1}, Lcom/google/android/location/g/o;->a(Lcom/google/android/location/e/w;Ljava/util/Map;)Ljava/util/Set;

    move-result-object v3

    .line 153
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_ef

    .line 154
    sget-object v0, Lcom/google/android/location/g/o;->a:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Not returning location for the following outliers: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 156
    :cond_ef
    new-instance v0, Lcom/google/android/location/g/n$a;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/g/n$a;-><init>(Lcom/google/android/location/e/w;ILjava/util/Set;)V

    goto/16 :goto_e

    :cond_f6
    move v2, v4

    goto/16 :goto_30
.end method
