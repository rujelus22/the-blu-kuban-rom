.class abstract Lcom/google/android/location/c/D;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/D$a;
    }
.end annotation


# instance fields
.field protected final a:Lcom/google/android/location/c/l;

.field protected final b:Lcom/google/android/location/k/a/c;

.field protected volatile c:Z

.field protected final d:Lcom/google/android/location/c/H;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;Lcom/google/android/location/c/H;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c/D;->c:Z

    .line 24
    iput-object p1, p0, Lcom/google/android/location/c/D;->a:Lcom/google/android/location/c/l;

    .line 25
    invoke-static {p2}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/D;->b:Lcom/google/android/location/k/a/c;

    .line 26
    iput-object p3, p0, Lcom/google/android/location/c/D;->d:Lcom/google/android/location/c/H;

    .line 27
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected abstract a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
.end method

.method public declared-synchronized b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 38
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/D;->c:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_16

    if-eqz v0, :cond_8

    .line 40
    const/4 v0, 0x0

    .line 45
    :goto_6
    monitor-exit p0

    return v0

    .line 42
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/google/android/location/c/D;->d:Lcom/google/android/location/c/H;

    if-eqz v0, :cond_11

    .line 43
    iget-object v0, p0, Lcom/google/android/location/c/D;->d:Lcom/google/android/location/c/H;

    invoke-virtual {v0, p1}, Lcom/google/android/location/c/H;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 45
    :cond_11
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/c/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    :try_end_14
    .catchall {:try_start_8 .. :try_end_14} :catchall_16

    move-result v0

    goto :goto_6

    .line 38
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()V
    .registers 2

    .prologue
    .line 60
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/location/c/D;->c:Z

    .line 61
    invoke-virtual {p0}, Lcom/google/android/location/c/D;->a()V
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_9

    .line 62
    monitor-exit p0

    return-void

    .line 60
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method
