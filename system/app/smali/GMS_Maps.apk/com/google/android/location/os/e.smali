.class public final Lcom/google/android/location/os/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/e$b;,
        Lcom/google/android/location/os/e$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/location/os/e$a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/location/os/e$b;

.field private final c:Lcom/google/android/location/k/a/b;

.field private final d:Ljava/io/PrintWriter;

.field private final e:Ljava/util/Date;

.field private final f:Ljava/lang/StringBuffer;

.field private final g:Ljava/text/FieldPosition;

.field private final h:Ljava/text/SimpleDateFormat;

.field private final i:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/location/os/d;",
            ">;"
        }
    .end annotation
.end field

.field private final j:[Lcom/google/android/location/os/e$a;

.field private final k:[I

.field private final l:[J

.field private m:J

.field private n:I


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/e$b;Lcom/google/android/location/k/a/b;Ljava/io/PrintWriter;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/e;->a:Ljava/util/LinkedList;

    .line 92
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/e;->e:Ljava/util/Date;

    .line 93
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/os/e;->f:Ljava/lang/StringBuffer;

    .line 94
    new-instance v0, Ljava/text/FieldPosition;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/text/FieldPosition;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/os/e;->g:Ljava/text/FieldPosition;

    .line 95
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy.MM.dd HH:mm:ss "

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/os/e;->h:Ljava/text/SimpleDateFormat;

    .line 97
    sget-object v0, Lcom/google/android/location/os/d;->h:Lcom/google/android/location/os/d;

    sget-object v1, Lcom/google/android/location/os/d;->K:Lcom/google/android/location/os/d;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/e;->i:Ljava/util/EnumSet;

    .line 103
    invoke-static {}, Lcom/google/android/location/os/d;->values()[Lcom/google/android/location/os/d;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/location/os/e$a;

    iput-object v0, p0, Lcom/google/android/location/os/e;->j:[Lcom/google/android/location/os/e$a;

    .line 105
    invoke-static {}, Lcom/google/android/location/os/d;->values()[Lcom/google/android/location/os/d;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/location/os/e;->k:[I

    .line 107
    invoke-static {}, Lcom/google/android/location/os/d;->values()[Lcom/google/android/location/os/d;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/location/os/e;->l:[J

    .line 108
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/os/e;->m:J

    .line 114
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/os/e;->n:I

    .line 122
    iput-object p1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    .line 123
    iput-object p2, p0, Lcom/google/android/location/os/e;->c:Lcom/google/android/location/k/a/b;

    .line 124
    iput-object p3, p0, Lcom/google/android/location/os/e;->d:Ljava/io/PrintWriter;

    .line 125
    iget-object v0, p0, Lcom/google/android/location/os/e;->l:[J

    const-wide/16 v1, -0x1

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->fill([JJ)V

    .line 126
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/location/os/e$a;)V
    .registers 3
    .parameter

    .prologue
    .line 167
    monitor-enter p0

    const/4 v0, -0x1

    :try_start_2
    invoke-direct {p0, p1, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;I)V
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_7

    .line 168
    monitor-exit p0

    return-void

    .line 167
    :catchall_7
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/android/location/os/e$a;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 171
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/os/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_1d

    .line 172
    iget-object v0, p0, Lcom/google/android/location/os/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/e$a;

    .line 173
    iget-object v1, p0, Lcom/google/android/location/os/e;->l:[J

    iget-object v0, v0, Lcom/google/android/location/os/e$a;->e:Lcom/google/android/location/os/d;

    invoke-virtual {v0}, Lcom/google/android/location/os/d;->ordinal()I

    move-result v0

    iget-wide v2, p1, Lcom/google/android/location/os/e$a;->f:J

    aput-wide v2, v1, v0

    .line 175
    :cond_1d
    iget-object v0, p0, Lcom/google/android/location/os/e;->j:[Lcom/google/android/location/os/e$a;

    iget-object v1, p1, Lcom/google/android/location/os/e$a;->e:Lcom/google/android/location/os/d;

    invoke-virtual {v1}, Lcom/google/android/location/os/d;->ordinal()I

    move-result v1

    aput-object p1, v0, v1

    .line 176
    iget-object v0, p0, Lcom/google/android/location/os/e;->k:[I

    iget-object v1, p1, Lcom/google/android/location/os/e$a;->e:Lcom/google/android/location/os/d;

    invoke-virtual {v1}, Lcom/google/android/location/os/d;->ordinal()I

    move-result v1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 179
    iget-object v0, p1, Lcom/google/android/location/os/e$a;->e:Lcom/google/android/location/os/d;

    sget-object v1, Lcom/google/android/location/os/d;->n:Lcom/google/android/location/os/d;

    if-ne v0, v1, :cond_6d

    iget-object v0, p0, Lcom/google/android/location/os/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_6d

    iget-object v0, p0, Lcom/google/android/location/os/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/e$a;

    iget-object v0, v0, Lcom/google/android/location/os/e$a;->e:Lcom/google/android/location/os/d;

    sget-object v1, Lcom/google/android/location/os/d;->n:Lcom/google/android/location/os/d;

    if-ne v0, v1, :cond_6d

    iget-object v0, p0, Lcom/google/android/location/os/e;->a:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/google/android/location/os/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/e$a;

    iget-object v0, v0, Lcom/google/android/location/os/e$a;->e:Lcom/google/android/location/os/d;

    sget-object v1, Lcom/google/android/location/os/d;->n:Lcom/google/android/location/os/d;

    if-ne v0, v1, :cond_6d

    .line 182
    iget-object v0, p0, Lcom/google/android/location/os/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    .line 184
    :cond_6d
    iget-object v0, p0, Lcom/google/android/location/os/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 185
    :goto_72
    iget-object v0, p0, Lcom/google/android/location/os/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0xc8

    if-le v0, v1, :cond_85

    .line 186
    iget-object v0, p0, Lcom/google/android/location/os/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
    :try_end_81
    .catchall {:try_start_1 .. :try_end_81} :catchall_82

    goto :goto_72

    .line 171
    :catchall_82
    move-exception v0

    monitor-exit p0

    throw v0

    .line 188
    :cond_85
    :try_start_85
    iget-object v0, p1, Lcom/google/android/location/os/e$a;->e:Lcom/google/android/location/os/d;

    iget-object v0, v0, Lcom/google/android/location/os/d;->P:Lcom/google/android/location/os/d$a;

    sget-object v1, Lcom/google/android/location/os/d$a;->a:Lcom/google/android/location/os/d$a;

    if-ne v0, v1, :cond_91

    .line 189
    iget-wide v0, p1, Lcom/google/android/location/os/e$a;->f:J

    iput-wide v0, p0, Lcom/google/android/location/os/e;->m:J

    .line 191
    :cond_91
    iget-object v0, p1, Lcom/google/android/location/os/e$a;->e:Lcom/google/android/location/os/d;

    .line 192
    sget-object v1, Lcom/google/android/location/os/d;->u:Lcom/google/android/location/os/d;

    if-eq v0, v1, :cond_a7

    sget-object v1, Lcom/google/android/location/os/d;->t:Lcom/google/android/location/os/d;

    if-eq v0, v1, :cond_a7

    sget-object v1, Lcom/google/android/location/os/d;->e:Lcom/google/android/location/os/d;

    if-eq v0, v1, :cond_a7

    sget-object v1, Lcom/google/android/location/os/d;->L:Lcom/google/android/location/os/d;

    if-eq v0, v1, :cond_a7

    sget-object v1, Lcom/google/android/location/os/d;->M:Lcom/google/android/location/os/d;

    if-ne v0, v1, :cond_112

    .line 194
    :cond_a7
    iput p2, p0, Lcom/google/android/location/os/e;->n:I

    .line 198
    :goto_a9
    iget-object v0, p0, Lcom/google/android/location/os/e;->c:Lcom/google/android/location/k/a/b;

    if-eqz v0, :cond_dd

    iget-object v0, p0, Lcom/google/android/location/os/e;->i:Ljava/util/EnumSet;

    iget-object v1, p1, Lcom/google/android/location/os/e$a;->e:Lcom/google/android/location/os/d;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_dd

    iget-object v0, p0, Lcom/google/android/location/os/e;->c:Lcom/google/android/location/k/a/b;

    const-string v1, "gmmNlpEventLog"

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/google/android/location/k/a/b;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_dd

    .line 200
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 201
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 202
    invoke-virtual {p1, v1}, Lcom/google/android/location/os/e$a;->b(Ljava/io/PrintWriter;)V

    .line 203
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    .line 204
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lcom/google/android/location/os/e;->c:Lcom/google/android/location/k/a/b;

    const-string v2, "gmmNlpEventLog"

    invoke-interface {v1, v2, v0}, Lcom/google/android/location/k/a/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :cond_dd
    iget-object v0, p0, Lcom/google/android/location/os/e;->d:Ljava/io/PrintWriter;

    if-eqz v0, :cond_110

    .line 212
    iget-object v0, p0, Lcom/google/android/location/os/e;->e:Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Date;->setTime(J)V

    .line 213
    iget-object v0, p0, Lcom/google/android/location/os/e;->f:Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 214
    iget-object v0, p0, Lcom/google/android/location/os/e;->h:Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/google/android/location/os/e;->e:Ljava/util/Date;

    iget-object v2, p0, Lcom/google/android/location/os/e;->f:Ljava/lang/StringBuffer;

    iget-object v3, p0, Lcom/google/android/location/os/e;->g:Ljava/text/FieldPosition;

    invoke-virtual {v0, v1, v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 215
    iget-object v0, p0, Lcom/google/android/location/os/e;->d:Ljava/io/PrintWriter;

    iget-object v1, p0, Lcom/google/android/location/os/e;->f:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/location/os/e;->d:Ljava/io/PrintWriter;

    invoke-virtual {p1, v0}, Lcom/google/android/location/os/e$a;->b(Ljava/io/PrintWriter;)V

    .line 217
    iget-object v0, p0, Lcom/google/android/location/os/e;->d:Ljava/io/PrintWriter;

    invoke-virtual {v0}, Ljava/io/PrintWriter;->flush()V
    :try_end_110
    .catchall {:try_start_85 .. :try_end_110} :catchall_82

    .line 219
    :cond_110
    monitor-exit p0

    return-void

    .line 196
    :cond_112
    const/4 v0, -0x1

    :try_start_113
    iput v0, p0, Lcom/google/android/location/os/e;->n:I
    :try_end_115
    .catchall {:try_start_113 .. :try_end_115} :catchall_82

    goto :goto_a9
.end method


# virtual methods
.method public a(I)V
    .registers 8
    .parameter

    .prologue
    .line 246
    new-instance v0, Lcom/google/android/location/os/e$12;

    sget-object v2, Lcom/google/android/location/os/d;->e:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/e$12;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JI)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;I)V

    .line 252
    return-void
.end method

.method public a(IIZ)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 255
    new-instance v0, Lcom/google/android/location/os/e$13;

    sget-object v2, Lcom/google/android/location/os/d;->f:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    move v6, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/os/e$13;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JIIZ)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 266
    return-void
.end method

.method public a(IJ)V
    .registers 12
    .parameter
    .parameter

    .prologue
    .line 343
    new-instance v0, Lcom/google/android/location/os/e$5;

    sget-object v2, Lcom/google/android/location/os/d;->t:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    move-wide v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/os/e$5;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JIJ)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;I)V

    .line 351
    return-void
.end method

.method public a(IZ)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 226
    new-instance v0, Lcom/google/android/location/os/e$1;

    sget-object v2, Lcom/google/android/location/os/d;->c:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/e$1;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JIZ)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 234
    return-void
.end method

.method public a(Lcom/google/android/location/e/E;)V
    .registers 8
    .parameter

    .prologue
    .line 325
    new-instance v0, Lcom/google/android/location/os/e$3;

    sget-object v2, Lcom/google/android/location/os/d;->r:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/e$3;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JLcom/google/android/location/e/E;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 331
    return-void
.end method

.method public a(Lcom/google/android/location/e/e;)V
    .registers 8
    .parameter

    .prologue
    .line 269
    new-instance v0, Lcom/google/android/location/os/e$14;

    sget-object v2, Lcom/google/android/location/os/d;->g:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/e$14;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JLcom/google/android/location/e/e;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 275
    return-void
.end method

.method public a(Lcom/google/android/location/e/t;)V
    .registers 8
    .parameter

    .prologue
    .line 363
    new-instance v0, Lcom/google/android/location/os/e$6;

    sget-object v2, Lcom/google/android/location/os/d;->J:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/e$6;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JLcom/google/android/location/e/t;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 369
    return-void
.end method

.method public a(Lcom/google/android/location/os/d;)V
    .registers 5
    .parameter

    .prologue
    .line 222
    new-instance v0, Lcom/google/android/location/os/e$a;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v1

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/location/os/e$a;-><init>(Lcom/google/android/location/os/d;J)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 223
    return-void
.end method

.method public a(Lcom/google/android/location/os/g;)V
    .registers 8
    .parameter

    .prologue
    .line 307
    new-instance v0, Lcom/google/android/location/os/e$18;

    sget-object v2, Lcom/google/android/location/os/d;->n:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/e$18;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JLcom/google/android/location/os/g;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 313
    return-void
.end method

.method public declared-synchronized a(Ljava/io/PrintWriter;)V
    .registers 6
    .parameter

    .prologue
    .line 422
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/os/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/e$a;

    .line 423
    iget-object v2, v0, Lcom/google/android/location/os/e$a;->e:Lcom/google/android/location/os/d;

    iget-object v2, v2, Lcom/google/android/location/os/d;->P:Lcom/google/android/location/os/d$a;

    sget-object v3, Lcom/google/android/location/os/d$a;->a:Lcom/google/android/location/os/d$a;

    if-ne v2, v3, :cond_20

    .line 424
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(C)V

    .line 426
    :cond_20
    invoke-virtual {v0, p1}, Lcom/google/android/location/os/e$a;->b(Ljava/io/PrintWriter;)V
    :try_end_23
    .catchall {:try_start_1 .. :try_end_23} :catchall_24

    goto :goto_7

    .line 422
    :catchall_24
    move-exception v0

    monitor-exit p0

    throw v0

    .line 428
    :cond_27
    monitor-exit p0

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 399
    new-instance v0, Lcom/google/android/location/os/e$10;

    sget-object v2, Lcom/google/android/location/os/d;->K:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/e$10;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JLjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 407
    return-void
.end method

.method public declared-synchronized a(Ljava/text/Format;JJLjava/io/PrintWriter;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 439
    monitor-enter p0

    :try_start_1
    new-instance v1, Ljava/util/Date;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 440
    const-wide/32 v2, 0x927c0

    sub-long v2, p4, v2

    .line 441
    iget-object v0, p0, Lcom/google/android/location/os/e;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_13
    :goto_13
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/e$a;

    .line 442
    iget-wide v5, v0, Lcom/google/android/location/os/e$a;->f:J

    cmp-long v5, v5, v2

    if-ltz v5, :cond_13

    .line 445
    iget-object v5, v0, Lcom/google/android/location/os/e$a;->e:Lcom/google/android/location/os/d;

    iget-object v5, v5, Lcom/google/android/location/os/d;->P:Lcom/google/android/location/os/d$a;

    sget-object v6, Lcom/google/android/location/os/d$a;->a:Lcom/google/android/location/os/d$a;

    if-ne v5, v6, :cond_32

    .line 446
    const/16 v5, 0xa

    invoke-virtual {p6, v5}, Ljava/io/PrintWriter;->print(C)V

    .line 448
    :cond_32
    invoke-virtual {v1, p2, p3}, Ljava/util/Date;->setTime(J)V

    .line 449
    invoke-virtual {v0, p1, v1, p6}, Lcom/google/android/location/os/e$a;->a(Ljava/text/Format;Ljava/util/Date;Ljava/io/PrintWriter;)V
    :try_end_38
    .catchall {:try_start_1 .. :try_end_38} :catchall_39

    goto :goto_13

    .line 439
    :catchall_39
    move-exception v0

    monitor-exit p0

    throw v0

    .line 451
    :cond_3c
    monitor-exit p0

    return-void
.end method

.method public a(Z)V
    .registers 8
    .parameter

    .prologue
    .line 237
    new-instance v0, Lcom/google/android/location/os/e$11;

    sget-object v2, Lcom/google/android/location/os/d;->d:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/e$11;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JZ)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 243
    return-void
.end method

.method public a(ZZI)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 297
    new-instance v0, Lcom/google/android/location/os/e$17;

    sget-object v2, Lcom/google/android/location/os/d;->o:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    move v6, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/os/e$17;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JZZI)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 304
    return-void
.end method

.method public b(I)V
    .registers 8
    .parameter

    .prologue
    .line 278
    new-instance v0, Lcom/google/android/location/os/e$15;

    sget-object v2, Lcom/google/android/location/os/d;->h:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/e$15;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JI)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 284
    return-void
.end method

.method public b(Z)V
    .registers 8
    .parameter

    .prologue
    .line 287
    new-instance v0, Lcom/google/android/location/os/e$16;

    sget-object v2, Lcom/google/android/location/os/d;->i:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/e$16;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JZ)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 293
    return-void
.end method

.method public c(I)V
    .registers 8
    .parameter

    .prologue
    .line 381
    new-instance v0, Lcom/google/android/location/os/e$8;

    sget-object v2, Lcom/google/android/location/os/d;->L:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/e$8;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JI)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;I)V

    .line 387
    return-void
.end method

.method public c(Z)V
    .registers 8
    .parameter

    .prologue
    .line 316
    new-instance v0, Lcom/google/android/location/os/e$2;

    sget-object v2, Lcom/google/android/location/os/d;->q:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/e$2;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JZ)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 322
    return-void
.end method

.method public d(I)V
    .registers 8
    .parameter

    .prologue
    .line 390
    new-instance v0, Lcom/google/android/location/os/e$9;

    sget-object v2, Lcom/google/android/location/os/d;->M:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/e$9;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JI)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;I)V

    .line 396
    return-void
.end method

.method public d(Z)V
    .registers 8
    .parameter

    .prologue
    .line 334
    new-instance v0, Lcom/google/android/location/os/e$4;

    sget-object v2, Lcom/google/android/location/os/d;->s:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/e$4;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JZ)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 340
    return-void
.end method

.method public e(Z)V
    .registers 8
    .parameter

    .prologue
    .line 372
    new-instance v0, Lcom/google/android/location/os/e$7;

    sget-object v2, Lcom/google/android/location/os/d;->H:Lcom/google/android/location/os/d;

    iget-object v1, p0, Lcom/google/android/location/os/e;->b:Lcom/google/android/location/os/e$b;

    invoke-interface {v1}, Lcom/google/android/location/os/e$b;->a()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/e$7;-><init>(Lcom/google/android/location/os/e;Lcom/google/android/location/os/d;JZ)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/e;->a(Lcom/google/android/location/os/e$a;)V

    .line 378
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 414
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x2710

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 415
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 416
    invoke-virtual {p0, v1}, Lcom/google/android/location/os/e;->a(Ljava/io/PrintWriter;)V

    .line 417
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    .line 418
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
