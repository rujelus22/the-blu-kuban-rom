.class public Lcom/google/android/location/h/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/h/a/c$b;,
        Lcom/google/android/location/h/a/c$a;
    }
.end annotation


# instance fields
.field protected a:Z

.field private b:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/google/android/location/h/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Object;

.field private d:Las/c;

.field private e:Lar/d;

.field private f:Lcom/google/googlenav/common/io/g;

.field private g:Ljava/lang/String;

.field private h:[Ljava/lang/Thread;


# direct methods
.method protected constructor <init>()V
    .registers 2

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    .line 34
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    .line 56
    return-void
.end method

.method public constructor <init>(Las/c;Lar/d;Lcom/google/googlenav/common/io/g;Ljava/lang/String;I)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    .line 34
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    .line 69
    iput-object p1, p0, Lcom/google/android/location/h/a/c;->d:Las/c;

    .line 70
    iput-object p2, p0, Lcom/google/android/location/h/a/c;->e:Lar/d;

    .line 71
    iput-object p3, p0, Lcom/google/android/location/h/a/c;->f:Lcom/google/googlenav/common/io/g;

    .line 72
    iput-object p4, p0, Lcom/google/android/location/h/a/c;->g:Ljava/lang/String;

    .line 73
    new-array v0, p5, [Ljava/lang/Thread;

    iput-object v0, p0, Lcom/google/android/location/h/a/c;->h:[Ljava/lang/Thread;

    .line 74
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/h/a/c;)Las/c;
    .registers 2
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/location/h/a/c;->b()Las/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/h/a/c;Ljava/lang/String;Z)Lcom/google/googlenav/common/io/GoogleHttpConnection;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/a/c;->a(Ljava/lang/String;Z)Lcom/google/googlenav/common/io/GoogleHttpConnection;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Z)Lcom/google/googlenav/common/io/GoogleHttpConnection;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/location/h/a/c;->f:Lcom/google/googlenav/common/io/g;

    invoke-interface {v0, p1, p2}, Lcom/google/googlenav/common/io/g;->a(Ljava/lang/String;Z)Lcom/google/googlenav/common/io/GoogleHttpConnection;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/location/h/a/c$a;)Z
    .registers 4
    .parameter

    .prologue
    .line 173
    iget-object v1, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 174
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 175
    iget-object v0, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 176
    iget-object v0, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 177
    const/4 v0, 0x1

    monitor-exit v1

    .line 179
    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_17

    .line 181
    :catchall_1b
    move-exception v0

    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    throw v0
.end method

.method static synthetic a(Lcom/google/android/location/h/a/c;Lcom/google/android/location/h/a/c$a;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/location/h/a/c;->a(Lcom/google/android/location/h/a/c$a;)Z

    move-result v0

    return v0
.end method

.method private b()Las/c;
    .registers 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/location/h/a/c;->d:Las/c;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/location/h/a/c;)Lcom/google/googlenav/common/io/g;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/h/a/c;->f:Lcom/google/googlenav/common/io/g;

    return-object v0
.end method

.method private c()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 205
    .line 207
    iget-object v1, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 208
    :goto_4
    :try_start_4
    iget-object v2, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_19

    iget-boolean v2, p0, Lcom/google/android/location/h/a/c;->a:Z
    :try_end_e
    .catchall {:try_start_4 .. :try_end_e} :catchall_1f

    if-eqz v2, :cond_19

    .line 210
    :try_start_10
    iget-object v2, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_15
    .catchall {:try_start_10 .. :try_end_15} :catchall_1f
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_15} :catch_16

    goto :goto_4

    .line 211
    :catch_16
    move-exception v2

    .line 212
    :try_start_17
    monitor-exit v1

    .line 229
    :goto_18
    return v0

    .line 216
    :cond_19
    iget-boolean v2, p0, Lcom/google/android/location/h/a/c;->a:Z

    if-nez v2, :cond_22

    .line 217
    monitor-exit v1

    goto :goto_18

    .line 222
    :catchall_1f
    move-exception v0

    monitor-exit v1
    :try_end_21
    .catchall {:try_start_17 .. :try_end_21} :catchall_1f

    throw v0

    .line 220
    :cond_22
    :try_start_22
    iget-object v0, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/h/a/c$a;

    .line 221
    iget-object v2, p0, Lcom/google/android/location/h/a/c;->b:Ljava/util/Vector;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/Vector;->removeElementAt(I)V

    .line 222
    monitor-exit v1
    :try_end_32
    .catchall {:try_start_22 .. :try_end_32} :catchall_1f

    .line 225
    :try_start_32
    invoke-virtual {v0}, Lcom/google/android/location/h/a/c$a;->run()V
    :try_end_35
    .catch Ljava/lang/Throwable; {:try_start_32 .. :try_end_35} :catch_37

    .line 229
    :goto_35
    const/4 v0, 0x1

    goto :goto_18

    .line 226
    :catch_37
    move-exception v0

    .line 227
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_35
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lcom/google/android/location/h/a/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 92
    new-instance v0, Lcom/google/android/location/h/a/c$a;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/location/h/a/c$a;-><init>(Lcom/google/android/location/h/a/c;Ljava/lang/String;I)V

    return-object v0
.end method

.method public a()V
    .registers 7

    .prologue
    .line 114
    iget-object v1, p0, Lcom/google/android/location/h/a/c;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 115
    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/location/h/a/c;->a:Z

    if-nez v0, :cond_3d

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/h/a/c;->a:Z

    .line 117
    const/4 v0, 0x0

    :goto_b
    iget-object v2, p0, Lcom/google/android/location/h/a/c;->h:[Ljava/lang/Thread;

    array-length v2, v2

    if-ge v0, v2, :cond_3d

    .line 118
    iget-object v2, p0, Lcom/google/android/location/h/a/c;->h:[Ljava/lang/Thread;

    iget-object v3, p0, Lcom/google/android/location/h/a/c;->e:Lar/d;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/location/h/a/c;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, p0}, Lar/d;->a(Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Thread;

    move-result-object v3

    aput-object v3, v2, v0

    .line 119
    iget-object v2, p0, Lcom/google/android/location/h/a/c;->h:[Ljava/lang/Thread;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 117
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 122
    :cond_3d
    monitor-exit v1

    .line 123
    return-void

    .line 122
    :catchall_3f
    move-exception v0

    monitor-exit v1
    :try_end_41
    .catchall {:try_start_3 .. :try_end_41} :catchall_3f

    throw v0
.end method

.method public run()V
    .registers 2

    .prologue
    .line 199
    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/h/a/c;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    return-void
.end method
