.class public Lcom/google/android/location/h/b/h;
.super Lcom/google/android/location/h/b/n;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/location/h/b/b;

.field private final c:[Lcom/google/android/location/h/b/b;

.field private final d:I


# direct methods
.method public constructor <init>(IILcom/google/android/location/h/b/b;[Lcom/google/android/location/h/b/b;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/b/n;-><init>(II)V

    .line 31
    iput-object p3, p0, Lcom/google/android/location/h/b/h;->b:Lcom/google/android/location/h/b/b;

    .line 32
    iput-object p4, p0, Lcom/google/android/location/h/b/h;->c:[Lcom/google/android/location/h/b/b;

    .line 33
    array-length v0, p4

    iput v0, p0, Lcom/google/android/location/h/b/h;->d:I

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/h/f;)V
    .registers 6
    .parameter

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/location/h/b/n;-><init>(Lcom/google/android/location/h/f;)V

    .line 46
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 49
    :try_start_8
    invoke-static {v1}, Lcom/google/android/location/h/b/b;->a(Ljava/io/InputStream;)Lcom/google/android/location/h/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/b/h;->b:Lcom/google/android/location/h/b/b;

    .line 50
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readShort()S

    move-result v0

    iput v0, p0, Lcom/google/android/location/h/b/h;->d:I

    .line 51
    iget v0, p0, Lcom/google/android/location/h/b/h;->d:I

    new-array v0, v0, [Lcom/google/android/location/h/b/b;

    iput-object v0, p0, Lcom/google/android/location/h/b/h;->c:[Lcom/google/android/location/h/b/b;

    .line 53
    const/4 v0, 0x0

    :goto_1b
    iget v2, p0, Lcom/google/android/location/h/b/h;->d:I

    if-ge v0, v2, :cond_2c

    .line 54
    iget-object v2, p0, Lcom/google/android/location/h/b/h;->c:[Lcom/google/android/location/h/b/b;

    invoke-static {v1}, Lcom/google/android/location/h/b/b;->a(Ljava/io/InputStream;)Lcom/google/android/location/h/b/b;

    move-result-object v3

    aput-object v3, v2, v0
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_27} :catch_2a

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b

    .line 56
    :catch_2a
    move-exception v0

    .line 57
    throw v0

    .line 59
    :cond_2c
    return-void
.end method


# virtual methods
.method public b_()I
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/location/h/b/h;->b:Lcom/google/android/location/h/b/b;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/b;->f()Lcom/google/android/location/h/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/h/g;->b_()I

    move-result v0

    return v0
.end method

.method public c_()Ljava/io/InputStream;
    .registers 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/location/h/b/h;->b:Lcom/google/android/location/h/b/b;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/b;->f()Lcom/google/android/location/h/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/h/g;->c_()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/google/android/location/h/b/b;
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/location/h/b/h;->b:Lcom/google/android/location/h/b/b;

    return-object v0
.end method

.method public f()[Lcom/google/android/location/h/b/b;
    .registers 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/location/h/b/h;->c:[Lcom/google/android/location/h/b/b;

    return-object v0
.end method
