.class public Lcom/google/android/location/i/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/i/c$1;,
        Lcom/google/android/location/i/c$a;,
        Lcom/google/android/location/i/c$b;
    }
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field final b:J

.field final c:J

.field d:Lcom/google/android/location/os/h$a;

.field e:J

.field f:J

.field g:Lcom/google/android/location/i/c$b;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLcom/google/android/location/os/h$a;JJ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Lcom/google/android/location/i/c$b;

    invoke-direct {v0}, Lcom/google/android/location/i/c$b;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/i/c;->g:Lcom/google/android/location/i/c$b;

    .line 86
    iput-object p1, p0, Lcom/google/android/location/i/c;->a:Ljava/lang/String;

    .line 87
    iput-wide p2, p0, Lcom/google/android/location/i/c;->c:J

    .line 88
    iput-object p4, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    .line 89
    iput-wide p5, p0, Lcom/google/android/location/i/c;->b:J

    .line 90
    invoke-virtual {p0, p7, p8}, Lcom/google/android/location/i/c;->a(J)V

    .line 91
    return-void
.end method

.method private static a(JJJJ)J
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 199
    add-long v0, p0, p2

    sub-long/2addr v0, p4

    invoke-static {p6, p7, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/location/i/c;J)J
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/i/c;->f(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private b(JJ)J
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 182
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 183
    iget-wide v1, p0, Lcom/google/android/location/i/c;->b:J

    add-long/2addr v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 184
    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;)J

    move-result-wide v1

    .line 185
    iget-wide v3, p0, Lcom/google/android/location/i/c;->c:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_15

    .line 194
    :goto_14
    return-wide p1

    .line 189
    :cond_15
    iget-wide v1, p0, Lcom/google/android/location/i/c;->c:J

    invoke-static {v0, v1, v2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    .line 190
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/location/i/c;->b:J

    add-long/2addr v3, p3

    cmp-long v1, v1, v3

    if-lez v1, :cond_2a

    .line 192
    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 194
    :cond_2a
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/i/c;->b:J

    sub-long p1, v0, v2

    goto :goto_14
.end method

.method private b()Z
    .registers 7

    .prologue
    const-wide/32 v4, 0x5265c00

    .line 124
    iget-object v0, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v0, v0, Lcom/google/android/location/os/h$a;->c:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1b

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/google/android/location/i/c;->c:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1b

    iget-wide v0, p0, Lcom/google/android/location/i/c;->c:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_1b

    const/4 v0, 0x1

    :goto_1a
    return v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method private e(J)V
    .registers 7
    .parameter

    .prologue
    .line 105
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/i/c;->e:J

    .line 106
    invoke-direct {p0}, Lcom/google/android/location/i/c;->b()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 109
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 110
    iget-wide v1, p0, Lcom/google/android/location/i/c;->b:J

    add-long/2addr v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 111
    iget-wide v1, p0, Lcom/google/android/location/i/c;->c:J

    invoke-static {v0, v1, v2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    .line 112
    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 113
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/i/c;->b:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/i/c;->f:J

    .line 117
    :goto_27
    return-void

    .line 115
    :cond_28
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/i/c;->f:J

    goto :goto_27
.end method

.method private declared-synchronized f(J)J
    .registers 9
    .parameter

    .prologue
    const-wide/16 v0, 0x0

    .line 302
    monitor-enter p0

    cmp-long v2, p1, v0

    if-gez v2, :cond_9

    .line 307
    :goto_7
    monitor-exit p0

    return-wide v0

    .line 305
    :cond_9
    :try_start_9
    iget-object v0, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v0, v0, Lcom/google/android/location/os/h$a;->a:J

    iget-wide v2, p0, Lcom/google/android/location/i/c;->e:J

    sub-long/2addr v0, v2

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 306
    iget-wide v2, p0, Lcom/google/android/location/i/c;->e:J

    add-long/2addr v2, v0

    iget-object v4, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v4, v4, Lcom/google/android/location/os/h$a;->a:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/location/i/c;->e:J
    :try_end_21
    .catchall {:try_start_9 .. :try_end_21} :catchall_22

    goto :goto_7

    .line 302
    :catchall_22
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 352
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/i/c;->g:Lcom/google/android/location/i/c$b;

    invoke-virtual {v0}, Lcom/google/android/location/i/c$b;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(J)V
    .registers 4
    .parameter

    .prologue
    .line 97
    monitor-enter p0

    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/i/c;->e(J)V

    .line 98
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/i/c;->d(J)Z
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 99
    monitor-exit p0

    return-void

    .line 97
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(JJJLcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 153
    monitor-enter p0

    if-nez p7, :cond_8

    .line 154
    :try_start_3
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/i/c;->a(J)V
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_36

    .line 179
    :goto_6
    monitor-exit p0

    return-void

    .line 157
    :cond_8
    const/4 v1, 0x2

    :try_start_9
    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v9

    .line 159
    const/4 v1, 0x1

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v11

    .line 161
    iget-wide v5, p0, Lcom/google/android/location/i/c;->b:J

    move-wide/from16 v1, p3

    move-wide/from16 v3, p5

    move-wide v7, p1

    invoke-static/range {v1 .. v8}, Lcom/google/android/location/i/c;->a(JJJJ)J

    move-result-wide v1

    .line 163
    const-wide/32 v3, 0x5265c00

    add-long/2addr v1, v3

    cmp-long v1, v1, p1

    if-ltz v1, :cond_2f

    const-wide/16 v1, -0x1

    cmp-long v1, v9, v1

    if-nez v1, :cond_39

    .line 168
    :cond_2f
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/i/c;->e(J)V

    .line 178
    :goto_32
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/i/c;->d(J)Z
    :try_end_35
    .catchall {:try_start_9 .. :try_end_35} :catchall_36

    goto :goto_6

    .line 153
    :catchall_36
    move-exception v1

    monitor-exit p0

    throw v1

    .line 171
    :cond_39
    :try_start_39
    iget-wide v5, p0, Lcom/google/android/location/i/c;->b:J

    move-wide/from16 v1, p3

    move-wide v3, v9

    move-wide v7, p1

    invoke-static/range {v1 .. v8}, Lcom/google/android/location/i/c;->a(JJJJ)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/location/i/c;->f:J

    .line 173
    invoke-direct {p0}, Lcom/google/android/location/i/c;->b()Z

    move-result v1

    if-eqz v1, :cond_53

    .line 174
    iget-wide v1, p0, Lcom/google/android/location/i/c;->f:J

    invoke-direct {p0, v1, v2, p1, p2}, Lcom/google/android/location/i/c;->b(JJ)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/location/i/c;->f:J

    .line 176
    :cond_53
    iget-object v1, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v1, v1, Lcom/google/android/location/os/h$a;->a:J

    invoke-static {v1, v2, v11, v12}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/location/i/c;->e:J
    :try_end_5d
    .catchall {:try_start_39 .. :try_end_5d} :catchall_36

    goto :goto_32
.end method

.method declared-synchronized a(Lcom/google/android/location/os/h$a;J)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 204
    monitor-enter p0

    if-nez p1, :cond_5

    .line 217
    :goto_3
    monitor-exit p0

    return-void

    .line 208
    :cond_5
    :try_start_5
    invoke-virtual {p0, p2, p3}, Lcom/google/android/location/i/c;->d(J)Z

    .line 211
    iget-wide v0, p0, Lcom/google/android/location/i/c;->e:J

    iget-wide v2, p1, Lcom/google/android/location/os/h$a;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_14

    .line 213
    iget-wide v0, p1, Lcom/google/android/location/os/h$a;->a:J

    iput-wide v0, p0, Lcom/google/android/location/i/c;->e:J

    .line 216
    :cond_14
    iput-object p1, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;
    :try_end_16
    .catchall {:try_start_5 .. :try_end_16} :catchall_17

    goto :goto_3

    .line 204
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter

    .prologue
    .line 136
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iget-wide v1, p0, Lcom/google/android/location/i/c;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 137
    const/4 v0, 0x2

    iget-wide v1, p0, Lcom/google/android/location/i/c;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_d
    .catchall {:try_start_2 .. :try_end_d} :catchall_f

    .line 138
    monitor-exit p0

    return-void

    .line 136
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(JJ)Z
    .registers 10
    .parameter
    .parameter

    .prologue
    const-wide/16 v0, 0x0

    .line 258
    monitor-enter p0

    cmp-long v0, p1, v0

    if-gez v0, :cond_a

    .line 259
    const/4 v0, 0x0

    .line 264
    :goto_8
    monitor-exit p0

    return v0

    .line 261
    :cond_a
    :try_start_a
    invoke-virtual {p0, p3, p4}, Lcom/google/android/location/i/c;->d(J)Z

    .line 262
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/i/c;->c(J)Z

    move-result v0

    .line 263
    const-wide/16 v1, 0x0

    iget-wide v3, p0, Lcom/google/android/location/i/c;->e:J

    sub-long/2addr v3, p1

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/location/i/c;->e:J
    :try_end_1c
    .catchall {:try_start_a .. :try_end_1c} :catchall_1d

    goto :goto_8

    .line 258
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(JJZ)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 237
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0, p3, p4}, Lcom/google/android/location/i/c;->d(J)Z

    .line 238
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/i/c;->c(J)Z

    move-result v1

    .line 239
    if-eqz p5, :cond_12

    .line 240
    iget-object v2, p0, Lcom/google/android/location/i/c;->g:Lcom/google/android/location/i/c$b;

    if-nez v1, :cond_14

    const/4 v0, 0x1

    :goto_f
    invoke-virtual {v2, v0}, Lcom/google/android/location/i/c$b;->a(Z)V
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_16

    .line 242
    :cond_12
    monitor-exit p0

    return v1

    .line 240
    :cond_14
    const/4 v0, 0x0

    goto :goto_f

    .line 237
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized b(J)J
    .registers 5
    .parameter

    .prologue
    .line 223
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/i/c;->d(J)Z

    .line 224
    iget-wide v0, p0, Lcom/google/android/location/i/c;->e:J
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    monitor-exit p0

    return-wide v0

    .line 223
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized b(JJZ)Lcom/google/android/location/i/c$a;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    .line 281
    monitor-enter p0

    cmp-long v1, p1, v1

    if-gez v1, :cond_a

    .line 290
    :cond_8
    :goto_8
    monitor-exit p0

    return-object v0

    .line 284
    :cond_a
    :try_start_a
    invoke-virtual {p0, p3, p4}, Lcom/google/android/location/i/c;->d(J)Z

    .line 285
    invoke-virtual/range {p0 .. p5}, Lcom/google/android/location/i/c;->a(JJZ)Z

    move-result v1

    .line 286
    if-eqz v1, :cond_8

    .line 287
    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/google/android/location/i/c;->e:J

    sub-long/2addr v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/i/c;->e:J

    .line 288
    new-instance v0, Lcom/google/android/location/i/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/location/i/c$a;-><init>(Lcom/google/android/location/i/c;JLcom/google/android/location/i/c$1;)V
    :try_end_24
    .catchall {:try_start_a .. :try_end_24} :catchall_25

    goto :goto_8

    .line 281
    :catchall_25
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized c(J)Z
    .registers 5
    .parameter

    .prologue
    .line 246
    monitor-enter p0

    :try_start_1
    iget-wide v0, p0, Lcom/google/android/location/i/c;->e:J
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_c

    cmp-long v0, p1, v0

    if-gtz v0, :cond_a

    const/4 v0, 0x1

    :goto_8
    monitor-exit p0

    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_8

    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized d(J)Z
    .registers 13
    .parameter

    .prologue
    const-wide/16 v0, 0x0

    .line 322
    monitor-enter p0

    :try_start_3
    iget-wide v2, p0, Lcom/google/android/location/i/c;->f:J

    .line 323
    iget-wide v4, p0, Lcom/google/android/location/i/c;->f:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_33

    .line 324
    iget-object v0, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v0, v0, Lcom/google/android/location/os/h$a;->b:J

    .line 325
    iput-wide p1, p0, Lcom/google/android/location/i/c;->f:J

    .line 335
    :cond_13
    :goto_13
    iget-wide v4, p0, Lcom/google/android/location/i/c;->e:J

    .line 336
    iget-wide v6, p0, Lcom/google/android/location/i/c;->e:J

    add-long/2addr v0, v6

    iget-object v6, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v6, v6, Lcom/google/android/location/os/h$a;->a:J

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/i/c;->e:J

    .line 337
    iget-wide v0, p0, Lcom/google/android/location/i/c;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_2e

    iget-wide v0, p0, Lcom/google/android/location/i/c;->f:J
    :try_end_2a
    .catchall {:try_start_3 .. :try_end_2a} :catchall_50

    cmp-long v0, v2, v0

    if-eqz v0, :cond_53

    :cond_2e
    const/4 v0, 0x1

    .line 338
    :goto_2f
    if-eqz v0, :cond_31

    .line 341
    :cond_31
    monitor-exit p0

    return v0

    .line 327
    :cond_33
    :try_start_33
    iget-wide v4, p0, Lcom/google/android/location/i/c;->f:J

    sub-long v4, p1, v4

    iget-object v6, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v6, v6, Lcom/google/android/location/os/h$a;->c:J

    div-long/2addr v4, v6

    .line 328
    cmp-long v6, v4, v0

    if-ltz v6, :cond_13

    .line 331
    iget-object v0, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v0, v0, Lcom/google/android/location/os/h$a;->b:J

    mul-long/2addr v0, v4

    .line 332
    iget-wide v6, p0, Lcom/google/android/location/i/c;->f:J

    iget-object v8, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    iget-wide v8, v8, Lcom/google/android/location/os/h$a;->c:J

    mul-long/2addr v4, v8

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/location/i/c;->f:J
    :try_end_4f
    .catchall {:try_start_33 .. :try_end_4f} :catchall_50

    goto :goto_13

    .line 322
    :catchall_50
    move-exception v0

    monitor-exit p0

    throw v0

    .line 337
    :cond_53
    const/4 v0, 0x0

    goto :goto_2f
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .registers 10

    .prologue
    .line 357
    monitor-enter p0

    :try_start_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy/MM/dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 358
    const-string v1, "%s - current tokens: %d, last refill: %s, params: %s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/location/i/c;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/google/android/location/i/c;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Ljava/util/Date;

    iget-wide v5, p0, Lcom/google/android/location/i/c;->b:J

    iget-wide v7, p0, Lcom/google/android/location/i/c;->f:J

    add-long/2addr v5, v7

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/android/location/i/c;->d:Lcom/google/android/location/os/h$a;

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_34
    .catchall {:try_start_1 .. :try_end_34} :catchall_37

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 357
    :catchall_37
    move-exception v0

    monitor-exit p0

    throw v0
.end method
