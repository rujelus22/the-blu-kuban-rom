.class Lcom/google/android/location/f/b$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/f/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/f/b$a$a;
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/location/f/b$a$a;

.field private b:I

.field private c:F

.field private d:I

.field private e:I

.field private f:Lcom/google/android/location/f/c;


# direct methods
.method constructor <init>(IFII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297
    sget-object v0, Lcom/google/android/location/f/b$a$a;->b:Lcom/google/android/location/f/b$a$a;

    iput-object v0, p0, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    .line 298
    iput p1, p0, Lcom/google/android/location/f/b$a;->b:I

    .line 299
    iput p2, p0, Lcom/google/android/location/f/b$a;->c:F

    .line 300
    iput p3, p0, Lcom/google/android/location/f/b$a;->d:I

    .line 301
    iput p4, p0, Lcom/google/android/location/f/b$a;->e:I

    .line 302
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    .line 303
    return-void
.end method

.method constructor <init>(Lcom/google/android/location/f/c;)V
    .registers 3
    .parameter

    .prologue
    .line 309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310
    sget-object v0, Lcom/google/android/location/f/b$a$a;->a:Lcom/google/android/location/f/b$a$a;

    iput-object v0, p0, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    .line 311
    iput-object p1, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    .line 313
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/f/b$a;)Lcom/google/android/location/f/b$a$a;
    .registers 2
    .parameter

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    return-object v0
.end method

.method static a(Ljava/io/DataInputStream;)Lcom/google/android/location/f/b$a;
    .registers 6
    .parameter

    .prologue
    .line 338
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 339
    invoke-static {}, Lcom/google/android/location/f/b$a$a;->values()[Lcom/google/android/location/f/b$a$a;

    move-result-object v1

    aget-object v0, v1, v0

    .line 340
    sget-object v1, Lcom/google/android/location/f/b$a$a;->a:Lcom/google/android/location/f/b$a$a;

    if-ne v0, v1, :cond_18

    .line 341
    invoke-static {p0}, Lcom/google/android/location/f/c;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/f/c;

    move-result-object v1

    .line 342
    new-instance v0, Lcom/google/android/location/f/b$a;

    invoke-direct {v0, v1}, Lcom/google/android/location/f/b$a;-><init>(Lcom/google/android/location/f/c;)V

    .line 348
    :goto_17
    return-object v0

    .line 344
    :cond_18
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 345
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readFloat()F

    move-result v2

    .line 346
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 347
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    .line 348
    new-instance v0, Lcom/google/android/location/f/b$a;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/f/b$a;-><init>(IFII)V

    goto :goto_17
.end method

.method static synthetic b(Lcom/google/android/location/f/b$a;)Lcom/google/android/location/f/c;
    .registers 2
    .parameter

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/f/b$a;)I
    .registers 2
    .parameter

    .prologue
    .line 246
    iget v0, p0, Lcom/google/android/location/f/b$a;->b:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/location/f/b$a;)F
    .registers 2
    .parameter

    .prologue
    .line 246
    iget v0, p0, Lcom/google/android/location/f/b$a;->c:F

    return v0
.end method

.method static synthetic e(Lcom/google/android/location/f/b$a;)I
    .registers 2
    .parameter

    .prologue
    .line 246
    iget v0, p0, Lcom/google/android/location/f/b$a;->d:I

    return v0
.end method

.method static synthetic f(Lcom/google/android/location/f/b$a;)I
    .registers 2
    .parameter

    .prologue
    .line 246
    iget v0, p0, Lcom/google/android/location/f/b$a;->e:I

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 355
    if-ne p0, p1, :cond_5

    .line 365
    :cond_4
    :goto_4
    return v0

    .line 359
    :cond_5
    instance-of v2, p1, Lcom/google/android/location/f/b$a;

    if-nez v2, :cond_b

    move v0, v1

    .line 360
    goto :goto_4

    .line 363
    :cond_b
    check-cast p1, Lcom/google/android/location/f/b$a;

    .line 365
    iget-object v2, p0, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    iget-object v3, p1, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    if-ne v2, v3, :cond_35

    iget v2, p0, Lcom/google/android/location/f/b$a;->b:I

    iget v3, p1, Lcom/google/android/location/f/b$a;->b:I

    if-ne v2, v3, :cond_35

    iget v2, p0, Lcom/google/android/location/f/b$a;->c:F

    iget v3, p1, Lcom/google/android/location/f/b$a;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_35

    iget v2, p0, Lcom/google/android/location/f/b$a;->d:I

    iget v3, p1, Lcom/google/android/location/f/b$a;->d:I

    if-ne v2, v3, :cond_35

    iget v2, p0, Lcom/google/android/location/f/b$a;->e:I

    iget v3, p1, Lcom/google/android/location/f/b$a;->e:I

    if-ne v2, v3, :cond_35

    iget-object v2, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    if-nez v2, :cond_37

    iget-object v2, p1, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    if-eqz v2, :cond_4

    :cond_35
    move v0, v1

    goto :goto_4

    :cond_37
    iget-object v2, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    iget-object v3, p1, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    invoke-virtual {v2, v3}, Lcom/google/android/location/f/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_35

    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 375
    .line 376
    iget-object v0, p0, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    invoke-virtual {v0}, Lcom/google/android/location/f/b$a$a;->ordinal()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 377
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/location/f/b$a;->b:I

    add-int/2addr v0, v1

    .line 378
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/location/f/b$a;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 379
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/location/f/b$a;->d:I

    add-int/2addr v0, v1

    .line 380
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/location/f/b$a;->e:I

    add-int/2addr v0, v1

    .line 381
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    if-nez v0, :cond_29

    const/4 v0, 0x0

    :goto_27
    add-int/2addr v0, v1

    .line 382
    return v0

    .line 381
    :cond_29
    iget-object v0, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    invoke-virtual {v0}, Lcom/google/android/location/f/c;->hashCode()I

    move-result v0

    goto :goto_27
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/location/f/b$a;->a:Lcom/google/android/location/f/b$a$a;

    sget-object v1, Lcom/google/android/location/f/b$a$a;->a:Lcom/google/android/location/f/b$a$a;

    if-ne v0, v1, :cond_20

    .line 318
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Leaf: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/f/b$a;->f:Lcom/google/android/location/f/c;

    invoke-virtual {v1}, Lcom/google/android/location/f/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 320
    :goto_1f
    return-object v0

    :cond_20
    const-string v0, "[%d] <= %f (%d) : (%d)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/location/f/b$a;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/location/f/b$a;->c:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/location/f/b$a;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/location/f/b$a;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1f
.end method
