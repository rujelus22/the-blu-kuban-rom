.class public Lcom/google/android/location/j;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Lcom/google/android/location/i$a;)D
    .registers 10
    .parameter

    .prologue
    const-wide/high16 v6, 0x3ff0

    .line 62
    const-wide v0, 0x3fe6a0902de00d1bL

    const-wide v2, -0x403f10cb295e9e1bL

    iget v4, p1, Lcom/google/android/location/i$a;->a:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, -0x4065c91d14e3bcd3L

    iget v4, p1, Lcom/google/android/location/i$a;->b:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, -0x40aab367a0f9096cL

    iget v4, p1, Lcom/google/android/location/i$a;->c:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, 0x3fa1de69ad42c3caL

    iget v4, p1, Lcom/google/android/location/i$a;->d:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, -0x4048c7e28240b780L

    iget v4, p1, Lcom/google/android/location/i$a;->e:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, -0x407d916872b020c5L

    iget v4, p1, Lcom/google/android/location/i$a;->f:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    .line 69
    const-wide v2, 0x4005bf0a8b145769L

    neg-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    add-double/2addr v0, v6

    div-double v0, v6, v0

    return-wide v0
.end method


# virtual methods
.method public a(Lcom/google/android/location/i$a;)Lcom/google/android/location/e/u;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/i$a;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/location/j;->b(Lcom/google/android/location/i$a;)D

    move-result-wide v0

    .line 56
    const-wide/high16 v2, 0x3fe0

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_1a

    const/4 v2, 0x1

    .line 57
    :goto_b
    if-eqz v2, :cond_1c

    .line 58
    :goto_d
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    return-object v0

    .line 56
    :cond_1a
    const/4 v2, 0x0

    goto :goto_b

    .line 57
    :cond_1c
    const-wide/high16 v3, 0x3ff0

    sub-double v0, v3, v0

    goto :goto_d
.end method

.method public a(Ljava/util/List;)Lcom/google/android/location/e/u;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/location/i;

    invoke-direct {v0}, Lcom/google/android/location/i;-><init>()V

    .line 43
    invoke-virtual {v0, p1}, Lcom/google/android/location/i;->a(Ljava/util/List;)Lcom/google/android/location/i$a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/j;->a(Lcom/google/android/location/i$a;)Lcom/google/android/location/e/u;

    move-result-object v0

    return-object v0
.end method
