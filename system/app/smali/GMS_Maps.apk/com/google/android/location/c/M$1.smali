.class Lcom/google/android/location/c/M$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/M;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/M;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/M;)V
    .registers 2
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 38
    iget-object v1, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    monitor-enter v1

    .line 41
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    invoke-virtual {v0}, Lcom/google/android/location/c/M;->g()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 42
    monitor-exit v1

    .line 53
    :goto_c
    return-void

    .line 45
    :cond_d
    iget-object v0, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    invoke-static {v0}, Lcom/google/android/location/c/M;->a(Lcom/google/android/location/c/M;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    invoke-static {v0}, Lcom/google/android/location/c/M;->b(Lcom/google/android/location/c/M;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_34

    .line 49
    iget-object v2, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    invoke-static {v2}, Lcom/google/android/location/c/M;->c(Lcom/google/android/location/c/M;)Lcom/google/android/location/d/e;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/location/d/e;->a(Ljava/util/List;)V

    .line 50
    iget-object v2, p0, Lcom/google/android/location/c/M$1;->a:Lcom/google/android/location/c/M;

    invoke-virtual {v2}, Lcom/google/android/location/c/M;->f()Lcom/google/android/location/c/k;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/location/c/k;->a(Ljava/util/List;J)V

    .line 52
    :cond_34
    monitor-exit v1

    goto :goto_c

    :catchall_36
    move-exception v0

    monitor-exit v1
    :try_end_38
    .catchall {:try_start_3 .. :try_end_38} :catchall_36

    throw v0
.end method
