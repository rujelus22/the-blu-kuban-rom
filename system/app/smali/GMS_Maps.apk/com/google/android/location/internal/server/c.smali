.class Lcom/google/android/location/internal/server/c;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;
.implements Lcom/google/android/location/os/real/h$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/internal/server/c$a;
    }
.end annotation


# instance fields
.field a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private final c:Landroid/location/LocationManager;

.field private final d:Lcom/google/android/location/internal/d;

.field private e:Landroid/database/Cursor;

.field private f:Landroid/content/ContentQueryMap;

.field private g:Lcom/google/android/location/internal/server/c$a;

.field private h:Z

.field private i:Z

.field private final j:Ljava/lang/Object;

.field private k:Lcom/google/android/location/os/e;

.field private final l:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private m:Z

.field private n:Lcom/google/android/location/os/real/h;

.field private o:Lcom/google/android/location/b/e;

.field private p:J

.field private final q:Lcom/google/android/location/internal/server/b;

.field private r:Lcom/google/android/location/os/real/g;

.field private s:J


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const-wide/16 v3, -0x1

    const/16 v2, 0xa

    const/4 v1, 0x0

    .line 161
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 100
    iput-boolean v1, p0, Lcom/google/android/location/internal/server/c;->h:Z

    .line 103
    iput-boolean v1, p0, Lcom/google/android/location/internal/server/c;->i:Z

    .line 109
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    .line 116
    new-instance v0, Lcom/google/android/location/internal/server/c$1;

    invoke-direct {v0, p0, v2}, Lcom/google/android/location/internal/server/c$1;-><init>(Lcom/google/android/location/internal/server/c;I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->l:Ljava/util/LinkedHashMap;

    .line 125
    iput-boolean v1, p0, Lcom/google/android/location/internal/server/c;->m:Z

    .line 141
    iput-wide v3, p0, Lcom/google/android/location/internal/server/c;->p:J

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/os/real/g;

    .line 154
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    .line 155
    iput-wide v3, p0, Lcom/google/android/location/internal/server/c;->s:J

    .line 162
    iput-object p1, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    .line 163
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->c:Landroid/location/LocationManager;

    .line 164
    sget-object v0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/internal/e;

    invoke-static {v0, p1}, Lcom/google/android/location/internal/d;->a(Lcom/google/android/location/internal/e;Landroid/content/Context;)Lcom/google/android/location/internal/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->d:Lcom/google/android/location/internal/d;

    .line 165
    new-instance v0, Lcom/google/android/location/internal/server/b;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->d:Lcom/google/android/location/internal/d;

    iget v1, v1, Lcom/google/android/location/internal/d;->d:I

    invoke-direct {v0, v1}, Lcom/google/android/location/internal/server/b;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->q:Lcom/google/android/location/internal/server/b;

    .line 166
    return-void
.end method

.method private a(Lcom/google/android/location/e/o;)Landroid/location/Location;
    .registers 8
    .parameter

    .prologue
    const-wide v4, 0x416312d000000000L

    .line 752
    new-instance v0, Landroid/location/Location;

    const-string v1, "network"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 753
    iget-object v1, p1, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    .line 754
    iget v2, v1, Lcom/google/android/location/e/w;->a:I

    int-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 755
    iget v2, v1, Lcom/google/android/location/e/w;->b:I

    int-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 756
    iget v1, v1, Lcom/google/android/location/e/w;->c:I

    int-to-float v1, v1

    const/high16 v2, 0x447a

    div-float/2addr v1, v2

    .line 758
    const/high16 v2, 0x3f80

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    .line 759
    iget-wide v1, p1, Lcom/google/android/location/e/o;->e:J

    invoke-static {}, Lcom/google/android/location/os/real/h;->H()J

    move-result-wide v3

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setTime(J)V

    .line 760
    return-object v0
.end method

.method private a(Landroid/location/Location;Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;Z)Landroid/os/Bundle;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 765
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 766
    iget-object v1, p2, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    .line 767
    iget-object v2, p2, Lcom/google/android/location/e/t;->d:Lcom/google/android/location/e/g;

    if-ne v1, v2, :cond_26

    .line 768
    const-string v1, "networkLocationSource"

    const-string v2, "server"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    :cond_12
    :goto_12
    if-eqz p3, :cond_25

    sget-object v1, Lcom/google/android/location/e/B;->c:Lcom/google/android/location/e/B;

    if-eq p3, v1, :cond_25

    .line 801
    const-string v1, "travelState"

    invoke-virtual {p3}, Lcom/google/android/location/e/B;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    :cond_25
    return-object v0

    .line 771
    :cond_26
    if-eqz p4, :cond_35

    .line 772
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, p1, p2, v2, v3}, Lcom/google/android/location/internal/server/c;->a(Landroid/location/Location;Ljava/lang/Object;Ljava/lang/String;Z)[B

    move-result-object v2

    .line 774
    if-eqz v2, :cond_35

    .line 775
    const-string v3, "dbgProtoBuf"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 779
    :cond_35
    const-string v2, "networkLocationSource"

    const-string v3, "cached"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    iget-object v2, p2, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    if-ne v1, v2, :cond_48

    .line 782
    const-string v1, "networkLocationType"

    const-string v2, "cell"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_12

    .line 784
    :cond_48
    iget-object v2, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-ne v1, v2, :cond_12

    .line 785
    const-string v1, "networkLocationType"

    const-string v2, "wifi"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    iget-object v1, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v1, v1, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    if-eqz v1, :cond_6c

    iget-object v1, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v1, v1, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget-object v1, v1, Lcom/google/android/location/e/w;->f:Ljava/lang/String;

    if-eqz v1, :cond_6c

    .line 789
    const-string v1, "levelId"

    iget-object v2, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v2, v2, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget-object v2, v2, Lcom/google/android/location/e/w;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    :cond_6c
    iget-object v1, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v1, v1, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    if-eqz v1, :cond_12

    iget-object v1, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v1, v1, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget v1, v1, Lcom/google/android/location/e/w;->g:I

    const/high16 v2, -0x8000

    if-eq v1, v2, :cond_12

    .line 795
    const-string v1, "levelNumberE3"

    iget-object v2, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v2, v2, Lcom/google/android/location/e/D;->c:Lcom/google/android/location/e/w;

    iget v2, v2, Lcom/google/android/location/e/w;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_12
.end method

.method private a(J)V
    .registers 9
    .parameter

    .prologue
    .line 545
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/os/real/g;

    if-eqz v0, :cond_13

    invoke-static {}, Lcom/google/android/location/os/real/h;->F()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/os/real/g;

    invoke-virtual {v2}, Lcom/google/android/location/os/real/g;->f()J

    move-result-wide v2

    sub-long/2addr v0, v2

    cmp-long v0, v0, p1

    if-lez v0, :cond_51

    .line 550
    :cond_13
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->c:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 552
    if-eqz v0, :cond_51

    invoke-static {}, Lcom/google/android/location/os/real/h;->G()J

    move-result-wide v1

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    cmp-long v1, v1, p1

    if-gtz v1, :cond_51

    .line 556
    new-instance v1, Lcom/google/android/location/os/real/g;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-static {}, Lcom/google/android/location/os/real/h;->H()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/location/os/real/g;-><init>(Landroid/location/Location;JI)V

    iput-object v1, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/os/real/g;

    .line 558
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->l:Ljava/util/LinkedHashMap;

    new-instance v1, Ljava/lang/Long;

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/os/real/g;

    invoke-virtual {v2}, Lcom/google/android/location/os/real/g;->n()Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v2}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/os/real/g;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    :cond_51
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/internal/server/c;)V
    .registers 1
    .parameter

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->f()V

    return-void
.end method

.method private a(Z)V
    .registers 10
    .parameter

    .prologue
    .line 470
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->q:Lcom/google/android/location/internal/server/b;

    invoke-virtual {v0}, Lcom/google/android/location/internal/server/b;->a()I

    move-result v1

    .line 471
    if-nez p1, :cond_f

    int-to-long v2, v1

    iget-wide v4, p0, Lcom/google/android/location/internal/server/c;->p:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_18

    :cond_f
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    if-eqz v0, :cond_18

    .line 472
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/location/os/real/h;->a(IZ)V

    .line 475
    :cond_18
    invoke-static {}, Lcom/google/android/location/os/real/h;->F()J

    move-result-wide v2

    .line 477
    iget-wide v4, p0, Lcom/google/android/location/internal/server/c;->s:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_2a

    .line 478
    iput-wide v2, p0, Lcom/google/android/location/internal/server/c;->s:J

    .line 496
    :cond_26
    :goto_26
    int-to-long v0, v1

    iput-wide v0, p0, Lcom/google/android/location/internal/server/c;->p:J

    .line 497
    return-void

    .line 480
    :cond_2a
    iget-wide v4, p0, Lcom/google/android/location/internal/server/c;->s:J

    sub-long v4, v2, v4

    .line 481
    iput-wide v2, p0, Lcom/google/android/location/internal/server/c;->s:J

    .line 483
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 484
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 486
    if-nez v0, :cond_52

    .line 487
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v3, 0xa

    if-ge v0, v3, :cond_26

    .line 488
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_26

    .line 491
    :cond_52
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long v3, v4, v6

    .line 492
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_26
.end method

.method private final a(Landroid/content/Context;)Z
    .registers 4
    .parameter

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->c:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private final b(Landroid/content/Context;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 212
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "network_location_opt_in"

    const/4 v3, -0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/c;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v0, v1, :cond_f

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method private e()V
    .registers 5

    .prologue
    .line 222
    sget-object v0, Lcom/google/android/location/internal/e;->a:Lcom/google/android/location/internal/e;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/location/internal/d;->a(Lcom/google/android/location/internal/e;Landroid/content/Context;)Lcom/google/android/location/internal/d;

    move-result-object v0

    .line 223
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->d:Lcom/google/android/location/internal/d;

    .line 224
    iget v2, v0, Lcom/google/android/location/internal/d;->d:I

    iget-object v3, p0, Lcom/google/android/location/internal/server/c;->d:Lcom/google/android/location/internal/d;

    iget v3, v3, Lcom/google/android/location/internal/d;->d:I

    if-lt v2, v3, :cond_24

    .line 227
    :goto_12
    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/google/android/location/internal/server/c;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_27

    if-ne v0, v1, :cond_27

    .line 229
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    iget-object v1, v1, Lcom/google/android/location/internal/d;->e:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 236
    :goto_23
    return-void

    .line 224
    :cond_24
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->d:Lcom/google/android/location/internal/d;

    goto :goto_12

    .line 234
    :cond_27
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    iget-object v1, v1, Lcom/google/android/location/internal/d;->e:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_23
.end method

.method private f()V
    .registers 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 258
    iget-boolean v0, p0, Lcom/google/android/location/internal/server/c;->m:Z

    if-eqz v0, :cond_b7

    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/c;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b7

    move v0, v1

    .line 261
    :goto_f
    iget-object v3, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    if-eqz v3, :cond_ba

    iget-object v3, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/google/android/location/internal/server/c;->b(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_ba

    move v3, v1

    .line 264
    :goto_1c
    iget-object v4, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v4

    .line 267
    if-nez v3, :cond_2e

    :try_start_21
    iget-boolean v5, p0, Lcom/google/android/location/internal/server/c;->h:Z

    if-eq v5, v3, :cond_2e

    .line 269
    iput-boolean v3, p0, Lcom/google/android/location/internal/server/c;->h:Z

    .line 270
    iget-object v5, p0, Lcom/google/android/location/internal/server/c;->q:Lcom/google/android/location/internal/server/b;

    iget-object v6, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-virtual {v5, v6, v3}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Z)V

    .line 272
    :cond_2e
    monitor-exit v4
    :try_end_2f
    .catchall {:try_start_21 .. :try_end_2f} :catchall_bd

    .line 277
    if-eqz v3, :cond_c5

    iget-object v4, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    if-nez v4, :cond_c5

    .line 278
    iget-object v4, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v4

    .line 279
    :try_start_38
    iget-boolean v5, p0, Lcom/google/android/location/internal/server/c;->m:Z

    if-eqz v5, :cond_79

    .line 288
    sget-object v5, Lcom/google/android/location/internal/e;->a:Lcom/google/android/location/internal/e;

    iget-object v6, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-static {v5, v6}, Lcom/google/android/location/internal/d;->a(Lcom/google/android/location/internal/e;Landroid/content/Context;)Lcom/google/android/location/internal/d;

    move-result-object v5

    .line 290
    iget-object v5, v5, Lcom/google/android/location/internal/d;->a:Lcom/google/android/location/internal/e;

    sget-object v6, Lcom/google/android/location/internal/e;->c:Lcom/google/android/location/internal/e;

    if-eq v5, v6, :cond_c0

    .line 291
    :goto_4a
    new-instance v2, Lcom/google/android/location/os/real/h;

    iget-object v5, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/location/internal/server/c;->k:Lcom/google/android/location/os/e;

    invoke-direct {v2, v5, v6, p0, v1}, Lcom/google/android/location/os/real/h;-><init>(Landroid/content/Context;Lcom/google/android/location/os/e;Lcom/google/android/location/os/real/h$a;Z)V

    iput-object v2, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    .line 292
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    invoke-static {v1}, Lcom/google/android/location/b/e;->a(Lcom/google/android/location/os/i;)Lcom/google/android/location/b/e;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/b/e;

    .line 293
    new-instance v1, Lcom/google/android/location/q;

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    iget-object v5, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    invoke-virtual {v5}, Lcom/google/android/location/os/real/h;->D()Lcom/google/android/location/os/h;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    invoke-virtual {v6}, Lcom/google/android/location/os/real/h;->C()Lcom/google/android/location/i/a;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/b/e;

    invoke-direct {v1, v2, v5, v6, v7}, Lcom/google/android/location/q;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;Lcom/google/android/location/i/a;Lcom/google/android/location/b/e;)V

    .line 295
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/location/internal/server/c;->a(Z)V

    .line 296
    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->g()V

    .line 298
    :cond_79
    monitor-exit v4
    :try_end_7a
    .catchall {:try_start_38 .. :try_end_7a} :catchall_c2

    .line 313
    :cond_7a
    :goto_7a
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 316
    if-eqz v3, :cond_98

    :try_start_7f
    iget-boolean v2, p0, Lcom/google/android/location/internal/server/c;->h:Z

    if-eq v2, v3, :cond_98

    .line 318
    iput-boolean v3, p0, Lcom/google/android/location/internal/server/c;->h:Z

    .line 319
    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->q:Lcom/google/android/location/internal/server/b;

    iget-object v4, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-virtual {v2, v4, v3}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Z)V

    .line 320
    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.location.internal.server.ACTION_RESTARTED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 322
    :cond_98
    monitor-exit v1
    :try_end_99
    .catchall {:try_start_7f .. :try_end_99} :catchall_e7

    .line 325
    iget-boolean v1, p0, Lcom/google/android/location/internal/server/c;->h:Z

    if-nez v1, :cond_ab

    .line 326
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/location/os/real/h;->a(Landroid/content/Context;)V

    .line 327
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/b/e;

    if-eqz v1, :cond_ab

    .line 328
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->o:Lcom/google/android/location/b/e;

    invoke-virtual {v1}, Lcom/google/android/location/b/e;->c()V

    .line 333
    :cond_ab
    if-nez v0, :cond_b6

    .line 335
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->d:Lcom/google/android/location/internal/d;

    iget-object v1, v1, Lcom/google/android/location/internal/d;->e:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 337
    :cond_b6
    return-void

    :cond_b7
    move v0, v2

    .line 258
    goto/16 :goto_f

    :cond_ba
    move v3, v2

    .line 261
    goto/16 :goto_1c

    .line 272
    :catchall_bd
    move-exception v0

    :try_start_be
    monitor-exit v4
    :try_end_bf
    .catchall {:try_start_be .. :try_end_bf} :catchall_bd

    throw v0

    :cond_c0
    move v1, v2

    .line 290
    goto :goto_4a

    .line 298
    :catchall_c2
    move-exception v0

    :try_start_c3
    monitor-exit v4
    :try_end_c4
    .catchall {:try_start_c3 .. :try_end_c4} :catchall_c2

    throw v0

    .line 299
    :cond_c5
    if-nez v3, :cond_7a

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    if-eqz v1, :cond_7a

    .line 301
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    invoke-virtual {v1, v3}, Lcom/google/android/location/os/real/h;->a(Z)V

    .line 302
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/h;->E()V

    .line 306
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 307
    const/4 v2, 0x0

    :try_start_d9
    iput-object v2, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    .line 308
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/os/real/g;

    .line 309
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/google/android/location/internal/server/c;->p:J

    .line 310
    monitor-exit v1

    goto :goto_7a

    :catchall_e4
    move-exception v0

    monitor-exit v1
    :try_end_e6
    .catchall {:try_start_d9 .. :try_end_e6} :catchall_e4

    throw v0

    .line 322
    :catchall_e7
    move-exception v0

    :try_start_e8
    monitor-exit v1
    :try_end_e9
    .catchall {:try_start_e8 .. :try_end_e9} :catchall_e7

    throw v0
.end method

.method private g()V
    .registers 4

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    if-eqz v0, :cond_15

    .line 459
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->q:Lcom/google/android/location/internal/server/b;

    invoke-virtual {v1}, Lcom/google/android/location/internal/server/b;->c()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->q:Lcom/google/android/location/internal/server/b;

    invoke-virtual {v2}, Lcom/google/android/location/internal/server/b;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/os/real/h;->a(II)V

    .line 462
    :cond_15
    return-void
.end method


# virtual methods
.method public a(Landroid/location/Location;)Ljava/lang/Object;
    .registers 6
    .parameter

    .prologue
    .line 585
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 586
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->l:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 587
    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    throw v0
.end method

.method public a()V
    .registers 3

    .prologue
    .line 687
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 688
    const/4 v0, 0x2

    :try_start_4
    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 689
    monitor-exit v1

    .line 690
    return-void

    .line 689
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_4 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public a(Landroid/app/PendingIntent;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 447
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 449
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->q:Lcom/google/android/location/internal/server/b;

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {p2, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {v0, v2, p1, v3}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Landroid/app/PendingIntent;I)V

    .line 450
    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->g()V

    .line 451
    monitor-exit v1

    .line 452
    return-void

    .line 451
    :catchall_14
    move-exception v0

    monitor-exit v1
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    throw v0
.end method

.method public a(Landroid/app/PendingIntent;IZ)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 435
    iget-object v6, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v6

    .line 437
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->q:Lcom/google/android/location/internal/server/b;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    const/4 v2, 0x5

    invoke-static {p2, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    const/4 v4, 0x0

    move-object v2, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Landroid/app/PendingIntent;ILandroid/location/Location;Z)V

    .line 439
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/c;->a(Z)V

    .line 440
    monitor-exit v6

    .line 441
    return-void

    .line 440
    :catchall_18
    move-exception v0

    monitor-exit v6
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    throw v0
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .registers 5
    .parameter

    .prologue
    .line 741
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 742
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->q:Lcom/google/android/location/internal/server/b;

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-virtual {v0, v2, p1}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Lcom/google/android/location/clientlib/NlpActivity;)V

    .line 743
    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->g()V

    .line 744
    monitor-exit v1

    .line 745
    return-void

    .line 744
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method a(Lcom/google/android/location/e/t;)V
    .registers 3
    .parameter

    .prologue
    .line 596
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/h;->a(Lcom/google/android/location/e/t;)V

    .line 597
    return-void
.end method

.method public a(Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 698
    iget-object v0, p1, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/c;->a(Lcom/google/android/location/e/o;)Landroid/location/Location;

    move-result-object v0

    .line 701
    new-instance v1, Landroid/location/Location;

    invoke-direct {v1, v0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    .line 702
    const/4 v2, 0x1

    invoke-direct {p0, v0, p1, p2, v2}, Lcom/google/android/location/internal/server/c;->a(Landroid/location/Location;Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;Z)Landroid/os/Bundle;

    move-result-object v2

    .line 703
    invoke-direct {p0, v0, p1, p2, v3}, Lcom/google/android/location/internal/server/c;->a(Landroid/location/Location;Lcom/google/android/location/e/t;Lcom/google/android/location/e/B;Z)Landroid/os/Bundle;

    move-result-object v3

    .line 704
    invoke-virtual {v0, v3}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    .line 705
    invoke-virtual {v1, v2}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    .line 707
    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v2

    .line 708
    :try_start_1e
    iget-object v3, p0, Lcom/google/android/location/internal/server/c;->q:Lcom/google/android/location/internal/server/b;

    iget-object v4, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-virtual {v3, v4, v0, v1}, Lcom/google/android/location/internal/server/b;->a(Landroid/content/Context;Landroid/location/Location;Landroid/location/Location;)Ljava/util/List;

    move-result-object v3

    .line 710
    new-instance v4, Lcom/google/android/location/os/real/g;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v5

    invoke-static {}, Lcom/google/android/location/os/real/h;->H()J

    move-result-wide v7

    sub-long/2addr v5, v7

    const/4 v0, 0x0

    invoke-direct {v4, v1, v5, v6, v0}, Lcom/google/android/location/os/real/g;-><init>(Landroid/location/Location;JI)V

    iput-object v4, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/os/real/g;

    .line 712
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->l:Ljava/util/LinkedHashMap;

    new-instance v1, Ljava/lang/Long;

    iget-object v4, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/os/real/g;

    invoke-virtual {v4}, Lcom/google/android/location/os/real/g;->n()Landroid/location/Location;

    move-result-object v4

    invoke-virtual {v4}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 715
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/c;->a(Z)V

    .line 726
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_53
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_67

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 727
    const/4 v3, 0x0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/location/internal/server/c;->a(ZLjava/lang/String;)V

    goto :goto_53

    .line 729
    :catchall_64
    move-exception v0

    monitor-exit v2
    :try_end_66
    .catchall {:try_start_1e .. :try_end_66} :catchall_64

    throw v0

    :cond_67
    :try_start_67
    monitor-exit v2
    :try_end_68
    .catchall {:try_start_67 .. :try_end_68} :catchall_64

    .line 730
    return-void
.end method

.method public a(Lcom/google/android/location/internal/ILocationListener;)V
    .registers 4
    .parameter

    .prologue
    .line 568
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 570
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->q:Lcom/google/android/location/internal/server/b;

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/server/b;->a(Lcom/google/android/location/internal/ILocationListener;)V

    .line 571
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/c;->a(Z)V

    .line 572
    monitor-exit v1

    .line 573
    return-void

    .line 572
    :catchall_e
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    throw v0
.end method

.method public a(Lcom/google/android/location/internal/ILocationListener;Ljava/lang/String;II)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 408
    mul-int/lit16 v0, p4, 0x3e8

    int-to-long v0, v0

    .line 409
    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v2

    .line 411
    const/4 v3, -0x1

    if-eq p4, v3, :cond_28

    .line 412
    :try_start_9
    invoke-direct {p0, v0, v1}, Lcom/google/android/location/internal/server/c;->a(J)V

    .line 413
    iget-object v3, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/os/real/g;

    if-eqz v3, :cond_28

    invoke-static {}, Lcom/google/android/location/os/real/h;->F()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/os/real/g;

    invoke-virtual {v5}, Lcom/google/android/location/os/real/g;->f()J
    :try_end_19
    .catchall {:try_start_9 .. :try_end_19} :catchall_3b

    move-result-wide v5

    sub-long/2addr v3, v5

    cmp-long v0, v3, v0

    if-gtz v0, :cond_28

    .line 418
    :try_start_1f
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/os/real/g;

    invoke-virtual {v0}, Lcom/google/android/location/os/real/g;->n()Landroid/location/Location;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/location/internal/ILocationListener;->onLocationChanged(Landroid/location/Location;)V
    :try_end_28
    .catchall {:try_start_1f .. :try_end_28} :catchall_3b
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_28} :catch_38

    .line 425
    :cond_28
    :try_start_28
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->q:Lcom/google/android/location/internal/server/b;

    const/4 v1, 0x5

    invoke-static {p3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/location/internal/server/b;->a(Lcom/google/android/location/internal/ILocationListener;Ljava/lang/String;I)V

    .line 426
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/c;->a(Z)V

    .line 427
    monitor-exit v2

    .line 428
    :goto_37
    return-void

    .line 419
    :catch_38
    move-exception v0

    .line 421
    monitor-exit v2

    goto :goto_37

    .line 427
    :catchall_3b
    move-exception v0

    monitor-exit v2
    :try_end_3d
    .catchall {:try_start_28 .. :try_end_3d} :catchall_3b

    throw v0
.end method

.method public a(Lcom/google/android/location/os/e;)V
    .registers 4
    .parameter

    .prologue
    .line 170
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 171
    :try_start_3
    iput-object p1, p0, Lcom/google/android/location/internal/server/c;->k:Lcom/google/android/location/os/e;

    .line 172
    monitor-exit v1

    .line 173
    return-void

    .line 172
    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    throw v0
.end method

.method public a(Ljava/io/PrintWriter;)V
    .registers 12
    .parameter

    .prologue
    .line 503
    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v2

    .line 504
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NLP-Period is currently "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lcom/google/android/location/internal/server/c;->p:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 506
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_25
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_90

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 507
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 509
    iget-wide v4, p0, Lcom/google/android/location/internal/server/c;->p:J

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_54

    .line 511
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {}, Lcom/google/android/location/os/real/h;->F()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/location/internal/server/c;->s:J

    sub-long/2addr v6, v8

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 514
    :cond_54
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NLP-Period interval "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const v6, 0x7fffffff

    if-ne v5, v6, :cond_6a

    const-string v0, "<no-client>"

    :cond_6a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", duration was "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_25

    .line 519
    :catchall_8d
    move-exception v0

    monitor-exit v2
    :try_end_8f
    .catchall {:try_start_3 .. :try_end_8f} :catchall_8d

    throw v0

    :cond_90
    :try_start_90
    monitor-exit v2
    :try_end_91
    .catchall {:try_start_90 .. :try_end_91} :catchall_8d

    .line 520
    return-void
.end method

.method public a(Ljava/text/Format;Ljava/io/PrintWriter;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 528
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 529
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    if-eqz v0, :cond_14

    .line 530
    const-string v0, "RealOs stats:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 531
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/h;->a(Ljava/text/Format;Ljava/io/PrintWriter;)V

    .line 532
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 534
    :cond_14
    monitor-exit v1

    .line 535
    return-void

    .line 534
    :catchall_16
    move-exception v0

    monitor-exit v1
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    throw v0
.end method

.method public a(ZLjava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 815
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    if-eqz v0, :cond_9

    .line 816
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/h;->a(ZLjava/lang/String;)V

    .line 818
    :cond_9
    return-void
.end method

.method a(Landroid/location/Location;Ljava/lang/Object;Ljava/lang/String;Z)[B
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 620
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 621
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->n:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 623
    if-eqz p3, :cond_14

    .line 624
    const/4 v2, 0x5

    invoke-virtual {v1, v2, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 628
    :cond_14
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/j/a;->Q:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 629
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 630
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 631
    const/4 v3, 0x3

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 633
    :cond_2e
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/android/location/j/a;->x:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 634
    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    const-wide v7, 0x416312d000000000L

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 635
    const/4 v4, 0x2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    const-wide v7, 0x416312d000000000L

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 636
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 639
    if-nez p2, :cond_70

    .line 640
    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 672
    :cond_5e
    :goto_5e
    const/4 v2, 0x7

    :try_start_5f
    invoke-virtual {v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addBytes(I[B)V

    .line 673
    const/4 v1, 0x6

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 674
    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B
    :try_end_6e
    .catch Ljava/io/IOException; {:try_start_5f .. :try_end_6e} :catch_c6

    move-result-object v0

    .line 677
    :goto_6f
    return-object v0

    .line 642
    :cond_70
    instance-of v2, p2, Lcom/google/android/location/e/t;

    if-eqz v2, :cond_b2

    .line 643
    check-cast p2, Lcom/google/android/location/e/t;

    .line 644
    iget-object v2, p2, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v3, p2, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    if-ne v2, v3, :cond_a6

    .line 645
    const/4 v2, 0x6

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 649
    :cond_81
    :goto_81
    invoke-static {}, Lcom/google/android/location/os/real/h;->H()J

    move-result-wide v2

    .line 650
    iget-object v4, p2, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    if-eqz v4, :cond_92

    .line 651
    iget-object v4, p2, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    iget-object v4, v4, Lcom/google/android/location/e/c;->a:Lcom/google/android/location/e/f;

    .line 652
    if-eqz v4, :cond_92

    .line 653
    invoke-virtual {v4, v0, v2, v3, p4}, Lcom/google/android/location/e/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;JZ)V

    .line 656
    :cond_92
    iget-object v4, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-eqz v4, :cond_5e

    .line 657
    iget-object v4, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    iget-object v4, v4, Lcom/google/android/location/e/D;->a:Lcom/google/android/location/e/E;

    .line 658
    if-eqz v4, :cond_5e

    .line 659
    const/4 v5, 0x0

    invoke-virtual {v4, v2, v3, v5}, Lcom/google/android/location/e/E;->a(JZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 660
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_5e

    .line 646
    :cond_a6
    iget-object v2, p2, Lcom/google/android/location/e/t;->a:Lcom/google/android/location/e/o;

    iget-object v3, p2, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    if-ne v2, v3, :cond_81

    .line 647
    const/4 v2, 0x6

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_81

    .line 663
    :cond_b2
    instance-of v2, p2, Lcom/google/android/location/e/o;

    if-eqz v2, :cond_bc

    .line 664
    const/4 v2, 0x6

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_5e

    .line 665
    :cond_bc
    instance-of v2, p2, Landroid/location/Location;

    if-eqz v2, :cond_5e

    .line 666
    const/4 v2, 0x6

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_5e

    .line 675
    :catch_c6
    move-exception v0

    .line 677
    const/4 v0, 0x0

    goto :goto_6f
.end method

.method public b()V
    .registers 9

    .prologue
    .line 345
    iget-object v6, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v6

    .line 346
    :try_start_3
    invoke-static {p0}, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;->addListener(Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;)V

    .line 347
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/c;->m:Z

    .line 348
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    if-nez v0, :cond_46

    .line 351
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 352
    sget-object v1, Lcom/google/android/gsf/c;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "(name=?)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "network_location_opt_in"

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    .line 358
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_46

    .line 359
    new-instance v0, Landroid/content/ContentQueryMap;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    const-string v2, "name"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3, p0}, Landroid/content/ContentQueryMap;-><init>(Landroid/database/Cursor;Ljava/lang/String;ZLandroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->f:Landroid/content/ContentQueryMap;

    .line 361
    new-instance v0, Lcom/google/android/location/internal/server/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/internal/server/c$a;-><init>(Lcom/google/android/location/internal/server/c;Lcom/google/android/location/internal/server/c$1;)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->g:Lcom/google/android/location/internal/server/c$a;

    .line 362
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->f:Landroid/content/ContentQueryMap;

    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->g:Lcom/google/android/location/internal/server/c$a;

    invoke-virtual {v0, v1}, Landroid/content/ContentQueryMap;->addObserver(Ljava/util/Observer;)V

    .line 367
    :cond_46
    const/4 v0, 0x1

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 368
    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->e()V

    .line 369
    monitor-exit v6

    .line 370
    return-void

    .line 369
    :catchall_53
    move-exception v0

    monitor-exit v6
    :try_end_55
    .catchall {:try_start_3 .. :try_end_55} :catchall_53

    throw v0
.end method

.method public c()V
    .registers 4

    .prologue
    .line 379
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 380
    :try_start_3
    invoke-static {p0}, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;->removeListener(Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;)V

    .line 381
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/c;->m:Z

    .line 382
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_27

    .line 385
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->f:Landroid/content/ContentQueryMap;

    iget-object v2, p0, Lcom/google/android/location/internal/server/c;->g:Lcom/google/android/location/internal/server/c$a;

    invoke-virtual {v0, v2}, Landroid/content/ContentQueryMap;->deleteObserver(Ljava/util/Observer;)V

    .line 386
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->f:Landroid/content/ContentQueryMap;

    invoke-virtual {v0}, Landroid/content/ContentQueryMap;->close()V

    .line 387
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 388
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->g:Lcom/google/android/location/internal/server/c$a;

    .line 389
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->f:Landroid/content/ContentQueryMap;

    .line 390
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/internal/server/c;->e:Landroid/database/Cursor;

    .line 392
    :cond_27
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    if-eqz v0, :cond_31

    .line 393
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->n:Lcom/google/android/location/os/real/h;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/h;->a(Z)V

    .line 395
    :cond_31
    const/4 v0, 0x1

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 396
    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->e()V

    .line 397
    monitor-exit v1

    .line 398
    return-void

    .line 397
    :catchall_3e
    move-exception v0

    monitor-exit v1
    :try_end_40
    .catchall {:try_start_3 .. :try_end_40} :catchall_3e

    throw v0
.end method

.method public d()Lcom/google/android/location/os/g;
    .registers 3

    .prologue
    .line 734
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 735
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->r:Lcom/google/android/location/os/real/g;

    monitor-exit v1

    return-object v0

    .line 736
    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/google/android/location/internal/server/c;->i:Z

    if-nez v0, :cond_c

    .line 178
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/c;->i:Z

    .line 179
    iget-object v0, p0, Lcom/google/android/location/internal/server/c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/h;->b(Landroid/content/Context;)V

    .line 181
    :cond_c
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_22

    .line 193
    :goto_11
    return-void

    .line 183
    :pswitch_12
    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->f()V

    goto :goto_11

    .line 187
    :pswitch_16
    iget-object v1, p0, Lcom/google/android/location/internal/server/c;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 188
    :try_start_19
    invoke-direct {p0}, Lcom/google/android/location/internal/server/c;->e()V

    .line 189
    monitor-exit v1

    goto :goto_11

    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_19 .. :try_end_20} :catchall_1e

    throw v0

    .line 181
    nop

    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_12
        :pswitch_16
    .end packed-switch
.end method
