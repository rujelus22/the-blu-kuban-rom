.class public Lcom/google/android/location/c/H;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private final c:Z


# direct methods
.method constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput v0, p0, Lcom/google/android/location/c/H;->a:I

    .line 29
    iput v0, p0, Lcom/google/android/location/c/H;->b:I

    .line 39
    iput-boolean p1, p0, Lcom/google/android/location/c/H;->c:Z

    .line 40
    return-void
.end method

.method static a(Ljava/lang/String;)Lcom/google/android/location/c/H;
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 48
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-static {p0}, Lcom/google/android/location/c/H;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 53
    :try_start_10
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 54
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 55
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 57
    const/4 v6, 0x2

    if-ne v3, v6, :cond_54

    .line 58
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v0, :cond_42

    .line 60
    :goto_35
    new-instance v1, Lcom/google/android/location/c/H;

    invoke-direct {v1, v0}, Lcom/google/android/location/c/H;-><init>(Z)V

    .line 61
    iput v4, v1, Lcom/google/android/location/c/H;->a:I

    .line 62
    iput v5, v1, Lcom/google/android/location/c/H;->b:I
    :try_end_3e
    .catchall {:try_start_10 .. :try_end_3e} :catchall_4f
    .catch Ljava/lang/NumberFormatException; {:try_start_10 .. :try_end_3e} :catch_44

    .line 66
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 68
    return-object v1

    :cond_42
    move v0, v1

    .line 58
    goto :goto_35

    .line 63
    :catch_44
    move-exception v0

    .line 64
    :try_start_45
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4f
    .catchall {:try_start_45 .. :try_end_4f} :catchall_4f

    .line 66
    :catchall_4f
    move-exception v0

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    throw v0

    :cond_54
    move v0, v1

    goto :goto_35
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    const/16 v5, 0x8

    .line 91
    .line 92
    invoke-virtual {p1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    move v1, v0

    .line 93
    :goto_8
    if-ge v1, v2, :cond_1f

    .line 94
    invoke-virtual {p1, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 95
    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_1c

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    if-nez v3, :cond_1c

    .line 97
    add-int/lit8 v0, v0, 0x1

    .line 93
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 100
    :cond_1f
    return v0
.end method

.method private static c(Ljava/lang/String;)Ljava/io/File;
    .registers 3
    .parameter

    .prologue
    .line 131
    new-instance v0, Ljava/io/File;

    const-string v1, "sessionSummary"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a()I
    .registers 2

    .prologue
    .line 119
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/location/c/H;->a:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 7
    .parameter

    .prologue
    .line 77
    monitor-enter p0

    const/4 v0, 0x4

    :try_start_2
    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 78
    if-lez v1, :cond_34

    .line 79
    const/4 v0, 0x0

    :goto_9
    if-ge v0, v1, :cond_34

    .line 80
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 81
    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_31

    .line 82
    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 83
    iget v3, p0, Lcom/google/android/location/c/H;->a:I

    invoke-direct {p0, v2}, Lcom/google/android/location/c/H;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/location/c/H;->a:I

    .line 84
    iget v3, p0, Lcom/google/android/location/c/H;->b:I

    const/4 v4, 0x7

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/location/c/H;->b:I
    :try_end_31
    .catchall {:try_start_2 .. :try_end_31} :catchall_36

    .line 79
    :cond_31
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 88
    :cond_34
    monitor-exit p0

    return-void

    .line 77
    :catchall_36
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()I
    .registers 2

    .prologue
    .line 123
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/location/c/H;->b:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 110
    monitor-enter p0

    :try_start_1
    new-instance v1, Ljava/io/PrintWriter;

    invoke-static {p1}, Lcom/google/android/location/c/H;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/File;)V

    .line 111
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 112
    iget v0, p0, Lcom/google/android/location/c/H;->a:I

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 113
    iget v0, p0, Lcom/google/android/location/c/H;->b:I

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 114
    iget-boolean v0, p0, Lcom/google/android/location/c/H;->c:Z

    if-eqz v0, :cond_26

    const-string v0, "1"

    :goto_1e
    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 115
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V
    :try_end_24
    .catchall {:try_start_1 .. :try_end_24} :catchall_29

    .line 116
    monitor-exit p0

    return-void

    .line 114
    :cond_26
    :try_start_26
    const-string v0, "0"
    :try_end_28
    .catchall {:try_start_26 .. :try_end_28} :catchall_29

    goto :goto_1e

    .line 110
    :catchall_29
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()Z
    .registers 2

    .prologue
    .line 127
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/H;->c:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SessionSummary [gpsCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/c/H;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", wifiScanCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/c/H;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", forceUpload="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/c/H;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
