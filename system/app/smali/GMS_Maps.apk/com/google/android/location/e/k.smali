.class public Lcom/google/android/location/e/k;
.super Lcom/google/android/location/e/x;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/location/e/x",
        "<",
        "Lcom/google/android/location/e/j;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/location/j/a;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {p0, v0}, Lcom/google/android/location/e/x;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 28
    return-void
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x4

    .line 78
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 79
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 81
    :goto_b
    return v0

    :cond_c
    const/16 v0, 0x3e8

    goto :goto_b
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/e/j;
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x3

    const/4 v0, 0x0

    .line 43
    if-nez p1, :cond_5

    .line 73
    :cond_4
    :goto_4
    return-object v0

    .line 47
    :cond_5
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 51
    const/4 v2, 0x1

    :try_start_a
    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/e/r;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Map;

    move-result-object v2

    .line 54
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/e/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/f/a;

    move-result-object v3

    .line 56
    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/location/e/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/f/a;

    move-result-object v4

    .line 59
    if-eqz v3, :cond_4

    if-eqz v4, :cond_4

    .line 63
    new-instance v5, Lcom/google/android/location/e/z;

    invoke-direct {v5, v2, v3}, Lcom/google/android/location/e/z;-><init>(Ljava/util/Map;Lcom/google/android/location/f/a;)V

    .line 65
    new-instance v3, Lcom/google/android/location/e/z;

    invoke-direct {v3, v2, v4}, Lcom/google/android/location/e/z;-><init>(Ljava/util/Map;Lcom/google/android/location/f/a;)V

    .line 68
    invoke-direct {p0, v1}, Lcom/google/android/location/e/k;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v2

    .line 70
    new-instance v1, Lcom/google/android/location/e/a;

    invoke-direct {v1, v5, v3, v2}, Lcom/google/android/location/e/a;-><init>(Lcom/google/android/location/e/z;Lcom/google/android/location/e/z;I)V
    :try_end_3b
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_3b} :catch_3d

    move-object v0, v1

    goto :goto_4

    .line 71
    :catch_3d
    move-exception v1

    goto :goto_4
.end method

.method public synthetic b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/google/android/location/e/k;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/e/j;

    move-result-object v0

    return-object v0
.end method
