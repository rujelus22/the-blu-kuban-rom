.class public Lcom/google/android/location/c/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/location/k/a/c;

.field private final b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private final e:Lcom/google/android/location/c/a;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/location/c/a;Lcom/google/android/location/k/a/c;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/c/c;->c:I

    .line 59
    iput-object p1, p0, Lcom/google/android/location/c/c;->b:Ljava/lang/String;

    .line 60
    iput-object p2, p0, Lcom/google/android/location/c/c;->e:Lcom/google/android/location/c/a;

    .line 61
    invoke-static {p3}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/c;->a:Lcom/google/android/location/k/a/c;

    .line 62
    return-void
.end method

.method private a()V
    .registers 2

    .prologue
    .line 117
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/c/c;->c:I

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/c;->d:Ljava/lang/String;

    .line 119
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/c/D$a;
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 70
    monitor-enter p0

    const/4 v4, 0x6

    :try_start_6
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    .line 72
    iget-object v5, p0, Lcom/google/android/location/c/c;->d:Ljava/lang/String;

    if-nez v5, :cond_1a

    .line 74
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/location/c/c;->d:Ljava/lang/String;

    move v3, v1

    .line 77
    :cond_1a
    new-instance v4, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/c/c;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/location/c/c;->d:Ljava/lang/String;

    invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_23
    .catchall {:try_start_6 .. :try_end_23} :catchall_a1

    .line 80
    :try_start_23
    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_29
    .catchall {:try_start_23 .. :try_end_29} :catchall_97
    .catch Ljava/io/FileNotFoundException; {:try_start_23 .. :try_end_29} :catch_6c
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_29} :catch_80

    .line 81
    :try_start_29
    new-instance v5, Ljava/io/DataOutputStream;

    invoke-direct {v5, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 82
    if-eqz v3, :cond_3c

    .line 83
    iget-object v0, p0, Lcom/google/android/location/c/c;->e:Lcom/google/android/location/c/a;

    invoke-virtual {v0}, Lcom/google/android/location/c/a;->a()Z

    move-result v0

    if-eqz v0, :cond_6a

    move v0, v2

    :goto_39
    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 86
    :cond_3c
    iget-object v0, p0, Lcom/google/android/location/c/c;->e:Lcom/google/android/location/c/a;

    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/c/L;->a([B)[B

    move-result-object v2

    invoke-virtual {v0, v5, v2}, Lcom/google/android/location/c/a;->a(Ljava/io/DataOutputStream;[B)V

    .line 89
    iget v0, p0, Lcom/google/android/location/c/c;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/c/c;->c:I

    .line 90
    iget v0, p0, Lcom/google/android/location/c/c;->c:I

    const/16 v2, 0x32

    if-lt v0, v2, :cond_58

    .line 92
    invoke-direct {p0}, Lcom/google/android/location/c/c;->a()V

    .line 94
    :cond_58
    new-instance v0, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x1

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_63
    .catchall {:try_start_29 .. :try_end_63} :catchall_ac
    .catch Ljava/io/FileNotFoundException; {:try_start_29 .. :try_end_63} :catch_b4
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_63} :catch_b1

    .line 104
    if-eqz v1, :cond_68

    .line 105
    :try_start_65
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_68
    .catchall {:try_start_65 .. :try_end_68} :catchall_a1
    .catch Ljava/io/IOException; {:try_start_65 .. :try_end_68} :catch_a4

    .line 109
    :cond_68
    :goto_68
    monitor-exit p0

    return-object v0

    .line 83
    :cond_6a
    const/4 v0, 0x2

    goto :goto_39

    .line 95
    :catch_6c
    move-exception v1

    move-object v2, v0

    .line 97
    :goto_6e
    :try_start_6e
    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v3, 0x0

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    const-string v4, "File not found."

    invoke-direct {v1, v3, v0, v4}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_79
    .catchall {:try_start_6e .. :try_end_79} :catchall_ae

    .line 104
    if-eqz v2, :cond_7e

    .line 105
    :try_start_7b
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7e
    .catchall {:try_start_7b .. :try_end_7e} :catchall_a1
    .catch Ljava/io/IOException; {:try_start_7b .. :try_end_7e} :catch_a6

    :cond_7e
    :goto_7e
    move-object v0, v1

    .line 109
    goto :goto_68

    .line 98
    :catch_80
    move-exception v1

    move-object v2, v0

    .line 100
    :goto_82
    :try_start_82
    invoke-direct {p0}, Lcom/google/android/location/c/c;->a()V

    .line 101
    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v3, 0x0

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    const-string v4, "Failed to write data to file"

    invoke-direct {v1, v3, v0, v4}, Lcom/google/android/location/c/D$a;-><init>(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_90
    .catchall {:try_start_82 .. :try_end_90} :catchall_ae

    .line 104
    if-eqz v2, :cond_95

    .line 105
    :try_start_92
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_95
    .catchall {:try_start_92 .. :try_end_95} :catchall_a1
    .catch Ljava/io/IOException; {:try_start_92 .. :try_end_95} :catch_a8

    :cond_95
    :goto_95
    move-object v0, v1

    .line 109
    goto :goto_68

    .line 103
    :catchall_97
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 104
    :goto_9b
    if-eqz v1, :cond_a0

    .line 105
    :try_start_9d
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_a0
    .catchall {:try_start_9d .. :try_end_a0} :catchall_a1
    .catch Ljava/io/IOException; {:try_start_9d .. :try_end_a0} :catch_aa

    .line 109
    :cond_a0
    :goto_a0
    :try_start_a0
    throw v0
    :try_end_a1
    .catchall {:try_start_a0 .. :try_end_a1} :catchall_a1

    .line 70
    :catchall_a1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 107
    :catch_a4
    move-exception v1

    goto :goto_68

    :catch_a6
    move-exception v0

    goto :goto_7e

    :catch_a8
    move-exception v0

    goto :goto_95

    :catch_aa
    move-exception v1

    goto :goto_a0

    .line 103
    :catchall_ac
    move-exception v0

    goto :goto_9b

    :catchall_ae
    move-exception v0

    move-object v1, v2

    goto :goto_9b

    .line 98
    :catch_b1
    move-exception v0

    move-object v2, v1

    goto :goto_82

    .line 95
    :catch_b4
    move-exception v0

    move-object v2, v1

    goto :goto_6e
.end method
