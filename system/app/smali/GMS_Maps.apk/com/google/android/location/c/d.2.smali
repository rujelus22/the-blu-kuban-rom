.class Lcom/google/android/location/c/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/d$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/c/d$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 11

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    const-string v0, "com.google.android.apps.maps.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/c/d;->a:Ljava/util/regex/Pattern;

    .line 34
    new-instance v0, Lcom/google/android/location/c/d$a;

    const-string v1, "com.google.android.apps.modis"

    sget-object v4, Lcom/google/android/location/c/F;->k:Ljava/util/Set;

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/c/d$a;-><init>(Ljava/lang/String;ZZLjava/util/Set;Z)V

    .line 43
    new-instance v4, Lcom/google/android/location/c/d$a;

    const-string v5, "com.google.android.apps.maps"

    sget-object v8, Lcom/google/android/location/c/F;->k:Ljava/util/Set;

    move v6, v2

    move v7, v3

    move v9, v2

    invoke-direct/range {v4 .. v9}, Lcom/google/android/location/c/d$a;-><init>(Ljava/lang/String;ZZLjava/util/Set;Z)V

    .line 52
    const/16 v1, 0x8

    new-array v1, v1, [Lcom/google/android/location/c/F;

    sget-object v5, Lcom/google/android/location/c/F;->a:Lcom/google/android/location/c/F;

    aput-object v5, v1, v2

    sget-object v5, Lcom/google/android/location/c/F;->b:Lcom/google/android/location/c/F;

    aput-object v5, v1, v3

    const/4 v5, 0x2

    sget-object v6, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    aput-object v6, v1, v5

    const/4 v5, 0x3

    sget-object v6, Lcom/google/android/location/c/F;->g:Lcom/google/android/location/c/F;

    aput-object v6, v1, v5

    const/4 v5, 0x4

    sget-object v6, Lcom/google/android/location/c/F;->h:Lcom/google/android/location/c/F;

    aput-object v6, v1, v5

    const/4 v5, 0x5

    sget-object v6, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    aput-object v6, v1, v5

    const/4 v5, 0x6

    sget-object v6, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    aput-object v6, v1, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/google/android/location/c/F;->f:Lcom/google/android/location/c/F;

    aput-object v6, v1, v5

    invoke-static {v1}, Lcom/google/android/location/c/F;->a([Lcom/google/android/location/c/F;)Ljava/util/Set;

    move-result-object v9

    .line 56
    new-instance v5, Lcom/google/android/location/c/d$a;

    const-string v6, "com.google.location.lbs.collectionlib"

    move v7, v3

    move v8, v2

    move v10, v3

    invoke-direct/range {v5 .. v10}, Lcom/google/android/location/c/d$a;-><init>(Ljava/lang/String;ZZLjava/util/Set;Z)V

    .line 64
    invoke-static {}, Lcom/google/android/location/c/L;->d()Ljava/util/Map;

    move-result-object v1

    .line 65
    invoke-static {v0}, Lcom/google/android/location/c/d$a;->a(Lcom/google/android/location/c/d$a;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static {v4}, Lcom/google/android/location/c/d$a;->a(Lcom/google/android/location/c/d$a;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    invoke-static {v5}, Lcom/google/android/location/c/d$a;->a(Lcom/google/android/location/c/d$a;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/c/d;->b:Ljava/util/Map;

    .line 69
    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/google/android/location/c/d$a;
    .registers 5
    .parameter

    .prologue
    .line 115
    sget-object v0, Lcom/google/android/location/c/d;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 118
    const-string p0, "com.google.android.apps.maps"

    .line 120
    :cond_e
    sget-object v0, Lcom/google/android/location/c/d;->b:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/c/d$a;

    .line 121
    if-nez v0, :cond_2a

    .line 122
    new-instance v0, Lcom/google/android/location/c/e;

    const-string v1, "%s cannot access to this library. Please contact lbs-team@google.com."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/location/c/e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_2a
    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/location/c/j;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-static {p0}, Lcom/google/android/location/c/d;->a(Ljava/lang/String;)Lcom/google/android/location/c/d$a;

    move-result-object v0

    .line 84
    invoke-virtual {v0, p1}, Lcom/google/android/location/c/d$a;->a(Lcom/google/android/location/c/j;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 85
    new-instance v0, Lcom/google/android/location/c/e;

    const-string v1, "Some features are prohibited from use by this application. Please contact lbs-team@google.com"

    invoke-direct {v0, v1}, Lcom/google/android/location/c/e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_12
    return-void
.end method
