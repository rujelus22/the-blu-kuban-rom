.class abstract Lcom/google/android/location/b/k$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/b/k$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/b/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/location/b/k$b",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

.field private final c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

.field private final d:I

.field private final e:I

.field private final f:I


# direct methods
.method protected constructor <init>(ILcom/google/googlenav/common/io/protocol/ProtoBufType;Lcom/google/googlenav/common/io/protocol/ProtoBufType;III)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput p1, p0, Lcom/google/android/location/b/k$a;->a:I

    .line 80
    iput-object p2, p0, Lcom/google/android/location/b/k$a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    .line 81
    iput-object p3, p0, Lcom/google/android/location/b/k$a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    .line 83
    iput p4, p0, Lcom/google/android/location/b/k$a;->d:I

    .line 84
    iput p5, p0, Lcom/google/android/location/b/k$a;->e:I

    .line 85
    iput p6, p0, Lcom/google/android/location/b/k$a;->f:I

    .line 86
    return-void
.end method

.method private a(Lcom/google/android/location/b/j;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/j",
            "<TK;TV;>;)",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;"
        }
    .end annotation

    .prologue
    .line 100
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/b/k$a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 102
    invoke-virtual {p1}, Lcom/google/android/location/b/j;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 103
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 104
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/A;

    .line 106
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v5, p0, Lcom/google/android/location/b/k$a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 108
    invoke-virtual {v0}, Lcom/google/android/location/e/A;->a()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p0, v4, v3, v5}, Lcom/google/android/location/b/k$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 111
    iget v3, p0, Lcom/google/android/location/b/k$a;->e:I

    invoke-virtual {v0}, Lcom/google/android/location/e/A;->b()J

    move-result-wide v5

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 112
    iget v3, p0, Lcom/google/android/location/b/k$a;->f:I

    invoke-virtual {v0}, Lcom/google/android/location/e/A;->c()J

    move-result-wide v5

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 114
    iget v0, p0, Lcom/google/android/location/b/k$a;->d:I

    invoke-virtual {v1, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_f

    .line 116
    :cond_4b
    return-object v1
.end method


# virtual methods
.method public a(Lcom/google/android/location/b/j;Ljava/io/InputStream;)V
    .registers 13
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/j",
            "<TK;TV;>;",
            "Ljava/io/InputStream;",
            ")V"
        }
    .end annotation

    .prologue
    .line 127
    invoke-virtual {p2}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 128
    iget v1, p0, Lcom/google/android/location/b/k$a;->a:I

    if-eq v0, v1, :cond_2d

    .line 129
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Version mismatch while reading LRU cache file (expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/location/b/k$a;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", but "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 133
    :cond_2d
    iget-object v0, p0, Lcom/google/android/location/b/k$a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {p2, v0}, Lcom/google/android/location/k/c;->a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 134
    iget v0, p0, Lcom/google/android/location/b/k$a;->d:I

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 135
    const/4 v0, 0x0

    :goto_3a
    if-ge v0, v2, :cond_64

    .line 136
    iget v3, p0, Lcom/google/android/location/b/k$a;->d:I

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 138
    invoke-virtual {p0, v3}, Lcom/google/android/location/b/k$a;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v4

    .line 139
    invoke-virtual {p0, v3}, Lcom/google/android/location/b/k$a;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v5

    .line 141
    iget v6, p0, Lcom/google/android/location/b/k$a;->e:I

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v6

    .line 142
    iget v8, p0, Lcom/google/android/location/b/k$a;->f:I

    invoke-virtual {v3, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v8

    .line 144
    new-instance v3, Lcom/google/android/location/e/A;

    invoke-direct {v3, v5, v6, v7}, Lcom/google/android/location/e/A;-><init>(Ljava/lang/Object;J)V

    .line 145
    invoke-virtual {v3, v8, v9}, Lcom/google/android/location/e/A;->b(J)V

    .line 147
    invoke-virtual {p1, v4, v3}, Lcom/google/android/location/b/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_3a

    .line 149
    :cond_64
    return-void
.end method

.method public a(Lcom/google/android/location/b/j;Ljava/io/OutputStream;)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/j",
            "<TK;TV;>;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/location/b/k$a;->a(Lcom/google/android/location/b/j;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 92
    iget v1, p0, Lcom/google/android/location/b/k$a;->a:I

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    .line 93
    invoke-virtual {v0, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    .line 94
    return-void
.end method

.method protected abstract a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/Object;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "TK;TV;)V"
        }
    .end annotation
.end method

.method protected abstract c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ")TV;"
        }
    .end annotation
.end method

.method protected abstract d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ")TK;"
        }
    .end annotation
.end method
