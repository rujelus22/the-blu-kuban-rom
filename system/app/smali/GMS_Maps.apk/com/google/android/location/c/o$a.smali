.class public Lcom/google/android/location/c/o$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/o;

.field private b:J

.field private c:J

.field private d:J

.field private e:J


# direct methods
.method protected constructor <init>(Lcom/google/android/location/c/o;)V
    .registers 6
    .parameter

    .prologue
    const-wide/16 v2, 0x0

    .line 165
    iput-object p1, p0, Lcom/google/android/location/c/o$a;->a:Lcom/google/android/location/c/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/c/o$a;->b:J

    .line 186
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/google/android/location/c/o$a;->c:J

    .line 191
    iput-wide v2, p0, Lcom/google/android/location/c/o$a;->d:J

    .line 194
    iput-wide v2, p0, Lcom/google/android/location/c/o$a;->e:J

    return-void
.end method

.method private a(JJJJ)J
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v4, 0x7800

    .line 241
    iget-wide v0, p0, Lcom/google/android/location/c/o$a;->c:J

    const-wide/16 v2, 0x64

    cmp-long v0, v0, v2

    if-gez v0, :cond_d

    .line 242
    iget-wide v0, p0, Lcom/google/android/location/c/o$a;->c:J

    .line 251
    :goto_c
    return-wide v0

    .line 244
    :cond_d
    sub-long v0, p7, p5

    long-to-double v0, v0

    sub-long v2, p3, p1

    long-to-double v2, v2

    div-double/2addr v0, v2

    .line 245
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-lez v2, :cond_37

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_37

    cmp-long v2, p7, v4

    if-gez v2, :cond_37

    .line 246
    sub-long v2, v4, p7

    long-to-double v2, v2

    div-double v0, v2, v0

    .line 249
    const-wide/16 v2, 0x7d0

    double-to-long v0, v0

    const-wide/16 v4, 0x2

    mul-long/2addr v0, v4

    const-wide/16 v4, 0x3

    div-long/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_c

    .line 251
    :cond_37
    const-wide/16 v0, 0xc8

    goto :goto_c
.end method

.method private a(J)V
    .registers 12
    .parameter

    .prologue
    const-wide/16 v2, 0x0

    .line 214
    iget-wide v0, p0, Lcom/google/android/location/c/o$a;->e:J

    sub-long v0, p1, v0

    iget-wide v4, p0, Lcom/google/android/location/c/o$a;->c:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_39

    .line 215
    iget-object v0, p0, Lcom/google/android/location/c/o$a;->a:Lcom/google/android/location/c/o;

    invoke-static {v0}, Lcom/google/android/location/c/o;->a(Lcom/google/android/location/c/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_3a

    move-wide v0, v2

    .line 216
    :goto_15
    iget-object v4, p0, Lcom/google/android/location/c/o$a;->a:Lcom/google/android/location/c/o;

    invoke-static {v4}, Lcom/google/android/location/c/o;->b(Lcom/google/android/location/c/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    if-nez v4, :cond_46

    move-wide v4, v2

    .line 217
    :goto_1e
    iget-object v6, p0, Lcom/google/android/location/c/o$a;->a:Lcom/google/android/location/c/o;

    invoke-static {v6}, Lcom/google/android/location/c/o;->c(Lcom/google/android/location/c/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    if-nez v6, :cond_52

    .line 219
    :goto_26
    add-long/2addr v0, v4

    add-long v7, v0, v2

    .line 220
    iget-wide v1, p0, Lcom/google/android/location/c/o$a;->e:J

    iget-wide v5, p0, Lcom/google/android/location/c/o$a;->d:J

    move-object v0, p0

    move-wide v3, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/c/o$a;->a(JJJJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/c/o$a;->c:J

    .line 222
    iput-wide v7, p0, Lcom/google/android/location/c/o$a;->d:J

    .line 223
    iput-wide p1, p0, Lcom/google/android/location/c/o$a;->e:J

    .line 225
    :cond_39
    return-void

    .line 215
    :cond_3a
    iget-object v0, p0, Lcom/google/android/location/c/o$a;->a:Lcom/google/android/location/c/o;

    invoke-static {v0}, Lcom/google/android/location/c/o;->a(Lcom/google/android/location/c/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDataSize()I

    move-result v0

    int-to-long v0, v0

    goto :goto_15

    .line 216
    :cond_46
    iget-object v4, p0, Lcom/google/android/location/c/o$a;->a:Lcom/google/android/location/c/o;

    invoke-static {v4}, Lcom/google/android/location/c/o;->b(Lcom/google/android/location/c/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDataSize()I

    move-result v4

    int-to-long v4, v4

    goto :goto_1e

    .line 217
    :cond_52
    iget-object v2, p0, Lcom/google/android/location/c/o$a;->a:Lcom/google/android/location/c/o;

    invoke-static {v2}, Lcom/google/android/location/c/o;->c(Lcom/google/android/location/c/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDataSize()I

    move-result v2

    int-to-long v2, v2

    goto :goto_26
.end method


# virtual methods
.method protected a()Z
    .registers 7

    .prologue
    .line 197
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 198
    invoke-direct {p0, v0, v1}, Lcom/google/android/location/c/o$a;->a(J)V

    .line 199
    iget-wide v2, p0, Lcom/google/android/location/c/o$a;->d:J

    const-wide/16 v4, 0x7800

    cmp-long v2, v2, v4

    if-gez v2, :cond_19

    iget-wide v2, p0, Lcom/google/android/location/c/o$a;->b:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-lez v0, :cond_1b

    :cond_19
    const/4 v0, 0x1

    :goto_1a
    return v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method protected b()V
    .registers 5

    .prologue
    const-wide/16 v2, 0x0

    .line 256
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/c/o$a;->b:J

    .line 257
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/google/android/location/c/o$a;->c:J

    .line 258
    iput-wide v2, p0, Lcom/google/android/location/c/o$a;->d:J

    .line 259
    iput-wide v2, p0, Lcom/google/android/location/c/o$a;->e:J

    .line 260
    return-void
.end method
