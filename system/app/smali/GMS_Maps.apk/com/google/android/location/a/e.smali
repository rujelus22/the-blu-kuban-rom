.class Lcom/google/android/location/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:D

.field final b:D

.field final c:D

.field private final d:[D

.field private final e:[D

.field private final f:[D

.field private final g:[D

.field private final h:[D

.field private final i:[D

.field private final j:[D


# direct methods
.method constructor <init>(DDD[D[D[D[D[D[D)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-wide p1, p0, Lcom/google/android/location/a/e;->a:D

    .line 54
    iput-wide p3, p0, Lcom/google/android/location/a/e;->b:D

    .line 55
    iput-wide p5, p0, Lcom/google/android/location/a/e;->c:D

    .line 56
    iput-object p7, p0, Lcom/google/android/location/a/e;->d:[D

    .line 57
    iput-object p8, p0, Lcom/google/android/location/a/e;->e:[D

    .line 60
    array-length v0, p8

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/google/android/location/a/e;->f:[D

    .line 61
    iget-object v0, p0, Lcom/google/android/location/a/e;->f:[D

    array-length v1, p8

    invoke-static {p8, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 63
    iput-object p9, p0, Lcom/google/android/location/a/e;->g:[D

    .line 64
    iput-object p10, p0, Lcom/google/android/location/a/e;->h:[D

    .line 65
    iput-object p11, p0, Lcom/google/android/location/a/e;->i:[D

    .line 66
    iput-object p12, p0, Lcom/google/android/location/a/e;->j:[D

    .line 67
    return-void
.end method

.method private a(D[D)D
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 86
    array-length v1, p3

    .line 87
    int-to-double v2, v1

    mul-double/2addr v2, p1

    double-to-int v0, v2

    .line 88
    if-ne v0, v1, :cond_8

    .line 89
    add-int/lit8 v0, v0, -0x1

    .line 91
    :cond_8
    aget-wide v0, p3, v0

    return-wide v0
.end method


# virtual methods
.method a(D)D
    .registers 5
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/location/a/e;->d:[D

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/a/e;->a(D[D)D

    move-result-wide v0

    return-wide v0
.end method

.method a(I)D
    .registers 4
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/location/a/e;->e:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method b(I)D
    .registers 4
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/location/a/e;->g:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method c(I)D
    .registers 4
    .parameter

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/location/a/e;->h:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method d(I)D
    .registers 6
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/location/a/e;->i:[D

    aget-wide v0, v0, p1

    iget-object v2, p0, Lcom/google/android/location/a/e;->j:[D

    aget-wide v2, v2, p1

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ActivityFeatures [accelStandardDeviationRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/a/e;->a:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accelMeanCrossingRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/a/e;->b:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", frequencyStandardDeviationRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/a/e;->c:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", frequencyData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/e;->e:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", frequencyTangentData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/e;->g:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dominantFrequency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/e;->h:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", earlyMean="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/e;->i:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lateMean="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/e;->j:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
