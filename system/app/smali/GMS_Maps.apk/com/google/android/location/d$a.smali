.class final enum Lcom/google/android/location/d$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/location/d$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/location/d$a;

.field public static final enum b:Lcom/google/android/location/d$a;

.field public static final enum c:Lcom/google/android/location/d$a;

.field public static final enum d:Lcom/google/android/location/d$a;

.field public static final enum e:Lcom/google/android/location/d$a;

.field private static final synthetic f:[Lcom/google/android/location/d$a;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 150
    new-instance v0, Lcom/google/android/location/d$a;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d$a;->a:Lcom/google/android/location/d$a;

    .line 151
    new-instance v0, Lcom/google/android/location/d$a;

    const-string v1, "DETECTING_ACTIVITY"

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d$a;->b:Lcom/google/android/location/d$a;

    .line 152
    new-instance v0, Lcom/google/android/location/d$a;

    const-string v1, "RANDOM_WAIT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/location/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d$a;->c:Lcom/google/android/location/d$a;

    .line 153
    new-instance v0, Lcom/google/android/location/d$a;

    const-string v1, "CLIENT_WAIT"

    invoke-direct {v0, v1, v5}, Lcom/google/android/location/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d$a;->d:Lcom/google/android/location/d$a;

    .line 154
    new-instance v0, Lcom/google/android/location/d$a;

    const-string v1, "GPS_WAIT"

    invoke-direct {v0, v1, v6}, Lcom/google/android/location/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/d$a;->e:Lcom/google/android/location/d$a;

    .line 149
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/location/d$a;

    sget-object v1, Lcom/google/android/location/d$a;->a:Lcom/google/android/location/d$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/location/d$a;->b:Lcom/google/android/location/d$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/d$a;->c:Lcom/google/android/location/d$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/location/d$a;->d:Lcom/google/android/location/d$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/location/d$a;->e:Lcom/google/android/location/d$a;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/location/d$a;->f:[Lcom/google/android/location/d$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 149
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/d$a;
    .registers 2
    .parameter

    .prologue
    .line 149
    const-class v0, Lcom/google/android/location/d$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/d$a;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/d$a;
    .registers 1

    .prologue
    .line 149
    sget-object v0, Lcom/google/android/location/d$a;->f:[Lcom/google/android/location/d$a;

    invoke-virtual {v0}, [Lcom/google/android/location/d$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/d$a;

    return-object v0
.end method
