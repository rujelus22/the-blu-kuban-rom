.class abstract Lcom/google/android/location/a/b$d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "d"
.end annotation


# instance fields
.field final synthetic b:Lcom/google/android/location/a/b;


# direct methods
.method private constructor <init>(Lcom/google/android/location/a/b;)V
    .registers 2
    .parameter

    .prologue
    .line 406
    iput-object p1, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 406
    invoke-direct {p0, p1}, Lcom/google/android/location/a/b$d;-><init>(Lcom/google/android/location/a/b;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .registers 1

    .prologue
    .line 411
    return-void
.end method

.method protected a(Lcom/google/android/location/a/n$b;)V
    .registers 2
    .parameter

    .prologue
    .line 474
    return-void
.end method

.method protected a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    invoke-static {v0, p1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Z)Z

    .line 440
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 449
    if-eqz p2, :cond_13

    sget-object v0, Lcom/google/android/location/os/i;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 451
    iget-object v0, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    invoke-static {v0, p1}, Lcom/google/android/location/a/b;->b(Lcom/google/android/location/a/b;Z)Z

    .line 453
    :cond_13
    return-void
.end method

.method protected b()V
    .registers 2

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->d(Lcom/google/android/location/a/b;)V

    .line 418
    return-void
.end method

.method protected c()V
    .registers 1

    .prologue
    .line 467
    return-void
.end method

.method protected d()V
    .registers 1

    .prologue
    .line 433
    return-void
.end method

.method protected e()V
    .registers 5

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$b;

    iget-object v2, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/a/b$b;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    .line 427
    return-void
.end method

.method protected f()Z
    .registers 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->j(Lcom/google/android/location/a/b;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/location/a/b$d;->b:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->k(Lcom/google/android/location/a/b;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method
