.class public Lcom/google/android/location/h/b;
.super Lcom/google/android/location/h/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/b/m$a;
.implements Lcom/google/googlenav/common/io/GoogleAsyncHttpConnection;


# instance fields
.field protected b:I

.field private final c:Lcom/google/android/location/h/b/e;

.field private final d:Lcom/google/android/location/h/h;

.field private e:Ljava/io/ByteArrayOutputStream;

.field private f:Ljava/lang/Exception;

.field private g:Ljava/io/DataInputStream;

.field private h:[Ljava/lang/String;

.field private i:[Ljava/lang/String;

.field private j:I

.field private k:I

.field private l:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Lcom/google/android/location/h/a;-><init>()V

    .line 44
    iput v0, p0, Lcom/google/android/location/h/b;->b:I

    .line 45
    iput-boolean v0, p0, Lcom/google/android/location/h/b;->l:Z

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncHttpConnection("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/google/android/location/h/h;->g()Lcom/google/android/location/h/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/b;->d:Lcom/google/android/location/h/h;

    .line 51
    new-instance v0, Lcom/google/android/location/h/b/e;

    invoke-direct {v0, p1, v2}, Lcom/google/android/location/h/b/e;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/location/h/b;->c:Lcom/google/android/location/h/b/e;

    .line 52
    iget-object v0, p0, Lcom/google/android/location/h/b;->c:Lcom/google/android/location/h/b/e;

    invoke-virtual {v0, p0}, Lcom/google/android/location/h/b/e;->a(Lcom/google/android/location/h/b/m$a;)V

    .line 54
    if-eqz p2, :cond_50

    .line 63
    iget-object v0, p0, Lcom/google/android/location/h/b;->c:Lcom/google/android/location/h/b/e;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Lcom/google/android/location/h/b/e;->c(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/location/h/b;->c:Lcom/google/android/location/h/b/e;

    invoke-virtual {v0, v2}, Lcom/google/android/location/h/b/e;->b(I)V

    .line 68
    :goto_4f
    return-void

    .line 66
    :cond_50
    iget-object v0, p0, Lcom/google/android/location/h/b;->c:Lcom/google/android/location/h/b/e;

    const-string v1, "GET"

    invoke-virtual {v0, v1}, Lcom/google/android/location/h/b/e;->c(Ljava/lang/String;)V

    goto :goto_4f
.end method

.method private a(I)V
    .registers 4
    .parameter

    .prologue
    .line 397
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncHttpConnection.setState("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 402
    iput p1, p0, Lcom/google/android/location/h/b;->b:I

    .line 404
    invoke-virtual {p0}, Lcom/google/android/location/h/b;->notifyObservers()V

    .line 405
    return-void
.end method

.method private static b(Ljava/lang/String;)V
    .registers 1
    .parameter

    .prologue
    .line 470
    return-void
.end method

.method private j()V
    .registers 2

    .prologue
    .line 71
    const-string v0, "AsyncHttpConnection.assertStateInit()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method private k()V
    .registers 3

    .prologue
    .line 80
    const-string v0, "AsyncHttpConnection.assertStateCompleted()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 82
    iget v0, p0, Lcom/google/android/location/h/b;->b:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_a

    .line 90
    :cond_a
    return-void
.end method

.method private l()V
    .registers 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/location/h/b;->f:Ljava/lang/Exception;

    if-nez v0, :cond_5

    .line 102
    :cond_4
    return-void

    .line 95
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/h/b;->f:Ljava/lang/Exception;

    instance-of v0, v0, Ljava/io/IOException;

    if-eqz v0, :cond_10

    .line 96
    iget-object v0, p0, Lcom/google/android/location/h/b;->f:Ljava/lang/Exception;

    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 97
    :cond_10
    iget-object v0, p0, Lcom/google/android/location/h/b;->f:Ljava/lang/Exception;

    instance-of v0, v0, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_4

    .line 98
    iget-object v0, p0, Lcom/google/android/location/h/b;->f:Ljava/lang/Exception;

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0
.end method


# virtual methods
.method public declared-synchronized a()Ljava/io/DataOutputStream;
    .registers 3

    .prologue
    .line 369
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.openDataOutputStream()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 371
    invoke-direct {p0}, Lcom/google/android/location/h/b;->j()V

    .line 373
    iget-object v0, p0, Lcom/google/android/location/h/b;->e:Ljava/io/ByteArrayOutputStream;

    if-nez v0, :cond_14

    .line 374
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/h/b;->e:Ljava/io/ByteArrayOutputStream;

    .line 377
    :cond_14
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/google/android/location/h/b;->e:Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_1d

    monitor-exit p0

    return-object v0

    .line 369
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 176
    monitor-enter p0

    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncHttpConnection.getHeaderField(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 178
    invoke-direct {p0}, Lcom/google/android/location/h/b;->k()V

    .line 179
    invoke-direct {p0}, Lcom/google/android/location/h/b;->l()V

    .line 183
    iget-object v0, p0, Lcom/google/android/location/h/b;->h:[Ljava/lang/String;

    if-eqz v0, :cond_48

    .line 184
    const/4 v0, 0x0

    :goto_28
    iget-object v1, p0, Lcom/google/android/location/h/b;->h:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_48

    .line 185
    iget-object v1, p0, Lcom/google/android/location/h/b;->h:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_45

    .line 186
    iget-object v1, p0, Lcom/google/android/location/h/b;->i:[Ljava/lang/String;

    aget-object v0, v1, v0
    :try_end_43
    .catchall {:try_start_1 .. :try_end_43} :catchall_4a

    .line 191
    :goto_43
    monitor-exit p0

    return-object v0

    .line 184
    :cond_45
    add-int/lit8 v0, v0, 0x1

    goto :goto_28

    .line 191
    :cond_48
    const/4 v0, 0x0

    goto :goto_43

    .line 176
    :catchall_4a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 435
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.requestComplete(request, response)"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 437
    iget v0, p0, Lcom/google/android/location/h/b;->b:I
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_3c

    const/4 v1, 0x1

    if-ne v0, v1, :cond_32

    .line 439
    :try_start_b
    new-instance v0, Lcom/google/android/location/h/b/f;

    invoke-direct {v0, p2}, Lcom/google/android/location/h/b/f;-><init>(Lcom/google/android/location/h/b/n;)V

    .line 440
    invoke-virtual {v0}, Lcom/google/android/location/h/b/f;->a()I

    move-result v1

    iput v1, p0, Lcom/google/android/location/h/b;->j:I

    .line 441
    invoke-virtual {v0}, Lcom/google/android/location/h/b/f;->d()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/h/b;->h:[Ljava/lang/String;

    .line 442
    invoke-virtual {v0}, Lcom/google/android/location/h/b/f;->e()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/h/b;->i:[Ljava/lang/String;

    .line 443
    invoke-virtual {v0}, Lcom/google/android/location/h/b/f;->b()I

    move-result v1

    iput v1, p0, Lcom/google/android/location/h/b;->k:I

    .line 444
    invoke-virtual {v0}, Lcom/google/android/location/h/b/f;->c()Ljava/io/DataInputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/b;->g:Ljava/io/DataInputStream;
    :try_end_2e
    .catchall {:try_start_b .. :try_end_2e} :catchall_47
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_2e} :catch_34
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_2e} :catch_3f

    .line 450
    const/4 v0, 0x2

    :try_start_2f
    invoke-direct {p0, v0}, Lcom/google/android/location/h/b;->a(I)V
    :try_end_32
    .catchall {:try_start_2f .. :try_end_32} :catchall_3c

    .line 453
    :cond_32
    :goto_32
    monitor-exit p0

    return-void

    .line 445
    :catch_34
    move-exception v0

    .line 446
    :try_start_35
    iput-object v0, p0, Lcom/google/android/location/h/b;->f:Ljava/lang/Exception;
    :try_end_37
    .catchall {:try_start_35 .. :try_end_37} :catchall_47

    .line 450
    const/4 v0, 0x2

    :try_start_38
    invoke-direct {p0, v0}, Lcom/google/android/location/h/b;->a(I)V
    :try_end_3b
    .catchall {:try_start_38 .. :try_end_3b} :catchall_3c

    goto :goto_32

    .line 435
    :catchall_3c
    move-exception v0

    monitor-exit p0

    throw v0

    .line 447
    :catch_3f
    move-exception v0

    .line 448
    :try_start_40
    iput-object v0, p0, Lcom/google/android/location/h/b;->f:Ljava/lang/Exception;
    :try_end_42
    .catchall {:try_start_40 .. :try_end_42} :catchall_47

    .line 450
    const/4 v0, 0x2

    :try_start_43
    invoke-direct {p0, v0}, Lcom/google/android/location/h/b;->a(I)V

    goto :goto_32

    :catchall_47
    move-exception v0

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/android/location/h/b;->a(I)V

    throw v0
    :try_end_4d
    .catchall {:try_start_43 .. :try_end_4d} :catchall_3c
.end method

.method public declared-synchronized a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 459
    monitor-enter p0

    :try_start_1
    iput-object p2, p0, Lcom/google/android/location/h/b;->f:Ljava/lang/Exception;

    .line 460
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/location/h/b;->a(I)V
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 461
    monitor-exit p0

    return-void

    .line 459
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 384
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.setConnectionProperty()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 386
    invoke-direct {p0}, Lcom/google/android/location/h/b;->j()V

    .line 388
    iget-object v0, p0, Lcom/google/android/location/h/b;->c:Lcom/google/android/location/h/b/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/h/b/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    .line 389
    monitor-exit p0

    return-void

    .line 384
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()Ljava/io/DataInputStream;
    .registers 2

    .prologue
    .line 353
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.openDataInputStream()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 355
    invoke-direct {p0}, Lcom/google/android/location/h/b;->k()V

    .line 356
    invoke-direct {p0}, Lcom/google/android/location/h/b;->l()V

    .line 358
    iget-object v0, p0, Lcom/google/android/location/h/b;->g:Ljava/io/DataInputStream;

    if-eqz v0, :cond_14

    .line 359
    iget-object v0, p0, Lcom/google/android/location/h/b;->g:Ljava/io/DataInputStream;
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_16

    .line 361
    :goto_12
    monitor-exit p0

    return-object v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_12

    .line 353
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()I
    .registers 2

    .prologue
    .line 271
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.getResponseCode()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 273
    invoke-direct {p0}, Lcom/google/android/location/h/b;->k()V

    .line 274
    invoke-direct {p0}, Lcom/google/android/location/h/b;->l()V

    .line 276
    iget v0, p0, Lcom/google/android/location/h/b;->j:I
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    monitor-exit p0

    return v0

    .line 271
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 142
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.getContentType()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 144
    invoke-direct {p0}, Lcom/google/android/location/h/b;->k()V

    .line 145
    invoke-direct {p0}, Lcom/google/android/location/h/b;->l()V

    .line 147
    const-string v0, "content-type"

    invoke-virtual {p0, v0}, Lcom/google/android/location/h/b;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_14

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 142
    :catchall_14
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()J
    .registers 3

    .prologue
    .line 246
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.getLength()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 248
    invoke-direct {p0}, Lcom/google/android/location/h/b;->k()V

    .line 249
    invoke-direct {p0}, Lcom/google/android/location/h/b;->l()V

    .line 251
    iget v0, p0, Lcom/google/android/location/h/b;->k:I
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_11

    int-to-long v0, v0

    monitor-exit p0

    return-wide v0

    .line 246
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()V
    .registers 2

    .prologue
    .line 120
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.close()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/location/h/b;->e:Ljava/io/ByteArrayOutputStream;

    invoke-static {v0}, Lcom/google/googlenav/common/io/i;->a(Ljava/io/OutputStream;)V

    .line 123
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/h/b;->e:Ljava/io/ByteArrayOutputStream;

    .line 124
    iget-object v0, p0, Lcom/google/android/location/h/b;->c:Lcom/google/android/location/h/b/e;

    invoke-virtual {v0}, Lcom/google/android/location/h/b/e;->a()V

    .line 126
    iget-object v0, p0, Lcom/google/android/location/h/b;->g:Ljava/io/DataInputStream;

    invoke-static {v0}, Lcom/google/googlenav/common/io/i;->b(Ljava/io/InputStream;)V

    .line 127
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/h/b;->g:Ljava/io/DataInputStream;

    .line 128
    iget-boolean v0, p0, Lcom/google/android/location/h/b;->l:Z

    if-nez v0, :cond_25

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/h/b;->h:[Ljava/lang/String;

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/h/b;->i:[Ljava/lang/String;

    .line 133
    :cond_25
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/location/h/b;->a(I)V
    :try_end_29
    .catchall {:try_start_1 .. :try_end_29} :catchall_2b

    .line 134
    monitor-exit p0

    return-void

    .line 120
    :catchall_2b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()Z
    .registers 2

    .prologue
    .line 306
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.isInit()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 308
    iget v0, p0, Lcom/google/android/location/h/b;->b:I
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_f

    if-nez v0, :cond_d

    const/4 v0, 0x1

    :goto_b
    monitor-exit p0

    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_b

    .line 306
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getHeaderField(I)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 203
    monitor-enter p0

    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncHttpConnection.getHeaderField("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 205
    invoke-direct {p0}, Lcom/google/android/location/h/b;->k()V

    .line 206
    invoke-direct {p0}, Lcom/google/android/location/h/b;->l()V

    .line 208
    iget-object v0, p0, Lcom/google/android/location/h/b;->i:[Ljava/lang/String;

    if-eqz v0, :cond_34

    .line 209
    if-ltz p1, :cond_34

    iget-object v0, p0, Lcom/google/android/location/h/b;->i:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_34

    .line 210
    iget-object v0, p0, Lcom/google/android/location/h/b;->i:[Ljava/lang/String;

    aget-object v0, v0, p1
    :try_end_32
    .catchall {:try_start_1 .. :try_end_32} :catchall_36

    .line 214
    :goto_32
    monitor-exit p0

    return-object v0

    :cond_34
    const/4 v0, 0x0

    goto :goto_32

    .line 203
    :catchall_36
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getHeaderFieldKey(I)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 226
    monitor-enter p0

    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncHttpConnection.getHeaderFieldKey("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 228
    invoke-direct {p0}, Lcom/google/android/location/h/b;->k()V

    .line 229
    invoke-direct {p0}, Lcom/google/android/location/h/b;->l()V

    .line 231
    iget-object v0, p0, Lcom/google/android/location/h/b;->h:[Ljava/lang/String;

    if-eqz v0, :cond_34

    .line 232
    if-ltz p1, :cond_34

    iget-object v0, p0, Lcom/google/android/location/h/b;->h:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_34

    .line 233
    iget-object v0, p0, Lcom/google/android/location/h/b;->h:[Ljava/lang/String;

    aget-object v0, v0, p1
    :try_end_32
    .catchall {:try_start_1 .. :try_end_32} :catchall_36

    .line 237
    :goto_32
    monitor-exit p0

    return-object v0

    :cond_34
    const/4 v0, 0x0

    goto :goto_32

    .line 226
    :catchall_36
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getProtocolName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 258
    const-string v0, "AsyncHttpConnection.getProtocolName()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 262
    const-string v0, "http"

    return-object v0
.end method

.method public declared-synchronized getResponseMessage()Ljava/lang/String;
    .registers 2

    .prologue
    .line 283
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.getResponseMessage()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 285
    invoke-direct {p0}, Lcom/google/android/location/h/b;->k()V

    .line 286
    invoke-direct {p0}, Lcom/google/android/location/h/b;->l()V
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_f

    .line 288
    const/4 v0, 0x0

    monitor-exit p0

    return-object v0

    .line 283
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getState()I
    .registers 2

    .prologue
    .line 297
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.getState()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 299
    iget v0, p0, Lcom/google/android/location/h/b;->b:I
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    monitor-exit p0

    return v0

    .line 297
    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized h()Z
    .registers 3

    .prologue
    .line 315
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.isCompleted()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 317
    iget v0, p0, Lcom/google/android/location/h/b;->b:I
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_10

    const/4 v1, 0x2

    if-ne v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_c
    monitor-exit p0

    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_c

    .line 315
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized i()Z
    .registers 3

    .prologue
    .line 324
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.isClosed()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 326
    iget v0, p0, Lcom/google/android/location/h/b;->b:I
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_10

    const/4 v1, 0x3

    if-ne v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_c
    monitor-exit p0

    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_c

    .line 324
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isEndToEndSecure()Z
    .registers 2

    .prologue
    .line 333
    const-string v0, "AsyncHttpConnection.isEndToEndSecure()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lcom/google/android/location/h/b;->d:Lcom/google/android/location/h/h;

    invoke-virtual {v0}, Lcom/google/android/location/h/h;->k()Z

    move-result v0

    return v0
.end method

.method public isHttps()Z
    .registers 2

    .prologue
    .line 342
    const-string v0, "AsyncHttpConnection.isHttps()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 344
    iget-object v0, p0, Lcom/google/android/location/h/b;->d:Lcom/google/android/location/h/h;

    invoke-virtual {v0}, Lcom/google/android/location/h/h;->l()Z

    move-result v0

    return v0
.end method

.method public notifyTimeout()V
    .registers 1

    .prologue
    .line 474
    return-void
.end method

.method public declared-synchronized submitRequest()V
    .registers 2

    .prologue
    .line 411
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    invoke-virtual {p0, v0}, Lcom/google/android/location/h/b;->submitRequest(Z)V
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_7

    .line 412
    monitor-exit p0

    return-void

    .line 411
    :catchall_7
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized submitRequest(Z)V
    .registers 4
    .parameter

    .prologue
    .line 418
    monitor-enter p0

    :try_start_1
    const-string v0, "AsyncHttpConnection.submitRequest()"

    invoke-static {v0}, Lcom/google/android/location/h/b;->b(Ljava/lang/String;)V

    .line 420
    iget v0, p0, Lcom/google/android/location/h/b;->b:I

    if-nez v0, :cond_24

    .line 421
    iget-object v0, p0, Lcom/google/android/location/h/b;->e:Ljava/io/ByteArrayOutputStream;

    if-eqz v0, :cond_19

    .line 422
    iget-object v0, p0, Lcom/google/android/location/h/b;->c:Lcom/google/android/location/h/b/e;

    iget-object v1, p0, Lcom/google/android/location/h/b;->e:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/h/b/e;->a([B)V

    .line 425
    :cond_19
    iget-object v0, p0, Lcom/google/android/location/h/b;->d:Lcom/google/android/location/h/h;

    iget-object v1, p0, Lcom/google/android/location/h/b;->c:Lcom/google/android/location/h/b/e;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/location/h/h;->a(Lcom/google/android/location/h/b/m;Z)V

    .line 427
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/h/b;->a(I)V
    :try_end_24
    .catchall {:try_start_1 .. :try_end_24} :catchall_26

    .line 429
    :cond_24
    monitor-exit p0

    return-void

    .line 418
    :catchall_26
    move-exception v0

    monitor-exit p0

    throw v0
.end method
