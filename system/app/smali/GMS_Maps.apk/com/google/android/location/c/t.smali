.class public Lcom/google/android/location/c/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/b/m$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/t$a;
    }
.end annotation


# static fields
.field private static final a:Lcom/google/android/location/h/h$a;


# instance fields
.field private final b:Lcom/google/android/location/d/c;

.field private final c:Lcom/google/android/location/k/a/c;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/h/b/m;",
            "Lcom/google/android/location/c/t$a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/h/b/m;",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Lcom/google/android/location/c/h",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Lcom/google/android/location/c/D$a;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/location/h/h$a;

    invoke-direct {v0}, Lcom/google/android/location/h/h$a;-><init>()V

    sput-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    .line 49
    sget-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    const-string v1, "https://www.google.com/loc/m/api"

    invoke-virtual {v0, v1}, Lcom/google/android/location/h/h$a;->a(Ljava/lang/String;)V

    .line 50
    sget-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Lcom/google/android/location/h/h$a;->b(Ljava/lang/String;)V

    .line 51
    sget-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    const-string v1, "1.0"

    invoke-virtual {v0, v1}, Lcom/google/android/location/h/h$a;->c(Ljava/lang/String;)V

    .line 52
    sget-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    sget-object v1, Lcom/google/android/location/c/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/location/h/h$a;->d(Ljava/lang/String;)V

    .line 53
    sget-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    const-string v1, "android"

    invoke-virtual {v0, v1}, Lcom/google/android/location/h/h$a;->e(Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/k/a/c;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    sget-object v0, Lcom/google/android/location/c/t;->a:Lcom/google/android/location/h/h$a;

    invoke-static {p1, v0}, Lcom/google/android/location/d/c;->a(Landroid/content/Context;Lcom/google/android/location/h/h$a;)V

    .line 69
    invoke-static {}, Lcom/google/android/location/d/c;->a()Lcom/google/android/location/d/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/t;->b:Lcom/google/android/location/d/c;

    .line 70
    invoke-static {p2}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/t;->c:Lcom/google/android/location/k/a/c;

    .line 71
    invoke-static {}, Lcom/google/android/location/c/L;->e()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/t;->d:Ljava/util/Map;

    .line 72
    invoke-static {}, Lcom/google/android/location/c/L;->e()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/t;->e:Ljava/util/Map;

    .line 73
    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/android/location/c/D$a;
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 156
    if-nez p1, :cond_d

    .line 157
    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v1, v2, v0, p2}, Lcom/google/android/location/c/D$a;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    move-object v0, v1

    .line 161
    :goto_c
    return-object v0

    :cond_d
    new-instance v1, Lcom/google/android/location/c/D$a;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1, v0}, Lcom/google/android/location/c/D$a;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_c
.end method

.method private a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/h/b/m;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 145
    :try_start_0
    invoke-virtual {p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_3} :catch_12

    move-result-object v0

    .line 149
    new-instance v1, Lcom/google/android/location/c/v;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, v0}, Lcom/google/android/location/c/v;-><init>(Ljava/lang/String;I[B)V

    .line 150
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/android/location/h/b/m;->b(I)V

    .line 151
    invoke-virtual {v1, p0}, Lcom/google/android/location/h/b/m;->a(Lcom/google/android/location/h/b/m$a;)V

    .line 152
    return-object v1

    .line 146
    :catch_12
    move-exception v0

    .line 147
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Lcom/google/android/location/h/b/m;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/location/c/t;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/c/t$a;

    .line 225
    if-eqz v0, :cond_e

    .line 226
    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/c/t$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    .line 234
    :cond_d
    :goto_d
    return-void

    .line 228
    :cond_e
    iget-object v0, p0, Lcom/google/android/location/c/t;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 230
    if-eqz v0, :cond_d

    .line 231
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/c/h;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/c/t;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/android/location/c/D$a;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/location/c/h;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_d
.end method


# virtual methods
.method public a(Lcom/google/android/location/h/b/m;Lcom/google/android/location/h/b/n;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 168
    .line 171
    :try_start_3
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->g()I

    move-result v1

    const/16 v2, 0xc8

    if-eq v1, v2, :cond_26

    .line 172
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Server error, RC="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->g()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_21} :catch_3e

    move-result-object v1

    .line 201
    :goto_22
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/c/t;->a(Lcom/google/android/location/h/b/m;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    .line 202
    return-void

    .line 175
    :cond_26
    :try_start_26
    invoke-virtual {p2}, Lcom/google/android/location/h/b/n;->c_()Ljava/io/InputStream;

    move-result-object v1

    .line 176
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 177
    const/16 v3, 0x400

    new-array v3, v3, [B

    .line 179
    :goto_33
    invoke-virtual {v1, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-ltz v4, :cond_4e

    .line 180
    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_3d
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_3d} :catch_3e

    goto :goto_33

    .line 196
    :catch_3e
    move-exception v1

    .line 198
    const-string v2, "Failed to read data from MASF: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_22

    .line 182
    :cond_4e
    :try_start_4e
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 183
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/j/a;->ai:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 184
    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 185
    invoke-virtual {v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->isValid()Z

    move-result v2

    if-nez v2, :cond_6a

    .line 186
    new-instance v1, Ljava/io/IOException;

    const-string v2, "isValid returned after parsing reply"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 188
    :cond_6a
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_87

    .line 189
    const-string v2, "GLS error: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_85
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_85} :catch_3e

    move-result-object v1

    goto :goto_22

    :cond_87
    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    .line 193
    goto :goto_22
.end method

.method public a(Lcom/google/android/location/h/b/m;Ljava/lang/Exception;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 207
    const-string v0, "Failed to send data to MASF: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 209
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/location/c/t;->a(Lcom/google/android/location/h/b/m;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    .line 210
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/h;)Z
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Lcom/google/android/location/c/h",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Lcom/google/android/location/c/D$a;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 96
    iget-object v2, p0, Lcom/google/android/location/c/t;->b:Lcom/google/android/location/d/c;

    if-nez v2, :cond_7

    .line 110
    :goto_6
    return v0

    .line 100
    :cond_7
    const-string v2, "g:loc/uil"

    invoke-direct {p0, v2, p1}, Lcom/google/android/location/c/t;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/location/h/b/m;

    move-result-object v2

    .line 101
    if-eqz p2, :cond_2a

    .line 102
    iget-object v3, p0, Lcom/google/android/location/c/t;->e:Ljava/util/Map;

    monitor-enter v3

    .line 103
    :try_start_12
    iget-object v4, p0, Lcom/google/android/location/c/t;->e:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1b

    move v0, v1

    :cond_1b
    const-string v4, "Duplicated request."

    invoke-static {v0, v4}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/location/c/t;->e:Ljava/util/Map;

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v0, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    monitor-exit v3
    :try_end_2a
    .catchall {:try_start_12 .. :try_end_2a} :catchall_31

    .line 109
    :cond_2a
    iget-object v0, p0, Lcom/google/android/location/c/t;->b:Lcom/google/android/location/d/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/d/c;->a(Lcom/google/android/location/h/b/m;Z)V

    move v0, v1

    .line 110
    goto :goto_6

    .line 107
    :catchall_31
    move-exception v0

    :try_start_32
    monitor-exit v3
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_31

    throw v0
.end method
