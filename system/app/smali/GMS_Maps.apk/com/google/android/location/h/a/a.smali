.class public abstract Lcom/google/android/location/h/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/a/d;


# instance fields
.field private a:Ljava/lang/Exception;

.field private b:Las/a;

.field private c:I


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-virtual {p0}, Lcom/google/android/location/h/a/a;->l()V

    .line 21
    return-void
.end method

.method private declared-synchronized a(I)V
    .registers 3
    .parameter

    .prologue
    .line 138
    monitor-enter p0

    :try_start_1
    iput p1, p0, Lcom/google/android/location/h/a/a;->c:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 139
    monitor-exit p0

    return-void

    .line 138
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Ljava/lang/Exception;)V
    .registers 3
    .parameter

    .prologue
    .line 133
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/location/h/a/a;->a:Ljava/lang/Exception;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 134
    monitor-exit p0

    return-void

    .line 133
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected declared-synchronized a()V
    .registers 3

    .prologue
    .line 26
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/location/h/a/a;->c:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_11

    .line 27
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "state != STATE_COMPLETED"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_e

    .line 26
    :catchall_e
    move-exception v0

    monitor-exit p0

    throw v0

    .line 29
    :cond_11
    monitor-exit p0

    return-void
.end method

.method protected declared-synchronized a(Las/a;)V
    .registers 3
    .parameter

    .prologue
    .line 144
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/location/h/a/a;->b:Las/a;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 145
    monitor-exit p0

    return-void

    .line 144
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(Ljava/lang/Exception;)V
    .registers 3
    .parameter

    .prologue
    .line 108
    monitor-enter p0

    const/4 v0, 0x3

    :try_start_2
    invoke-direct {p0, v0}, Lcom/google/android/location/h/a/a;->a(I)V

    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/location/h/a/a;->b(Ljava/lang/Exception;)V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/location/h/a/a;->m()V
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_d

    .line 111
    monitor-exit p0

    return-void

    .line 108
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .registers 2

    .prologue
    .line 33
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/location/h/a/a;->h_()V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_6

    .line 34
    monitor-exit p0

    return-void

    .line 33
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()Ljava/lang/Exception;
    .registers 2

    .prologue
    .line 38
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/h/a/a;->a:Ljava/lang/Exception;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-object v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()Z
    .registers 2

    .prologue
    .line 43
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/h/a/a;->a:Ljava/lang/Exception;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_a

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_6
    monitor-exit p0

    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_6

    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e_()I
    .registers 2

    .prologue
    .line 48
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/location/h/a/a;->c:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 58
    monitor-enter p0

    :try_start_2
    iget v1, p0, Lcom/google/android/location/h/a/a;->c:I
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_a

    if-ne v1, v0, :cond_8

    :goto_6
    monitor-exit p0

    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_6

    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized f_()V
    .registers 2

    .prologue
    .line 69
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    invoke-direct {p0, v0}, Lcom/google/android/location/h/a/a;->a(I)V
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_7

    .line 70
    monitor-exit p0

    return-void

    .line 69
    :catchall_7
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()Z
    .registers 3

    .prologue
    .line 63
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/location/h/a/a;->c:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_b

    iget v0, p0, Lcom/google/android/location/h/a/a;->c:I
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_10

    const/4 v1, 0x3

    if-ne v0, v1, :cond_e

    :cond_b
    const/4 v0, 0x1

    :goto_c
    monitor-exit p0

    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_c

    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized g_()V
    .registers 2

    .prologue
    .line 94
    monitor-enter p0

    const/4 v0, 0x2

    :try_start_2
    invoke-direct {p0, v0}, Lcom/google/android/location/h/a/a;->a(I)V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/location/h/a/a;->m()V
    :try_end_8
    .catchall {:try_start_2 .. :try_end_8} :catchall_a

    .line 96
    monitor-exit p0

    return-void

    .line 94
    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized h_()V
    .registers 2

    .prologue
    .line 115
    monitor-enter p0

    const/4 v0, 0x4

    :try_start_2
    invoke-direct {p0, v0}, Lcom/google/android/location/h/a/a;->a(I)V
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_7

    .line 116
    monitor-exit p0

    return-void

    .line 115
    :catchall_7
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized i()V
    .registers 2

    .prologue
    .line 81
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/location/h/a/a;->m()V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_6

    .line 82
    monitor-exit p0

    return-void

    .line 81
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized l()V
    .registers 2

    .prologue
    .line 120
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput v0, p0, Lcom/google/android/location/h/a/a;->c:I
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    .line 121
    monitor-exit p0

    return-void

    .line 120
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized m()V
    .registers 2

    .prologue
    .line 125
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/h/a/a;->b:Las/a;

    if-eqz v0, :cond_a

    .line 126
    iget-object v0, p0, Lcom/google/android/location/h/a/a;->b:Las/a;

    invoke-virtual {v0}, Las/a;->g()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 128
    :cond_a
    monitor-exit p0

    return-void

    .line 125
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method
