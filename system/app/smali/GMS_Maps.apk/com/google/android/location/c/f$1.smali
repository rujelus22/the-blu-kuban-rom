.class Lcom/google/android/location/c/f$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/f;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/f;)V
    .registers 2
    .parameter

    .prologue
    .line 165
    iput-object p1, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .registers 3

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->c(Lcom/google/android/location/c/f;)Lcom/google/android/location/c/g;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 231
    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->d(Lcom/google/android/location/c/f;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/c/f$1$1;

    invoke-direct {v1, p0}, Lcom/google/android/location/c/f$1$1;-><init>(Lcom/google/android/location/c/f$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 238
    :cond_16
    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->b(Lcom/google/android/location/c/f;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 239
    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->e(Lcom/google/android/location/c/f;)Z

    .line 241
    :cond_23
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 168
    new-instance v3, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v2}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 169
    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v4

    .line 170
    if-nez v4, :cond_32

    .line 172
    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to list files in directory:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v2}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Ljava/lang/String;)V

    .line 223
    :goto_31
    return-void

    .line 175
    :cond_32
    array-length v2, v4

    if-nez v2, :cond_39

    .line 176
    invoke-direct {p0}, Lcom/google/android/location/c/f$1;->a()V

    goto :goto_31

    .line 182
    :cond_39
    invoke-static {}, Lcom/google/android/location/c/C;->a()Lcom/google/android/location/c/C;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/location/c/C;->c(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_62

    .line 183
    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v2}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is locked."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Ljava/lang/String;)V

    goto :goto_31

    .line 188
    :cond_62
    :try_start_62
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/location/c/H;->a(Ljava/lang/String;)Lcom/google/android/location/c/H;

    move-result-object v5

    .line 189
    invoke-virtual {v5}, Lcom/google/android/location/c/H;->a()I

    move-result v2

    if-eqz v2, :cond_ae

    invoke-virtual {v5}, Lcom/google/android/location/c/H;->b()I

    move-result v2

    if-eqz v2, :cond_ae

    move v2, v0

    .line 193
    :goto_77
    invoke-virtual {v5}, Lcom/google/android/location/c/H;->c()Z

    move-result v5

    if-nez v5, :cond_b0

    if-nez v2, :cond_b0

    .line 196
    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->b(Lcom/google/android/location/c/f;)Z

    move-result v0

    if-eqz v0, :cond_8c

    .line 197
    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0, v3}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Ljava/io/File;)Z

    .line 199
    :cond_8c
    invoke-direct {p0}, Lcom/google/android/location/c/f$1;->a()V
    :try_end_8f
    .catch Ljava/io/IOException; {:try_start_62 .. :try_end_8f} :catch_90

    goto :goto_31

    .line 202
    :catch_90
    move-exception v0

    .line 203
    iget-object v1, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to load SessionSummary: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Ljava/lang/String;)V

    goto :goto_31

    :cond_ae
    move v2, v1

    .line 189
    goto :goto_77

    .line 207
    :cond_b0
    iget-object v2, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v2, v4}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;[Ljava/lang/String;)Lcom/google/android/location/c/f$a;

    move-result-object v2

    .line 208
    invoke-virtual {v2}, Lcom/google/android/location/c/f$a;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_c5

    .line 209
    invoke-direct {p0}, Lcom/google/android/location/c/f$1;->a()V

    goto/16 :goto_31

    .line 212
    :cond_c5
    iget-object v3, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-virtual {v2}, Lcom/google/android/location/c/f$a;->c()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_e9

    :goto_cd
    invoke-static {v3, v0}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Z)Z

    .line 213
    invoke-static {}, Lcom/google/android/location/c/C;->a()Lcom/google/android/location/c/C;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/C;->a(Ljava/lang/String;)Z

    move-result v0

    .line 214
    if-nez v0, :cond_eb

    .line 215
    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    const-string v1, "Failed to lock working directory."

    invoke-static {v0, v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Ljava/lang/String;)V

    goto/16 :goto_31

    :cond_e9
    move v0, v1

    .line 212
    goto :goto_cd

    .line 219
    :cond_eb
    :try_start_eb
    iget-object v0, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v0, v2}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Lcom/google/android/location/c/f$a;)V
    :try_end_f0
    .catchall {:try_start_eb .. :try_end_f0} :catchall_ff

    .line 221
    invoke-static {}, Lcom/google/android/location/c/C;->a()Lcom/google/android/location/c/C;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/C;->b(Ljava/lang/String;)V

    goto/16 :goto_31

    :catchall_ff
    move-exception v0

    invoke-static {}, Lcom/google/android/location/c/C;->a()Lcom/google/android/location/c/C;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/c/f$1;->a:Lcom/google/android/location/c/f;

    invoke-static {v2}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/location/c/C;->b(Ljava/lang/String;)V

    throw v0
.end method
