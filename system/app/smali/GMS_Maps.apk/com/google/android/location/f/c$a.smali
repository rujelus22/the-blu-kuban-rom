.class Lcom/google/android/location/f/c$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/f/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field public final a:I

.field public final b:F


# direct methods
.method public constructor <init>(IF)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput p1, p0, Lcom/google/android/location/f/c$a;->a:I

    .line 155
    iput p2, p0, Lcom/google/android/location/f/c$a;->b:F

    .line 156
    return-void
.end method

.method public static a(Ljava/io/DataInputStream;)Lcom/google/android/location/f/c$a;
    .registers 4
    .parameter

    .prologue
    .line 174
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 175
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readFloat()F

    move-result v1

    .line 176
    new-instance v2, Lcom/google/android/location/f/c$a;

    invoke-direct {v2, v0, v1}, Lcom/google/android/location/f/c$a;-><init>(IF)V

    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 181
    if-ne p0, p1, :cond_5

    .line 191
    :cond_4
    :goto_4
    return v0

    .line 185
    :cond_5
    instance-of v2, p1, Lcom/google/android/location/f/c$a;

    if-nez v2, :cond_b

    move v0, v1

    .line 186
    goto :goto_4

    .line 189
    :cond_b
    check-cast p1, Lcom/google/android/location/f/c$a;

    .line 191
    iget v2, p0, Lcom/google/android/location/f/c$a;->a:I

    iget v3, p1, Lcom/google/android/location/f/c$a;->a:I

    if-ne v2, v3, :cond_1b

    iget v2, p0, Lcom/google/android/location/f/c$a;->b:F

    iget v3, p1, Lcom/google/android/location/f/c$a;->b:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_4

    :cond_1b
    move v0, v1

    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 197
    .line 198
    iget v0, p0, Lcom/google/android/location/f/c$a;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 199
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/location/f/c$a;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 161
    iget v0, p0, Lcom/google/android/location/f/c$a;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_18

    .line 162
    const-string v0, "%+.4f"

    new-array v1, v4, [Ljava/lang/Object;

    iget v2, p0, Lcom/google/android/location/f/c$a;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 164
    :goto_17
    return-object v0

    :cond_18
    const-string v0, "%+.4f * [%d]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/google/android/location/f/c$a;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/google/android/location/f/c$a;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_17
.end method
