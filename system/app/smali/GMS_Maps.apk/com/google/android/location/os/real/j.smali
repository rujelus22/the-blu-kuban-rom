.class public abstract Lcom/google/android/location/os/real/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/location/os/real/j;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/location/os/real/j;->a:Lcom/google/android/location/os/real/j;

    if-nez v0, :cond_12

    .line 37
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 39
    const/16 v1, 0x11

    if-lt v0, v1, :cond_13

    .line 41
    const-string v0, "com.google.android.location.os.real.SdkSpecific17"

    invoke-static {v0}, Lcom/google/android/location/os/real/j;->a(Ljava/lang/String;)Lcom/google/android/location/os/real/j;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/j;->a:Lcom/google/android/location/os/real/j;

    .line 56
    :cond_12
    :goto_12
    return-void

    .line 42
    :cond_13
    const/16 v1, 0xb

    if-lt v0, v1, :cond_20

    .line 45
    const-string v0, "com.google.android.location.os.real.SdkSpecific11"

    invoke-static {v0}, Lcom/google/android/location/os/real/j;->a(Ljava/lang/String;)Lcom/google/android/location/os/real/j;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/j;->a:Lcom/google/android/location/os/real/j;

    goto :goto_12

    .line 46
    :cond_20
    const/16 v1, 0x9

    if-lt v0, v1, :cond_2d

    .line 48
    const-string v0, "com.google.android.location.os.real.SdkSpecific9"

    invoke-static {v0}, Lcom/google/android/location/os/real/j;->a(Ljava/lang/String;)Lcom/google/android/location/os/real/j;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/j;->a:Lcom/google/android/location/os/real/j;

    goto :goto_12

    .line 49
    :cond_2d
    const/16 v1, 0x8

    if-lt v0, v1, :cond_3a

    .line 51
    const-string v0, "com.google.android.location.os.real.SdkSpecific8"

    invoke-static {v0}, Lcom/google/android/location/os/real/j;->a(Ljava/lang/String;)Lcom/google/android/location/os/real/j;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/os/real/j;->a:Lcom/google/android/location/os/real/j;

    goto :goto_12

    .line 53
    :cond_3a
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported SDK"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    return-void
.end method

.method public static a()Lcom/google/android/location/os/real/j;
    .registers 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/android/location/os/real/j;->a:Lcom/google/android/location/os/real/j;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Lcom/google/android/location/os/real/j;
    .registers 3
    .parameter

    .prologue
    .line 84
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/location/os/real/j;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/real/j;
    :try_end_10
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_10} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_10} :catch_18
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_10} :catch_1f

    return-object v0

    .line 86
    :catch_11
    move-exception v0

    .line 87
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 88
    :catch_18
    move-exception v0

    .line 89
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 90
    :catch_1f
    move-exception v0

    .line 91
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public abstract a(Landroid/telephony/gsm/GsmCellLocation;)I
.end method

.method public abstract a(Landroid/location/Location;)J
.end method

.method public abstract a(Landroid/net/wifi/ScanResult;)J
.end method

.method public abstract a(Landroid/telephony/TelephonyManager;J)Lcom/google/android/location/e/e;
.end method

.method public abstract a(Ljava/io/File;)V
.end method

.method public abstract b()Z
.end method

.method public abstract c()V
.end method
