.class public Lcom/google/android/location/r;
.super Lcom/google/android/location/a;
.source "SourceFile"


# instance fields
.field A:J

.field B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field C:J

.field D:Lcom/google/android/location/i/a;

.field E:Z

.field private final F:Ljava/util/Random;

.field g:Z

.field h:Z

.field i:Z

.field j:Lcom/google/android/location/e/e;

.field k:Lcom/google/android/location/e/E;

.field l:Z

.field m:Lcom/google/android/location/e/E;

.field n:Lcom/google/android/location/os/g;

.field o:Z

.field p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field q:J

.field r:Z

.field s:Z

.field t:Z

.field u:Z

.field v:Z

.field w:J

.field x:J

.field y:Lcom/google/android/location/i/c$a;

.field z:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/i/a;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 154
    const-string v1, "PassiveCollector"

    sget-object v6, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/r;->i:Z

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/r;->j:Lcom/google/android/location/e/e;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/r;->k:Lcom/google/android/location/e/E;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/r;->l:Z

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/r;->o:Z

    .line 89
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/r;->q:J

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/r;->r:Z

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/r;->s:Z

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/r;->t:Z

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/r;->u:Z

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/r;->v:Z

    .line 109
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/r;->w:J

    .line 110
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/r;->x:J

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/r;->y:Lcom/google/android/location/i/c$a;

    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/r;->z:Z

    .line 125
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/r;->A:J

    .line 139
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/r;->C:J

    .line 155
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/r;->F:Ljava/util/Random;

    .line 156
    iget-object v0, p2, Lcom/google/android/location/b/f;->c:Lcom/google/android/location/os/h;

    invoke-virtual {p0, v0}, Lcom/google/android/location/r;->a(Lcom/google/android/location/os/h;)V

    .line 158
    iput-object p5, p0, Lcom/google/android/location/r;->D:Lcom/google/android/location/i/a;

    .line 159
    return-void
.end method

.method private b(I)Z
    .registers 15
    .parameter

    .prologue
    const/4 v12, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 476
    iget-object v0, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v8

    .line 477
    iget-object v0, p0, Lcom/google/android/location/r;->n:Lcom/google/android/location/os/g;

    if-eqz v0, :cond_5e

    iget-object v0, p0, Lcom/google/android/location/r;->n:Lcom/google/android/location/os/g;

    invoke-interface {v0}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v2

    sub-long v2, v8, v2

    const-wide/32 v10, 0xafc80

    cmp-long v0, v2, v10

    if-gtz v0, :cond_5e

    move v0, v7

    .line 484
    :goto_1e
    iget-object v2, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    if-eqz v2, :cond_b2

    .line 485
    if-eq p1, v12, :cond_38

    const/16 v2, 0x10

    if-eq p1, v2, :cond_38

    iget-boolean v2, p0, Lcom/google/android/location/r;->l:Z

    if-eqz v2, :cond_b2

    if-eqz v0, :cond_b2

    iget-object v2, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    iget-object v3, p0, Lcom/google/android/location/r;->n:Lcom/google/android/location/os/g;

    invoke-static {v2, v3}, Lcom/google/android/location/r;->a(Lcom/google/android/location/e/E;Lcom/google/android/location/os/g;)Z

    move-result v2

    if-eqz v2, :cond_b2

    :cond_38
    move v4, v7

    .line 498
    :goto_39
    iget-object v2, p0, Lcom/google/android/location/r;->j:Lcom/google/android/location/e/e;

    if-eqz v2, :cond_b0

    if-nez v4, :cond_4d

    if-eqz v0, :cond_b0

    iget-object v2, p0, Lcom/google/android/location/r;->e:Lcom/google/android/location/a$b;

    iget-object v3, p0, Lcom/google/android/location/r;->j:Lcom/google/android/location/e/e;

    iget-object v6, p0, Lcom/google/android/location/r;->n:Lcom/google/android/location/os/g;

    invoke-virtual {v2, v3, v6}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z

    move-result v2

    if-nez v2, :cond_b0

    :cond_4d
    move v3, v7

    .line 506
    :goto_4e
    if-eqz v0, :cond_60

    move v2, v7

    :goto_51
    add-int v6, v1, v2

    .line 507
    if-eqz v4, :cond_62

    move v2, v7

    :goto_56
    add-int/2addr v6, v2

    .line 508
    if-eqz v3, :cond_64

    move v2, v7

    :goto_5a
    add-int/2addr v2, v6

    .line 509
    if-ge v2, v12, :cond_66

    .line 530
    :cond_5d
    :goto_5d
    return v1

    :cond_5e
    move v0, v1

    .line 477
    goto :goto_1e

    :cond_60
    move v2, v1

    .line 506
    goto :goto_51

    :cond_62
    move v2, v1

    .line 507
    goto :goto_56

    :cond_64
    move v2, v1

    .line 508
    goto :goto_5a

    .line 512
    :cond_66
    if-nez v3, :cond_6a

    if-eq p1, v7, :cond_5d

    .line 515
    :cond_6a
    if-eqz v4, :cond_75

    .line 516
    if-nez v0, :cond_6f

    move v1, v7

    :cond_6f
    iput-boolean v1, p0, Lcom/google/android/location/r;->l:Z

    .line 517
    iget-object v1, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    iput-object v1, p0, Lcom/google/android/location/r;->k:Lcom/google/android/location/e/E;

    .line 519
    :cond_75
    if-eqz v0, :cond_aa

    iget-object v2, p0, Lcom/google/android/location/r;->n:Lcom/google/android/location/os/g;

    .line 520
    :goto_79
    if-eqz v3, :cond_ac

    iget-object v3, p0, Lcom/google/android/location/r;->j:Lcom/google/android/location/e/e;

    .line 521
    :goto_7d
    if-eqz v4, :cond_ae

    iget-object v4, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    .line 522
    :goto_81
    iget-object v0, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v0

    iget-boolean v5, p0, Lcom/google/android/location/r;->h:Z

    move v6, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/location/r;->a(JLcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;ZI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 524
    iget-object v1, p0, Lcom/google/android/location/r;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;)V

    .line 525
    iget-object v1, p0, Lcom/google/android/location/r;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v1, :cond_a2

    .line 526
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v1, p0, Lcom/google/android/location/r;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 527
    iput-wide v8, p0, Lcom/google/android/location/r;->C:J

    .line 529
    :cond_a2
    iget-object v1, p0, Lcom/google/android/location/r;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    move v1, v7

    .line 530
    goto :goto_5d

    :cond_aa
    move-object v2, v5

    .line 519
    goto :goto_79

    :cond_ac
    move-object v3, v5

    .line 520
    goto :goto_7d

    :cond_ae
    move-object v4, v5

    .line 521
    goto :goto_81

    :cond_b0
    move v3, v1

    goto :goto_4e

    :cond_b2
    move v4, v1

    goto :goto_39
.end method

.method private e()V
    .registers 5

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(I)V

    .line 282
    sget-object v0, Lcom/google/android/location/a$c;->f:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/r;->f:Lcom/google/android/location/a$c;

    .line 283
    iget-object v0, p0, Lcom/google/android/location/r;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/android/location/r;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 284
    iget-object v0, p0, Lcom/google/android/location/r;->d:Lcom/google/android/location/x;

    iget-object v1, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    iget-object v2, p0, Lcom/google/android/location/r;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/x;->a(Lcom/google/android/location/os/i;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 285
    iget-object v0, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    iget-object v1, p0, Lcom/google/android/location/r;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 286
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/r;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 287
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/r;->C:J

    .line 288
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/r;->r:Z

    .line 289
    iget-object v0, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/r;->q:J

    .line 290
    iget-wide v0, p0, Lcom/google/android/location/r;->q:J

    const-wide/16 v2, 0x3a98

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/r;->k(J)V

    .line 291
    return-void
.end method

.method private final f()Z
    .registers 7

    .prologue
    const-wide/32 v4, 0x1d4c0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 339
    iget-object v2, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->m()Z

    move-result v2

    if-nez v2, :cond_e

    .line 356
    :cond_d
    :goto_d
    return v0

    .line 345
    :cond_e
    iget-object v2, p0, Lcom/google/android/location/r;->D:Lcom/google/android/location/i/a;

    invoke-virtual {v2, v4, v5, v1}, Lcom/google/android/location/i/a;->b(JZ)Lcom/google/android/location/i/c$a;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/location/r;->y:Lcom/google/android/location/i/c$a;

    .line 346
    iget-object v2, p0, Lcom/google/android/location/r;->y:Lcom/google/android/location/i/c$a;

    if-eqz v2, :cond_1b

    move v0, v1

    .line 347
    :cond_1b
    if-eqz v0, :cond_d

    .line 348
    iget-object v2, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    const/4 v3, 0x3

    invoke-interface {v2, v3}, Lcom/google/android/location/os/i;->b(I)V

    .line 349
    iput-boolean v1, p0, Lcom/google/android/location/r;->r:Z

    .line 350
    iget-object v2, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    iget-object v3, p0, Lcom/google/android/location/r;->a:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    .line 351
    iget-object v1, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/location/r;->w:J

    .line 353
    iget-wide v1, p0, Lcom/google/android/location/r;->w:J

    add-long/2addr v1, v4

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/r;->k(J)V

    goto :goto_d
.end method

.method private final g()V
    .registers 9

    .prologue
    .line 363
    invoke-direct {p0}, Lcom/google/android/location/r;->h()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 364
    iget-object v0, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    iget-object v1, p0, Lcom/google/android/location/r;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    .line 368
    const-wide/16 v0, 0x0

    const-wide/32 v2, 0x1d4c0

    iget-object v4, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    invoke-interface {v4}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/location/r;->w:J

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 370
    iget-object v2, p0, Lcom/google/android/location/r;->D:Lcom/google/android/location/i/a;

    iget-object v3, p0, Lcom/google/android/location/r;->y:Lcom/google/android/location/i/c$a;

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/location/i/a;->a(Lcom/google/android/location/i/c$a;J)V

    .line 371
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/r;->y:Lcom/google/android/location/i/c$a;

    .line 372
    invoke-direct {p0}, Lcom/google/android/location/r;->i()V

    .line 373
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/r;->w:J

    .line 375
    :cond_32
    return-void
.end method

.method private final h()Z
    .registers 5

    .prologue
    .line 391
    iget-wide v0, p0, Lcom/google/android/location/r;->w:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private i()V
    .registers 4

    .prologue
    const/4 v2, 0x3

    .line 425
    iget-boolean v0, p0, Lcom/google/android/location/r;->r:Z

    if-eqz v0, :cond_16

    .line 426
    iget-object v0, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0, v2}, Lcom/google/android/location/os/i;->a(I)V

    .line 427
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/r;->A:J

    .line 428
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/r;->r:Z

    .line 429
    iget-object v0, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0, v2}, Lcom/google/android/location/os/i;->c(I)V

    .line 431
    :cond_16
    return-void
.end method

.method private j()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 434
    iput-boolean v0, p0, Lcom/google/android/location/r;->s:Z

    .line 435
    iput-boolean v0, p0, Lcom/google/android/location/r;->t:Z

    .line 436
    iput-boolean v0, p0, Lcom/google/android/location/r;->u:Z

    .line 437
    iput-boolean v0, p0, Lcom/google/android/location/r;->v:Z

    .line 438
    iput-boolean v0, p0, Lcom/google/android/location/r;->z:Z

    .line 439
    return-void
.end method

.method private j(J)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x4

    .line 274
    iget-object v0, p0, Lcom/google/android/location/r;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/google/android/location/r;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/google/android/location/r;->B:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_22

    iget-wide v0, p0, Lcom/google/android/location/r;->C:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0xdbba0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_24

    :cond_22
    const/4 v0, 0x1

    :goto_23
    return v0

    :cond_24
    const/4 v0, 0x0

    goto :goto_23
.end method

.method private k()I
    .registers 2

    .prologue
    .line 450
    iget-boolean v0, p0, Lcom/google/android/location/r;->s:Z

    if-eqz v0, :cond_d

    .line 451
    iget-boolean v0, p0, Lcom/google/android/location/r;->z:Z

    if-eqz v0, :cond_b

    .line 452
    const/16 v0, 0x10

    .line 466
    :goto_a
    return v0

    .line 454
    :cond_b
    const/4 v0, 0x2

    goto :goto_a

    .line 457
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/location/r;->u:Z

    if-eqz v0, :cond_13

    .line 458
    const/4 v0, 0x4

    goto :goto_a

    .line 460
    :cond_13
    iget-boolean v0, p0, Lcom/google/android/location/r;->v:Z

    if-eqz v0, :cond_1a

    .line 461
    const/16 v0, 0xb

    goto :goto_a

    .line 463
    :cond_1a
    iget-boolean v0, p0, Lcom/google/android/location/r;->t:Z

    if-eqz v0, :cond_20

    .line 464
    const/4 v0, 0x1

    goto :goto_a

    .line 466
    :cond_20
    const/4 v0, -0x1

    goto :goto_a
.end method

.method private k(J)V
    .registers 7
    .parameter

    .prologue
    .line 381
    iget-wide v0, p0, Lcom/google/android/location/r;->A:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_10

    .line 382
    iput-wide p1, p0, Lcom/google/android/location/r;->A:J

    .line 383
    iget-object v0, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/location/r;->A:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 385
    :cond_10
    return-void
.end method

.method private l()Z
    .registers 2

    .prologue
    .line 553
    invoke-virtual {p0}, Lcom/google/android/location/r;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/location/r;->o:Z

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 538
    const/4 v0, 0x3

    if-ne p1, v0, :cond_7

    .line 539
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/r;->A:J

    .line 541
    :cond_7
    return-void
.end method

.method public a(IIZ)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 545
    invoke-static {p1, p2}, Lcom/google/android/location/k/c;->a(II)F

    move-result v0

    .line 546
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_18

    .line 547
    if-nez p3, :cond_15

    float-to-double v0, v0

    const-wide v2, 0x3fc999999999999aL

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_1b

    :cond_15
    const/4 v0, 0x1

    :goto_16
    iput-boolean v0, p0, Lcom/google/android/location/r;->g:Z

    .line 549
    :cond_18
    iput-boolean p3, p0, Lcom/google/android/location/r;->h:Z

    .line 550
    return-void

    .line 547
    :cond_1b
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public a(Lcom/google/android/location/e/E;)V
    .registers 9
    .parameter

    .prologue
    const-wide/32 v4, 0x927c0

    .line 627
    if-eqz p1, :cond_b

    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v0

    if-nez v0, :cond_c

    .line 654
    :cond_b
    :goto_b
    return-void

    .line 633
    :cond_c
    iput-object p1, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    .line 634
    iget-object v0, p0, Lcom/google/android/location/r;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v0}, Lcom/google/android/location/a$b;->a()Lcom/google/android/location/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/r;->a(Lcom/google/android/location/b/g;Lcom/google/android/location/e/E;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/r;->z:Z

    .line 636
    invoke-direct {p0}, Lcom/google/android/location/r;->l()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 639
    iget-object v0, p0, Lcom/google/android/location/r;->k:Lcom/google/android/location/e/E;

    if-eqz v0, :cond_33

    iget-object v0, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    iget-wide v0, v0, Lcom/google/android/location/e/E;->a:J

    iget-object v2, p0, Lcom/google/android/location/r;->k:Lcom/google/android/location/e/E;

    iget-wide v2, v2, Lcom/google/android/location/e/E;->a:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-lez v0, :cond_b

    .line 647
    :cond_33
    iget-boolean v6, p0, Lcom/google/android/location/r;->s:Z

    iget-object v0, p0, Lcom/google/android/location/r;->e:Lcom/google/android/location/a$b;

    iget-object v1, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    iget-object v2, p0, Lcom/google/android/location/r;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/e/E;JJ)Z

    move-result v0

    or-int/2addr v0, v6

    iput-boolean v0, p0, Lcom/google/android/location/r;->s:Z

    .line 650
    iget-boolean v0, p0, Lcom/google/android/location/r;->s:Z

    if-eqz v0, :cond_b

    goto :goto_b
.end method

.method public a(Lcom/google/android/location/e/e;)V
    .registers 3
    .parameter

    .prologue
    .line 558
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/google/android/location/e/e;->i()Z

    move-result v0

    if-nez v0, :cond_c

    .line 559
    :cond_8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/r;->j:Lcom/google/android/location/e/e;

    .line 570
    :cond_b
    :goto_b
    return-void

    .line 562
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/r;->j:Lcom/google/android/location/e/e;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/location/r;->j:Lcom/google/android/location/e/e;

    invoke-virtual {v0, p1}, Lcom/google/android/location/e/e;->b(Lcom/google/android/location/e/e;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 565
    :cond_18
    iput-object p1, p0, Lcom/google/android/location/r;->j:Lcom/google/android/location/e/e;

    .line 566
    invoke-direct {p0}, Lcom/google/android/location/r;->l()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 567
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/r;->t:Z

    goto :goto_b
.end method

.method public a(Lcom/google/android/location/os/g;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 590
    if-nez p1, :cond_4

    .line 610
    :cond_3
    :goto_3
    return-void

    .line 594
    :cond_4
    iput-object p1, p0, Lcom/google/android/location/r;->n:Lcom/google/android/location/os/g;

    .line 595
    invoke-direct {p0}, Lcom/google/android/location/r;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 596
    iget-boolean v0, p0, Lcom/google/android/location/r;->l:Z

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    iget-object v1, p0, Lcom/google/android/location/r;->n:Lcom/google/android/location/os/g;

    invoke-static {v0, v1}, Lcom/google/android/location/r;->a(Lcom/google/android/location/e/E;Lcom/google/android/location/os/g;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 599
    iput-boolean v4, p0, Lcom/google/android/location/r;->u:Z

    goto :goto_3

    .line 601
    :cond_1d
    iget-object v0, p0, Lcom/google/android/location/r;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v0}, Lcom/google/android/location/a$b;->b()Lcom/google/android/location/os/g;

    move-result-object v0

    .line 602
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/location/r;->n:Lcom/google/android/location/os/g;

    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;)D

    move-result-wide v0

    const-wide/high16 v2, 0x4069

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    .line 606
    iput-boolean v4, p0, Lcom/google/android/location/r;->v:Z

    goto :goto_3
.end method

.method public a(Lcom/google/android/location/os/h;)V
    .registers 3
    .parameter

    .prologue
    .line 663
    invoke-virtual {p1}, Lcom/google/android/location/os/h;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/r;->E:Z

    .line 665
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 579
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/r;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 580
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 659
    return-void
.end method

.method a(Lcom/google/android/location/b/g;Lcom/google/android/location/e/E;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 677
    move v0, v1

    :goto_2
    invoke-virtual {p2}, Lcom/google/android/location/e/E;->a()I

    move-result v2

    if-ge v0, v2, :cond_22

    .line 678
    invoke-virtual {p2, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    .line 679
    invoke-virtual {p1, v2}, Lcom/google/android/location/b/g;->a(Ljava/lang/Long;)F

    move-result v2

    .line 681
    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-lez v3, :cond_23

    .line 682
    iget-object v3, p0, Lcom/google/android/location/r;->F:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    .line 684
    cmpg-float v2, v3, v2

    if-gez v2, :cond_23

    .line 685
    const/4 v1, 0x1

    .line 690
    :cond_22
    return v1

    .line 677
    :cond_23
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 622
    iput-boolean p1, p0, Lcom/google/android/location/r;->i:Z

    .line 623
    return-void
.end method

.method protected b(J)Z
    .registers 12
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 170
    invoke-direct {p0}, Lcom/google/android/location/r;->l()Z

    move-result v0

    if-nez v0, :cond_d

    .line 171
    invoke-direct {p0}, Lcom/google/android/location/r;->j()V

    move v2, v1

    .line 266
    :goto_c
    return v2

    .line 177
    :cond_d
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/r;->j(J)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 178
    invoke-direct {p0}, Lcom/google/android/location/r;->e()V

    goto :goto_c

    .line 187
    :cond_17
    iget-object v0, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    iget-wide v3, v0, Lcom/google/android/location/e/E;->a:J

    iget-object v0, p0, Lcom/google/android/location/r;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v0}, Lcom/google/android/location/a$b;->e()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-gtz v0, :cond_94

    :cond_29
    move v0, v2

    .line 189
    :goto_2a
    iget-object v3, p0, Lcom/google/android/location/r;->j:Lcom/google/android/location/e/e;

    if-eqz v3, :cond_3e

    iget-object v3, p0, Lcom/google/android/location/r;->j:Lcom/google/android/location/e/e;

    invoke-virtual {v3}, Lcom/google/android/location/e/e;->f()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/location/r;->e:Lcom/google/android/location/a$b;

    invoke-virtual {v5}, Lcom/google/android/location/a$b;->d()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gtz v3, :cond_96

    :cond_3e
    move v3, v2

    .line 191
    :goto_3f
    iget-boolean v4, p0, Lcom/google/android/location/r;->s:Z

    if-eqz v4, :cond_47

    if-eqz v0, :cond_47

    .line 192
    iput-boolean v1, p0, Lcom/google/android/location/r;->s:Z

    .line 195
    :cond_47
    iget-boolean v4, p0, Lcom/google/android/location/r;->t:Z

    if-eqz v4, :cond_4f

    if-eqz v3, :cond_4f

    .line 196
    iput-boolean v1, p0, Lcom/google/android/location/r;->t:Z

    .line 216
    :cond_4f
    iget-object v4, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    if-eqz v4, :cond_98

    iget-object v4, p0, Lcom/google/android/location/r;->m:Lcom/google/android/location/e/E;

    iget-wide v4, v4, Lcom/google/android/location/e/E;->a:J

    sub-long v4, p1, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/32 v6, 0xafc80

    cmp-long v4, v4, v6

    if-gtz v4, :cond_98

    move v4, v2

    .line 220
    :goto_65
    iget-wide v5, p0, Lcom/google/android/location/r;->x:J

    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-nez v5, :cond_9a

    move v5, v1

    .line 223
    :goto_6e
    iget-boolean v6, p0, Lcom/google/android/location/r;->s:Z

    if-eqz v6, :cond_a9

    iget-boolean v6, p0, Lcom/google/android/location/r;->z:Z

    if-eqz v6, :cond_a9

    iget-boolean v6, p0, Lcom/google/android/location/r;->u:Z

    if-nez v6, :cond_a9

    iget-boolean v6, p0, Lcom/google/android/location/r;->E:Z

    if-eqz v6, :cond_a9

    iget-boolean v6, p0, Lcom/google/android/location/r;->i:Z

    if-nez v6, :cond_a9

    if-eqz v4, :cond_a9

    if-nez v5, :cond_a9

    invoke-direct {p0}, Lcom/google/android/location/r;->f()Z

    move-result v4

    if-eqz v4, :cond_a9

    .line 225
    sget-object v0, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/r;->f:Lcom/google/android/location/a$c;

    .line 230
    iput-boolean v2, p0, Lcom/google/android/location/r;->l:Z

    goto/16 :goto_c

    :cond_94
    move v0, v1

    .line 187
    goto :goto_2a

    :cond_96
    move v3, v1

    .line 189
    goto :goto_3f

    :cond_98
    move v4, v1

    .line 216
    goto :goto_65

    .line 220
    :cond_9a
    iget-wide v5, p0, Lcom/google/android/location/r;->x:J

    sub-long v5, p1, v5

    const-wide/32 v7, 0x493e0

    cmp-long v5, v5, v7

    if-gez v5, :cond_a7

    move v5, v2

    goto :goto_6e

    :cond_a7
    move v5, v1

    goto :goto_6e

    .line 241
    :cond_a9
    iget-boolean v4, p0, Lcom/google/android/location/r;->z:Z

    if-nez v4, :cond_bd

    if-eqz v0, :cond_bd

    if-eqz v3, :cond_bd

    iget-boolean v0, p0, Lcom/google/android/location/r;->u:Z

    if-nez v0, :cond_b9

    iget-boolean v0, p0, Lcom/google/android/location/r;->v:Z

    if-eqz v0, :cond_bd

    .line 242
    :cond_b9
    iput-boolean v1, p0, Lcom/google/android/location/r;->u:Z

    .line 243
    iput-boolean v1, p0, Lcom/google/android/location/r;->v:Z

    .line 249
    :cond_bd
    iget-boolean v0, p0, Lcom/google/android/location/r;->s:Z

    if-nez v0, :cond_cd

    iget-boolean v0, p0, Lcom/google/android/location/r;->t:Z

    if-nez v0, :cond_cd

    iget-boolean v0, p0, Lcom/google/android/location/r;->u:Z

    if-nez v0, :cond_cd

    iget-boolean v0, p0, Lcom/google/android/location/r;->v:Z

    if-eqz v0, :cond_ea

    .line 250
    :cond_cd
    invoke-direct {p0}, Lcom/google/android/location/r;->k()I

    move-result v0

    .line 251
    const/4 v3, -0x1

    if-eq v0, v3, :cond_ea

    .line 254
    invoke-direct {p0, v0}, Lcom/google/android/location/r;->b(I)Z

    move-result v0

    .line 255
    if-eqz v0, :cond_dd

    .line 256
    invoke-direct {p0}, Lcom/google/android/location/r;->j()V

    .line 258
    :cond_dd
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/r;->j(J)Z

    move-result v0

    if-eqz v0, :cond_ea

    .line 259
    invoke-direct {p0}, Lcom/google/android/location/r;->e()V

    .line 265
    :goto_e6
    iput-boolean v1, p0, Lcom/google/android/location/r;->z:Z

    goto/16 :goto_c

    :cond_ea
    move v2, v1

    goto :goto_e6
.end method

.method public bridge synthetic c()V
    .registers 1

    .prologue
    .line 34
    invoke-super {p0}, Lcom/google/android/location/a;->c()V

    return-void
.end method

.method public c(Z)V
    .registers 2
    .parameter

    .prologue
    .line 574
    iput-boolean p1, p0, Lcom/google/android/location/r;->o:Z

    .line 575
    return-void
.end method

.method protected c(J)Z
    .registers 9
    .parameter

    .prologue
    const-wide/32 v4, 0x1d4c0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 306
    .line 307
    iget-wide v2, p0, Lcom/google/android/location/r;->w:J

    sub-long v2, p1, v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_2f

    move v2, v1

    .line 309
    :goto_e
    if-eqz v2, :cond_31

    :goto_10
    iput-wide p1, p0, Lcom/google/android/location/r;->x:J

    .line 311
    iget-boolean v3, p0, Lcom/google/android/location/r;->E:Z

    if-eqz v3, :cond_3b

    invoke-direct {p0}, Lcom/google/android/location/r;->l()Z

    move-result v3

    if-eqz v3, :cond_3b

    iget-boolean v3, p0, Lcom/google/android/location/r;->i:Z

    if-nez v3, :cond_3b

    .line 314
    iget-boolean v3, p0, Lcom/google/android/location/r;->u:Z

    if-nez v3, :cond_26

    if-eqz v2, :cond_34

    .line 315
    :cond_26
    invoke-direct {p0}, Lcom/google/android/location/r;->g()V

    .line 316
    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/r;->f:Lcom/google/android/location/a$c;

    move v0, v1

    .line 330
    :goto_2e
    return v0

    :cond_2f
    move v2, v0

    .line 307
    goto :goto_e

    .line 309
    :cond_31
    const-wide/16 p1, -0x1

    goto :goto_10

    .line 320
    :cond_34
    iget-wide v1, p0, Lcom/google/android/location/r;->w:J

    add-long/2addr v1, v4

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/r;->k(J)V

    goto :goto_2e

    .line 325
    :cond_3b
    invoke-direct {p0}, Lcom/google/android/location/r;->g()V

    .line 326
    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/r;->f:Lcom/google/android/location/a$c;

    move v0, v1

    .line 327
    goto :goto_2e
.end method

.method protected d()Z
    .registers 2

    .prologue
    .line 613
    iget-boolean v0, p0, Lcom/google/android/location/r;->g:Z

    return v0
.end method

.method protected f(J)Z
    .registers 8
    .parameter

    .prologue
    const-wide/16 v3, 0x3a98

    .line 402
    const/4 v0, 0x0

    .line 403
    iget-object v1, p0, Lcom/google/android/location/r;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v1, :cond_14

    .line 404
    const/4 v0, 0x1

    .line 405
    invoke-direct {p0}, Lcom/google/android/location/r;->i()V

    .line 406
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/location/r;->q:J

    .line 407
    sget-object v1, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v1, p0, Lcom/google/android/location/r;->f:Lcom/google/android/location/a$c;

    .line 418
    :goto_13
    return v0

    .line 411
    :cond_14
    iget-wide v1, p0, Lcom/google/android/location/r;->q:J

    sub-long v1, p1, v1

    cmp-long v1, v1, v3

    if-ltz v1, :cond_20

    .line 412
    invoke-direct {p0}, Lcom/google/android/location/r;->i()V

    goto :goto_13

    .line 415
    :cond_20
    iget-wide v1, p0, Lcom/google/android/location/r;->q:J

    add-long/2addr v1, v3

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/r;->k(J)V

    goto :goto_13
.end method
