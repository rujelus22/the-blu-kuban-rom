.class final Lcom/google/android/location/os/real/e$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/h/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private a:[B

.field private b:[B


# direct methods
.method public constructor <init>([B)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    iput-object v0, p0, Lcom/google/android/location/os/real/e$a;->a:[B

    .line 185
    iput-object v0, p0, Lcom/google/android/location/os/real/e$a;->b:[B

    .line 193
    iput-object p1, p0, Lcom/google/android/location/os/real/e$a;->a:[B

    .line 194
    return-void
.end method

.method private a([B)[B
    .registers 4
    .parameter

    .prologue
    .line 209
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 210
    new-instance v1, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v1, v0}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 211
    invoke-virtual {v1, p1}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    .line 212
    invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 213
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 214
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/os/real/e$a;->a:[B

    .line 215
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized d()V
    .registers 2

    .prologue
    .line 222
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/os/real/e$a;->b:[B

    if-nez v0, :cond_10

    .line 223
    iget-object v0, p0, Lcom/google/android/location/os/real/e$a;->a:[B

    invoke-direct {p0, v0}, Lcom/google/android/location/os/real/e$a;->a([B)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/e$a;->b:[B

    .line 224
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/e$a;->a:[B
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_12

    .line 226
    :cond_10
    monitor-exit p0

    return-void

    .line 222
    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 198
    iput-object v0, p0, Lcom/google/android/location/os/real/e$a;->a:[B

    .line 199
    iput-object v0, p0, Lcom/google/android/location/os/real/e$a;->b:[B

    .line 200
    return-void
.end method

.method public declared-synchronized b_()I
    .registers 2

    .prologue
    .line 242
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/location/os/real/e$a;->d()V

    .line 243
    iget-object v0, p0, Lcom/google/android/location/os/real/e$a;->b:[B

    array-length v0, v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    monitor-exit p0

    return v0

    .line 242
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c_()Ljava/io/InputStream;
    .registers 3

    .prologue
    .line 233
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/location/os/real/e$a;->d()V

    .line 234
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/google/android/location/os/real/e$a;->b:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_d

    monitor-exit p0

    return-object v0

    .line 233
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method
