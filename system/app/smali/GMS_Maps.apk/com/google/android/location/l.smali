.class public Lcom/google/android/location/l;
.super Lcom/google/android/location/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/l$1;,
        Lcom/google/android/location/l$a;
    }
.end annotation


# static fields
.field static final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final g:Lcom/google/android/location/c/l;

.field i:Lcom/google/android/location/c/r;

.field j:J

.field k:J

.field private final l:Lcom/google/android/location/g;

.field private final m:Lcom/google/android/location/t;

.field private final n:Lcom/google/android/location/k;

.field private final o:Lcom/google/android/location/m;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 40
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/location/c/F;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/location/c/F;->h:Lcom/google/android/location/c/F;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/location/c/F;->g:Lcom/google/android/location/c/F;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/location/c/F;->a([Lcom/google/android/location/c/F;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/l;->h:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/g;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-static {}, Lcom/google/android/location/t;->a()Lcom/google/android/location/t;

    move-result-object v6

    new-instance v7, Lcom/google/android/location/k;

    sget-object v0, Lcom/google/android/location/v;->i:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    invoke-static {}, Lcom/google/android/location/t;->a()Lcom/google/android/location/t;

    move-result-object v1

    invoke-direct {v7, p1, v0, p5, v1}, Lcom/google/android/location/k;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/k/b;Lcom/google/android/location/g;Lcom/google/android/location/t;)V

    new-instance v8, Lcom/google/android/location/m;

    invoke-direct {v8, p5, p1}, Lcom/google/android/location/m;-><init>(Lcom/google/android/location/g;Lcom/google/android/location/os/i;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/l;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/g;Lcom/google/android/location/t;Lcom/google/android/location/k;Lcom/google/android/location/m;)V

    .line 62
    return-void
.end method

.method constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/g;Lcom/google/android/location/t;Lcom/google/android/location/k;Lcom/google/android/location/m;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 68
    const-string v1, "IOCollector"

    sget-object v6, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V

    .line 34
    new-instance v0, Lcom/google/android/location/l$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/l$a;-><init>(Lcom/google/android/location/l;Lcom/google/android/location/l$1;)V

    iput-object v0, p0, Lcom/google/android/location/l;->g:Lcom/google/android/location/c/l;

    .line 44
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/l;->j:J

    .line 52
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/l;->k:J

    .line 69
    iput-object p5, p0, Lcom/google/android/location/l;->l:Lcom/google/android/location/g;

    .line 70
    iput-object p6, p0, Lcom/google/android/location/l;->m:Lcom/google/android/location/t;

    .line 71
    iput-object p7, p0, Lcom/google/android/location/l;->n:Lcom/google/android/location/k;

    .line 72
    iput-object p8, p0, Lcom/google/android/location/l;->o:Lcom/google/android/location/m;

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/l;)Lcom/google/android/location/m;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/location/l;->o:Lcom/google/android/location/m;

    return-object v0
.end method

.method private j(J)Z
    .registers 11
    .parameter

    .prologue
    const/4 v7, 0x1

    .line 139
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v2

    .line 140
    sget-object v0, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    iget-object v0, p0, Lcom/google/android/location/l;->b:Lcom/google/android/location/os/i;

    sget-object v1, Lcom/google/android/location/l;->h:Ljava/util/Set;

    const-wide/16 v3, 0x2710

    iget-object v5, p0, Lcom/google/android/location/l;->g:Lcom/google/android/location/c/l;

    iget-object v6, p0, Lcom/google/android/location/l;->a:Ljava/lang/String;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/i;->a(Ljava/util/Set;Ljava/util/Map;JLcom/google/android/location/c/l;Ljava/lang/String;)Lcom/google/android/location/c/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/l;->i:Lcom/google/android/location/c/r;

    .line 143
    iget-object v0, p0, Lcom/google/android/location/l;->i:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_29

    .line 144
    iget-object v0, p0, Lcom/google/android/location/l;->i:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->a()V

    .line 145
    iput-wide p1, p0, Lcom/google/android/location/l;->j:J

    .line 149
    :cond_29
    iget-object v0, p0, Lcom/google/android/location/l;->i:Lcom/google/android/location/c/r;

    if-eqz v0, :cond_2f

    move v0, v7

    :goto_2e
    return v0

    :cond_2f
    const/4 v0, 0x0

    goto :goto_2e
.end method

.method private k(J)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 167
    iget-wide v1, p0, Lcom/google/android/location/l;->k:J

    const-wide v3, 0x7fffffffffffffffL

    cmp-long v1, v1, v3

    if-nez v1, :cond_d

    .line 170
    :cond_c
    :goto_c
    return v0

    :cond_d
    iget-wide v1, p0, Lcom/google/android/location/l;->k:J

    const-wide/16 v3, 0x2710

    sub-long/2addr v1, v3

    cmp-long v1, v1, p1

    if-gtz v1, :cond_c

    iget-wide v1, p0, Lcom/google/android/location/l;->k:J

    const-wide/32 v3, 0x2bf20

    add-long/2addr v1, v3

    cmp-long v1, p1, v1

    if-gtz v1, :cond_c

    const/4 v0, 0x1

    goto :goto_c
.end method


# virtual methods
.method protected b(J)Z
    .registers 9
    .parameter

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    .line 81
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/l;->k(J)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 82
    invoke-virtual {p0}, Lcom/google/android/location/l;->d()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 85
    iput-wide v4, p0, Lcom/google/android/location/l;->k:J

    .line 86
    iget-object v0, p0, Lcom/google/android/location/l;->l:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->l()V

    .line 87
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/l;->j(J)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 88
    sget-object v0, Lcom/google/android/location/a$c;->i:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/l;->f:Lcom/google/android/location/a$c;

    .line 110
    :cond_22
    :goto_22
    iget-object v0, p0, Lcom/google/android/location/l;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->i:Lcom/google/android/location/a$c;

    if-eq v0, v1, :cond_46

    iget-wide v0, p0, Lcom/google/android/location/l;->k:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_46

    iget-object v0, p0, Lcom/google/android/location/l;->m:Lcom/google/android/location/t;

    invoke-virtual {v0}, Lcom/google/android/location/t;->b()Z

    move-result v0

    if-eqz v0, :cond_46

    .line 113
    iget-object v0, p0, Lcom/google/android/location/l;->n:Lcom/google/android/location/k;

    invoke-virtual {v0}, Lcom/google/android/location/k;->a()Lcom/google/android/location/e/u;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/l;->k:J

    .line 115
    :cond_46
    iget-object v0, p0, Lcom/google/android/location/l;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    if-eq v0, v1, :cond_66

    const/4 v0, 0x1

    :goto_4d
    return v0

    .line 96
    :cond_4e
    iget-wide v0, p0, Lcom/google/android/location/l;->k:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_22

    iget-wide v0, p0, Lcom/google/android/location/l;->k:J

    const-wide/32 v2, 0x2bf20

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-lez v0, :cond_22

    .line 101
    iput-wide v4, p0, Lcom/google/android/location/l;->k:J

    .line 102
    iget-object v0, p0, Lcom/google/android/location/l;->l:Lcom/google/android/location/g;

    invoke-virtual {v0}, Lcom/google/android/location/g;->l()V

    goto :goto_22

    .line 115
    :cond_66
    const/4 v0, 0x0

    goto :goto_4d
.end method

.method public bridge synthetic c()V
    .registers 1

    .prologue
    .line 31
    invoke-super {p0}, Lcom/google/android/location/a;->c()V

    return-void
.end method

.method d()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 157
    iget-object v0, p0, Lcom/google/android/location/l;->m:Lcom/google/android/location/t;

    invoke-virtual {v0, v1}, Lcom/google/android/location/t;->a(Z)Lcom/google/android/location/e/u;

    move-result-object v0

    .line 158
    iget-object v2, p0, Lcom/google/android/location/l;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->m()Z

    move-result v2

    .line 159
    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1b

    if-eqz v2, :cond_1b

    const/4 v0, 0x1

    .line 160
    :goto_1a
    return v0

    :cond_1b
    move v0, v1

    .line 159
    goto :goto_1a
.end method

.method protected i(J)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 120
    iget-object v0, p0, Lcom/google/android/location/l;->i:Lcom/google/android/location/c/r;

    if-nez v0, :cond_b

    .line 122
    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/l;->f:Lcom/google/android/location/a$c;

    .line 123
    const/4 v0, 0x1

    .line 132
    :goto_a
    return v0

    .line 125
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/l;->m:Lcom/google/android/location/t;

    invoke-virtual {v0, v1}, Lcom/google/android/location/t;->a(Z)Lcom/google/android/location/e/u;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_20

    .line 130
    iget-object v0, p0, Lcom/google/android/location/l;->i:Lcom/google/android/location/c/r;

    invoke-interface {v0}, Lcom/google/android/location/c/r;->b()V

    :cond_20
    move v0, v1

    .line 132
    goto :goto_a
.end method
