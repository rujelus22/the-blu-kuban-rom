.class public abstract Lcom/google/android/location/h/b/o;
.super Lcom/google/android/location/h/b/m;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private f:I

.field private g:[B


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/location/h/b/m;-><init>()V

    .line 33
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/location/h/b/m;-><init>()V

    .line 39
    invoke-virtual {p0, p1}, Lcom/google/android/location/h/b/o;->a(Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0, p2}, Lcom/google/android/location/h/b/o;->d(I)V

    .line 41
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 2

    .prologue
    .line 84
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/location/h/b/o;->g:[B
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    .line 85
    monitor-exit p0

    return-void

    .line 84
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 147
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/location/h/b/o;->a:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 148
    monitor-exit p0

    return-void

    .line 147
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract b()Ljava/io/InputStream;
.end method

.method public declared-synchronized b_()I
    .registers 3

    .prologue
    .line 93
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/location/h/b/o;->u()[B

    .line 95
    iget-object v0, p0, Lcom/google/android/location/h/b/o;->g:[B

    array-length v0, v0

    invoke-virtual {p0}, Lcom/google/android/location/h/b/o;->c()I
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_e

    move-result v1

    add-int/2addr v0, v1

    monitor-exit p0

    return v0

    .line 93
    :catchall_e
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract c()I
.end method

.method public final declared-synchronized c(I)V
    .registers 3
    .parameter

    .prologue
    .line 134
    monitor-enter p0

    :try_start_1
    iput p1, p0, Lcom/google/android/location/h/b/o;->f:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 135
    monitor-exit p0

    return-void

    .line 134
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c_()Ljava/io/InputStream;
    .registers 4

    .prologue
    .line 104
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/location/h/b/o;->u()[B

    .line 109
    new-instance v0, Lcom/google/googlenav/common/io/SequenceInputStream;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/google/android/location/h/b/o;->g:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {p0}, Lcom/google/android/location/h/b/o;->b()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/common/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_16

    monitor-exit p0

    return-object v0

    .line 104
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized d(I)V
    .registers 3
    .parameter

    .prologue
    .line 161
    monitor-enter p0

    :try_start_1
    iput p1, p0, Lcom/google/android/location/h/b/o;->b:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 162
    monitor-exit p0

    return-void

    .line 161
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected u()[B
    .registers 4

    .prologue
    .line 58
    .line 62
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 63
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/location/h/b/o;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 65
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 66
    iget v2, p0, Lcom/google/android/location/h/b/o;->f:I

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 67
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 68
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 73
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/h/b/o;->g:[B

    .line 74
    iget-object v0, p0, Lcom/google/android/location/h/b/o;->g:[B

    return-object v0
.end method

.method public final declared-synchronized v()Ljava/lang/String;
    .registers 2

    .prologue
    .line 117
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/h/b/o;->a:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-object v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized w()I
    .registers 2

    .prologue
    .line 124
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/location/h/b/o;->b:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method
