.class public Lcom/google/android/location/os/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/os/f;

.field private final b:Ljavax/crypto/SecretKey;

.field private final c:Lcom/google/android/location/c/a;

.field private final d:Ljava/io/File;

.field private final e:I

.field private final f:I

.field private volatile g:I

.field private final h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

.field private final i:Lcom/google/common/base/K;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/K",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjavax/crypto/SecretKey;I[BLcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/File;Lcom/google/common/base/K;Lcom/google/android/location/os/f;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljavax/crypto/SecretKey;",
            "I[B",
            "Lcom/google/googlenav/common/io/protocol/ProtoBufType;",
            "Ljava/io/File;",
            "Lcom/google/common/base/K",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;",
            "Lcom/google/android/location/os/f;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/os/j;->g:I

    .line 85
    iput-object p5, p0, Lcom/google/android/location/os/j;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    .line 86
    iput-object p8, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/f;

    .line 87
    iput-object p6, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    .line 88
    iput p1, p0, Lcom/google/android/location/os/j;->e:I

    .line 89
    iput p3, p0, Lcom/google/android/location/os/j;->f:I

    .line 90
    iput-object p2, p0, Lcom/google/android/location/os/j;->b:Ljavax/crypto/SecretKey;

    .line 91
    if-eqz p4, :cond_1e

    .line 92
    invoke-static {p4, v1}, Lcom/google/android/location/c/a;->b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/c/a;

    .line 96
    :goto_1b
    iput-object p7, p0, Lcom/google/android/location/os/j;->i:Lcom/google/common/base/K;

    .line 97
    return-void

    .line 94
    :cond_1e
    iput-object v1, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/c/a;

    goto :goto_1b
.end method

.method private a(ILjava/io/DataInputStream;)Ljava/io/InputStream;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 145
    iget v0, p0, Lcom/google/android/location/os/j;->e:I

    if-ne p1, v0, :cond_b

    .line 148
    iget-object v0, p0, Lcom/google/android/location/os/j;->b:Ljavax/crypto/SecretKey;

    invoke-static {p2, v0}, Lcom/google/android/location/os/b;->b(Ljava/io/InputStream;Ljavax/crypto/SecretKey;)Ljava/io/InputStream;

    move-result-object v0

    .line 155
    :goto_a
    return-object v0

    .line 151
    :cond_b
    iget-object v0, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/c/a;

    if-nez v0, :cond_17

    .line 152
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No cipher key specified."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_17
    iget-object v0, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/c/a;

    invoke-virtual {v0, p2}, Lcom/google/android/location/c/a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/e/u;

    move-result-object v0

    .line 155
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, [B

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    move-object v0, v1

    goto :goto_a
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/io/OutputStream;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 216
    const/4 v2, 0x0

    .line 218
    :try_start_1
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, p2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_3a

    .line 220
    :try_start_6
    iget v0, p0, Lcom/google/android/location/os/j;->f:I

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 221
    iget-object v0, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/c/a;
    :try_end_d
    .catchall {:try_start_6 .. :try_end_d} :catchall_2d

    if-eqz v0, :cond_32

    .line 224
    :try_start_f
    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B
    :try_end_12
    .catchall {:try_start_f .. :try_end_12} :catchall_2d
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_12} :catch_1f

    move-result-object v0

    .line 232
    :try_start_13
    iget-object v2, p0, Lcom/google/android/location/os/j;->c:Lcom/google/android/location/c/a;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/location/c/a;->a(Ljava/io/DataOutputStream;[B)V

    .line 236
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V
    :try_end_1b
    .catchall {:try_start_13 .. :try_end_1b} :catchall_2d

    .line 238
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    .line 240
    return-void

    .line 225
    :catch_1f
    move-exception v0

    .line 229
    :try_start_20
    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 230
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Runtime while writing protobuf to bytes."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2d
    .catchall {:try_start_20 .. :try_end_2d} :catchall_2d

    .line 238
    :catchall_2d
    move-exception v0

    :goto_2e
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    throw v0

    .line 234
    :cond_32
    :try_start_32
    new-instance v0, Ljava/io/IOException;

    const-string v2, "No cipher specified."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3a
    .catchall {:try_start_32 .. :try_end_3a} :catchall_2d

    .line 238
    :catchall_3a
    move-exception v0

    move-object v1, v2

    goto :goto_2e
.end method


# virtual methods
.method public a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 3

    .prologue
    .line 114
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {p0, v0}, Lcom/google/android/location/os/j;->a(Ljava/io/InputStream;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_a} :catch_c

    move-result-object v0

    return-object v0

    .line 115
    :catch_c
    move-exception v0

    .line 116
    new-instance v0, Ljava/io/IOException;

    const-string v1, "File not found."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method a(Ljava/io/InputStream;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 122
    .line 125
    :try_start_1
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_6d

    .line 126
    :try_start_6
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/os/j;->g:I

    .line 127
    iget v0, p0, Lcom/google/android/location/os/j;->g:I

    iget v3, p0, Lcom/google/android/location/os/j;->e:I

    if-eq v0, v3, :cond_4a

    iget v0, p0, Lcom/google/android/location/os/j;->g:I

    iget v3, p0, Lcom/google/android/location/os/j;->f:I

    if-eq v0, v3, :cond_4a

    .line 128
    new-instance v0, Ljava/io/IOException;

    const-string v3, "Invalid version, desired = %d or %d, actual = %d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/location/os/j;->e:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/location/os/j;->f:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget v6, p0, Lcom/google/android/location/os/j;->g:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_42
    .catchall {:try_start_6 .. :try_end_42} :catchall_42

    .line 138
    :catchall_42
    move-exception v0

    :goto_43
    invoke-static {v2}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    .line 139
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    throw v0

    .line 131
    :cond_4a
    :try_start_4a
    iget v0, p0, Lcom/google/android/location/os/j;->g:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/os/j;->a(ILjava/io/DataInputStream;)Ljava/io/InputStream;

    move-result-object v2

    .line 132
    iget-object v0, p0, Lcom/google/android/location/os/j;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, v0}, Lcom/google/android/location/k/c;->a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 133
    iget-object v3, p0, Lcom/google/android/location/os/j;->i:Lcom/google/common/base/K;

    invoke-interface {v3, v0}, Lcom/google/common/base/K;->a(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_66

    .line 134
    new-instance v0, Ljava/io/IOException;

    const-string v3, "Invalid file format."

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_66
    .catchall {:try_start_4a .. :try_end_66} :catchall_42

    .line 138
    :cond_66
    invoke-static {v2}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    .line 139
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    return-object v0

    .line 138
    :catchall_6d
    move-exception v0

    move-object v1, v2

    goto :goto_43
.end method

.method public a([B)V
    .registers 5
    .parameter

    .prologue
    .line 176
    const-string v0, "output buffer can not be null."

    invoke-static {p1, v0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    const/4 v1, 0x0

    .line 179
    :try_start_6
    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_13

    .line 180
    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 182
    :cond_13
    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/f;

    iget-object v2, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-interface {v0, v2}, Lcom/google/android/location/os/f;->a(Ljava/io/File;)V

    .line 183
    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_21
    .catchall {:try_start_6 .. :try_end_21} :catchall_28

    .line 184
    :try_start_21
    invoke-virtual {v2, p1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_24
    .catchall {:try_start_21 .. :try_end_24} :catchall_2d

    .line 186
    invoke-static {v2}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    .line 188
    return-void

    .line 186
    :catchall_28
    move-exception v0

    :goto_29
    invoke-static {v1}, Lcom/google/android/location/k/c;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_2d
    move-exception v0

    move-object v1, v2

    goto :goto_29
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[B
    .registers 3
    .parameter

    .prologue
    .line 165
    const-string v0, "protoBuf can not be null."

    invoke-static {p1, v0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 167
    invoke-direct {p0, p1, v0}, Lcom/google/android/location/os/j;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/io/OutputStream;)V

    .line 168
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 202
    const-string v0, "protoBuf can not be null."

    invoke-static {p1, v0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_12

    .line 204
    iget-object v0, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 206
    :cond_12
    iget-object v0, p0, Lcom/google/android/location/os/j;->a:Lcom/google/android/location/os/f;

    iget-object v1, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/f;->a(Ljava/io/File;)V

    .line 207
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/google/android/location/os/j;->d:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/os/j;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/io/OutputStream;)V

    .line 208
    return-void
.end method
