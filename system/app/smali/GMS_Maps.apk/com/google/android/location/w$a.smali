.class final enum Lcom/google/android/location/w$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/location/w$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/location/w$a;

.field public static final enum b:Lcom/google/android/location/w$a;

.field public static final enum c:Lcom/google/android/location/w$a;

.field private static final synthetic d:[Lcom/google/android/location/w$a;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 94
    new-instance v0, Lcom/google/android/location/w$a;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/w$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w$a;

    .line 95
    new-instance v0, Lcom/google/android/location/w$a;

    const-string v1, "WIFI_WAIT"

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/w$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/w$a;->b:Lcom/google/android/location/w$a;

    .line 96
    new-instance v0, Lcom/google/android/location/w$a;

    const-string v1, "UPLOADING"

    invoke-direct {v0, v1, v4}, Lcom/google/android/location/w$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/w$a;->c:Lcom/google/android/location/w$a;

    .line 93
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/location/w$a;

    sget-object v1, Lcom/google/android/location/w$a;->a:Lcom/google/android/location/w$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/location/w$a;->b:Lcom/google/android/location/w$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/w$a;->c:Lcom/google/android/location/w$a;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/location/w$a;->d:[Lcom/google/android/location/w$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/w$a;
    .registers 2
    .parameter

    .prologue
    .line 93
    const-class v0, Lcom/google/android/location/w$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/w$a;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/w$a;
    .registers 1

    .prologue
    .line 93
    sget-object v0, Lcom/google/android/location/w$a;->d:[Lcom/google/android/location/w$a;

    invoke-virtual {v0}, [Lcom/google/android/location/w$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/w$a;

    return-object v0
.end method
