.class public Lcom/google/android/location/e/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/location/e/e;

.field private b:J

.field private c:Ljava/lang/Boolean;

.field private d:J

.field private e:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    return-void
.end method

.method private a(JJ)I
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 151
    sub-long v0, p1, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 152
    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method declared-synchronized a()I
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 120
    monitor-enter p0

    :try_start_2
    iget-boolean v1, p0, Lcom/google/android/location/e/d;->e:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_12

    if-eqz v1, :cond_9

    .line 121
    const/4 v0, 0x1

    .line 128
    :cond_7
    :goto_7
    monitor-exit p0

    return v0

    .line 122
    :cond_9
    :try_start_9
    iget-object v1, p0, Lcom/google/android/location/e/d;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/location/e/d;->a:Lcom/google/android/location/e/e;
    :try_end_f
    .catchall {:try_start_9 .. :try_end_f} :catchall_12

    if-nez v1, :cond_7

    goto :goto_7

    .line 120
    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/location/e/e;J)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 93
    monitor-enter p0

    if-eqz p1, :cond_9

    :try_start_3
    invoke-virtual {p1}, Lcom/google/android/location/e/e;->i()Z
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_36

    move-result v0

    if-nez v0, :cond_b

    .line 117
    :cond_9
    :goto_9
    monitor-exit p0

    return-void

    .line 99
    :cond_b
    :try_start_b
    iget-object v0, p0, Lcom/google/android/location/e/d;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/google/android/location/e/d;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_31

    .line 101
    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/google/android/location/e/d;->d:J

    invoke-direct {p0, p2, p3, v1, v2}, Lcom/google/android/location/e/d;->a(JJ)I

    move-result v1

    if-ge v0, v1, :cond_31

    .line 104
    if-eqz p1, :cond_31

    iget-object v0, p0, Lcom/google/android/location/e/d;->a:Lcom/google/android/location/e/e;

    if-eqz v0, :cond_31

    .line 106
    iget-object v0, p0, Lcom/google/android/location/e/d;->a:Lcom/google/android/location/e/e;

    invoke-virtual {p1, v0}, Lcom/google/android/location/e/e;->b(Lcom/google/android/location/e/e;)Z

    move-result v0

    if-nez v0, :cond_31

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/e/d;->e:Z

    .line 115
    :cond_31
    iput-wide p2, p0, Lcom/google/android/location/e/d;->b:J

    .line 116
    iput-object p1, p0, Lcom/google/android/location/e/d;->a:Lcom/google/android/location/e/e;
    :try_end_35
    .catchall {:try_start_b .. :try_end_35} :catchall_36

    goto :goto_9

    .line 93
    :catchall_36
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ZJ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 87
    monitor-enter p0

    :try_start_1
    iput-wide p2, p0, Lcom/google/android/location/e/d;->d:J

    .line 88
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/e/d;->c:Ljava/lang/Boolean;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    .line 89
    monitor-exit p0

    return-void

    .line 87
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 146
    monitor-enter p0

    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/location/e/d;->a()I

    move-result v1

    if-eq v1, v0, :cond_14

    iget-object v1, p0, Lcom/google/android/location/e/d;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    iget-object v1, p0, Lcom/google/android/location/e/d;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_11
    .catchall {:try_start_2 .. :try_end_11} :catchall_18

    move-result v1

    if-eqz v1, :cond_16

    :cond_14
    :goto_14
    monitor-exit p0

    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_14

    :catchall_18
    move-exception v0

    monitor-exit p0

    throw v0
.end method
