.class public Lcom/google/android/location/c;
.super Lcom/google/android/location/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c$1;,
        Lcom/google/android/location/c$a;
    }
.end annotation


# instance fields
.field g:J

.field h:F

.field i:Z

.field j:Lcom/google/android/location/c$a;

.field k:Z

.field l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/os/g;",
            ">;"
        }
    .end annotation
.end field

.field m:Z

.field n:Lcom/google/android/location/os/g;

.field o:J

.field p:Lcom/google/android/location/e/E;

.field q:Z

.field r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field s:J

.field t:Lcom/google/android/location/e/E;

.field u:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v8, -0x1

    const/4 v7, 0x0

    .line 93
    const-string v1, "ActiveCollector"

    sget-object v6, Lcom/google/android/location/a$c;->a:Lcom/google/android/location/a$c;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/a;-><init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V

    .line 58
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/c;->g:J

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/c;->h:F

    .line 61
    iput-boolean v7, p0, Lcom/google/android/location/c;->i:Z

    .line 65
    sget-object v0, Lcom/google/android/location/c$a;->a:Lcom/google/android/location/c$a;

    iput-object v0, p0, Lcom/google/android/location/c;->j:Lcom/google/android/location/c$a;

    .line 66
    iput-boolean v7, p0, Lcom/google/android/location/c;->k:Z

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    .line 69
    iput-boolean v7, p0, Lcom/google/android/location/c;->m:Z

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c;->q:Z

    .line 82
    iput-wide v8, p0, Lcom/google/android/location/c;->s:J

    .line 89
    iput-wide v8, p0, Lcom/google/android/location/c;->u:J

    .line 97
    invoke-interface {p1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    const-wide/32 v2, 0xa4cb80

    add-long/2addr v0, v2

    const-wide/32 v2, 0x112a880

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/c;->g:J

    .line 98
    return-void
.end method

.method private a(Lcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;ZI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/location/c;->a(JLcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;ZI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 225
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 226
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 227
    iget-object v0, p0, Lcom/google/android/location/c;->d:Lcom/google/android/location/x;

    iget-object v2, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/x;->a(Lcom/google/android/location/os/i;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 228
    return-object v1
.end method

.method private b(Lcom/google/android/location/os/g;)Z
    .registers 4
    .parameter

    .prologue
    .line 317
    invoke-interface {p1}, Lcom/google/android/location/os/g;->a()F

    move-result v0

    const/high16 v1, 0x4220

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_16

    invoke-interface {p1}, Lcom/google/android/location/os/g;->e()F

    move-result v0

    const/high16 v1, 0x3fc0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private e()V
    .registers 4

    .prologue
    .line 261
    sget-object v0, Lcom/google/android/location/a$c;->a:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    .line 262
    iget-object v0, p0, Lcom/google/android/location/c;->j:Lcom/google/android/location/c$a;

    sget-object v1, Lcom/google/android/location/c$a;->a:Lcom/google/android/location/c$a;

    if-eq v0, v1, :cond_2a

    .line 263
    iget-object v0, p0, Lcom/google/android/location/c;->j:Lcom/google/android/location/c$a;

    sget-object v1, Lcom/google/android/location/c$a;->b:Lcom/google/android/location/c$a;

    if-ne v0, v1, :cond_18

    .line 264
    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    iget-object v1, p0, Lcom/google/android/location/c;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    .line 267
    :cond_18
    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->c(I)V

    .line 268
    sget-object v0, Lcom/google/android/location/c$a;->a:Lcom/google/android/location/c$a;

    iput-object v0, p0, Lcom/google/android/location/c;->j:Lcom/google/android/location/c$a;

    .line 269
    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/c;->g:J

    .line 271
    :cond_2a
    invoke-direct {p0}, Lcom/google/android/location/c;->f()V

    .line 272
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c;->p:Lcom/google/android/location/e/E;

    .line 273
    return-void
.end method

.method private f()V
    .registers 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 312
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c;->m:Z

    .line 313
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c;->n:Lcom/google/android/location/os/g;

    .line 314
    return-void
.end method

.method private j(J)V
    .registers 7
    .parameter

    .prologue
    .line 253
    iget-wide v0, p0, Lcom/google/android/location/c;->u:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_10

    .line 254
    iput-wide p1, p0, Lcom/google/android/location/c;->u:J

    .line 255
    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/location/c;->u:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 257
    :cond_10
    return-void
.end method

.method private k(J)Z
    .registers 9
    .parameter

    .prologue
    const-wide/32 v4, 0x1d4c0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 279
    sget-object v2, Lcom/google/android/location/c$1;->a:[I

    iget-object v3, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    invoke-virtual {v3}, Lcom/google/android/location/a$c;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_4a

    .line 306
    :cond_12
    :goto_12
    return v1

    .line 281
    :pswitch_13
    iget-wide v2, p0, Lcom/google/android/location/c;->g:J

    sub-long v2, p1, v2

    cmp-long v2, v4, v2

    if-lez v2, :cond_1d

    :goto_1b
    move v1, v0

    goto :goto_12

    :cond_1d
    move v0, v1

    goto :goto_1b

    .line 285
    :pswitch_1f
    iget-boolean v2, p0, Lcom/google/android/location/c;->m:Z

    if-eqz v2, :cond_12

    .line 290
    :pswitch_23
    iget-object v2, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->m()Z

    move-result v2

    if-eqz v2, :cond_12

    iget-wide v2, p0, Lcom/google/android/location/c;->g:J

    sub-long v2, p1, v2

    cmp-long v2, v4, v2

    if-lez v2, :cond_12

    .line 300
    :pswitch_33
    iget-boolean v2, p0, Lcom/google/android/location/c;->k:Z

    if-eqz v2, :cond_47

    iget-boolean v2, p0, Lcom/google/android/location/c;->q:Z

    if-nez v2, :cond_47

    iget-object v2, p0, Lcom/google/android/location/c;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v2, :cond_47

    invoke-virtual {p0}, Lcom/google/android/location/c;->d()Z

    move-result v2

    if-eqz v2, :cond_47

    :goto_45
    move v1, v0

    goto :goto_12

    :cond_47
    move v0, v1

    goto :goto_45

    .line 279
    nop

    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_13
        :pswitch_1f
        :pswitch_1f
        :pswitch_23
        :pswitch_33
        :pswitch_33
    .end packed-switch
.end method


# virtual methods
.method a(I)V
    .registers 4
    .parameter

    .prologue
    .line 329
    const/4 v0, 0x1

    if-ne p1, v0, :cond_7

    .line 330
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/c;->u:J

    .line 332
    :cond_7
    return-void
.end method

.method a(IIZ)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 399
    iput-boolean p3, p0, Lcom/google/android/location/c;->i:Z

    .line 400
    invoke-static {p1, p2}, Lcom/google/android/location/k/c;->a(II)F

    move-result v0

    .line 401
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_d

    .line 402
    iput v0, p0, Lcom/google/android/location/c;->h:F

    .line 404
    :cond_d
    return-void
.end method

.method a(Lcom/google/android/location/e/E;)V
    .registers 8
    .parameter

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->d:Lcom/google/android/location/a$c;

    if-ne v0, v1, :cond_16

    .line 381
    const-wide/16 v0, 0x3e8

    iget-wide v2, p1, Lcom/google/android/location/e/E;->a:J

    iget-wide v4, p0, Lcom/google/android/location/c;->o:J

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_17

    .line 383
    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->n()V

    .line 388
    :cond_16
    :goto_16
    return-void

    .line 385
    :cond_17
    iput-object p1, p0, Lcom/google/android/location/c;->p:Lcom/google/android/location/e/E;

    goto :goto_16
.end method

.method a(Lcom/google/android/location/e/e;)V
    .registers 2
    .parameter

    .prologue
    .line 414
    return-void
.end method

.method a(Lcom/google/android/location/os/g;)V
    .registers 6
    .parameter

    .prologue
    const/16 v3, 0xa

    const/4 v2, 0x0

    .line 341
    sget-object v0, Lcom/google/android/location/c$1;->a:[I

    iget-object v1, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    invoke-virtual {v1}, Lcom/google/android/location/a$c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_80

    .line 375
    :cond_10
    :goto_10
    :pswitch_10
    return-void

    .line 347
    :pswitch_11
    invoke-direct {p0, p1}, Lcom/google/android/location/c;->b(Lcom/google/android/location/os/g;)Z

    move-result v0

    if-nez v0, :cond_27

    .line 348
    invoke-direct {p0}, Lcom/google/android/location/c;->f()V

    .line 356
    :goto_1a
    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_10

    .line 357
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c;->m:Z

    goto :goto_10

    .line 349
    :cond_27
    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3d

    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    invoke-static {v0, p1, v3}, Lcom/google/android/location/c;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 351
    :cond_3d
    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1a

    .line 353
    :cond_43
    invoke-direct {p0}, Lcom/google/android/location/c;->f()V

    .line 354
    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1a

    .line 361
    :pswitch_4c
    invoke-direct {p0, p1}, Lcom/google/android/location/c;->b(Lcom/google/android/location/os/g;)Z

    move-result v0

    if-eqz v0, :cond_60

    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    invoke-static {v0, p1, v3}, Lcom/google/android/location/c;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z

    move-result v0

    if-nez v0, :cond_10

    .line 363
    :cond_60
    invoke-direct {p0}, Lcom/google/android/location/c;->f()V

    goto :goto_10

    .line 367
    :pswitch_64
    invoke-direct {p0, p1}, Lcom/google/android/location/c;->b(Lcom/google/android/location/os/g;)Z

    move-result v0

    if-eqz v0, :cond_78

    iget-object v0, p0, Lcom/google/android/location/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/g;

    invoke-static {v0, p1, v3}, Lcom/google/android/location/c;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z

    move-result v0

    if-nez v0, :cond_7c

    .line 369
    :cond_78
    invoke-direct {p0}, Lcom/google/android/location/c;->f()V

    goto :goto_10

    .line 371
    :cond_7c
    iput-object p1, p0, Lcom/google/android/location/c;->n:Lcom/google/android/location/os/g;

    goto :goto_10

    .line 341
    nop

    :pswitch_data_80
    .packed-switch 0x2
        :pswitch_64
        :pswitch_4c
        :pswitch_11
        :pswitch_10
        :pswitch_10
    .end packed-switch
.end method

.method a(Lcom/google/android/location/os/h;)V
    .registers 2
    .parameter

    .prologue
    .line 424
    return-void
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 336
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 337
    return-void
.end method

.method a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 392
    iget-boolean v0, p0, Lcom/google/android/location/c;->k:Z

    if-eq v0, p1, :cond_6

    .line 393
    iput-boolean p1, p0, Lcom/google/android/location/c;->k:Z

    .line 395
    :cond_6
    return-void
.end method

.method protected a(J)Z
    .registers 5
    .parameter

    .prologue
    .line 102
    const/4 v0, 0x0

    .line 104
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c;->k(J)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 106
    sget-object v0, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    .line 107
    const/4 v0, 0x1

    .line 109
    :cond_c
    return v0
.end method

.method b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 408
    iput-boolean p1, p0, Lcom/google/android/location/c;->q:Z

    .line 409
    return-void
.end method

.method protected b(J)Z
    .registers 9
    .parameter

    .prologue
    const-wide/32 v4, 0x112a880

    const/4 v0, 0x1

    .line 114
    const/4 v1, 0x0

    .line 117
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c;->k(J)Z

    move-result v2

    if-nez v2, :cond_f

    .line 118
    invoke-direct {p0}, Lcom/google/android/location/c;->e()V

    .line 137
    :goto_e
    return v0

    .line 124
    :cond_f
    iget-wide v2, p0, Lcom/google/android/location/c;->g:J

    sub-long v2, p1, v2

    cmp-long v2, v4, v2

    if-gtz v2, :cond_34

    .line 127
    iget-object v1, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/i;->b(I)V

    .line 128
    sget-object v1, Lcom/google/android/location/c$a;->b:Lcom/google/android/location/c$a;

    iput-object v1, p0, Lcom/google/android/location/c;->j:Lcom/google/android/location/c$a;

    .line 129
    iget-object v1, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/location/c;->g:J

    .line 130
    iget-object v1, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    iget-object v2, p0, Lcom/google/android/location/c;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    .line 131
    sget-object v1, Lcom/google/android/location/a$c;->c:Lcom/google/android/location/a$c;

    iput-object v1, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    goto :goto_e

    .line 135
    :cond_34
    iget-wide v2, p0, Lcom/google/android/location/c;->g:J

    add-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/c;->j(J)V

    move v0, v1

    goto :goto_e
.end method

.method public bridge synthetic c()V
    .registers 1

    .prologue
    .line 33
    invoke-super {p0}, Lcom/google/android/location/a;->c()V

    return-void
.end method

.method c(Z)V
    .registers 2
    .parameter

    .prologue
    .line 419
    return-void
.end method

.method protected c(J)Z
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 142
    const/4 v1, 0x0

    .line 144
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c;->k(J)Z

    move-result v2

    if-nez v2, :cond_c

    .line 145
    invoke-direct {p0}, Lcom/google/android/location/c;->e()V

    .line 161
    :goto_b
    return v0

    .line 151
    :cond_c
    iget-boolean v2, p0, Lcom/google/android/location/c;->m:Z

    if-eqz v2, :cond_1c

    .line 152
    iput-wide p1, p0, Lcom/google/android/location/c;->o:J

    .line 153
    iget-object v1, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    invoke-interface {v1}, Lcom/google/android/location/os/i;->n()V

    .line 154
    sget-object v1, Lcom/google/android/location/a$c;->d:Lcom/google/android/location/a$c;

    iput-object v1, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    goto :goto_b

    .line 158
    :cond_1c
    iget-wide v2, p0, Lcom/google/android/location/c;->g:J

    const-wide/32 v4, 0x1d4c0

    add-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/c;->j(J)V

    move v0, v1

    goto :goto_b
.end method

.method d()Z
    .registers 3

    .prologue
    .line 321
    iget v1, p0, Lcom/google/android/location/c;->h:F

    iget-boolean v0, p0, Lcom/google/android/location/c;->i:Z

    if-eqz v0, :cond_f

    const v0, 0x3e99999a

    :goto_9
    cmpl-float v0, v1, v0

    if-ltz v0, :cond_13

    const/4 v0, 0x1

    :goto_e
    return v0

    :cond_f
    const v0, 0x3f333333

    goto :goto_9

    :cond_13
    const/4 v0, 0x0

    goto :goto_e
.end method

.method protected d(J)Z
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 166
    const/4 v1, 0x0

    .line 169
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c;->k(J)Z

    move-result v2

    if-nez v2, :cond_c

    .line 170
    invoke-direct {p0}, Lcom/google/android/location/c;->e()V

    .line 184
    :goto_b
    return v0

    .line 176
    :cond_c
    iget-object v2, p0, Lcom/google/android/location/c;->p:Lcom/google/android/location/e/E;

    if-eqz v2, :cond_15

    .line 177
    sget-object v1, Lcom/google/android/location/a$c;->e:Lcom/google/android/location/a$c;

    iput-object v1, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    goto :goto_b

    .line 181
    :cond_15
    iget-wide v2, p0, Lcom/google/android/location/c;->g:J

    const-wide/32 v4, 0x1d4c0

    add-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/c;->j(J)V

    move v0, v1

    goto :goto_b
.end method

.method protected e(J)Z
    .registers 11
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 189
    .line 192
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c;->k(J)Z

    move-result v0

    if-nez v0, :cond_d

    .line 193
    invoke-direct {p0}, Lcom/google/android/location/c;->e()V

    .line 218
    :goto_c
    return v6

    .line 199
    :cond_d
    iget-object v0, p0, Lcom/google/android/location/c;->n:Lcom/google/android/location/os/g;

    if-eqz v0, :cond_48

    .line 204
    iget-object v1, p0, Lcom/google/android/location/c;->n:Lcom/google/android/location/os/g;

    iget-object v3, p0, Lcom/google/android/location/c;->p:Lcom/google/android/location/e/E;

    iget-boolean v4, p0, Lcom/google/android/location/c;->i:Z

    const/16 v5, 0xd

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/c;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;ZI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 206
    iput-wide p1, p0, Lcom/google/android/location/c;->s:J

    .line 207
    iget-object v0, p0, Lcom/google/android/location/c;->p:Lcom/google/android/location/e/E;

    iput-object v0, p0, Lcom/google/android/location/c;->t:Lcom/google/android/location/e/E;

    .line 208
    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    iget-object v1, p0, Lcom/google/android/location/c;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/location/c;->e:Lcom/google/android/location/a$b;

    iget-object v1, p0, Lcom/google/android/location/c;->n:Lcom/google/android/location/os/g;

    iget-object v3, p0, Lcom/google/android/location/c;->p:Lcom/google/android/location/e/E;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/a$b;->a(Lcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;)V

    .line 210
    sget-object v0, Lcom/google/android/location/a$c;->f:Lcom/google/android/location/a$c;

    iput-object v0, p0, Lcom/google/android/location/c;->f:Lcom/google/android/location/a$c;

    .line 211
    iget-object v0, p0, Lcom/google/android/location/c;->b:Lcom/google/android/location/os/i;

    iget-object v1, p0, Lcom/google/android/location/c;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v7}, Lcom/google/android/location/os/i;->a(Ljava/lang/String;Z)V

    .line 212
    sget-object v0, Lcom/google/android/location/c$a;->c:Lcom/google/android/location/c$a;

    iput-object v0, p0, Lcom/google/android/location/c;->j:Lcom/google/android/location/c$a;

    move v0, v6

    :goto_46
    move v6, v0

    .line 218
    goto :goto_c

    .line 216
    :cond_48
    iget-wide v0, p0, Lcom/google/android/location/c;->g:J

    const-wide/32 v2, 0x1d4c0

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/c;->j(J)V

    move v0, v7

    goto :goto_46
.end method

.method protected f(J)Z
    .registers 8
    .parameter

    .prologue
    .line 233
    const/4 v0, 0x0

    .line 237
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c;->k(J)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/google/android/location/c;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v1, :cond_10

    .line 238
    :cond_b
    invoke-direct {p0}, Lcom/google/android/location/c;->e()V

    .line 241
    const/4 v0, 0x1

    .line 246
    :goto_f
    return v0

    .line 244
    :cond_10
    iget-wide v1, p0, Lcom/google/android/location/c;->g:J

    const-wide/32 v3, 0x1d4c0

    add-long/2addr v1, v3

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/c;->j(J)V

    goto :goto_f
.end method
