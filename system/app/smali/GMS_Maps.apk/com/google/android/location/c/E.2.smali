.class abstract Lcom/google/android/location/c/E;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/location/k/a/c;

.field protected final b:Lcom/google/android/location/c/l;

.field private final c:Lcom/google/android/location/c/k;

.field private volatile d:Z

.field private volatile e:Z

.field private final f:Ljava/lang/Thread;

.field private final g:Landroid/os/PowerManager$WakeLock;

.field private final h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/c/k;Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-boolean v2, p0, Lcom/google/android/location/c/E;->d:Z

    .line 41
    iput-boolean v2, p0, Lcom/google/android/location/c/E;->e:Z

    .line 47
    new-instance v0, Lcom/google/android/location/c/E$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/E$1;-><init>(Lcom/google/android/location/c/E;)V

    iput-object v0, p0, Lcom/google/android/location/c/E;->h:Ljava/lang/Runnable;

    .line 64
    const-string v0, "No Handler specified!"

    invoke-static {p2, v0}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 67
    const-string v3, "SignalCollector.Scanner"

    invoke-virtual {v0, v1, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/E;->g:Landroid/os/PowerManager$WakeLock;

    .line 69
    iget-object v0, p0, Lcom/google/android/location/c/E;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 70
    iput-object p2, p0, Lcom/google/android/location/c/E;->c:Lcom/google/android/location/c/k;

    .line 71
    invoke-static {p4}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/E;->a:Lcom/google/android/location/k/a/c;

    .line 72
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/E;->f:Ljava/lang/Thread;

    .line 73
    invoke-virtual {p2}, Lcom/google/android/location/c/k;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_4c

    .line 75
    iget-object v3, p0, Lcom/google/android/location/c/E;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    if-ne v3, v0, :cond_4f

    move v0, v1

    :goto_47
    const-string v1, "Scanner should be called in handler\'s thread."

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    .line 78
    :cond_4c
    iput-object p3, p0, Lcom/google/android/location/c/E;->b:Lcom/google/android/location/c/l;

    .line 79
    return-void

    :cond_4f
    move v0, v2

    .line 75
    goto :goto_47
.end method


# virtual methods
.method protected abstract a()V
.end method

.method declared-synchronized a(J)V
    .registers 11
    .parameter

    .prologue
    const-wide v6, 0x7fffffffffffffffL

    const-wide/32 v4, 0x927c0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 90
    monitor-enter p0

    :try_start_b
    invoke-virtual {p0}, Lcom/google/android/location/c/E;->h()V

    .line 91
    iget-boolean v2, p0, Lcom/google/android/location/c/E;->d:Z

    if-nez v2, :cond_54

    move v2, v0

    :goto_13
    const-string v3, "Start should be called only once!"

    invoke-static {v2, v3}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    .line 92
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-lez v2, :cond_22

    cmp-long v2, p1, v4

    if-lez v2, :cond_26

    :cond_22
    cmp-long v2, p1, v6

    if-nez v2, :cond_56

    :cond_26
    :goto_26
    const-string v1, "Duration should be > 0 and <= %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-wide/32 v4, 0x927c0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/location/c/E;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 99
    cmp-long v0, p1, v6

    if-eqz v0, :cond_4c

    .line 100
    iget-object v0, p0, Lcom/google/android/location/c/E;->c:Lcom/google/android/location/c/k;

    iget-object v1, p0, Lcom/google/android/location/c/E;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/location/c/k;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 102
    :cond_4c
    invoke-virtual {p0}, Lcom/google/android/location/c/E;->a()V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/E;->d:Z
    :try_end_52
    .catchall {:try_start_b .. :try_end_52} :catchall_58

    .line 105
    monitor-exit p0

    return-void

    :cond_54
    move v2, v1

    .line 91
    goto :goto_13

    :cond_56
    move v0, v1

    .line 92
    goto :goto_26

    .line 90
    :catchall_58
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract b()V
.end method

.method declared-synchronized d()V
    .registers 3

    .prologue
    .line 112
    monitor-enter p0

    const-wide v0, 0x7fffffffffffffffL

    :try_start_6
    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/c/E;->a(J)V
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_b

    .line 113
    monitor-exit p0

    return-void

    .line 112
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized e()V
    .registers 3

    .prologue
    .line 119
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/E;->d:Z

    const-string v1, "Call start before calling stop!"

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    .line 120
    iget-boolean v0, p0, Lcom/google/android/location/c/E;->e:Z

    if-nez v0, :cond_1e

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/E;->e:Z

    .line 122
    iget-object v0, p0, Lcom/google/android/location/c/E;->c:Lcom/google/android/location/c/k;

    iget-object v1, p0, Lcom/google/android/location/c/E;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/k;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/location/c/E;->b()V

    .line 125
    iget-object v0, p0, Lcom/google/android/location/c/E;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_1e
    .catchall {:try_start_1 .. :try_end_1e} :catchall_20

    .line 130
    :cond_1e
    monitor-exit p0

    return-void

    .line 119
    :catchall_20
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected f()Lcom/google/android/location/c/k;
    .registers 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/location/c/E;->c:Lcom/google/android/location/c/k;

    return-object v0
.end method

.method protected declared-synchronized g()Z
    .registers 2

    .prologue
    .line 137
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/E;->e:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected h()V
    .registers 3

    .prologue
    .line 149
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/E;->f:Ljava/lang/Thread;

    if-ne v0, v1, :cond_f

    const/4 v0, 0x1

    :goto_9
    const-string v1, "Could not be called outside owner thread."

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    .line 151
    return-void

    .line 149
    :cond_f
    const/4 v0, 0x0

    goto :goto_9
.end method
