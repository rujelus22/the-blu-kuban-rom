.class public Lcom/google/android/location/os/real/e;
.super Lcom/google/android/location/h/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/real/e$a;
    }
.end annotation


# static fields
.field private static final a:[B


# instance fields
.field private b:Lcom/google/android/location/h/g;

.field private f:[B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 35
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_a

    sput-object v0, Lcom/google/android/location/os/real/e;->a:[B

    return-void

    nop

    :array_a
    .array-data 0x1
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;I[B)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/h/b/o;-><init>(Ljava/lang/String;I)V

    .line 68
    const/16 v0, 0x101

    invoke-virtual {p0, v0}, Lcom/google/android/location/os/real/e;->c(I)V

    .line 69
    invoke-virtual {p0, p3}, Lcom/google/android/location/os/real/e;->a([B)V

    .line 70
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 253
    if-nez p1, :cond_1b

    .line 254
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "::object is null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 265
    :cond_1b
    :try_start_1b
    invoke-virtual {p1}, Ljava/lang/Object;->notify()V
    :try_end_1e
    .catch Ljava/lang/IllegalMonitorStateException; {:try_start_1b .. :try_end_1e} :catch_1f

    .line 270
    return-void

    .line 266
    :catch_1f
    move-exception v0

    .line 267
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "::monitor on ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is not held by current thread"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private f()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 105
    const-string v0, "MultipartRequest.generatePayloadHeader(): monitor on \'this\' must be held by the current thread"

    invoke-static {v0, p0}, Lcom/google/android/location/os/real/e;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 108
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 109
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 111
    invoke-virtual {p0}, Lcom/google/android/location/os/real/e;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 112
    invoke-virtual {p0}, Lcom/google/android/location/os/real/e;->v()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0}, Lcom/google/android/location/os/real/e;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 114
    const-string v2, "POST"

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 115
    const/16 v2, 0x6d72

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 116
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 117
    const-string v2, "ROOT"

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 118
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 120
    iget-object v2, p0, Lcom/google/android/location/os/real/e;->b:Lcom/google/android/location/h/g;

    if-eqz v2, :cond_63

    iget-object v2, p0, Lcom/google/android/location/os/real/e;->b:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->b_()I

    move-result v2

    if-lez v2, :cond_63

    .line 122
    iget-object v2, p0, Lcom/google/android/location/os/real/e;->b:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->b_()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 123
    const-string v2, "g"

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 129
    :goto_56
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 130
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 132
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/e;->f:[B

    .line 133
    return-void

    .line 125
    :cond_63
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_56
.end method

.method private g()V
    .registers 2

    .prologue
    .line 146
    const-string v0, "MultipartRequest.generateBlockData(): monitor on \'this\' must be held by the current thread"

    invoke-static {v0, p0}, Lcom/google/android/location/os/real/e;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/location/os/real/e;->f:[B

    if-nez v0, :cond_c

    .line 150
    invoke-direct {p0}, Lcom/google/android/location/os/real/e;->f()V

    .line 152
    :cond_c
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 2

    .prologue
    .line 140
    monitor-enter p0

    :try_start_1
    invoke-super {p0}, Lcom/google/android/location/h/b/o;->a()V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/e;->f:[B

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/e;->b:Lcom/google/android/location/h/g;
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 143
    monitor-exit p0

    return-void

    .line 140
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .registers 3
    .parameter

    .prologue
    .line 79
    monitor-enter p0

    :try_start_1
    invoke-super {p0, p1}, Lcom/google/android/location/h/b/o;->a(I)V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/e;->f:[B
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 84
    monitor-exit p0

    return-void

    .line 79
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a([B)V
    .registers 3
    .parameter

    .prologue
    .line 92
    monitor-enter p0

    if-eqz p1, :cond_f

    :try_start_3
    array-length v0, p1

    if-lez v0, :cond_f

    .line 95
    new-instance v0, Lcom/google/android/location/os/real/e$a;

    invoke-direct {v0, p1}, Lcom/google/android/location/os/real/e$a;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/location/os/real/e;->b:Lcom/google/android/location/h/g;
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_13

    .line 99
    :goto_d
    monitor-exit p0

    return-void

    .line 97
    :cond_f
    const/4 v0, 0x0

    :try_start_10
    iput-object v0, p0, Lcom/google/android/location/os/real/e;->b:Lcom/google/android/location/h/g;
    :try_end_12
    .catchall {:try_start_10 .. :try_end_12} :catchall_13

    goto :goto_d

    .line 92
    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Ljava/io/InputStream;
    .registers 6

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/google/android/location/os/real/e;->g()V

    .line 157
    iget-object v0, p0, Lcom/google/android/location/os/real/e;->b:Lcom/google/android/location/h/g;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/location/os/real/e;->b:Lcom/google/android/location/h/g;

    invoke-interface {v0}, Lcom/google/android/location/h/g;->b_()I

    move-result v0

    if-nez v0, :cond_23

    .line 159
    :cond_f
    new-instance v0, Lcom/google/googlenav/common/io/SequenceInputStream;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/google/android/location/os/real/e;->f:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v2, Ljava/io/ByteArrayInputStream;

    sget-object v3, Lcom/google/android/location/os/real/e;->a:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/common/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    .line 164
    :goto_22
    return-object v0

    :cond_23
    new-instance v0, Lcom/google/googlenav/common/io/SequenceInputStream;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/google/android/location/os/real/e;->f:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget-object v2, p0, Lcom/google/android/location/os/real/e;->b:Lcom/google/android/location/h/g;

    invoke-interface {v2}, Lcom/google/android/location/h/g;->c_()Ljava/io/InputStream;

    move-result-object v2

    new-instance v3, Ljava/io/ByteArrayInputStream;

    sget-object v4, Lcom/google/android/location/os/real/e;->a:[B

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;Ljava/io/InputStream;)V

    goto :goto_22
.end method

.method protected c()I
    .registers 3

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/android/location/os/real/e;->g()V

    .line 172
    iget-object v0, p0, Lcom/google/android/location/os/real/e;->f:[B

    array-length v0, v0

    sget-object v1, Lcom/google/android/location/os/real/e;->a:[B

    array-length v1, v1

    add-int/2addr v0, v1

    .line 173
    iget-object v1, p0, Lcom/google/android/location/os/real/e;->b:Lcom/google/android/location/h/g;

    if-eqz v1, :cond_15

    .line 174
    iget-object v1, p0, Lcom/google/android/location/os/real/e;->b:Lcom/google/android/location/h/g;

    invoke-interface {v1}, Lcom/google/android/location/h/g;->b_()I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_15
    return v0
.end method
