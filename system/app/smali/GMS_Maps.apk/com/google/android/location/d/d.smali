.class public Lcom/google/android/location/d/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/d/e;


# static fields
.field private static final a:Lcom/google/android/location/d/d;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/location/d/d;

    invoke-direct {v0}, Lcom/google/android/location/d/d;-><init>()V

    sput-object v0, Lcom/google/android/location/d/d;->a:Lcom/google/android/location/d/d;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static a()Lcom/google/android/location/d/d;
    .registers 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/location/d/d;->a:Lcom/google/android/location/d/d;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Z
    .registers 2
    .parameter

    .prologue
    .line 54
    if-eqz p0, :cond_c

    const-string v0, "_nomap"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 41
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    iget-object v0, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    .line 42
    invoke-static {v0}, Lcom/google/android/location/d/d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 43
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 46
    :cond_1c
    return-void
.end method

.method public a(Lcom/google/android/location/e/E$a;)Z
    .registers 3
    .parameter

    .prologue
    .line 50
    iget-object v0, p1, Lcom/google/android/location/e/E$a;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/location/d/d;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method
