.class Lcom/google/android/location/f$a;
.super Lcom/google/android/location/c/J;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/f;


# direct methods
.method private constructor <init>(Lcom/google/android/location/f;)V
    .registers 2
    .parameter

    .prologue
    .line 317
    iput-object p1, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-direct {p0}, Lcom/google/android/location/c/J;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/f;Lcom/google/android/location/f$1;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 317
    invoke-direct {p0, p1}, Lcom/google/android/location/f$a;-><init>(Lcom/google/android/location/f;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;III)Lcom/google/android/location/e/u;
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "III)",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 408
    const/4 v5, 0x0

    .line 410
    const-wide/16 v3, 0x0

    .line 411
    const/4 v2, 0x0

    .line 412
    const/4 v1, 0x0

    :goto_a
    const/4 v6, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    if-ge v1, v6, :cond_89

    .line 413
    const/4 v6, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    .line 414
    const/16 v7, 0x9

    invoke-virtual {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_86

    .line 415
    const/16 v7, 0x9

    invoke-virtual {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v11

    .line 416
    const/4 v7, 0x1

    .line 417
    const/4 v6, 0x0

    move/from16 v16, v6

    move/from16 v17, v7

    move v7, v5

    move-wide v5, v3

    move v4, v2

    move/from16 v3, v17

    move/from16 v2, v16

    :goto_35
    move/from16 v0, p2

    invoke-virtual {v11, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v8

    if-ge v2, v8, :cond_83

    .line 418
    move/from16 v0, p2

    invoke-virtual {v11, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v9

    .line 419
    add-int/lit8 v7, v7, 0x1

    .line 420
    int-to-long v12, v7

    const-wide/16 v14, 0xa

    cmp-long v8, v12, v14

    if-lez v8, :cond_52

    .line 421
    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 422
    if-eqz v3, :cond_55

    .line 427
    const/4 v3, 0x0

    .line 417
    :cond_52
    :goto_52
    add-int/lit8 v2, v2, 0x1

    goto :goto_35

    .line 428
    :cond_55
    const/16 v8, 0x1388

    if-ge v4, v8, :cond_52

    .line 429
    move/from16 v0, p3

    invoke-virtual {v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_7f

    move/from16 v0, p3

    invoke-virtual {v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v8

    .line 430
    :goto_67
    move/from16 v0, p4

    invoke-virtual {v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v12

    if-eqz v12, :cond_81

    move/from16 v0, p4

    invoke-virtual {v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v9

    .line 431
    :goto_75
    int-to-long v12, v8

    const-wide/16 v14, 0x3e8

    mul-long/2addr v12, v14

    int-to-long v8, v9

    add-long/2addr v8, v12

    add-long/2addr v5, v8

    .line 432
    add-int/lit8 v4, v4, 0x1

    goto :goto_52

    .line 429
    :cond_7f
    const/4 v8, 0x0

    goto :goto_67

    .line 430
    :cond_81
    const/4 v9, 0x0

    goto :goto_75

    :cond_83
    move v2, v4

    move-wide v3, v5

    move v5, v7

    .line 412
    :cond_86
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 438
    :cond_89
    const/16 v1, 0x64

    if-ge v2, v1, :cond_8f

    .line 440
    const/4 v1, 0x0

    .line 444
    :goto_8e
    return-object v1

    .line 442
    :cond_8f
    long-to-double v3, v3

    int-to-double v1, v2

    div-double v1, v3, v1

    .line 444
    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v10, v1}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v1

    goto :goto_8e
.end method

.method private a(Z)V
    .registers 5
    .parameter

    .prologue
    .line 469
    if-eqz p1, :cond_c

    .line 470
    iget-object v0, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    iget-object v1, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v1}, Lcom/google/android/location/f;->e(Lcom/google/android/location/f;)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/google/android/location/f;->h:J

    .line 472
    :cond_c
    iget-object v0, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    sget-object v1, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    iput-object v1, v0, Lcom/google/android/location/f;->f:Lcom/google/android/location/a$c;

    .line 475
    iget-object v0, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/location/f;->a(Lcom/google/android/location/f;J)V

    .line 476
    return-void
.end method

.method private a(DD)Z
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 382
    iget-object v0, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v0}, Lcom/google/android/location/f;->a(Lcom/google/android/location/f;)I

    move-result v0

    .line 383
    const-wide v1, 0x415b3f7249249249L

    cmpg-double v1, p1, v1

    if-gez v1, :cond_1b

    .line 384
    iget-object v0, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v0}, Lcom/google/android/location/f;->a(Lcom/google/android/location/f;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/location/a/l;->a(I)I

    move-result v0

    .line 386
    :cond_1b
    iget-object v1, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v1}, Lcom/google/android/location/f;->b(Lcom/google/android/location/f;)I

    move-result v1

    .line 387
    const-wide v5, 0x416d5804e0000000L

    cmpg-double v2, p3, v5

    if-gez v2, :cond_34

    .line 388
    iget-object v1, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v1}, Lcom/google/android/location/f;->b(Lcom/google/android/location/f;)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/location/a/l;->a(I)I

    move-result v1

    .line 390
    :cond_34
    iget-object v2, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v2}, Lcom/google/android/location/f;->a(Lcom/google/android/location/f;)I

    move-result v2

    if-ne v0, v2, :cond_44

    iget-object v2, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v2}, Lcom/google/android/location/f;->b(Lcom/google/android/location/f;)I

    move-result v2

    if-eq v1, v2, :cond_7d

    :cond_44
    move v2, v4

    .line 391
    :goto_45
    iget-object v5, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v5}, Lcom/google/android/location/f;->c(Lcom/google/android/location/f;)Lcom/google/android/location/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/location/g;->h()Z

    move-result v5

    if-eqz v5, :cond_5f

    iget-object v5, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v5}, Lcom/google/android/location/f;->c(Lcom/google/android/location/f;)Lcom/google/android/location/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/location/g;->i()Z

    move-result v5

    if-eqz v5, :cond_5f

    if-eqz v2, :cond_7a

    .line 394
    :cond_5f
    iget-object v5, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v5}, Lcom/google/android/location/f;->c(Lcom/google/android/location/f;)Lcom/google/android/location/g;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/location/g;->a(I)V

    .line 395
    iget-object v0, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v0}, Lcom/google/android/location/f;->c(Lcom/google/android/location/f;)Lcom/google/android/location/g;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/location/g;->b(I)V

    .line 396
    iget-object v0, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v0}, Lcom/google/android/location/f;->c(Lcom/google/android/location/f;)Lcom/google/android/location/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/g;->b()V

    .line 398
    :cond_7a
    if-eqz v2, :cond_7f

    .line 402
    :goto_7c
    return v4

    :cond_7d
    move v2, v3

    .line 390
    goto :goto_45

    :cond_7f
    move v4, v3

    .line 402
    goto :goto_7c
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    iget-object v0, v0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v0

    .line 449
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2d

    .line 450
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    .line 451
    if-nez v1, :cond_2d

    .line 452
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to create sensorCacheDir: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 455
    :cond_2d
    new-instance v1, Ljava/io/File;

    const-string v2, "calibration"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 456
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 457
    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 458
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 459
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 10
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x7

    const/4 v7, 0x1

    .line 327
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/google/android/location/f$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;III)Lcom/google/android/location/e/u;

    move-result-object v1

    .line 331
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/google/android/location/f$a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;III)Lcom/google/android/location/e/u;

    move-result-object v2

    .line 335
    if-eqz v1, :cond_12

    if-nez v2, :cond_16

    .line 336
    :cond_12
    invoke-direct {p0, v7}, Lcom/google/android/location/f$a;->a(Z)V

    .line 369
    :goto_15
    return-void

    .line 340
    :cond_16
    iget-object v0, v1, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    iget-object v0, v2, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/location/f$a;->a(DD)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 344
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/f$a;->a(Z)V

    goto :goto_15

    .line 352
    :cond_31
    iget-object v0, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    iget-object v0, v0, Lcom/google/android/location/f;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v2

    iget-object v0, v1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v2, v3, v0}, Lcom/google/android/location/f;->a(JLjava/util/List;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 355
    if-eqz v0, :cond_55

    .line 357
    :try_start_43
    invoke-direct {p0, v0}, Lcom/google/android/location/f$a;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 358
    iget-object v1, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v1, v0}, Lcom/google/android/location/f;->a(Lcom/google/android/location/f;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 360
    iget-object v1, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    iget-object v2, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v2, v0}, Lcom/google/android/location/f;->b(Lcom/google/android/location/f;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/location/f;->g:J
    :try_end_55
    .catch Ljava/io/IOException; {:try_start_43 .. :try_end_55} :catch_59

    .line 368
    :cond_55
    :goto_55
    invoke-direct {p0, v7}, Lcom/google/android/location/f$a;->a(Z)V

    goto :goto_15

    .line 362
    :catch_59
    move-exception v0

    goto :goto_55
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/location/f$a;->a:Lcom/google/android/location/f;

    invoke-static {v0}, Lcom/google/android/location/f;->d(Lcom/google/android/location/f;)Lcom/google/android/location/c/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/c/r;->b()V

    .line 465
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/f$a;->a(Z)V

    .line 466
    return-void
.end method
