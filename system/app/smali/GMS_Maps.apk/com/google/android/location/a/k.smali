.class Lcom/google/android/location/a/k;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IIIIII)Ljava/util/List;
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "IIIIII)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 75
    const/4 v1, 0x0

    :goto_6
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-ge v1, v2, :cond_87

    .line 76
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 77
    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_84

    .line 78
    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    .line 79
    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    .line 80
    const/4 v2, 0x0

    :goto_26
    move/from16 v0, p2

    invoke-virtual {v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    if-ge v2, v5, :cond_84

    .line 81
    move/from16 v0, p2

    invoke-virtual {v8, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    .line 82
    const/4 v5, 0x3

    new-array v9, v5, [F

    .line 83
    const/4 v5, 0x0

    move/from16 v0, p5

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v10

    aput v10, v9, v5

    .line 84
    const/4 v5, 0x1

    move/from16 v0, p6

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v10

    aput v10, v9, v5

    .line 85
    const/4 v5, 0x2

    move/from16 v0, p7

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v10

    aput v10, v9, v5

    .line 86
    move/from16 v0, p3

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_80

    move/from16 v0, p3

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    .line 87
    :goto_60
    move/from16 v0, p4

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v10

    if-eqz v10, :cond_82

    move/from16 v0, p4

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    .line 88
    :goto_6e
    int-to-long v10, v5

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    add-long/2addr v3, v10

    int-to-long v5, v6

    add-long/2addr v3, v5

    .line 89
    new-instance v5, Lcom/google/android/location/a/j;

    invoke-direct {v5, v3, v4, v9}, Lcom/google/android/location/a/j;-><init>(J[F)V

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    add-int/lit8 v2, v2, 0x1

    goto :goto_26

    .line 86
    :cond_80
    const/4 v5, 0x0

    goto :goto_60

    .line 87
    :cond_82
    const/4 v6, 0x0

    goto :goto_6e

    .line 75
    :cond_84
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 94
    :cond_87
    return-object v7
.end method


# virtual methods
.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/F;)Ljava/util/List;
    .registers 11
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "Lcom/google/android/location/c/F;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    if-ne p2, v0, :cond_12

    .line 38
    const/4 v2, 0x3

    const/4 v3, 0x7

    const/16 v4, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/a/k;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IIIIII)Ljava/util/List;

    move-result-object v0

    .line 54
    :goto_11
    return-object v0

    .line 45
    :cond_12
    sget-object v0, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    if-ne p2, v0, :cond_24

    .line 46
    const/4 v2, 0x4

    const/4 v3, 0x7

    const/16 v4, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/a/k;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IIIIII)Ljava/util/List;

    move-result-object v0

    goto :goto_11

    .line 53
    :cond_24
    sget-object v0, Lcom/google/android/location/c/F;->f:Lcom/google/android/location/c/F;

    if-ne p2, v0, :cond_37

    .line 54
    const/4 v2, 0x5

    const/16 v3, 0x8

    const/16 v4, 0x9

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/a/k;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IIIIII)Ljava/util/List;

    move-result-object v0

    goto :goto_11

    .line 62
    :cond_37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported scanner type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
