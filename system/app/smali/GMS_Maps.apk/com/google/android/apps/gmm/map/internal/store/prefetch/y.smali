.class public Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/Class;

.field private volatile c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

.field private d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

.field private e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

.field private f:Z

.field private final g:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    .line 145
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->g:Landroid/content/ServiceConnection;

    .line 60
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    .line 61
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a:Landroid/content/Context;

    .line 62
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->b:Ljava/lang/Class;

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    .line 145
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/z;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->g:Landroid/content/ServiceConnection;

    .line 47
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->b:Ljava/lang/Class;

    .line 48
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a:Landroid/content/Context;

    .line 49
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;)Z
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->f:Z

    return v0
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 5

    .prologue
    .line 67
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->b:Ljava/lang/Class;

    if-eqz v0, :cond_16

    .line 69
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->b:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 71
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->g:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_18

    .line 73
    :cond_16
    monitor-exit p0

    return-void

    .line 67
    :catchall_18
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 103
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->f()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_16

    .line 110
    :goto_c
    monitor-exit p0

    return-void

    .line 106
    :cond_e
    :try_start_e
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    .line 107
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->f:Z
    :try_end_15
    .catchall {:try_start_e .. :try_end_15} :catchall_16

    goto :goto_c

    .line 103
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Li/f;)V
    .registers 3
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    if-eqz v0, :cond_9

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Li/f;)V

    .line 132
    :cond_9
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    if-eqz v0, :cond_9

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->f()V

    .line 85
    :cond_9
    return-void
.end method

.method public declared-synchronized b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 118
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->f()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_16

    .line 125
    :goto_c
    monitor-exit p0

    return-void

    .line 121
    :cond_e
    :try_start_e
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    .line 122
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->f:Z
    :try_end_15
    .catchall {:try_start_e .. :try_end_15} :catchall_16

    goto :goto_c

    .line 118
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Lcom/google/android/apps/gmm/map/internal/store/prefetch/A;
    .registers 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/A;

    move-result-object v0

    return-object v0
.end method

.method public d()Landroid/content/Context;
    .registers 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->a:Landroid/content/Context;

    return-object v0
.end method

.method public e()V
    .registers 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->h()V

    .line 176
    return-void
.end method

.method public declared-synchronized f()Z
    .registers 2

    .prologue
    .line 77
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_a

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_6
    monitor-exit p0

    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_6

    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method
