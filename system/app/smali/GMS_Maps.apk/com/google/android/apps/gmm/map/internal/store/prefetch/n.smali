.class final Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 449
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    .line 450
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 451
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter

    .prologue
    .line 455
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_6a

    .line 508
    :goto_5
    return-void

    .line 458
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e()V

    goto :goto_5

    .line 461
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V

    goto :goto_5

    .line 468
    :pswitch_12
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V

    goto :goto_5

    .line 472
    :pswitch_18
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    .line 473
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V

    goto :goto_5

    .line 477
    :pswitch_22
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V

    goto :goto_5

    .line 482
    :pswitch_28
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 484
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    goto :goto_5

    .line 490
    :pswitch_3a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 492
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    goto :goto_5

    .line 497
    :pswitch_4c
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Li/f;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Li/f;)V

    goto :goto_5

    .line 501
    :pswitch_56
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->d(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    move-result-object v0

    const/4 v1, 0x6

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_5

    .line 505
    :pswitch_63
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->stopSelf()V

    goto :goto_5

    .line 455
    nop

    :pswitch_data_6a
    .packed-switch 0x0
        :pswitch_6
        :pswitch_c
        :pswitch_12
        :pswitch_18
        :pswitch_22
        :pswitch_28
        :pswitch_63
        :pswitch_56
        :pswitch_3a
        :pswitch_4c
    .end packed-switch
.end method
