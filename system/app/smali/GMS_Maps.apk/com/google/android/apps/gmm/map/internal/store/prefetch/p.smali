.class public Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
.implements Li/f;


# instance fields
.field private final a:Li/f;

.field private final b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

.field private volatile c:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Li/f;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->a:Li/f;

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    .line 25
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->i()V

    .line 26
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->a:Li/f;

    invoke-interface {v0}, Li/f;->a()V

    .line 36
    return-void
.end method

.method public a(Lo/aq;)Z
    .registers 3
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->a(Lo/aq;)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->a:Li/f;

    invoke-interface {v0}, Li/f;->b()Z

    move-result v0

    return v0
.end method

.method public c()Lo/aq;
    .registers 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b()Z

    move-result v0

    if-nez v0, :cond_15

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->c:I

    if-ltz v0, :cond_15

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->c()Lo/aq;

    move-result-object v0

    :goto_14
    return-object v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public d()V
    .registers 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->d()V

    .line 31
    return-void
.end method

.method public e()I
    .registers 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->e()I

    move-result v0

    return v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->f()I

    move-result v0

    return v0
.end method

.method public g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 60
    const/4 v0, 0x0

    return-object v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->h()I

    move-result v0

    return v0
.end method

.method i()V
    .registers 2

    .prologue
    .line 75
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->l()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->c:I

    .line 76
    return-void
.end method
