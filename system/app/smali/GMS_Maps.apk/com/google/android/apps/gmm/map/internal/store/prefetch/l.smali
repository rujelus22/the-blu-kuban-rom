.class Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

.field private b:I

.field private c:I

.field private d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

.field private e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

.field private f:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;ILcom/google/android/apps/gmm/map/internal/store/prefetch/t;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;J)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 814
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    .line 815
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->b:I

    .line 816
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->h()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->c:I

    .line 817
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    .line 818
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    .line 819
    iput-wide p5, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f:J

    .line 820
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;
    .registers 2
    .parameter

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;
    .registers 2
    .parameter

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
    .registers 2
    .parameter

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
    .registers 2

    .prologue
    .line 823
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    return-object v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 827
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->b:I

    return v0
.end method

.method public c()V
    .registers 2

    .prologue
    .line 831
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->b:I

    .line 832
    return-void
.end method

.method public d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;
    .registers 2

    .prologue
    .line 835
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    return-object v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 839
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->c:I

    return v0
.end method

.method public f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;
    .registers 2

    .prologue
    .line 843
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    return-object v0
.end method

.method public g()J
    .registers 3

    .prologue
    .line 847
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f:J

    return-wide v0
.end method
