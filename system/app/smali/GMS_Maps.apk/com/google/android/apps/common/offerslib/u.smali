.class Lcom/google/android/apps/common/offerslib/u;
.super Lcom/google/commerce/wireless/topiary/HybridWebViewControl;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Landroid/content/Context;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/D;Lcom/google/commerce/wireless/topiary/z;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/u;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    .line 79
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;-><init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/D;Lcom/google/commerce/wireless/topiary/z;)V

    .line 80
    return-void
.end method


# virtual methods
.method public a(Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 83
    if-eqz p2, :cond_24

    const-string v0, "geo:0,0?q"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 84
    const-string v0, "OfferDetailsFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ignoring loading of URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const/4 v0, 0x1

    .line 87
    :goto_23
    return v0

    :cond_24
    invoke-super {p0, p1, p2}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a(Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_23
.end method
