.class public Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;
.super Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Li/g;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;-><init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Li/g;)V

    .line 23
    return-void
.end method


# virtual methods
.method public c()Z
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 27
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i()Z

    move-result v0

    if-nez v0, :cond_47

    move v0, v1

    .line 28
    :goto_b
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->b:Lr/I;

    invoke-virtual {v3}, Lr/I;->d()Z

    move-result v3

    .line 29
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v4

    .line 31
    if-eqz v0, :cond_1b

    if-eqz v3, :cond_1b

    if-nez v4, :cond_46

    .line 34
    :cond_1b
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v0, :cond_49

    const-string v0, "r"

    :goto_24
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v3, :cond_4c

    const-string v0, "c"

    :goto_2c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v4, :cond_4f

    const-string v0, "n"

    :goto_34
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    const-string v4, "n"

    invoke-virtual {v1, v3, v4, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 46
    :cond_46
    return v1

    :cond_47
    move v0, v2

    .line 27
    goto :goto_b

    .line 34
    :cond_49
    const-string v0, ""

    goto :goto_24

    :cond_4c
    const-string v0, ""

    goto :goto_2c

    :cond_4f
    const-string v0, ""

    goto :goto_34
.end method

.method protected d()Z
    .registers 5

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->b:Lr/I;

    invoke-virtual {v0}, Lr/I;->d()Z

    move-result v0

    .line 52
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v1

    .line 54
    if-eqz v0, :cond_e

    if-nez v1, :cond_38

    .line 57
    :cond_e
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v0, :cond_32

    const-string v0, "c"

    :goto_17
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v1, :cond_35

    const-string v0, "n"

    :goto_1f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    const-string v3, "u"

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const/4 v0, 0x0

    .line 68
    :goto_31
    return v0

    .line 57
    :cond_32
    const-string v0, ""

    goto :goto_17

    :cond_35
    const-string v0, ""

    goto :goto_1f

    .line 68
    :cond_38
    const/4 v0, 0x1

    goto :goto_31
.end method
