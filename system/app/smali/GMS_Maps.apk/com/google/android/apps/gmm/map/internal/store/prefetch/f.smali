.class Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ls/e;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;

.field final synthetic c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

.field final synthetic d:I

.field final synthetic e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

.field private f:I

.field private g:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;ILcom/google/android/apps/gmm/map/internal/store/prefetch/p;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;I)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 651
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->a:I

    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;

    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    iput p5, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 652
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->f:I

    .line 653
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->g:I

    return-void
.end method


# virtual methods
.method public a(Lo/aq;ILo/ap;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 659
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 686
    :cond_8
    :goto_8
    return-void

    .line 664
    :cond_9
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->f:I

    .line 665
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->g:I

    .line 669
    if-eqz p2, :cond_23

    const/4 v0, 0x2

    if-eq p2, v0, :cond_23

    .line 670
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    const/4 v0, 0x1

    if-ne p2, v0, :cond_36

    const/4 v0, 0x5

    :goto_20
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(I)V

    .line 674
    :cond_23
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->d:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->g:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(II)V

    .line 675
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->g:I

    if-nez v0, :cond_38

    .line 676
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->b()V

    goto :goto_8

    .line 670
    :cond_36
    const/4 v0, 0x6

    goto :goto_20

    .line 680
    :cond_38
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->f:I

    if-nez v0, :cond_8

    .line 681
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->f:I

    .line 682
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->i()V

    .line 683
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;

    sget-object v2, Lr/c;->c:Lr/c;

    invoke-virtual {v0, v1, v2, p0}, Lr/I;->a(Lo/ar;Lr/c;Ls/e;)V

    goto :goto_8
.end method
