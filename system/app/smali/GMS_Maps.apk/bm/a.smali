.class public Lbm/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/io/InputStream;)I
    .registers 3
    .parameter

    .prologue
    .line 72
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 73
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 74
    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 76
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public static a(Ljava/lang/String;)I
    .registers 3
    .parameter

    .prologue
    .line 89
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 91
    :try_start_5
    invoke-static {v0}, Lbm/a;->a(Ljava/io/InputStream;)I
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_d

    move-result v1

    .line 93
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return v1

    :catchall_d
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v1
.end method

.method public static a(Ljava/lang/String;IZ)Lam/f;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 199
    new-instance v0, Lan/f;

    invoke-static {p0, p1, p2}, Lbm/a;->b(Ljava/lang/String;IZ)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Lan/f;-><init>(Landroid/graphics/Bitmap;)V

    .line 202
    invoke-static {p0}, Lbm/a;->c(Ljava/lang/String;)I

    move-result v1

    .line 209
    if-eqz v1, :cond_13

    .line 210
    invoke-interface {v0, v1}, Lam/f;->a(I)Lam/f;

    move-result-object v0

    .line 213
    :cond_13
    return-object v0
.end method

.method public static a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 224
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 225
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 227
    if-ne v0, v1, :cond_10

    .line 228
    invoke-static {p0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 235
    :goto_f
    return-object v0

    .line 230
    :cond_10
    if-le v0, v1, :cond_1a

    .line 232
    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    invoke-static {p0, v0, v2, v1, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_f

    .line 235
    :cond_1a
    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    invoke-static {p0, v2, v1, v0, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_f
.end method

.method public static a(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/lang/String;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 292
    .line 293
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 295
    const-string v1, "content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 296
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v4

    move-object v0, p1

    move-object v1, p0

    move-object v4, v3

    move-object v5, v3

    .line 299
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 300
    if-nez v1, :cond_20

    .line 316
    :cond_1f
    :goto_1f
    return-object v3

    .line 305
    :cond_20
    :try_start_20
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 306
    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 307
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_30
    .catchall {:try_start_20 .. :try_end_30} :catchall_35

    move-result-object v3

    .line 310
    :cond_31
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1f

    :catchall_35
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 312
    :cond_3a
    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 313
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    goto :goto_1f
.end method

.method public static a(Ljava/io/InputStream;II)[B
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lbm/a;->a(Ljava/io/InputStream;III)[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;III)[B
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 123
    int-to-double v0, p1

    int-to-double v2, p2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/StrictMath;->sqrt(D)D

    move-result-wide v0

    .line 124
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 125
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 127
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 128
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v0, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 129
    const/4 v0, 0x0

    invoke-static {p0, v0, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 131
    if-eqz p3, :cond_33

    .line 132
    new-instance v1, Lan/f;

    invoke-direct {v1, v0}, Lan/f;-><init>(Landroid/graphics/Bitmap;)V

    .line 133
    invoke-virtual {v1, p3}, Lan/f;->a(I)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    .line 134
    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 135
    invoke-virtual {v1}, Lan/f;->d()V

    .line 138
    :cond_33
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 139
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x4b

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 142
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 143
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;II)[B
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 157
    invoke-static {p0}, Lbm/a;->c(Ljava/lang/String;)I

    move-result v0

    .line 158
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 160
    :try_start_9
    invoke-static {v1, p1, p2, v0}, Lbm/a;->a(Ljava/io/InputStream;III)[B
    :try_end_c
    .catchall {:try_start_9 .. :try_end_c} :catchall_11

    move-result-object v0

    .line 162
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object v0

    :catchall_11
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method public static b(Ljava/lang/String;IZ)Landroid/graphics/Bitmap;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 251
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 255
    :try_start_6
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 256
    const/4 v3, 0x1

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 257
    const/4 v3, 0x0

    invoke-static {v1, v3, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 258
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_15
    .catchall {:try_start_6 .. :try_end_15} :catchall_41

    .line 261
    :try_start_15
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 262
    div-int/2addr v0, p1

    .line 265
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_23
    .catchall {:try_start_15 .. :try_end_23} :catchall_48

    .line 266
    :try_start_23
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 267
    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 268
    const/4 v0, 0x0

    invoke-static {v1, v0, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 271
    if-eqz p2, :cond_3b

    .line 272
    invoke-static {v0}, Lbm/a;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_34
    .catchall {:try_start_23 .. :try_end_34} :catchall_41

    move-result-object v0

    .line 277
    if-eqz v1, :cond_3a

    .line 278
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    :cond_3a
    :goto_3a
    return-object v0

    .line 277
    :cond_3b
    if-eqz v1, :cond_3a

    .line 278
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    goto :goto_3a

    .line 277
    :catchall_41
    move-exception v0

    :goto_42
    if-eqz v1, :cond_47

    .line 278
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    :cond_47
    throw v0

    .line 277
    :catchall_48
    move-exception v0

    move-object v1, v2

    goto :goto_42
.end method

.method public static b(Ljava/lang/String;II)[B
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 178
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 180
    const/4 v1, 0x0

    :try_start_6
    invoke-static {v0, p1, p2, v1}, Lbm/a;->a(Ljava/io/InputStream;III)[B
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_e

    move-result-object v1

    .line 182
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-object v1

    :catchall_e
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v1
.end method

.method public static b(Ljava/lang/String;)[F
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 328
    :try_start_1
    new-instance v2, Landroid/media/ExifInterface;

    invoke-direct {v2, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 329
    const/4 v0, 0x2

    new-array v0, v0, [F

    .line 330
    invoke-virtual {v2, v0}, Landroid/media/ExifInterface;->getLatLong([F)Z
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_c} :catch_10

    move-result v2

    if-eqz v2, :cond_13

    .line 336
    :goto_f
    return-object v0

    .line 333
    :catch_10
    move-exception v0

    move-object v0, v1

    .line 334
    goto :goto_f

    :cond_13
    move-object v0, v1

    .line 336
    goto :goto_f
.end method

.method private static c(Ljava/lang/String;)I
    .registers 4
    .parameter

    .prologue
    .line 45
    new-instance v0, Landroid/media/ExifInterface;

    invoke-direct {v0, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 47
    const-string v1, "Orientation"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    .line 50
    packed-switch v0, :pswitch_data_1a

    .line 61
    :pswitch_f
    const/4 v0, 0x0

    :goto_10
    return v0

    .line 52
    :pswitch_11
    const/16 v0, 0x5a

    goto :goto_10

    .line 55
    :pswitch_14
    const/16 v0, 0xb4

    goto :goto_10

    .line 58
    :pswitch_17
    const/16 v0, 0x10e

    goto :goto_10

    .line 50
    :pswitch_data_1a
    .packed-switch 0x3
        :pswitch_14
        :pswitch_f
        :pswitch_f
        :pswitch_11
        :pswitch_f
        :pswitch_17
    .end packed-switch
.end method
