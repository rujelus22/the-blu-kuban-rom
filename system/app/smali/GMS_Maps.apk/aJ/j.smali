.class public LaJ/j;
.super LaJ/f;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/LinkedList;

.field private b:LaJ/l;

.field private final c:LaJ/k;

.field private volatile d:I

.field private e:J

.field private final f:LaJ/w;


# direct methods
.method public constructor <init>(LaJ/w;)V
    .registers 4
    .parameter

    .prologue
    .line 93
    invoke-direct {p0}, LaJ/f;-><init>()V

    .line 72
    const-wide/16 v0, 0x64

    iput-wide v0, p0, LaJ/j;->e:J

    .line 94
    iput-object p1, p0, LaJ/j;->f:LaJ/w;

    .line 95
    new-instance v0, LaJ/k;

    invoke-direct {v0, p0}, LaJ/k;-><init>(LaJ/j;)V

    iput-object v0, p0, LaJ/j;->c:LaJ/k;

    .line 96
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LaJ/j;->a:Ljava/util/LinkedList;

    .line 97
    const/4 v0, 0x0

    iput v0, p0, LaJ/j;->d:I

    .line 98
    return-void
.end method

.method static synthetic a(LaJ/j;)LaJ/l;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, LaJ/j;->b:LaJ/l;

    return-object v0
.end method

.method static synthetic a(LaJ/j;LaJ/l;)LaJ/l;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, LaJ/j;->b:LaJ/l;

    return-object p1
.end method

.method static synthetic b(LaJ/j;)Ljava/util/LinkedList;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, LaJ/j;->a:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic c(LaJ/j;)J
    .registers 3
    .parameter

    .prologue
    .line 50
    iget-wide v0, p0, LaJ/j;->e:J

    return-wide v0
.end method

.method static synthetic d(LaJ/j;)I
    .registers 2
    .parameter

    .prologue
    .line 50
    iget v0, p0, LaJ/j;->d:I

    return v0
.end method

.method static synthetic e(LaJ/j;)I
    .registers 2
    .parameter

    .prologue
    .line 50
    invoke-super {p0}, LaJ/f;->j()I

    move-result v0

    return v0
.end method

.method static synthetic f(LaJ/j;)LaJ/w;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, LaJ/j;->f:LaJ/w;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized K_()Z
    .registers 2

    .prologue
    .line 113
    monitor-enter p0

    :try_start_1
    invoke-super {p0}, LaJ/f;->K_()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, LaJ/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, LaJ/j;->b:LaJ/l;
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_18

    if-nez v0, :cond_16

    const/4 v0, 0x1

    :goto_14
    monitor-exit p0

    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_14

    :catchall_18
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)I
    .registers 3
    .parameter

    .prologue
    .line 154
    const/4 v0, 0x3

    if-ne p1, v0, :cond_5

    const/4 v0, 0x2

    :goto_4
    return v0

    :cond_5
    const/4 v0, 0x1

    goto :goto_4
.end method

.method protected a_(LaJ/s;)V
    .registers 8
    .parameter

    .prologue
    .line 123
    invoke-static {}, LaJ/o;->a()LaJ/o;

    move-result-object v1

    .line 124
    monitor-enter p0

    .line 127
    :try_start_5
    new-instance v0, LaJ/l;

    invoke-virtual {v1}, LaJ/o;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v1}, LaJ/o;->f()Lau/B;

    move-result-object v4

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LaJ/l;-><init>(LaJ/j;LaJ/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lau/B;I)V

    iput-object v0, p0, LaJ/j;->b:LaJ/l;

    .line 129
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_5 .. :try_end_18} :catchall_2c

    .line 130
    iget-object v0, p0, LaJ/j;->c:LaJ/k;

    invoke-virtual {v0}, LaJ/k;->a()V

    .line 137
    invoke-static {p1}, LaJ/j;->f(LaJ/s;)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 140
    new-instance v0, LaJ/z;

    invoke-direct {v0, p1}, LaJ/z;-><init>(LaJ/s;)V

    invoke-virtual {p0, v0}, LaJ/j;->a(LaJ/z;)V

    .line 142
    :cond_2b
    return-void

    .line 129
    :catchall_2c
    move-exception v0

    :try_start_2d
    monitor-exit p0
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_2c

    throw v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 149
    const-string v0, "r"

    return-object v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 118
    const/4 v0, 0x1

    return v0
.end method

.method public c(I)V
    .registers 2
    .parameter

    .prologue
    .line 105
    iput p1, p0, LaJ/j;->d:I

    .line 106
    return-void
.end method

.method public d()[I
    .registers 2

    .prologue
    .line 161
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    return-object v0

    nop

    :array_8
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 172
    const/4 v0, 0x1

    return v0
.end method
