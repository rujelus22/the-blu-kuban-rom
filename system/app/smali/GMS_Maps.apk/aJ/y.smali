.class public LaJ/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Lau/B;

.field private i:Lau/Y;

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/util/List;

.field private m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private n:J


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaJ/y;->l:Ljava/util/List;

    .line 147
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LaJ/y;->n:J

    .line 150
    return-void
.end method


# virtual methods
.method public a()LaJ/w;
    .registers 19

    .prologue
    .line 223
    new-instance v1, LaJ/w;

    move-object/from16 v0, p0

    iget-object v2, v0, LaJ/y;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LaJ/y;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LaJ/y;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, LaJ/y;->d:I

    move-object/from16 v0, p0

    iget v6, v0, LaJ/y;->e:I

    move-object/from16 v0, p0

    iget v7, v0, LaJ/y;->f:I

    move-object/from16 v0, p0

    iget-object v8, v0, LaJ/y;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, LaJ/y;->h:Lau/B;

    move-object/from16 v0, p0

    iget-object v10, v0, LaJ/y;->i:Lau/Y;

    move-object/from16 v0, p0

    iget v11, v0, LaJ/y;->j:I

    move-object/from16 v0, p0

    iget-object v12, v0, LaJ/y;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, LaJ/y;->l:Ljava/util/List;

    const/4 v14, 0x0

    new-array v14, v14, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-interface {v13, v14}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object/from16 v0, p0

    iget-object v14, v0, LaJ/y;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object/from16 v0, p0

    iget-wide v15, v0, LaJ/y;->n:J

    const/16 v17, 0x0

    invoke-direct/range {v1 .. v17}, LaJ/w;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Lau/B;Lau/Y;ILjava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;JLaJ/x;)V

    return-object v1
.end method

.method public a(I)LaJ/y;
    .registers 2
    .parameter

    .prologue
    .line 168
    iput p1, p0, LaJ/y;->d:I

    .line 169
    return-object p0
.end method

.method public a(J)LaJ/y;
    .registers 3
    .parameter

    .prologue
    .line 218
    iput-wide p1, p0, LaJ/y;->n:J

    .line 219
    return-object p0
.end method

.method public a(Lau/B;)LaJ/y;
    .registers 2
    .parameter

    .prologue
    .line 188
    iput-object p1, p0, LaJ/y;->h:Lau/B;

    .line 189
    return-object p0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaJ/y;
    .registers 3
    .parameter

    .prologue
    .line 208
    iget-object v0, p0, LaJ/y;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    return-object p0
.end method

.method public a(Ljava/lang/String;)LaJ/y;
    .registers 2
    .parameter

    .prologue
    .line 153
    iput-object p1, p0, LaJ/y;->a:Ljava/lang/String;

    .line 154
    return-object p0
.end method

.method public b(I)LaJ/y;
    .registers 2
    .parameter

    .prologue
    .line 173
    iput p1, p0, LaJ/y;->e:I

    .line 174
    return-object p0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaJ/y;
    .registers 2
    .parameter

    .prologue
    .line 213
    iput-object p1, p0, LaJ/y;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 214
    return-object p0
.end method

.method public b(Ljava/lang/String;)LaJ/y;
    .registers 2
    .parameter

    .prologue
    .line 158
    iput-object p1, p0, LaJ/y;->b:Ljava/lang/String;

    .line 159
    return-object p0
.end method

.method public c(I)LaJ/y;
    .registers 2
    .parameter

    .prologue
    .line 178
    iput p1, p0, LaJ/y;->f:I

    .line 179
    return-object p0
.end method

.method public c(Ljava/lang/String;)LaJ/y;
    .registers 2
    .parameter

    .prologue
    .line 163
    iput-object p1, p0, LaJ/y;->c:Ljava/lang/String;

    .line 164
    return-object p0
.end method

.method public d(I)LaJ/y;
    .registers 2
    .parameter

    .prologue
    .line 198
    iput p1, p0, LaJ/y;->j:I

    .line 199
    return-object p0
.end method

.method public d(Ljava/lang/String;)LaJ/y;
    .registers 2
    .parameter

    .prologue
    .line 183
    iput-object p1, p0, LaJ/y;->g:Ljava/lang/String;

    .line 184
    return-object p0
.end method

.method public e(Ljava/lang/String;)LaJ/y;
    .registers 2
    .parameter

    .prologue
    .line 203
    iput-object p1, p0, LaJ/y;->k:Ljava/lang/String;

    .line 204
    return-object p0
.end method
