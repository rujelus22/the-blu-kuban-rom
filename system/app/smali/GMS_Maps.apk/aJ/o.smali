.class public LaJ/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:LaJ/o;


# instance fields
.field private final b:LaJ/z;

.field private c:LaJ/q;

.field private d:LaJ/p;

.field private final e:Ljava/util/List;

.field private f:Ljava/util/Set;

.field private g:Ljava/util/Set;

.field private h:LaJ/v;

.field private i:Lcom/google/googlenav/android/Y;

.field private final j:Ljava/lang/Object;

.field private k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private l:J

.field private m:LaJ/s;

.field private n:LaJ/s;

.field private final o:Ljava/util/Set;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaJ/o;->j:Ljava/lang/Object;

    .line 190
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LaJ/o;->e:Ljava/util/List;

    .line 191
    new-instance v0, LaJ/z;

    invoke-direct {v0}, LaJ/z;-><init>()V

    iput-object v0, p0, LaJ/o;->b:LaJ/z;

    .line 192
    invoke-static {}, Lcom/google/common/collect/dm;->a()Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LaJ/o;->o:Ljava/util/Set;

    .line 193
    invoke-static {}, Lcom/google/common/collect/dm;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LaJ/o;->g:Ljava/util/Set;

    .line 194
    invoke-static {}, Lcom/google/common/collect/dm;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LaJ/o;->f:Ljava/util/Set;

    .line 195
    return-void
.end method

.method public static a()LaJ/o;
    .registers 1

    .prologue
    .line 219
    sget-object v0, LaJ/o;->a:LaJ/o;

    return-object v0
.end method

.method private a(LaJ/m;)V
    .registers 6
    .parameter

    .prologue
    .line 773
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 774
    :try_start_3
    invoke-direct {p0}, LaJ/o;->o()Z

    move-result v0

    if-nez v0, :cond_b

    .line 775
    monitor-exit v1

    .line 792
    :goto_a
    return-void

    .line 777
    :cond_b
    invoke-direct {p0}, LaJ/o;->m()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_42

    .line 778
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->u()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    iput-wide v2, p0, LaJ/o;->l:J

    .line 779
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LaJ/m;->a(I)LaJ/m;

    .line 780
    iget-object v0, p0, LaJ/o;->c:LaJ/q;

    if-eqz v0, :cond_30

    .line 781
    iget-object v0, p0, LaJ/o;->c:LaJ/q;

    invoke-interface {v0}, LaJ/q;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1, v0}, LaJ/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaJ/m;

    .line 787
    :cond_30
    :goto_30
    invoke-virtual {p1}, LaJ/m;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 788
    iget-object v2, p0, LaJ/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 790
    invoke-static {v0}, LaJ/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 791
    monitor-exit v1

    goto :goto_a

    :catchall_3f
    move-exception v0

    monitor-exit v1
    :try_end_41
    .catchall {:try_start_3 .. :try_end_41} :catchall_3f

    throw v0

    .line 784
    :cond_42
    :try_start_42
    invoke-direct {p0}, LaJ/o;->n()I

    move-result v0

    invoke-virtual {p1, v0}, LaJ/m;->a(I)LaJ/m;
    :try_end_49
    .catchall {:try_start_42 .. :try_end_49} :catchall_3f

    goto :goto_30
.end method

.method public static a(LaJ/q;LaJ/p;Lcom/google/googlenav/android/Y;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 201
    sget-object v0, LaJ/o;->a:LaJ/o;

    if-nez v0, :cond_b

    .line 202
    new-instance v0, LaJ/o;

    invoke-direct {v0}, LaJ/o;-><init>()V

    sput-object v0, LaJ/o;->a:LaJ/o;

    .line 204
    :cond_b
    if-eqz p0, :cond_11

    .line 205
    sget-object v0, LaJ/o;->a:LaJ/o;

    iput-object p0, v0, LaJ/o;->c:LaJ/q;

    .line 207
    :cond_11
    if-eqz p1, :cond_17

    .line 208
    sget-object v0, LaJ/o;->a:LaJ/o;

    iput-object p1, v0, LaJ/o;->d:LaJ/p;

    .line 210
    :cond_17
    if-eqz p2, :cond_1d

    .line 211
    sget-object v0, LaJ/o;->a:LaJ/o;

    iput-object p2, v0, LaJ/o;->i:Lcom/google/googlenav/android/Y;

    .line 213
    :cond_1d
    return-void
.end method

.method private a(LaJ/z;)V
    .registers 5
    .parameter

    .prologue
    .line 286
    invoke-virtual {p1}, LaJ/z;->b()LaJ/s;

    move-result-object v0

    invoke-virtual {v0}, LaJ/s;->b()Ljava/lang/String;

    move-result-object v0

    .line 288
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 291
    :try_start_b
    iget-object v2, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v2}, LaJ/z;->b()LaJ/s;

    move-result-object v2

    invoke-virtual {v2}, LaJ/s;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, LaJ/o;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 292
    iget-object v0, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v0, p1}, LaJ/z;->a(LaJ/z;)V

    .line 294
    :cond_20
    monitor-exit v1

    .line 295
    return-void

    .line 294
    :catchall_22
    move-exception v0

    monitor-exit v1
    :try_end_24
    .catchall {:try_start_b .. :try_end_24} :catchall_22

    throw v0
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 10
    .parameter

    .prologue
    const/4 v5, 0x6

    const/4 v8, 0x4

    const/4 v4, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 663
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 664
    const-string v0, "<<<<\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 665
    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 666
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prefix: ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 668
    :cond_35
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_13d

    .line 669
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prefix_delta: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674
    :goto_5b
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_81

    .line 675
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "timestamp:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 677
    :cond_81
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_cd

    .line 678
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 679
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action timestamp:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 680
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    :cond_cd
    invoke-virtual {p0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_144

    .line 683
    invoke-virtual {p0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 684
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "timestamp for impression:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 686
    const/4 v0, 0x0

    :goto_f8
    invoke-virtual {v2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-ge v0, v3, :cond_144

    .line 687
    invoke-virtual {v2, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 688
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "impression("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") count:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " source:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 686
    add-int/lit8 v0, v0, 0x1

    goto :goto_f8

    .line 672
    :cond_13d
    const-string v0, "prefix_delta: 1\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5b

    .line 693
    :cond_144
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_15d

    .line 694
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 695
    const-string v2, "input_index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 697
    :cond_15d
    const-string v0, ">>>>\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 699
    return-void
.end method

.method private a(Ljava/util/List;)V
    .registers 4
    .parameter

    .prologue
    .line 397
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJ/r;

    .line 399
    invoke-interface {v0}, LaJ/r;->h()V

    goto :goto_4

    .line 401
    :cond_14
    return-void
.end method

.method private a(Ljava/util/Set;)V
    .registers 6
    .parameter

    .prologue
    .line 758
    iget-object v0, p0, LaJ/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_5

    .line 764
    :cond_4
    return-void

    .line 761
    :cond_5
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 762
    iget-object v2, p0, LaJ/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    goto :goto_9
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 916
    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v2, v1

    .line 917
    :goto_7
    if-ge v2, v3, :cond_1f

    .line 918
    invoke-virtual {p0, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 919
    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-nez v5, :cond_1b

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 924
    :cond_1b
    :goto_1b
    return v0

    .line 917
    :cond_1c
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_1f
    move v0, v1

    .line 924
    goto :goto_1b
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 298
    if-nez p0, :cond_17

    const-string v0, ""

    move-object v1, v0

    .line 299
    :goto_5
    if-nez p1, :cond_1d

    const-string v0, ""

    .line 300
    :goto_9
    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_15

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    :cond_15
    const/4 v0, 0x1

    :goto_16
    return v0

    .line 298
    :cond_17
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_5

    .line 299
    :cond_1d
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 300
    :cond_22
    const/4 v0, 0x0

    goto :goto_16
.end method

.method private c(LaJ/s;)Ljava/util/List;
    .registers 5
    .parameter

    .prologue
    .line 409
    invoke-virtual {p0, p1}, LaJ/o;->b(LaJ/s;)Ljava/util/Set;

    move-result-object v0

    .line 410
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/common/collect/cx;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 411
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_10
    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 412
    invoke-virtual {p0, v0}, LaJ/o;->b(I)LaJ/r;

    move-result-object v0

    .line 413
    if-eqz v0, :cond_10

    .line 414
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 417
    :cond_2a
    return-object v1
.end method

.method private d(LaJ/s;)V
    .registers 7
    .parameter

    .prologue
    .line 426
    invoke-virtual {p1}, LaJ/s;->b()Ljava/lang/String;

    move-result-object v1

    .line 427
    invoke-direct {p0, p1}, LaJ/o;->c(LaJ/s;)Ljava/util/List;

    move-result-object v2

    .line 429
    iget-object v3, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v3

    .line 430
    :try_start_b
    iget-object v0, p0, LaJ/o;->m:LaJ/s;

    if-eqz v0, :cond_1d

    iget-object v0, p0, LaJ/o;->m:LaJ/s;

    invoke-virtual {v0}, LaJ/s;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lab/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 433
    monitor-exit v3

    .line 474
    :goto_1c
    return-void

    .line 435
    :cond_1d
    invoke-direct {p0}, LaJ/o;->o()Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 437
    new-instance v4, LaJ/m;

    invoke-direct {v4}, LaJ/m;-><init>()V

    .line 440
    iget-object v0, p0, LaJ/o;->m:LaJ/s;

    if-eqz v0, :cond_8a

    iget-object v0, p0, LaJ/o;->m:LaJ/s;

    invoke-virtual {v0}, LaJ/s;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lab/b;->i(Ljava/lang/String;)I

    move-result v0

    .line 442
    :goto_36
    invoke-static {v1}, Lab/b;->i(Ljava/lang/String;)I

    move-result v1

    sub-int v0, v1, v0

    invoke-virtual {v4, v0}, LaJ/m;->b(I)LaJ/m;

    .line 444
    invoke-virtual {p1}, LaJ/s;->f()Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 445
    invoke-virtual {p1}, LaJ/s;->e()I

    move-result v0

    invoke-virtual {v4, v0}, LaJ/m;->c(I)LaJ/m;

    .line 447
    :cond_4c
    invoke-direct {p0, v4}, LaJ/o;->a(LaJ/m;)V

    .line 453
    :cond_4f
    iput-object p1, p0, LaJ/o;->m:LaJ/s;

    .line 454
    monitor-exit v3
    :try_end_52
    .catchall {:try_start_b .. :try_end_52} :catchall_8c

    .line 457
    invoke-direct {p0, v2}, LaJ/o;->a(Ljava/util/List;)V

    .line 459
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 461
    :try_start_58
    iget-object v0, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v0, p1}, LaJ/z;->a(LaJ/s;)V

    .line 462
    monitor-exit v1
    :try_end_5e
    .catchall {:try_start_58 .. :try_end_5e} :catchall_8f

    .line 464
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_62
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_92

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJ/r;

    .line 468
    :try_start_6e
    invoke-interface {v0, p1}, LaJ/r;->e(LaJ/s;)V
    :try_end_71
    .catch Ljava/lang/Exception; {:try_start_6e .. :try_end_71} :catch_72

    goto :goto_62

    .line 469
    :catch_72
    move-exception v2

    .line 470
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SuggestManager, Provider:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, LaU/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_62

    .line 440
    :cond_8a
    const/4 v0, 0x0

    goto :goto_36

    .line 454
    :catchall_8c
    move-exception v0

    :try_start_8d
    monitor-exit v3
    :try_end_8e
    .catchall {:try_start_8d .. :try_end_8e} :catchall_8c

    throw v0

    .line 462
    :catchall_8f
    move-exception v0

    :try_start_90
    monitor-exit v1
    :try_end_91
    .catchall {:try_start_90 .. :try_end_91} :catchall_8f

    throw v0

    .line 473
    :cond_92
    invoke-direct {p0}, LaJ/o;->k()V

    goto :goto_1c
.end method

.method private e(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 955
    invoke-virtual {p0, p1}, LaJ/o;->b(I)LaJ/r;

    move-result-object v0

    .line 956
    if-eqz v0, :cond_b

    .line 957
    invoke-interface {v0}, LaJ/r;->b()Ljava/lang/String;

    move-result-object v0

    .line 960
    :goto_a
    return-object v0

    :cond_b
    const-string v0, "o"

    goto :goto_a
.end method

.method private declared-synchronized k()V
    .registers 4

    .prologue
    .line 553
    monitor-enter p0

    :try_start_1
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_1d

    .line 554
    :try_start_4
    iget-object v0, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v0}, LaJ/z;->c()LaJ/z;

    move-result-object v0

    .line 555
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_1a

    .line 556
    :try_start_b
    iget-object v1, p0, LaJ/o;->h:LaJ/v;

    if-eqz v1, :cond_18

    .line 559
    iget-object v1, p0, LaJ/o;->h:LaJ/v;

    invoke-virtual {p0}, LaJ/o;->h()Z

    move-result v2

    invoke-interface {v1, v0, v2}, LaJ/v;->a(LaJ/z;Z)V
    :try_end_18
    .catchall {:try_start_b .. :try_end_18} :catchall_1d

    .line 561
    :cond_18
    monitor-exit p0

    return-void

    .line 555
    :catchall_1a
    move-exception v0

    :try_start_1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_1b .. :try_end_1c} :catchall_1a

    :try_start_1c
    throw v0
    :try_end_1d
    .catchall {:try_start_1c .. :try_end_1d} :catchall_1d

    .line 553
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private l()V
    .registers 8

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 707
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 708
    :try_start_7
    iget-object v0, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v0}, LaJ/z;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 709
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_1c

    .line 710
    invoke-direct {p0}, LaJ/o;->n()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 712
    invoke-direct {p0}, LaJ/o;->m()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 713
    if-nez v1, :cond_1f

    .line 742
    :goto_1b
    return-void

    .line 709
    :catchall_1c
    move-exception v0

    :try_start_1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_1d .. :try_end_1e} :catchall_1c

    throw v0

    .line 721
    :cond_1f
    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_50

    .line 722
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 724
    new-instance v2, LaJ/m;

    invoke-direct {v2}, LaJ/m;-><init>()V

    .line 725
    invoke-virtual {v2, v0}, LaJ/m;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaJ/m;

    .line 728
    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 729
    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-virtual {v2, v0}, LaJ/m;->b(I)LaJ/m;

    .line 732
    :cond_3f
    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 733
    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-virtual {v2, v0}, LaJ/m;->c(I)LaJ/m;

    .line 736
    :cond_4c
    invoke-direct {p0, v2}, LaJ/o;->a(LaJ/m;)V

    goto :goto_1b

    .line 738
    :cond_50
    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 740
    invoke-static {v1}, LaJ/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1b
.end method

.method private m()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 745
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 746
    :try_start_3
    iget-object v0, p0, LaJ/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_1b

    .line 747
    iget-object v0, p0, LaJ/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    .line 748
    if-lez v0, :cond_1b

    .line 749
    iget-object v2, p0, LaJ/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    monitor-exit v1

    .line 754
    :goto_1a
    return-object v0

    .line 753
    :cond_1b
    monitor-exit v1

    .line 754
    const/4 v0, 0x0

    goto :goto_1a

    .line 753
    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    throw v0
.end method

.method private n()I
    .registers 7

    .prologue
    .line 796
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 797
    :try_start_3
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->u()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    iget-wide v4, p0, LaJ/o;->l:J

    sub-long/2addr v2, v4

    .line 798
    iget-wide v4, p0, LaJ/o;->l:J

    add-long/2addr v4, v2

    iput-wide v4, p0, LaJ/o;->l:J

    .line 799
    long-to-int v0, v2

    monitor-exit v1

    return v0

    .line 800
    :catchall_1a
    move-exception v0

    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    throw v0
.end method

.method private o()Z
    .registers 3

    .prologue
    .line 905
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 906
    :try_start_3
    iget-object v0, p0, LaJ/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_8
    monitor-exit v1

    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_8

    .line 907
    :catchall_c
    move-exception v0

    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    throw v0
.end method


# virtual methods
.method a(Ljava/lang/String;Z)I
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 574
    invoke-static {p1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 598
    :goto_8
    return v0

    .line 577
    :cond_9
    iget-object v3, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v3

    move v1, v2

    .line 578
    :goto_d
    :try_start_d
    iget-object v4, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v4}, LaJ/z;->d()I

    move-result v4

    if-ge v1, v4, :cond_4a

    .line 579
    iget-object v4, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v4, v1}, LaJ/z;->a(I)LaJ/w;

    move-result-object v4

    .line 580
    invoke-virtual {v4}, LaJ/w;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_47

    .line 583
    if-eqz p2, :cond_44

    .line 585
    invoke-virtual {v4}, LaJ/w;->a()I

    move-result v4

    move v0, v2

    .line 586
    :goto_2c
    if-ge v2, v1, :cond_3f

    .line 587
    iget-object v5, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v5, v2}, LaJ/z;->a(I)LaJ/w;

    move-result-object v5

    invoke-virtual {v5}, LaJ/w;->a()I

    move-result v5

    if-ne v5, v4, :cond_3c

    .line 588
    add-int/lit8 v0, v0, 0x1

    .line 586
    :cond_3c
    add-int/lit8 v2, v2, 0x1

    goto :goto_2c

    .line 591
    :cond_3f
    monitor-exit v3

    goto :goto_8

    .line 596
    :catchall_41
    move-exception v0

    monitor-exit v3
    :try_end_43
    .catchall {:try_start_d .. :try_end_43} :catchall_41

    throw v0

    .line 593
    :cond_44
    :try_start_44
    monitor-exit v3

    move v0, v1

    goto :goto_8

    .line 578
    :cond_47
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 596
    :cond_4a
    monitor-exit v3
    :try_end_4b
    .catchall {:try_start_44 .. :try_end_4b} :catchall_41

    goto :goto_8
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 307
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 308
    :try_start_3
    iget-object v0, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v0, p1}, LaJ/z;->b(I)V

    .line 309
    monitor-exit v1

    .line 310
    return-void

    .line 309
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public a(III)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    .line 814
    :try_start_0
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_3} :catch_ac

    .line 815
    :try_start_3
    iget-object v0, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v0, p2}, LaJ/z;->a(I)LaJ/w;

    move-result-object v0

    .line 816
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_a9

    .line 824
    :goto_a
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gV;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 825
    const/4 v1, 0x1

    invoke-direct {p0}, LaJ/o;->n()I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 826
    const/4 v1, 0x2

    invoke-virtual {v2, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 827
    const/4 v1, 0x3

    invoke-virtual {v2, v1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 828
    const/4 v1, 0x5

    invoke-virtual {v2, v1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 831
    if-eqz v0, :cond_b0

    .line 832
    const/4 v1, 0x0

    :goto_28
    invoke-virtual {v0}, LaJ/w;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    array-length v3, v3

    if-ge v1, v3, :cond_b0

    .line 833
    invoke-virtual {v0}, LaJ/w;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    aget-object v3, v3, v1

    .line 834
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v5, Lcom/google/wireless/googlenav/proto/j2me/gV;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 835
    const/4 v5, 0x1

    invoke-virtual {v0}, LaJ/w;->b()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v7

    const/4 v8, 0x2

    invoke-virtual {v3, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 837
    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_62

    .line 838
    const/4 v5, 0x2

    const/4 v6, 0x5

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 840
    :cond_62
    const/4 v5, 0x6

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_72

    .line 841
    const/4 v5, 0x3

    const/4 v6, 0x6

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 843
    :cond_72
    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_82

    .line 844
    const/4 v5, 0x4

    const/4 v6, 0x4

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 847
    :cond_82
    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_92

    .line 848
    const/4 v5, 0x5

    const/4 v6, 0x3

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 850
    :cond_92
    const/4 v5, 0x7

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_a2

    .line 851
    const/4 v5, 0x6

    const/4 v6, 0x7

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v3

    invoke-virtual {v4, v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 854
    :cond_a2
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 832
    add-int/lit8 v1, v1, 0x1

    goto :goto_28

    .line 816
    :catchall_a9
    move-exception v0

    :try_start_aa
    monitor-exit v1
    :try_end_ab
    .catchall {:try_start_aa .. :try_end_ab} :catchall_a9

    :try_start_ab
    throw v0
    :try_end_ac
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_ab .. :try_end_ac} :catch_ac

    .line 817
    :catch_ac
    move-exception v0

    .line 820
    const/4 v0, 0x0

    goto/16 :goto_a

    .line 857
    :cond_b0
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 858
    :try_start_b3
    invoke-direct {p0}, LaJ/o;->m()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 860
    if-eqz v0, :cond_c0

    .line 861
    const/4 v3, 0x7

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 863
    invoke-static {v0}, LaJ/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 865
    :cond_c0
    monitor-exit v1

    .line 866
    return-void

    .line 865
    :catchall_c2
    move-exception v0

    monitor-exit v1
    :try_end_c4
    .catchall {:try_start_b3 .. :try_end_c4} :catchall_c2

    throw v0
.end method

.method public a(LaJ/r;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 238
    invoke-interface {p1, p2}, LaJ/r;->b(I)V

    .line 239
    iget-object v0, p0, LaJ/o;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    invoke-interface {p1}, LaJ/r;->e()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 241
    iget-object v0, p0, LaJ/o;->f:Ljava/util/Set;

    invoke-interface {p1}, LaJ/r;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 243
    :cond_1b
    invoke-interface {p1}, LaJ/r;->f()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 244
    iget-object v0, p0, LaJ/o;->g:Ljava/util/Set;

    invoke-interface {p1}, LaJ/r;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 246
    :cond_2e
    return-void
.end method

.method public a(LaJ/s;)V
    .registers 5
    .parameter

    .prologue
    .line 484
    invoke-virtual {p0, p1}, LaJ/o;->b(LaJ/s;)Ljava/util/Set;

    move-result-object v0

    .line 485
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 486
    :try_start_7
    iget-object v2, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v2, v0}, LaJ/z;->a(Ljava/util/Set;)V

    .line 487
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_21

    .line 491
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaJ/o;->b(I)LaJ/r;

    move-result-object v0

    check-cast v0, LaJ/j;

    .line 492
    if-eqz v0, :cond_1d

    .line 493
    invoke-virtual {p1}, LaJ/s;->c()I

    move-result v1

    invoke-virtual {v0, v1}, LaJ/j;->c(I)V

    .line 496
    :cond_1d
    invoke-direct {p0, p1}, LaJ/o;->d(LaJ/s;)V

    .line 497
    return-void

    .line 487
    :catchall_21
    move-exception v0

    :try_start_22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_22 .. :try_end_23} :catchall_21

    throw v0
.end method

.method public a(LaJ/v;)V
    .registers 2
    .parameter

    .prologue
    .line 964
    iput-object p1, p0, LaJ/o;->h:LaJ/v;

    .line 965
    return-void
.end method

.method public a(LaJ/z;ZI)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 336
    invoke-static {p1}, Lcom/google/common/base/P;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    invoke-direct {p0, p1}, LaJ/o;->a(LaJ/z;)V

    .line 338
    invoke-direct {p0}, LaJ/o;->o()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 339
    invoke-direct {p0}, LaJ/o;->l()V

    .line 341
    :cond_f
    if-eqz p2, :cond_14

    .line 342
    invoke-direct {p0}, LaJ/o;->k()V

    .line 345
    :cond_14
    invoke-virtual {p0}, LaJ/o;->h()Z

    move-result v0

    if-eqz v0, :cond_40

    .line 347
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 348
    :try_start_1d
    iget-object v0, p0, LaJ/o;->n:LaJ/s;

    if-eqz v0, :cond_39

    invoke-virtual {p1}, LaJ/z;->b()LaJ/s;

    move-result-object v0

    if-eqz v0, :cond_3f

    invoke-virtual {p1}, LaJ/z;->b()LaJ/s;

    move-result-object v0

    invoke-virtual {v0}, LaJ/s;->g()J

    move-result-wide v2

    iget-object v0, p0, LaJ/o;->n:LaJ/s;

    invoke-virtual {v0}, LaJ/s;->g()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_3f

    .line 351
    :cond_39
    invoke-virtual {p1}, LaJ/z;->b()LaJ/s;

    move-result-object v0

    iput-object v0, p0, LaJ/o;->n:LaJ/s;

    .line 353
    :cond_3f
    monitor-exit v1
    :try_end_40
    .catchall {:try_start_1d .. :try_end_40} :catchall_5c

    .line 357
    :cond_40
    invoke-virtual {p0, p3}, LaJ/o;->b(I)LaJ/r;

    move-result-object v0

    .line 358
    if-eqz v0, :cond_5f

    .line 359
    invoke-interface {v0}, LaJ/r;->d()[I

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4c
    if-ge v0, v2, :cond_5f

    aget v3, v1, v0

    .line 360
    iget-object v4, p0, LaJ/o;->o:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 359
    add-int/lit8 v0, v0, 0x1

    goto :goto_4c

    .line 353
    :catchall_5c
    move-exception v0

    :try_start_5d
    monitor-exit v1
    :try_end_5e
    .catchall {:try_start_5d .. :try_end_5e} :catchall_5c

    throw v0

    .line 363
    :cond_5f
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 620
    sget-object v0, Lcom/google/googlenav/ag;->a:Lcom/google/googlenav/ag;

    invoke-virtual {p0, p1, p2, v0}, LaJ/o;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    .line 621
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 637
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 638
    const/4 v0, 0x0

    :try_start_4
    invoke-virtual {p0, p1, v0}, LaJ/o;->a(Ljava/lang/String;Z)I

    move-result v0

    .line 639
    const/4 v2, -0x1

    if-ne v0, v2, :cond_d

    .line 640
    monitor-exit v1

    .line 660
    :goto_c
    return-void

    .line 642
    :cond_d
    invoke-virtual {p0, v0}, LaJ/o;->c(I)LaJ/w;

    move-result-object v2

    .line 643
    invoke-virtual {p0}, LaJ/o;->c()Ljava/lang/String;

    move-result-object v3

    .line 644
    const/4 v4, 0x1

    invoke-virtual {p0, p1, v4}, LaJ/o;->a(Ljava/lang/String;Z)I

    move-result v4

    .line 645
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_4 .. :try_end_1b} :catchall_e9

    .line 647
    const/16 v1, 0x49

    const-string v5, "s"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "i="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3}, Lab/b;->i(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "s"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lab/b;->i(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "o"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, LaJ/w;->a()I

    move-result v6

    invoke-direct {p0, v6}, LaJ/o;->e(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "y"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, LaJ/w;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "p"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "t"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "u"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "c"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/googlenav/ag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v5, v0}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 645
    :catchall_e9
    move-exception v0

    :try_start_ea
    monitor-exit v1
    :try_end_eb
    .catchall {:try_start_ea .. :try_end_eb} :catchall_e9

    throw v0
.end method

.method public b(I)LaJ/r;
    .registers 5
    .parameter

    .prologue
    .line 503
    iget-object v0, p0, LaJ/o;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJ/r;

    .line 504
    invoke-interface {v0}, LaJ/r;->c()I

    move-result v2

    if-ne v2, p1, :cond_6

    .line 508
    :goto_18
    return-object v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public b(LaJ/s;)Ljava/util/Set;
    .registers 3
    .parameter

    .prologue
    .line 515
    invoke-virtual {p1}, LaJ/s;->d()Ljava/util/Set;

    move-result-object v0

    .line 521
    return-object v0
.end method

.method public b()V
    .registers 4

    .prologue
    .line 318
    iget-object v0, p0, LaJ/o;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJ/r;

    .line 319
    invoke-interface {v0}, LaJ/r;->i()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 322
    invoke-interface {v0}, LaJ/r;->c()I

    move-result v0

    invoke-virtual {p0, v0}, LaJ/o;->a(I)V

    goto :goto_6

    .line 325
    :cond_20
    return-void
.end method

.method public c(I)LaJ/w;
    .registers 4
    .parameter

    .prologue
    .line 608
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 609
    :try_start_3
    iget-object v0, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v0, p1}, LaJ/z;->a(I)LaJ/w;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 610
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    throw v0
.end method

.method public c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 370
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 371
    :try_start_3
    iget-object v0, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v0}, LaJ/z;->b()LaJ/s;

    move-result-object v0

    invoke-virtual {v0}, LaJ/s;->b()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 372
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public d()LaJ/s;
    .registers 3

    .prologue
    .line 376
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 377
    :try_start_3
    iget-object v0, p0, LaJ/o;->n:LaJ/s;

    monitor-exit v1

    return-object v0

    .line 378
    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    throw v0
.end method

.method public d(I)V
    .registers 5
    .parameter

    .prologue
    .line 885
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 886
    :try_start_3
    invoke-direct {p0}, LaJ/o;->o()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 887
    invoke-virtual {p0}, LaJ/o;->j()V

    .line 889
    :cond_c
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/gV;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, LaJ/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 890
    if-eqz p1, :cond_1d

    .line 891
    iget-object v0, p0, LaJ/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 893
    :cond_1d
    iget-object v0, p0, LaJ/o;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 894
    iget-object v0, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v0}, LaJ/z;->a()V

    .line 895
    const/4 v0, 0x0

    iput-object v0, p0, LaJ/o;->m:LaJ/s;

    .line 896
    const/4 v0, 0x0

    iput-object v0, p0, LaJ/o;->n:LaJ/s;

    .line 897
    monitor-exit v1

    .line 898
    return-void

    .line 897
    :catchall_2f
    move-exception v0

    monitor-exit v1
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    throw v0
.end method

.method public e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 385
    iget-object v0, p0, LaJ/o;->c:LaJ/q;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, LaJ/o;->c:LaJ/q;

    invoke-interface {v0}, LaJ/q;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_5
.end method

.method public f()Lau/B;
    .registers 2

    .prologue
    .line 389
    iget-object v0, p0, LaJ/o;->d:LaJ/p;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, LaJ/o;->d:LaJ/p;

    invoke-interface {v0}, LaJ/p;->a()Lau/B;

    move-result-object v0

    goto :goto_5
.end method

.method public g()Lcom/google/common/collect/ImmutableSet;
    .registers 4

    .prologue
    .line 528
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->g()Lcom/google/common/collect/bD;

    move-result-object v1

    .line 529
    iget-object v0, p0, LaJ/o;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJ/r;

    .line 530
    invoke-interface {v0}, LaJ/r;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/common/collect/bD;->b(Ljava/lang/Object;)Lcom/google/common/collect/bD;

    goto :goto_a

    .line 532
    :cond_22
    invoke-virtual {v1}, Lcom/google/common/collect/bD;->a()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .registers 3

    .prologue
    .line 539
    iget-object v0, p0, LaJ/o;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJ/r;

    .line 540
    invoke-interface {v0}, LaJ/r;->K_()Z

    move-result v0

    if-nez v0, :cond_6

    .line 541
    const/4 v0, 0x0

    .line 544
    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x1

    goto :goto_19
.end method

.method public i()V
    .registers 4

    .prologue
    .line 873
    invoke-direct {p0}, LaJ/o;->m()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 874
    if-eqz v0, :cond_e

    .line 875
    const/4 v1, 0x3

    invoke-virtual {p0}, LaJ/o;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 877
    :cond_e
    return-void
.end method

.method public j()V
    .registers 3

    .prologue
    .line 932
    iget-object v1, p0, LaJ/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 933
    :try_start_3
    invoke-direct {p0}, LaJ/o;->o()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 942
    :cond_9
    :goto_9
    const/4 v0, 0x0

    iput-object v0, p0, LaJ/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 943
    const/4 v0, 0x0

    iput-object v0, p0, LaJ/o;->m:LaJ/s;

    .line 944
    const/4 v0, 0x0

    iput-object v0, p0, LaJ/o;->n:LaJ/s;

    .line 945
    iget-object v0, p0, LaJ/o;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 946
    iget-object v0, p0, LaJ/o;->b:LaJ/z;

    invoke-virtual {v0}, LaJ/z;->a()V

    .line 948
    monitor-exit v1

    .line 949
    return-void

    .line 935
    :cond_1e
    iget-object v0, p0, LaJ/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, LaJ/o;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 938
    iget-object v0, p0, LaJ/o;->o:Ljava/util/Set;

    invoke-direct {p0, v0}, LaJ/o;->a(Ljava/util/Set;)V

    .line 939
    iget-object v0, p0, LaJ/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, LaU/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_9

    .line 948
    :catchall_31
    move-exception v0

    monitor-exit v1
    :try_end_33
    .catchall {:try_start_3 .. :try_end_33} :catchall_31

    throw v0
.end method
