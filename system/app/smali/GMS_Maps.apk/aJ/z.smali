.class public LaJ/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field static a:I


# instance fields
.field private b:LaJ/s;

.field private c:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 24
    const/16 v0, 0x14

    sput v0, LaJ/z;->a:I

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 37
    const-string v0, ""

    invoke-static {v0}, LaJ/s;->a(Ljava/lang/String;)LaJ/s;

    move-result-object v0

    invoke-direct {p0, v0}, LaJ/z;-><init>(LaJ/s;)V

    .line 38
    return-void
.end method

.method public constructor <init>(LaJ/s;)V
    .registers 3
    .parameter

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LaJ/z;-><init>(LaJ/s;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(LaJ/s;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    sget v0, LaJ/z;->a:I

    if-le p2, v0, :cond_9

    .line 57
    sget p2, LaJ/z;->a:I

    .line 59
    :cond_9
    iput-object p1, p0, LaJ/z;->b:LaJ/s;

    .line 60
    if-nez p2, :cond_15

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_12
    iput-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    .line 62
    return-void

    .line 60
    :cond_15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_12
.end method

.method private a(Lcom/google/common/base/Q;)Ljava/util/ArrayList;
    .registers 4
    .parameter

    .prologue
    .line 208
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-static {v1, p1}, Lcom/google/common/collect/R;->a(Ljava/util/Collection;Lcom/google/common/base/Q;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method private static final a(LaJ/w;LaJ/w;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 83
    invoke-virtual {p0}, LaJ/w;->j()J

    move-result-wide v2

    .line 84
    invoke-virtual {p1}, LaJ/w;->j()J

    move-result-wide v4

    .line 85
    cmp-long v6, v2, v4

    if-lez v6, :cond_f

    .line 91
    :cond_e
    :goto_e
    return v0

    .line 88
    :cond_f
    cmp-long v2, v2, v4

    if-gez v2, :cond_15

    move v0, v1

    .line 89
    goto :goto_e

    .line 91
    :cond_15
    invoke-virtual {p0}, LaJ/w;->f()I

    move-result v2

    invoke-virtual {p1}, LaJ/w;->f()I

    move-result v3

    if-lt v2, v3, :cond_e

    move v0, v1

    goto :goto_e
.end method


# virtual methods
.method public a(I)LaJ/w;
    .registers 3
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJ/w;

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 212
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 213
    return-void
.end method

.method public a(LaJ/s;)V
    .registers 2
    .parameter

    .prologue
    .line 220
    iput-object p1, p0, LaJ/z;->b:LaJ/s;

    .line 221
    return-void
.end method

.method public a(LaJ/w;)V
    .registers 4
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget v1, LaJ/z;->a:I

    if-lt v0, v1, :cond_b

    .line 80
    :goto_a
    return-void

    .line 79
    :cond_b
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a
.end method

.method public a(LaJ/z;)V
    .registers 4
    .parameter

    .prologue
    .line 136
    iget-object v0, p1, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJ/w;

    .line 137
    invoke-virtual {p0, v0}, LaJ/z;->b(LaJ/w;)V

    goto :goto_6

    .line 139
    :cond_16
    return-void
.end method

.method public a(Ljava/util/Set;)V
    .registers 3
    .parameter

    .prologue
    .line 163
    new-instance v0, LaJ/B;

    invoke-direct {v0, p0, p1}, LaJ/B;-><init>(LaJ/z;Ljava/util/Set;)V

    invoke-direct {p0, v0}, LaJ/z;->a(Lcom/google/common/base/Q;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    .line 169
    return-void
.end method

.method public b()LaJ/s;
    .registers 2

    .prologue
    .line 216
    iget-object v0, p0, LaJ/z;->b:LaJ/s;

    return-object v0
.end method

.method public b(I)V
    .registers 3
    .parameter

    .prologue
    .line 148
    new-instance v0, LaJ/A;

    invoke-direct {v0, p0, p1}, LaJ/A;-><init>(LaJ/z;I)V

    invoke-direct {p0, v0}, LaJ/z;->a(Lcom/google/common/base/Q;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    .line 154
    return-void
.end method

.method public b(LaJ/w;)V
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 102
    move v1, v0

    move v2, v0

    .line 104
    :goto_3
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3e

    .line 105
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJ/w;

    .line 106
    if-nez v1, :cond_28

    invoke-static {p1, v0}, LaJ/z;->a(LaJ/w;LaJ/w;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 107
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 108
    const/4 v1, 0x1

    .line 109
    add-int/lit8 v2, v2, 0x1

    move v0, v1

    move v1, v2

    :goto_25
    move v2, v1

    move v1, v0

    .line 121
    goto :goto_3

    .line 110
    :cond_28
    invoke-virtual {p1, v0}, LaJ/w;->a(LaJ/w;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 114
    if-nez v1, :cond_31

    .line 128
    :cond_30
    return-void

    .line 117
    :cond_31
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v1

    move v1, v2

    goto :goto_25

    .line 119
    :cond_39
    add-int/lit8 v2, v2, 0x1

    move v0, v1

    move v1, v2

    goto :goto_25

    .line 122
    :cond_3e
    if-nez v1, :cond_43

    .line 123
    invoke-virtual {p0, p1}, LaJ/z;->a(LaJ/w;)V

    .line 125
    :cond_43
    :goto_43
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget v1, LaJ/z;->a:I

    if-le v0, v1, :cond_30

    .line 126
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    iget-object v1, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_43
.end method

.method public c()LaJ/z;
    .registers 4

    .prologue
    .line 226
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJ/z;

    .line 228
    iget-object v1, p0, LaJ/z;->b:LaJ/s;

    iput-object v1, v0, LaJ/z;->b:LaJ/s;

    .line 230
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, LaJ/z;->c:Ljava/util/ArrayList;

    .line 231
    iget-object v1, v0, LaJ/z;->c:Ljava/util/ArrayList;

    iget-object v2, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_18
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_18} :catch_19

    .line 233
    return-object v0

    .line 234
    :catch_19
    move-exception v0

    .line 235
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Superclass does not support clone"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 19
    invoke-virtual {p0}, LaJ/z;->c()LaJ/z;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 243
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 252
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gV;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 253
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJ/w;

    .line 254
    const/4 v3, 0x2

    invoke-virtual {v0}, LaJ/w;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_d

    .line 257
    :cond_22
    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 277
    instance-of v0, p1, LaJ/z;

    if-nez v0, :cond_6

    .line 278
    const/4 v0, 0x0

    .line 283
    :goto_5
    return v0

    .line 281
    :cond_6
    check-cast p1, LaJ/z;

    .line 283
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    iget-object v1, p1, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 292
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 262
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 263
    const-string v0, "["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    iget-object v0, p0, LaJ/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJ/w;

    .line 265
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, LaJ/w;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "; "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_10

    .line 267
    :cond_37
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
