.class public LaJ/w;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Ljava/lang/String;

.field private final j:Lau/B;

.field private final k:Lau/Y;

.field private final l:I

.field private final m:Ljava/lang/String;

.field private final n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final o:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 64
    const-string v0, "indexInList="

    sput-object v0, LaJ/w;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Lau/B;Lau/Y;ILjava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    if-nez p1, :cond_2e

    const-string v0, ""

    :goto_7
    iput-object v0, p0, LaJ/w;->b:Ljava/lang/String;

    .line 255
    invoke-static {p2}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    :goto_f
    iput-object p1, p0, LaJ/w;->c:Ljava/lang/String;

    .line 256
    if-nez p3, :cond_15

    const-string p3, ""

    :cond_15
    iput-object p3, p0, LaJ/w;->d:Ljava/lang/String;

    .line 257
    iput p4, p0, LaJ/w;->f:I

    .line 258
    iput p5, p0, LaJ/w;->g:I

    .line 259
    iput p6, p0, LaJ/w;->h:I

    .line 260
    iput-object p7, p0, LaJ/w;->i:Ljava/lang/String;

    .line 261
    iput-object p8, p0, LaJ/w;->j:Lau/B;

    .line 262
    iput-object p9, p0, LaJ/w;->k:Lau/Y;

    .line 263
    iput p10, p0, LaJ/w;->l:I

    .line 264
    iput-object p11, p0, LaJ/w;->m:Ljava/lang/String;

    .line 265
    iput-object p12, p0, LaJ/w;->o:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 266
    iput-object p13, p0, LaJ/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 267
    iput-wide p14, p0, LaJ/w;->e:J

    .line 268
    return-void

    :cond_2e
    move-object v0, p1

    .line 254
    goto :goto_7

    :cond_30
    move-object p1, p2

    .line 255
    goto :goto_f
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Lau/B;Lau/Y;ILjava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;JLaJ/x;)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct/range {p0 .. p15}, LaJ/w;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Lau/B;Lau/Y;ILjava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 397
    invoke-static {p0}, Lcom/google/common/base/ah;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 398
    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 399
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 271
    iget v0, p0, LaJ/w;->g:I

    return v0
.end method

.method public a(I)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 439
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 440
    invoke-virtual {p0}, LaJ/w;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_12

    .line 441
    invoke-virtual {p0}, LaJ/w;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    :cond_12
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 444
    invoke-virtual {p0}, LaJ/w;->h()Lau/B;

    move-result-object v1

    if-eqz v1, :cond_28

    .line 445
    invoke-virtual {p0}, LaJ/w;->h()Lau/B;

    move-result-object v1

    invoke-virtual {v1}, Lau/B;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    :cond_28
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448
    const/4 v1, -0x1

    if-eq p1, v1, :cond_38

    .line 449
    sget-object v1, LaJ/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 452
    :cond_38
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LaJ/w;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 407
    invoke-virtual {p0}, LaJ/w;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LaJ/w;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LaJ/w;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 411
    :cond_f
    :goto_f
    return v0

    :cond_10
    invoke-virtual {p0}, LaJ/w;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LaJ/w;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LaJ/w;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2c

    invoke-virtual {p0}, LaJ/w;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LaJ/w;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LaJ/w;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_f

    :cond_2c
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 275
    iget-object v0, p0, LaJ/w;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 279
    iget-object v0, p0, LaJ/w;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 283
    iget-object v0, p0, LaJ/w;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 287
    iget v0, p0, LaJ/w;->f:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 383
    instance-of v1, p1, LaJ/w;

    if-nez v1, :cond_6

    .line 387
    :cond_5
    :goto_5
    return v0

    .line 386
    :cond_6
    check-cast p1, LaJ/w;

    .line 387
    iget v1, p0, LaJ/w;->f:I

    invoke-virtual {p1}, LaJ/w;->e()I

    move-result v2

    if-ne v1, v2, :cond_5

    iget v1, p0, LaJ/w;->g:I

    invoke-virtual {p1}, LaJ/w;->a()I

    move-result v2

    if-ne v1, v2, :cond_5

    iget v1, p0, LaJ/w;->h:I

    invoke-virtual {p1}, LaJ/w;->f()I

    move-result v2

    if-ne v1, v2, :cond_5

    iget-object v1, p0, LaJ/w;->b:Ljava/lang/String;

    invoke-virtual {p1}, LaJ/w;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lab/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LaJ/w;->c:Ljava/lang/String;

    invoke-virtual {p1}, LaJ/w;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lab/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LaJ/w;->d:Ljava/lang/String;

    invoke-virtual {p1}, LaJ/w;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lab/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LaJ/w;->i:Ljava/lang/String;

    invoke-virtual {p1}, LaJ/w;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lab/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public f()I
    .registers 2

    .prologue
    .line 291
    iget v0, p0, LaJ/w;->h:I

    return v0
.end method

.method public g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 295
    iget-object v0, p0, LaJ/w;->i:Ljava/lang/String;

    return-object v0
.end method

.method public h()Lau/B;
    .registers 2

    .prologue
    .line 299
    iget-object v0, p0, LaJ/w;->j:Lau/B;

    return-object v0
.end method

.method public i()Lau/Y;
    .registers 2

    .prologue
    .line 303
    iget-object v0, p0, LaJ/w;->k:Lau/Y;

    return-object v0
.end method

.method public j()J
    .registers 3

    .prologue
    .line 307
    iget-wide v0, p0, LaJ/w;->e:J

    return-wide v0
.end method

.method public k()Ljava/lang/String;
    .registers 2

    .prologue
    .line 314
    iget v0, p0, LaJ/w;->f:I

    packed-switch v0, :pswitch_data_24

    .line 335
    :pswitch_5
    const-string v0, "d"

    :goto_7
    return-object v0

    .line 316
    :pswitch_8
    const-string v0, "c"

    goto :goto_7

    .line 318
    :pswitch_b
    const-string v0, "h"

    goto :goto_7

    .line 320
    :pswitch_e
    const-string v0, "r"

    goto :goto_7

    .line 322
    :pswitch_11
    const-string v0, "v"

    goto :goto_7

    .line 324
    :pswitch_14
    const-string v0, "f"

    goto :goto_7

    .line 326
    :pswitch_17
    const-string v0, "p"

    goto :goto_7

    .line 328
    :pswitch_1a
    const-string v0, "l"

    goto :goto_7

    .line 330
    :pswitch_1d
    const-string v0, "d"

    goto :goto_7

    .line 332
    :pswitch_20
    const-string v0, "x"

    goto :goto_7

    .line 314
    nop

    :pswitch_data_24
    .packed-switch -0x1
        :pswitch_20
        :pswitch_1d
        :pswitch_14
        :pswitch_8
        :pswitch_b
        :pswitch_e
        :pswitch_11
        :pswitch_17
        :pswitch_5
        :pswitch_1a
    .end packed-switch
.end method

.method public l()Ljava/lang/String;
    .registers 4

    .prologue
    const v0, 0x7f0203d9

    const v1, 0x7f0203d7

    .line 342
    .line 343
    iget v2, p0, LaJ/w;->f:I

    packed-switch v2, :pswitch_data_2a

    .line 365
    :pswitch_b
    const v0, 0x7f0203d5

    .line 368
    :goto_e
    :pswitch_e
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_22
    move v0, v1

    .line 350
    goto :goto_e

    :pswitch_24
    move v0, v1

    .line 353
    goto :goto_e

    .line 355
    :pswitch_26
    const v0, 0x7f0203d6

    .line 356
    goto :goto_e

    .line 343
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_24
        :pswitch_b
        :pswitch_22
        :pswitch_26
        :pswitch_e
        :pswitch_22
        :pswitch_e
    .end packed-switch
.end method

.method public m()Ljava/lang/String;
    .registers 2

    .prologue
    .line 372
    iget-object v0, p0, LaJ/w;->m:Ljava/lang/String;

    return-object v0
.end method

.method public n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 8

    .prologue
    const/16 v4, 0xe

    const/16 v3, 0xd

    const/16 v6, 0xc

    .line 460
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gV;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 461
    const/4 v1, 0x1

    iget v2, p0, LaJ/w;->l:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 462
    iget-object v1, p0, LaJ/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_54

    .line 463
    iget-object v1, p0, LaJ/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_29

    .line 464
    const/4 v1, 0x4

    iget-object v2, p0, LaJ/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 467
    :cond_29
    iget-object v1, p0, LaJ/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_42

    .line 468
    const/4 v1, 0x5

    iget-object v2, p0, LaJ/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDouble(I)D

    move-result-wide v2

    const-wide v4, 0x40c3880000000000L

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 471
    :cond_42
    iget-object v1, p0, LaJ/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_54

    .line 472
    const/4 v1, 0x3

    iget-object v2, p0, LaJ/w;->n:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 476
    :cond_54
    return-object v0
.end method

.method public o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 480
    iget-object v0, p0, LaJ/w;->o:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "query:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaJ/w;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " description:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaJ/w;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LaJ/w;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " providerId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LaJ/w;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ranking:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LaJ/w;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
