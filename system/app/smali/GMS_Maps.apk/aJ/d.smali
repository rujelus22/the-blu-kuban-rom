.class public abstract LaJ/d;
.super LaJ/f;
.source "SourceFile"


# instance fields
.field private volatile a:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 21
    invoke-direct {p0}, LaJ/f;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, LaJ/d;->a:I

    return-void
.end method

.method static synthetic a(LaJ/d;)I
    .registers 3
    .parameter

    .prologue
    .line 21
    iget v0, p0, LaJ/d;->a:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, LaJ/d;->a:I

    return v0
.end method


# virtual methods
.method public declared-synchronized K_()Z
    .registers 2

    .prologue
    .line 79
    monitor-enter p0

    :try_start_1
    invoke-super {p0}, LaJ/f;->K_()Z

    move-result v0

    if-eqz v0, :cond_e

    iget v0, p0, LaJ/d;->a:I
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_10

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_c
    monitor-exit p0

    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_c

    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract a(LaJ/s;)LaJ/z;
.end method

.method protected a_(LaJ/s;)V
    .registers 8
    .parameter

    .prologue
    .line 42
    invoke-virtual {p0, p1}, LaJ/d;->b(LaJ/s;)Z

    move-result v4

    .line 43
    invoke-virtual {p0, p1}, LaJ/d;->c(LaJ/s;)Z

    move-result v5

    .line 44
    if-nez v4, :cond_d

    if-nez v5, :cond_d

    .line 75
    :goto_c
    return-void

    .line 48
    :cond_d
    iget v0, p0, LaJ/d;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LaJ/d;->a:I

    .line 49
    new-instance v0, LaJ/e;

    invoke-static {}, Lcom/google/googlenav/bL;->a()LZ/c;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LaJ/e;-><init>(LaJ/d;LZ/c;LaJ/s;ZZ)V

    invoke-virtual {v0}, LaJ/e;->g()V

    goto :goto_c
.end method
