.class public Lbg/b;
.super Lbf/am;
.source "SourceFile"

# interfaces
.implements Lbf/j;
.implements Lbf/k;
.implements Lcom/google/android/maps/driveabout/vector/y;
.implements Ln/s;


# static fields
.field public static final k:LaN/Y;


# instance fields
.field private final l:Lcom/google/common/collect/Q;

.field private final m:Ljava/util/Map;

.field private n:Lcom/google/android/maps/driveabout/vector/c;

.field private final o:Ljava/lang/Object;

.field private p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

.field private q:Lcom/google/android/maps/driveabout/vector/f;

.field private r:Lcom/google/android/maps/driveabout/vector/aA;

.field private final s:Lbg/k;

.field private t:Lcom/google/android/maps/driveabout/vector/az;

.field private u:Z

.field private final v:Ljava/lang/Object;

.field private final w:Ljava/lang/Object;

.field private x:J

.field private y:J

.field private volatile z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 125
    const/16 v0, 0x12

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    sput-object v0, Lbg/b;->k:LaN/Y;

    return-void
.end method

.method private constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;LaN/k;Lcom/google/googlenav/layer/r;Lcom/google/googlenav/offers/j;)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 213
    invoke-direct/range {p0 .. p13}, Lbf/am;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;LaN/k;Lcom/google/googlenav/layer/r;Lcom/google/googlenav/offers/j;)V

    .line 131
    invoke-static {}, Lcom/google/common/collect/am;->e()Lcom/google/common/collect/am;

    move-result-object v0

    iput-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    .line 137
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lbg/b;->m:Ljava/util/Map;

    .line 147
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbg/b;->o:Ljava/lang/Object;

    .line 173
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbg/b;->v:Ljava/lang/Object;

    .line 176
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbg/b;->w:Ljava/lang/Object;

    .line 178
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbg/b;->x:J

    .line 179
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbg/b;->y:J

    .line 185
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbg/b;->z:Z

    .line 216
    new-instance v0, Lbg/k;

    invoke-direct {v0}, Lbg/k;-><init>()V

    iput-object v0, p0, Lbg/b;->s:Lbg/k;

    .line 217
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 218
    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v0

    invoke-virtual {v0, p0}, Ln/q;->a(Ln/s;)V

    .line 221
    :cond_43
    new-instance v0, Lbg/c;

    const-string v1, "LayerTileCompactor"

    invoke-direct {v0, p0, v1}, Lbg/c;-><init>(Lbg/b;Ljava/lang/String;)V

    .line 262
    sget-object v1, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v2, Lbg/d;

    invoke-direct {v2, p0, v0}, Lbg/d;-><init>(Lbg/b;LR/c;)V

    invoke-static {v1, v2}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 268
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;LaN/k;Lcom/google/googlenav/layer/r;Lcom/google/googlenav/offers/j;Lbg/c;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 117
    invoke-direct/range {p0 .. p13}, Lbg/b;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;LaN/k;Lcom/google/googlenav/layer/r;Lcom/google/googlenav/offers/j;)V

    return-void
.end method

.method static synthetic a(Lbg/b;J)J
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 117
    iput-wide p1, p0, Lbg/b;->y:J

    return-wide p1
.end method

.method static synthetic a(Lbg/b;Lcom/google/android/maps/driveabout/vector/c;)Lcom/google/android/maps/driveabout/vector/c;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 117
    iput-object p1, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    return-object p1
.end method

.method private static a(Landroid/view/View;)Lcom/google/android/maps/driveabout/vector/f;
    .registers 4
    .parameter

    .prologue
    const/16 v2, 0x8

    .line 1028
    const v0, 0x7f100048

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1033
    if-eqz v0, :cond_2a

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v2, :cond_2a

    .line 1035
    const v1, 0x7f10001a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1036
    if-eqz v0, :cond_2a

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v2, :cond_2a

    instance-of v0, v0, Lcom/google/googlenav/ui/android/BubbleButton;

    if-eqz v0, :cond_2a

    .line 1038
    new-instance v0, Lcom/google/googlenav/ui/android/ae;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/ae;-><init>(Landroid/view/View;)V

    .line 1041
    :goto_29
    return-object v0

    :cond_2a
    new-instance v0, Lcom/google/android/maps/driveabout/vector/f;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/f;-><init>(Landroid/view/View;)V

    goto :goto_29
.end method

.method static synthetic a(Lbg/b;Lbf/i;I)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 117
    invoke-virtual {p0, p1, p2}, Lbg/b;->a(Lbf/i;I)V

    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;Lah/c;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1118
    return-void
.end method

.method private a(Lbf/i;Lcom/google/googlenav/E;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 898
    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    if-nez v0, :cond_10

    move-object v0, p2

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-nez v0, :cond_21

    .line 900
    :cond_10
    invoke-interface {p2}, Lcom/google/googlenav/E;->e()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {p2}, Lcom/google/googlenav/E;->c()B

    move-result v0

    if-eqz v0, :cond_1f

    const/4 v0, 0x1

    :goto_1d
    move v1, v0

    .line 907
    :cond_1e
    :goto_1e
    return v1

    :cond_1f
    move v0, v1

    .line 900
    goto :goto_1d

    .line 902
    :cond_21
    check-cast p1, Lbf/bk;

    move-object v0, p2

    .line 903
    check-cast v0, Lcom/google/googlenav/W;

    .line 904
    invoke-virtual {p1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/aZ;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 907
    invoke-interface {p2}, Lcom/google/googlenav/E;->e()Z

    move-result v1

    goto :goto_1e
.end method

.method static synthetic a(Lbg/b;)Z
    .registers 2
    .parameter

    .prologue
    .line 117
    iget-boolean v0, p0, Lbg/b;->u:Z

    return v0
.end method

.method static synthetic a(Lbg/b;Lbf/i;Lcom/google/googlenav/E;)Z
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Lbg/b;->a(Lbf/i;Lcom/google/googlenav/E;)Z

    move-result v0

    return v0
.end method

.method private ad()V
    .registers 2

    .prologue
    .line 667
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_9

    .line 668
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    .line 670
    :cond_9
    return-void
.end method

.method private ae()V
    .registers 4

    .prologue
    .line 995
    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_25

    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_25

    .line 996
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->g()LS/b;

    move-result-object v0

    .line 997
    if-eqz v0, :cond_25

    .line 998
    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {v1}, Lbf/i;->L()Z

    move-result v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->at()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LS/b;->a(ZZ)V

    .line 1002
    :cond_25
    return-void
.end method

.method private af()V
    .registers 3

    .prologue
    .line 1253
    const-string v0, "LayerTransit"

    invoke-virtual {p0, v0}, Lbg/b;->e(Ljava/lang/String;)Lbf/i;

    move-result-object v0

    check-cast v0, Lbg/q;

    .line 1255
    if-eqz v0, :cond_16

    .line 1256
    invoke-virtual {v0}, Lbg/q;->bH()Z

    move-result v1

    if-eqz v1, :cond_17

    .line 1257
    const/4 v1, 0x0

    check-cast v1, Lo/o;

    invoke-virtual {v0, v1}, Lbg/q;->a(Lo/o;)V

    .line 1262
    :cond_16
    :goto_16
    return-void

    .line 1259
    :cond_17
    invoke-virtual {p0, v0}, Lbg/b;->h(Lbf/i;)V

    goto :goto_16
.end method

.method private ag()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1286
    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    instance-of v0, v0, Lbf/O;

    if-eqz v0, :cond_b

    .line 1287
    const-string v0, "dd"

    .line 1289
    :goto_a
    return-object v0

    :cond_b
    const-string v0, "1"

    goto :goto_a
.end method

.method private static b(Lbf/i;I)Lbg/j;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 819
    invoke-virtual {p0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    .line 820
    if-nez v2, :cond_d

    .line 894
    :cond_c
    :goto_c
    return-object v4

    .line 830
    :cond_d
    invoke-interface {v2}, Lcom/google/googlenav/E;->c()B

    move-result v0

    if-eqz v0, :cond_91

    .line 833
    invoke-virtual {p0, p1}, Lbf/i;->c(I)Lcom/google/googlenav/e;

    move-result-object v1

    .line 834
    if-eqz v1, :cond_91

    .line 835
    invoke-virtual {v1}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 836
    invoke-virtual {v1}, Lcom/google/googlenav/e;->b()I

    move-result v5

    .line 837
    invoke-virtual {v1}, Lcom/google/googlenav/e;->c()I

    move-result v6

    .line 838
    invoke-virtual {v1}, Lcom/google/googlenav/e;->d()Z

    move-result v9

    .line 845
    :goto_2f
    instance-of v0, p0, Lbf/aA;

    if-eqz v0, :cond_81

    move-object v1, v2

    .line 846
    check-cast v1, Lbf/aF;

    .line 847
    new-instance v0, Lbg/n;

    invoke-virtual {p0, v2}, Lbf/i;->b(Lcom/google/googlenav/E;)I

    move-result v7

    invoke-virtual {p0, v2}, Lbf/i;->c(Lcom/google/googlenav/E;)I

    move-result v8

    invoke-virtual {v1}, Lbf/aF;->h()I

    move-result v10

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, Lbg/n;-><init>(Lbf/i;Lcom/google/googlenav/E;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIIIZI)V

    move-object v4, v0

    .line 855
    :goto_49
    instance-of v0, p0, Lbf/O;

    if-eqz v0, :cond_c

    if-eqz p1, :cond_5b

    invoke-virtual {p0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_c

    .line 857
    :cond_5b
    invoke-interface {v2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v0

    invoke-interface {v2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    invoke-static {v0, v1}, Lo/T;->b(II)Lo/T;

    move-result-object v5

    move-object v1, p0

    .line 859
    check-cast v1, Lbf/O;

    .line 860
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Lbg/j;->b(Z)V

    .line 864
    new-instance v0, Lbg/f;

    move-object v2, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lbg/f;-><init>(Lbf/O;Lbf/i;ILbg/j;Lo/T;)V

    invoke-virtual {v4, v0}, Lbg/j;->a(LF/I;)V

    goto :goto_c

    .line 851
    :cond_81
    new-instance v0, Lbg/j;

    invoke-virtual {p0, v2}, Lbf/i;->b(Lcom/google/googlenav/E;)I

    move-result v7

    invoke-virtual {p0, v2}, Lbf/i;->c(Lcom/google/googlenav/E;)I

    move-result v8

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lbg/j;-><init>(Lbf/i;Lcom/google/googlenav/E;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIIIZ)V

    move-object v4, v0

    goto :goto_49

    :cond_91
    move v6, v9

    move v5, v9

    move-object v3, v4

    goto :goto_2f
.end method

.method static synthetic b(Lbg/b;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lbg/b;->v:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lbg/b;Lbf/i;I)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 117
    invoke-virtual {p0, p1, p2}, Lbg/b;->a(Lbf/i;I)V

    return-void
.end method

.method static synthetic c(Lbg/b;)J
    .registers 3
    .parameter

    .prologue
    .line 117
    iget-wide v0, p0, Lbg/b;->y:J

    return-wide v0
.end method

.method static synthetic d(Lbg/b;)LaN/p;
    .registers 2
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lbg/b;->e:LaN/p;

    return-object v0
.end method

.method static synthetic e(Lbg/b;)J
    .registers 3
    .parameter

    .prologue
    .line 117
    iget-wide v0, p0, Lbg/b;->x:J

    return-wide v0
.end method

.method static synthetic f(Lbg/b;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lbg/b;->w:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic g(Lbg/b;)Lbf/C;
    .registers 2
    .parameter

    .prologue
    .line 117
    invoke-virtual {p0}, Lbg/b;->h()Lbf/C;

    move-result-object v0

    return-object v0
.end method

.method private g(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1228
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/f;->b(Ljava/lang/String;)Lcom/google/googlenav/layer/e;

    move-result-object v0

    .line 1230
    if-eqz v0, :cond_20

    .line 1231
    invoke-virtual {v0}, Lcom/google/googlenav/layer/e;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    .line 1234
    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbg/b;->b(Ljava/lang/String;)Lbf/y;

    move-result-object v0

    .line 1236
    if-eqz v0, :cond_21

    .line 1237
    check-cast v0, Lbg/q;

    invoke-virtual {v0, p1}, Lbg/q;->b(Ljava/lang/String;)V

    .line 1250
    :cond_20
    :goto_20
    return-void

    .line 1243
    :cond_21
    const/4 v0, 0x1

    .line 1245
    invoke-virtual {v1, p1}, Lcom/google/googlenav/layer/m;->a(Ljava/lang/String;)V

    .line 1246
    invoke-virtual {p0, v1, v2, v0, v2}, Lbg/b;->a(Lcom/google/googlenav/layer/m;ZZZ)Lbf/y;

    goto :goto_20
.end method

.method static synthetic h(Lbg/b;)Lcom/google/common/collect/Q;
    .registers 2
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    return-object v0
.end method

.method static synthetic i(Lbg/b;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lbg/b;->o:Ljava/lang/Object;

    return-object v0
.end method

.method private o(Lbf/i;)V
    .registers 10
    .parameter

    .prologue
    .line 441
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-nez v0, :cond_5

    .line 479
    :goto_4
    return-void

    .line 445
    :cond_5
    invoke-virtual {p1}, Lbf/i;->e()[Lcom/google/googlenav/ui/aI;

    move-result-object v3

    .line 446
    if-nez v3, :cond_f

    .line 447
    invoke-direct {p0, p1}, Lbg/b;->p(Lbf/i;)V

    goto :goto_4

    .line 451
    :cond_f
    iget-object v0, p0, Lbg/b;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 452
    array-length v1, v3

    invoke-static {v1}, Lcom/google/common/collect/Maps;->a(I)Ljava/util/HashMap;

    move-result-object v4

    .line 455
    array-length v5, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1f
    if-ge v2, v5, :cond_47

    aget-object v6, v3, v2

    .line 456
    if-eqz v6, :cond_43

    .line 457
    const/4 v1, 0x0

    .line 460
    if-eqz v0, :cond_2e

    .line 461
    invoke-interface {v0, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbg/a;

    .line 464
    :cond_2e
    if-nez v1, :cond_40

    .line 465
    new-instance v1, Lbg/a;

    iget-object v7, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v7}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v1, v7, v6}, Lbg/a;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/aI;)V

    .line 466
    iget-object v7, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v7, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 468
    :cond_40
    invoke-interface {v4, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    :cond_43
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1f

    .line 473
    :cond_47
    if-eqz v0, :cond_63

    .line 474
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_51
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_63

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbg/a;

    .line 475
    iget-object v2, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v2, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_51

    .line 478
    :cond_63
    iget-object v0, p0, Lbg/b;->m:Ljava/util/Map;

    invoke-interface {v0, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4
.end method

.method private p(Lbf/i;)V
    .registers 5
    .parameter

    .prologue
    .line 482
    iget-object v0, p0, Lbg/b;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 484
    if-nez v0, :cond_b

    .line 491
    :goto_a
    return-void

    .line 487
    :cond_b
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbg/a;

    .line 488
    iget-object v2, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v2, v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_13

    .line 490
    :cond_25
    iget-object v0, p0, Lbg/b;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a
.end method

.method private q(Lbf/i;)V
    .registers 6
    .parameter

    .prologue
    .line 1063
    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0xf

    if-ne v0, v1, :cond_1f

    .line 1064
    iget-object v0, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_1e

    .line 1066
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    iget-object v2, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    .line 1069
    iget-object v0, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    iget-object v1, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    sget-object v2, Lah/c;->a:Lah/c;

    invoke-direct {p0, v0, v1, v2}, Lbg/b;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;Lah/c;)V

    .line 1098
    :cond_1e
    :goto_1e
    return-void

    .line 1071
    :cond_1f
    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_43

    move-object v0, p1

    check-cast v0, Lbf/C;

    invoke-virtual {v0}, Lbf/C;->bI()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 1073
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    iget-object v2, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    .line 1076
    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    iget-object v1, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    sget-object v2, Lah/c;->c:Lah/c;

    invoke-direct {p0, v0, v1, v2}, Lbg/b;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;Lah/c;)V

    goto :goto_1e

    .line 1078
    :cond_43
    const/4 v1, 0x0

    .line 1079
    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v0, p1}, Lcom/google/common/collect/Q;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    .line 1080
    if-eqz v0, :cond_7f

    .line 1081
    invoke-virtual {p1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/F;->c()I

    move-result v2

    .line 1082
    if-ltz v2, :cond_7f

    .line 1083
    invoke-virtual {p1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/E;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(Ljava/lang/Object;)LF/H;

    move-result-object v2

    .line 1085
    if-eqz v2, :cond_7f

    .line 1086
    const/4 v0, 0x1

    .line 1087
    iget-object v1, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v3, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    .line 1090
    iget-object v1, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    sget-object v3, Lah/c;->b:Lah/c;

    invoke-direct {p0, v2, v1, v3}, Lbg/b;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;Lah/c;)V

    .line 1094
    :goto_79
    if-nez v0, :cond_1e

    .line 1095
    invoke-virtual {p0, p1}, Lbg/b;->b(Lbf/i;)V

    goto :goto_1e

    :cond_7f
    move v0, v1

    goto :goto_79
.end method


# virtual methods
.method public F()Z
    .registers 13

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 532
    iget-object v0, p0, Lbg/b;->f:LaN/u;

    invoke-virtual {v0}, LaN/u;->l()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 636
    :cond_a
    :goto_a
    return v5

    .line 536
    :cond_b
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    .line 541
    iget-object v0, p0, Lbg/b;->e:LaN/p;

    invoke-virtual {v0}, LaN/p;->g()[LaN/P;

    move-result-object v4

    array-length v6, v4

    move v0, v5

    :goto_1f
    if-ge v0, v6, :cond_31

    aget-object v7, v4, v0

    .line 547
    iget-object v8, p0, Lbg/b;->e:LaN/p;

    invoke-virtual {v8}, LaN/p;->a()LaN/D;

    move-result-object v8

    int-to-long v9, v5

    add-long/2addr v9, v1

    invoke-virtual {v8, v7, v3, v9, v10}, LaN/D;->a(LaN/P;ZJ)Ljava/util/Vector;

    .line 541
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f

    .line 549
    :cond_31
    iget-object v0, p0, Lbg/b;->e:LaN/p;

    invoke-virtual {v0}, LaN/p;->a()LaN/D;

    move-result-object v0

    invoke-virtual {v0}, LaN/D;->k()V

    .line 552
    invoke-super {p0}, Lbf/am;->F()Z

    move-result v0

    .line 554
    iget-boolean v1, p0, Lbg/b;->z:Z

    if-eqz v1, :cond_45

    .line 555
    iput-boolean v5, p0, Lbg/b;->z:Z

    move v0, v3

    .line 559
    :cond_45
    iget-object v1, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    if-eqz v1, :cond_54

    iget-object v1, p0, Lbg/b;->d:LaH/m;

    invoke-interface {v1}, LaH/m;->g()Z

    move-result v1

    if-nez v1, :cond_54

    .line 560
    invoke-virtual {p0}, Lbg/b;->ab()V

    .line 564
    :cond_54
    if-eqz v0, :cond_a

    .line 571
    iget-object v1, p0, Lbg/b;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 572
    :try_start_59
    iget-object v0, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v0, :cond_78

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v0

    if-eqz v0, :cond_78

    .line 573
    iget-object v0, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/c;->f()Lo/D;

    move-result-object v0

    .line 574
    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v2

    .line 576
    if-eqz v0, :cond_78

    invoke-virtual {v2, v0}, Ln/q;->d(Lo/D;)Z

    move-result v0

    if-nez v0, :cond_78

    .line 577
    invoke-virtual {p0}, Lbg/b;->U()V

    .line 580
    :cond_78
    monitor-exit v1
    :try_end_79
    .catchall {:try_start_59 .. :try_end_79} :catchall_9b

    move v4, v5

    move v2, v5

    .line 584
    :goto_7b
    iget-object v0, p0, Lbg/b;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v4, v0, :cond_111

    .line 585
    iget-object v0, p0, Lbg/b;->i:Ljava/util/Vector;

    invoke-virtual {v0, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 586
    iget-object v1, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v1, v0}, Lcom/google/common/collect/Q;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/maps/driveabout/vector/A;

    .line 587
    if-nez v1, :cond_9e

    move v1, v2

    .line 584
    :goto_96
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v2, v1

    goto :goto_7b

    .line 580
    :catchall_9b
    move-exception v0

    :try_start_9c
    monitor-exit v1
    :try_end_9d
    .catchall {:try_start_9c .. :try_end_9d} :catchall_9b

    throw v0

    .line 591
    :cond_9e
    invoke-virtual {p0, v0}, Lbg/b;->n(Lbf/i;)V

    .line 592
    invoke-virtual {v0}, Lbf/i;->ax()Z

    move-result v6

    if-eqz v6, :cond_d7

    invoke-virtual {v0}, Lbf/i;->aj()Z

    move-result v6

    if-eqz v6, :cond_d7

    move v6, v3

    .line 594
    :goto_ae
    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v9

    move v7, v5

    move v8, v2

    .line 595
    :goto_b4
    invoke-interface {v9}, Lcom/google/googlenav/F;->f()I

    move-result v2

    if-ge v7, v2, :cond_136

    .line 596
    invoke-interface {v9, v7}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v10

    .line 597
    invoke-interface {v10}, Lcom/google/googlenav/E;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(Ljava/lang/Object;)LF/H;

    move-result-object v2

    check-cast v2, Lbg/j;

    .line 599
    invoke-interface {v10}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v11

    if-nez v11, :cond_d9

    if-eqz v2, :cond_d9

    .line 604
    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/A;->b(LF/H;)V

    .line 595
    :cond_d3
    :goto_d3
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_b4

    :cond_d7
    move v6, v5

    .line 592
    goto :goto_ae

    .line 608
    :cond_d9
    invoke-virtual {v0, v7}, Lbf/i;->c(I)Lcom/google/googlenav/e;

    move-result-object v11

    .line 609
    if-eqz v2, :cond_d3

    .line 610
    invoke-virtual {v2, v11}, Lbg/j;->a(Lcom/google/googlenav/e;)Z

    move-result v11

    if-nez v11, :cond_f7

    invoke-virtual {v2}, Lbg/j;->e()Lo/T;

    move-result-object v11

    invoke-interface {v10}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v10

    invoke-static {v10}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v10

    invoke-virtual {v11, v10}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_d3

    .line 612
    :cond_f7
    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/A;->b(LF/H;)V

    .line 613
    invoke-static {v0, v7}, Lbg/b;->b(Lbf/i;I)Lbg/j;

    move-result-object v2

    .line 614
    if-eqz v2, :cond_103

    .line 615
    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/H;)V

    .line 617
    :cond_103
    if-eqz v6, :cond_d3

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/F;->c()I

    move-result v2

    if-ne v2, v7, :cond_d3

    move v8, v3

    .line 618
    goto :goto_d3

    .line 627
    :cond_111
    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    .line 628
    if-eqz v2, :cond_11e

    .line 629
    invoke-virtual {v0}, Lbf/i;->ak()Lcom/google/googlenav/ui/view/d;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbg/b;->a(Lbf/i;Lcom/google/googlenav/ui/view/d;)V

    .line 631
    :cond_11e
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbg/b;->x:J

    .line 632
    iget-object v1, p0, Lbg/b;->w:Ljava/lang/Object;

    monitor-enter v1

    .line 633
    :try_start_127
    iget-object v0, p0, Lbg/b;->w:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 634
    monitor-exit v1
    :try_end_12d
    .catchall {:try_start_127 .. :try_end_12d} :catchall_133

    .line 635
    invoke-direct {p0}, Lbg/b;->ad()V

    move v5, v3

    .line 636
    goto/16 :goto_a

    .line 634
    :catchall_133
    move-exception v0

    :try_start_134
    monitor-exit v1
    :try_end_135
    .catchall {:try_start_134 .. :try_end_135} :catchall_133

    throw v0

    :cond_136
    move v1, v8

    goto/16 :goto_96
.end method

.method public G()V
    .registers 3

    .prologue
    .line 645
    iget-object v0, p0, Lbg/b;->t:Lcom/google/android/maps/driveabout/vector/az;

    if-eqz v0, :cond_1e

    .line 646
    iget-object v0, p0, Lbg/b;->t:Lcom/google/android/maps/driveabout/vector/az;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/az;->e()Z

    move-result v0

    .line 647
    iget-object v1, p0, Lbg/b;->e:LaN/p;

    invoke-virtual {v1}, LaN/p;->a()LaN/D;

    move-result-object v1

    invoke-virtual {v1}, LaN/D;->m()Z

    move-result v1

    .line 648
    if-eq v0, v1, :cond_1e

    .line 649
    iget-object v0, p0, Lbg/b;->t:Lcom/google/android/maps/driveabout/vector/az;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/az;->b(Z)V

    .line 650
    invoke-direct {p0}, Lbg/b;->ad()V

    .line 653
    :cond_1e
    return-void
.end method

.method protected T()V
    .registers 2

    .prologue
    .line 1506
    const/4 v0, 0x1

    .line 1507
    invoke-virtual {p0, v0}, Lbg/b;->a(Z)Lbf/by;

    .line 1508
    invoke-virtual {p0}, Lbg/b;->s()V

    .line 1509
    return-void
.end method

.method public U()V
    .registers 2

    .prologue
    .line 1581
    invoke-super {p0}, Lbf/am;->U()V

    .line 1585
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbg/b;->z:Z

    .line 1586
    invoke-virtual {p0}, Lbg/b;->F()Z

    .line 1587
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;ZBZZ)Lbf/ak;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1123
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lbg/b;->a(I)V

    .line 1125
    new-instance v0, Lbf/ak;

    iget-object v1, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbg/b;->e:LaN/p;

    iget-object v3, p0, Lbg/b;->f:LaN/u;

    iget-object v4, p0, Lbg/b;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbg/b;->h:LaN/k;

    move-object v6, p1

    move v7, p2

    move v8, p3

    move v9, p4

    invoke-direct/range {v0 .. v9}, Lbf/ak;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;Lcom/google/googlenav/ai;ZBZ)V

    .line 1127
    iget-object v1, p0, Lbg/b;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 1130
    const/4 v2, 0x0

    :try_start_1c
    invoke-static {v0, v2}, Lbg/b;->b(Lbf/i;I)Lbg/j;

    move-result-object v2

    iput-object v2, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    .line 1131
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_1c .. :try_end_23} :catchall_3c

    .line 1132
    invoke-virtual {p0, v0, p5}, Lbg/b;->a(Lbf/i;Z)V

    .line 1134
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->w()Z

    move-result v1

    if-eqz v1, :cond_3b

    .line 1135
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_3f

    check-cast p1, Lcom/google/googlenav/W;

    invoke-virtual {p1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    .line 1137
    :goto_38
    invoke-direct {p0, v1}, Lbg/b;->g(Ljava/lang/String;)V

    .line 1140
    :cond_3b
    return-object v0

    .line 1131
    :catchall_3c
    move-exception v0

    :try_start_3d
    monitor-exit v1
    :try_end_3e
    .catchall {:try_start_3d .. :try_end_3e} :catchall_3c

    throw v0

    .line 1135
    :cond_3f
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bV()Ljava/lang/String;

    move-result-object v1

    goto :goto_38
.end method

.method protected a(Lcom/google/googlenav/layer/m;)Lbf/bE;
    .registers 10
    .parameter

    .prologue
    .line 1007
    new-instance v0, Lbg/o;

    iget-object v1, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbg/b;->e:LaN/p;

    iget-object v3, p0, Lbg/b;->f:LaN/u;

    iget-object v4, p0, Lbg/b;->g:Lcom/google/googlenav/ui/X;

    new-instance v6, LaN/k;

    invoke-direct {v6}, LaN/k;-><init>()V

    iget-object v7, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-object v5, p1

    invoke-direct/range {v0 .. v7}, Lbg/o;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/android/maps/driveabout/vector/VectorMapView;)V

    .line 1009
    return-object v0
.end method

.method protected a(Lcom/google/googlenav/layer/m;Z)Lbf/y;
    .registers 12
    .parameter
    .parameter

    .prologue
    .line 1015
    new-instance v0, Lbg/q;

    iget-object v1, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbg/b;->e:LaN/p;

    iget-object v3, p0, Lbg/b;->f:LaN/u;

    iget-object v4, p0, Lbg/b;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbg/b;->h:LaN/k;

    iget-object v7, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-object v5, p1

    move v8, p2

    invoke-direct/range {v0 .. v8}, Lbg/q;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/android/maps/driveabout/vector/VectorMapView;Z)V

    .line 1017
    return-object v0
.end method

.method public a(Lbf/i;)V
    .registers 3
    .parameter

    .prologue
    .line 1102
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-nez v0, :cond_5

    .line 1107
    :goto_4
    return-void

    .line 1105
    :cond_5
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->p()V

    .line 1106
    invoke-direct {p0, p1}, Lbg/b;->q(Lbf/i;)V

    goto :goto_4
.end method

.method public a(Lbf/i;Lcom/google/googlenav/ui/view/d;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1046
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_6

    if-nez p2, :cond_7

    .line 1060
    :cond_6
    :goto_6
    return-void

    .line 1052
    :cond_7
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V

    .line 1054
    invoke-virtual {p2}, Lcom/google/googlenav/ui/view/d;->d()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lbg/b;->a(Landroid/view/View;)Lcom/google/android/maps/driveabout/vector/f;

    move-result-object v0

    iput-object v0, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    .line 1056
    instance-of v0, p1, Lcom/google/android/maps/driveabout/vector/h;

    if-eqz v0, :cond_22

    .line 1057
    iget-object v1, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    move-object v0, p1

    check-cast v0, Lcom/google/android/maps/driveabout/vector/h;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/f;->a(Lcom/google/android/maps/driveabout/vector/h;)V

    .line 1059
    :cond_22
    invoke-direct {p0, p1}, Lbg/b;->q(Lbf/i;)V

    goto :goto_6
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/VectorMapView;)V
    .registers 13
    .parameter

    .prologue
    const/4 v10, 0x2

    const v9, 0x338cc6ef

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 287
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-ne v0, p1, :cond_c

    .line 429
    :goto_b
    return-void

    .line 290
    :cond_c
    iput-object p1, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    .line 291
    iget-object v0, p0, Lbg/b;->s:Lbg/k;

    iget-object v1, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, v1}, Lbg/k;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;)V

    .line 292
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-nez v0, :cond_31

    .line 293
    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v0}, Lcom/google/common/collect/Q;->clear()V

    .line 294
    iget-object v0, p0, Lbg/b;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 295
    iput-object v8, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    .line 296
    iput-object v8, p0, Lbg/b;->t:Lcom/google/android/maps/driveabout/vector/az;

    .line 297
    iput-object v8, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    .line 298
    iput-object v8, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    .line 301
    iget-object v0, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/s;->d(Z)V

    goto :goto_b

    :cond_31
    move v1, v2

    .line 306
    :goto_32
    iget-object v0, p0, Lbg/b;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_76

    .line 307
    iget-object v0, p0, Lbg/b;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 308
    iget-object v3, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v3, v0}, Lcom/google/common/collect/Q;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_68

    .line 309
    iget-object v3, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v3, v4}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/E;)Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v3

    .line 313
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v4

    if-ne v4, v10, :cond_72

    .line 314
    invoke-virtual {v3, v7}, Lcom/google/android/maps/driveabout/vector/A;->b(I)V

    .line 320
    :goto_5b
    invoke-virtual {p1, v10}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setBaseDistancePenaltyFactorForLabelOverlay(I)V

    .line 321
    iget-object v4, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v4, v0, v3}, Lcom/google/common/collect/Q;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    iget-object v4, p0, Lbg/b;->s:Lbg/k;

    invoke-virtual {v4, v3}, Lbg/k;->a(Lcom/google/android/maps/driveabout/vector/A;)V

    .line 324
    :cond_68
    invoke-virtual {p0, v0}, Lbg/b;->c(Lbf/i;)V

    .line 325
    invoke-direct {p0, v0}, Lbg/b;->o(Lbf/i;)V

    .line 306
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_32

    .line 316
    :cond_72
    invoke-virtual {v3, v2}, Lcom/google/android/maps/driveabout/vector/A;->b(I)V

    goto :goto_5b

    .line 327
    :cond_76
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lbg/b;->s:Lbg/k;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 331
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, v7}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->c(Z)Lcom/google/android/maps/driveabout/vector/aA;

    move-result-object v0

    iput-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    .line 332
    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    invoke-virtual {v0, v7}, Lcom/google/android/maps/driveabout/vector/aA;->b(Z)V

    .line 335
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 359
    iget-object v1, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    const/4 v3, 0x5

    new-array v3, v3, [Lcom/google/android/maps/driveabout/vector/aC;

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/maps/driveabout/vector/aD;->b(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/maps/driveabout/vector/aD;->c(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    const v5, 0x7f02045d

    const v6, 0x7f02045e

    invoke-virtual {v4, v5, v6}, Lcom/google/android/maps/driveabout/vector/aD;->a(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v2, v9}, Lcom/google/android/maps/driveabout/vector/aD;->b(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/maps/driveabout/vector/aD;->b(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/maps/driveabout/vector/aD;->c(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->b()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    const v5, 0x7f020459

    const v6, 0x7f02045a

    invoke-virtual {v4, v5, v6}, Lcom/google/android/maps/driveabout/vector/aD;->a(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v2, v9}, Lcom/google/android/maps/driveabout/vector/aD;->b(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/maps/driveabout/vector/aD;->b(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    const v5, 0x7f02045f

    const v6, 0x7f020460

    invoke-virtual {v4, v5, v6}, Lcom/google/android/maps/driveabout/vector/aD;->a(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    const v5, 0x73217bce

    invoke-virtual {v4, v5, v9}, Lcom/google/android/maps/driveabout/vector/aD;->b(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v4

    aput-object v4, v3, v10

    const/4 v4, 0x3

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/maps/driveabout/vector/aD;->b(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aD;->b()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    const v5, 0x7f02045b

    const v6, 0x7f02045c

    invoke-virtual {v2, v5, v6}, Lcom/google/android/maps/driveabout/vector/aD;->a(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    const v5, 0x73217bce

    invoke-virtual {v2, v5, v9}, Lcom/google/android/maps/driveabout/vector/aD;->b(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v2, 0x4

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/maps/driveabout/vector/aD;->b(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->b()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    const v5, 0x7f020461

    const v6, 0x7f020462

    invoke-virtual {v4, v5, v6}, Lcom/google/android/maps/driveabout/vector/aD;->a(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    const v5, 0x73aaaaaa

    const v6, 0x33cccccc

    invoke-virtual {v4, v5, v6}, Lcom/google/android/maps/driveabout/vector/aD;->b(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {v1, v3}, Lcom/google/android/maps/driveabout/vector/aA;->a([Lcom/google/android/maps/driveabout/vector/aC;)V

    .line 400
    iget-object v1, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    const v2, 0x7f0b00ec

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    const v3, 0x7f0c0005

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    const v4, 0x7f0c0006

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a(FII)V

    .line 405
    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->x:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lcom/google/android/maps/driveabout/vector/E;)V

    .line 406
    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    const/high16 v1, 0x4160

    const/high16 v2, 0x4120

    const v3, 0x3f4ccccd

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/aA;->a(FFF)V

    .line 407
    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    new-instance v1, Lbg/e;

    invoke-direct {v1, p0}, Lbg/e;-><init>(Lbg/b;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lcom/google/android/maps/driveabout/vector/F;)V

    .line 419
    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->d(I)V

    .line 420
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 423
    new-instance v0, Lcom/google/android/maps/driveabout/vector/az;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/az;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lbg/b;->t:Lcom/google/android/maps/driveabout/vector/az;

    .line 424
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lbg/b;->t:Lcom/google/android/maps/driveabout/vector/az;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 427
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    new-instance v1, Lbg/l;

    invoke-direct {v1, p0, v8}, Lbg/l;-><init>(Lbg/b;Lbg/c;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setLabelTapListener(Lcom/google/android/maps/driveabout/vector/e;)V

    .line 428
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V

    goto/16 :goto_b
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/c;)V
    .registers 3
    .parameter

    .prologue
    .line 1266
    if-nez p1, :cond_6

    .line 1269
    invoke-virtual {p0}, Lbg/b;->U()V

    .line 1283
    :cond_5
    :goto_5
    return-void

    .line 1280
    :cond_6
    iget-object v0, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    if-eqz v0, :cond_5

    .line 1281
    iget-object v0, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/f;->d()V

    goto :goto_5
.end method

.method public a(Ln/q;)V
    .registers 2
    .parameter

    .prologue
    .line 1592
    return-void
.end method

.method public a(Ln/q;Lo/y;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1596
    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    if-nez v0, :cond_5

    .line 1604
    :goto_4
    return-void

    .line 1599
    :cond_5
    iget-object v0, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->b()V

    .line 1602
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbg/b;->z:Z

    .line 1603
    invoke-direct {p0}, Lbg/b;->ad()V

    goto :goto_4
.end method

.method public a(Lo/S;)V
    .registers 4
    .parameter

    .prologue
    .line 501
    invoke-virtual {p1}, Lo/S;->a()Lo/T;

    move-result-object v0

    if-nez v0, :cond_2d

    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_2d

    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_2d

    .line 504
    invoke-virtual {p0}, Lbg/b;->H()Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/C;

    .line 505
    invoke-virtual {v0}, Lbf/C;->bI()Z

    move-result v1

    if-eqz v1, :cond_2d

    invoke-virtual {v0}, Lbf/C;->aa()Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 506
    invoke-virtual {p0, v0}, Lbg/b;->h(Lbf/i;)V

    .line 509
    :cond_2d
    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    if-eqz v0, :cond_36

    .line 510
    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lo/S;)V

    .line 512
    :cond_36
    return-void
.end method

.method protected a(Lbf/i;ZZ)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1527
    invoke-super {p0, p1, p2, p3}, Lbf/am;->a(Lbf/i;ZZ)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1528
    const/4 v0, 0x0

    .line 1548
    :goto_7
    return v0

    .line 1530
    :cond_8
    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0xf

    if-eq v0, v1, :cond_35

    .line 1535
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_32

    .line 1536
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/E;)Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v0

    .line 1537
    new-instance v1, Lbg/m;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lbg/m;-><init>(Lbg/b;Lbg/c;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/A;->a(Lcom/google/android/maps/driveabout/vector/e;)V

    .line 1538
    iget-object v1, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v1, p1, v0}, Lcom/google/common/collect/Q;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1541
    invoke-virtual {p0, p1}, Lbg/b;->c(Lbf/i;)V

    .line 1542
    iget-object v1, p0, Lbg/b;->s:Lbg/k;

    invoke-virtual {v1, v0}, Lbg/k;->a(Lcom/google/android/maps/driveabout/vector/A;)V

    .line 1544
    :cond_32
    invoke-virtual {p1, p0}, Lbf/i;->a(Lbf/k;)V

    .line 1546
    :cond_35
    invoke-virtual {p1, p0}, Lbf/i;->a(Lbf/j;)V

    .line 1547
    invoke-direct {p0, p1}, Lbg/b;->o(Lbf/i;)V

    .line 1548
    const/4 v0, 0x1

    goto :goto_7
.end method

.method public ab()V
    .registers 3

    .prologue
    .line 519
    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    if-eqz v0, :cond_d

    .line 520
    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lo/S;)V

    .line 521
    invoke-direct {p0}, Lbg/b;->ad()V

    .line 523
    :cond_d
    return-void
.end method

.method public ac()Lcom/google/android/maps/driveabout/vector/VectorMapView;
    .registers 2

    .prologue
    .line 1576
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    return-object v0
.end method

.method public b(Lbf/i;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1308
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_a

    .line 1309
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->p()V

    .line 1311
    :cond_a
    iput-object v1, p0, Lbg/b;->q:Lcom/google/android/maps/driveabout/vector/f;

    .line 1312
    invoke-direct {p0}, Lbg/b;->af()V

    .line 1313
    iget-object v1, p0, Lbg/b;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 1314
    const/4 v0, 0x0

    :try_start_13
    iput-object v0, p0, Lbg/b;->n:Lcom/google/android/maps/driveabout/vector/c;

    .line 1315
    monitor-exit v1

    .line 1321
    return-void

    .line 1315
    :catchall_17
    move-exception v0

    monitor-exit v1
    :try_end_19
    .catchall {:try_start_13 .. :try_end_19} :catchall_17

    throw v0
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/c;)V
    .registers 7
    .parameter

    .prologue
    .line 1294
    const/16 v0, 0x12

    invoke-direct {p0}, Lbg/b;->ag()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "a=d"

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1302
    iget-object v0, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/c;->e()Lo/T;

    move-result-object v1

    invoke-static {v1}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v1

    sget-object v2, Lbg/b;->k:LaN/Y;

    invoke-virtual {v0, v1, v2}, LaN/u;->d(LaN/B;LaN/Y;)V

    .line 1304
    return-void
.end method

.method public b(Ln/q;)V
    .registers 3
    .parameter

    .prologue
    .line 1608
    iget-object v0, p0, Lbg/b;->r:Lcom/google/android/maps/driveabout/vector/aA;

    if-nez v0, :cond_5

    .line 1614
    :goto_4
    return-void

    .line 1611
    :cond_5
    iget-object v0, p0, Lbg/b;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->b()V

    .line 1612
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbg/b;->z:Z

    .line 1613
    invoke-direct {p0}, Lbg/b;->ad()V

    goto :goto_4
.end method

.method public c()V
    .registers 3

    .prologue
    .line 272
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbg/b;->u:Z

    .line 273
    iget-object v1, p0, Lbg/b;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 274
    :try_start_6
    iget-object v0, p0, Lbg/b;->v:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 275
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_6 .. :try_end_c} :catchall_16

    .line 276
    iget-object v1, p0, Lbg/b;->w:Ljava/lang/Object;

    monitor-enter v1

    .line 277
    :try_start_f
    iget-object v0, p0, Lbg/b;->w:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 278
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_f .. :try_end_15} :catchall_19

    .line 279
    return-void

    .line 275
    :catchall_16
    move-exception v0

    :try_start_17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    throw v0

    .line 278
    :catchall_19
    move-exception v0

    :try_start_1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_1a .. :try_end_1b} :catchall_19

    throw v0
.end method

.method public c(Lbf/i;)V
    .registers 8
    .parameter

    .prologue
    .line 912
    invoke-virtual {p0, p1}, Lbg/b;->l(Lbf/i;)V

    .line 914
    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v0, p1}, Lcom/google/common/collect/Q;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    .line 915
    if-nez v0, :cond_e

    .line 961
    :cond_d
    return-void

    .line 918
    :cond_e
    invoke-virtual {p1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v2

    .line 919
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v3

    .line 920
    const/4 v1, 0x0

    :goto_17
    invoke-interface {v2}, Lcom/google/googlenav/F;->f()I

    move-result v4

    if-ge v1, v4, :cond_37

    .line 921
    invoke-interface {v2, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    .line 922
    if-eqz v4, :cond_34

    invoke-interface {v4}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v5

    if-eqz v5, :cond_34

    .line 923
    invoke-interface {v4}, Lcom/google/googlenav/E;->f()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 920
    :cond_34
    add-int/lit8 v1, v1, 0x1

    goto :goto_17

    .line 929
    :cond_37
    new-instance v1, Lbg/h;

    invoke-direct {v1, p0, v3, v2, p1}, Lbg/h;-><init>(Lbg/b;Ljava/util/Map;Lcom/google/googlenav/F;Lbf/i;)V

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v4}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/J;Z)V

    .line 951
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 952
    :cond_48
    :goto_48
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 953
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 954
    invoke-static {p1, v1}, Lbg/b;->b(Lbf/i;I)Lbg/j;

    move-result-object v4

    .line 955
    if-eqz v4, :cond_48

    .line 956
    invoke-interface {v2, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    .line 957
    invoke-direct {p0, p1, v1}, Lbg/b;->a(Lbf/i;Lcom/google/googlenav/E;)Z

    move-result v1

    invoke-virtual {v4, v1}, LF/H;->a(Z)V

    .line 958
    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/A;->a(LF/H;)V

    goto :goto_48
.end method

.method protected d()V
    .registers 1

    .prologue
    .line 971
    invoke-super {p0}, Lbf/am;->d()V

    .line 972
    invoke-direct {p0}, Lbg/b;->ae()V

    .line 973
    return-void
.end method

.method protected d(Z)V
    .registers 3
    .parameter

    .prologue
    .line 657
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_9

    .line 658
    iget-object v0, p0, Lbg/b;->p:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setShouldUpdateFeatureCluster(Z)V

    .line 660
    :cond_9
    return-void
.end method

.method public e(Lbf/i;)V
    .registers 4
    .parameter

    .prologue
    .line 983
    invoke-super {p0, p1}, Lbf/am;->e(Lbf/i;)V

    .line 984
    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v0, p1}, Lcom/google/common/collect/Q;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    .line 985
    if-eqz v0, :cond_12

    .line 986
    iget-object v1, p0, Lbg/b;->s:Lbg/k;

    invoke-virtual {v1, v0}, Lbg/k;->c(Lcom/google/android/maps/driveabout/vector/A;)V

    .line 990
    :cond_12
    invoke-direct {p0}, Lbg/b;->ae()V

    .line 991
    return-void
.end method

.method protected i(Lbf/i;)V
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1559
    invoke-super {p0, p1}, Lbf/am;->i(Lbf/i;)V

    .line 1561
    invoke-virtual {p1, v0}, Lbf/i;->a(Lbf/k;)V

    .line 1562
    invoke-virtual {p1, v0}, Lbf/i;->a(Lbf/j;)V

    .line 1565
    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v0, p1}, Lcom/google/common/collect/Q;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    .line 1566
    if-eqz v0, :cond_19

    .line 1567
    iget-object v1, p0, Lbg/b;->s:Lbg/k;

    invoke-virtual {v1, v0}, Lbg/k;->b(Lcom/google/android/maps/driveabout/vector/A;)V

    .line 1569
    :cond_19
    iget-object v0, p0, Lbg/b;->l:Lcom/google/common/collect/Q;

    invoke-interface {v0, p1}, Lcom/google/common/collect/Q;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1572
    invoke-direct {p0, p1}, Lbg/b;->p(Lbf/i;)V

    .line 1573
    return-void
.end method

.method public n(Lbf/i;)V
    .registers 2
    .parameter

    .prologue
    .line 1023
    invoke-super {p0, p1}, Lbf/am;->n(Lbf/i;)V

    .line 1024
    invoke-direct {p0, p1}, Lbg/b;->o(Lbf/i;)V

    .line 1025
    return-void
.end method
