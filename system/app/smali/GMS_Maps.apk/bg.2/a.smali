.class public Lbg/a;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/googlenav/ui/aI;

.field private c:I

.field private d:Lo/X;

.field private e:Lo/X;

.field private f:Ljava/util/List;

.field private g:F

.field private h:Lcom/google/android/maps/driveabout/vector/D;

.field private final i:Lo/T;

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/googlenav/ui/aI;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 77
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lbg/a;->i:Lo/T;

    .line 83
    iput-object p1, p0, Lbg/a;->a:Landroid/content/Context;

    .line 84
    iput-object p2, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    .line 85
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/aH;)Ljava/util/List;
    .registers 12
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 211
    invoke-interface {p1}, Lcom/google/googlenav/ui/aH;->o()[[LaN/B;

    move-result-object v4

    .line 212
    if-nez v4, :cond_c

    .line 213
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 224
    :cond_b
    return-object v0

    .line 215
    :cond_c
    array-length v0, v4

    invoke-static {v0}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 216
    array-length v5, v4

    move v3, v2

    :goto_13
    if-ge v3, v5, :cond_b

    aget-object v6, v4, v3

    .line 217
    new-instance v7, Lo/Z;

    array-length v1, v6

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v7, v1}, Lo/Z;-><init>(I)V

    .line 218
    array-length v8, v6

    move v1, v2

    :goto_21
    if-ge v1, v8, :cond_2f

    .line 219
    aget-object v9, v6, v1

    invoke-static {v9}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v9

    invoke-virtual {v7, v9}, Lo/Z;->a(Lo/T;)Z

    .line 218
    add-int/lit8 v1, v1, 0x1

    goto :goto_21

    .line 221
    :cond_2f
    aget-object v1, v6, v2

    invoke-static {v1}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v1

    invoke-virtual {v7, v1}, Lo/Z;->a(Lo/T;)Z

    .line 222
    invoke-virtual {v7}, Lo/Z;->d()Lo/X;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_13
.end method

.method private a(Lcom/google/googlenav/ui/aH;Z)Lo/X;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 195
    invoke-interface {p1}, Lcom/google/googlenav/ui/aH;->j()[LaN/B;

    move-result-object v2

    .line 196
    new-instance v3, Lo/Z;

    array-length v0, v2

    invoke-direct {v3, v0}, Lo/Z;-><init>(I)V

    .line 197
    new-instance v4, Lo/T;

    invoke-direct {v4}, Lo/T;-><init>()V

    .line 198
    array-length v5, v2

    move v0, v1

    :goto_12
    if-ge v0, v5, :cond_1f

    aget-object v6, v2, v0

    .line 199
    invoke-static {v6, v4}, LR/e;->a(LaN/B;Lo/T;)V

    .line 200
    invoke-virtual {v3, v4}, Lo/Z;->a(Lo/T;)Z

    .line 198
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 202
    :cond_1f
    if-eqz p2, :cond_29

    .line 203
    aget-object v0, v2, v1

    invoke-static {v0, v4}, LR/e;->a(LaN/B;Lo/T;)V

    .line 204
    invoke-virtual {v3, v4}, Lo/Z;->a(Lo/T;)Z

    .line 206
    :cond_29
    invoke-virtual {v3}, Lo/Z;->d()Lo/X;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/google/googlenav/ui/aH;)I
    .registers 3
    .parameter

    .prologue
    .line 229
    iget v0, p0, Lbg/a;->g:F

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/googlenav/ui/aH;->a(LaN/Y;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lbg/a;->d(LD/a;)V

    .line 90
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_c

    .line 91
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 93
    :cond_c
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 247
    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    iget v1, p0, Lbg/a;->g:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_39

    .line 248
    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    iput v0, p0, Lbg/a;->g:F

    .line 250
    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    instance-of v0, v0, Lcom/google/googlenav/ui/aG;

    if-nez v0, :cond_39

    .line 251
    iget v1, p0, Lbg/a;->j:I

    .line 252
    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    check-cast v0, Lcom/google/googlenav/ui/aH;

    invoke-direct {p0, v0}, Lbg/a;->b(Lcom/google/googlenav/ui/aH;)I

    move-result v0

    iput v0, p0, Lbg/a;->j:I

    .line 253
    iget v0, p0, Lbg/a;->j:I

    if-eq v0, v1, :cond_39

    .line 256
    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    instance-of v0, v0, LaN/M;

    if-eqz v0, :cond_36

    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    check-cast v0, LaN/M;

    invoke-virtual {v0}, LaN/M;->l()Z

    move-result v0

    if-nez v0, :cond_39

    .line 257
    :cond_36
    const/4 v0, 0x0

    iput-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    .line 263
    :cond_39
    invoke-virtual {p0, p2}, Lbg/a;->d(LD/a;)V

    .line 264
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_45

    .line 265
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/D;->b(LC/a;LD/a;)Z

    .line 268
    :cond_45
    invoke-super {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/D;->b(LC/a;LD/a;)Z

    move-result v0

    return v0
.end method

.method public c(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 273
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_9

    .line 274
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/D;->c(LD/a;)V

    .line 276
    :cond_9
    return-void
.end method

.method d(LD/a;)V
    .registers 10
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 102
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aI;->i()I

    move-result v0

    iget v2, p0, Lbg/a;->c:I

    if-ne v0, v2, :cond_12

    .line 175
    :goto_11
    return-void

    .line 106
    :cond_12
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    .line 107
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_1d

    .line 108
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;)V

    .line 110
    :cond_1d
    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    instance-of v0, v0, Lcom/google/googlenav/ui/aG;

    if-eqz v0, :cond_91

    .line 111
    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    move-object v6, v0

    check-cast v6, Lcom/google/googlenav/ui/aG;

    .line 116
    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->k()I

    move-result v3

    .line 117
    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->m()I

    move-result v4

    .line 118
    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->c()LaN/B;

    move-result-object v0

    .line 119
    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->b()I

    move-result v2

    .line 120
    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->d()Lo/D;

    move-result-object v5

    .line 121
    if-nez v5, :cond_78

    move-object v5, v7

    .line 122
    :goto_3f
    if-eqz v0, :cond_44

    const/4 v7, -0x1

    if-ne v3, v7, :cond_7d

    .line 123
    :cond_44
    iget-object v0, p0, Lbg/a;->i:Lo/T;

    invoke-virtual {v0, v1, v1}, Lo/T;->d(II)V

    move v2, v1

    move v4, v1

    move v3, v1

    .line 130
    :goto_4c
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_83

    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/a;

    if-eqz v0, :cond_83

    .line 131
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/a;

    .line 132
    iget-object v1, p0, Lbg/a;->i:Lo/T;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/a;->a(Lo/T;I)V

    .line 133
    invoke-virtual {v0, v5}, Lcom/google/android/maps/driveabout/vector/a;->a(Lo/r;)V

    .line 134
    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/a;->b(I)V

    .line 135
    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/a;->c(I)V

    .line 136
    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/a;->a(Ljava/lang/String;)V

    .line 170
    :goto_6f
    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aI;->i()I

    move-result v0

    iput v0, p0, Lbg/a;->c:I

    goto :goto_11

    .line 121
    :cond_78
    invoke-virtual {v5}, Lo/D;->a()Lo/r;

    move-result-object v5

    goto :goto_3f

    .line 128
    :cond_7d
    iget-object v1, p0, Lbg/a;->i:Lo/T;

    invoke-static {v0, v1}, LR/e;->a(LaN/B;Lo/T;)V

    goto :goto_4c

    .line 138
    :cond_83
    new-instance v0, Lcom/google/android/maps/driveabout/vector/a;

    iget-object v1, p0, Lbg/a;->i:Lo/T;

    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->n()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/a;-><init>(Lo/T;IIILo/r;Ljava/lang/String;)V

    iput-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    goto :goto_6f

    .line 142
    :cond_91
    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    move-object v5, v0

    check-cast v5, Lcom/google/googlenav/ui/aH;

    .line 143
    invoke-interface {v5}, Lcom/google/googlenav/ui/aH;->j()[LaN/B;

    move-result-object v0

    array-length v0, v0

    if-gt v0, v3, :cond_a0

    .line 144
    iput-object v7, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    goto :goto_6f

    .line 145
    :cond_a0
    invoke-interface {v5}, Lcom/google/googlenav/ui/aH;->l()Z

    move-result v0

    if-eqz v0, :cond_db

    .line 147
    iget-object v0, p0, Lbg/a;->e:Lo/X;

    if-eqz v0, :cond_b4

    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aI;->i()I

    move-result v0

    iget v1, p0, Lbg/a;->c:I

    if-eq v0, v1, :cond_c0

    .line 149
    :cond_b4
    invoke-direct {p0, v5, v3}, Lbg/a;->a(Lcom/google/googlenav/ui/aH;Z)Lo/X;

    move-result-object v0

    iput-object v0, p0, Lbg/a;->e:Lo/X;

    .line 150
    invoke-direct {p0, v5}, Lbg/a;->a(Lcom/google/googlenav/ui/aH;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbg/a;->f:Ljava/util/List;

    .line 155
    :cond_c0
    new-instance v0, Lcom/google/android/maps/driveabout/vector/G;

    iget-object v1, p0, Lbg/a;->e:Lo/X;

    iget-object v2, p0, Lbg/a;->f:Ljava/util/List;

    move-object v3, v5

    check-cast v3, LaN/M;

    invoke-virtual {v3}, LaN/M;->f()I

    move-result v3

    invoke-interface {v5}, Lcom/google/googlenav/ui/aH;->k()I

    move-result v4

    invoke-interface {v5}, Lcom/google/googlenav/ui/aH;->m()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/G;-><init>(Lo/X;Ljava/util/List;III)V

    iput-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    goto :goto_6f

    .line 159
    :cond_db
    iget-object v0, p0, Lbg/a;->d:Lo/X;

    if-eqz v0, :cond_e9

    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aI;->i()I

    move-result v0

    iget v2, p0, Lbg/a;->c:I

    if-eq v0, v2, :cond_ef

    .line 161
    :cond_e9
    invoke-direct {p0, v5, v1}, Lbg/a;->a(Lcom/google/googlenav/ui/aH;Z)Lo/X;

    move-result-object v0

    iput-object v0, p0, Lbg/a;->d:Lo/X;

    .line 163
    :cond_ef
    invoke-interface {v5}, Lcom/google/googlenav/ui/aH;->c()Lo/D;

    move-result-object v0

    .line 164
    if-nez v0, :cond_108

    move-object v0, v7

    .line 167
    :goto_f6
    new-instance v1, Lcom/google/android/maps/driveabout/vector/z;

    iget-object v2, p0, Lbg/a;->d:Lo/X;

    iget v3, p0, Lbg/a;->j:I

    int-to-float v3, v3

    invoke-interface {v5}, Lcom/google/googlenav/ui/aH;->k()I

    move-result v4

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/maps/driveabout/vector/z;-><init>(Lo/X;FILo/r;)V

    iput-object v1, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    goto/16 :goto_6f

    .line 164
    :cond_108
    invoke-virtual {v0}, Lo/D;->a()Lo/r;

    move-result-object v0

    goto :goto_f6
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 236
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->h:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
