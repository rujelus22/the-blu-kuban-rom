.class public LG/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final s:LG/a;

.field public static final t:LG/a;

.field public static final u:LG/a;

.field public static final v:LG/a;


# instance fields
.field public final a:Lcom/google/android/maps/driveabout/vector/aX;

.field public final b:I

.field public final c:I

.field public final d:Lcom/google/android/maps/driveabout/vector/aX;

.field public final e:F

.field public final f:I

.field public final g:I

.field public final h:Lcom/google/android/maps/driveabout/vector/aX;

.field public final i:F

.field public final j:I

.field public final k:I

.field public final l:F

.field public final m:F

.field public final n:F

.field public final o:Z

.field public final p:Z

.field public final q:Z

.field public final r:Z


# direct methods
.method static constructor <clinit>()V
    .registers 19

    .prologue
    .line 105
    new-instance v0, LG/a;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aV;->d:Lcom/google/android/maps/driveabout/vector/aX;

    const/16 v2, 0x12

    const/16 v3, 0xc

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aV;->c:Lcom/google/android/maps/driveabout/vector/aX;

    const v5, 0x3fcccccd

    const/16 v6, 0xe

    const/16 v7, 0x20

    sget-object v8, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    const v9, 0x3f99999a

    const/16 v10, 0x8

    const/16 v11, 0x10

    const v12, 0x3feccccd

    const/high16 v13, 0x3fc0

    const/high16 v14, 0x3f80

    const/4 v15, 0x1

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-direct/range {v0 .. v18}, LG/a;-><init>(Lcom/google/android/maps/driveabout/vector/aX;IILcom/google/android/maps/driveabout/vector/aX;FIILcom/google/android/maps/driveabout/vector/aX;FIIFFFZZZZ)V

    sput-object v0, LG/a;->s:LG/a;

    .line 127
    new-instance v0, LG/a;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    const/16 v2, 0xb

    const/16 v3, 0xb

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    const/high16 v5, 0x3f80

    const/16 v6, 0x8

    const/16 v7, 0x18

    sget-object v8, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    const v9, 0x3f99999a

    const/16 v10, 0x8

    const/16 v11, 0x10

    const v12, 0x3f99999a

    const/high16 v13, 0x3f80

    const/high16 v14, 0x3f80

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x1

    const/16 v18, 0x1

    invoke-direct/range {v0 .. v18}, LG/a;-><init>(Lcom/google/android/maps/driveabout/vector/aX;IILcom/google/android/maps/driveabout/vector/aX;FIILcom/google/android/maps/driveabout/vector/aX;FIIFFFZZZZ)V

    sput-object v0, LG/a;->t:LG/a;

    .line 151
    new-instance v0, LG/a;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    const/16 v2, 0xd

    const/16 v3, 0xd

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    const v5, 0x3f99999a

    const/16 v6, 0xa

    const/16 v7, 0x1c

    sget-object v8, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    const v9, 0x3f99999a

    const/16 v10, 0xa

    const/16 v11, 0x14

    const v12, 0x3f99999a

    const/high16 v13, 0x3fa0

    const/high16 v14, 0x3fc0

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x1

    const/16 v18, 0x1

    invoke-direct/range {v0 .. v18}, LG/a;-><init>(Lcom/google/android/maps/driveabout/vector/aX;IILcom/google/android/maps/driveabout/vector/aX;FIILcom/google/android/maps/driveabout/vector/aX;FIIFFFZZZZ)V

    sput-object v0, LG/a;->u:LG/a;

    .line 178
    new-instance v0, LG/a;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    const/16 v2, 0xd

    const/16 v3, 0xd

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    const v5, 0x3f99999a

    const/16 v6, 0xa

    const/16 v7, 0x1c

    sget-object v8, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    const v9, 0x3f99999a

    const/16 v10, 0xa

    const/16 v11, 0x14

    const v12, 0x3f99999a

    const/high16 v13, 0x3f80

    const v14, 0x3fa66666

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x1

    const/16 v18, 0x1

    invoke-direct/range {v0 .. v18}, LG/a;-><init>(Lcom/google/android/maps/driveabout/vector/aX;IILcom/google/android/maps/driveabout/vector/aX;FIILcom/google/android/maps/driveabout/vector/aX;FIIFFFZZZZ)V

    sput-object v0, LG/a;->v:LG/a;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/maps/driveabout/vector/aX;IILcom/google/android/maps/driveabout/vector/aX;FIILcom/google/android/maps/driveabout/vector/aX;FIIFFFZZZZ)V
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249
    iput-object p1, p0, LG/a;->a:Lcom/google/android/maps/driveabout/vector/aX;

    .line 250
    iput p2, p0, LG/a;->b:I

    .line 251
    iput p3, p0, LG/a;->c:I

    .line 252
    iput-object p4, p0, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    .line 253
    iput p5, p0, LG/a;->e:F

    .line 254
    iput p6, p0, LG/a;->f:I

    .line 255
    iput p7, p0, LG/a;->g:I

    .line 256
    iput-object p8, p0, LG/a;->h:Lcom/google/android/maps/driveabout/vector/aX;

    .line 257
    iput p9, p0, LG/a;->i:F

    .line 258
    iput p10, p0, LG/a;->j:I

    .line 259
    iput p11, p0, LG/a;->k:I

    .line 260
    iput p12, p0, LG/a;->l:F

    .line 261
    iput p13, p0, LG/a;->m:F

    .line 262
    iput p14, p0, LG/a;->n:F

    .line 263
    move/from16 v0, p15

    iput-boolean v0, p0, LG/a;->o:Z

    .line 264
    move/from16 v0, p16

    iput-boolean v0, p0, LG/a;->p:Z

    .line 265
    move/from16 v0, p17

    iput-boolean v0, p0, LG/a;->q:Z

    .line 266
    move/from16 v0, p18

    iput-boolean v0, p0, LG/a;->r:Z

    .line 267
    return-void
.end method

.method public static a(LG/a;IFFLcom/google/android/maps/driveabout/vector/aX;)LG/a;
    .registers 25
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 206
    new-instance v1, LG/a;

    move-object/from16 v0, p0

    iget v2, v0, LG/a;->b:I

    add-int v3, v2, p1

    move-object/from16 v0, p0

    iget v2, v0, LG/a;->c:I

    add-int v4, v2, p1

    move-object/from16 v0, p0

    iget-object v5, v0, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v0, p0

    iget v2, v0, LG/a;->e:F

    add-float v6, v2, p2

    move-object/from16 v0, p0

    iget v2, v0, LG/a;->f:I

    add-int v7, v2, p1

    move-object/from16 v0, p0

    iget v2, v0, LG/a;->g:I

    add-int v8, v2, p1

    move-object/from16 v0, p0

    iget-object v9, v0, LG/a;->h:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v0, p0

    iget v2, v0, LG/a;->i:F

    add-float v10, v2, p3

    move-object/from16 v0, p0

    iget v2, v0, LG/a;->j:I

    add-int v11, v2, p1

    move-object/from16 v0, p0

    iget v2, v0, LG/a;->k:I

    add-int v12, v2, p1

    move-object/from16 v0, p0

    iget v13, v0, LG/a;->l:F

    move-object/from16 v0, p0

    iget v14, v0, LG/a;->m:F

    move-object/from16 v0, p0

    iget v15, v0, LG/a;->n:F

    move-object/from16 v0, p0

    iget-boolean v0, v0, LG/a;->o:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LG/a;->p:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LG/a;->q:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LG/a;->r:Z

    move/from16 v19, v0

    move-object/from16 v2, p4

    invoke-direct/range {v1 .. v19}, LG/a;-><init>(Lcom/google/android/maps/driveabout/vector/aX;IILcom/google/android/maps/driveabout/vector/aX;FIILcom/google/android/maps/driveabout/vector/aX;FIIFFFZZZZ)V

    return-object v1
.end method
