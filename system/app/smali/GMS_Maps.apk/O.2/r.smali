.class LO/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field b:J

.field private c:Landroid/content/Context;

.field private final d:LO/J;

.field private e:LO/I;

.field private final f:Ljava/util/List;

.field private g:LO/g;

.field private h:LO/g;

.field private i:LO/g;

.field private volatile j:Z

.field private volatile k:Z

.field private volatile l:Z

.field private final m:Ljava/lang/Thread;

.field private n:LP/u;

.field private o:Z

.field private final p:Ljava/io/File;


# direct methods
.method constructor <init>(LO/J;LP/u;Ljava/lang/Thread;Ljava/io/File;Landroid/content/Context;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-boolean v0, p0, LO/r;->k:Z

    .line 93
    iput-boolean v0, p0, LO/r;->l:Z

    .line 138
    iput-object p1, p0, LO/r;->d:LO/J;

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LO/r;->f:Ljava/util/List;

    .line 140
    new-instance v0, LO/I;

    invoke-direct {v0, p0}, LO/I;-><init>(LO/r;)V

    iput-object v0, p0, LO/r;->e:LO/I;

    .line 141
    iput-object p3, p0, LO/r;->m:Ljava/lang/Thread;

    .line 142
    iput-object p2, p0, LO/r;->n:LP/u;

    .line 143
    iput-object p4, p0, LO/r;->p:Ljava/io/File;

    .line 144
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LO/r;->b:J

    .line 145
    iput-object p5, p0, LO/r;->c:Landroid/content/Context;

    .line 146
    return-void
.end method

.method private a(I)V
    .registers 3
    .parameter

    .prologue
    .line 759
    iget-object v0, p0, LO/r;->g:LO/g;

    if-eqz v0, :cond_f

    iget-object v0, p0, LO/r;->g:LO/g;

    invoke-virtual {v0}, LO/g;->q()I

    move-result v0

    if-ne v0, p1, :cond_f

    .line 760
    const/4 v0, 0x0

    iput-object v0, p0, LO/r;->g:LO/g;

    .line 762
    :cond_f
    iget-boolean v0, p0, LO/r;->a:Z

    if-eqz v0, :cond_16

    .line 763
    invoke-direct {p0}, LO/r;->p()V

    .line 765
    :cond_16
    return-void
.end method

.method private a(LO/g;I)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 393
    invoke-virtual {p1}, LO/g;->q()I

    move-result v0

    if-ne v0, v1, :cond_28

    move v0, v1

    .line 395
    :goto_a
    invoke-virtual {p1}, LO/g;->k()Z

    move-result v3

    if-eqz v3, :cond_77

    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v3

    if-eqz v3, :cond_77

    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v3

    array-length v3, v3

    if-lez v3, :cond_77

    .line 397
    iget-object v3, p0, LO/r;->e:LO/I;

    invoke-virtual {v3}, LO/I;->b()Z

    move-result v3

    if-nez v3, :cond_2a

    if-eqz v0, :cond_2a

    .line 442
    :cond_27
    :goto_27
    return-void

    :cond_28
    move v0, v2

    .line 393
    goto :goto_a

    .line 407
    :cond_2a
    if-eqz v0, :cond_70

    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v0

    array-length v0, v0

    if-le v0, v1, :cond_70

    move v0, v1

    .line 409
    :goto_34
    iget-object v3, p0, LO/r;->e:LO/I;

    invoke-virtual {v3}, LO/I;->a()LO/s;

    move-result-object v3

    .line 410
    invoke-virtual {v3}, LO/s;->f()[LO/z;

    move-result-object v4

    .line 411
    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v5

    aget-object v5, v5, v2

    .line 412
    aget-object v6, v4, p2

    .line 413
    invoke-virtual {v6}, LO/z;->w()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_53

    .line 414
    invoke-virtual {v6}, LO/z;->w()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LO/z;->a(Ljava/lang/String;)V

    .line 417
    :cond_53
    invoke-virtual {v3}, LO/s;->m()I

    move-result v3

    .line 419
    if-eqz v0, :cond_72

    .line 421
    new-array v3, v8, [LO/z;

    aput-object v5, v3, v2

    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v4

    aget-object v4, v4, v1

    aput-object v4, v3, v1

    .line 430
    :goto_65
    iget-object v4, p0, LO/r;->e:LO/I;

    invoke-virtual {v4, v3, v2, v1}, LO/I;->a([LO/z;IZ)V

    .line 432
    if-eqz v0, :cond_27

    .line 433
    invoke-virtual {p0}, LO/r;->c()V

    goto :goto_27

    :cond_70
    move v0, v2

    .line 407
    goto :goto_34

    .line 425
    :cond_72
    aput-object v5, v4, p2

    move v2, v3

    move-object v3, v4

    goto :goto_65

    .line 436
    :cond_77
    if-nez v0, :cond_27

    .line 439
    invoke-virtual {p0, v8, p1}, LO/r;->a(ILO/g;)V

    goto :goto_27
.end method

.method private a(LO/z;LO/z;I)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 386
    add-int/lit8 v0, p3, 0x1

    shr-int/lit8 v0, v0, 0x3

    add-int/2addr v0, p3

    .line 387
    invoke-virtual {p1}, LO/z;->p()I

    move-result v1

    invoke-virtual {p2}, LO/z;->p()I

    move-result v2

    sub-int/2addr v1, v2

    if-le v1, v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private b(LO/g;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 281
    iget-object v0, p0, LO/r;->i:LO/g;

    if-ne p1, v0, :cond_20

    .line 282
    iput-object v1, p0, LO/r;->i:LO/g;

    .line 289
    :cond_7
    :goto_7
    invoke-virtual {p1}, LO/g;->k()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 290
    iget-object v0, p0, LO/r;->c:Landroid/content/Context;

    invoke-static {v0}, LO/M;->a(Landroid/content/Context;)LO/M;

    move-result-object v0

    .line 291
    invoke-virtual {p1}, LO/g;->h()Z

    move-result v1

    if-eqz v1, :cond_27

    .line 292
    invoke-virtual {v0, p1}, LO/M;->a(LO/g;)V

    .line 296
    :goto_1c
    invoke-virtual {v0}, LO/M;->d()V

    .line 298
    :cond_1f
    return-void

    .line 283
    :cond_20
    iget-object v0, p0, LO/r;->h:LO/g;

    if-ne p1, v0, :cond_7

    .line 284
    iput-object v1, p0, LO/r;->h:LO/g;

    goto :goto_7

    .line 294
    :cond_27
    invoke-virtual {v0, p1}, LO/M;->b(LO/g;)Z

    goto :goto_1c
.end method

.method private b(Landroid/location/Location;)V
    .registers 5
    .parameter

    .prologue
    .line 638
    iget-object v0, p0, LO/r;->g:LO/g;

    if-eqz v0, :cond_5

    .line 656
    :cond_4
    :goto_4
    return-void

    .line 641
    :cond_5
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v0

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v0

    .line 642
    invoke-virtual {v0}, LO/z;->C()LO/g;

    move-result-object v1

    if-nez v1, :cond_4

    .line 645
    iget-object v1, p0, LO/r;->e:LO/I;

    invoke-virtual {v1}, LO/I;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 649
    const/4 v1, 0x0

    .line 654
    iget-object v2, p0, LO/r;->d:LO/J;

    invoke-virtual {v2, v0, v1}, LO/J;->a(LO/z;Landroid/location/Location;)LO/g;

    move-result-object v1

    invoke-virtual {v0, v1}, LO/z;->a(LO/g;)V

    goto :goto_4
.end method

.method private c(LO/g;)V
    .registers 10
    .parameter

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 301
    const/4 v0, 0x0

    iput-object v0, p0, LO/r;->g:LO/g;

    .line 303
    invoke-virtual {p1}, LO/g;->k()Z

    move-result v0

    if-eqz v0, :cond_a1

    .line 305
    iget-object v0, p0, LO/r;->n:LP/u;

    if-eqz v0, :cond_17

    iget-boolean v0, p0, LO/r;->a:Z

    if-eqz v0, :cond_17

    .line 306
    invoke-direct {p0}, LO/r;->p()V

    .line 309
    :cond_17
    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v1

    .line 310
    array-length v4, v1

    move v0, v3

    :goto_1d
    if-ge v0, v4, :cond_24

    aget-object v5, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1d

    .line 318
    :cond_24
    array-length v0, v1

    if-lt v0, v6, :cond_ab

    aget-object v0, v1, v2

    invoke-virtual {v0}, LO/z;->v()[LO/W;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v6, :cond_ab

    aget-object v0, v1, v3

    aget-object v4, v1, v2

    invoke-virtual {p1}, LO/g;->j()I

    move-result v5

    invoke-direct {p0, v0, v4, v5}, LO/r;->a(LO/z;LO/z;I)Z

    move-result v0

    if-eqz v0, :cond_ab

    .line 321
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    new-array v0, v0, [LO/z;

    .line 323
    array-length v4, v0

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 329
    :goto_47
    array-length v1, v0

    if-lt v1, v6, :cond_78

    .line 331
    aget-object v1, v0, v3

    invoke-virtual {v1}, LO/z;->p()I

    move-result v1

    move v7, v2

    move v2, v1

    move v1, v7

    .line 332
    :goto_53
    array-length v4, v0

    if-ge v1, v4, :cond_67

    .line 333
    aget-object v4, v0, v1

    invoke-virtual {v4}, LO/z;->p()I

    move-result v4

    if-ge v4, v2, :cond_64

    .line 334
    aget-object v2, v0, v1

    invoke-virtual {v2}, LO/z;->p()I

    move-result v2

    .line 332
    :cond_64
    add-int/lit8 v1, v1, 0x1

    goto :goto_53

    .line 338
    :cond_67
    array-length v4, v0

    move v1, v3

    :goto_69
    if-ge v1, v4, :cond_78

    aget-object v5, v0, v1

    .line 339
    invoke-virtual {v5}, LO/z;->p()I

    move-result v6

    sub-int/2addr v6, v2

    invoke-virtual {v5, v6}, LO/z;->d(I)V

    .line 338
    add-int/lit8 v1, v1, 0x1

    goto :goto_69

    .line 343
    :cond_78
    aget-object v1, v0, v3

    invoke-virtual {v1}, LO/z;->y()Z

    move-result v1

    if-eqz v1, :cond_95

    aget-object v1, v0, v3

    invoke-virtual {v1}, LO/z;->C()LO/g;

    move-result-object v1

    if-nez v1, :cond_95

    .line 347
    aget-object v1, v0, v3

    iget-object v2, p0, LO/r;->d:LO/J;

    aget-object v4, v0, v3

    invoke-virtual {v2, v4}, LO/J;->a(LO/z;)LO/g;

    move-result-object v2

    invoke-virtual {v1, v2}, LO/z;->a(LO/g;)V

    .line 360
    :cond_95
    iget-object v1, p0, LO/r;->e:LO/I;

    aget-object v2, v0, v3

    invoke-virtual {v2}, LO/z;->h()Z

    move-result v2

    invoke-virtual {v1, v0, v3, v2}, LO/I;->a([LO/z;IZ)V

    .line 364
    :cond_a0
    :goto_a0
    return-void

    .line 361
    :cond_a1
    invoke-virtual {p1}, LO/g;->h()Z

    move-result v0

    if-nez v0, :cond_a0

    .line 362
    invoke-virtual {p0, v6, p1}, LO/r;->a(ILO/g;)V

    goto :goto_a0

    :cond_ab
    move-object v0, v1

    goto :goto_47
.end method

.method private final o()V
    .registers 3

    .prologue
    .line 721
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, LO/r;->m:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_16

    .line 722
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Method must be called on NavigationThread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 724
    :cond_16
    return-void
.end method

.method private p()V
    .registers 3

    .prologue
    .line 771
    const/4 v0, 0x0

    iput-boolean v0, p0, LO/r;->a:Z

    .line 772
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LO/r;->b:J

    .line 773
    iget-object v0, p0, LO/r;->n:LP/u;

    invoke-virtual {v0}, LP/u;->b()V

    .line 774
    return-void
.end method


# virtual methods
.method declared-synchronized a()V
    .registers 4

    .prologue
    .line 502
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    .line 503
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    .line 504
    invoke-interface {v0, v1}, LO/q;->d(LO/s;)V
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_1d

    goto :goto_d

    .line 502
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 506
    :cond_20
    monitor-exit p0

    return-void
.end method

.method declared-synchronized a(ILO/g;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 572
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    .line 573
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    .line 574
    invoke-interface {v0, p1, p2, v1}, LO/q;->a(ILO/g;LO/s;)V
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_1d

    goto :goto_d

    .line 572
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 576
    :cond_20
    if-eqz p2, :cond_28

    .line 577
    :try_start_22
    invoke-virtual {p2}, LO/g;->p()Z
    :try_end_25
    .catchall {:try_start_22 .. :try_end_25} :catchall_1d

    move-result v0

    if-eqz v0, :cond_2a

    .line 589
    :cond_28
    :goto_28
    monitor-exit p0

    return-void

    .line 579
    :cond_2a
    :try_start_2a
    invoke-virtual {p2}, LO/g;->o()Z

    move-result v0

    if-nez v0, :cond_28

    .line 581
    invoke-virtual {p2}, LO/g;->m()Z

    move-result v0

    if-nez v0, :cond_28

    .line 583
    invoke-virtual {p2}, LO/g;->l()Z
    :try_end_39
    .catchall {:try_start_2a .. :try_end_39} :catchall_1d

    move-result v0

    if-eqz v0, :cond_28

    goto :goto_28
.end method

.method public a(LO/a;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 372
    invoke-virtual {p1}, LO/a;->a()LO/g;

    move-result-object v0

    invoke-virtual {v0}, LO/g;->d()[LO/z;

    move-result-object v0

    .line 374
    iget-object v1, p0, LO/r;->e:LO/I;

    invoke-virtual {v1, v0, v2, v2}, LO/I;->a([LO/z;IZ)V

    .line 375
    return-void
.end method

.method public a(LO/g;)V
    .registers 5
    .parameter

    .prologue
    .line 243
    invoke-direct {p0}, LO/r;->o()V

    .line 252
    const/4 v0, 0x1

    iput-boolean v0, p0, LO/r;->o:Z

    .line 254
    invoke-virtual {p1}, LO/g;->h()Z

    move-result v0

    if-nez v0, :cond_12

    invoke-virtual {p1}, LO/g;->r_()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 255
    :cond_12
    invoke-direct {p0, p1}, LO/r;->b(LO/g;)V

    .line 278
    :cond_15
    :goto_15
    return-void

    .line 256
    :cond_16
    iget-object v0, p0, LO/r;->g:LO/g;

    if-ne p1, v0, :cond_20

    .line 257
    iget-object v0, p0, LO/r;->g:LO/g;

    invoke-direct {p0, v0}, LO/r;->c(LO/g;)V

    goto :goto_15

    .line 259
    :cond_20
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v0

    invoke-virtual {v0}, LO/s;->f()[LO/z;

    move-result-object v1

    .line 260
    if-eqz v1, :cond_15

    .line 261
    const/4 v0, 0x0

    :goto_2d
    array-length v2, v1

    if-ge v0, v2, :cond_15

    .line 262
    aget-object v2, v1, v0

    invoke-virtual {v2}, LO/z;->C()LO/g;

    move-result-object v2

    if-ne v2, p1, :cond_42

    .line 268
    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LO/z;->a(LO/g;)V

    .line 269
    invoke-direct {p0, p1, v0}, LO/r;->a(LO/g;I)V

    goto :goto_15

    .line 261
    :cond_42
    add-int/lit8 v0, v0, 0x1

    goto :goto_2d
.end method

.method declared-synchronized a(LO/j;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 523
    monitor-enter p0

    :try_start_1
    new-instance v0, LL/u;

    invoke-direct {v0, p1}, LL/u;-><init>(LO/j;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 524
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    .line 525
    invoke-interface {v0, p1, p2}, LO/q;->a(LO/j;I)V
    :try_end_1e
    .catchall {:try_start_1 .. :try_end_1e} :catchall_1f

    goto :goto_f

    .line 523
    :catchall_1f
    move-exception v0

    monitor-exit p0

    throw v0

    .line 527
    :cond_22
    monitor-exit p0

    return-void
.end method

.method declared-synchronized a(LO/q;)V
    .registers 3
    .parameter

    .prologue
    .line 489
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 490
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    .line 492
    :cond_e
    monitor-exit p0

    return-void

    .line 489
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LO/z;I)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const-wide/16 v3, -0x1

    const/4 v2, 0x1

    .line 592
    invoke-direct {p0}, LO/r;->o()V

    .line 596
    iget-boolean v0, p0, LO/r;->a:Z

    if-eqz v0, :cond_19

    .line 597
    const/4 v0, 0x0

    iput-boolean v0, p0, LO/r;->a:Z

    .line 598
    if-nez p2, :cond_1a

    .line 599
    iput-wide v3, p0, LO/r;->b:J

    .line 600
    invoke-virtual {p0, p1, v2}, LO/r;->a(LO/z;Z)V

    .line 601
    const-string v0, "j"

    invoke-static {v0, p2}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;I)V

    .line 615
    :cond_19
    :goto_19
    return-void

    .line 604
    :cond_1a
    iget-wide v0, p0, LO/r;->b:J

    cmp-long v0, v0, v3

    if-nez v0, :cond_29

    .line 606
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, LO/r;->a(ILO/g;)V

    .line 608
    const-string v0, "j"

    invoke-static {v0, p2}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;I)V

    .line 610
    :cond_29
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LO/r;->b:J

    goto :goto_19
.end method

.method public a(LO/z;Z)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 667
    invoke-direct {p0}, LO/r;->o()V

    .line 669
    if-eqz p2, :cond_11

    .line 670
    new-array v1, v4, [LO/z;

    aput-object p1, v1, v0

    .line 671
    iget-object v2, p0, LO/r;->e:LO/I;

    invoke-virtual {v2, v1, v0, v0}, LO/I;->a([LO/z;IZ)V

    .line 694
    :cond_10
    :goto_10
    return-void

    .line 673
    :cond_11
    const/4 v1, -0x1

    .line 674
    iget-object v2, p0, LO/r;->e:LO/I;

    invoke-virtual {v2}, LO/I;->a()LO/s;

    move-result-object v2

    invoke-virtual {v2}, LO/s;->f()[LO/z;

    move-result-object v2

    .line 675
    if-eqz v2, :cond_10

    .line 680
    :goto_1e
    array-length v3, v2

    if-ge v0, v3, :cond_45

    .line 681
    aget-object v3, v2, v0

    if-ne v3, p1, :cond_42

    .line 686
    :goto_25
    if-ltz v0, :cond_10

    .line 687
    invoke-virtual {p1}, LO/z;->y()Z

    move-result v1

    if-eqz v1, :cond_3c

    invoke-virtual {p1}, LO/z;->C()LO/g;

    move-result-object v1

    if-nez v1, :cond_3c

    .line 688
    iget-object v1, p0, LO/r;->d:LO/J;

    invoke-virtual {v1, p1}, LO/J;->a(LO/z;)LO/g;

    move-result-object v1

    invoke-virtual {p1, v1}, LO/z;->a(LO/g;)V

    .line 691
    :cond_3c
    iget-object v1, p0, LO/r;->e:LO/I;

    invoke-virtual {v1, v2, v0, v4}, LO/I;->a([LO/z;IZ)V

    goto :goto_10

    .line 680
    :cond_42
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    :cond_45
    move v0, v1

    goto :goto_25
.end method

.method public a(LaH/h;)V
    .registers 8
    .parameter

    .prologue
    .line 196
    invoke-direct {p0}, LO/r;->o()V

    .line 198
    iget-boolean v0, p0, LO/r;->l:Z

    if-nez v0, :cond_8

    .line 240
    :cond_7
    :goto_7
    return-void

    .line 202
    :cond_8
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_7

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 218
    invoke-virtual {v0}, LO/s;->i()LO/z;

    move-result-object v1

    .line 219
    iget-object v2, p0, LO/r;->n:LP/u;

    if-eqz v2, :cond_32

    if-eqz v1, :cond_32

    invoke-virtual {v1}, LO/z;->d()I

    move-result v2

    if-nez v2, :cond_32

    iget-boolean v2, p0, LO/r;->a:Z

    if-nez v2, :cond_32

    iget-boolean v2, p0, LO/r;->o:Z

    if-nez v2, :cond_32

    iget-object v2, p0, LO/r;->g:LO/g;

    if-nez v2, :cond_47

    .line 233
    :cond_32
    iget-object v1, p0, LO/r;->g:LO/g;

    if-nez v1, :cond_7

    .line 236
    iget-object v1, p0, LO/r;->d:LO/J;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v1, p1, v0, v2}, LO/J;->a(LaH/h;LO/z;I)LO/g;

    move-result-object v0

    iput-object v0, p0, LO/r;->g:LO/g;

    .line 238
    invoke-virtual {p0}, LO/r;->g()V

    goto :goto_7

    .line 224
    :cond_47
    iget-wide v2, p0, LO/r;->b:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_5c

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, LO/r;->b:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2710

    cmp-long v0, v2, v4

    if-lez v0, :cond_7

    .line 229
    :cond_5c
    const/4 v0, 0x1

    iput-boolean v0, p0, LO/r;->a:Z

    .line 230
    iget-object v0, p0, LO/r;->n:LP/u;

    invoke-virtual {v0, p1, v1}, LP/u;->a(LaH/h;LO/z;)V

    .line 231
    invoke-virtual {p0}, LO/r;->g()V

    goto :goto_7
.end method

.method public a(LaH/h;LO/z;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 186
    invoke-direct {p0}, LO/r;->o()V

    .line 187
    iget-object v0, p0, LO/r;->d:LO/J;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, p2, v1}, LO/J;->a(LaH/h;LO/z;I)LO/g;

    move-result-object v0

    iput-object v0, p0, LO/r;->g:LO/g;

    .line 189
    return-void
.end method

.method public a(LaH/h;[LO/U;II[LO/b;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 160
    invoke-direct {p0}, LO/r;->o()V

    .line 161
    iget-object v0, p0, LO/r;->d:LO/J;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LO/J;->a(LaH/h;[LO/U;II[LO/b;)LO/g;

    move-result-object v0

    iput-object v0, p0, LO/r;->g:LO/g;

    .line 163
    return-void
.end method

.method public a(Landroid/location/Location;)V
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x0

    .line 449
    invoke-direct {p0}, LO/r;->o()V

    .line 451
    iget-boolean v0, p0, LO/r;->j:Z

    if-eqz v0, :cond_20

    .line 452
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LO/r;->p:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/navgation_location"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 453
    invoke-static {v0}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;)V

    .line 456
    :cond_20
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0, p1}, LO/I;->a(Landroid/location/Location;)V

    .line 458
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 464
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v2

    .line 465
    iget-object v3, p0, LO/r;->e:LO/I;

    invoke-virtual {v3}, LO/I;->a()LO/s;

    move-result-object v3

    invoke-virtual {v3}, LO/s;->g()LO/z;

    move-result-object v3

    .line 467
    if-eqz v3, :cond_74

    invoke-virtual {v3}, LO/z;->B()Z

    move-result v4

    if-eqz v4, :cond_74

    iget-boolean v4, p0, LO/r;->l:Z

    if-eqz v4, :cond_74

    invoke-virtual {v3}, LO/z;->z()J

    move-result-wide v4

    invoke-virtual {v2}, LR/m;->p()I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    int-to-long v6, v6

    add-long/2addr v4, v6

    cmp-long v4, v0, v4

    if-lez v4, :cond_74

    .line 471
    invoke-virtual {v3}, LO/z;->z()J

    move-result-wide v4

    invoke-virtual {v2}, LR/m;->q()I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v6, v2

    add-long/2addr v4, v6

    cmp-long v0, v0, v4

    if-lez v0, :cond_71

    invoke-virtual {v3}, LO/z;->t()Z

    move-result v0

    if-eqz v0, :cond_71

    .line 475
    invoke-virtual {v3}, LO/z;->A()V

    .line 476
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0, v8}, LO/I;->a(I)Z

    .line 479
    :cond_71
    invoke-direct {p0, p1}, LO/r;->b(Landroid/location/Location;)V

    .line 481
    :cond_74
    iget-boolean v0, p0, LO/r;->j:Z

    if-eqz v0, :cond_7d

    .line 482
    invoke-static {}, Landroid/os/Debug;->stopMethodTracing()V

    .line 483
    iput-boolean v8, p0, LO/r;->j:Z

    .line 485
    :cond_7d
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 445
    iput-boolean p1, p0, LO/r;->k:Z

    .line 446
    return-void
.end method

.method public a([LO/U;I[LO/b;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 174
    invoke-direct {p0}, LO/r;->o()V

    .line 175
    iget-object v0, p0, LO/r;->d:LO/J;

    invoke-virtual {v0, p1, p2, p3}, LO/J;->a([LO/U;I[LO/b;)LO/g;

    move-result-object v0

    iput-object v0, p0, LO/r;->i:LO/g;

    .line 177
    return-void
.end method

.method declared-synchronized b()V
    .registers 4

    .prologue
    .line 509
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    .line 510
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    .line 511
    invoke-interface {v0, v1}, LO/q;->f(LO/s;)V
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_1d

    goto :goto_d

    .line 509
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 513
    :cond_20
    monitor-exit p0

    return-void
.end method

.method declared-synchronized b(LO/j;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 531
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    .line 532
    invoke-interface {v0, p1, p2}, LO/q;->b(LO/j;I)V
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_17

    goto :goto_7

    .line 531
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0

    .line 534
    :cond_1a
    monitor-exit p0

    return-void
.end method

.method declared-synchronized b(LO/q;)V
    .registers 3
    .parameter

    .prologue
    .line 496
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 497
    monitor-exit p0

    return-void

    .line 496
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 697
    iput-boolean p1, p0, LO/r;->l:Z

    .line 698
    return-void
.end method

.method declared-synchronized c()V
    .registers 4

    .prologue
    .line 516
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    .line 517
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    .line 518
    invoke-interface {v0, v1}, LO/q;->g(LO/s;)V
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_1d

    goto :goto_d

    .line 516
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 520
    :cond_20
    monitor-exit p0

    return-void
.end method

.method declared-synchronized d()V
    .registers 4

    .prologue
    .line 541
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    .line 542
    invoke-virtual {v1}, LO/s;->h()LO/N;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 543
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    .line 544
    invoke-interface {v0, v1}, LO/q;->b(LO/s;)V
    :try_end_22
    .catchall {:try_start_1 .. :try_end_22} :catchall_23

    goto :goto_13

    .line 541
    :catchall_23
    move-exception v0

    monitor-exit p0

    throw v0

    .line 547
    :cond_26
    monitor-exit p0

    return-void
.end method

.method declared-synchronized e()V
    .registers 4

    .prologue
    .line 551
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    .line 552
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    .line 553
    invoke-interface {v0, v1}, LO/q;->c(LO/s;)V
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_1d

    goto :goto_d

    .line 551
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 555
    :cond_20
    monitor-exit p0

    return-void
.end method

.method declared-synchronized f()V
    .registers 4

    .prologue
    .line 558
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    .line 559
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    .line 560
    invoke-interface {v0, v1}, LO/q;->e(LO/s;)V
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_1d

    goto :goto_d

    .line 558
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 562
    :cond_20
    monitor-exit p0

    return-void
.end method

.method declared-synchronized g()V
    .registers 4

    .prologue
    .line 565
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    .line 566
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    .line 567
    invoke-interface {v0, v1}, LO/q;->a(LO/s;)V
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_1d

    goto :goto_d

    .line 565
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 569
    :cond_20
    monitor-exit p0

    return-void
.end method

.method public h()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 618
    invoke-direct {p0}, LO/r;->o()V

    .line 620
    iput-boolean v1, p0, LO/r;->o:Z

    .line 625
    iget-boolean v0, p0, LO/r;->a:Z

    if-nez v0, :cond_e

    .line 628
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, LO/r;->a(ILO/g;)V

    .line 630
    :cond_e
    return-void
.end method

.method i()Z
    .registers 2

    .prologue
    .line 701
    iget-boolean v0, p0, LO/r;->l:Z

    return v0
.end method

.method public j()V
    .registers 6

    .prologue
    .line 708
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v0

    invoke-virtual {v0}, LO/s;->f()[LO/z;

    move-result-object v1

    .line 709
    array-length v2, v1

    const/4 v0, 0x0

    :goto_c
    if-ge v0, v2, :cond_28

    aget-object v3, v1, v0

    .line 710
    invoke-virtual {v3}, LO/z;->y()Z

    move-result v4

    if-eqz v4, :cond_25

    invoke-virtual {v3}, LO/z;->C()LO/g;

    move-result-object v4

    if-nez v4, :cond_25

    .line 711
    iget-object v4, p0, LO/r;->d:LO/J;

    invoke-virtual {v4, v3}, LO/J;->a(LO/z;)LO/g;

    move-result-object v4

    invoke-virtual {v3, v4}, LO/z;->a(LO/g;)V

    .line 709
    :cond_25
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 715
    :cond_28
    return-void
.end method

.method public k()V
    .registers 2

    .prologue
    .line 740
    const/4 v0, 0x4

    invoke-direct {p0, v0}, LO/r;->a(I)V

    .line 741
    return-void
.end method

.method public l()V
    .registers 2

    .prologue
    .line 748
    const/4 v0, 0x5

    invoke-direct {p0, v0}, LO/r;->a(I)V

    .line 749
    return-void
.end method

.method public m()V
    .registers 2

    .prologue
    .line 780
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->d()V

    .line 781
    return-void
.end method

.method public n()V
    .registers 2

    .prologue
    .line 787
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 788
    iget-object v0, p0, LO/r;->d:LO/J;

    invoke-virtual {v0}, LO/J;->a()V

    .line 789
    return-void
.end method
