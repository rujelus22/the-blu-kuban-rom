.class public LO/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private c:[LO/U;

.field private d:LO/g;


# direct methods
.method private constructor <init>(LO/g;)V
    .registers 4
    .parameter

    .prologue
    .line 32
    invoke-virtual {p1}, LO/g;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1}, LO/g;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LO/a;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 33
    iput-object p1, p0, LO/a;->d:LO/g;

    .line 34
    return-void
.end method

.method private constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 22
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LO/a;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 24
    return-void
.end method

.method private constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 28
    iput-object p2, p0, LO/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 29
    return-void
.end method

.method public static a(LO/g;)LO/a;
    .registers 2
    .parameter

    .prologue
    .line 54
    new-instance v0, LO/a;

    invoke-direct {v0, p0}, LO/a;-><init>(LO/g;)V

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/a;
    .registers 2
    .parameter

    .prologue
    .line 44
    new-instance v0, LO/a;

    invoke-direct {v0, p0}, LO/a;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v0
.end method


# virtual methods
.method public a()LO/g;
    .registers 3

    .prologue
    .line 61
    iget-object v0, p0, LO/a;->d:LO/g;

    if-nez v0, :cond_22

    .line 62
    iget-object v0, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_a

    .line 63
    const/4 v0, 0x0

    .line 70
    :goto_9
    return-object v0

    .line 65
    :cond_a
    new-instance v0, LO/i;

    iget-object v1, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v1}, LO/i;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, LO/i;->a()LO/g;

    move-result-object v0

    iput-object v0, p0, LO/a;->d:LO/g;

    .line 66
    iget-object v0, p0, LO/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_22

    .line 67
    iget-object v0, p0, LO/a;->d:LO/g;

    iget-object v1, p0, LO/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, LO/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 70
    :cond_22
    iget-object v0, p0, LO/a;->d:LO/g;

    goto :goto_9
.end method

.method public a([LO/U;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 152
    invoke-virtual {p0}, LO/a;->d()[LO/U;

    .line 153
    aget-object v1, p1, v0

    iget-object v2, p0, LO/a;->c:[LO/U;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, LO/U;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    aget-object v1, p1, v1

    iget-object v2, p0, LO/a;->c:[LO/U;

    iget-object v3, p0, LO/a;->c:[LO/U;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, LO/U;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    const/4 v0, 0x1

    :cond_25
    return v0
.end method

.method public b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 84
    iget-object v0, p0, LO/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public d()[LO/U;
    .registers 8

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x0

    .line 104
    iget-object v1, p0, LO/a;->c:[LO/U;

    if-nez v1, :cond_2c

    .line 105
    iget-object v1, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v1, :cond_d

    .line 106
    new-array v0, v0, [LO/U;

    .line 116
    :goto_c
    return-object v0

    .line 108
    :cond_d
    iget-object v1, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 109
    new-array v2, v1, [LO/U;

    iput-object v2, p0, LO/a;->c:[LO/U;

    .line 110
    :goto_17
    if-ge v0, v1, :cond_2c

    .line 111
    iget-object v2, p0, LO/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 113
    iget-object v3, p0, LO/a;->c:[LO/U;

    new-instance v4, LO/U;

    const/4 v5, 0x0

    invoke-direct {v4, v2, v5}, LO/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v4, v3, v0

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_17

    .line 116
    :cond_2c
    iget-object v0, p0, LO/a;->c:[LO/U;

    goto :goto_c
.end method
