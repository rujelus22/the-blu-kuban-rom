.class public LO/t;
.super LR/c;
.source "SourceFile"

# interfaces
.implements LM/b;
.implements LP/t;


# static fields
.field private static a:LO/t;


# instance fields
.field private b:Landroid/os/Handler;

.field private c:Landroid/os/Looper;

.field private d:Z

.field private final e:LO/r;

.field private f:Landroid/location/Location;

.field private g:LP/u;

.field private h:Z

.field private i:LO/y;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput-object v0, LO/t;->a:LO/t;

    return-void
.end method

.method protected constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 131
    invoke-direct {p0}, LR/c;-><init>()V

    .line 85
    new-instance v0, LO/y;

    invoke-direct {v0, p0, v1}, LO/y;-><init>(LO/t;LO/u;)V

    iput-object v0, p0, LO/t;->i:LO/y;

    .line 132
    iput-object v1, p0, LO/t;->e:LO/r;

    .line 133
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Law/p;Ljava/io/File;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 143
    const-string v0, "NavigationThread"

    invoke-direct {p0, v0}, LR/c;-><init>(Ljava/lang/String;)V

    .line 85
    new-instance v0, LO/y;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LO/y;-><init>(LO/t;LO/u;)V

    iput-object v0, p0, LO/t;->i:LO/y;

    .line 144
    new-instance v1, LO/J;

    invoke-direct {v1, p2, p0}, LO/J;-><init>(Law/p;LO/t;)V

    .line 145
    sget-object v0, LA/c;->i:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    .line 146
    new-instance v2, LP/u;

    invoke-direct {v2, p1, v0}, LP/u;-><init>(Landroid/content/Context;Lr/z;)V

    iput-object v2, p0, LO/t;->g:LP/u;

    .line 147
    iget-object v0, p0, LO/t;->g:LP/u;

    invoke-virtual {v0, p0}, LP/u;->a(LP/t;)V

    .line 148
    new-instance v0, LO/r;

    iget-object v2, p0, LO/t;->g:LP/u;

    move-object v3, p0

    move-object v4, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LO/r;-><init>(LO/J;LP/u;Ljava/lang/Thread;Ljava/io/File;Landroid/content/Context;)V

    iput-object v0, p0, LO/t;->e:LO/r;

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, LO/t;->d:Z

    .line 150
    invoke-virtual {p0}, LO/t;->start()V

    .line 154
    monitor-enter p0

    .line 155
    :goto_37
    :try_start_37
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;
    :try_end_39
    .catchall {:try_start_37 .. :try_end_39} :catchall_43

    if-nez v0, :cond_41

    .line 157
    :try_start_3b
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_3e
    .catchall {:try_start_3b .. :try_end_3e} :catchall_43
    .catch Ljava/lang/InterruptedException; {:try_start_3b .. :try_end_3e} :catch_3f

    goto :goto_37

    .line 158
    :catch_3f
    move-exception v0

    goto :goto_37

    .line 162
    :cond_41
    :try_start_41
    monitor-exit p0

    .line 163
    return-void

    .line 162
    :catchall_43
    move-exception v0

    monitor-exit p0
    :try_end_45
    .catchall {:try_start_41 .. :try_end_45} :catchall_43

    throw v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)LO/t;
    .registers 5
    .parameter

    .prologue
    .line 115
    const-class v1, LO/t;

    monitor-enter v1

    :try_start_3
    sget-object v0, LO/t;->a:LO/t;

    if-nez v0, :cond_16

    .line 116
    new-instance v0, LO/t;

    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v2

    invoke-static {p0}, LJ/a;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    invoke-direct {v0, p0, v2, v3}, LO/t;-><init>(Landroid/content/Context;Law/p;Ljava/io/File;)V

    sput-object v0, LO/t;->a:LO/t;

    .line 119
    :cond_16
    sget-object v0, LO/t;->a:LO/t;

    iget-object v0, v0, LO/t;->i:LO/y;

    invoke-static {v0, p0}, LO/y;->a(LO/y;Landroid/content/Context;)V

    .line 120
    sget-object v0, LO/t;->a:LO/t;
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_21

    monitor-exit v1

    return-object v0

    .line 115
    :catchall_21
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(LO/t;Landroid/location/Location;)Landroid/location/Location;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, LO/t;->f:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic a(LO/t;Landroid/os/Message;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0, p1}, LO/t;->a(Landroid/os/Message;)V

    return-void
.end method

.method private a(Landroid/location/Location;)V
    .registers 5
    .parameter

    .prologue
    .line 446
    invoke-direct {p0}, LO/t;->m()V

    .line 447
    iput-object p1, p0, LO/t;->f:Landroid/location/Location;

    .line 450
    iget-boolean v0, p0, LO/t;->h:Z

    if-nez v0, :cond_1a

    .line 451
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    new-instance v1, LO/v;

    const-string v2, "NavigationThread.IdleHandler"

    invoke-direct {v1, p0, v2}, LO/v;-><init>(LO/t;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    .line 463
    const/4 v0, 0x1

    iput-boolean v0, p0, LO/t;->h:Z

    .line 465
    :cond_1a
    return-void
.end method

.method private a(Landroid/os/Message;)V
    .registers 8
    .parameter

    .prologue
    .line 489
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_b2

    .line 564
    :goto_5
    return-void

    .line 492
    :pswitch_6
    invoke-direct {p0}, LO/t;->j()V

    goto :goto_5

    .line 496
    :pswitch_a
    invoke-direct {p0}, LO/t;->i()V

    goto :goto_5

    .line 500
    :pswitch_e
    invoke-direct {p0}, LO/t;->k()V

    goto :goto_5

    .line 505
    :pswitch_12
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v5, v0

    check-cast v5, LO/w;

    .line 506
    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-static {v5}, LO/w;->a(LO/w;)LaH/h;

    move-result-object v1

    invoke-static {v5}, LO/w;->b(LO/w;)[LO/U;

    move-result-object v2

    invoke-static {v5}, LO/w;->c(LO/w;)I

    move-result v3

    invoke-static {v5}, LO/w;->d(LO/w;)I

    move-result v4

    invoke-static {v5}, LO/w;->e(LO/w;)[LO/b;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LO/r;->a(LaH/h;[LO/U;II[LO/b;)V

    goto :goto_5

    .line 514
    :pswitch_31
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LO/w;

    .line 515
    iget-object v1, p0, LO/t;->e:LO/r;

    invoke-static {v0}, LO/w;->a(LO/w;)LaH/h;

    move-result-object v2

    invoke-static {v0}, LO/w;->f(LO/w;)LO/z;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LO/r;->a(LaH/h;LO/z;)V

    goto :goto_5

    .line 521
    :pswitch_43
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LO/w;

    .line 522
    iget-object v1, p0, LO/t;->e:LO/r;

    invoke-static {v0}, LO/w;->b(LO/w;)[LO/U;

    move-result-object v2

    invoke-static {v0}, LO/w;->c(LO/w;)I

    move-result v3

    invoke-static {v0}, LO/w;->e(LO/w;)[LO/b;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LO/r;->a([LO/U;I[LO/b;)V

    goto :goto_5

    .line 527
    :pswitch_59
    iget-object v1, p0, LO/t;->e:LO/r;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LO/z;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LO/r;->a(LO/z;Z)V

    goto :goto_5

    .line 531
    :pswitch_64
    iget-object v1, p0, LO/t;->e:LO/r;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LO/a;

    invoke-virtual {v1, v0}, LO/r;->a(LO/a;)V

    goto :goto_5

    .line 535
    :pswitch_6e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-direct {p0, v0}, LO/t;->a(Landroid/location/Location;)V

    goto :goto_5

    .line 539
    :pswitch_76
    iget-object v1, p0, LO/t;->e:LO/r;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LO/g;

    invoke-virtual {v1, v0}, LO/r;->a(LO/g;)V

    goto :goto_5

    .line 543
    :pswitch_80
    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0}, LO/r;->h()V

    goto :goto_5

    .line 547
    :pswitch_86
    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0}, LO/r;->j()V

    goto/16 :goto_5

    .line 551
    :pswitch_8d
    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0}, LO/r;->l()V

    goto/16 :goto_5

    .line 556
    :pswitch_94
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LO/x;

    .line 557
    iget-object v1, p0, LO/t;->e:LO/r;

    invoke-static {v0}, LO/x;->a(LO/x;)LO/z;

    move-result-object v2

    invoke-static {v0}, LO/x;->b(LO/x;)I

    move-result v0

    invoke-virtual {v1, v2, v0}, LO/r;->a(LO/z;I)V

    goto/16 :goto_5

    .line 561
    :pswitch_a7
    invoke-direct {p0}, LO/t;->j()V

    .line 562
    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0}, LO/r;->m()V

    goto/16 :goto_5

    .line 489
    nop

    :pswitch_data_b2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_a
        :pswitch_e
        :pswitch_12
        :pswitch_31
        :pswitch_86
        :pswitch_59
        :pswitch_6e
        :pswitch_76
        :pswitch_80
        :pswitch_8d
        :pswitch_94
        :pswitch_a7
        :pswitch_43
        :pswitch_64
    .end packed-switch
.end method

.method static synthetic a(LO/t;)Z
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-boolean v0, p0, LO/t;->d:Z

    return v0
.end method

.method static synthetic a(LO/t;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 46
    iput-boolean p1, p0, LO/t;->h:Z

    return p1
.end method

.method static synthetic b(LO/t;)Landroid/location/Location;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, LO/t;->f:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic c(LO/t;)LO/r;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, LO/t;->e:LO/r;

    return-object v0
.end method

.method private declared-synchronized h()V
    .registers 2

    .prologue
    .line 410
    monitor-enter p0

    :try_start_1
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 411
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, LO/t;->c:Landroid/os/Looper;

    .line 412
    new-instance v0, LO/u;

    invoke-direct {v0, p0}, LO/u;-><init>(LO/t;)V

    iput-object v0, p0, LO/t;->b:Landroid/os/Handler;

    .line 421
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_16

    .line 422
    monitor-exit p0

    return-void

    .line 410
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private i()V
    .registers 2

    .prologue
    .line 428
    invoke-direct {p0}, LO/t;->m()V

    .line 429
    const/4 v0, 0x0

    iput-boolean v0, p0, LO/t;->d:Z

    .line 430
    return-void
.end method

.method private j()V
    .registers 2

    .prologue
    .line 433
    invoke-direct {p0}, LO/t;->m()V

    .line 434
    const/4 v0, 0x1

    iput-boolean v0, p0, LO/t;->d:Z

    .line 435
    return-void
.end method

.method private k()V
    .registers 2

    .prologue
    .line 438
    invoke-direct {p0}, LO/t;->m()V

    .line 439
    iget-object v0, p0, LO/t;->c:Landroid/os/Looper;

    if-eqz v0, :cond_f

    .line 440
    iget-object v0, p0, LO/t;->c:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 441
    const/4 v0, 0x0

    iput-object v0, p0, LO/t;->c:Landroid/os/Looper;

    .line 443
    :cond_f
    return-void
.end method

.method private final m()V
    .registers 3

    .prologue
    .line 571
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq v0, p0, :cond_14

    .line 572
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called on NavigationThread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 574
    :cond_14
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 127
    iget-object v0, p0, LO/t;->i:LO/y;

    invoke-static {v0}, LO/y;->a(LO/y;)V

    .line 128
    return-void
.end method

.method public a(IZ)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 194
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 195
    return-void
.end method

.method public a(LM/C;)V
    .registers 2
    .parameter

    .prologue
    .line 350
    return-void
.end method

.method public a(LO/a;)V
    .registers 5
    .parameter

    .prologue
    .line 183
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/16 v2, 0xe

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 186
    return-void
.end method

.method public a(LO/g;)V
    .registers 5
    .parameter

    .prologue
    .line 189
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 191
    return-void
.end method

.method public a(LO/q;)V
    .registers 3
    .parameter

    .prologue
    .line 367
    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0, p1}, LO/r;->a(LO/q;)V

    .line 368
    return-void
.end method

.method public a(LO/z;)V
    .registers 5
    .parameter

    .prologue
    .line 387
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 388
    return-void
.end method

.method public a(LO/z;I)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 355
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/16 v2, 0xb

    new-instance v3, LO/x;

    const/4 v4, 0x0

    invoke-direct {v3, p1, p2, v4}, LO/x;-><init>(LO/z;ILO/u;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 357
    return-void
.end method

.method public a(LaH/h;LO/z;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 244
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x4

    new-instance v3, LO/w;

    const/4 v4, 0x0

    invoke-direct {v3, p1, p2, v4}, LO/w;-><init>(LaH/h;LO/z;LO/u;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 246
    return-void
.end method

.method public a(LaH/h;LO/z;[LO/b;)V
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    .line 230
    const/4 v0, 0x1

    new-array v2, v0, [LO/U;

    const/4 v0, 0x0

    invoke-virtual {p2}, LO/z;->m()LO/U;

    move-result-object v1

    aput-object v1, v2, v0

    .line 231
    iget-object v7, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v8, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v9, 0x3

    new-instance v0, LO/w;

    invoke-virtual {p2}, LO/z;->d()I

    move-result v3

    const/4 v4, 0x5

    const/4 v6, 0x0

    move-object v1, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, LO/w;-><init>(LaH/h;[LO/U;II[LO/b;LO/u;)V

    invoke-virtual {v8, v9, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 234
    return-void
.end method

.method public a(LaH/h;[LO/U;I[LO/b;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 208
    if-nez p2, :cond_a

    .line 209
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A to location must be specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_a
    iget-object v7, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v8, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v9, 0x3

    new-instance v0, LO/w;

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, LO/w;-><init>(LaH/h;[LO/U;II[LO/b;LO/u;)V

    invoke-virtual {v8, v9, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 215
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 286
    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0, p1}, LO/r;->a(Z)V

    .line 287
    return-void
.end method

.method public b()V
    .registers 4

    .prologue
    .line 278
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 279
    return-void
.end method

.method public b(LO/q;)V
    .registers 3
    .parameter

    .prologue
    .line 379
    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0, p1}, LO/r;->b(LO/q;)V

    .line 380
    return-void
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 391
    iget-object v0, p0, LO/t;->e:LO/r;

    if-eqz v0, :cond_9

    .line 392
    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0, p1}, LO/r;->b(Z)V

    .line 394
    :cond_9
    return-void
.end method

.method public c()V
    .registers 4

    .prologue
    .line 290
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 291
    return-void
.end method

.method public d()V
    .registers 4

    .prologue
    .line 294
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 295
    return-void
.end method

.method protected e()V
    .registers 4

    .prologue
    .line 298
    iget-object v0, p0, LO/t;->g:LP/u;

    if-eqz v0, :cond_9

    .line 299
    iget-object v0, p0, LO/t;->g:LP/u;

    invoke-virtual {v0}, LP/u;->a()V

    .line 301
    :cond_9
    iget-object v0, p0, LO/t;->e:LO/r;

    if-eqz v0, :cond_12

    .line 302
    iget-object v0, p0, LO/t;->e:LO/r;

    invoke-virtual {v0}, LO/r;->n()V

    .line 304
    :cond_12
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 307
    :try_start_1e
    invoke-virtual {p0}, LO/t;->join()V
    :try_end_21
    .catch Ljava/lang/InterruptedException; {:try_start_1e .. :try_end_21} :catch_25

    .line 311
    :goto_21
    const/4 v0, 0x0

    sput-object v0, LO/t;->a:LO/t;

    .line 312
    return-void

    .line 308
    :catch_25
    move-exception v0

    goto :goto_21
.end method

.method public f()V
    .registers 4

    .prologue
    .line 323
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 324
    return-void
.end method

.method public g()V
    .registers 4

    .prologue
    .line 330
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 331
    return-void
.end method

.method public l()V
    .registers 5

    .prologue
    .line 399
    const/4 v0, 0x1

    :try_start_1
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_4} :catch_b

    .line 404
    :goto_4
    invoke-direct {p0}, LO/t;->h()V

    .line 405
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 407
    return-void

    .line 400
    :catch_b
    move-exception v0

    .line 401
    const-string v1, "NavigationThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .registers 5
    .parameter

    .prologue
    .line 316
    iget-object v0, p0, LO/t;->b:Landroid/os/Handler;

    iget-object v1, p0, LO/t;->b:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 317
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 336
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 341
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 346
    return-void
.end method
