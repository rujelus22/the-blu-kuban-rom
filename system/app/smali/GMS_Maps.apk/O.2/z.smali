.class public final LO/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LO/G;


# static fields
.field private static final F:Ljava/util/Comparator;

.field public static final a:[Ljava/lang/String;


# instance fields
.field private A:Ljava/util/List;

.field private B:Z

.field private final C:Z

.field private D:Z

.field private E:Ljava/lang/String;

.field private final b:I

.field private final c:I

.field private final d:Z

.field private final e:[LO/N;

.field private f:Lo/X;

.field private g:Lo/aa;

.field private final h:[LO/W;

.field private i:I

.field private j:Ljava/util/ArrayList;

.field private k:Ljava/lang/String;

.field private volatile l:[LO/C;

.field private m:[D

.field private n:[D

.field private final o:I

.field private final p:I

.field private final q:Z

.field private final r:F

.field private final s:F

.field private t:J

.field private u:Z

.field private final v:I

.field private w:LO/g;

.field private x:[LO/b;

.field private y:[B

.field private final z:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 203
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "TRAFFIC_STATUS_UNKNOWN"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "TRAFFIC_STATUS_BLACK"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "TRAFFIC_STATUS_RED"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "TRAFFIC_STATUS_YELLOW"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "TRAFFIC_STATUS_GREEN"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "TRAFFIC_STATUS_IRRELEVANT"

    aput-object v2, v0, v1

    sput-object v0, LO/z;->a:[Ljava/lang/String;

    .line 976
    new-instance v0, LO/A;

    invoke-direct {v0}, LO/A;-><init>()V

    sput-object v0, LO/z;->F:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>(II[LO/U;LO/U;FF[LO/N;Lo/X;Ljava/lang/String;IILjava/util/ArrayList;[LO/C;ZI[LO/b;[BILjava/util/List;Ljava/lang/String;)V
    .registers 26
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    const/4 v1, 0x0

    iput v1, p0, LO/z;->i:I

    .line 272
    const/4 v1, 0x0

    iput-boolean v1, p0, LO/z;->u:Z

    .line 316
    const/4 v1, 0x0

    iput-boolean v1, p0, LO/z;->D:Z

    .line 320
    const-string v1, ""

    iput-object v1, p0, LO/z;->E:Ljava/lang/String;

    .line 373
    iput p1, p0, LO/z;->b:I

    .line 374
    iput p2, p0, LO/z;->c:I

    .line 375
    iput p5, p0, LO/z;->r:F

    .line 376
    iput p6, p0, LO/z;->s:F

    .line 377
    iput-object p7, p0, LO/z;->e:[LO/N;

    .line 378
    iput-object p8, p0, LO/z;->f:Lo/X;

    .line 379
    iput-object p9, p0, LO/z;->k:Ljava/lang/String;

    .line 380
    iput p10, p0, LO/z;->p:I

    .line 381
    move-object/from16 v0, p12

    iput-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    .line 382
    move/from16 v0, p14

    iput-boolean v0, p0, LO/z;->d:Z

    .line 383
    move/from16 v0, p15

    iput v0, p0, LO/z;->v:I

    .line 384
    move-object/from16 v0, p16

    iput-object v0, p0, LO/z;->x:[LO/b;

    .line 385
    move-object/from16 v0, p17

    iput-object v0, p0, LO/z;->y:[B

    .line 386
    move/from16 v0, p18

    iput v0, p0, LO/z;->z:I

    .line 387
    if-eqz p19, :cond_b1

    :goto_3a
    move-object/from16 v0, p19

    iput-object v0, p0, LO/z;->A:Ljava/util/List;

    .line 390
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, LO/z;->t:J

    .line 391
    array-length v1, p7

    if-nez v1, :cond_b7

    const/4 v1, 0x1

    :goto_48
    iput-boolean v1, p0, LO/z;->q:Z

    .line 393
    new-instance v1, Lo/aa;

    iget-object v2, p0, LO/z;->f:Lo/X;

    invoke-direct {v1, v2}, Lo/aa;-><init>(Lo/X;)V

    iput-object v1, p0, LO/z;->g:Lo/aa;

    .line 394
    invoke-direct {p0}, LO/z;->I()V

    .line 396
    iget-object v1, p0, LO/z;->e:[LO/N;

    invoke-static {v1}, LO/z;->c([LO/N;)V

    .line 397
    iget-object v1, p0, LO/z;->e:[LO/N;

    invoke-static {v1}, LO/z;->d([LO/N;)V

    .line 398
    iget-object v1, p0, LO/z;->e:[LO/N;

    invoke-static {v1}, LO/z;->a([LO/N;)Z

    move-result v1

    iput-boolean v1, p0, LO/z;->B:Z

    .line 399
    iget-object v1, p0, LO/z;->e:[LO/N;

    invoke-static {v1}, LO/z;->b([LO/N;)Z

    move-result v1

    iput-boolean v1, p0, LO/z;->C:Z

    .line 401
    if-eqz p3, :cond_b9

    .line 402
    array-length v1, p3

    new-array v1, v1, [LO/W;

    iput-object v1, p0, LO/z;->h:[LO/W;

    .line 403
    const/4 v1, 0x0

    :goto_78
    array-length v2, p3

    if-ge v1, v2, :cond_bc

    .line 404
    iget-object v2, p0, LO/z;->h:[LO/W;

    new-instance v3, LO/W;

    aget-object v4, p3, v1

    invoke-direct {v3, v4, p0}, LO/W;-><init>(LO/U;LO/z;)V

    aput-object v3, v2, v1

    .line 405
    if-eqz p4, :cond_a0

    aget-object v2, p3, v1

    invoke-virtual {v2}, LO/U;->c()Lo/u;

    move-result-object v2

    invoke-virtual {p4}, LO/U;->c()Lo/u;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/u;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a0

    .line 407
    iget-object v2, p0, LO/z;->h:[LO/W;

    aget-object v2, v2, v1

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LO/W;->b(Z)V

    .line 409
    :cond_a0
    iget-object v2, p0, LO/z;->h:[LO/W;

    aget-object v2, v2, v1

    invoke-virtual {v2}, LO/W;->b()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_ae

    .line 410
    const/4 v2, 0x1

    iput-boolean v2, p0, LO/z;->D:Z

    .line 403
    :cond_ae
    add-int/lit8 v1, v1, 0x1

    goto :goto_78

    .line 387
    :cond_b1
    new-instance p19, Ljava/util/ArrayList;

    invoke-direct/range {p19 .. p19}, Ljava/util/ArrayList;-><init>()V

    goto :goto_3a

    .line 391
    :cond_b7
    const/4 v1, 0x0

    goto :goto_48

    .line 414
    :cond_b9
    const/4 v1, 0x0

    iput-object v1, p0, LO/z;->h:[LO/W;

    .line 420
    :cond_bc
    invoke-virtual {p0}, LO/z;->t()Z

    move-result v1

    if-nez v1, :cond_c5

    const/4 v1, 0x1

    if-ne p2, v1, :cond_d8

    :cond_c5
    const/4 v1, 0x1

    :goto_c6
    iput-boolean v1, p0, LO/z;->u:Z

    .line 423
    if-nez p13, :cond_da

    .line 424
    invoke-direct {p0}, LO/z;->H()V

    .line 425
    move/from16 v0, p11

    iput v0, p0, LO/z;->o:I

    .line 431
    :goto_d1
    if-eqz p20, :cond_d7

    .line 432
    move-object/from16 v0, p20

    iput-object v0, p0, LO/z;->E:Ljava/lang/String;

    .line 434
    :cond_d7
    return-void

    .line 420
    :cond_d8
    const/4 v1, 0x0

    goto :goto_c6

    .line 427
    :cond_da
    move-object/from16 v0, p13

    iput-object v0, p0, LO/z;->l:[LO/C;

    .line 428
    const-wide/16 v1, 0x0

    invoke-virtual {p0, v1, v2}, LO/z;->b(D)I

    move-result v1

    iput v1, p0, LO/z;->o:I

    goto :goto_d1
.end method

.method private H()V
    .registers 6

    .prologue
    .line 839
    iget-object v0, p0, LO/z;->e:[LO/N;

    if-eqz v0, :cond_2e

    .line 840
    iget-object v0, p0, LO/z;->e:[LO/N;

    array-length v0, v0

    new-array v2, v0, [LO/C;

    .line 841
    const/4 v1, 0x0

    .line 842
    iget-object v0, p0, LO/z;->e:[LO/N;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_f
    if-ltz v0, :cond_2c

    .line 843
    new-instance v3, LO/C;

    iget-object v4, p0, LO/z;->e:[LO/N;

    aget-object v4, v4, v0

    invoke-virtual {v4}, LO/N;->z()I

    move-result v4

    invoke-direct {v3, v4, v1}, LO/C;-><init>(II)V

    aput-object v3, v2, v0

    .line 844
    iget-object v3, p0, LO/z;->e:[LO/N;

    aget-object v3, v3, v0

    invoke-virtual {v3}, LO/N;->f()I

    move-result v3

    add-int/2addr v1, v3

    .line 842
    add-int/lit8 v0, v0, -0x1

    goto :goto_f

    .line 846
    :cond_2c
    iput-object v2, p0, LO/z;->l:[LO/C;

    .line 848
    :cond_2e
    return-void
.end method

.method private I()V
    .registers 10

    .prologue
    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    .line 855
    iget-object v0, p0, LO/z;->f:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    new-array v0, v0, [D

    iput-object v0, p0, LO/z;->m:[D

    .line 856
    iget-object v0, p0, LO/z;->f:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    new-array v0, v0, [D

    iput-object v0, p0, LO/z;->n:[D

    .line 857
    iget-object v0, p0, LO/z;->f:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    if-nez v0, :cond_20

    .line 873
    :cond_1f
    return-void

    .line 860
    :cond_20
    iget-object v0, p0, LO/z;->m:[D

    aput-wide v1, v0, v3

    .line 861
    iget-object v0, p0, LO/z;->n:[D

    aput-wide v1, v0, v3

    .line 865
    const/4 v0, 0x1

    move-wide v3, v1

    :goto_2a
    iget-object v5, p0, LO/z;->m:[D

    array-length v5, v5

    if-ge v0, v5, :cond_1f

    .line 866
    iget-object v5, p0, LO/z;->f:Lo/X;

    add-int/lit8 v6, v0, -0x1

    invoke-virtual {v5, v6}, Lo/X;->b(I)F

    move-result v5

    float-to-double v5, v5

    .line 867
    add-double/2addr v3, v5

    .line 868
    iget-object v7, p0, LO/z;->f:Lo/X;

    invoke-virtual {v7, v0}, Lo/X;->a(I)Lo/T;

    move-result-object v7

    invoke-virtual {v7}, Lo/T;->b()D

    move-result-wide v7

    invoke-static {v7, v8}, Lo/T;->a(D)D

    move-result-wide v7

    div-double/2addr v5, v7

    add-double/2addr v1, v5

    .line 870
    iget-object v5, p0, LO/z;->m:[D

    aput-wide v3, v5, v0

    .line 871
    iget-object v5, p0, LO/z;->n:[D

    aput-wide v1, v5, v0

    .line 865
    add-int/lit8 v0, v0, 0x1

    goto :goto_2a
.end method

.method private static a(LO/N;)LO/j;
    .registers 5
    .parameter

    .prologue
    .line 1292
    invoke-virtual {p0}, LO/N;->v()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/j;

    .line 1293
    invoke-virtual {v0}, LO/j;->a()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    .line 1297
    :goto_1b
    return-object v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public static a(IILO/U;LO/U;[LO/N;Lo/X;Ljava/lang/String;I[LO/b;ILjava/util/List;)LO/z;
    .registers 33
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 550
    const/4 v1, 0x0

    :goto_1
    move-object/from16 v0, p4

    array-length v2, v0

    if-ge v1, v2, :cond_55

    .line 551
    aget-object v2, p4, v1

    invoke-virtual {v2}, LO/N;->z()I

    move-result v2

    if-ltz v2, :cond_1a

    aget-object v2, p4, v1

    invoke-virtual {v2}, LO/N;->z()I

    move-result v2

    invoke-virtual/range {p5 .. p5}, Lo/X;->b()I

    move-result v3

    if-lt v2, v3, :cond_52

    .line 553
    :cond_1a
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid point index for step: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " point index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v1, p4, v1

    invoke-virtual {v1}, LO/N;->z()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 550
    :cond_52
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 557
    :cond_55
    const/4 v1, 0x2

    new-array v4, v1, [LO/U;

    const/4 v1, 0x0

    aput-object p2, v4, v1

    const/4 v1, 0x1

    aput-object p3, v4, v1

    .line 558
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 559
    const/4 v1, 0x1

    :goto_61
    move-object/from16 v0, p4

    array-length v2, v0

    if-ge v1, v2, :cond_77

    .line 560
    aget-object v2, p4, v1

    invoke-virtual {v2}, LO/N;->f()I

    move-result v2

    add-int/2addr v11, v2

    .line 561
    aget-object v2, p4, v1

    invoke-virtual {v2}, LO/N;->e()I

    move-result v2

    add-int/2addr v12, v2

    .line 559
    add-int/lit8 v1, v1, 0x1

    goto :goto_61

    .line 563
    :cond_77
    new-instance v1, LO/z;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v18, 0x0

    const/16 v21, 0x0

    move/from16 v2, p0

    move/from16 v3, p1

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move/from16 v16, p7

    move-object/from16 v17, p8

    move/from16 v19, p9

    move-object/from16 v20, p10

    invoke-direct/range {v1 .. v21}, LO/z;-><init>(II[LO/U;LO/U;FF[LO/N;Lo/X;Ljava/lang/String;IILjava/util/ArrayList;[LO/C;ZI[LO/b;[BILjava/util/List;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I[LO/U;FFZLO/U;I[LO/b;)LO/z;
    .registers 31
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 464
    const/16 v1, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_13

    .line 465
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trips with multiple routes are not supported."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 467
    :cond_13
    const/16 v1, 0x15

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_b4

    const/4 v1, 0x1

    .line 469
    :goto_1e
    const/16 v2, 0x14

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v10

    .line 471
    const/16 v2, 0x1c

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_b7

    const/16 v2, 0x1c

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 473
    :goto_38
    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 474
    const/16 v2, 0xb

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_bb

    const/16 v2, 0xb

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    .line 476
    :goto_4f
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_bd

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v11

    .line 478
    :goto_5b
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_bf

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v12

    .line 481
    :goto_67
    invoke-static/range {p0 .. p0}, LO/z;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v20

    .line 482
    const/16 v4, 0x1d

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v18

    .line 484
    invoke-static {v3}, LO/z;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lo/u;

    move-result-object v4

    invoke-static {v4}, LO/z;->a([Lo/u;)Lo/X;

    move-result-object v9

    .line 487
    if-nez v1, :cond_c1

    .line 488
    move-object/from16 v0, p2

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, p2, v1

    invoke-static {v3, v9, v1}, LO/z;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/X;LO/U;)[LO/N;

    move-result-object v8

    .line 494
    :goto_88
    const/4 v1, 0x0

    aget-object v1, p2, v1

    move-object/from16 v0, p2

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v4, p2, v4

    invoke-static {v3, v1, v4}, LO/z;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LO/U;LO/U;)[LO/U;

    move-result-object v4

    .line 496
    invoke-static {v3, v9}, LO/z;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/X;)Ljava/util/ArrayList;

    move-result-object v13

    .line 497
    invoke-static {v3}, LO/z;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/C;

    move-result-object v14

    .line 499
    const/16 v19, -0x1

    .line 500
    new-instance v1, LO/z;

    move/from16 v3, p1

    move-object/from16 v5, p6

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v15, p5

    move/from16 v16, p7

    move-object/from16 v17, p8

    invoke-direct/range {v1 .. v21}, LO/z;-><init>(II[LO/U;LO/U;FF[LO/N;Lo/X;Ljava/lang/String;IILjava/util/ArrayList;[LO/C;ZI[LO/b;[BILjava/util/List;Ljava/lang/String;)V

    return-object v1

    .line 467
    :cond_b4
    const/4 v1, 0x0

    goto/16 :goto_1e

    .line 471
    :cond_b7
    const-string v21, ""

    goto/16 :goto_38

    .line 474
    :cond_bb
    const/4 v2, 0x0

    goto :goto_4f

    .line 476
    :cond_bd
    const/4 v11, 0x0

    goto :goto_5b

    .line 478
    :cond_bf
    const/4 v12, 0x0

    goto :goto_67

    .line 490
    :cond_c1
    const/4 v1, 0x0

    new-array v8, v1, [LO/N;

    goto :goto_88
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/X;)Ljava/util/ArrayList;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/16 v8, 0xf

    const/4 v0, 0x0

    .line 1025
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1028
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    move v1, v0

    move v2, v0

    move v3, v0

    .line 1033
    :goto_f
    if-ge v1, v5, :cond_39

    .line 1034
    invoke-virtual {p0, v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    .line 1036
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 1039
    invoke-virtual {p1}, Lo/X;->b()I

    move-result v7

    if-lt v0, v7, :cond_26

    .line 1040
    invoke-virtual {p1}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1042
    :cond_26
    if-le v0, v3, :cond_30

    .line 1043
    new-instance v7, LO/F;

    invoke-direct {v7, v3, v0, v2}, LO/F;-><init>(III)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1046
    :cond_30
    const/4 v2, 0x2

    invoke-virtual {v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    .line 1033
    add-int/lit8 v1, v1, 0x1

    move v3, v0

    goto :goto_f

    .line 1048
    :cond_39
    invoke-virtual {p1}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v3, v0, :cond_4f

    .line 1049
    new-instance v0, LO/F;

    invoke-virtual {p1}, Lo/X;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-direct {v0, v3, v1, v2}, LO/F;-><init>(III)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1052
    :cond_4f
    return-object v4
.end method

.method static a([Lo/u;)Lo/X;
    .registers 10
    .parameter

    .prologue
    const/high16 v8, 0x4100

    .line 802
    array-length v3, p0

    .line 803
    new-instance v4, Lo/Z;

    invoke-direct {v4, v3}, Lo/Z;-><init>(I)V

    .line 804
    const/4 v1, 0x0

    .line 805
    const/4 v0, 0x0

    :goto_a
    if-ge v0, v3, :cond_5d

    .line 806
    aget-object v2, p0, v0

    .line 807
    invoke-virtual {v2}, Lo/u;->a()I

    move-result v5

    invoke-virtual {v2}, Lo/u;->b()I

    move-result v2

    invoke-static {v5, v2}, Lo/T;->b(II)Lo/T;

    move-result-object v2

    .line 814
    if-eqz v1, :cond_56

    invoke-virtual {v2, v1}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_56

    .line 815
    add-int/lit8 v5, v0, 0x1

    if-ge v5, v3, :cond_47

    .line 816
    add-int/lit8 v5, v0, 0x1

    aget-object v5, p0, v5

    invoke-virtual {v5}, Lo/u;->a()I

    move-result v5

    add-int/lit8 v6, v0, 0x1

    aget-object v6, p0, v6

    invoke-virtual {v6}, Lo/u;->b()I

    move-result v6

    invoke-static {v5, v6}, Lo/T;->b(II)Lo/T;

    move-result-object v5

    .line 819
    invoke-virtual {v5, v2}, Lo/T;->c(Lo/T;)F

    move-result v6

    .line 820
    cmpl-float v7, v6, v8

    if-lez v7, :cond_47

    .line 821
    div-float v6, v8, v6

    invoke-static {v2, v5, v6, v2}, Lo/T;->a(Lo/T;Lo/T;FLo/T;)V

    .line 824
    :cond_47
    invoke-virtual {v2, v1}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_56

    .line 826
    invoke-virtual {v2}, Lo/T;->f()I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    invoke-virtual {v2, v1}, Lo/T;->a(I)V

    .line 829
    :cond_56
    invoke-virtual {v4, v2}, Lo/Z;->a(Lo/T;)Z

    .line 805
    add-int/lit8 v0, v0, 0x1

    move-object v1, v2

    goto :goto_a

    .line 832
    :cond_5d
    invoke-virtual {v4}, Lo/Z;->d()Lo/X;

    move-result-object v0

    return-object v0
.end method

.method static a([LO/N;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1489
    array-length v3, p0

    move v2, v1

    :goto_3
    if-ge v2, v3, :cond_2f

    aget-object v0, p0, v2

    .line 1490
    invoke-virtual {v0}, LO/N;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/Q;

    .line 1495
    invoke-virtual {v0}, LO/Q;->a()I

    move-result v0

    .line 1496
    const/4 v5, 0x2

    if-eq v0, v5, :cond_29

    const/4 v5, 0x3

    if-eq v0, v5, :cond_29

    const/16 v5, 0x9

    if-ne v0, v5, :cond_f

    .line 1498
    :cond_29
    const/4 v0, 0x1

    .line 1502
    :goto_2a
    return v0

    .line 1489
    :cond_2b
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_2f
    move v0, v1

    .line 1502
    goto :goto_2a
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/C;
    .registers 8
    .parameter

    .prologue
    const/16 v6, 0xc

    .line 1060
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 1061
    new-array v0, v2, [LO/C;

    .line 1062
    const/4 v1, 0x0

    :goto_9
    if-ge v1, v2, :cond_25

    .line 1063
    invoke-virtual {p0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 1064
    const/16 v4, 0xd

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    .line 1065
    const/16 v5, 0xe

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    .line 1066
    new-instance v5, LO/C;

    invoke-direct {v5, v4, v3}, LO/C;-><init>(II)V

    aput-object v5, v0, v1

    .line 1062
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 1068
    :cond_25
    if-nez v2, :cond_28

    const/4 v0, 0x0

    :cond_28
    return-object v0
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/X;LO/U;)[LO/N;
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v11, 0xa

    const/4 v8, 0x0

    .line 1233
    invoke-virtual {p0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v9

    .line 1234
    new-array v10, v9, [LO/N;

    .line 1235
    const/4 v0, 0x0

    move v2, v8

    move-object v1, v0

    .line 1236
    :goto_c
    if-ge v2, v9, :cond_5e

    .line 1237
    invoke-virtual {p0, v11, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1240
    if-eqz v1, :cond_5f

    .line 1241
    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v3

    .line 1243
    const/4 v4, 0x4

    invoke-static {v1, v4}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v4

    .line 1247
    :goto_1e
    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v7

    .line 1251
    invoke-virtual {p1}, Lo/X;->b()I

    move-result v1

    const/4 v5, 0x2

    if-lt v1, v5, :cond_5b

    .line 1252
    if-lez v7, :cond_4f

    add-int/lit8 v1, v7, -0x1

    invoke-virtual {p1, v1}, Lo/X;->d(I)F

    move-result v1

    .line 1255
    :goto_32
    invoke-virtual {p1}, Lo/X;->b()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v7, v5, :cond_54

    invoke-virtual {p1, v7}, Lo/X;->d(I)F

    move-result v5

    :goto_3e
    move v6, v5

    move v5, v1

    .line 1261
    :goto_40
    invoke-virtual {p1, v7}, Lo/X;->a(I)Lo/T;

    move-result-object v1

    move-object v7, p2

    invoke-static/range {v0 .. v7}, LO/N;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/T;IIIFFLO/U;)LO/N;

    move-result-object v1

    aput-object v1, v10, v2

    .line 1236
    add-int/lit8 v2, v2, 0x1

    move-object v1, v0

    goto :goto_c

    .line 1252
    :cond_4f
    invoke-virtual {p1, v7}, Lo/X;->d(I)F

    move-result v1

    goto :goto_32

    .line 1255
    :cond_54
    add-int/lit8 v5, v7, -0x1

    invoke-virtual {p1, v5}, Lo/X;->d(I)F

    move-result v5

    goto :goto_3e

    .line 1259
    :cond_5b
    const/4 v6, 0x0

    move v5, v6

    goto :goto_40

    .line 1266
    :cond_5e
    return-object v10

    :cond_5f
    move v4, v8

    move v3, v8

    goto :goto_1e
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LO/U;LO/U;)[LO/U;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v7, 0x10

    const/4 v0, 0x0

    .line 1082
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 1083
    add-int/lit8 v2, v1, 0x2

    new-array v2, v2, [LO/U;

    .line 1084
    new-instance v3, LO/U;

    invoke-direct {v3, p1}, LO/U;-><init>(LO/U;)V

    aput-object v3, v2, v0

    .line 1085
    add-int/lit8 v3, v1, 0x1

    new-instance v4, LO/U;

    invoke-direct {v4, p2}, LO/U;-><init>(LO/U;)V

    aput-object v4, v2, v3

    .line 1086
    :goto_1b
    if-ge v0, v1, :cond_2e

    .line 1087
    add-int/lit8 v3, v0, 0x1

    new-instance v4, LO/U;

    invoke-virtual {p0, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, LO/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v4, v2, v3

    .line 1086
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b

    .line 1089
    :cond_2e
    return-object v2
.end method

.method private b(Lo/T;DZ)Ljava/util/List;
    .registers 21
    .parameter
    .parameter
    .parameter

    .prologue
    .line 931
    move-object/from16 v0, p0

    iget-object v1, v0, LO/z;->g:Lo/aa;

    const-wide/high16 v2, 0x3ff0

    add-double v2, v2, p2

    double-to-int v2, v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v2

    invoke-virtual {v1, v2}, Lo/aa;->a(Lo/ad;)Ljava/util/List;

    move-result-object v7

    .line 933
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1b

    .line 934
    const/4 v1, 0x0

    .line 971
    :goto_1a
    return-object v1

    .line 938
    :cond_1b
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 940
    new-instance v8, Lo/T;

    invoke-direct {v8}, Lo/T;-><init>()V

    .line 941
    new-instance v9, Lo/T;

    invoke-direct {v9}, Lo/T;-><init>()V

    .line 942
    new-instance v10, Lo/T;

    invoke-direct {v10}, Lo/T;-><init>()V

    .line 943
    const/4 v1, 0x0

    move v3, v1

    :goto_31
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_ac

    .line 944
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/am;

    .line 945
    invoke-virtual {v1}, Lo/am;->a()I

    move-result v2

    add-int/lit8 v11, v2, -0x1

    .line 946
    const/4 v2, 0x0

    move v6, v2

    :goto_45
    if-ge v6, v11, :cond_a8

    .line 947
    invoke-virtual {v1, v6, v8}, Lo/am;->a(ILo/T;)V

    .line 948
    add-int/lit8 v2, v6, 0x1

    invoke-virtual {v1, v2, v9}, Lo/am;->a(ILo/T;)V

    .line 949
    move-object/from16 v0, p1

    invoke-static {v8, v9, v0, v10}, Lo/T;->a(Lo/T;Lo/T;Lo/T;Lo/T;)F

    move-result v2

    float-to-double v12, v2

    .line 951
    cmpg-double v2, v12, p2

    if-gez v2, :cond_8d

    .line 952
    const/4 v5, 0x0

    .line 953
    if-nez p4, :cond_63

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_91

    .line 954
    :cond_63
    new-instance v2, LO/D;

    const/4 v5, 0x0

    invoke-direct {v2, v5}, LO/D;-><init>(LO/A;)V

    .line 955
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 961
    :goto_6c
    if-eqz v2, :cond_8d

    .line 962
    move-object/from16 v0, p0

    invoke-static {v2, v0}, LO/D;->a(LO/D;LO/z;)LO/z;

    .line 963
    invoke-static {v2, v12, v13}, LO/D;->a(LO/D;D)D

    .line 964
    invoke-virtual {v1}, Lo/am;->b()I

    move-result v5

    add-int/2addr v5, v6

    invoke-static {v2, v5}, LO/D;->a(LO/D;I)I

    .line 965
    invoke-static {v10}, Lo/T;->a(Lo/T;)Lo/T;

    move-result-object v5

    invoke-static {v2, v5}, LO/D;->a(LO/D;Lo/T;)Lo/T;

    .line 966
    invoke-static {v8, v9}, Lo/V;->b(Lo/T;Lo/T;)F

    move-result v5

    float-to-double v12, v5

    invoke-static {v2, v12, v13}, LO/D;->b(LO/D;D)D

    .line 946
    :cond_8d
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_45

    .line 956
    :cond_91
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LO/D;

    invoke-static {v2}, LO/D;->b(LO/D;)D

    move-result-wide v14

    cmpg-double v2, v12, v14

    if-gez v2, :cond_b8

    .line 959
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LO/D;

    goto :goto_6c

    .line 943
    :cond_a8
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_31

    .line 971
    :cond_ac
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_b5

    const/4 v1, 0x0

    goto/16 :goto_1a

    :cond_b5
    move-object v1, v4

    goto/16 :goto_1a

    :cond_b8
    move-object v2, v5

    goto :goto_6c
.end method

.method static b([LO/N;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1510
    array-length v3, p0

    move v2, v1

    :goto_3
    if-ge v2, v3, :cond_28

    aget-object v0, p0, v2

    .line 1511
    invoke-virtual {v0}, LO/N;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/Q;

    .line 1512
    invoke-virtual {v0}, LO/Q;->a()I

    move-result v0

    const/4 v5, 0x4

    if-ne v0, v5, :cond_f

    .line 1513
    const/4 v0, 0x1

    .line 1517
    :goto_23
    return v0

    .line 1510
    :cond_24
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_28
    move v0, v1

    .line 1517
    goto :goto_23
.end method

.method static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lo/u;
    .registers 15
    .parameter

    .prologue
    const/16 v13, 0x9

    const/4 v12, 0x7

    const/4 v2, 0x0

    .line 1304
    invoke-virtual {p0, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v7

    move v0, v2

    move v1, v2

    .line 1307
    :goto_a
    if-ge v0, v7, :cond_1d

    .line 1310
    invoke-virtual {p0, v12, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 1312
    invoke-virtual {v3, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v3

    array-length v3, v3

    .line 1315
    div-int/lit8 v3, v3, 0x4

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v1, v3

    .line 1307
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 1317
    :cond_1d
    new-array v8, v1, [Lo/u;

    move v6, v2

    move v4, v2

    .line 1320
    :goto_21
    if-ge v6, v7, :cond_84

    .line 1321
    invoke-virtual {p0, v12, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    .line 1323
    const/16 v0, 0x8

    invoke-virtual {v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1326
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    .line 1327
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 1329
    add-int/lit8 v0, v4, 0x1

    new-instance v9, Lo/u;

    invoke-direct {v9, v3, v1}, Lo/u;-><init>(II)V

    aput-object v9, v8, v4

    .line 1331
    invoke-virtual {v5, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v9

    .line 1333
    if-eqz v9, :cond_7f

    .line 1334
    array-length v10, v9

    move v4, v3

    move v3, v1

    move v1, v2

    .line 1335
    :goto_50
    if-ge v1, v10, :cond_7f

    .line 1338
    add-int/lit8 v5, v1, 0x1

    aget-byte v1, v9, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v11, v5, 0x1

    aget-byte v5, v9, v5

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v1, v5

    int-to-short v1, v1

    add-int/2addr v4, v1

    .line 1341
    add-int/lit8 v5, v11, 0x1

    aget-byte v1, v9, v11

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v11, v1, 0x8

    add-int/lit8 v1, v5, 0x1

    aget-byte v5, v9, v5

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v5, v11

    int-to-short v5, v5

    add-int/2addr v3, v5

    .line 1344
    add-int/lit8 v5, v0, 0x1

    new-instance v11, Lo/u;

    invoke-direct {v11, v4, v3}, Lo/u;-><init>(II)V

    aput-object v11, v8, v0

    move v0, v5

    goto :goto_50

    .line 1320
    :cond_7f
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v4, v0

    goto :goto_21

    .line 1349
    :cond_84
    return-object v8
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .registers 9
    .parameter

    .prologue
    const/16 v7, 0x1a

    const/4 v6, 0x2

    .line 507
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 509
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    .line 510
    const/4 v0, 0x0

    move v1, v0

    :goto_e
    if-ge v1, v3, :cond_31

    .line 511
    invoke-virtual {p0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 514
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 516
    invoke-virtual {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_2f

    invoke-virtual {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 520
    :goto_23
    new-instance v5, LO/S;

    invoke-direct {v5, v4, v0}, LO/S;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 510
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_e

    .line 516
    :cond_2f
    const/4 v0, 0x0

    goto :goto_23

    .line 524
    :cond_31
    return-object v2
.end method

.method private static c([LO/N;)V
    .registers 4
    .parameter

    .prologue
    .line 1271
    const/4 v0, 0x1

    :goto_1
    array-length v1, p0

    if-ge v0, v1, :cond_19

    .line 1272
    add-int/lit8 v1, v0, -0x1

    aget-object v1, p0, v1

    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, LO/N;->a(LO/N;)V

    .line 1273
    aget-object v1, p0, v0

    add-int/lit8 v2, v0, -0x1

    aget-object v2, p0, v2

    invoke-virtual {v1, v2}, LO/N;->b(LO/N;)V

    .line 1271
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1275
    :cond_19
    return-void
.end method

.method private static d([LO/N;)V
    .registers 5
    .parameter

    .prologue
    .line 1279
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_33

    .line 1280
    aget-object v0, p0, v1

    invoke-virtual {v0}, LO/N;->v()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_11
    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/j;

    .line 1281
    invoke-virtual {v0}, LO/j;->f()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 1282
    add-int/lit8 v3, v1, 0x1

    aget-object v3, p0, v3

    invoke-static {v3}, LO/z;->a(LO/N;)LO/j;

    move-result-object v3

    invoke-virtual {v0, v3}, LO/j;->a(LO/j;)V

    goto :goto_11

    .line 1279
    :cond_2f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1286
    :cond_33
    return-void
.end method


# virtual methods
.method public A()V
    .registers 2

    .prologue
    .line 1378
    const/4 v0, 0x0

    iput-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    .line 1379
    invoke-direct {p0}, LO/z;->H()V

    .line 1380
    return-void
.end method

.method public B()Z
    .registers 2

    .prologue
    .line 1383
    iget-boolean v0, p0, LO/z;->u:Z

    return v0
.end method

.method C()LO/g;
    .registers 2

    .prologue
    .line 1399
    iget-object v0, p0, LO/z;->w:LO/g;

    return-object v0
.end method

.method public D()[LO/b;
    .registers 2

    .prologue
    .line 1438
    iget-object v0, p0, LO/z;->x:[LO/b;

    return-object v0
.end method

.method public E()Z
    .registers 2

    .prologue
    .line 1453
    iget-boolean v0, p0, LO/z;->B:Z

    return v0
.end method

.method public F()Z
    .registers 2

    .prologue
    .line 1460
    iget-boolean v0, p0, LO/z;->C:Z

    return v0
.end method

.method public G()Z
    .registers 2

    .prologue
    .line 1524
    iget-boolean v0, p0, LO/z;->D:Z

    return v0
.end method

.method public a(LO/D;)D
    .registers 6
    .parameter

    .prologue
    .line 760
    iget-object v0, p0, LO/z;->m:[D

    invoke-virtual {p1}, LO/D;->e()I

    move-result v1

    aget-wide v0, v0, v1

    iget-object v2, p0, LO/z;->f:Lo/X;

    invoke-virtual {p1}, LO/D;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Lo/X;->a(I)Lo/T;

    move-result-object v2

    invoke-static {p1}, LO/D;->a(LO/D;)Lo/T;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/T;->c(Lo/T;)F

    move-result v2

    float-to-double v2, v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public a(D)I
    .registers 4
    .parameter

    .prologue
    .line 1097
    iget-object v0, p0, LO/z;->m:[D

    invoke-static {v0, p1, p2}, Ljava/util/Arrays;->binarySearch([DD)I

    move-result v0

    .line 1098
    if-gez v0, :cond_b

    .line 1101
    add-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    .line 1103
    :cond_b
    return v0
.end method

.method public a(Landroid/content/Context;II)LO/E;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 1177
    if-gez p2, :cond_b

    .line 1178
    new-instance v0, LO/E;

    const-string v1, ""

    invoke-direct {v0, v1, v3}, LO/E;-><init>(Ljava/lang/String;LO/A;)V

    .line 1216
    :goto_a
    return-object v0

    .line 1182
    :cond_b
    const/4 v2, 0x0

    .line 1183
    if-lez p2, :cond_10

    .line 1184
    add-int/lit8 p2, p2, -0x1

    .line 1187
    :cond_10
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1188
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object v0, v3

    move-object v1, v3

    .line 1189
    :goto_1c
    invoke-virtual {p0}, LO/z;->k()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge p2, v4, :cond_6f

    .line 1190
    invoke-virtual {p0, p2}, LO/z;->a(I)LO/N;

    move-result-object v4

    .line 1191
    invoke-virtual {v4}, LO/N;->x()LO/P;

    move-result-object v7

    .line 1192
    if-eqz v7, :cond_6c

    invoke-virtual {v7}, LO/P;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6c

    .line 1193
    add-int/lit8 v4, p2, 0x1

    invoke-virtual {p0, v4}, LO/z;->a(I)LO/N;

    move-result-object v4

    invoke-virtual {v4}, LO/N;->e()I

    move-result v4

    int-to-float v4, v4

    .line 1195
    cmpl-float v8, v4, v2

    if-lez v8, :cond_4c

    .line 1196
    invoke-virtual {v7}, LO/P;->b()Ljava/lang/String;

    move-result-object v1

    .line 1197
    invoke-virtual {v7}, LO/P;->c()Ljava/lang/String;

    move-result-object v0

    move v2, v4

    .line 1200
    :cond_4c
    int-to-float v8, p3

    const/high16 v9, 0x3e80

    mul-float/2addr v8, v9

    cmpl-float v4, v4, v8

    if-lez v4, :cond_6c

    invoke-virtual {v7}, LO/P;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6c

    .line 1202
    invoke-virtual {v7}, LO/P;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1203
    invoke-virtual {v7}, LO/P;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1189
    :cond_6c
    add-int/lit8 p2, p2, 0x1

    goto :goto_1c

    .line 1208
    :cond_6f
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v4, 0x1

    if-le v2, v4, :cond_93

    .line 1209
    const v0, 0x7f0d0094

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1210
    const v0, 0x7f0d0095

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1211
    new-instance v0, LO/E;

    invoke-static {v1, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, LO/E;-><init>(Ljava/lang/String;Ljava/lang/String;LO/A;)V

    goto/16 :goto_a

    .line 1213
    :cond_93
    if-eqz v1, :cond_9d

    .line 1214
    new-instance v2, LO/E;

    invoke-direct {v2, v1, v0, v3}, LO/E;-><init>(Ljava/lang/String;Ljava/lang/String;LO/A;)V

    move-object v0, v2

    goto/16 :goto_a

    .line 1216
    :cond_9d
    new-instance v0, LO/E;

    const v1, 0x7f0d00a2

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, LO/E;-><init>(Ljava/lang/String;LO/A;)V

    goto/16 :goto_a
.end method

.method public a(Lo/T;D)LO/H;
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 886
    invoke-direct {p0, p1, p2, p3, v1}, LO/z;->b(Lo/T;DZ)Ljava/util/List;

    move-result-object v0

    .line 887
    if-nez v0, :cond_9

    .line 888
    const/4 v0, 0x0

    .line 890
    :goto_8
    return-object v0

    :cond_9
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/H;

    goto :goto_8
.end method

.method public a(I)LO/N;
    .registers 3
    .parameter

    .prologue
    .line 718
    iget-object v0, p0, LO/z;->e:[LO/N;

    aget-object v0, v0, p1

    return-object v0
.end method

.method a(LO/g;)V
    .registers 2
    .parameter

    .prologue
    .line 1407
    iput-object p1, p0, LO/z;->w:LO/g;

    .line 1408
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 1158
    iput-object p1, p0, LO/z;->k:Ljava/lang/String;

    .line 1159
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 662
    iget v0, p0, LO/z;->z:I

    if-ltz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public a(Lo/T;DZ)[LO/D;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 906
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, LO/z;->b(Lo/T;DZ)Ljava/util/List;

    move-result-object v0

    .line 907
    if-nez v0, :cond_b

    .line 908
    const/4 v0, 0x0

    new-array v0, v0, [LO/D;

    .line 915
    :cond_a
    :goto_a
    return-object v0

    .line 910
    :cond_b
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [LO/D;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LO/D;

    .line 912
    if-eqz p4, :cond_a

    .line 913
    sget-object v1, LO/z;->F:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    goto :goto_a
.end method

.method public b(I)D
    .registers 4
    .parameter

    .prologue
    .line 752
    iget-object v0, p0, LO/z;->m:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public b(LO/D;)D
    .registers 8
    .parameter

    .prologue
    .line 778
    iget-object v0, p0, LO/z;->n:[D

    invoke-virtual {p1}, LO/D;->e()I

    move-result v1

    aget-wide v0, v0, v1

    iget-object v2, p0, LO/z;->f:Lo/X;

    invoke-virtual {p1}, LO/D;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Lo/X;->a(I)Lo/T;

    move-result-object v2

    invoke-static {p1}, LO/D;->a(LO/D;)Lo/T;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/T;->c(Lo/T;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {p1}, LO/D;->a(LO/D;)Lo/T;

    move-result-object v4

    invoke-virtual {v4}, Lo/T;->b()D

    move-result-wide v4

    invoke-static {v4, v5}, Lo/T;->a(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public b(D)I
    .registers 14
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1123
    iget-object v2, p0, LO/z;->l:[LO/C;

    .line 1124
    new-instance v0, LO/C;

    invoke-virtual {p0, p1, p2}, LO/z;->a(D)I

    move-result v3

    invoke-direct {v0, v3, v1}, LO/C;-><init>(II)V

    new-instance v3, LO/B;

    invoke-direct {v3, p0}, LO/B;-><init>(LO/z;)V

    invoke-static {v2, v0, v3}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    .line 1133
    if-gez v0, :cond_1a

    .line 1134
    add-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    .line 1136
    :cond_1a
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-lt v0, v3, :cond_21

    move v0, v1

    .line 1142
    :goto_20
    return v0

    .line 1139
    :cond_21
    iget-object v1, p0, LO/z;->m:[D

    aget-object v3, v2, v0

    iget v3, v3, LO/C;->a:I

    aget-wide v3, v1, v3

    .line 1140
    iget-object v1, p0, LO/z;->m:[D

    add-int/lit8 v5, v0, 0x1

    aget-object v5, v2, v5

    iget v5, v5, LO/C;->a:I

    aget-wide v5, v1, v5

    .line 1142
    add-int/lit8 v1, v0, 0x1

    aget-object v1, v2, v1

    iget v1, v1, LO/C;->b:I

    int-to-double v7, v1

    sub-double v9, v5, p1

    sub-double v3, v5, v3

    div-double v3, v9, v3

    aget-object v1, v2, v0

    iget v1, v1, LO/C;->b:I

    add-int/lit8 v0, v0, 0x1

    aget-object v0, v2, v0

    iget v0, v0, LO/C;->b:I

    sub-int v0, v1, v0

    int-to-double v0, v0

    mul-double/2addr v0, v3

    add-double/2addr v0, v7

    double-to-int v0, v0

    goto :goto_20
.end method

.method public b()Z
    .registers 3

    .prologue
    .line 666
    iget v0, p0, LO/z;->c:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public c(I)D
    .registers 4
    .parameter

    .prologue
    .line 770
    iget-object v0, p0, LO/z;->n:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 670
    iget v0, p0, LO/z;->z:I

    return v0
.end method

.method public c(D)Ljava/util/Collection;
    .registers 7
    .parameter

    .prologue
    .line 1362
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1363
    const/4 v0, 0x1

    :goto_6
    iget-object v2, p0, LO/z;->h:[LO/W;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_37

    .line 1364
    iget-object v2, p0, LO/z;->h:[LO/W;

    aget-object v2, v2, v0

    invoke-virtual {v2}, LO/W;->a()Z

    move-result v2

    if-eqz v2, :cond_34

    iget-object v2, p0, LO/z;->h:[LO/W;

    aget-object v2, v2, v0

    invoke-virtual {v2}, LO/W;->m()Z

    move-result v2

    if-nez v2, :cond_34

    iget-object v2, p0, LO/z;->h:[LO/W;

    aget-object v2, v2, v0

    invoke-virtual {v2}, LO/W;->n()D

    move-result-wide v2

    cmpl-double v2, v2, p1

    if-lez v2, :cond_34

    .line 1367
    iget-object v2, p0, LO/z;->h:[LO/W;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1363
    :cond_34
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1370
    :cond_37
    return-object v1
.end method

.method public d()I
    .registers 2

    .prologue
    .line 674
    iget v0, p0, LO/z;->b:I

    return v0
.end method

.method public d(I)V
    .registers 2
    .parameter

    .prologue
    .line 789
    iput p1, p0, LO/z;->i:I

    .line 790
    return-void
.end method

.method public e()F
    .registers 2

    .prologue
    .line 678
    iget v0, p0, LO/z;->r:F

    return v0
.end method

.method public f()F
    .registers 2

    .prologue
    .line 682
    iget v0, p0, LO/z;->s:F

    return v0
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 690
    iget v0, p0, LO/z;->c:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public h()Z
    .registers 3

    .prologue
    .line 694
    iget v0, p0, LO/z;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public i()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 698
    iget v1, p0, LO/z;->c:I

    if-ne v1, v0, :cond_6

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 702
    iget-boolean v0, p0, LO/z;->d:Z

    return v0
.end method

.method public k()I
    .registers 2

    .prologue
    .line 706
    iget-object v0, p0, LO/z;->e:[LO/N;

    array-length v0, v0

    return v0
.end method

.method public l()LO/U;
    .registers 3

    .prologue
    .line 710
    iget-object v0, p0, LO/z;->h:[LO/W;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, LO/z;->h:[LO/W;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_5
.end method

.method public m()LO/U;
    .registers 3

    .prologue
    .line 714
    iget-object v0, p0, LO/z;->h:[LO/W;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, LO/z;->h:[LO/W;

    iget-object v1, p0, LO/z;->h:[LO/W;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    goto :goto_5
.end method

.method public n()Lo/X;
    .registers 2

    .prologue
    .line 722
    iget-object v0, p0, LO/z;->f:Lo/X;

    return-object v0
.end method

.method public o()I
    .registers 2

    .prologue
    .line 729
    iget v0, p0, LO/z;->o:I

    return v0
.end method

.method public p()I
    .registers 2

    .prologue
    .line 736
    iget v0, p0, LO/z;->p:I

    return v0
.end method

.method public q()I
    .registers 2

    .prologue
    .line 744
    iget v0, p0, LO/z;->v:I

    return v0
.end method

.method public r()I
    .registers 2

    .prologue
    .line 785
    iget v0, p0, LO/z;->i:I

    return v0
.end method

.method public s()Ljava/util/List;
    .registers 2

    .prologue
    .line 793
    iget-object v0, p0, LO/z;->A:Ljava/util/List;

    return-object v0
.end method

.method public t()Z
    .registers 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 999
    iget-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_e

    iget-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_10

    :cond_e
    move v0, v1

    .line 1010
    :goto_f
    return v0

    .line 1002
    :cond_10
    iget-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v2, :cond_2f

    .line 1003
    iget-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/F;

    .line 1004
    invoke-virtual {v0}, LO/F;->a()I

    move-result v3

    if-eqz v3, :cond_2d

    invoke-virtual {v0}, LO/F;->a()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_2f

    :cond_2d
    move v0, v1

    .line 1007
    goto :goto_f

    :cond_2f
    move v0, v2

    .line 1010
    goto :goto_f
.end method

.method public u()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1018
    iget-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method public v()[LO/W;
    .registers 2

    .prologue
    .line 1150
    iget-object v0, p0, LO/z;->h:[LO/W;

    return-object v0
.end method

.method public w()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1154
    iget-object v0, p0, LO/z;->k:Ljava/lang/String;

    return-object v0
.end method

.method public x()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1221
    iget-object v0, p0, LO/z;->E:Ljava/lang/String;

    return-object v0
.end method

.method public y()Z
    .registers 2

    .prologue
    .line 1353
    iget-boolean v0, p0, LO/z;->q:Z

    return v0
.end method

.method public z()J
    .registers 3

    .prologue
    .line 1374
    iget-wide v0, p0, LO/z;->t:J

    return-wide v0
.end method
