.class public LO/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Z


# direct methods
.method constructor <init>(IZ)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput p1, p0, LO/l;->a:I

    .line 31
    iput-boolean p2, p0, LO/l;->b:Z

    .line 32
    return-void
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/l;
    .registers 4
    .parameter

    .prologue
    .line 43
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 44
    if-ltz v1, :cond_a

    const/4 v0, 0x6

    if-lt v1, v0, :cond_c

    .line 45
    :cond_a
    const/4 v0, 0x0

    .line 48
    :goto_b
    return-object v0

    .line 47
    :cond_c
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v2

    .line 48
    new-instance v0, LO/l;

    invoke-direct {v0, v1, v2}, LO/l;-><init>(IZ)V

    goto :goto_b
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 67
    iget v0, p0, LO/l;->a:I

    packed-switch v0, :pswitch_data_3e

    .line 96
    const-string v0, " "

    :goto_7
    return-object v0

    .line 69
    :pswitch_8
    const-string v0, "\u2191"

    goto :goto_7

    .line 71
    :pswitch_b
    iget-boolean v0, p0, LO/l;->b:Z

    if-eqz v0, :cond_12

    .line 72
    const-string v0, "\u2197"

    goto :goto_7

    .line 74
    :cond_12
    const-string v0, "\u2196"

    goto :goto_7

    .line 76
    :pswitch_15
    iget-boolean v0, p0, LO/l;->b:Z

    if-eqz v0, :cond_1c

    .line 77
    const-string v0, "\u21b1"

    goto :goto_7

    .line 79
    :cond_1c
    const-string v0, "\u21b0"

    goto :goto_7

    .line 81
    :pswitch_1f
    iget-boolean v0, p0, LO/l;->b:Z

    if-eqz v0, :cond_26

    .line 82
    const-string v0, "\u2198"

    goto :goto_7

    .line 84
    :cond_26
    const-string v0, "\u2199"

    goto :goto_7

    .line 86
    :pswitch_29
    iget-boolean v0, p0, LO/l;->b:Z

    if-eqz v0, :cond_30

    .line 87
    const-string v0, "\u21b7"

    goto :goto_7

    .line 89
    :cond_30
    const-string v0, "\u21b6"

    goto :goto_7

    .line 91
    :pswitch_33
    iget-boolean v0, p0, LO/l;->b:Z

    if-eqz v0, :cond_3a

    .line 92
    const-string v0, "\u21bf"

    goto :goto_7

    .line 94
    :cond_3a
    const-string v0, "\u21be"

    goto :goto_7

    .line 67
    nop

    :pswitch_data_3e
    .packed-switch 0x0
        :pswitch_8
        :pswitch_b
        :pswitch_15
        :pswitch_1f
        :pswitch_29
        :pswitch_33
    .end packed-switch
.end method
