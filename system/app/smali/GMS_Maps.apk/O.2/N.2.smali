.class public LO/N;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lo/T;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:F

.field private final j:F

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/String;

.field private final n:Landroid/text/Spanned;

.field private final o:LO/P;

.field private final p:Ljava/util/Map;

.field private final q:Ljava/util/List;

.field private final r:Ljava/util/List;

.field private final s:Ljava/util/List;

.field private final t:Ljava/util/List;

.field private final u:Ljava/util/List;

.field private final v:Ljava/util/List;

.field private final w:Ljava/util/List;

.field private x:LO/N;

.field private y:LO/N;


# direct methods
.method protected constructor <init>(IIILo/T;IILjava/lang/String;IIFFLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .registers 24
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392
    iput p1, p0, LO/N;->b:I

    .line 393
    iput p2, p0, LO/N;->c:I

    .line 394
    iput p3, p0, LO/N;->d:I

    .line 395
    iput-object p4, p0, LO/N;->a:Lo/T;

    .line 396
    iput p5, p0, LO/N;->e:I

    .line 397
    iput p6, p0, LO/N;->f:I

    .line 398
    iput p8, p0, LO/N;->g:I

    .line 399
    iput p9, p0, LO/N;->h:I

    .line 400
    iput p10, p0, LO/N;->i:F

    .line 401
    move/from16 v0, p11

    iput v0, p0, LO/N;->j:F

    .line 403
    move-object/from16 v0, p12

    iput-object v0, p0, LO/N;->k:Ljava/lang/String;

    .line 404
    move-object/from16 v0, p13

    iput-object v0, p0, LO/N;->l:Ljava/lang/String;

    .line 405
    move-object/from16 v0, p14

    iput-object v0, p0, LO/N;->m:Ljava/lang/String;

    .line 406
    move-object/from16 v0, p16

    iput-object v0, p0, LO/N;->u:Ljava/util/List;

    .line 407
    move-object/from16 v0, p17

    iput-object v0, p0, LO/N;->v:Ljava/util/List;

    .line 408
    move-object/from16 v0, p18

    iput-object v0, p0, LO/N;->w:Ljava/util/List;

    .line 410
    iget-object v1, p0, LO/N;->v:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_37
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_47

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LO/j;

    .line 411
    invoke-virtual {v1, p0}, LO/j;->a(LO/N;)V

    goto :goto_37

    .line 414
    :cond_47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LO/N;->q:Ljava/util/List;

    .line 415
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LO/N;->r:Ljava/util/List;

    .line 416
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LO/N;->s:Ljava/util/List;

    .line 417
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LO/N;->t:Ljava/util/List;

    .line 419
    move-object/from16 v0, p15

    invoke-static {p0, v0}, LO/N;->a(LO/N;Ljava/util/List;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, LO/N;->p:Ljava/util/Map;

    .line 420
    iget-object v1, p0, LO/N;->p:Ljava/util/Map;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, LO/N;->q:Ljava/util/List;

    invoke-static {v1, v2}, LO/N;->a(Ljava/util/List;Ljava/util/List;)V

    .line 421
    iget-object v1, p0, LO/N;->p:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, LO/N;->r:Ljava/util/List;

    invoke-static {v1, v2}, LO/N;->a(Ljava/util/List;Ljava/util/List;)V

    .line 422
    iget-object v1, p0, LO/N;->p:Ljava/util/Map;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, LO/N;->r:Ljava/util/List;

    invoke-static {v1, v2}, LO/N;->a(Ljava/util/List;Ljava/util/List;)V

    .line 423
    iget-object v1, p0, LO/N;->p:Ljava/util/Map;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, LO/N;->s:Ljava/util/List;

    invoke-static {v1, v2}, LO/N;->a(Ljava/util/List;Ljava/util/List;)V

    .line 424
    iget-object v1, p0, LO/N;->p:Ljava/util/Map;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, LO/N;->t:Ljava/util/List;

    invoke-static {v1, v2}, LO/N;->a(Ljava/util/List;Ljava/util/List;)V

    .line 425
    iget v1, p0, LO/N;->b:I

    const/16 v2, 0x10

    if-ne v1, v2, :cond_10d

    .line 426
    iget-object v1, p0, LO/N;->p:Ljava/util/Map;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 427
    if-eqz v1, :cond_eb

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_eb

    .line 428
    iget-object v2, p0, LO/N;->q:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 430
    :cond_eb
    iget-object v1, p0, LO/N;->p:Ljava/util/Map;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 431
    if-eqz v1, :cond_10d

    .line 432
    const/4 v2, 0x0

    :goto_fb
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_10d

    .line 433
    iget-object v3, p0, LO/N;->r:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 432
    add-int/lit8 v2, v2, 0x1

    goto :goto_fb

    .line 438
    :cond_10d
    iget-object v1, p0, LO/N;->p:Ljava/util/Map;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 439
    if-eqz v1, :cond_134

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_134

    .line 440
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LO/P;

    iput-object v1, p0, LO/N;->o:LO/P;

    .line 444
    :goto_12b
    iget-object v1, p0, LO/N;->p:Ljava/util/Map;

    invoke-static {p7, v1}, LO/N;->a(Ljava/lang/String;Ljava/util/Map;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p0, LO/N;->n:Landroid/text/Spanned;

    .line 445
    return-void

    .line 442
    :cond_134
    const/4 v1, 0x0

    iput-object v1, p0, LO/N;->o:LO/P;

    goto :goto_12b
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 919
    .line 920
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 922
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 924
    if-ltz v0, :cond_12

    const/16 v2, 0x14

    if-lt v0, v2, :cond_13

    :cond_12
    move v0, v1

    .line 928
    :cond_13
    :goto_13
    return v0

    :cond_14
    move v0, v1

    goto :goto_13
.end method

.method public static a(LO/R;III)LO/N;
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 544
    new-instance v0, LO/N;

    invoke-virtual/range {p0 .. p0}, LO/R;->b()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, LO/R;->c()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, LO/R;->d()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, LO/R;->a()Lo/T;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, LO/R;->e()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, LO/R;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, LO/R;->h()F

    move-result v10

    invoke-virtual/range {p0 .. p0}, LO/R;->i()F

    move-result v11

    invoke-virtual/range {p0 .. p0}, LO/R;->k()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, LO/R;->l()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, LO/R;->m()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, LO/R;->n()Ljava/util/List;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, LO/R;->o()Ljava/util/List;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, LO/R;->p()Ljava/util/List;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, LO/R;->q()Ljava/util/List;

    move-result-object v18

    move/from16 v5, p1

    move/from16 v8, p2

    move/from16 v9, p3

    invoke-direct/range {v0 .. v18}, LO/N;-><init>(IIILo/T;IILjava/lang/String;IIFFLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/T;IIIFFLO/U;)LO/N;
    .registers 29
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 469
    const/16 v3, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 470
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 471
    const/16 v3, 0x14

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 472
    const/4 v3, 0x5

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v8

    .line 473
    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v5

    .line 474
    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v9

    .line 475
    const/4 v3, 0x4

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v10

    .line 483
    const/16 v3, 0x13

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_a2

    .line 484
    const/16 v3, 0x13

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 486
    invoke-static {v4}, LO/N;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v11

    .line 487
    invoke-static {v4}, LO/N;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v7

    .line 488
    const/16 v3, 0xc

    if-ne v11, v3, :cond_9b

    .line 489
    const/4 v3, 0x7

    const/4 v6, -0x1

    invoke-static {v4, v3, v6}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v3

    .line 497
    :goto_58
    invoke-static {v4}, LO/N;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v18

    .line 498
    move-object/from16 v0, p1

    invoke-static {v4, v0}, LO/N;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/T;)Ljava/util/List;

    move-result-object v19

    .line 499
    invoke-static {v4}, LO/N;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v20

    .line 501
    const/16 v6, 0x10

    if-ne v11, v6, :cond_9d

    .line 502
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 503
    if-eqz p7, :cond_be

    .line 504
    move-object/from16 v0, p7

    invoke-static {v0, v6}, LO/N;->a(LO/U;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    .line 505
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v12

    if-nez v12, :cond_bc

    :goto_7d
    move-object v5, v4

    move-object v4, v6

    :goto_7f
    move-object/from16 v17, v4

    move v6, v7

    move-object v13, v5

    move v5, v11

    move v7, v3

    .line 523
    :goto_85
    new-instance v3, LO/R;

    move-object/from16 v4, p1

    move/from16 v11, p5

    move/from16 v12, p6

    invoke-direct/range {v3 .. v20}, LO/R;-><init>(Lo/T;IIIIIIFFLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 527
    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-static {v3, v0, v1, v2}, LO/N;->a(LO/R;III)LO/N;

    move-result-object v3

    return-object v3

    .line 494
    :cond_9b
    const/4 v3, -0x1

    goto :goto_58

    .line 510
    :cond_9d
    invoke-static {v4}, LO/N;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/ArrayList;

    move-result-object v4

    goto :goto_7f

    .line 513
    :cond_a2
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 514
    const/4 v3, 0x0

    .line 515
    const/4 v6, 0x0

    .line 516
    const/4 v7, -0x1

    .line 517
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 518
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 519
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object v13, v5

    move v5, v3

    goto :goto_85

    :cond_bc
    move-object v4, v5

    goto :goto_7d

    :cond_be
    move-object v4, v6

    goto :goto_7f
.end method

.method protected static a(Ljava/lang/String;Ljava/util/Map;)Landroid/text/Spanned;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 586
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 591
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v0, v1, v3}, Ljava/text/Bidi;->requiresBidi([CII)Z

    move-result v0

    if-eqz v0, :cond_16

    move-object v0, v2

    .line 627
    :goto_15
    return-object v0

    .line 595
    :cond_16
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 596
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_23
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 597
    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_23

    .line 601
    :cond_33
    new-instance v0, LO/O;

    invoke-direct {v0}, LO/O;-><init>()V

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 609
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v3, v1

    .line 610
    :goto_40
    if-ge v3, v5, :cond_7e

    .line 611
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/P;

    .line 612
    invoke-virtual {v0}, LO/P;->b()Ljava/lang/String;

    move-result-object v6

    .line 613
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_56

    .line 610
    :cond_52
    :goto_52
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_40

    .line 616
    :cond_56
    const/4 v1, -0x1

    .line 618
    :cond_57
    invoke-virtual {v0}, LO/P;->b()Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v7, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 621
    if-ltz v1, :cond_71

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v7, v1

    const-class v8, Ljava/lang/Object;

    invoke-virtual {v2, v1, v7, v8}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v7

    array-length v7, v7

    if-nez v7, :cond_57

    .line 622
    :cond_71
    if-ltz v1, :cond_52

    .line 623
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v1

    const/16 v7, 0x21

    invoke-virtual {v2, v0, v1, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_52

    :cond_7e
    move-object v0, v2

    .line 627
    goto :goto_15
.end method

.method private static a(LO/U;Ljava/util/List;)Ljava/lang/String;
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 562
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 565
    invoke-virtual {p0}, LO/U;->e()Ljava/lang/String;

    move-result-object v2

    .line 566
    if-eqz v2, :cond_1b

    .line 567
    new-instance v0, LO/P;

    const/4 v1, 0x6

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, LO/P;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 568
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 571
    :cond_1b
    invoke-virtual {p0}, LO/U;->d()LO/V;

    move-result-object v9

    .line 572
    if-eqz v9, :cond_4d

    move v7, v6

    .line 573
    :goto_22
    invoke-virtual {v9}, LO/V;->a()I

    move-result v0

    if-ge v7, v0, :cond_4d

    .line 574
    new-instance v0, LO/P;

    const/4 v1, 0x7

    invoke-virtual {v9, v7}, LO/V;->a(I)Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, LO/P;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 575
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_42

    .line 576
    const/16 v0, 0xa

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 578
    :cond_42
    invoke-virtual {v9, v7}, LO/V;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 573
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_22

    .line 581
    :cond_4d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/T;)Ljava/util/List;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x6

    .line 966
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 967
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 969
    invoke-virtual {p1}, Lo/T;->e()D

    move-result-wide v3

    .line 970
    const/4 v0, 0x0

    :goto_f
    if-ge v0, v2, :cond_21

    .line 971
    invoke-virtual {p0, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    .line 973
    invoke-static {v5, v3, v4}, LO/j;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;D)LO/j;

    move-result-object v5

    .line 974
    if-eqz v5, :cond_1e

    .line 975
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 970
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 978
    :cond_21
    return-object v1
.end method

.method private static a(LO/N;Ljava/util/List;)Ljava/util/Map;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 637
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 638
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/P;

    .line 639
    invoke-virtual {v0}, LO/P;->a()I

    move-result v4

    .line 640
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 641
    if-nez v1, :cond_31

    .line 642
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 643
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 645
    :cond_31
    invoke-static {v0, p0}, LO/P;->a(LO/P;LO/N;)V

    .line 646
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 648
    :cond_38
    return-object v2
.end method

.method private static a(Ljava/util/List;Ljava/util/List;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 631
    if-eqz p0, :cond_5

    .line 632
    invoke-interface {p1, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 634
    :cond_5
    return-void
.end method

.method public static a(I)Z
    .registers 2
    .parameter

    .prologue
    .line 1013
    const/4 v0, 0x5

    if-eq p0, v0, :cond_10

    const/4 v0, 0x3

    if-eq p0, v0, :cond_10

    const/16 v0, 0xf

    if-eq p0, v0, :cond_10

    const/4 v0, 0x6

    if-eq p0, v0, :cond_10

    const/4 v0, 0x4

    if-ne p0, v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 935
    .line 936
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 938
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 940
    if-ltz v0, :cond_11

    const/4 v2, 0x3

    if-lt v0, v2, :cond_12

    :cond_11
    move v0, v1

    .line 944
    :cond_12
    :goto_12
    return v0

    :cond_13
    move v0, v1

    goto :goto_12
.end method

.method static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .registers 6
    .parameter

    .prologue
    const/4 v4, 0x5

    .line 951
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 952
    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 954
    const/4 v0, 0x0

    :goto_b
    if-ge v0, v2, :cond_1b

    .line 955
    invoke-virtual {p0, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3}, LO/Q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/Q;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 954
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 958
    :cond_1b
    return-object v1
.end method

.method static d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .registers 6
    .parameter

    .prologue
    const/16 v4, 0x8

    .line 985
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 986
    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 988
    const/4 v1, 0x0

    :goto_c
    if-ge v1, v2, :cond_24

    .line 989
    invoke-virtual {p0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 991
    invoke-static {v3}, LO/k;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/k;

    move-result-object v3

    .line 992
    if-nez v3, :cond_20

    .line 994
    const-string v3, "e"

    invoke-static {v3}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    .line 988
    :goto_1d
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 996
    :cond_20
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1d

    .line 1002
    :cond_24
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eq v2, v1, :cond_2f

    .line 1003
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1005
    :cond_2f
    return-object v0
.end method

.method private static e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/ArrayList;
    .registers 6
    .parameter

    .prologue
    const/4 v4, 0x4

    .line 652
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 653
    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 654
    const/4 v0, 0x0

    :goto_b
    if-ge v0, v2, :cond_1b

    .line 655
    invoke-virtual {p0, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 657
    invoke-static {v3}, LO/P;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/P;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 654
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 659
    :cond_1b
    return-object v1
.end method


# virtual methods
.method public a()Lo/T;
    .registers 2

    .prologue
    .line 666
    iget-object v0, p0, LO/N;->a:Lo/T;

    return-object v0
.end method

.method a(LO/N;)V
    .registers 2
    .parameter

    .prologue
    .line 891
    iput-object p1, p0, LO/N;->x:LO/N;

    .line 892
    return-void
.end method

.method public a(LO/j;)V
    .registers 3
    .parameter

    .prologue
    .line 882
    iget-object v0, p0, LO/N;->v:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 883
    invoke-virtual {p1, p0}, LO/j;->a(LO/N;)V

    .line 884
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 674
    iget v0, p0, LO/N;->b:I

    return v0
.end method

.method b(LO/N;)V
    .registers 2
    .parameter

    .prologue
    .line 899
    iput-object p1, p0, LO/N;->y:LO/N;

    .line 900
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 682
    iget v0, p0, LO/N;->c:I

    return v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 690
    iget v0, p0, LO/N;->d:I

    return v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 698
    iget v0, p0, LO/N;->g:I

    return v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 706
    iget v0, p0, LO/N;->h:I

    return v0
.end method

.method public g()F
    .registers 2

    .prologue
    .line 713
    iget v0, p0, LO/N;->i:F

    return v0
.end method

.method public h()F
    .registers 2

    .prologue
    .line 720
    iget v0, p0, LO/N;->j:F

    return v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 727
    iget v0, p0, LO/N;->e:I

    return v0
.end method

.method public j()LO/N;
    .registers 2

    .prologue
    .line 734
    iget-object v0, p0, LO/N;->x:LO/N;

    return-object v0
.end method

.method public k()LO/N;
    .registers 2

    .prologue
    .line 741
    iget-object v0, p0, LO/N;->y:LO/N;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .registers 2

    .prologue
    .line 748
    iget-object v0, p0, LO/N;->k:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .registers 2

    .prologue
    .line 755
    iget-object v0, p0, LO/N;->l:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .registers 2

    .prologue
    .line 763
    iget-object v0, p0, LO/N;->m:Ljava/lang/String;

    return-object v0
.end method

.method public o()Landroid/text/Spanned;
    .registers 2

    .prologue
    .line 771
    iget-object v0, p0, LO/N;->n:Landroid/text/Spanned;

    return-object v0
.end method

.method public p()Ljava/util/List;
    .registers 2

    .prologue
    .line 780
    iget-object v0, p0, LO/N;->q:Ljava/util/List;

    return-object v0
.end method

.method public q()Ljava/util/List;
    .registers 2

    .prologue
    .line 789
    iget-object v0, p0, LO/N;->r:Ljava/util/List;

    return-object v0
.end method

.method public r()Ljava/util/List;
    .registers 2

    .prologue
    .line 801
    iget-object v0, p0, LO/N;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 802
    iget-object v0, p0, LO/N;->q:Ljava/util/List;

    .line 804
    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, LO/N;->s:Ljava/util/List;

    goto :goto_a
.end method

.method public s()Ljava/util/List;
    .registers 2

    .prologue
    .line 813
    iget-object v0, p0, LO/N;->t:Ljava/util/List;

    return-object v0
.end method

.method public t()Ljava/util/Map;
    .registers 2

    .prologue
    .line 821
    iget-object v0, p0, LO/N;->p:Ljava/util/Map;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 872
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[idx:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LO/N;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " loc:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LO/N;->a:Lo/T;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " point:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LO/N;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LO/N;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " side:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LO/N;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " text:\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LO/N;->o()Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\']"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Ljava/util/List;
    .registers 2

    .prologue
    .line 829
    iget-object v0, p0, LO/N;->u:Ljava/util/List;

    return-object v0
.end method

.method public v()Ljava/util/List;
    .registers 2

    .prologue
    .line 837
    iget-object v0, p0, LO/N;->v:Ljava/util/List;

    return-object v0
.end method

.method public w()Ljava/util/List;
    .registers 2

    .prologue
    .line 845
    iget-object v0, p0, LO/N;->w:Ljava/util/List;

    return-object v0
.end method

.method public final x()LO/P;
    .registers 3

    .prologue
    .line 853
    invoke-virtual {p0}, LO/N;->r()Ljava/util/List;

    move-result-object v0

    .line 854
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 855
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_15

    .line 856
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/P;

    .line 858
    :goto_14
    return-object v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public y()LO/P;
    .registers 2

    .prologue
    .line 867
    iget-object v0, p0, LO/N;->o:LO/P;

    return-object v0
.end method

.method public z()I
    .registers 2

    .prologue
    .line 904
    iget v0, p0, LO/N;->f:I

    return v0
.end method
