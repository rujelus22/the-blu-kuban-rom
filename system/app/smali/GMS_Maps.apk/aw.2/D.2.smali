.class public LaW/D;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:LaR/t;

.field public d:Lcom/google/googlenav/ai;

.field final synthetic e:LaW/A;

.field private f:LaR/D;

.field private g:LaR/H;


# direct methods
.method public constructor <init>(LaW/A;Lcom/google/googlenav/ai;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, LaW/D;->e:LaW/A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p2, p0, LaW/D;->d:Lcom/google/googlenav/ai;

    .line 72
    return-void
.end method

.method public constructor <init>(LaW/A;Ljava/lang/String;ZLaR/t;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, LaW/D;->e:LaW/A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p2, p0, LaW/D;->a:Ljava/lang/String;

    .line 76
    iput-boolean p3, p0, LaW/D;->b:Z

    .line 77
    iput-object p4, p0, LaW/D;->c:LaR/t;

    .line 78
    return-void
.end method


# virtual methods
.method public a(LaW/D;)I
    .registers 7
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, LaW/D;->c:LaR/t;

    invoke-virtual {v0}, LaR/t;->i()J

    move-result-wide v0

    .line 83
    iget-object v2, p1, LaW/D;->c:LaR/t;

    invoke-virtual {v2}, LaR/t;->i()J

    move-result-wide v2

    .line 84
    cmp-long v4, v0, v2

    if-nez v4, :cond_12

    .line 85
    const/4 v0, 0x0

    .line 87
    :goto_11
    return v0

    :cond_12
    cmp-long v0, v0, v2

    if-lez v0, :cond_18

    const/4 v0, -0x1

    goto :goto_11

    :cond_18
    const/4 v0, 0x1

    goto :goto_11
.end method

.method public a()LaR/D;
    .registers 3

    .prologue
    .line 110
    iget-object v0, p0, LaW/D;->f:LaR/D;

    if-nez v0, :cond_1c

    .line 111
    iget-boolean v0, p0, LaW/D;->b:Z

    if-nez v0, :cond_1f

    .line 112
    iget-object v0, p0, LaW/D;->e:LaW/A;

    invoke-static {v0}, LaW/A;->a(LaW/A;)LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    iget-object v1, p0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    iput-object v0, p0, LaW/D;->f:LaR/D;

    .line 119
    :cond_1c
    :goto_1c
    iget-object v0, p0, LaW/D;->f:LaR/D;

    return-object v0

    .line 115
    :cond_1f
    iget-object v0, p0, LaW/D;->e:LaW/A;

    invoke-static {v0}, LaW/A;->b(LaW/A;)LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    iget-object v1, p0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    iput-object v0, p0, LaW/D;->f:LaR/D;

    goto :goto_1c
.end method

.method public b()LaR/H;
    .registers 3

    .prologue
    .line 123
    iget-boolean v0, p0, LaW/D;->b:Z

    if-nez v0, :cond_19

    .line 124
    iget-object v0, p0, LaW/D;->e:LaW/A;

    invoke-static {v0}, LaW/A;->a(LaW/A;)LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    iget-object v1, p0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LaR/u;->b(Ljava/lang/String;)LaR/H;

    move-result-object v0

    iput-object v0, p0, LaW/D;->g:LaR/H;

    .line 130
    :goto_16
    iget-object v0, p0, LaW/D;->g:LaR/H;

    return-object v0

    .line 127
    :cond_19
    iget-object v0, p0, LaW/D;->e:LaW/A;

    invoke-static {v0}, LaW/A;->b(LaW/A;)LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    iget-object v1, p0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LaR/u;->b(Ljava/lang/String;)LaR/H;

    move-result-object v0

    iput-object v0, p0, LaW/D;->g:LaR/H;

    goto :goto_16
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 61
    check-cast p1, LaW/D;

    invoke-virtual {p0, p1}, LaW/D;->a(LaW/D;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 93
    instance-of v0, p1, LaW/D;

    if-nez v0, :cond_6

    .line 94
    const/4 v0, 0x0

    .line 100
    :goto_5
    return v0

    .line 96
    :cond_6
    check-cast p1, LaW/D;

    .line 97
    iget-object v0, p0, LaW/D;->a:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 98
    iget-object v0, p0, LaW/D;->a:Ljava/lang/String;

    iget-object v1, p1, LaW/D;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5

    .line 100
    :cond_15
    iget-object v0, p0, LaW/D;->d:Lcom/google/googlenav/ai;

    iget-object v1, p1, LaW/D;->d:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 106
    iget-object v0, p0, LaW/D;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
