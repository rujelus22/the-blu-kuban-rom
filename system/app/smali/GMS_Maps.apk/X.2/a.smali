.class public Lx/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:LD/b;

.field private b:Z

.field private final c:Landroid/graphics/Bitmap;

.field private final d:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lx/a;->a:LD/b;

    .line 65
    iput-boolean v2, p0, Lx/a;->b:Z

    .line 71
    const/16 v0, 0x100

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lx/a;->c:Landroid/graphics/Bitmap;

    .line 79
    invoke-static {}, Lcom/google/common/collect/Maps;->b()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lx/a;->d:Ljava/util/Map;

    .line 81
    return-void
.end method

.method public static c(LD/a;)V
    .registers 4
    .parameter

    .prologue
    .line 106
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1702

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 107
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 108
    const/4 v0, 0x1

    .line 109
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    .line 110
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 112
    return-void
.end method

.method public static d(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 119
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1702

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 120
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 121
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 123
    return-void
.end method

.method private e(LD/a;)V
    .registers 4
    .parameter

    .prologue
    .line 162
    invoke-virtual {p0}, Lx/a;->b()V

    .line 164
    iget-object v0, p0, Lx/a;->a:LD/b;

    if-eqz v0, :cond_16

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v1, p0, Lx/a;->a:LD/b;

    invoke-virtual {v1}, LD/b;->a()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    if-eq v0, v1, :cond_16

    .line 165
    invoke-virtual {p0}, Lx/a;->a()V

    .line 167
    :cond_16
    iget-object v0, p0, Lx/a;->a:LD/b;

    if-nez v0, :cond_27

    .line 168
    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    iput-object v0, p0, Lx/a;->a:LD/b;

    .line 171
    iget-object v0, p0, Lx/a;->a:LD/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LD/b;->c(Z)V

    .line 173
    :cond_27
    iget-object v0, p0, Lx/a;->a:LD/b;

    iget-object v1, p0, Lx/a;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, LD/b;->b(Landroid/graphics/Bitmap;)V

    .line 174
    return-void
.end method


# virtual methods
.method public declared-synchronized a(LD/a;)LD/b;
    .registers 3
    .parameter

    .prologue
    .line 87
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0, p1}, Lx/a;->b(LD/a;)V

    .line 88
    iget-object v0, p0, Lx/a;->a:LD/b;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    monitor-exit p0

    return-object v0

    .line 87
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .registers 2

    .prologue
    .line 180
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lx/a;->a:LD/b;

    if-eqz v0, :cond_d

    .line 183
    iget-object v0, p0, Lx/a;->a:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lx/a;->a:LD/b;
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 186
    :cond_d
    monitor-exit p0

    return-void

    .line 180
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(ILx/b;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x1

    const v3, 0x8000

    .line 140
    monitor-enter p0

    :try_start_6
    iget-object v0, p0, Lx/a;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 141
    if-nez v0, :cond_3b

    .line 142
    iget-object v0, p0, Lx/a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 143
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x100

    if-lt v1, v2, :cond_2f

    .line 144
    const-string v0, "ColorPalette"

    const-string v1, "Color texture is full"

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2d
    .catchall {:try_start_6 .. :try_end_2d} :catchall_54

    .line 156
    :goto_2d
    monitor-exit p0

    return-void

    .line 149
    :cond_2f
    :try_start_2f
    iget-object v1, p0, Lx/a;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    const/4 v1, 0x1

    iput-boolean v1, p0, Lx/a;->b:Z

    .line 152
    :cond_3b
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    rem-int/lit16 v1, v1, 0x100

    mul-int/2addr v1, v4

    add-int/2addr v1, v3

    div-int/lit16 v1, v1, 0x100

    iput v1, p2, Lx/b;->a:I

    .line 154
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit16 v0, v0, 0x100

    mul-int/2addr v0, v4

    add-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x1

    iput v0, p2, Lx/b;->b:I
    :try_end_53
    .catchall {:try_start_2f .. :try_end_53} :catchall_54

    goto :goto_2d

    .line 140
    :catchall_54
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized b()V
    .registers 6

    .prologue
    .line 194
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lx/a;->b:Z

    .line 195
    iget-object v0, p0, Lx/a;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_45

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 196
    iget-object v3, p0, Lx/a;->c:Landroid/graphics/Bitmap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    rem-int/lit16 v4, v1, 0x100

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    div-int/lit16 v1, v1, 0x100

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v4, v1, v0}, Landroid/graphics/Bitmap;->setPixel(III)V
    :try_end_41
    .catchall {:try_start_2 .. :try_end_41} :catchall_42

    goto :goto_e

    .line 194
    :catchall_42
    move-exception v0

    monitor-exit p0

    throw v0

    .line 199
    :cond_45
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 95
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lx/a;->b:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lx/a;->a:LD/b;

    if-nez v0, :cond_c

    .line 96
    :cond_9
    invoke-direct {p0, p1}, Lx/a;->e(LD/a;)V
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_e

    .line 98
    :cond_c
    monitor-exit p0

    return-void

    .line 95
    :catchall_e
    move-exception v0

    monitor-exit p0

    throw v0
.end method
