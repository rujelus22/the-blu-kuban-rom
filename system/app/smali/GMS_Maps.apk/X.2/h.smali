.class public Lx/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:F

.field private static final t:Ljava/lang/ThreadLocal;

.field private static final u:[I

.field private static final v:[I

.field private static final w:[I

.field private static final x:[I

.field private static final y:[[I


# instance fields
.field private final b:Lo/T;

.field private final c:Lo/T;

.field private final d:Lo/T;

.field private final e:Lo/T;

.field private final f:Lo/T;

.field private final g:Lo/T;

.field private final h:Lo/T;

.field private final i:Lo/T;

.field private final j:Lo/T;

.field private final k:Lo/T;

.field private final l:Lo/aH;

.field private final m:Lo/aH;

.field private final n:Lo/aH;

.field private final o:Lo/aH;

.field private final p:Lo/aH;

.field private final q:Lo/aH;

.field private final r:Lo/aH;

.field private final s:Lo/aH;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/16 v2, 0x8

    .line 35
    const-wide/high16 v0, 0x4000

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lx/h;->a:F

    .line 59
    new-instance v0, Lx/i;

    invoke-direct {v0}, Lx/i;-><init>()V

    sput-object v0, Lx/h;->t:Ljava/lang/ThreadLocal;

    .line 101
    new-array v0, v2, [I

    fill-array-data v0, :array_38

    sput-object v0, Lx/h;->u:[I

    .line 109
    new-array v0, v2, [I

    fill-array-data v0, :array_4c

    sput-object v0, Lx/h;->v:[I

    .line 117
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_60

    sput-object v0, Lx/h;->w:[I

    .line 123
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_6c

    sput-object v0, Lx/h;->x:[I

    .line 465
    const/16 v0, 0x10

    new-array v0, v0, [[I

    sput-object v0, Lx/h;->y:[[I

    .line 466
    return-void

    .line 101
    :array_38
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x80t 0x0t 0x0t
    .end array-data

    .line 109
    :array_4c
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x1t 0x0t
    .end array-data

    .line 117
    :array_60
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x40t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x40t 0x0t 0x0t
    .end array-data

    .line 123
    :array_6c
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x40t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x40t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x40t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x40t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x40t 0x0t 0x0t
    .end array-data
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lx/h;->b:Lo/T;

    .line 68
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lx/h;->c:Lo/T;

    .line 69
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lx/h;->d:Lo/T;

    .line 70
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lx/h;->e:Lo/T;

    .line 71
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lx/h;->f:Lo/T;

    .line 72
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lx/h;->g:Lo/T;

    .line 73
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lx/h;->h:Lo/T;

    .line 74
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lx/h;->i:Lo/T;

    .line 75
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lx/h;->j:Lo/T;

    .line 76
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lx/h;->k:Lo/T;

    .line 77
    new-instance v0, Lo/aH;

    invoke-direct {v0}, Lo/aH;-><init>()V

    iput-object v0, p0, Lx/h;->l:Lo/aH;

    .line 78
    new-instance v0, Lo/aH;

    invoke-direct {v0}, Lo/aH;-><init>()V

    iput-object v0, p0, Lx/h;->m:Lo/aH;

    .line 79
    new-instance v0, Lo/aH;

    invoke-direct {v0}, Lo/aH;-><init>()V

    iput-object v0, p0, Lx/h;->n:Lo/aH;

    .line 80
    new-instance v0, Lo/aH;

    invoke-direct {v0}, Lo/aH;-><init>()V

    iput-object v0, p0, Lx/h;->o:Lo/aH;

    .line 81
    new-instance v0, Lo/aH;

    invoke-direct {v0}, Lo/aH;-><init>()V

    iput-object v0, p0, Lx/h;->p:Lo/aH;

    .line 82
    new-instance v0, Lo/aH;

    invoke-direct {v0}, Lo/aH;-><init>()V

    iput-object v0, p0, Lx/h;->q:Lo/aH;

    .line 83
    new-instance v0, Lo/aH;

    invoke-direct {v0}, Lo/aH;-><init>()V

    iput-object v0, p0, Lx/h;->r:Lo/aH;

    .line 84
    new-instance v0, Lo/aH;

    invoke-direct {v0}, Lo/aH;-><init>()V

    iput-object v0, p0, Lx/h;->s:Lo/aH;

    .line 85
    return-void
.end method

.method synthetic constructor <init>(Lx/i;)V
    .registers 2
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Lx/h;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;)I
    .registers 4
    .parameter

    .prologue
    .line 548
    const/4 v0, 0x0

    .line 549
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    .line 550
    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_6

    .line 552
    :cond_1b
    return v1
.end method

.method public static a(Ljava/util/List;Z)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 511
    if-eqz p1, :cond_20

    const/4 v0, 0x5

    move v1, v0

    .line 513
    :goto_4
    const/4 v0, 0x0

    .line 514
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    .line 515
    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 516
    mul-int/2addr v0, v1

    add-int/2addr v0, v2

    move v2, v0

    .line 517
    goto :goto_a

    .line 511
    :cond_20
    const/4 v0, 0x4

    move v1, v0

    goto :goto_4

    .line 518
    :cond_23
    return v2
.end method

.method public static a(Lo/X;)I
    .registers 3
    .parameter

    .prologue
    .line 430
    invoke-virtual {p0}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 431
    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    .line 433
    const/4 v0, 0x6

    .line 438
    :goto_a
    return v0

    :cond_b
    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, 0x8

    goto :goto_a
.end method

.method public static a()Lx/h;
    .registers 1

    .prologue
    .line 88
    sget-object v0, Lx/h;->t:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/h;

    return-object v0
.end method

.method private a(Lo/T;Lo/T;FIZLE/q;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 828
    iget-object v0, p0, Lx/h;->d:Lo/T;

    .line 829
    iget-object v1, p0, Lx/h;->e:Lo/T;

    .line 832
    invoke-static {p2, p1, v0}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 833
    invoke-static {v0, p3, v1}, Lo/V;->a(Lo/T;FLo/T;)V

    .line 836
    invoke-static {p1, v1, v0}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 837
    invoke-interface {p6, v0, p4}, LE/q;->a(Lo/T;I)V

    .line 838
    invoke-static {p1, v1, v0}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 839
    invoke-interface {p6, v0, p4}, LE/q;->a(Lo/T;I)V

    .line 842
    invoke-static {p2, v1, v0}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 843
    invoke-interface {p6, v0, p4}, LE/q;->a(Lo/T;I)V

    .line 844
    invoke-static {p2, v1, v0}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 845
    invoke-interface {p6, v0, p4}, LE/q;->a(Lo/T;I)V

    .line 847
    if-eqz p5, :cond_27

    .line 849
    invoke-interface {p6, p2, p4}, LE/q;->a(Lo/T;I)V

    .line 851
    :cond_27
    return-void
.end method

.method private static a(I)[I
    .registers 8
    .parameter

    .prologue
    const v6, 0x8000

    const/high16 v5, 0x1

    const/4 v1, 0x0

    .line 469
    mul-int/lit8 v0, p0, 0x5

    mul-int/lit8 v0, v0, 0x2

    new-array v3, v0, [I

    .line 471
    div-int v0, v6, p0

    move v2, v0

    move v0, v1

    .line 472
    :goto_10
    array-length v4, v3

    if-ge v0, v4, :cond_3f

    .line 474
    aput v1, v3, v0

    .line 475
    add-int/lit8 v4, v0, 0x1

    aput v2, v3, v4

    .line 476
    add-int/lit8 v4, v0, 0x2

    aput v5, v3, v4

    .line 477
    add-int/lit8 v4, v0, 0x3

    aput v2, v3, v4

    .line 478
    add-int/lit8 v4, v0, 0x4

    aput v5, v3, v4

    .line 479
    add-int/lit8 v4, v0, 0x5

    aput v2, v3, v4

    .line 480
    add-int/lit8 v4, v0, 0x6

    aput v1, v3, v4

    .line 481
    add-int/lit8 v4, v0, 0x7

    aput v2, v3, v4

    .line 482
    add-int/lit8 v4, v0, 0x8

    aput v6, v3, v4

    .line 483
    add-int/lit8 v4, v0, 0x9

    aput v2, v3, v4

    .line 484
    div-int v4, v5, p0

    add-int/2addr v2, v4

    .line 473
    add-int/lit8 v0, v0, 0xa

    goto :goto_10

    .line 486
    :cond_3f
    return-object v3
.end method

.method public static b(Ljava/util/List;)I
    .registers 5
    .parameter

    .prologue
    .line 561
    const/4 v0, 0x0

    .line 562
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    .line 563
    invoke-virtual {v0}, Lo/X;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 564
    invoke-virtual {v0}, Lo/X;->e()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 565
    add-int/lit8 v0, v2, 0x1

    .line 567
    :goto_20
    mul-int/lit8 v0, v0, 0x3

    mul-int/lit8 v0, v0, 0x6

    add-int/2addr v0, v1

    move v1, v0

    .line 568
    goto :goto_6

    .line 569
    :cond_27
    return v1

    :cond_28
    move v0, v2

    goto :goto_20
.end method

.method public static b(Ljava/util/List;Z)I
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 528
    .line 529
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    .line 530
    invoke-virtual {v0}, Lo/X;->b()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    .line 531
    mul-int/lit8 v5, v4, 0x2

    .line 532
    invoke-virtual {v0}, Lo/X;->e()Z

    move-result v0

    if-eqz v0, :cond_2b

    move v0, v2

    :goto_21
    sub-int v0, v4, v0

    .line 533
    if-eqz p1, :cond_2d

    .line 534
    add-int/2addr v0, v5

    mul-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v1

    :goto_29
    move v1, v0

    .line 538
    goto :goto_6

    .line 532
    :cond_2b
    const/4 v0, 0x1

    goto :goto_21

    .line 536
    :cond_2d
    mul-int/lit8 v0, v5, 0x3

    add-int/2addr v0, v1

    goto :goto_29

    .line 539
    :cond_31
    return v1
.end method

.method public static b(Lo/X;)I
    .registers 3
    .parameter

    .prologue
    .line 444
    .line 446
    invoke-virtual {p0}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 447
    const/4 v1, 0x1

    if-ne v0, v1, :cond_c

    .line 449
    const/16 v0, 0xc

    .line 452
    :goto_b
    return v0

    :cond_c
    add-int/lit8 v1, v0, 0x2

    mul-int/lit8 v1, v1, 0x6

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v1

    goto :goto_b
.end method

.method private b(Lo/X;FLo/T;ILE/q;LE/k;LE/e;ZZB)I
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 346
    invoke-interface {p5}, LE/q;->a()I

    move-result v1

    .line 350
    iget-object v2, p0, Lx/h;->b:Lo/T;

    .line 351
    iget-object v3, p0, Lx/h;->c:Lo/T;

    .line 352
    iget-object v4, p0, Lx/h;->d:Lo/T;

    .line 353
    iget-object v5, p0, Lx/h;->e:Lo/T;

    .line 354
    iget-object v6, p0, Lx/h;->f:Lo/T;

    .line 355
    iget-object v7, p0, Lx/h;->g:Lo/T;

    .line 356
    iget-object v8, p0, Lx/h;->h:Lo/T;

    .line 359
    const/4 v9, 0x0

    invoke-virtual {p1, v9, p3, v2}, Lo/X;->a(ILo/T;Lo/T;)V

    .line 360
    const/4 v9, 0x1

    invoke-virtual {p1, v9, p3, v3}, Lo/X;->a(ILo/T;Lo/T;)V

    .line 365
    invoke-static {v3, v2, v4}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 366
    invoke-static {v4, p2, v5}, Lo/V;->a(Lo/T;FLo/T;)V

    .line 367
    invoke-static {v5, v6}, Lo/V;->a(Lo/T;Lo/T;)V

    .line 370
    if-eqz p8, :cond_28

    .line 371
    invoke-static {v2, v6, v2}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 373
    :cond_28
    if-eqz p9, :cond_2d

    .line 374
    invoke-static {v3, v6, v3}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 378
    :cond_2d
    invoke-static {v2, v5, v8}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 379
    move/from16 v0, p10

    invoke-interface {p5, v8, p4, v0}, LE/q;->a(Lo/T;IB)V

    .line 380
    invoke-static {v2, v5, v8}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 381
    move/from16 v0, p10

    invoke-interface {p5, v8, p4, v0}, LE/q;->a(Lo/T;IB)V

    .line 383
    invoke-static {v2, v3, v7}, Lo/V;->e(Lo/T;Lo/T;Lo/T;)V

    .line 384
    invoke-static {v7, v5, v8}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 385
    move/from16 v0, p10

    invoke-interface {p5, v8, p4, v0}, LE/q;->a(Lo/T;IB)V

    .line 386
    invoke-static {v7, v5, v8}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 387
    move/from16 v0, p10

    invoke-interface {p5, v8, p4, v0}, LE/q;->a(Lo/T;IB)V

    .line 389
    invoke-static {v3, v5, v8}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 390
    move/from16 v0, p10

    invoke-interface {p5, v8, p4, v0}, LE/q;->a(Lo/T;IB)V

    .line 391
    invoke-static {v3, v5, v8}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 392
    move/from16 v0, p10

    invoke-interface {p5, v8, p4, v0}, LE/q;->a(Lo/T;IB)V

    .line 394
    const/4 v2, 0x6

    .line 399
    const/high16 v3, -0x4180

    invoke-virtual {v4}, Lo/T;->i()F

    move-result v4

    div-float/2addr v4, p2

    mul-float/2addr v3, v4

    const/high16 v4, 0x3f00

    add-float/2addr v3, v4

    .line 400
    const/high16 v4, 0x4780

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 401
    if-eqz p8, :cond_bc

    .line 402
    const/4 v4, 0x0

    const/high16 v5, 0x1

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v5}, LE/k;->a(II)V

    .line 403
    const/high16 v4, 0x1

    const/high16 v5, 0x1

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v5}, LE/k;->a(II)V

    .line 408
    :goto_83
    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, LE/k;->a(II)V

    .line 409
    const/high16 v4, 0x1

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, LE/k;->a(II)V

    .line 410
    if-eqz p9, :cond_ca

    .line 411
    const/4 v3, 0x0

    const/high16 v4, 0x1

    move-object/from16 v0, p6

    invoke-interface {v0, v3, v4}, LE/k;->a(II)V

    .line 412
    const/high16 v3, 0x1

    const/high16 v4, 0x1

    move-object/from16 v0, p6

    invoke-interface {v0, v3, v4}, LE/k;->a(II)V

    .line 419
    :goto_a3
    add-int/lit8 v3, v1, 0x1

    add-int/lit8 v4, v1, 0x2

    add-int/lit8 v5, v1, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v1, v3, v4, v5}, LE/e;->a(IIII)V

    .line 420
    add-int/lit8 v3, v1, 0x2

    add-int/lit8 v4, v1, 0x3

    add-int/lit8 v5, v1, 0x4

    add-int/lit8 v1, v1, 0x5

    move-object/from16 v0, p7

    invoke-interface {v0, v3, v4, v5, v1}, LE/e;->a(IIII)V

    .line 422
    return v2

    .line 405
    :cond_bc
    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, LE/k;->a(II)V

    .line 406
    const/high16 v4, 0x1

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, LE/k;->a(II)V

    goto :goto_83

    .line 414
    :cond_ca
    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, LE/k;->a(II)V

    .line 415
    const/high16 v4, 0x1

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v3}, LE/k;->a(II)V

    goto :goto_a3
.end method

.method private static b(I)[I
    .registers 3
    .parameter

    .prologue
    .line 497
    sget-object v0, Lx/h;->y:[[I

    aget-object v0, v0, p0

    if-nez v0, :cond_10

    .line 498
    sget-object v0, Lx/h;->y:[[I

    const/4 v1, 0x1

    shl-int/2addr v1, p0

    invoke-static {v1}, Lx/h;->a(I)[I

    move-result-object v1

    aput-object v1, v0, p0

    .line 501
    :cond_10
    sget-object v0, Lx/h;->y:[[I

    aget-object v0, v0, p0

    return-object v0
.end method


# virtual methods
.method public a(Lo/X;FLo/T;ILE/q;LE/k;LE/e;ZZB)I
    .registers 34
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 154
    const/4 v3, 0x0

    .line 155
    const/high16 v4, 0x3f80

    cmpg-float v4, p2, v4

    if-gez v4, :cond_8

    .line 323
    :cond_7
    :goto_7
    return v3

    .line 158
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lo/X;->b()I

    move-result v11

    .line 159
    const/4 v4, 0x2

    if-ne v11, v4, :cond_15

    .line 160
    invoke-direct/range {p0 .. p10}, Lx/h;->b(Lo/X;FLo/T;ILE/q;LE/k;LE/e;ZZB)I

    move-result v4

    add-int/2addr v3, v4

    .line 162
    goto :goto_7

    .line 163
    :cond_15
    const/4 v4, 0x2

    if-lt v11, v4, :cond_7

    .line 168
    move-object/from16 v0, p0

    iget-object v12, v0, Lx/h;->b:Lo/T;

    .line 169
    move-object/from16 v0, p0

    iget-object v13, v0, Lx/h;->c:Lo/T;

    .line 170
    move-object/from16 v0, p0

    iget-object v14, v0, Lx/h;->d:Lo/T;

    .line 171
    move-object/from16 v0, p0

    iget-object v15, v0, Lx/h;->e:Lo/T;

    .line 172
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->f:Lo/T;

    move-object/from16 v16, v0

    .line 173
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->g:Lo/T;

    move-object/from16 v17, v0

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->h:Lo/T;

    move-object/from16 v18, v0

    .line 175
    move-object/from16 v0, p0

    iget-object v6, v0, Lx/h;->i:Lo/T;

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->j:Lo/T;

    move-object/from16 v19, v0

    .line 177
    move-object/from16 v0, p0

    iget-object v5, v0, Lx/h;->k:Lo/T;

    .line 180
    invoke-interface/range {p5 .. p5}, LE/q;->a()I

    move-result v3

    .line 183
    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1, v12}, Lo/X;->a(ILo/T;Lo/T;)V

    .line 184
    const/4 v4, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1, v13}, Lo/X;->a(ILo/T;Lo/T;)V

    .line 189
    invoke-static {v13, v12, v15}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 190
    move/from16 v0, p2

    move-object/from16 v1, v17

    invoke-static {v15, v0, v1}, Lo/V;->a(Lo/T;FLo/T;)V

    .line 191
    move-object/from16 v0, v17

    invoke-static {v0, v6}, Lo/V;->a(Lo/T;Lo/T;)V

    .line 196
    invoke-static {v12, v6, v12}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 197
    move-object/from16 v0, v17

    invoke-static {v12, v0, v5}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 198
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 199
    move-object/from16 v0, v17

    invoke-static {v12, v0, v5}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 200
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 201
    invoke-static {v12, v6, v12}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 202
    move-object/from16 v0, v17

    invoke-static {v12, v0, v5}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 203
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 204
    move-object/from16 v0, v17

    invoke-static {v12, v0, v5}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 205
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 206
    sget-object v4, Lx/h;->u:[I

    move-object/from16 v0, p6

    invoke-interface {v0, v4}, LE/k;->a([I)V

    .line 207
    const/4 v9, 0x4

    .line 211
    if-eqz p8, :cond_1e5

    .line 212
    add-int/lit8 v4, v3, 0x1

    add-int/lit8 v7, v3, 0x2

    add-int/lit8 v8, v3, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v3, v4, v7, v8}, LE/e;->a(IIII)V

    .line 217
    :goto_be
    add-int/lit8 v4, v3, 0x2

    add-int/lit8 v7, v3, 0x3

    add-int/lit8 v8, v3, 0x4

    add-int/lit8 v10, v3, 0x5

    move-object/from16 v0, p7

    invoke-interface {v0, v4, v7, v8, v10}, LE/e;->a(IIII)V

    .line 218
    add-int/lit8 v8, v3, 0x4

    .line 227
    mul-float v20, p2, p2

    .line 228
    const/4 v3, 0x1

    move v10, v3

    :goto_d1
    add-int/lit8 v3, v11, -0x1

    if-ge v10, v3, :cond_206

    .line 230
    add-int/lit8 v3, v10, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1, v14}, Lo/X;->a(ILo/T;Lo/T;)V

    .line 231
    move-object/from16 v0, v16

    invoke-static {v14, v13, v0}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 232
    move-object/from16 v0, v16

    move/from16 v1, p2

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lo/V;->a(Lo/T;FLo/T;)V

    .line 233
    invoke-static/range {v15 .. v16}, Lo/V;->c(Lo/T;Lo/T;)J

    move-result-wide v3

    const-wide/16 v21, 0x0

    cmp-long v3, v3, v21

    if-lez v3, :cond_1f4

    const/4 v3, 0x1

    .line 236
    :goto_f7
    const/4 v7, 0x1

    .line 237
    invoke-static/range {v17 .. v19}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 238
    invoke-static/range {v18 .. v19}, Lo/T;->b(Lo/T;Lo/T;)F

    move-result v4

    .line 242
    const/high16 v21, 0x3f80

    cmpl-float v21, v4, v21

    if-lez v21, :cond_269

    invoke-static/range {v15 .. v16}, Lo/T;->b(Lo/T;Lo/T;)F

    move-result v21

    const/16 v22, 0x0

    cmpl-float v21, v21, v22

    if-ltz v21, :cond_269

    .line 243
    div-float v4, v20, v4

    .line 244
    move-object/from16 v0, v19

    move-object/from16 v1, v19

    invoke-static {v0, v4, v1}, Lo/T;->a(Lo/T;FLo/T;)V

    .line 250
    move-object/from16 v0, v19

    invoke-static {v13, v0, v5}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 251
    move-object/from16 v0, v19

    invoke-static {v13, v0, v6}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 255
    if-eqz v3, :cond_1f7

    move-object v4, v5

    .line 256
    :goto_125
    invoke-static {v13, v12, v4}, Lo/T;->d(Lo/T;Lo/T;Lo/T;)F

    move-result v21

    const/high16 v22, 0x3f00

    cmpg-float v21, v21, v22

    if-gez v21, :cond_269

    invoke-static {v13, v14, v4}, Lo/T;->d(Lo/T;Lo/T;Lo/T;)F

    move-result v4

    const/high16 v21, 0x3f00

    cmpg-float v4, v4, v21

    if-gez v4, :cond_269

    .line 262
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 263
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v6, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 264
    add-int/lit8 v9, v9, 0x2

    .line 265
    sget-object v4, Lx/h;->w:[I

    move-object/from16 v0, p6

    invoke-interface {v0, v4}, LE/k;->a([I)V

    .line 266
    add-int/lit8 v4, v8, 0x1

    add-int/lit8 v7, v8, 0x2

    add-int/lit8 v21, v8, 0x3

    move-object/from16 v0, p7

    move/from16 v1, v21

    invoke-interface {v0, v8, v4, v7, v1}, LE/e;->a(IIII)V

    .line 267
    add-int/lit8 v7, v8, 0x2

    .line 268
    const/4 v4, 0x0

    move v8, v4

    move v4, v7

    move v7, v9

    .line 271
    :goto_167
    if-eqz v8, :cond_265

    .line 273
    move-object/from16 v0, v17

    invoke-static {v13, v0, v5}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 274
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 275
    move-object/from16 v0, v17

    invoke-static {v13, v0, v5}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 276
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 277
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v13, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 278
    move-object/from16 v0, v18

    invoke-static {v13, v0, v5}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 279
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 280
    move-object/from16 v0, v18

    invoke-static {v13, v0, v5}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 281
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 282
    sget-object v8, Lx/h;->x:[I

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, LE/k;->a([I)V

    .line 283
    add-int/lit8 v7, v7, 0x5

    .line 284
    if-eqz v3, :cond_1fa

    .line 285
    add-int/lit8 v3, v4, 0x2

    add-int/lit8 v8, v4, 0x1

    add-int/lit8 v9, v4, 0x4

    move-object/from16 v0, p7

    invoke-interface {v0, v3, v8, v9}, LE/e;->a(III)V

    .line 289
    :goto_1c0
    add-int/lit8 v3, v4, 0x3

    add-int/lit8 v8, v4, 0x4

    add-int/lit8 v9, v4, 0x5

    add-int/lit8 v21, v4, 0x6

    move-object/from16 v0, p7

    move/from16 v1, v21

    invoke-interface {v0, v3, v8, v9, v1}, LE/e;->a(IIII)V

    .line 290
    add-int/lit8 v3, v4, 0x5

    move v4, v7

    .line 294
    :goto_1d2
    invoke-virtual/range {v17 .. v18}, Lo/T;->b(Lo/T;)V

    .line 295
    invoke-virtual/range {v15 .. v16}, Lo/T;->b(Lo/T;)V

    .line 296
    invoke-virtual {v12, v13}, Lo/T;->b(Lo/T;)V

    .line 297
    invoke-virtual {v13, v14}, Lo/T;->b(Lo/T;)V

    .line 228
    add-int/lit8 v7, v10, 0x1

    move v10, v7

    move v8, v3

    move v9, v4

    goto/16 :goto_d1

    .line 215
    :cond_1e5
    add-int/lit8 v4, v3, 0x2

    add-int/lit8 v7, v3, 0x2

    add-int/lit8 v8, v3, 0x2

    add-int/lit8 v10, v3, 0x2

    move-object/from16 v0, p7

    invoke-interface {v0, v4, v7, v8, v10}, LE/e;->a(IIII)V

    goto/16 :goto_be

    .line 233
    :cond_1f4
    const/4 v3, 0x0

    goto/16 :goto_f7

    :cond_1f7
    move-object v4, v6

    .line 255
    goto/16 :goto_125

    .line 287
    :cond_1fa
    add-int/lit8 v3, v4, 0x0

    add-int/lit8 v8, v4, 0x2

    add-int/lit8 v9, v4, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v3, v8, v9}, LE/e;->a(III)V

    goto :goto_1c0

    .line 301
    :cond_206
    move-object/from16 v0, v18

    invoke-static {v14, v0, v5}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 302
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 303
    move-object/from16 v0, v18

    invoke-static {v14, v0, v5}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 304
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 305
    move-object/from16 v0, v18

    invoke-static {v0, v6}, Lo/V;->a(Lo/T;Lo/T;)V

    .line 306
    invoke-static {v14, v6, v14}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 307
    move-object/from16 v0, v18

    invoke-static {v14, v0, v5}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 308
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 309
    move-object/from16 v0, v18

    invoke-static {v14, v0, v5}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 310
    move-object/from16 v0, p5

    move/from16 v1, p4

    move/from16 v2, p10

    invoke-interface {v0, v5, v1, v2}, LE/q;->a(Lo/T;IB)V

    .line 311
    sget-object v3, Lx/h;->v:[I

    move-object/from16 v0, p6

    invoke-interface {v0, v3}, LE/k;->a([I)V

    .line 312
    add-int/lit8 v3, v9, 0x4

    .line 314
    if-eqz p9, :cond_25e

    .line 316
    add-int/lit8 v4, v8, 0x1

    add-int/lit8 v5, v8, 0x2

    add-int/lit8 v6, v8, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v8, v4, v5, v6}, LE/e;->a(IIII)V

    goto/16 :goto_7

    .line 319
    :cond_25e
    move-object/from16 v0, p7

    invoke-interface {v0, v8, v8, v8, v8}, LE/e;->a(IIII)V

    goto/16 :goto_7

    :cond_265
    move v3, v4

    move v4, v7

    goto/16 :goto_1d2

    :cond_269
    move v4, v8

    move v8, v7

    move v7, v9

    goto/16 :goto_167
.end method

.method public a(IZI[ILE/k;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 791
    add-int/lit8 v3, p1, -0x1

    .line 793
    if-eqz p2, :cond_29

    .line 794
    const/4 v0, 0x5

    .line 798
    :goto_7
    mul-int/2addr v3, v0

    invoke-interface {p5}, LE/k;->g()I

    move-result v4

    add-int/2addr v3, v4

    invoke-interface {p5, v3}, LE/k;->c(I)V

    .line 801
    invoke-static {p3}, Lx/h;->b(I)[I

    move-result-object v3

    .line 803
    if-eqz p4, :cond_19

    array-length v4, p4

    if-ne v4, v2, :cond_2e

    .line 804
    :cond_19
    if-nez p4, :cond_2b

    .line 805
    :goto_1b
    if-ge v2, p1, :cond_3f

    .line 806
    mul-int/lit8 v4, v1, 0x5

    mul-int/lit8 v4, v4, 0x2

    mul-int/lit8 v5, v0, 0x2

    invoke-interface {p5, v3, v4, v5}, LE/k;->a([III)V

    .line 805
    add-int/lit8 v2, v2, 0x1

    goto :goto_1b

    .line 796
    :cond_29
    const/4 v0, 0x4

    goto :goto_7

    .line 804
    :cond_2b
    aget v1, p4, v1

    goto :goto_1b

    :cond_2e
    move v1, v2

    .line 811
    :goto_2f
    if-ge v1, p1, :cond_3f

    .line 812
    aget v2, p4, v1

    mul-int/lit8 v2, v2, 0x5

    mul-int/lit8 v2, v2, 0x2

    mul-int/lit8 v4, v0, 0x2

    invoke-interface {p5, v3, v2, v4}, LE/k;->a([III)V

    .line 811
    add-int/lit8 v1, v1, 0x1

    goto :goto_2f

    .line 818
    :cond_3f
    return-void
.end method

.method public a(Lo/X;FFLo/T;IIILE/q;LE/e;LE/k;)V
    .registers 46
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1058
    invoke-virtual/range {p1 .. p1}, Lo/X;->b()I

    move-result v18

    .line 1062
    const/4 v3, 0x1

    move/from16 v0, v18

    if-gt v0, v3, :cond_a

    .line 1331
    :cond_9
    return-void

    .line 1065
    :cond_a
    add-int/lit8 v3, v18, -0x1

    .line 1066
    invoke-interface/range {p8 .. p8}, LE/q;->a()I

    move-result v8

    .line 1067
    mul-int/lit8 v4, v18, 0x5

    .line 1068
    invoke-virtual/range {p1 .. p1}, Lo/X;->e()Z

    move-result v19

    .line 1071
    add-int v5, v8, v4

    move-object/from16 v0, p8

    invoke-interface {v0, v5}, LE/q;->a(I)V

    .line 1072
    if-eqz p10, :cond_29

    .line 1073
    invoke-interface/range {p10 .. p10}, LE/k;->g()I

    move-result v5

    add-int/2addr v4, v5

    move-object/from16 v0, p10

    invoke-interface {v0, v4}, LE/k;->c(I)V

    .line 1075
    :cond_29
    invoke-interface/range {p9 .. p9}, LE/e;->b()I

    move-result v4

    mul-int/lit8 v3, v3, 0x3

    mul-int/lit8 v3, v3, 0x6

    add-int/2addr v3, v4

    move-object/from16 v0, p9

    invoke-interface {v0, v3}, LE/e;->b(I)V

    .line 1086
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->b:Lo/T;

    move-object/from16 v20, v0

    .line 1087
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->c:Lo/T;

    move-object/from16 v21, v0

    .line 1088
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->d:Lo/T;

    move-object/from16 v22, v0

    .line 1089
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->e:Lo/T;

    move-object/from16 v23, v0

    .line 1090
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->f:Lo/T;

    move-object/from16 v24, v0

    .line 1091
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->g:Lo/T;

    move-object/from16 v25, v0

    .line 1092
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->h:Lo/T;

    move-object/from16 v26, v0

    .line 1095
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->l:Lo/aH;

    move-object/from16 v27, v0

    .line 1096
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->m:Lo/aH;

    move-object/from16 v28, v0

    .line 1097
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->n:Lo/aH;

    move-object/from16 v29, v0

    .line 1098
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->o:Lo/aH;

    move-object/from16 v30, v0

    .line 1099
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->p:Lo/aH;

    move-object/from16 v31, v0

    .line 1100
    move-object/from16 v0, p0

    iget-object v0, v0, Lx/h;->q:Lo/aH;

    move-object/from16 v32, v0

    .line 1104
    const/4 v6, -0x1

    .line 1105
    const/4 v5, -0x1

    .line 1106
    const/4 v7, -0x1

    .line 1107
    const/4 v4, -0x1

    .line 1115
    const/4 v10, -0x1

    .line 1119
    add-float v3, p2, p3

    .line 1120
    move/from16 v0, p6

    int-to-float v9, v0

    mul-float v9, v9, p3

    move/from16 v0, p7

    int-to-float v11, v0

    mul-float v11, v11, p2

    add-float/2addr v9, v11

    div-float v3, v9, v3

    float-to-int v0, v3

    move/from16 v33, v0

    .line 1133
    const/4 v3, 0x0

    move v11, v3

    move v12, v10

    move v13, v4

    move v14, v7

    move v4, v8

    :goto_a2
    move/from16 v0, v18

    if-ge v11, v0, :cond_9

    .line 1136
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, v21

    invoke-virtual {v0, v11, v1, v2}, Lo/X;->a(ILo/T;Lo/T;)V

    .line 1139
    add-int/lit8 v7, v11, -0x1

    .line 1140
    add-int/lit8 v3, v11, 0x1

    .line 1144
    if-eqz v19, :cond_3fe

    .line 1145
    if-gez v7, :cond_b9

    .line 1146
    add-int/lit8 v7, v18, -0x2

    .line 1148
    :cond_b9
    move/from16 v0, v18

    if-lt v3, v0, :cond_3fe

    .line 1149
    const/4 v3, 0x1

    move v8, v3

    move v3, v7

    .line 1153
    :goto_c0
    if-ltz v3, :cond_1da

    .line 1154
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, v20

    invoke-virtual {v0, v3, v1, v2}, Lo/X;->a(ILo/T;Lo/T;)V

    .line 1155
    move-object/from16 v0, p0

    iget-object v3, v0, Lx/h;->r:Lo/aH;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Lo/aH;->a(Lo/T;Lo/T;)Lo/aH;

    move-result-object v3

    move-object v7, v3

    .line 1160
    :goto_d8
    move/from16 v0, v18

    if-ge v8, v0, :cond_1de

    .line 1161
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, v22

    invoke-virtual {v0, v8, v1, v2}, Lo/X;->a(ILo/T;Lo/T;)V

    .line 1162
    move-object/from16 v0, p0

    iget-object v3, v0, Lx/h;->s:Lo/aH;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v3, v0, v1}, Lo/aH;->a(Lo/T;Lo/T;)Lo/aH;

    move-result-object v3

    move-object v8, v3

    .line 1169
    :goto_f2
    const/4 v3, 0x1

    .line 1171
    if-eqz v7, :cond_1e7

    if-eqz v8, :cond_1e7

    .line 1173
    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v9

    invoke-virtual {v9}, Lo/aH;->d()Lo/aH;

    .line 1174
    move-object/from16 v0, v28

    invoke-virtual {v0, v8}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v9

    invoke-virtual {v9}, Lo/aH;->d()Lo/aH;

    .line 1177
    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v9

    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Lo/aH;->b(Lo/aH;)Lo/aH;

    .line 1178
    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v9, v10}, Lo/aH;->b(FF)Z

    move-result v9

    if-eqz v9, :cond_1e2

    .line 1179
    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v9

    invoke-virtual {v9}, Lo/aH;->e()Lo/aH;

    move/from16 v17, v3

    .line 1193
    :goto_12d
    if-eqz v17, :cond_20f

    .line 1194
    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v3

    move/from16 v0, p2

    neg-float v7, v0

    invoke-virtual {v3, v7}, Lo/aH;->a(F)Lo/aH;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-static {v0, v3, v1}, Lo/aH;->a(Lo/T;Lo/aH;Lo/T;)Lo/T;

    .line 1195
    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lo/aH;->a(F)Lo/aH;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v3, v1}, Lo/aH;->a(Lo/T;Lo/aH;Lo/T;)Lo/T;

    .line 1197
    move-object/from16 v0, p8

    move-object/from16 v1, v23

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, LE/q;->a(Lo/T;I)V

    .line 1198
    move-object/from16 v0, p8

    move-object/from16 v1, v24

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, LE/q;->a(Lo/T;I)V

    .line 1200
    if-eqz p10, :cond_17e

    .line 1201
    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, LE/k;->a(II)V

    .line 1202
    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, LE/k;->a(II)V

    .line 1205
    :cond_17e
    add-int/lit8 v3, v4, 0x1

    .line 1206
    add-int/lit8 v7, v3, 0x1

    move v8, v5

    move v9, v6

    move v10, v7

    move v7, v3

    move v5, v3

    move v6, v4

    move v3, v4

    .line 1304
    :goto_189
    add-int/lit8 v15, v10, 0x1

    .line 1305
    move-object/from16 v0, p8

    move-object/from16 v1, v21

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, LE/q;->a(Lo/T;I)V

    .line 1306
    if-eqz p10, :cond_1a1

    .line 1307
    const/16 v16, 0x0

    move-object/from16 v0, p10

    move/from16 v1, v33

    move/from16 v2, v16

    invoke-interface {v0, v1, v2}, LE/k;->a(II)V

    .line 1312
    :cond_1a1
    if-eqz v19, :cond_3fa

    add-int/lit8 v16, v18, -0x1

    move/from16 v0, v16

    if-ne v11, v0, :cond_3fa

    const/16 v16, 0x1

    .line 1313
    :goto_1ab
    if-nez v17, :cond_1b9

    if-nez v16, :cond_1b9

    .line 1315
    move-object/from16 v0, p9

    invoke-interface {v0, v9, v10, v3}, LE/e;->a(III)V

    .line 1316
    move-object/from16 v0, p9

    invoke-interface {v0, v10, v8, v3}, LE/e;->a(III)V

    .line 1318
    :cond_1b9
    if-lez v11, :cond_1cf

    .line 1320
    move-object/from16 v0, p9

    invoke-interface {v0, v13, v12, v10}, LE/e;->a(III)V

    .line 1321
    move-object/from16 v0, p9

    invoke-interface {v0, v12, v14, v10}, LE/e;->a(III)V

    .line 1322
    move-object/from16 v0, p9

    invoke-interface {v0, v13, v10, v6}, LE/e;->a(III)V

    .line 1323
    move-object/from16 v0, p9

    invoke-interface {v0, v10, v14, v5}, LE/e;->a(III)V

    .line 1133
    :cond_1cf
    add-int/lit8 v3, v11, 0x1

    move v11, v3

    move v12, v10

    move v13, v4

    move v14, v7

    move v5, v8

    move v6, v9

    move v4, v15

    goto/16 :goto_a2

    .line 1157
    :cond_1da
    const/4 v3, 0x0

    move-object v7, v3

    goto/16 :goto_d8

    .line 1164
    :cond_1de
    const/4 v3, 0x0

    move-object v8, v3

    goto/16 :goto_f2

    .line 1181
    :cond_1e2
    const/4 v3, 0x0

    move/from16 v17, v3

    goto/16 :goto_12d

    .line 1183
    :cond_1e7
    if-eqz v7, :cond_1fa

    .line 1186
    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v9

    invoke-virtual {v9}, Lo/aH;->e()Lo/aH;

    move-result-object v9

    invoke-virtual {v9}, Lo/aH;->d()Lo/aH;

    move/from16 v17, v3

    goto/16 :goto_12d

    .line 1188
    :cond_1fa
    move-object/from16 v0, v29

    invoke-virtual {v0, v8}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v9

    invoke-virtual {v9}, Lo/aH;->e()Lo/aH;

    move-result-object v9

    invoke-virtual {v9}, Lo/aH;->d()Lo/aH;

    move-result-object v9

    invoke-virtual {v9}, Lo/aH;->a()Lo/aH;

    move/from16 v17, v3

    goto/16 :goto_12d

    .line 1213
    :cond_20f
    invoke-virtual/range {v29 .. v29}, Lo/aH;->d()Lo/aH;

    .line 1216
    invoke-virtual {v7, v8}, Lo/aH;->d(Lo/aH;)Z

    move-result v5

    .line 1219
    move-object/from16 v0, v30

    invoke-virtual {v0, v8}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v3

    invoke-virtual {v3}, Lo/aH;->e()Lo/aH;

    move-result-object v3

    invoke-virtual {v3}, Lo/aH;->d()Lo/aH;

    .line 1222
    invoke-virtual/range {v29 .. v30}, Lo/aH;->c(Lo/aH;)F

    move-result v6

    .line 1224
    if-eqz v5, :cond_33d

    move/from16 v3, p2

    :goto_22b
    neg-float v9, v6

    div-float/2addr v3, v9

    .line 1234
    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v9

    invoke-virtual {v9, v3}, Lo/aH;->a(F)Lo/aH;

    .line 1235
    invoke-virtual {v7}, Lo/aH;->c()F

    move-result v9

    .line 1236
    invoke-virtual {v8}, Lo/aH;->c()F

    move-result v10

    .line 1237
    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v15

    invoke-virtual {v15}, Lo/aH;->d()Lo/aH;

    move-result-object v15

    move-object/from16 v0, v31

    invoke-virtual {v0, v15}, Lo/aH;->c(Lo/aH;)F

    move-result v15

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    .line 1238
    move-object/from16 v0, v32

    invoke-virtual {v0, v8}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lo/aH;->d()Lo/aH;

    move-result-object v16

    move-object/from16 v0, v31

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lo/aH;->c(Lo/aH;)F

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v16

    .line 1239
    div-float/2addr v9, v15

    div-float v10, v10, v16

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    .line 1240
    const/high16 v10, 0x3f80

    cmpg-float v10, v9, v10

    if-gez v10, :cond_280

    .line 1241
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    invoke-static {v6, v9}, Ljava/lang/Math;->max(FF)F

    move-result v6

    mul-float/2addr v3, v6

    .line 1247
    :cond_280
    if-eqz v5, :cond_341

    .line 1248
    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v5

    move/from16 v0, p3

    neg-float v6, v0

    invoke-virtual {v5, v6}, Lo/aH;->a(F)Lo/aH;

    move-result-object v5

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-static {v0, v5, v1}, Lo/aH;->a(Lo/T;Lo/aH;Lo/T;)Lo/T;

    .line 1249
    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v5

    neg-float v3, v3

    invoke-virtual {v5, v3}, Lo/aH;->a(F)Lo/aH;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v3, v1}, Lo/aH;->a(Lo/T;Lo/aH;Lo/T;)Lo/T;

    .line 1250
    move-object/from16 v0, v32

    invoke-virtual {v0, v8}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v3

    invoke-virtual {v3}, Lo/aH;->e()Lo/aH;

    move-result-object v3

    invoke-virtual {v3}, Lo/aH;->d()Lo/aH;

    move-result-object v3

    move/from16 v0, p3

    neg-float v5, v0

    invoke-virtual {v3, v5}, Lo/aH;->a(F)Lo/aH;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-static {v0, v3, v1}, Lo/aH;->a(Lo/T;Lo/aH;Lo/T;)Lo/T;

    .line 1251
    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v3

    invoke-virtual {v3}, Lo/aH;->e()Lo/aH;

    move-result-object v3

    invoke-virtual {v3}, Lo/aH;->d()Lo/aH;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lo/aH;->a(F)Lo/aH;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v3, v1}, Lo/aH;->a(Lo/T;Lo/aH;Lo/T;)Lo/T;

    .line 1253
    move-object/from16 v0, p8

    move-object/from16 v1, v23

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, LE/q;->a(Lo/T;I)V

    .line 1254
    move-object/from16 v0, p8

    move-object/from16 v1, v24

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, LE/q;->a(Lo/T;I)V

    .line 1255
    move-object/from16 v0, p8

    move-object/from16 v1, v25

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, LE/q;->a(Lo/T;I)V

    .line 1256
    move-object/from16 v0, p8

    move-object/from16 v1, v26

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, LE/q;->a(Lo/T;I)V

    .line 1258
    if-eqz p10, :cond_329

    .line 1259
    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, LE/k;->a(II)V

    .line 1260
    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, LE/k;->a(II)V

    .line 1261
    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, LE/k;->a(II)V

    .line 1262
    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, LE/k;->a(II)V

    .line 1265
    :cond_329
    add-int/lit8 v3, v4, 0x1

    .line 1266
    add-int/lit8 v6, v3, 0x1

    .line 1267
    add-int/lit8 v5, v6, 0x1

    .line 1268
    add-int/lit8 v7, v5, 0x1

    move v8, v5

    move v9, v6

    move v10, v7

    move v7, v6

    move v6, v3

    move/from16 v34, v3

    move v3, v4

    move/from16 v4, v34

    .line 1273
    goto/16 :goto_189

    :cond_33d
    move/from16 v3, p3

    .line 1224
    goto/16 :goto_22b

    .line 1275
    :cond_341
    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v5

    invoke-virtual {v5, v3}, Lo/aH;->a(F)Lo/aH;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v3, v1}, Lo/aH;->a(Lo/T;Lo/aH;Lo/T;)Lo/T;

    .line 1276
    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v3

    move/from16 v0, p2

    neg-float v5, v0

    invoke-virtual {v3, v5}, Lo/aH;->a(F)Lo/aH;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-static {v0, v3, v1}, Lo/aH;->a(Lo/T;Lo/aH;Lo/T;)Lo/T;

    .line 1277
    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v3

    invoke-virtual {v3}, Lo/aH;->e()Lo/aH;

    move-result-object v3

    invoke-virtual {v3}, Lo/aH;->d()Lo/aH;

    move-result-object v3

    move/from16 v0, p2

    neg-float v5, v0

    invoke-virtual {v3, v5}, Lo/aH;->a(F)Lo/aH;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-static {v0, v3, v1}, Lo/aH;->a(Lo/T;Lo/aH;Lo/T;)Lo/T;

    .line 1278
    move-object/from16 v0, v32

    invoke-virtual {v0, v8}, Lo/aH;->a(Lo/aH;)Lo/aH;

    move-result-object v3

    invoke-virtual {v3}, Lo/aH;->e()Lo/aH;

    move-result-object v3

    invoke-virtual {v3}, Lo/aH;->d()Lo/aH;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lo/aH;->a(F)Lo/aH;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v3, v1}, Lo/aH;->a(Lo/T;Lo/aH;Lo/T;)Lo/T;

    .line 1280
    move-object/from16 v0, p8

    move-object/from16 v1, v23

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, LE/q;->a(Lo/T;I)V

    .line 1281
    move-object/from16 v0, p8

    move-object/from16 v1, v24

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, LE/q;->a(Lo/T;I)V

    .line 1282
    move-object/from16 v0, p8

    move-object/from16 v1, v25

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, LE/q;->a(Lo/T;I)V

    .line 1283
    move-object/from16 v0, p8

    move-object/from16 v1, v26

    move/from16 v2, p5

    invoke-interface {v0, v1, v2}, LE/q;->a(Lo/T;I)V

    .line 1285
    if-eqz p10, :cond_3e7

    .line 1286
    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, LE/k;->a(II)V

    .line 1287
    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p7

    invoke-interface {v0, v1, v3}, LE/k;->a(II)V

    .line 1288
    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, LE/k;->a(II)V

    .line 1289
    const/4 v3, 0x0

    move-object/from16 v0, p10

    move/from16 v1, p6

    invoke-interface {v0, v1, v3}, LE/k;->a(II)V

    .line 1292
    :cond_3e7
    add-int/lit8 v5, v4, 0x1

    .line 1293
    add-int/lit8 v6, v5, 0x1

    .line 1294
    add-int/lit8 v3, v6, 0x1

    .line 1295
    add-int/lit8 v7, v3, 0x1

    move v8, v3

    move v9, v6

    move v10, v7

    move v7, v5

    move/from16 v34, v3

    move v3, v4

    move/from16 v4, v34

    .line 1300
    goto/16 :goto_189

    .line 1312
    :cond_3fa
    const/16 v16, 0x0

    goto/16 :goto_1ab

    :cond_3fe
    move v8, v3

    move v3, v7

    goto/16 :goto_c0
.end method

.method public a(Lo/X;FLo/T;IFLE/q;LE/e;LE/k;LE/k;)V
    .registers 26
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 892
    invoke-virtual/range {p1 .. p1}, Lo/X;->b()I

    move-result v6

    .line 893
    add-int/lit8 v7, v6, -0x1

    .line 894
    invoke-interface/range {p6 .. p6}, LE/q;->a()I

    move-result v8

    .line 895
    mul-int/lit8 v2, v7, 0x4

    .line 898
    move-object/from16 v0, p0

    iget-object v5, v0, Lx/h;->b:Lo/T;

    .line 899
    move-object/from16 v0, p0

    iget-object v4, v0, Lx/h;->c:Lo/T;

    .line 900
    move-object/from16 v0, p0

    iget-object v9, v0, Lx/h;->d:Lo/T;

    .line 901
    move-object/from16 v0, p0

    iget-object v10, v0, Lx/h;->e:Lo/T;

    .line 902
    move-object/from16 v0, p0

    iget-object v11, v0, Lx/h;->f:Lo/T;

    .line 905
    invoke-interface/range {p6 .. p6}, LE/q;->a()I

    move-result v3

    add-int/2addr v3, v2

    move-object/from16 v0, p6

    invoke-interface {v0, v3}, LE/q;->a(I)V

    .line 906
    if-eqz p8, :cond_42

    .line 907
    invoke-interface/range {p8 .. p8}, LE/k;->g()I

    move-result v3

    add-int/2addr v3, v2

    move-object/from16 v0, p8

    invoke-interface {v0, v3}, LE/k;->c(I)V

    .line 908
    if-eqz p9, :cond_42

    .line 909
    invoke-interface/range {p9 .. p9}, LE/k;->g()I

    move-result v3

    add-int/2addr v2, v3

    move-object/from16 v0, p9

    invoke-interface {v0, v2}, LE/k;->c(I)V

    .line 915
    :cond_42
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Lo/X;->a(ILo/T;)V

    .line 916
    move-object/from16 v0, p3

    invoke-static {v5, v0, v5}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 917
    const/4 v3, 0x0

    .line 918
    const/4 v2, 0x1

    move v15, v2

    move v2, v3

    move v3, v15

    :goto_52
    if-ge v3, v6, :cond_e1

    .line 919
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lo/X;->a(ILo/T;)V

    .line 920
    move-object/from16 v0, p3

    invoke-static {v4, v0, v4}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 923
    invoke-static {v4, v5, v9}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 924
    move/from16 v0, p2

    neg-float v12, v0

    invoke-static {v9, v12, v10}, Lo/V;->a(Lo/T;FLo/T;)V

    .line 927
    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-interface {v0, v5, v1}, LE/q;->a(Lo/T;I)V

    .line 928
    invoke-static {v5, v10, v11}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 929
    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-interface {v0, v11, v1}, LE/q;->a(Lo/T;I)V

    .line 932
    invoke-static {v4, v10, v11}, Lo/V;->c(Lo/T;Lo/T;Lo/T;)V

    .line 933
    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-interface {v0, v11, v1}, LE/q;->a(Lo/T;I)V

    .line 934
    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-interface {v0, v4, v1}, LE/q;->a(Lo/T;I)V

    .line 936
    if-eqz p8, :cond_da

    .line 938
    invoke-virtual {v9}, Lo/T;->i()F

    move-result v12

    move/from16 v0, p4

    int-to-float v13, v0

    div-float/2addr v12, v13

    mul-float v12, v12, p5

    .line 939
    const/4 v13, 0x0

    move-object/from16 v0, p8

    invoke-interface {v0, v13, v2}, LE/k;->a(FF)V

    .line 940
    const/high16 v13, 0x3f80

    move-object/from16 v0, p8

    invoke-interface {v0, v13, v2}, LE/k;->a(FF)V

    .line 941
    if-eqz p9, :cond_b7

    .line 942
    const/4 v13, 0x0

    sget v14, Lx/h;->a:F

    mul-float/2addr v14, v2

    move-object/from16 v0, p9

    invoke-interface {v0, v13, v14}, LE/k;->a(FF)V

    .line 943
    sget v13, Lx/h;->a:F

    sget v14, Lx/h;->a:F

    mul-float/2addr v14, v2

    move-object/from16 v0, p9

    invoke-interface {v0, v13, v14}, LE/k;->a(FF)V

    .line 945
    :cond_b7
    add-float/2addr v2, v12

    .line 946
    const/high16 v12, 0x3f80

    move-object/from16 v0, p8

    invoke-interface {v0, v12, v2}, LE/k;->a(FF)V

    .line 947
    const/4 v12, 0x0

    move-object/from16 v0, p8

    invoke-interface {v0, v12, v2}, LE/k;->a(FF)V

    .line 948
    if-eqz p9, :cond_da

    .line 949
    sget v12, Lx/h;->a:F

    sget v13, Lx/h;->a:F

    mul-float/2addr v13, v2

    move-object/from16 v0, p9

    invoke-interface {v0, v12, v13}, LE/k;->a(FF)V

    .line 950
    const/4 v12, 0x0

    sget v13, Lx/h;->a:F

    mul-float/2addr v13, v2

    move-object/from16 v0, p9

    invoke-interface {v0, v12, v13}, LE/k;->a(FF)V

    .line 918
    :cond_da
    add-int/lit8 v3, v3, 0x1

    move-object v15, v5

    move-object v5, v4

    move-object v4, v15

    goto/16 :goto_52

    .line 961
    :cond_e1
    mul-int/lit8 v2, v7, 0x2

    .line 962
    add-int/lit8 v3, v7, -0x1

    .line 963
    invoke-interface/range {p7 .. p7}, LE/e;->b()I

    move-result v6

    add-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v2, v6

    .line 965
    move-object/from16 v0, p7

    invoke-interface {v0, v2}, LE/e;->b(I)V

    .line 967
    move-object/from16 v0, p0

    iget-object v3, v0, Lx/h;->d:Lo/T;

    .line 968
    move-object/from16 v0, p0

    iget-object v6, v0, Lx/h;->e:Lo/T;

    .line 969
    move-object/from16 v0, p0

    iget-object v9, v0, Lx/h;->f:Lo/T;

    .line 973
    const/4 v2, 0x0

    .line 974
    :goto_ff
    mul-int/lit8 v10, v2, 0x4

    add-int/2addr v10, v8

    .line 975
    const/4 v11, 0x0

    cmpl-float v11, p2, v11

    if-lez v11, :cond_11e

    .line 976
    add-int/lit8 v11, v10, 0x1

    add-int/lit8 v12, v10, 0x2

    move-object/from16 v0, p7

    invoke-interface {v0, v10, v11, v12}, LE/e;->a(III)V

    .line 977
    add-int/lit8 v11, v10, 0x2

    add-int/lit8 v12, v10, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v10, v11, v12}, LE/e;->a(III)V

    .line 983
    :goto_119
    add-int/lit8 v11, v7, -0x1

    if-ne v2, v11, :cond_131

    .line 1004
    return-void

    .line 979
    :cond_11e
    add-int/lit8 v11, v10, 0x2

    add-int/lit8 v12, v10, 0x1

    move-object/from16 v0, p7

    invoke-interface {v0, v10, v11, v12}, LE/e;->a(III)V

    .line 980
    add-int/lit8 v11, v10, 0x3

    add-int/lit8 v12, v10, 0x2

    move-object/from16 v0, p7

    invoke-interface {v0, v10, v11, v12}, LE/e;->a(III)V

    goto :goto_119

    .line 989
    :cond_131
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Lo/X;->a(ILo/T;)V

    .line 990
    add-int/lit8 v11, v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v4}, Lo/X;->a(ILo/T;)V

    .line 991
    add-int/lit8 v11, v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v3}, Lo/X;->a(ILo/T;)V

    .line 992
    invoke-static {v4, v5, v6}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 993
    invoke-static {v3, v4, v9}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 994
    invoke-static {v6, v9}, Lo/V;->c(Lo/T;Lo/T;)J

    move-result-wide v11

    .line 995
    long-to-float v11, v11

    mul-float v11, v11, p2

    const/4 v12, 0x0

    cmpl-float v11, v11, v12

    if-lez v11, :cond_168

    .line 996
    add-int/lit8 v11, v10, 0x4

    .line 997
    const/4 v12, 0x0

    cmpl-float v12, p2, v12

    if-lez v12, :cond_16b

    .line 998
    add-int/lit8 v12, v10, 0x3

    add-int/lit8 v10, v10, 0x2

    add-int/lit8 v11, v11, 0x1

    move-object/from16 v0, p7

    invoke-interface {v0, v12, v10, v11}, LE/e;->a(III)V

    .line 973
    :cond_168
    :goto_168
    add-int/lit8 v2, v2, 0x1

    goto :goto_ff

    .line 1000
    :cond_16b
    add-int/lit8 v12, v10, 0x2

    add-int/lit8 v10, v10, 0x3

    add-int/lit8 v11, v11, 0x1

    move-object/from16 v0, p7

    invoke-interface {v0, v12, v10, v11}, LE/e;->a(III)V

    goto :goto_168
.end method

.method public a(Lo/X;FZLo/T;IFLE/q;LE/e;LE/k;)V
    .registers 27
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 611
    invoke-virtual/range {p1 .. p1}, Lo/X;->b()I

    move-result v11

    .line 612
    add-int/lit8 v12, v11, -0x1

    .line 613
    invoke-interface/range {p7 .. p7}, LE/q;->a()I

    move-result v13

    .line 616
    if-gtz v12, :cond_d

    .line 762
    :cond_c
    :goto_c
    return-void

    .line 621
    :cond_d
    if-eqz p3, :cond_99

    .line 622
    const/4 v1, 0x5

    move v8, v1

    .line 626
    :goto_11
    mul-int v14, v8, v12

    .line 628
    move-object/from16 v0, p0

    iget-object v3, v0, Lx/h;->b:Lo/T;

    .line 629
    move-object/from16 v0, p0

    iget-object v2, v0, Lx/h;->c:Lo/T;

    .line 632
    invoke-interface/range {p7 .. p7}, LE/q;->a()I

    move-result v1

    add-int/2addr v1, v14

    move-object/from16 v0, p7

    invoke-interface {v0, v1}, LE/q;->a(I)V

    .line 636
    move-object/from16 v0, p0

    iget-object v15, v0, Lx/h;->d:Lo/T;

    .line 638
    if-eqz p9, :cond_35

    .line 639
    invoke-interface/range {p9 .. p9}, LE/k;->g()I

    move-result v1

    add-int/2addr v1, v14

    move-object/from16 v0, p9

    invoke-interface {v0, v1}, LE/k;->c(I)V

    .line 643
    :cond_35
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lo/X;->a(ILo/T;)V

    .line 644
    move-object/from16 v0, p4

    invoke-static {v2, v0, v2}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 645
    const/4 v9, 0x0

    .line 646
    const/4 v1, 0x1

    move v10, v1

    :goto_43
    if-ge v10, v11, :cond_9d

    .line 647
    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v3}, Lo/X;->a(ILo/T;)V

    .line 648
    move-object/from16 v0, p4

    invoke-static {v3, v0, v3}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    move-object/from16 v1, p0

    move/from16 v4, p2

    move/from16 v5, p5

    move/from16 v6, p3

    move-object/from16 v7, p7

    .line 649
    invoke-direct/range {v1 .. v7}, Lx/h;->a(Lo/T;Lo/T;FIZLE/q;)V

    .line 652
    if-eqz p9, :cond_1ad

    .line 654
    invoke-static {v3, v2, v15}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 656
    invoke-virtual {v15}, Lo/T;->i()F

    move-result v1

    move/from16 v0, p5

    int-to-float v4, v0

    div-float/2addr v1, v4

    mul-float v1, v1, p6

    .line 657
    const/4 v4, 0x0

    move-object/from16 v0, p9

    invoke-interface {v0, v4, v9}, LE/k;->a(FF)V

    .line 658
    const/high16 v4, 0x3f80

    move-object/from16 v0, p9

    invoke-interface {v0, v4, v9}, LE/k;->a(FF)V

    .line 659
    add-float/2addr v1, v9

    .line 660
    const/high16 v4, 0x3f80

    move-object/from16 v0, p9

    invoke-interface {v0, v4, v1}, LE/k;->a(FF)V

    .line 661
    const/4 v4, 0x0

    move-object/from16 v0, p9

    invoke-interface {v0, v4, v1}, LE/k;->a(FF)V

    .line 663
    if-eqz p3, :cond_8f

    .line 664
    const/high16 v4, 0x3f00

    move-object/from16 v0, p9

    invoke-interface {v0, v4, v1}, LE/k;->a(FF)V

    .line 646
    :cond_8f
    :goto_8f
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    move v9, v1

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    goto :goto_43

    .line 624
    :cond_99
    const/4 v1, 0x4

    move v8, v1

    goto/16 :goto_11

    .line 689
    :cond_9d
    if-eqz p8, :cond_c

    .line 690
    add-int v1, v13, v14

    .line 691
    const/16 v4, 0x7fff

    if-le v1, v4, :cond_c4

    .line 692
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " required, but we can only store "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0x7fff

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 695
    :cond_c4
    move-object/from16 v0, p0

    iget-object v5, v0, Lx/h;->d:Lo/T;

    .line 696
    move-object/from16 v0, p0

    iget-object v6, v0, Lx/h;->e:Lo/T;

    .line 697
    move-object/from16 v0, p0

    iget-object v7, v0, Lx/h;->f:Lo/T;

    .line 698
    mul-int/lit8 v4, v12, 0x2

    .line 699
    invoke-virtual/range {p1 .. p1}, Lo/X;->e()Z

    move-result v1

    if-eqz v1, :cond_105

    const/4 v1, 0x0

    :goto_d9
    sub-int v1, v12, v1

    .line 700
    if-eqz p3, :cond_107

    .line 701
    invoke-interface/range {p8 .. p8}, LE/e;->b()I

    move-result v9

    add-int/2addr v1, v4

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v1, v9

    move-object/from16 v0, p8

    invoke-interface {v0, v1}, LE/e;->b(I)V

    .line 708
    :goto_ea
    const/4 v1, 0x0

    :goto_eb
    if-ge v1, v12, :cond_114

    .line 709
    mul-int v4, v1, v8

    add-int/2addr v4, v13

    .line 710
    add-int/lit8 v9, v4, 0x1

    add-int/lit8 v10, v4, 0x2

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v9, v10}, LE/e;->a(III)V

    .line 711
    add-int/lit8 v9, v4, 0x2

    add-int/lit8 v10, v4, 0x3

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v9, v10}, LE/e;->a(III)V

    .line 708
    add-int/lit8 v1, v1, 0x1

    goto :goto_eb

    .line 699
    :cond_105
    const/4 v1, 0x1

    goto :goto_d9

    .line 704
    :cond_107
    invoke-interface/range {p8 .. p8}, LE/e;->b()I

    move-result v1

    mul-int/lit8 v4, v4, 0x3

    add-int/2addr v1, v4

    move-object/from16 v0, p8

    invoke-interface {v0, v1}, LE/e;->b(I)V

    goto :goto_ea

    .line 713
    :cond_114
    if-eqz p3, :cond_c

    .line 718
    const/4 v1, 0x0

    move v4, v1

    :goto_118
    add-int/lit8 v1, v12, -0x1

    if-ge v4, v1, :cond_162

    .line 722
    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, Lo/X;->a(ILo/T;)V

    .line 723
    add-int/lit8 v1, v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, Lo/X;->a(ILo/T;)V

    .line 724
    add-int/lit8 v1, v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, Lo/X;->a(ILo/T;)V

    .line 725
    invoke-static {v3, v2, v6}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 726
    invoke-static {v5, v3, v7}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 727
    invoke-static {v6, v7}, Lo/V;->c(Lo/T;Lo/T;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-lez v1, :cond_156

    const/4 v1, 0x1

    .line 729
    :goto_140
    mul-int/lit8 v8, v4, 0x5

    add-int/2addr v8, v13

    .line 730
    add-int/lit8 v9, v8, 0x5

    .line 732
    if-eqz v1, :cond_158

    .line 734
    add-int/lit8 v1, v8, 0x2

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v8, v8, 0x4

    move-object/from16 v0, p8

    invoke-interface {v0, v1, v9, v8}, LE/e;->a(III)V

    .line 718
    :goto_152
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_118

    .line 727
    :cond_156
    const/4 v1, 0x0

    goto :goto_140

    .line 737
    :cond_158
    add-int/lit8 v1, v8, 0x3

    add-int/lit8 v8, v8, 0x4

    move-object/from16 v0, p8

    invoke-interface {v0, v1, v8, v9}, LE/e;->a(III)V

    goto :goto_152

    .line 740
    :cond_162
    invoke-virtual/range {p1 .. p1}, Lo/X;->e()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 741
    add-int/lit8 v1, v12, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lo/X;->a(ILo/T;)V

    .line 742
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, Lo/X;->a(ILo/T;)V

    .line 743
    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, Lo/X;->a(ILo/T;)V

    .line 744
    invoke-static {v3, v2, v6}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 745
    invoke-static {v5, v3, v7}, Lo/V;->d(Lo/T;Lo/T;Lo/T;)V

    .line 746
    invoke-static {v6, v7}, Lo/V;->c(Lo/T;Lo/T;)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_1a0

    const/4 v1, 0x1

    .line 748
    :goto_18c
    add-int/lit8 v2, v12, -0x1

    mul-int/lit8 v2, v2, 0x5

    add-int/2addr v2, v13

    .line 752
    if-eqz v1, :cond_1a2

    .line 754
    add-int/lit8 v1, v2, 0x2

    add-int/lit8 v3, v13, 0x1

    add-int/lit8 v2, v2, 0x4

    move-object/from16 v0, p8

    invoke-interface {v0, v1, v3, v2}, LE/e;->a(III)V

    goto/16 :goto_c

    .line 746
    :cond_1a0
    const/4 v1, 0x0

    goto :goto_18c

    .line 757
    :cond_1a2
    add-int/lit8 v1, v2, 0x3

    add-int/lit8 v2, v2, 0x4

    move-object/from16 v0, p8

    invoke-interface {v0, v1, v2, v13}, LE/e;->a(III)V

    goto/16 :goto_c

    :cond_1ad
    move v1, v9

    goto/16 :goto_8f
.end method
