.class public LaN/bE;
.super LaN/h;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/googlenav/ui/bq;


# direct methods
.method public constructor <init>(LaN/bG;)V
    .registers 3
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1}, LaN/h;-><init>(LaN/i;)V

    .line 44
    invoke-static {}, Lcom/google/googlenav/ui/bq;->d()Lcom/google/googlenav/ui/bq;

    move-result-object v0

    iput-object v0, p0, LaN/bE;->d:Lcom/google/googlenav/ui/bq;

    .line 45
    return-void
.end method

.method private a(Lcom/google/googlenav/cd;)Lcom/google/googlenav/ui/bh;
    .registers 6
    .parameter

    .prologue
    .line 61
    new-instance v1, Ljava/util/Vector;

    const/4 v0, 0x5

    invoke-direct {v1, v0}, Ljava/util/Vector;-><init>(I)V

    .line 66
    const/4 v0, 0x0

    .line 67
    invoke-virtual {p1}, Lcom/google/googlenav/cd;->i()Ljava/lang/String;

    move-result-object v2

    .line 70
    invoke-static {v2}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2d

    .line 72
    const/4 v0, -0x1

    .line 78
    :goto_12
    invoke-virtual {p0, p1, v1}, LaN/bE;->a(Lcom/google/googlenav/cd;Ljava/util/List;)V

    .line 80
    new-instance v2, Lcom/google/googlenav/ui/bi;

    invoke-direct {v2}, Lcom/google/googlenav/ui/bi;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/bi;->a(I)Lcom/google/googlenav/ui/bi;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->d(I)Lcom/google/googlenav/ui/bi;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->a()Lcom/google/googlenav/ui/bh;

    move-result-object v0

    return-object v0

    .line 74
    :cond_2d
    invoke-static {v2, p1}, LaN/bE;->a(Ljava/lang/String;Lcom/google/googlenav/cd;)Ljava/lang/String;

    move-result-object v2

    .line 75
    sget-object v3, Lcom/google/googlenav/ui/aZ;->m:Lcom/google/googlenav/ui/aZ;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_12
.end method

.method static a(Ljava/lang/String;Lcom/google/googlenav/cd;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 132
    if-eqz p1, :cond_25

    invoke-virtual {p1}, Lcom/google/googlenav/cd;->l()I

    move-result v0

    if-lez v0, :cond_25

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-char v1, Lcom/google/googlenav/ui/bq;->bm:C

    invoke-static {v1}, Lcom/google/googlenav/ui/bq;->a(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 135
    :cond_25
    return-object p0
.end method

.method static a(Ljava/lang/String;Lcom/google/googlenav/cq;)Ljava/lang/String;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/16 v10, 0x303

    const/16 v9, 0x302

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 160
    invoke-virtual {p1}, Lcom/google/googlenav/cq;->c()Z

    move-result v0

    if-eqz v0, :cond_77

    invoke-virtual {p1}, Lcom/google/googlenav/cq;->h()Ljava/lang/String;

    move-result-object v0

    .line 163
    :goto_11
    invoke-virtual {p1}, Lcom/google/googlenav/cq;->a()Z

    move-result v3

    .line 164
    if-eqz v3, :cond_8a

    invoke-virtual {p1, v7}, Lcom/google/googlenav/cq;->d(Z)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 171
    :goto_1c
    if-eqz v3, :cond_90

    invoke-static {v10}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    :goto_22
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, ""

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    invoke-static {v1, v4}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 174
    const/16 v4, 0xa

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v1, v5, v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 175
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v1, :cond_66

    .line 176
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "..."

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v1, v5

    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    :cond_66
    if-eqz v3, :cond_95

    .line 180
    invoke-static {v10}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v2, v3, v7

    invoke-static {v1, v3}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 184
    :goto_76
    return-object v0

    .line 160
    :cond_77
    const/16 v0, 0x4dd

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/cq;->h()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_11

    .line 164
    :cond_8a
    invoke-virtual {p1, v6}, Lcom/google/googlenav/cq;->c(Z)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_1c

    .line 171
    :cond_90
    invoke-static {v9}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_22

    .line 184
    :cond_95
    invoke-static {v9}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v2, v3, v7

    invoke-static {v1, v3}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_76
.end method


# virtual methods
.method public a()Lcom/google/googlenav/ui/view/d;
    .registers 5

    .prologue
    .line 49
    iget-object v0, p0, LaN/bE;->c:LaN/i;

    invoke-virtual {v0}, LaN/i;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ct;

    .line 51
    invoke-virtual {v0}, Lcom/google/googlenav/ct;->c()B

    move-result v1

    if-nez v1, :cond_10

    .line 52
    const/4 v0, 0x0

    .line 55
    :goto_f
    return-object v0

    .line 54
    :cond_10
    invoke-virtual {v0}, Lcom/google/googlenav/ct;->au()Lcom/google/googlenav/cd;

    move-result-object v0

    invoke-direct {p0, v0}, LaN/bE;->a(Lcom/google/googlenav/cd;)Lcom/google/googlenav/ui/bh;

    move-result-object v0

    .line 55
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v1

    iget-object v2, p0, LaN/bE;->c:LaN/i;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/ui/view/e;->a(Lcom/google/googlenav/ui/bh;Lcom/google/googlenav/ui/view/c;Z)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    goto :goto_f
.end method

.method a(Lcom/google/googlenav/cd;Ljava/util/List;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 145
    .line 146
    invoke-virtual {p1, v1}, Lcom/google/googlenav/cd;->c(Z)Lcom/google/googlenav/cq;

    move-result-object v0

    .line 147
    if-nez v0, :cond_8

    .line 156
    :cond_7
    :goto_7
    return-void

    .line 150
    :cond_8
    invoke-virtual {v0, v1}, Lcom/google/googlenav/cq;->c(Z)Ljava/lang/String;

    move-result-object v1

    .line 151
    if-eqz v1, :cond_7

    .line 154
    invoke-virtual {p1}, Lcom/google/googlenav/cd;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LaN/bE;->a(Ljava/lang/String;Lcom/google/googlenav/cq;)Ljava/lang/String;

    move-result-object v0

    .line 155
    sget-object v1, Lcom/google/googlenav/ui/aZ;->h:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7
.end method
