.class public LaN/al;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:LaN/al;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()LaN/al;
    .registers 1

    .prologue
    .line 17
    sget-object v0, LaN/al;->a:LaN/al;

    return-object v0
.end method

.method public static a(LaN/al;)V
    .registers 1
    .parameter

    .prologue
    .line 25
    sput-object p0, LaN/al;->a:LaN/al;

    .line 26
    return-void
.end method


# virtual methods
.method public a(LaN/i;Lai/s;)LaN/w;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-virtual {p1}, LaN/i;->av()I

    move-result v0

    packed-switch v0, :pswitch_data_58

    .line 67
    :pswitch_7
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown Layer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :pswitch_20
    const/4 v0, 0x0

    .line 65
    :goto_21
    return-object v0

    .line 45
    :pswitch_22
    new-instance v0, LaN/aY;

    invoke-direct {v0, p1}, LaN/aY;-><init>(LaN/i;)V

    goto :goto_21

    .line 48
    :pswitch_28
    new-instance v0, LaN/aH;

    invoke-direct {v0, p1, p2}, LaN/aH;-><init>(LaN/i;Lai/s;)V

    goto :goto_21

    .line 51
    :pswitch_2e
    new-instance v0, LaN/bE;

    check-cast p1, LaN/bG;

    invoke-direct {v0, p1}, LaN/bE;-><init>(LaN/bG;)V

    goto :goto_21

    .line 54
    :pswitch_36
    new-instance v0, LaN/aj;

    invoke-direct {v0, p1}, LaN/aj;-><init>(LaN/i;)V

    goto :goto_21

    .line 57
    :pswitch_3c
    new-instance v0, LaN/A;

    invoke-direct {v0, p1}, LaN/A;-><init>(LaN/i;)V

    goto :goto_21

    .line 60
    :pswitch_42
    new-instance v0, LaN/R;

    invoke-direct {v0, p1}, LaN/R;-><init>(LaN/i;)V

    goto :goto_21

    .line 63
    :pswitch_48
    new-instance v0, LaN/E;

    check-cast p1, LaN/O;

    invoke-direct {v0, p1}, LaN/E;-><init>(LaN/O;)V

    goto :goto_21

    .line 65
    :pswitch_50
    new-instance v0, LaN/bI;

    check-cast p1, LaN/bJ;

    invoke-direct {v0, p1}, LaN/bI;-><init>(LaN/bJ;)V

    goto :goto_21

    .line 30
    :pswitch_data_58
    .packed-switch 0x0
        :pswitch_22
        :pswitch_48
        :pswitch_22
        :pswitch_42
        :pswitch_7
        :pswitch_20
        :pswitch_22
        :pswitch_22
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_22
        :pswitch_7
        :pswitch_3c
        :pswitch_7
        :pswitch_36
        :pswitch_22
        :pswitch_7
        :pswitch_22
        :pswitch_20
        :pswitch_7
        :pswitch_50
        :pswitch_20
        :pswitch_22
        :pswitch_2e
        :pswitch_7
        :pswitch_28
    .end packed-switch
.end method
