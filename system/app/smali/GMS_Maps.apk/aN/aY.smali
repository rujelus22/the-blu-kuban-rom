.class public LaN/aY;
.super LaN/h;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/googlenav/ui/bq;

.field private e:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(LaN/i;)V
    .registers 3
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1}, LaN/h;-><init>(LaN/i;)V

    .line 70
    invoke-static {}, Lcom/google/googlenav/ui/bq;->d()Lcom/google/googlenav/ui/bq;

    move-result-object v0

    iput-object v0, p0, LaN/aY;->d:Lcom/google/googlenav/ui/bq;

    .line 71
    return-void
.end method

.method private a(I)I
    .registers 3
    .parameter

    .prologue
    .line 325
    sparse-switch p1, :sswitch_data_10

    .line 335
    const/4 v0, 0x0

    :goto_4
    return v0

    .line 327
    :sswitch_5
    const/4 v0, 0x4

    goto :goto_4

    .line 329
    :sswitch_7
    const/16 v0, 0x45

    goto :goto_4

    .line 331
    :sswitch_a
    const/4 v0, 0x1

    goto :goto_4

    .line 333
    :sswitch_c
    const/16 v0, 0x39

    goto :goto_4

    .line 325
    nop

    :sswitch_data_10
    .sparse-switch
        0x10 -> :sswitch_a
        0x258 -> :sswitch_c
        0x25c -> :sswitch_7
        0x262 -> :sswitch_5
    .end sparse-switch
.end method

.method static synthetic a(LaN/aY;I)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0, p1}, LaN/aY;->a(I)I

    move-result v0

    return v0
.end method

.method static a(Ljava/lang/String;LaN/i;)Lcom/google/googlenav/ui/ba;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 448
    new-instance v0, LaN/bb;

    invoke-direct {v0, p1}, LaN/bb;-><init>(LaN/i;)V

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aZ;->e:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Ljava/lang/Integer;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, -0x1

    .line 264
    invoke-virtual {p2}, Lcom/google/googlenav/settings/e;->d()I

    move-result v0

    .line 265
    sget-object v4, LaN/ba;->a:[I

    invoke-virtual {p2}, Lcom/google/googlenav/settings/e;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_52

    .line 290
    const/4 v0, 0x0

    .line 292
    :goto_13
    return-object v0

    .line 269
    :pswitch_14
    iget-object v2, p0, LaN/aY;->c:LaN/i;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()Lau/B;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, LaN/i;->a(Lau/B;I)Z

    move-result v2

    .line 271
    if-nez v2, :cond_21

    move v0, v1

    .line 292
    :cond_21
    :goto_21
    :pswitch_21
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_13

    .line 276
    :pswitch_26
    iget-object v4, p0, LaN/aY;->c:LaN/i;

    invoke-virtual {v4}, LaN/i;->ba()Lcom/google/googlenav/ui/v;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/ui/v;->ad()Lcom/google/googlenav/aA;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/googlenav/aA;->e()Z

    move-result v4

    if-eqz v4, :cond_44

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_44

    .line 278
    :goto_40
    if-nez v2, :cond_21

    move v0, v1

    .line 279
    goto :goto_21

    :cond_44
    move v2, v3

    .line 276
    goto :goto_40

    .line 284
    :pswitch_46
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bq()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_50

    .line 285
    :goto_4c
    if-nez v2, :cond_21

    move v0, v1

    .line 286
    goto :goto_21

    :cond_50
    move v2, v3

    .line 284
    goto :goto_4c

    .line 265
    :pswitch_data_52
    .packed-switch 0x1
        :pswitch_21
        :pswitch_14
        :pswitch_26
        :pswitch_46
    .end packed-switch
.end method

.method static a(Ljava/lang/String;Lcom/google/googlenav/cd;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 455
    if-eqz p1, :cond_25

    invoke-virtual {p1}, Lcom/google/googlenav/cd;->l()I

    move-result v0

    if-lez v0, :cond_25

    .line 456
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-char v1, Lcom/google/googlenav/ui/bq;->bm:C

    invoke-static {v1}, Lcom/google/googlenav/ui/bq;->a(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 458
    :cond_25
    return-object p0
.end method

.method static a(Ljava/lang/String;Lcom/google/googlenav/cq;)Ljava/lang/String;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/16 v10, 0x303

    const/16 v9, 0x302

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 488
    invoke-virtual {p1}, Lcom/google/googlenav/cq;->c()Z

    move-result v0

    if-eqz v0, :cond_77

    invoke-virtual {p1}, Lcom/google/googlenav/cq;->h()Ljava/lang/String;

    move-result-object v0

    .line 491
    :goto_11
    invoke-virtual {p1}, Lcom/google/googlenav/cq;->a()Z

    move-result v3

    .line 492
    if-eqz v3, :cond_8a

    invoke-virtual {p1, v7}, Lcom/google/googlenav/cq;->d(Z)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 499
    :goto_1c
    if-eqz v3, :cond_90

    invoke-static {v10}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    :goto_22
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, ""

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    invoke-static {v1, v4}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 502
    const/16 v4, 0xa

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v1, v5, v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 503
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v1, :cond_66

    .line 504
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "..."

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v1, v5

    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 507
    :cond_66
    if-eqz v3, :cond_95

    .line 508
    invoke-static {v10}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v2, v3, v7

    invoke-static {v1, v3}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 512
    :goto_76
    return-object v0

    .line 488
    :cond_77
    const/16 v0, 0x4dd

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/cq;->h()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_11

    .line 492
    :cond_8a
    invoke-virtual {p1, v6}, Lcom/google/googlenav/cq;->c(Z)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_1c

    .line 499
    :cond_90
    invoke-static {v9}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_22

    .line 512
    :cond_95
    invoke-static {v9}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v2, v3, v7

    invoke-static {v1, v3}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_76
.end method

.method private b(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Lcom/google/googlenav/ui/bj;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 303
    invoke-direct {p0, p1, p2}, LaN/aY;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Ljava/lang/Integer;

    move-result-object v0

    .line 304
    if-nez v0, :cond_8

    .line 305
    const/4 v0, 0x0

    .line 314
    :goto_7
    return-object v0

    .line 309
    :cond_8
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_31

    .line 310
    iget-object v1, p0, LaN/aY;->d:Lcom/google/googlenav/ui/bq;

    invoke-virtual {p2}, Lcom/google/googlenav/settings/e;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/bq;->c(I)LT/f;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/view/a;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, LaN/aY;->c:LaN/i;

    invoke-virtual {v3}, LaN/i;->ar()Lcom/google/googlenav/F;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/F;->c()I

    move-result v3

    invoke-direct {v2, v0, v3}, Lcom/google/googlenav/ui/view/a;-><init>(II)V

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bj;->a(LT/f;Lcom/google/googlenav/ui/view/J;)Lcom/google/googlenav/ui/bj;

    move-result-object v0

    goto :goto_7

    .line 314
    :cond_31
    iget-object v0, p0, LaN/aY;->d:Lcom/google/googlenav/ui/bq;

    invoke-virtual {p2}, Lcom/google/googlenav/settings/e;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bq;->c(I)LT/f;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bj;->a(LT/f;)Lcom/google/googlenav/ui/bj;

    move-result-object v0

    goto :goto_7
.end method

.method private b(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/bi;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 346
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aH()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-nez v0, :cond_34

    .line 347
    :cond_12
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->av()Lcom/google/googlenav/settings/e;

    move-result-object v0

    .line 348
    invoke-direct {p0, p1, v0}, LaN/aY;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Lcom/google/googlenav/ui/bj;

    move-result-object v0

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 367
    :goto_21
    new-instance v2, LaN/aZ;

    invoke-direct {v2, p0}, LaN/aZ;-><init>(LaN/aY;)V

    .line 382
    if-eqz v1, :cond_2b

    .line 383
    invoke-virtual {p2, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/bj;)Lcom/google/googlenav/ui/bi;

    .line 385
    :cond_2b
    if-eqz v0, :cond_30

    .line 386
    invoke-virtual {p2, v0}, Lcom/google/googlenav/ui/bi;->b(Lcom/google/googlenav/ui/bj;)Lcom/google/googlenav/ui/bi;

    .line 388
    :cond_30
    invoke-virtual {p2, v2}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/bi;

    .line 389
    return-void

    .line 351
    :cond_34
    sget-object v0, Lcom/google/googlenav/settings/e;->a:Lcom/google/googlenav/settings/e;

    invoke-direct {p0, p1, v0}, LaN/aY;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Lcom/google/googlenav/ui/bj;

    move-result-object v2

    .line 352
    sget-object v0, Lcom/google/googlenav/settings/e;->c:Lcom/google/googlenav/settings/e;

    invoke-direct {p0, p1, v0}, LaN/aY;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Lcom/google/googlenav/ui/bj;

    move-result-object v3

    .line 353
    if-eqz v2, :cond_51

    iget-object v0, v2, Lcom/google/googlenav/ui/bj;->b:Lcom/google/googlenav/ui/view/J;

    if-eqz v0, :cond_51

    iget-object v0, v2, Lcom/google/googlenav/ui/bj;->b:Lcom/google/googlenav/ui/view/J;

    check-cast v0, Lcom/google/googlenav/ui/view/a;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->a()I

    move-result v0

    if-ne v0, v4, :cond_51

    move-object v2, v1

    .line 357
    :cond_51
    if-eqz v3, :cond_6a

    iget-object v0, v3, Lcom/google/googlenav/ui/bj;->b:Lcom/google/googlenav/ui/view/J;

    if-eqz v0, :cond_6a

    iget-object v0, v3, Lcom/google/googlenav/ui/bj;->b:Lcom/google/googlenav/ui/view/J;

    check-cast v0, Lcom/google/googlenav/ui/view/a;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->a()I

    move-result v0

    if-ne v0, v4, :cond_6a

    move-object v0, v1

    .line 361
    :goto_62
    if-nez v2, :cond_68

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 363
    goto :goto_21

    :cond_68
    move-object v1, v2

    goto :goto_21

    :cond_6a
    move-object v0, v3

    goto :goto_62
.end method

.method private c()Landroid/view/LayoutInflater;
    .registers 3

    .prologue
    .line 595
    iget-object v0, p0, LaN/aY;->e:Landroid/view/LayoutInflater;

    if-nez v0, :cond_14

    .line 596
    iget-object v0, p0, LaN/aY;->d:Lcom/google/googlenav/ui/bq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bq;->O()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, LaN/aY;->e:Landroid/view/LayoutInflater;

    .line 599
    :cond_14
    iget-object v0, p0, LaN/aY;->e:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/google/googlenav/ai;)LaR/F;
    .registers 3
    .parameter

    .prologue
    .line 112
    invoke-virtual {p0}, LaN/aY;->b()LaN/m;

    move-result-object v0

    invoke-virtual {v0}, LaN/m;->bh()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bx()Z

    move-result v0

    if-nez v0, :cond_16

    .line 114
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bJ;->a(Lcom/google/googlenav/ai;Z)Lcom/google/googlenav/ui/bJ;

    move-result-object v0

    .line 116
    :goto_15
    return-object v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public a()Lcom/google/googlenav/ui/view/d;
    .registers 6

    .prologue
    .line 75
    iget-object v0, p0, LaN/aY;->c:LaN/i;

    invoke-virtual {v0}, LaN/i;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 77
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->c()B

    move-result v1

    if-nez v1, :cond_10

    .line 78
    const/4 v0, 0x0

    .line 82
    :goto_f
    return-object v0

    .line 80
    :cond_10
    invoke-virtual {p0, v0}, LaN/aY;->d(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/bh;

    move-result-object v1

    .line 81
    invoke-virtual {p0, v0}, LaN/aY;->c(Lcom/google/googlenav/ai;)Landroid/view/View;

    move-result-object v0

    .line 82
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v2

    iget-object v3, p0, LaN/aY;->c:LaN/i;

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4, v0}, Lcom/google/googlenav/ui/view/e;->a(Lcom/google/googlenav/ui/bh;Lcom/google/googlenav/ui/view/c;ZLandroid/view/View;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    goto :goto_f
.end method

.method protected a(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/bi;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 249
    invoke-direct {p0, p1, p2}, LaN/aY;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/bi;)V

    .line 250
    return-void
.end method

.method a(Lcom/google/googlenav/ai;Ljava/util/List;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 467
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->au()Lcom/google/googlenav/cd;

    move-result-object v0

    .line 468
    if-nez v0, :cond_8

    .line 484
    :cond_7
    :goto_7
    return-void

    .line 474
    :cond_8
    invoke-virtual {v0, v1}, Lcom/google/googlenav/cd;->c(Z)Lcom/google/googlenav/cq;

    move-result-object v0

    .line 475
    if-eqz v0, :cond_7

    .line 478
    invoke-virtual {v0, v1}, Lcom/google/googlenav/cq;->c(Z)Ljava/lang/String;

    move-result-object v1

    .line 479
    if-eqz v1, :cond_7

    .line 482
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ak()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LaN/aY;->a(Ljava/lang/String;Lcom/google/googlenav/cq;)Ljava/lang/String;

    move-result-object v0

    .line 483
    sget-object v1, Lcom/google/googlenav/ui/aZ;->h:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7
.end method

.method protected a(Lcom/google/googlenav/ai;Ljava/util/Vector;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 199
    invoke-virtual {p0}, LaN/aY;->b()LaN/m;

    move-result-object v1

    .line 200
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-eqz v0, :cond_27

    move-object v0, p1

    .line 201
    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_27

    move-object v0, p1

    .line 202
    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LaN/aY;->c:LaN/i;

    invoke-static {v0, v2}, LaN/aY;->a(Ljava/lang/String;LaN/i;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 207
    :cond_27
    invoke-virtual {v1}, LaN/m;->bg()Z

    move-result v0

    if-eqz v0, :cond_83

    .line 208
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ai()Z

    move-result v0

    if-eqz v0, :cond_42

    .line 209
    const/16 v0, 0x202

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aZ;->h:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 213
    :cond_42
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bw()Z

    move-result v0

    if-eqz v0, :cond_57

    .line 214
    const/16 v0, 0x5e4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aZ;->h:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 218
    :cond_57
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bv()Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 219
    const/16 v0, 0x1df

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aZ;->h:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 224
    :cond_6c
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->y()Z

    move-result v0

    if-eqz v0, :cond_83

    .line 225
    invoke-virtual {v1}, LaN/m;->bm()Z

    move-result v0

    invoke-static {p1, v0}, LaN/aR;->c(Lcom/google/googlenav/ai;Z)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aZ;->h:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 231
    :cond_83
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bx()Z

    move-result v0

    if-eqz v0, :cond_98

    .line 232
    const/16 v0, 0x3b1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aZ;->br:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 236
    :cond_98
    invoke-virtual {p0, p1, p2}, LaN/aY;->a(Lcom/google/googlenav/ai;Ljava/util/List;)V

    .line 237
    return-void
.end method

.method protected final b()LaN/m;
    .registers 2

    .prologue
    .line 253
    iget-object v0, p0, LaN/aY;->c:LaN/i;

    check-cast v0, LaN/m;

    return-object v0
.end method

.method protected b(Lcom/google/googlenav/ai;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 185
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ak()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final c(Lcom/google/googlenav/ai;)Landroid/view/View;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 92
    invoke-virtual {p0, p1}, LaN/aY;->a(Lcom/google/googlenav/ai;)LaR/F;

    move-result-object v1

    .line 93
    if-eqz v1, :cond_1c

    .line 94
    invoke-direct {p0}, LaN/aY;->c()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-interface {v1}, LaR/F;->b()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 95
    invoke-interface {v1, v0}, LaR/F;->a(Landroid/view/View;)LaR/bE;

    move-result-object v2

    .line 96
    iget-object v3, p0, LaN/aY;->c:LaN/i;

    invoke-interface {v1, v3, v2}, LaR/F;->a(Lcom/google/googlenav/ui/g;LaR/bE;)V

    .line 99
    :cond_1c
    return-object v0
.end method

.method d(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/bh;
    .registers 8
    .parameter

    .prologue
    .line 123
    new-instance v2, Ljava/util/Vector;

    const/4 v0, 0x5

    invoke-direct {v2, v0}, Ljava/util/Vector;-><init>(I)V

    .line 128
    const/4 v0, 0x0

    .line 129
    new-instance v3, Lcom/google/googlenav/ui/bi;

    invoke-direct {v3}, Lcom/google/googlenav/ui/bi;-><init>()V

    .line 131
    invoke-virtual {p0, p1}, LaN/aY;->b(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    .line 133
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v4

    if-eqz v4, :cond_33

    .line 137
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/K;->aH()Z

    move-result v4

    if-nez v4, :cond_6c

    .line 138
    const/16 v4, 0x567

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/googlenav/ui/aZ;->ad:Lcom/google/googlenav/ui/aZ;

    invoke-static {v4, v5}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 140
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/bi;->b(Lcom/google/googlenav/ui/ba;)Lcom/google/googlenav/ui/bi;

    .line 149
    :cond_33
    :goto_33
    invoke-static {v1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7c

    .line 151
    const/4 v0, -0x1

    .line 157
    :goto_3a
    invoke-virtual {p0, p1, v2}, LaN/aY;->a(Lcom/google/googlenav/ai;Ljava/util/Vector;)V

    .line 159
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-eqz v1, :cond_8e

    const/4 v1, 0x1

    .line 162
    :goto_44
    invoke-virtual {v3, v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/bi;->a(I)Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->d(I)Lcom/google/googlenav/ui/bi;

    .line 167
    invoke-virtual {p0}, LaN/aY;->b()LaN/m;

    move-result-object v0

    invoke-virtual {v0}, LaN/m;->bh()Z

    move-result v0

    if-eqz v0, :cond_64

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bx()Z

    move-result v0

    if-nez v0, :cond_64

    .line 169
    sget-object v0, Lcom/google/googlenav/ui/bh;->g:Ljava/lang/Object;

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/Object;)Lcom/google/googlenav/ui/bi;

    .line 172
    :cond_64
    invoke-virtual {p0, p1, v3}, LaN/aY;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/bi;)V

    .line 174
    invoke-virtual {v3}, Lcom/google/googlenav/ui/bi;->a()Lcom/google/googlenav/ui/bh;

    move-result-object v0

    return-object v0

    .line 142
    :cond_6c
    const/16 v4, 0x34

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/googlenav/ui/aZ;->ad:Lcom/google/googlenav/ui/aZ;

    invoke-static {v4, v5}, Lcom/google/googlenav/ui/ba;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/bi;->b(Lcom/google/googlenav/ui/ba;)Lcom/google/googlenav/ui/bi;

    goto :goto_33

    .line 153
    :cond_7c
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->au()Lcom/google/googlenav/cd;

    move-result-object v4

    invoke-static {v1, v4}, LaN/aY;->a(Ljava/lang/String;Lcom/google/googlenav/cd;)Ljava/lang/String;

    move-result-object v1

    .line 154
    sget-object v4, Lcom/google/googlenav/ui/aZ;->m:Lcom/google/googlenav/ui/aZ;

    invoke-static {v1, v4}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_3a

    .line 159
    :cond_8e
    const/16 v1, 0x12

    goto :goto_44
.end method
