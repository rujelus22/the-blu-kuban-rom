.class public LaN/aI;
.super LaN/bj;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/dialog/bn;


# instance fields
.field private E:Lcom/google/googlenav/ui/view/dialog/bl;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;Lcom/google/googlenav/ui/view/dialog/bl;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 69
    const/4 v7, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, LaN/bj;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;I)V

    .line 71
    iput-object p7, p0, LaN/aI;->E:Lcom/google/googlenav/ui/view/dialog/bl;

    .line 72
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;Lau/k;Lcom/google/googlenav/ui/view/dialog/bl;)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 86
    const/4 v9, 0x6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v9}, LaN/bj;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;Lau/k;I)V

    .line 94
    move-object/from16 v0, p8

    iput-object v0, p0, LaN/aI;->E:Lcom/google/googlenav/ui/view/dialog/bl;

    .line 95
    return-void
.end method

.method static synthetic a(LaN/aI;)V
    .registers 1
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, LaN/aI;->bV()V

    return-void
.end method

.method private bV()V
    .registers 5

    .prologue
    .line 176
    invoke-virtual {p0}, LaN/aI;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 177
    iget-object v1, p0, LaN/aI;->y:Lai/s;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aE()[Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, LaN/aK;

    invoke-direct {v3, p0}, LaN/aK;-><init>(LaN/aI;)V

    iget-object v0, p0, LaN/aI;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lai/p;

    invoke-virtual {v1, v2, v3, v0}, Lai/s;->a(Ljava/lang/Iterable;Lcom/google/common/base/D;Lai/p;)V

    .line 184
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/view/dialog/bk;)V
    .registers 3
    .parameter

    .prologue
    .line 247
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->e()V

    .line 248
    invoke-virtual {p0}, LaN/aI;->h()V

    .line 249
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 188
    invoke-virtual {p0}, LaN/aI;->bP()Lcom/google/googlenav/aZ;

    .line 189
    sparse-switch p1, :sswitch_data_28

    .line 203
    invoke-super {p0, p1, p2, p3}, LaN/bj;->a(IILjava/lang/Object;)Z

    move-result v0

    :goto_b
    return v0

    .line 193
    :sswitch_c
    const/4 v1, 0x0

    iput-boolean v1, p0, LaN/aI;->B:Z

    .line 194
    iput p2, p0, LaN/aI;->D:I

    .line 195
    const-string v1, "o"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, LaN/aI;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-virtual {p0, p2}, LaN/aI;->j(I)V

    goto :goto_b

    .line 199
    :sswitch_1c
    iget-object v1, p0, LaN/aI;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->av()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v1

    iget-object v2, p0, LaN/aI;->r:Lcom/google/googlenav/ui/view/android/aY;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/jt;->a(Lcom/google/googlenav/ui/view/android/aY;)V

    goto :goto_b

    .line 189
    :sswitch_data_28
    .sparse-switch
        0x2bc -> :sswitch_c
        0x76f -> :sswitch_1c
    .end sparse-switch
.end method

.method public aC()Z
    .registers 2

    .prologue
    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method public aF()I
    .registers 3

    .prologue
    .line 221
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 224
    iget-object v0, p0, LaN/aI;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_17

    const v0, 0x7f110010

    .line 227
    :goto_16
    return v0

    .line 224
    :cond_17
    const v0, 0x7f110011

    goto :goto_16

    .line 227
    :cond_1b
    invoke-super {p0}, LaN/bj;->aF()I

    move-result v0

    goto :goto_16
.end method

.method protected ap()V
    .registers 4

    .prologue
    .line 136
    iget-object v0, p0, LaN/aI;->E:Lcom/google/googlenav/ui/view/dialog/bl;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bl;->a(Lcom/google/googlenav/ui/view/dialog/bn;)Lcom/google/googlenav/ui/view/dialog/bk;

    move-result-object v0

    .line 137
    invoke-virtual {p0}, LaN/aI;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/ai;I)V

    .line 138
    invoke-virtual {p0}, LaN/aI;->bK()V

    .line 139
    iput-object v0, p0, LaN/aI;->r:Lcom/google/googlenav/ui/view/android/aY;

    .line 140
    return-void
.end method

.method protected aq()V
    .registers 2

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LaN/aI;->d(Z)V

    .line 146
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bo;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bo;-><init>(LaN/aI;)V

    iput-object v0, p0, LaN/aI;->r:Lcom/google/googlenav/ui/view/android/aY;

    .line 154
    invoke-virtual {p0}, LaN/aI;->bF()V

    .line 155
    return-void
.end method

.method public av()I
    .registers 2

    .prologue
    .line 99
    const/16 v0, 0x1a

    return v0
.end method

.method protected b(ILjava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 240
    invoke-super {p0, p1, p2}, LaN/bj;->b(ILjava/lang/Object;)V

    .line 241
    invoke-virtual {p0}, LaN/aI;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/common/offerslib/x;->b:Lcom/google/android/apps/common/offerslib/x;

    invoke-virtual {v1}, Lcom/google/android/apps/common/offerslib/x;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/offers/i;->a(Lcom/google/googlenav/aZ;Ljava/lang/String;)V

    .line 242
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .registers 3
    .parameter

    .prologue
    .line 254
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->e()V

    .line 255
    invoke-virtual {p0}, LaN/aI;->h()V

    .line 256
    const/4 v0, 0x1

    return v0
.end method

.method protected bF()V
    .registers 3

    .prologue
    .line 160
    iget-object v0, p0, LaN/aI;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ak()LZ/c;

    move-result-object v0

    .line 161
    if-eqz v0, :cond_10

    .line 162
    new-instance v1, LaN/aJ;

    invoke-direct {v1, p0, v0}, LaN/aJ;-><init>(LaN/aI;LZ/c;)V

    invoke-virtual {v1}, LaN/aJ;->g()V

    .line 169
    :cond_10
    return-void
.end method

.method protected bG()Lcom/google/googlenav/aZ;
    .registers 4

    .prologue
    .line 213
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-virtual {p0}, LaN/aI;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->j(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    .line 216
    invoke-virtual {p0}, LaN/aI;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {p0}, LaN/aI;->bP()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->aq()I

    move-result v2

    invoke-virtual {v1, v0, v2, p0}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/bf;ILcom/google/googlenav/bb;)Lcom/google/googlenav/aZ;

    move-result-object v0

    return-object v0
.end method

.method public bp()V
    .registers 4

    .prologue
    .line 266
    iget-object v0, p0, LaN/aI;->r:Lcom/google/googlenav/ui/view/android/aY;

    if-eqz v0, :cond_10

    invoke-virtual {p0}, LaN/aI;->af()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p0}, LaN/aI;->bA()Z

    move-result v0

    if-nez v0, :cond_11

    .line 275
    :cond_10
    :goto_10
    return-void

    .line 269
    :cond_11
    iget-object v0, p0, LaN/aI;->r:Lcom/google/googlenav/ui/view/android/aY;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/dialog/bk;

    if-eqz v0, :cond_24

    .line 270
    iget-object v0, p0, LaN/aI;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bk;

    .line 271
    invoke-virtual {p0}, LaN/aI;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/ai;I)V

    goto :goto_10

    .line 273
    :cond_24
    invoke-super {p0}, LaN/bj;->bp()V

    goto :goto_10
.end method

.method protected c(ILjava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 233
    invoke-super {p0, p1, p2}, LaN/bj;->c(ILjava/lang/Object;)V

    .line 234
    iget-object v0, p0, LaN/aI;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->aA()Lcom/google/googlenav/offers/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/offers/a;->e()V

    .line 235
    invoke-static {}, Lcom/google/googlenav/offers/k;->h()Lcom/google/googlenav/offers/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/offers/k;->d()V

    .line 236
    return-void
.end method

.method protected c(Lcom/google/googlenav/aZ;)Z
    .registers 3
    .parameter

    .prologue
    .line 131
    const/4 v0, 0x1

    return v0
.end method

.method public c(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .registers 3
    .parameter

    .prologue
    .line 261
    const/4 v0, 0x0

    return v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 104
    const/16 v0, 0xb

    return v0
.end method

.method protected f()Z
    .registers 2

    .prologue
    .line 114
    const/4 v0, 0x1

    return v0
.end method

.method protected k(Z)Z
    .registers 3
    .parameter

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method
