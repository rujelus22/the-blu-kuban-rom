.class public LaN/az;
.super LaN/i;
.source "SourceFile"


# instance fields
.field private A:Lcom/google/googlenav/ui/android/M;

.field private u:Z

.field private v:[Lcom/google/googlenav/ui/aL;

.field private w:Ljava/util/List;

.field private x:I

.field private y:Lcom/google/googlenav/ui/view/d;

.field private z:Lcom/google/googlenav/ui/view/d;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 101
    new-instance v5, Lcom/google/googlenav/n;

    new-instance v0, LaN/aD;

    invoke-direct {v0}, LaN/aD;-><init>()V

    new-instance v1, LaN/aD;

    invoke-direct {v1}, LaN/aD;-><init>()V

    invoke-direct {v5, v0, v1}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LaN/i;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/F;)V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/az;->u:Z

    .line 103
    invoke-direct {p0}, LaN/az;->be()V

    .line 104
    invoke-direct {p0}, LaN/az;->bf()V

    .line 105
    return-void
.end method

.method static synthetic a(LaN/az;)V
    .registers 1
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, LaN/az;->bf()V

    return-void
.end method

.method private a(Lau/B;Lau/B;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 236
    if-nez p1, :cond_27

    .line 237
    invoke-static {p2}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 244
    :goto_6
    iget-object v1, p0, LaN/az;->w:Ljava/util/List;

    .line 246
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 248
    new-instance v3, Laf/b;

    new-instance v4, LaN/aC;

    invoke-direct {v4, p0, v2, v1}, LaN/aC;-><init>(LaN/az;ILjava/util/List;)V

    invoke-direct {v3, v0, v4}, Laf/b;-><init>(Ljava/util/List;Laf/c;)V

    .line 278
    invoke-static {}, Lad/h;->a()Lad/h;

    move-result-object v0

    invoke-virtual {v0, v3}, Lad/h;->c(Lad/g;)V

    .line 279
    return-void

    .line 239
    :cond_27
    invoke-static {p1, p2}, Laf/d;->a(Lau/B;Lau/B;)Ljava/util/List;

    move-result-object v0

    goto :goto_6
.end method

.method static synthetic b(LaN/az;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, LaN/az;->w:Ljava/util/List;

    return-object v0
.end method

.method private be()V
    .registers 2

    .prologue
    .line 108
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/az;->w:Ljava/util/List;

    .line 109
    const/4 v0, 0x0

    iput v0, p0, LaN/az;->x:I

    .line 110
    return-void
.end method

.method private bf()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 117
    iget-boolean v0, p0, LaN/az;->u:Z

    if-eqz v0, :cond_1a

    .line 118
    invoke-direct {p0}, LaN/az;->bg()V

    .line 120
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/16 v1, 0xa

    new-instance v2, LaN/aA;

    invoke-direct {v2, p0}, LaN/aA;-><init>(LaN/az;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, LaN/az;->z:Lcom/google/googlenav/ui/view/d;

    .line 148
    :goto_19
    return-void

    .line 132
    :cond_1a
    invoke-direct {p0}, LaN/az;->bh()V

    .line 134
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/16 v1, 0x9

    new-instance v2, LaN/aB;

    invoke-direct {v2, p0}, LaN/aB;-><init>(LaN/az;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, LaN/az;->y:Lcom/google/googlenav/ui/view/d;

    goto :goto_19
.end method

.method private bg()V
    .registers 2

    .prologue
    .line 151
    iget-object v0, p0, LaN/az;->y:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_c

    .line 152
    iget-object v0, p0, LaN/az;->y:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, LaN/az;->y:Lcom/google/googlenav/ui/view/d;

    .line 155
    :cond_c
    return-void
.end method

.method private bh()V
    .registers 2

    .prologue
    .line 158
    iget-object v0, p0, LaN/az;->z:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_c

    .line 159
    iget-object v0, p0, LaN/az;->z:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 160
    const/4 v0, 0x0

    iput-object v0, p0, LaN/az;->z:Lcom/google/googlenav/ui/view/d;

    .line 162
    :cond_c
    return-void
.end method

.method private bi()V
    .registers 2

    .prologue
    .line 165
    iget-object v0, p0, LaN/az;->A:Lcom/google/googlenav/ui/android/M;

    if-eqz v0, :cond_11

    .line 166
    iget-object v0, p0, LaN/az;->A:Lcom/google/googlenav/ui/android/M;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/M;->c()V

    .line 167
    iget-object v0, p0, LaN/az;->A:Lcom/google/googlenav/ui/android/M;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/M;->a()V

    .line 168
    const/4 v0, 0x0

    iput-object v0, p0, LaN/az;->A:Lcom/google/googlenav/ui/android/M;

    .line 170
    :cond_11
    return-void
.end method

.method private bj()V
    .registers 3

    .prologue
    .line 285
    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v0

    invoke-virtual {v0}, LaN/aD;->f()I

    move-result v0

    .line 286
    if-nez v0, :cond_b

    .line 294
    :goto_a
    return-void

    .line 289
    :cond_b
    const/4 v1, 0x1

    if-ne v0, v1, :cond_12

    .line 290
    invoke-direct {p0}, LaN/az;->bl()V

    goto :goto_a

    .line 292
    :cond_12
    invoke-direct {p0}, LaN/az;->bk()V

    goto :goto_a
.end method

.method private bk()V
    .registers 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 300
    invoke-virtual {p0}, LaN/az;->f()I

    move-result v0

    .line 301
    if-gtz v0, :cond_9

    .line 341
    :goto_8
    return-void

    .line 305
    :cond_9
    invoke-static {v0, v7}, Lcom/google/googlenav/ui/o;->b(II)Ljava/lang/String;

    move-result-object v2

    .line 307
    iget-object v0, p0, LaN/az;->A:Lcom/google/googlenav/ui/android/M;

    if-nez v0, :cond_23

    .line 308
    new-instance v0, Lcom/google/googlenav/ui/android/M;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v1

    const v3, 0x7f100065

    invoke-direct {v0, v1, v3}, Lcom/google/googlenav/ui/android/M;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;I)V

    iput-object v0, p0, LaN/az;->A:Lcom/google/googlenav/ui/android/M;

    .line 317
    :cond_23
    iget v0, p0, LaN/az;->x:I

    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v1

    invoke-virtual {v1}, LaN/aD;->f()I

    move-result v1

    if-ne v0, v1, :cond_88

    .line 318
    iget-object v0, p0, LaN/az;->w:Ljava/util/List;

    invoke-static {v0}, Laf/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 319
    invoke-static {v0}, Laf/d;->e(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 320
    invoke-static {v1}, Laf/d;->f(Ljava/util/List;)D

    move-result-wide v3

    double-to-int v1, v3

    invoke-static {v1, v7}, Lcom/google/googlenav/ui/o;->c(II)Ljava/lang/String;

    move-result-object v1

    .line 322
    invoke-static {v0}, Laf/d;->c(Ljava/util/List;)Laf/a;

    move-result-object v3

    .line 323
    invoke-virtual {v3}, Laf/a;->b()D

    move-result-wide v4

    double-to-int v0, v4

    invoke-static {v0, v7}, Lcom/google/googlenav/ui/o;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 326
    iget-object v4, p0, LaN/az;->A:Lcom/google/googlenav/ui/android/M;

    iget-object v5, p0, LaN/az;->w:Ljava/util/List;

    invoke-virtual {v4, v5, v8}, Lcom/google/googlenav/ui/android/M;->a(Ljava/util/List;Z)V

    .line 328
    invoke-virtual {p0}, LaN/az;->c()LaN/aD;

    move-result-object v4

    .line 329
    invoke-virtual {v4}, LaN/aD;->a()V

    .line 331
    new-instance v5, LaN/aE;

    invoke-virtual {v3}, Laf/a;->a()Lau/B;

    move-result-object v3

    const/16 v6, 0x16

    invoke-direct {v5, v3, v6, v8}, LaN/aE;-><init>(Lau/B;BI)V

    invoke-virtual {v4, v5}, LaN/aD;->a(LaN/aE;)V

    .line 333
    invoke-virtual {p0}, LaN/az;->R()V

    .line 338
    :goto_6e
    iget-object v3, p0, LaN/az;->A:Lcom/google/googlenav/ui/android/M;

    const/16 v4, 0x2ab

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    aput-object v2, v5, v7

    aput-object v1, v5, v8

    const/4 v1, 0x2

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/android/M;->a(Ljava/lang/CharSequence;)V

    goto :goto_8

    .line 335
    :cond_88
    const-string v1, "--"

    .line 336
    const-string v0, "--"

    goto :goto_6e
.end method

.method private bl()V
    .registers 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 347
    iget-object v0, p0, LaN/az;->A:Lcom/google/googlenav/ui/android/M;

    if-nez v0, :cond_18

    .line 348
    new-instance v0, Lcom/google/googlenav/ui/android/M;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v1

    const v2, 0x7f100065

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/android/M;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;I)V

    iput-object v0, p0, LaN/az;->A:Lcom/google/googlenav/ui/android/M;

    .line 353
    :cond_18
    iget v0, p0, LaN/az;->x:I

    if-ne v0, v3, :cond_45

    .line 354
    iget-object v0, p0, LaN/az;->w:Ljava/util/List;

    invoke-static {v0}, Laf/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 355
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laf/a;

    invoke-virtual {v0}, Laf/a;->b()D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0, v4}, Lcom/google/googlenav/ui/o;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 360
    :goto_31
    iget-object v1, p0, LaN/az;->A:Lcom/google/googlenav/ui/android/M;

    const/16 v2, 0x111

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/android/M;->a(Ljava/lang/CharSequence;)V

    .line 362
    return-void

    .line 358
    :cond_45
    const-string v0, "--"

    goto :goto_31
.end method

.method private bm()V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 421
    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v0

    invoke-virtual {v0}, LaN/aD;->f()I

    move-result v0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_10

    .line 422
    const/4 v0, 0x0

    iput-object v0, p0, LaN/az;->v:[Lcom/google/googlenav/ui/aL;

    .line 433
    :goto_f
    return-void

    .line 426
    :cond_10
    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v0

    invoke-virtual {v0}, LaN/aD;->f()I

    move-result v0

    new-array v2, v0, [Lau/B;

    move v0, v1

    .line 427
    :goto_1b
    array-length v3, v2

    if-ge v0, v3, :cond_2f

    .line 428
    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v3

    invoke-virtual {v3, v0}, LaN/aD;->d(I)LaN/aE;

    move-result-object v3

    invoke-virtual {v3}, LaN/aE;->a()Lau/B;

    move-result-object v3

    aput-object v3, v2, v0

    .line 427
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b

    .line 431
    :cond_2f
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/googlenav/ui/aL;

    new-instance v3, Lau/M;

    const v4, -0xd5ba98

    const/4 v5, 0x5

    invoke-direct {v3, v2, v4, v5}, Lau/M;-><init>([Lau/B;II)V

    aput-object v3, v0, v1

    iput-object v0, p0, LaN/az;->v:[Lcom/google/googlenav/ui/aL;

    .line 432
    iget-object v0, p0, LaN/az;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0, p0}, LaN/am;->n(LaN/i;)V

    goto :goto_f
.end method

.method static synthetic c(LaN/az;)I
    .registers 3
    .parameter

    .prologue
    .line 49
    iget v0, p0, LaN/az;->x:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LaN/az;->x:I

    return v0
.end method

.method static synthetic d(LaN/az;)V
    .registers 1
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, LaN/az;->bj()V

    return-void
.end method


# virtual methods
.method protected O()Z
    .registers 2

    .prologue
    .line 467
    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .registers 2

    .prologue
    .line 472
    const/4 v0, 0x0

    return v0
.end method

.method protected X()Z
    .registers 2

    .prologue
    .line 455
    invoke-super {p0}, LaN/i;->X()Z

    .line 457
    const/4 v0, 0x1

    return v0
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .registers 3
    .parameter

    .prologue
    .line 487
    iput-object p1, p0, LaN/az;->f:Lcom/google/googlenav/F;

    .line 488
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, LaN/az;->b(I)V

    .line 489
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 513
    const/4 v0, 0x0

    iput-object v0, p0, LaN/az;->v:[Lcom/google/googlenav/ui/aL;

    .line 514
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 113
    iget-boolean v0, p0, LaN/az;->u:Z

    return v0
.end method

.method protected a(Laa/b;Z)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 174
    iget-boolean v0, p0, LaN/az;->u:Z

    if-eqz v0, :cond_25

    iget-object v0, p0, LaN/az;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->D()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-virtual {p1}, Laa/b;->h()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 178
    iget-object v0, p0, LaN/az;->c:Lau/p;

    invoke-virtual {p1}, Laa/b;->k()I

    move-result v1

    invoke-virtual {p1}, Laa/b;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lau/p;->b(II)Lau/B;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/az;->d(Lau/B;)Z

    move-result v0

    .line 180
    :goto_24
    return v0

    :cond_25
    const/4 v0, 0x0

    goto :goto_24
.end method

.method public aC()Z
    .registers 2

    .prologue
    .line 462
    const/4 v0, 0x0

    return v0
.end method

.method public aU()V
    .registers 1

    .prologue
    .line 442
    invoke-super {p0}, LaN/i;->aU()V

    .line 443
    invoke-direct {p0}, LaN/az;->bh()V

    .line 444
    invoke-direct {p0}, LaN/az;->bg()V

    .line 445
    invoke-direct {p0}, LaN/az;->bi()V

    .line 446
    return-void
.end method

.method protected ap()V
    .registers 1

    .prologue
    .line 509
    return-void
.end method

.method protected aq()V
    .registers 1

    .prologue
    .line 504
    return-void
.end method

.method public av()I
    .registers 2

    .prologue
    .line 498
    const/16 v0, 0x13

    return v0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .registers 3
    .parameter

    .prologue
    .line 477
    const/4 v0, 0x0

    return v0
.end method

.method public b()LaN/aD;
    .registers 2

    .prologue
    .line 365
    invoke-virtual {p0}, LaN/az;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/n;

    invoke-virtual {v0}, Lcom/google/googlenav/n;->b()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, LaN/aD;

    return-object v0
.end method

.method public c(Lcom/google/googlenav/E;)I
    .registers 3
    .parameter

    .prologue
    .line 482
    const/4 v0, 0x0

    return v0
.end method

.method public c()LaN/aD;
    .registers 2

    .prologue
    .line 369
    invoke-virtual {p0}, LaN/az;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/n;

    invoke-virtual {v0}, Lcom/google/googlenav/n;->a()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, LaN/aD;

    return-object v0
.end method

.method public d(Lau/B;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 191
    iget-boolean v1, p0, LaN/az;->u:Z

    if-eqz v1, :cond_17

    .line 192
    iget-object v1, p0, LaN/az;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v1

    invoke-virtual {v1}, LaN/am;->J()LaN/i;

    move-result-object v1

    .line 193
    if-eqz v1, :cond_19

    invoke-virtual {v1}, LaN/i;->aj()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 227
    :cond_17
    const/4 v0, 0x0

    :goto_18
    return v0

    .line 199
    :cond_19
    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v1

    new-instance v2, LaN/aE;

    invoke-direct {v2, p1}, LaN/aE;-><init>(Lau/B;)V

    invoke-virtual {v1, v2}, LaN/aD;->a(LaN/aE;)V

    .line 201
    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v1

    invoke-virtual {v1}, LaN/aD;->f()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_50

    .line 203
    const/16 v1, 0x47

    const-string v2, "u-add"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lal/g;->a:Lal/g;

    invoke-virtual {v4}, Lal/g;->d()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_50
    invoke-direct {p0}, LaN/az;->bm()V

    .line 210
    invoke-virtual {p0}, LaN/az;->R()V

    .line 211
    invoke-direct {p0}, LaN/az;->bj()V

    .line 213
    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v1

    invoke-virtual {v1}, LaN/aD;->f()I

    move-result v1

    if-ne v1, v0, :cond_68

    .line 215
    const/4 v1, 0x0

    invoke-direct {p0, v1, p1}, LaN/az;->a(Lau/B;Lau/B;)V

    goto :goto_18

    .line 218
    :cond_68
    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v1

    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v2

    invoke-virtual {v2}, LaN/aD;->f()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v2}, LaN/aD;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()Lau/B;

    move-result-object v1

    invoke-direct {p0, v1, p1}, LaN/az;->a(Lau/B;Lau/B;)V

    goto :goto_18
.end method

.method public d()[Lcom/google/googlenav/ui/aL;
    .registers 2

    .prologue
    .line 437
    iget-object v0, p0, LaN/az;->v:[Lcom/google/googlenav/ui/aL;

    return-object v0
.end method

.method protected e()V
    .registers 5

    .prologue
    .line 373
    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/az;->u:Z

    .line 377
    const/16 v0, 0x47

    const-string v1, "u-start"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lal/g;->a:Lal/g;

    invoke-virtual {v3}, Lal/g;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 379
    return-void
.end method

.method public f()I
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 402
    .line 404
    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v1

    invoke-virtual {v1}, LaN/aD;->f()I

    move-result v1

    if-le v1, v0, :cond_39

    .line 405
    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v1

    invoke-virtual {v1, v2}, LaN/aD;->d(I)LaN/aE;

    move-result-object v1

    invoke-virtual {v1}, LaN/aE;->a()Lau/B;

    move-result-object v1

    move v3, v2

    .line 406
    :goto_19
    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v2

    invoke-virtual {v2}, LaN/aD;->f()I

    move-result v2

    if-ge v0, v2, :cond_3a

    .line 407
    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v2

    invoke-virtual {v2, v0}, LaN/aD;->d(I)LaN/aE;

    move-result-object v2

    invoke-virtual {v2}, LaN/aE;->a()Lau/B;

    move-result-object v2

    .line 408
    invoke-static {v1, v2}, Lcom/google/googlenav/ui/o;->a(Lau/B;Lau/B;)I

    move-result v1

    add-int/2addr v1, v3

    .line 406
    add-int/lit8 v0, v0, 0x1

    move v3, v1

    move-object v1, v2

    goto :goto_19

    :cond_39
    move v3, v2

    .line 413
    :cond_3a
    return v3
.end method

.method protected f(Laa/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 493
    const/4 v0, 0x0

    return v0
.end method

.method protected i()LaP/a;
    .registers 2

    .prologue
    .line 450
    new-instance v0, LaP/f;

    invoke-direct {v0, p0}, LaP/f;-><init>(LaN/i;)V

    return-object v0
.end method

.method protected k(Z)V
    .registers 6
    .parameter

    .prologue
    .line 382
    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/az;->u:Z

    .line 385
    if-eqz p1, :cond_3e

    .line 386
    const/16 v0, 0x47

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u-stop"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v2

    invoke-virtual {v2}, LaN/aD;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lal/g;->a:Lal/g;

    invoke-virtual {v3}, Lal/g;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 391
    :cond_3e
    invoke-virtual {p0}, LaN/az;->b()LaN/aD;

    move-result-object v0

    invoke-virtual {v0}, LaN/aD;->a()V

    .line 392
    invoke-virtual {p0}, LaN/az;->c()LaN/aD;

    move-result-object v0

    invoke-virtual {v0}, LaN/aD;->a()V

    .line 393
    invoke-direct {p0}, LaN/az;->be()V

    .line 394
    invoke-virtual {p0}, LaN/az;->R()V

    .line 395
    const/4 v0, 0x0

    iput-object v0, p0, LaN/az;->v:[Lcom/google/googlenav/ui/aL;

    .line 396
    iget-object v0, p0, LaN/az;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0, p0}, LaN/am;->n(LaN/i;)V

    .line 397
    invoke-direct {p0}, LaN/az;->bi()V

    .line 398
    invoke-direct {p0}, LaN/az;->bf()V

    .line 399
    return-void
.end method
