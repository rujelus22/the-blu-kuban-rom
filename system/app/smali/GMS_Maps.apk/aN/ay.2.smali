.class public final enum LaN/ay;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LaN/ay;

.field public static final enum b:LaN/ay;

.field public static final enum c:LaN/ay;

.field public static final enum d:LaN/ay;

.field public static final enum e:LaN/ay;

.field private static final synthetic f:[LaN/ay;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    new-instance v0, LaN/ay;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LaN/ay;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaN/ay;->a:LaN/ay;

    .line 61
    new-instance v0, LaN/ay;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v3}, LaN/ay;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaN/ay;->b:LaN/ay;

    .line 62
    new-instance v0, LaN/ay;

    const-string v1, "DIRECTIONS"

    invoke-direct {v0, v1, v4}, LaN/ay;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaN/ay;->c:LaN/ay;

    .line 63
    new-instance v0, LaN/ay;

    const-string v1, "LATITUDE"

    invoke-direct {v0, v1, v5}, LaN/ay;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaN/ay;->d:LaN/ay;

    .line 64
    new-instance v0, LaN/ay;

    const-string v1, "TRANSIT"

    invoke-direct {v0, v1, v6}, LaN/ay;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaN/ay;->e:LaN/ay;

    .line 59
    const/4 v0, 0x5

    new-array v0, v0, [LaN/ay;

    sget-object v1, LaN/ay;->a:LaN/ay;

    aput-object v1, v0, v2

    sget-object v1, LaN/ay;->b:LaN/ay;

    aput-object v1, v0, v3

    sget-object v1, LaN/ay;->c:LaN/ay;

    aput-object v1, v0, v4

    sget-object v1, LaN/ay;->d:LaN/ay;

    aput-object v1, v0, v5

    sget-object v1, LaN/ay;->e:LaN/ay;

    aput-object v1, v0, v6

    sput-object v0, LaN/ay;->f:[LaN/ay;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaN/ay;
    .registers 2
    .parameter

    .prologue
    .line 59
    const-class v0, LaN/ay;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaN/ay;

    return-object v0
.end method

.method public static values()[LaN/ay;
    .registers 1

    .prologue
    .line 59
    sget-object v0, LaN/ay;->f:[LaN/ay;

    invoke-virtual {v0}, [LaN/ay;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaN/ay;

    return-object v0
.end method
