.class public LaN/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaC/i;
.implements Lcom/google/googlenav/common/util/n;
.implements Lcom/google/googlenav/layer/l;


# static fields
.field private static final I:I

.field private static final J:I

.field private static final K:I

.field private static final Q:I

.field public static a:I

.field public static b:I

.field protected static final j:I

.field private static final t:[I

.field private static final u:[Z

.field private static v:Z

.field private static volatile w:I


# instance fields
.field private A:Z

.field private B:I

.field private C:Ljava/lang/Object;

.field private D:Ljava/lang/String;

.field private final E:Landroid/graphics/Point;

.field private final F:Lcom/google/googlenav/layer/r;

.field private final G:LaN/a;

.field private final H:Ljava/util/Map;

.field private final L:Ljava/util/Vector;

.field private M:Z

.field private N:Lau/B;

.field private O:I

.field private P:Lcom/google/googlenav/offers/j;

.field protected final c:Lcom/google/googlenav/ui/v;

.field protected final d:Lao/h;

.field protected final e:Lau/p;

.field protected final f:Lau/u;

.field protected final g:Lcom/google/googlenav/ui/ac;

.field protected final h:Lau/k;

.field protected final i:Ljava/util/Vector;

.field private final k:Lcom/google/googlenav/friend/J;

.field private final l:Lcom/google/googlenav/friend/p;

.field private final m:Lcom/google/googlenav/friend/ag;

.field private final n:Lcom/google/googlenav/android/Y;

.field private final o:Lcom/google/googlenav/ui/wizard/jt;

.field private final p:Ljava/util/Vector;

.field private final q:Ljava/util/Vector;

.field private final r:Ljava/util/Vector;

.field private s:Z

.field private x:B

.field private y:I

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x5

    .line 114
    sput v2, LaN/am;->a:I

    .line 122
    const/16 v0, 0x14

    sput v0, LaN/am;->b:I

    .line 194
    new-array v0, v2, [I

    fill-array-data v0, :array_4a

    sput-object v0, LaN/am;->t:[I

    .line 203
    sget-object v0, LaN/am;->t:[I

    array-length v0, v0

    new-array v0, v0, [Z

    sput-object v0, LaN/am;->u:[Z

    .line 212
    sput-boolean v1, LaN/am;->v:Z

    .line 245
    sput v1, LaN/am;->w:I

    .line 295
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, LaN/am;->I:I

    .line 301
    sget v0, LaN/am;->I:I

    sget v1, LaN/am;->I:I

    mul-int/2addr v0, v1

    sput v0, LaN/am;->J:I

    .line 319
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, LaN/am;->K:I

    .line 326
    sget v0, LaN/am;->K:I

    sget v1, LaN/am;->K:I

    mul-int/2addr v0, v1

    sput v0, LaN/am;->j:I

    .line 366
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, LaN/am;->Q:I

    return-void

    .line 194
    nop

    :array_4a
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lao/h;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/android/Y;Lcom/google/googlenav/ui/wizard/jt;Lau/k;Lcom/google/googlenav/layer/r;Lcom/google/googlenav/offers/j;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    .line 156
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/am;->p:Ljava/util/Vector;

    .line 167
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    .line 173
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/am;->r:Ljava/util/Vector;

    .line 181
    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/am;->s:Z

    .line 252
    const/4 v0, -0x1

    iput-byte v0, p0, LaN/am;->x:B

    .line 253
    const/16 v0, -0xa

    iput v0, p0, LaN/am;->y:I

    .line 254
    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/am;->z:Z

    .line 255
    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/am;->A:Z

    .line 258
    const/4 v0, -0x1

    iput v0, p0, LaN/am;->B:I

    .line 271
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LaN/am;->E:Landroid/graphics/Point;

    .line 283
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LaN/am;->H:Ljava/util/Map;

    .line 334
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/am;->L:Ljava/util/Vector;

    .line 341
    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/am;->M:Z

    .line 357
    const/4 v0, -0x1

    iput v0, p0, LaN/am;->O:I

    .line 428
    iput-object p1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    .line 429
    iput-object p2, p0, LaN/am;->e:Lau/p;

    .line 430
    iput-object p3, p0, LaN/am;->f:Lau/u;

    .line 431
    iput-object p4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    .line 432
    iput-object p5, p0, LaN/am;->d:Lao/h;

    .line 433
    iput-object p6, p0, LaN/am;->k:Lcom/google/googlenav/friend/J;

    .line 434
    iput-object p7, p0, LaN/am;->l:Lcom/google/googlenav/friend/p;

    .line 435
    iput-object p8, p0, LaN/am;->m:Lcom/google/googlenav/friend/ag;

    .line 436
    iput-object p9, p0, LaN/am;->n:Lcom/google/googlenav/android/Y;

    .line 437
    iput-object p10, p0, LaN/am;->o:Lcom/google/googlenav/ui/wizard/jt;

    .line 438
    iput-object p11, p0, LaN/am;->h:Lau/k;

    .line 439
    iput-object p12, p0, LaN/am;->F:Lcom/google/googlenav/layer/r;

    .line 440
    iput-object p13, p0, LaN/am;->P:Lcom/google/googlenav/offers/j;

    .line 441
    new-instance v0, LaN/a;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/v;->ak()LZ/c;

    move-result-object v1

    invoke-direct {v0, v1}, LaN/a;-><init>(LZ/c;)V

    iput-object v0, p0, LaN/am;->G:LaN/a;

    .line 442
    return-void
.end method

.method public static Y()I
    .registers 3

    .prologue
    const/4 v1, 0x0

    move v0, v1

    .line 4090
    :goto_2
    sget-object v2, LaN/am;->u:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_11

    .line 4091
    sget-object v2, LaN/am;->u:[Z

    aget-boolean v2, v2, v0

    if-nez v2, :cond_12

    .line 4092
    sget-object v1, LaN/am;->t:[I

    aget v1, v1, v0

    .line 4096
    :cond_11
    return v1

    .line 4090
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public static Z()I
    .registers 2

    .prologue
    .line 4118
    sget v0, LaN/am;->w:I

    add-int/lit8 v1, v0, 0x1

    sput v1, LaN/am;->w:I

    return v0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/i;Z)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3228
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 3230
    const/4 v1, 0x2

    invoke-virtual {p2}, LaN/i;->av()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3231
    const/4 v1, 0x3

    invoke-virtual {p2}, LaN/i;->aO()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/common/util/e;->a(I)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3233
    const/4 v1, 0x4

    invoke-virtual {p2}, LaN/i;->ay()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3234
    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3235
    return-object v0
.end method

.method public static a(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4106
    const/4 v0, 0x0

    :goto_1
    sget-object v1, LaN/am;->t:[I

    array-length v1, v1

    if-ge v0, v1, :cond_10

    .line 4107
    sget-object v1, LaN/am;->t:[I

    aget v1, v1, v0

    if-ne v1, p0, :cond_11

    .line 4108
    sget-object v1, LaN/am;->u:[Z

    aput-boolean p1, v1, v0

    .line 4112
    :cond_10
    return-void

    .line 4106
    :cond_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static synthetic a(LaN/am;)V
    .registers 1
    .parameter

    .prologue
    .line 107
    invoke-direct {p0}, LaN/am;->ai()V

    return-void
.end method

.method static synthetic a(LaN/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3, p4}, LaN/am;->a(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private a(LaN/i;)V
    .registers 5
    .parameter

    .prologue
    .line 744
    invoke-virtual {p1}, LaN/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, LaN/i;->aC()Z

    move-result v0

    if-nez v0, :cond_d

    .line 765
    :cond_c
    :goto_c
    return-void

    .line 750
    :cond_d
    iget-object v2, p0, LaN/am;->q:Ljava/util/Vector;

    monitor-enter v2

    .line 752
    :try_start_10
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 754
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    sget v1, LaN/am;->b:I

    if-le v0, v1, :cond_43

    .line 757
    sget v0, LaN/am;->b:I

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_25
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_3c

    .line 758
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 759
    invoke-direct {p0, v0}, LaN/am;->c(LaN/i;)V

    .line 757
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_25

    .line 761
    :cond_3c
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    sget v1, LaN/am;->b:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->setSize(I)V

    .line 763
    :cond_43
    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/am;->A:Z

    .line 764
    monitor-exit v2

    goto :goto_c

    :catchall_48
    move-exception v0

    monitor-exit v2
    :try_end_4a
    .catchall {:try_start_10 .. :try_end_4a} :catchall_48

    throw v0
.end method

.method private a(LaN/i;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/16 v1, 0x43

    .line 4178
    invoke-virtual {p1}, LaN/i;->av()I

    move-result v0

    packed-switch v0, :pswitch_data_26

    .line 4185
    invoke-static {p1}, LaN/am;->p(LaN/i;)C

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, p2, v0}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 4188
    :goto_14
    invoke-direct {p0}, LaN/am;->al()V

    .line 4189
    return-void

    .line 4181
    :pswitch_18
    check-cast p1, LaN/y;

    invoke-virtual {p1}, LaN/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, p2, v0}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_14

    .line 4178
    :pswitch_data_26
    .packed-switch 0x6
        :pswitch_18
        :pswitch_18
    .end packed-switch
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 4209
    invoke-static {p1}, LaU/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 4210
    invoke-direct {p0}, LaN/am;->al()V

    .line 4211
    return-void
.end method

.method private a(Ljava/io/DataInput;IZZ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3504
    new-instance v0, LaN/bj;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    sget-object v5, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v6, p0, LaN/am;->h:Lau/k;

    invoke-static {}, LaN/am;->Y()I

    move-result v7

    invoke-direct/range {v0 .. v7}, LaN/bj;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;I)V

    .line 3506
    invoke-virtual {v0, p1}, LaN/bj;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_3e

    .line 3507
    invoke-virtual {v0, p2}, LaN/bj;->e(I)V

    .line 3508
    invoke-virtual {v0, p3}, LaN/bj;->h(Z)V

    .line 3509
    if-eqz p4, :cond_3a

    invoke-virtual {v0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ab()Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 3514
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->f()Z

    move-result v1

    .line 3515
    const/4 v2, 0x0

    .line 3516
    invoke-virtual {p0, v0, v1, v2}, LaN/am;->a(LaN/i;ZZ)Z

    .line 3524
    :goto_39
    return-void

    .line 3518
    :cond_3a
    invoke-direct {p0, v0}, LaN/am;->a(LaN/i;)V

    goto :goto_39

    .line 3521
    :cond_3e
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_39
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3182
    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 3183
    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDataSize()I

    move-result v1

    if-lez v1, :cond_17

    .line 3184
    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I

    .line 3188
    :goto_15
    const/4 v0, 0x1

    .line 3199
    :goto_16
    return v0

    .line 3186
    :cond_17
    invoke-interface {v0, p2}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_1a} :catch_1b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_1a} :catch_26

    goto :goto_15

    .line 3189
    :catch_1b
    move-exception v0

    .line 3190
    const-string v1, "LAYER_MANAGER-LayerManager Error saving layers"

    invoke-static {v1, v0}, LaU/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3193
    invoke-direct {p0}, LaN/am;->ai()V

    .line 3199
    :goto_24
    const/4 v0, 0x0

    goto :goto_16

    .line 3194
    :catch_26
    move-exception v0

    .line 3195
    const-string v1, "LAYER_MANAGER-LayerManager OOME saving layers"

    invoke-static {v1, v0}, LaU/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3197
    invoke-direct {p0}, LaN/am;->ai()V

    goto :goto_24
.end method

.method private ad()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1032
    const/16 v0, 0xd

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1033
    const/16 v0, 0x12

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1034
    const/16 v0, 0xf

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1035
    return-void
.end method

.method private ae()Lcom/google/googlenav/ui/ap;
    .registers 2

    .prologue
    .line 1242
    iget-object v0, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->p()Lcom/google/googlenav/ui/ap;

    move-result-object v0

    return-object v0
.end method

.method private af()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 2224
    move v1, v2

    :goto_2
    iget-object v0, p0, LaN/am;->L:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 2225
    iget-object v0, p0, LaN/am;->L:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/ai;

    .line 2226
    invoke-virtual {v0, v2}, LaN/ai;->a(Z)V

    .line 2224
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2228
    :cond_19
    return-void
.end method

.method private ag()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    .line 3093
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ag;->o:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 3097
    const/4 v1, 0x0

    .line 3102
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 3103
    iget-object v4, p0, LaN/am;->i:Ljava/util/Vector;

    monitor-enter v4

    .line 3104
    const/4 v0, 0x0

    :goto_11
    :try_start_11
    iget-object v5, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v0, v5, :cond_25

    .line 3105
    iget-object v5, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v5, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3104
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 3107
    :cond_25
    monitor-exit v4
    :try_end_26
    .catchall {:try_start_11 .. :try_end_26} :catchall_50

    .line 3109
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_53

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 3110
    if-eqz v0, :cond_97

    invoke-virtual {v0}, LaN/i;->aM()Z

    move-result v4

    if-eqz v4, :cond_97

    .line 3111
    invoke-direct {p0, v3, v0, v6}, LaN/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/i;Z)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v3, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 3114
    iget-object v1, p0, LaN/am;->F:Lcom/google/googlenav/layer/r;

    if-eqz v1, :cond_4e

    .line 3115
    iget-object v1, p0, LaN/am;->F:Lcom/google/googlenav/layer/r;

    invoke-interface {v1, v0}, Lcom/google/googlenav/layer/r;->a(LaN/i;)V

    :cond_4e
    :goto_4e
    move-object v1, v0

    goto :goto_2a

    .line 3107
    :catchall_50
    move-exception v0

    :try_start_51
    monitor-exit v4
    :try_end_52
    .catchall {:try_start_51 .. :try_end_52} :catchall_50

    throw v0

    .line 3121
    :cond_53
    const/4 v2, -0x1

    .line 3122
    const/16 v0, -0xa

    .line 3123
    if-eqz v1, :cond_95

    .line 3124
    invoke-virtual {v1}, LaN/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v2

    .line 3125
    const/16 v0, 0xa

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3127
    invoke-virtual {v1}, LaN/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    .line 3128
    const/16 v4, 0xb

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3130
    const/16 v4, 0xe

    invoke-virtual {v1}, LaN/i;->aA()I

    move-result v1

    invoke-virtual {v3, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v1, v2

    .line 3135
    :goto_7c
    iget-boolean v2, p0, LaN/am;->z:Z

    if-eqz v2, :cond_88

    iget-byte v2, p0, LaN/am;->x:B

    if-ne v1, v2, :cond_88

    iget v2, p0, LaN/am;->y:I

    if-eq v0, v2, :cond_94

    .line 3137
    :cond_88
    const-string v2, "PROTO_SAVED_LAYER_STATE"

    invoke-direct {p0, v3, v2}, LaN/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, LaN/am;->z:Z

    .line 3138
    iput-byte v1, p0, LaN/am;->x:B

    .line 3139
    iput v0, p0, LaN/am;->y:I

    .line 3141
    :cond_94
    return-void

    :cond_95
    move v1, v2

    goto :goto_7c

    :cond_97
    move-object v0, v1

    goto :goto_4e
.end method

.method private ah()V
    .registers 7

    .prologue
    .line 3147
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ag;->o:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 3152
    iget-object v3, p0, LaN/am;->q:Ljava/util/Vector;

    monitor-enter v3

    .line 3154
    :try_start_a
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_13
    if-ltz v1, :cond_39

    .line 3155
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 3156
    invoke-virtual {v0}, LaN/i;->aM()Z

    move-result v4

    if-eqz v4, :cond_35

    .line 3157
    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, v2, v0, v5}, LaN/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/i;Z)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 3159
    iget-object v4, p0, LaN/am;->F:Lcom/google/googlenav/layer/r;

    if-eqz v4, :cond_35

    .line 3160
    iget-object v4, p0, LaN/am;->F:Lcom/google/googlenav/layer/r;

    invoke-interface {v4, v0}, Lcom/google/googlenav/layer/r;->a(LaN/i;)V

    .line 3154
    :cond_35
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_13

    .line 3164
    :cond_39
    monitor-exit v3
    :try_end_3a
    .catchall {:try_start_a .. :try_end_3a} :catchall_47

    .line 3167
    iget-boolean v0, p0, LaN/am;->A:Z

    if-nez v0, :cond_46

    .line 3168
    const-string v0, "PROTO_SAVED_RECENT_LAYERS"

    invoke-direct {p0, v2, v0}, LaN/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LaN/am;->A:Z

    .line 3170
    :cond_46
    return-void

    .line 3164
    :catchall_47
    move-exception v0

    :try_start_48
    monitor-exit v3
    :try_end_49
    .catchall {:try_start_48 .. :try_end_49} :catchall_47

    throw v0
.end method

.method private ai()V
    .registers 3

    .prologue
    .line 3660
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 3663
    const-string v1, "PROTO_SAVED_LAYER_STATE"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 3664
    const-string v1, "PROTO_SAVED_RECENT_LAYERS"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 3665
    const-string v1, "LAYER_"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->c(Ljava/lang/String;)V

    .line 3666
    return-void
.end method

.method private aj()V
    .registers 3

    .prologue
    .line 3672
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 3673
    const-string v1, "SAVED_SEARCH_1_DB"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 3674
    const-string v1, "SAVED_SEARCH_1"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 3675
    const-string v1, "PROTO_SAVED_SEARCH_DB"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 3676
    const-string v1, "SAVED_SEARCH_INFO"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 3680
    invoke-static {v0}, LaN/a;->a(Lcom/google/googlenav/common/io/j;)V

    .line 3681
    return-void
.end method

.method private ak()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 4027
    move v2, v3

    :goto_2
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_2c

    .line 4028
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 4029
    invoke-virtual {v0}, LaN/i;->av()I

    move-result v1

    if-nez v1, :cond_28

    move-object v1, v0

    .line 4030
    check-cast v1, LaN/bj;

    invoke-virtual {v1}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 4031
    invoke-virtual {v0, v3}, LaN/i;->h(Z)V

    .line 4027
    :cond_28
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 4035
    :cond_2c
    return-void
.end method

.method private al()V
    .registers 4

    .prologue
    .line 4217
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 4218
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_22

    .line 4219
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    invoke-static {v0}, LaN/am;->p(LaN/i;)C

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 4218
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 4221
    :cond_22
    const/16 v0, 0x43

    const-string v1, "v"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 4223
    return-void
.end method

.method private am()V
    .registers 4

    .prologue
    .line 4229
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 4230
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_22

    .line 4231
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    invoke-static {v0}, LaN/am;->p(LaN/i;)C

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 4230
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 4233
    :cond_22
    const/16 v0, 0x43

    const-string v1, "r"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 4235
    return-void
.end method

.method private b(IZ)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 962
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 963
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_2f

    .line 964
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 966
    invoke-virtual {v0}, LaN/i;->av()I

    move-result v3

    if-ne v3, p1, :cond_2b

    invoke-virtual {v0}, LaN/i;->ay()Z

    move-result v0

    if-nez v0, :cond_2b

    .line 967
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 963
    :cond_2b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 970
    :cond_2f
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_33
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_44

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 971
    const/4 v2, 0x1

    invoke-virtual {p0, v0, p2, v2}, LaN/am;->b(LaN/i;ZZ)V

    goto :goto_33

    .line 973
    :cond_44
    invoke-virtual {p0}, LaN/am;->d()V

    .line 974
    return-void
.end method

.method static synthetic b(LaN/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3, p4}, LaN/am;->b(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private b(LaN/i;)V
    .registers 4
    .parameter

    .prologue
    .line 929
    new-instance v0, LaN/an;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->ak()LZ/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, LaN/an;-><init>(LaN/am;LZ/c;LaN/i;)V

    invoke-virtual {v0}, LaN/an;->g()V

    .line 935
    return-void
.end method

.method private b(LaN/i;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2309
    invoke-virtual {p1}, LaN/i;->ah()Z

    move-result v0

    .line 2312
    invoke-virtual {p1, p2}, LaN/i;->b(I)V

    .line 2314
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 2317
    invoke-virtual {p1}, LaN/i;->n()V

    .line 2322
    if-eqz v0, :cond_20

    invoke-virtual {p1}, LaN/i;->av()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_20

    .line 2323
    invoke-virtual {p0, p1}, LaN/am;->d(LaN/i;)V

    .line 2335
    :cond_20
    :goto_20
    return-void

    .line 2329
    :cond_21
    invoke-virtual {p1}, LaN/i;->al()V

    .line 2330
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LaN/i;->b(B)V

    .line 2333
    invoke-virtual {p0, p1}, LaN/am;->e(LaN/i;)V

    goto :goto_20
.end method

.method private b(Ljava/io/DataInput;IZZ)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x0

    .line 3528
    new-instance v0, LaN/aI;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    sget-object v5, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v6, p0, LaN/am;->h:Lau/k;

    iget-object v7, p0, LaN/am;->P:Lcom/google/googlenav/offers/j;

    invoke-direct/range {v0 .. v7}, LaN/aI;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;Lcom/google/googlenav/ui/view/dialog/bl;)V

    .line 3530
    invoke-virtual {v0, p1}, LaN/aI;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 3531
    invoke-virtual {v0, p2}, LaN/aI;->e(I)V

    .line 3532
    invoke-virtual {v0, p3}, LaN/aI;->h(Z)V

    .line 3533
    if-eqz p4, :cond_30

    invoke-virtual {v0}, LaN/aI;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ab()Z

    move-result v1

    if-eqz v1, :cond_30

    .line 3534
    invoke-virtual {p0, v0, v8, v8}, LaN/am;->a(LaN/i;ZZ)Z

    .line 3542
    :goto_2f
    return-void

    .line 3536
    :cond_30
    invoke-direct {p0, v0}, LaN/am;->a(LaN/i;)V

    goto :goto_2f

    .line 3539
    :cond_34
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_2f
.end method

.method public static c(I)I
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 1949
    packed-switch p0, :pswitch_data_3a

    .line 1986
    :pswitch_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown layer of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1e
    move v0, v1

    .line 1983
    :goto_1f
    :pswitch_1f
    return v0

    :pswitch_20
    move v0, v1

    .line 1955
    goto :goto_1f

    :pswitch_22
    move v0, v1

    .line 1957
    goto :goto_1f

    :pswitch_24
    move v0, v1

    .line 1959
    goto :goto_1f

    :pswitch_26
    move v0, v1

    .line 1961
    goto :goto_1f

    :pswitch_28
    move v0, v1

    .line 1965
    goto :goto_1f

    :pswitch_2a
    move v0, v1

    .line 1969
    goto :goto_1f

    :pswitch_2c
    move v0, v1

    .line 1971
    goto :goto_1f

    :pswitch_2e
    move v0, v1

    .line 1973
    goto :goto_1f

    :pswitch_30
    move v0, v1

    .line 1975
    goto :goto_1f

    :pswitch_32
    move v0, v1

    .line 1977
    goto :goto_1f

    :pswitch_34
    move v0, v1

    .line 1979
    goto :goto_1f

    :pswitch_36
    move v0, v1

    .line 1981
    goto :goto_1f

    :pswitch_38
    move v0, v1

    .line 1983
    goto :goto_1f

    .line 1949
    :pswitch_data_3a
    .packed-switch 0x0
        :pswitch_1f
        :pswitch_1e
        :pswitch_22
        :pswitch_24
        :pswitch_5
        :pswitch_26
        :pswitch_1f
        :pswitch_28
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_1f
        :pswitch_5
        :pswitch_2a
        :pswitch_5
        :pswitch_2c
        :pswitch_2e
        :pswitch_5
        :pswitch_30
        :pswitch_32
        :pswitch_5
        :pswitch_20
        :pswitch_34
        :pswitch_36
        :pswitch_5
        :pswitch_5
        :pswitch_38
    .end packed-switch
.end method

.method static synthetic c(LaN/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3, p4}, LaN/am;->g(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private c(LaN/i;)V
    .registers 4
    .parameter

    .prologue
    .line 943
    new-instance v0, LaN/ao;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->ak()LZ/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, LaN/ao;-><init>(LaN/am;LZ/c;LaN/i;)V

    invoke-virtual {v0}, LaN/ao;->g()V

    .line 950
    return-void
.end method

.method private c(LaN/i;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2348
    invoke-virtual {p1, p2}, LaN/i;->b(I)V

    .line 2349
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 2351
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 2355
    invoke-virtual {p1}, LaN/i;->ae()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2359
    invoke-virtual {p1}, LaN/i;->l()V

    .line 2388
    :goto_16
    return-void

    .line 2360
    :cond_17
    invoke-virtual {p1}, LaN/i;->af()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 2361
    invoke-virtual {p1}, LaN/i;->m()V

    goto :goto_16

    .line 2363
    :cond_21
    invoke-virtual {p1}, LaN/i;->n()V

    goto :goto_16

    .line 2369
    :cond_25
    invoke-virtual {p1}, LaN/i;->al()V

    .line 2373
    if-eqz v0, :cond_41

    invoke-virtual {v0}, LaN/i;->ai()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 2376
    invoke-virtual {p0, p1}, LaN/am;->e(LaN/i;)V

    .line 2377
    invoke-virtual {p1}, LaN/i;->ad()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 2378
    invoke-virtual {p1}, LaN/i;->m()V

    goto :goto_16

    .line 2380
    :cond_3d
    invoke-virtual {p1}, LaN/i;->l()V

    goto :goto_16

    .line 2385
    :cond_41
    invoke-virtual {p0, p1}, LaN/am;->e(LaN/i;)V

    goto :goto_16
.end method

.method private c(Lcom/google/googlenav/aZ;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2843
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 2845
    invoke-virtual {p0}, LaN/am;->B()LaN/X;

    move-result-object v0

    .line 2849
    if-nez v0, :cond_2c

    sget-boolean v1, LaN/am;->v:Z

    if-eqz v1, :cond_2c

    .line 2850
    invoke-virtual {p0, p1}, LaN/am;->e(Lcom/google/googlenav/aZ;)LaN/X;

    move-result-object v0

    .line 2861
    :cond_1e
    :goto_1e
    invoke-virtual {p0, p1}, LaN/am;->f(Lcom/google/googlenav/aZ;)V

    .line 2866
    sget-boolean v1, LaN/am;->v:Z

    if-eqz v1, :cond_2b

    if-nez p2, :cond_2b

    .line 2867
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, LaN/X;->a(ZLcom/google/googlenav/aZ;)V

    .line 2870
    :cond_2b
    return-void

    .line 2854
    :cond_2c
    if-eqz v0, :cond_1e

    .line 2855
    invoke-virtual {p0, v0}, LaN/am;->a(LaN/X;)V

    goto :goto_1e
.end method

.method private c(Ljava/io/DataInput;IZZ)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 3546
    new-instance v0, LaN/O;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    iget-object v5, p0, LaN/am;->d:Lao/h;

    invoke-direct/range {v0 .. v5}, LaN/O;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lao/h;)V

    .line 3548
    invoke-virtual {v0, p1}, LaN/O;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 3549
    invoke-virtual {v0, p2}, LaN/O;->e(I)V

    .line 3550
    invoke-virtual {v0, p3}, LaN/O;->h(Z)V

    .line 3551
    if-eqz p4, :cond_26

    .line 3552
    invoke-virtual {p0, v0, v6, v6}, LaN/am;->a(LaN/i;ZZ)Z

    .line 3553
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaN/O;->b(B)V

    .line 3561
    :goto_25
    return-void

    .line 3555
    :cond_26
    invoke-direct {p0, v0}, LaN/am;->a(LaN/i;)V

    goto :goto_25

    .line 3558
    :cond_2a
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_25
.end method

.method private c(Lau/B;)Z
    .registers 11
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 1316
    invoke-direct {p0}, LaN/am;->ae()Lcom/google/googlenav/ui/ap;

    move-result-object v0

    .line 1317
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ap;->h()Lau/B;

    move-result-object v2

    .line 1318
    if-eqz v2, :cond_1e

    .line 1319
    invoke-static {}, Lcom/google/googlenav/ui/bq;->R()[LT/f;

    move-result-object v0

    aget-object v0, v0, v6

    .line 1323
    if-nez v0, :cond_1f

    move v4, v6

    .line 1324
    :goto_14
    const/4 v3, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    move v7, v6

    invoke-virtual/range {v0 .. v8}, LaN/am;->a(Lau/B;Lau/B;IIIIILT/e;)Z

    move-result v6

    .line 1327
    :cond_1e
    return v6

    .line 1323
    :cond_1f
    invoke-interface {v0}, LT/f;->a()I

    move-result v0

    div-int/lit8 v4, v0, 0x2

    goto :goto_14
.end method

.method static synthetic d(LaN/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3, p4}, LaN/am;->f(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private d(Ljava/io/DataInput;IZZ)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 3565
    new-instance v0, LaN/bJ;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-direct {v0, v1, v2, v3, v4}, LaN/bJ;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;)V

    .line 3567
    invoke-virtual {v0, p1}, LaN/bJ;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 3568
    invoke-virtual {v0, p2}, LaN/bJ;->e(I)V

    .line 3569
    invoke-virtual {v0, p3}, LaN/bJ;->h(Z)V

    .line 3570
    if-eqz p4, :cond_24

    .line 3571
    invoke-virtual {p0, v0, v5, v5}, LaN/am;->a(LaN/i;ZZ)Z

    .line 3572
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaN/bJ;->b(B)V

    .line 3580
    :goto_23
    return-void

    .line 3574
    :cond_24
    invoke-direct {p0, v0}, LaN/am;->a(LaN/i;)V

    goto :goto_23

    .line 3577
    :cond_28
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_23
.end method

.method static synthetic e(LaN/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3, p4}, LaN/am;->c(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private e(Ljava/io/DataInput;IZZ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 3584
    new-instance v0, LaN/bT;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    iget-object v5, p0, LaN/am;->h:Lau/k;

    new-instance v6, Lcom/google/googlenav/Y;

    invoke-direct {v6}, Lcom/google/googlenav/Y;-><init>()V

    invoke-direct/range {v0 .. v7}, LaN/bT;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lau/k;Lcom/google/googlenav/F;Z)V

    .line 3586
    invoke-virtual {v0, p1}, LaN/bT;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 3587
    invoke-virtual {v0, p2}, LaN/bT;->e(I)V

    .line 3588
    invoke-virtual {v0, p3}, LaN/bT;->h(Z)V

    .line 3589
    if-eqz p4, :cond_27

    .line 3590
    invoke-virtual {p0, v0, v7, v7}, LaN/am;->a(LaN/i;ZZ)Z

    .line 3598
    :goto_26
    return-void

    .line 3592
    :cond_27
    invoke-direct {p0, v0}, LaN/am;->a(LaN/i;)V

    goto :goto_26

    .line 3595
    :cond_2b
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_26
.end method

.method private f(I)I
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1061
    move v1, v0

    move v2, v0

    .line 1062
    :goto_3
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1f

    .line 1063
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    invoke-virtual {v0}, LaN/i;->av()I

    move-result v0

    if-ne v0, p1, :cond_1b

    .line 1064
    add-int/lit8 v2, v2, 0x1

    .line 1062
    :cond_1b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1067
    :cond_1f
    return v2
.end method

.method static synthetic f(LaN/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3, p4}, LaN/am;->d(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private f(Ljava/io/DataInput;IZZ)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x0

    .line 3602
    new-instance v0, LaN/y;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    iget-object v5, p0, LaN/am;->h:Lau/k;

    invoke-direct/range {v0 .. v5}, LaN/y;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lau/k;)V

    .line 3604
    invoke-virtual {v0, p1}, LaN/y;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_61

    .line 3608
    invoke-virtual {v0}, LaN/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    .line 3609
    const-string v2, "msid:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_47

    .line 3610
    new-instance v7, LaN/bC;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-virtual {v0}, LaN/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v5

    iget-object v6, p0, LaN/am;->h:Lau/k;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, LaN/bC;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;)V

    move-object v0, v7

    .line 3617
    :cond_3b
    :goto_3b
    invoke-virtual {v0, p2}, LaN/y;->e(I)V

    .line 3618
    invoke-virtual {v0, p3}, LaN/y;->h(Z)V

    .line 3619
    if-eqz p4, :cond_5d

    .line 3620
    invoke-virtual {p0, v0, v8, v8}, LaN/am;->a(LaN/i;ZZ)Z

    .line 3628
    :goto_46
    return-void

    .line 3612
    :cond_47
    const-string v2, "LayerTransit"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3b

    .line 3613
    invoke-virtual {v0}, LaN/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    .line 3614
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/m;->a(Ljava/lang/String;)V

    .line 3615
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LaN/am;->a(Lcom/google/googlenav/layer/m;Z)LaN/y;

    move-result-object v0

    goto :goto_3b

    .line 3622
    :cond_5d
    invoke-direct {p0, v0}, LaN/am;->a(LaN/i;)V

    goto :goto_46

    .line 3625
    :cond_61
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_46
.end method

.method private g(I)LaN/i;
    .registers 5
    .parameter

    .prologue
    .line 1822
    packed-switch p1, :pswitch_data_4a

    .line 1847
    :pswitch_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Layer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has MAX_ALLOWED_INSTANCES > 1."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1837
    :pswitch_22
    const/4 v0, 0x0

    move v1, v0

    :goto_24
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_47

    .line 1838
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    invoke-virtual {v0}, LaN/i;->av()I

    move-result v0

    if-ne v0, p1, :cond_43

    .line 1839
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 1850
    :goto_42
    return-object v0

    .line 1837
    :cond_43
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_24

    .line 1850
    :cond_47
    const/4 v0, 0x0

    goto :goto_42

    .line 1822
    nop

    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_22
        :pswitch_22
        :pswitch_22
        :pswitch_3
        :pswitch_22
        :pswitch_3
        :pswitch_22
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_22
        :pswitch_3
        :pswitch_22
        :pswitch_22
        :pswitch_3
        :pswitch_3
        :pswitch_22
        :pswitch_3
        :pswitch_22
        :pswitch_22
        :pswitch_22
        :pswitch_3
        :pswitch_3
        :pswitch_22
    .end packed-switch
.end method

.method private g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;
    .registers 8
    .parameter

    .prologue
    .line 1211
    new-instance v0, Lcom/google/googlenav/n;

    new-instance v1, Lcom/google/googlenav/T;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v2

    iget-object v3, p0, LaN/am;->h:Lau/k;

    iget-object v4, p0, LaN/am;->e:Lau/p;

    iget-object v5, p0, LaN/am;->f:Lau/u;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;Lau/o;Lau/p;Lau/u;)V

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    return-object v0
.end method

.method static synthetic g(LaN/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3, p4}, LaN/am;->e(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private g(Ljava/io/DataInput;IZZ)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x1

    .line 3632
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-nez v0, :cond_c

    .line 3654
    :goto_b
    return-void

    .line 3638
    :cond_c
    new-instance v0, LaN/X;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->n:Lcom/google/googlenav/android/Y;

    iget-object v5, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    iget-object v6, p0, LaN/am;->k:Lcom/google/googlenav/friend/J;

    iget-object v7, p0, LaN/am;->l:Lcom/google/googlenav/friend/p;

    iget-object v8, p0, LaN/am;->m:Lcom/google/googlenav/friend/ag;

    iget-object v9, p0, LaN/am;->G:LaN/a;

    invoke-direct/range {v0 .. v9}, LaN/X;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/android/Y;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;LaN/a;)V

    .line 3641
    invoke-virtual {v0, p1}, LaN/X;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_3c

    .line 3642
    invoke-virtual {v0, p2}, LaN/X;->e(I)V

    .line 3643
    invoke-virtual {v0, p3}, LaN/X;->h(Z)V

    .line 3644
    if-eqz p4, :cond_38

    .line 3645
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v10, v1}, LaN/am;->a(LaN/i;ZZ)Z

    .line 3646
    sput-boolean v10, LaN/am;->v:Z

    goto :goto_b

    .line 3648
    :cond_38
    invoke-direct {p0, v0}, LaN/am;->a(LaN/i;)V

    goto :goto_b

    .line 3651
    :cond_3c
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_b
.end method

.method private g(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1721
    invoke-virtual {p0, p1}, LaN/am;->b(Ljava/lang/String;)LaN/y;

    move-result-object v0

    .line 1722
    if-eqz v0, :cond_9

    .line 1723
    invoke-virtual {p0, v0}, LaN/am;->h(LaN/i;)V

    .line 1725
    :cond_9
    return-void
.end method

.method private g(Z)V
    .registers 5
    .parameter

    .prologue
    .line 2093
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2094
    const/4 v0, 0x0

    :goto_5
    iget-object v2, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_19

    .line 2095
    iget-object v2, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2094
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2097
    :cond_19
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 2098
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, p1}, LaN/am;->b(LaN/i;ZZ)V

    goto :goto_1d

    .line 2100
    :cond_2e
    return-void
.end method

.method private static h(I)C
    .registers 2
    .parameter

    .prologue
    .line 4245
    add-int/lit8 v0, p0, 0x61

    int-to-char v0, v0

    return v0
.end method

.method private h(Lcom/google/googlenav/aZ;)V
    .registers 4
    .parameter

    .prologue
    .line 2824
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, LaN/am;->g(I)LaN/i;

    move-result-object v0

    check-cast v0, LaN/aI;

    .line 2825
    if-nez v0, :cond_17

    .line 2826
    invoke-virtual {p0, p1}, LaN/am;->a(Lcom/google/googlenav/aZ;)LaN/aI;

    move-result-object v0

    .line 2830
    :cond_e
    :goto_e
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LaN/aI;->b(Lcom/google/googlenav/aZ;Z)V

    .line 2831
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LaN/am;->e(Z)V

    .line 2832
    return-void

    .line 2827
    :cond_17
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->af()Z

    move-result v1

    if-nez v1, :cond_e

    .line 2828
    invoke-direct {p0, p1}, LaN/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/aI;->c(Lcom/google/googlenav/F;)V

    goto :goto_e
.end method

.method private h(Ljava/lang/String;)V
    .registers 15
    .parameter

    .prologue
    .line 3317
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v9

    .line 3318
    invoke-interface {v9, p1}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v0

    .line 3319
    if-eqz v0, :cond_11

    array-length v1, v0

    if-nez v1, :cond_12

    .line 3500
    :cond_11
    :goto_11
    return-void

    .line 3324
    :cond_12
    invoke-virtual {p0, p1}, LaN/am;->c(Ljava/lang/String;)Z

    move-result v3

    .line 3326
    :try_start_16
    new-instance v10, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->o:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v10, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 3327
    invoke-virtual {v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3329
    const/4 v0, 0x1

    invoke-virtual {v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v11

    .line 3333
    const/4 v0, 0x0

    move v8, v0

    :goto_27
    if-ge v8, v11, :cond_61

    .line 3334
    const/4 v0, 0x1

    invoke-virtual {v10, v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 3336
    const/4 v1, 0x2

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v2

    .line 3338
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/common/util/e;->a([B)I

    move-result v5

    .line 3343
    sget v1, LaN/am;->w:I

    if-lt v5, v1, :cond_45

    .line 3344
    add-int/lit8 v1, v5, 0x1

    sput v1, LaN/am;->w:I

    .line 3347
    :cond_45
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LAYER_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3348
    invoke-static {v9, v1}, Lcom/google/googlenav/common/j;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v4

    .line 3349
    if-nez v4, :cond_7b

    .line 3354
    invoke-direct {p0}, LaN/am;->ai()V

    .line 3439
    :cond_61
    iget-object v0, p0, LaN/am;->n:Lcom/google/googlenav/android/Y;

    new-instance v1, LaN/as;

    move-object v2, p0

    move-object v4, p1

    move-object v5, v10

    move v6, v11

    invoke-direct/range {v1 .. v6}, LaN/as;-><init>(LaN/am;ZLjava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/Y;->a(Ljava/lang/Runnable;Z)V
    :try_end_70
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_70} :catch_71

    goto :goto_11

    .line 3485
    :catch_71
    move-exception v0

    .line 3496
    const-string v1, "LAYER_MANAGER-LayersManager load"

    invoke-static {v1, v0}, LaU/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3498
    invoke-direct {p0}, LaN/am;->ai()V

    goto :goto_11

    .line 3359
    :cond_7b
    const/4 v1, 0x4

    :try_start_7c
    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v6

    .line 3363
    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v7

    .line 3373
    if-eqz v7, :cond_95

    invoke-direct {p0, v2}, LaN/am;->f(I)I

    move-result v0

    invoke-static {v2}, LaN/am;->c(I)I

    move-result v1

    if-lt v0, v1, :cond_95

    .line 3333
    :goto_91
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_27

    .line 3381
    :cond_95
    iget-object v12, p0, LaN/am;->n:Lcom/google/googlenav/android/Y;

    new-instance v0, LaN/ar;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, LaN/ar;-><init>(LaN/am;IZLjava/io/DataInput;IZZ)V

    const/4 v1, 0x1

    invoke-virtual {v12, v0, v1}, Lcom/google/googlenav/android/Y;->a(Ljava/lang/Runnable;Z)V
    :try_end_a1
    .catch Ljava/lang/Exception; {:try_start_7c .. :try_end_a1} :catch_71

    goto :goto_91
.end method

.method private i(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter

    .prologue
    .line 4197
    const/16 v0, 0x43

    const/16 v1, 0xd

    invoke-static {v1}, LaN/am;->h(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, LaU/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method private i(Lcom/google/googlenav/aZ;)V
    .registers 4
    .parameter

    .prologue
    .line 3028
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->k()Lcom/google/googlenav/ba;

    move-result-object v1

    .line 3029
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 3030
    if-eqz v0, :cond_10

    .line 3031
    invoke-interface {v1, v0, p1}, Lcom/google/googlenav/ba;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/aZ;)V

    .line 3033
    :cond_10
    return-void
.end method

.method private j(Lcom/google/googlenav/aZ;)V
    .registers 10
    .parameter

    .prologue
    .line 3040
    invoke-direct {p0, p1}, LaN/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v5

    .line 3041
    new-instance v0, LaN/cf;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    iget-object v6, p0, LaN/am;->h:Lau/k;

    invoke-static {}, LaN/am;->Y()I

    move-result v7

    invoke-direct/range {v0 .. v7}, LaN/cf;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/n;Lau/k;I)V

    .line 3045
    invoke-virtual {v0}, LaN/cf;->av()I

    move-result v1

    invoke-virtual {p0, v1}, LaN/am;->b(I)V

    .line 3048
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LaN/am;->a(LaN/i;Z)V

    .line 3049
    invoke-virtual {v0}, LaN/cf;->l()V

    .line 3050
    return-void
.end method

.method public static m(LaN/i;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 3086
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LAYER_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LaN/i;->aO()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o(LaN/i;)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1013
    iget-object v2, p0, LaN/am;->q:Ljava/util/Vector;

    monitor-enter v2

    .line 1014
    :try_start_4
    iget-object v1, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v3

    move v1, v0

    :goto_b
    if-ge v1, v3, :cond_2c

    .line 1015
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 1016
    invoke-virtual {v0, p1}, LaN/i;->a(LaN/i;)Z

    move-result v4

    if-eqz v4, :cond_28

    .line 1017
    iget-object v1, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 1018
    invoke-direct {p0, v0}, LaN/am;->c(LaN/i;)V

    .line 1019
    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/am;->A:Z

    .line 1020
    monitor-exit v2

    .line 1024
    :goto_27
    return-void

    .line 1014
    :cond_28
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 1023
    :cond_2c
    monitor-exit v2

    goto :goto_27

    :catchall_2e
    move-exception v0

    monitor-exit v2
    :try_end_30
    .catchall {:try_start_4 .. :try_end_30} :catchall_2e

    throw v0
.end method

.method private static p(LaN/i;)C
    .registers 4
    .parameter

    .prologue
    .line 4250
    invoke-virtual {p0}, LaN/i;->av()I

    move-result v0

    .line 4251
    const/4 v1, 0x6

    if-ne v0, v1, :cond_27

    .line 4252
    check-cast p0, LaN/y;

    invoke-virtual {p0}, LaN/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    .line 4253
    const-string v2, "LayerTransit"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 4254
    const/16 v0, 0x54

    .line 4259
    :goto_1b
    return v0

    .line 4255
    :cond_1c
    const-string v2, "LayerWikipedia"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 4256
    const/16 v0, 0x57

    goto :goto_1b

    .line 4259
    :cond_27
    invoke-static {v0}, LaN/am;->h(I)C

    move-result v0

    goto :goto_1b
.end method


# virtual methods
.method public A()LaN/bw;
    .registers 2

    .prologue
    .line 1916
    const/16 v0, 0x10

    invoke-direct {p0, v0}, LaN/am;->g(I)LaN/i;

    move-result-object v0

    check-cast v0, LaN/bw;

    return-object v0
.end method

.method public B()LaN/X;
    .registers 2

    .prologue
    .line 1925
    const/4 v0, 0x3

    invoke-direct {p0, v0}, LaN/am;->g(I)LaN/i;

    move-result-object v0

    check-cast v0, LaN/X;

    return-object v0
.end method

.method public C()LaN/aI;
    .registers 2

    .prologue
    .line 1939
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, LaN/am;->g(I)LaN/i;

    move-result-object v0

    check-cast v0, LaN/aI;

    return-object v0
.end method

.method protected D()I
    .registers 12

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    const/4 v5, -0x1

    .line 2239
    iget-object v1, p0, LaN/am;->L:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_c

    .line 2276
    :goto_b
    return v5

    .line 2244
    :cond_c
    iget-object v1, p0, LaN/am;->L:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ne v1, v8, :cond_16

    move v5, v0

    .line 2245
    goto :goto_b

    .line 2250
    :cond_16
    const-wide v1, 0x7fffffffffffffffL

    move v4, v5

    move-wide v9, v1

    move-wide v2, v9

    move v1, v0

    .line 2251
    :goto_1f
    iget-object v0, p0, LaN/am;->L:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_47

    .line 2252
    iget-object v0, p0, LaN/am;->L:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/ai;

    .line 2253
    invoke-virtual {v0}, LaN/ai;->e()Z

    move-result v6

    if-eqz v6, :cond_39

    .line 2251
    :cond_35
    :goto_35
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1f

    .line 2256
    :cond_39
    invoke-virtual {v0}, LaN/ai;->d()J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-gez v6, :cond_35

    .line 2258
    invoke-virtual {v0}, LaN/ai;->d()J

    move-result-wide v2

    move v4, v1

    goto :goto_35

    .line 2264
    :cond_47
    if-ne v4, v5, :cond_62

    .line 2265
    invoke-direct {p0}, LaN/am;->af()V

    .line 2269
    iget v0, p0, LaN/am;->O:I

    if-eq v0, v5, :cond_5d

    .line 2270
    iget-object v0, p0, LaN/am;->L:Ljava/util/Vector;

    iget v1, p0, LaN/am;->O:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/ai;

    invoke-virtual {v0, v8}, LaN/ai;->a(Z)V

    .line 2273
    :cond_5d
    invoke-virtual {p0}, LaN/am;->D()I

    move-result v5

    goto :goto_b

    :cond_62
    move v5, v4

    .line 2276
    goto :goto_b
.end method

.method public E()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 2283
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 2289
    if-eqz v0, :cond_2b

    invoke-virtual {v0}, LaN/i;->av()I

    move-result v2

    if-nez v2, :cond_2b

    .line 2290
    check-cast v0, LaN/bj;

    invoke-virtual {v0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2292
    invoke-static {v0}, Lab/b;->e(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    move-object v0, v1

    .line 2296
    :cond_22
    :goto_22
    iget-object v2, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    const-string v3, "22"

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v1, v4}, Lcom/google/googlenav/ui/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2298
    return-void

    :cond_2b
    move-object v0, v1

    goto :goto_22
.end method

.method public F()V
    .registers 1

    .prologue
    .line 2514
    return-void
.end method

.method public G()V
    .registers 6

    .prologue
    .line 2522
    invoke-direct {p0}, LaN/am;->ae()Lcom/google/googlenav/ui/ap;

    move-result-object v0

    .line 2523
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ap;->h()Lau/B;

    move-result-object v1

    .line 2524
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    if-eqz v0, :cond_31

    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    invoke-virtual {v0}, LaN/i;->av()I

    move-result v0

    const/16 v2, 0xd

    if-ne v0, v2, :cond_31

    .line 2526
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    check-cast v0, LaN/C;

    .line 2527
    invoke-virtual {v0}, LaN/C;->bH()Z

    move-result v2

    if-eqz v2, :cond_31

    invoke-virtual {v0}, LaN/C;->aa()Z

    move-result v2

    if-eqz v2, :cond_31

    .line 2529
    if-nez v1, :cond_32

    .line 2530
    invoke-virtual {p0, v0}, LaN/am;->h(LaN/i;)V

    .line 2541
    :cond_31
    :goto_31
    return-void

    .line 2531
    :cond_32
    invoke-virtual {v0}, LaN/C;->bI()Z

    move-result v2

    if-nez v2, :cond_31

    .line 2532
    iget-object v2, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v0}, LaN/C;->a()Lau/B;

    move-result-object v3

    sget v4, LaN/am;->Q:I

    invoke-virtual {v2, v1, v3, v4}, Lau/u;->a(Lau/B;Lau/B;I)Z

    move-result v1

    if-nez v1, :cond_31

    .line 2536
    invoke-virtual {p0, v0}, LaN/am;->h(LaN/i;)V

    goto :goto_31
.end method

.method public H()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 2647
    move v1, v0

    move v2, v0

    .line 2648
    :goto_3
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1c

    .line 2649
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    invoke-virtual {v0}, LaN/i;->X()Z

    move-result v0

    or-int/2addr v2, v0

    .line 2648
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2651
    :cond_1c
    if-eqz v2, :cond_22

    .line 2652
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaN/am;->d(Z)V

    .line 2654
    :cond_22
    return v2
.end method

.method public I()V
    .registers 1

    .prologue
    .line 2663
    return-void
.end method

.method public J()LaN/i;
    .registers 2

    .prologue
    .line 2746
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    goto :goto_9
.end method

.method public K()Ljava/util/Vector;
    .registers 2

    .prologue
    .line 2750
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    return-object v0
.end method

.method public L()Ljava/util/Vector;
    .registers 2

    .prologue
    .line 2754
    iget-object v0, p0, LaN/am;->p:Ljava/util/Vector;

    return-object v0
.end method

.method public M()Ljava/util/Vector;
    .registers 2

    .prologue
    .line 2758
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    return-object v0
.end method

.method public N()V
    .registers 2

    .prologue
    .line 3209
    iget-object v0, p0, LaN/am;->F:Lcom/google/googlenav/layer/r;

    if-eqz v0, :cond_9

    .line 3210
    iget-object v0, p0, LaN/am;->F:Lcom/google/googlenav/layer/r;

    invoke-interface {v0}, Lcom/google/googlenav/layer/r;->a()V

    .line 3216
    :cond_9
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->c(Lcom/google/googlenav/z;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 3218
    invoke-direct {p0}, LaN/am;->ag()V

    .line 3219
    invoke-direct {p0}, LaN/am;->ah()V

    .line 3221
    :cond_17
    return-void
.end method

.method public O()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 3242
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->L()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 3243
    invoke-direct {p0}, LaN/am;->aj()V

    .line 3251
    :cond_e
    sget-object v0, Lal/g;->a:Lal/g;

    invoke-virtual {v0}, Lal/g;->e()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 3252
    iget-object v0, p0, LaN/am;->n:Lcom/google/googlenav/android/Y;

    new-instance v1, LaN/ap;

    invoke-direct {v1, p0}, LaN/ap;-><init>(LaN/am;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/Y;->a(Ljava/lang/Runnable;Z)V

    .line 3261
    :cond_20
    const-string v0, "PROTO_SAVED_LAYER_STATE"

    invoke-direct {p0, v0}, LaN/am;->h(Ljava/lang/String;)V

    .line 3267
    iget-object v0, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 3270
    iget-object v0, p0, LaN/am;->n:Lcom/google/googlenav/android/Y;

    new-instance v1, LaN/aq;

    invoke-direct {v1, p0}, LaN/aq;-><init>(LaN/am;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/Y;->a(Ljava/lang/Runnable;Z)V

    .line 3276
    return-void
.end method

.method public P()V
    .registers 2

    .prologue
    .line 3284
    const-string v0, "PROTO_SAVED_RECENT_LAYERS"

    invoke-direct {p0, v0}, LaN/am;->h(Ljava/lang/String;)V

    .line 3285
    invoke-direct {p0}, LaN/am;->am()V

    .line 3286
    return-void
.end method

.method public Q()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 3688
    invoke-virtual {p0}, LaN/am;->n()LaN/az;

    move-result-object v1

    if-eqz v1, :cond_12

    invoke-virtual {p0}, LaN/am;->n()LaN/az;

    move-result-object v1

    invoke-virtual {v1}, LaN/az;->a()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 3694
    :cond_11
    :goto_11
    return v0

    :cond_12
    iget-object v1, p0, LaN/am;->p:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-gtz v1, :cond_11

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->b()I

    move-result v1

    if-nez v1, :cond_11

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->c()Z

    move-result v1

    if-nez v1, :cond_11

    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v1

    if-eqz v1, :cond_3a

    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v1

    invoke-virtual {v1}, LaN/i;->ah()Z

    move-result v1

    if-eqz v1, :cond_11

    :cond_3a
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v1

    if-eqz v1, :cond_4c

    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v1

    invoke-virtual {v1}, LaN/i;->av()I

    move-result v1

    const/16 v2, 0x16

    if-eq v1, v2, :cond_11

    :cond_4c
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public R()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3710
    iget-object v0, p0, LaN/am;->p:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ne v0, v1, :cond_24

    iget-object v0, p0, LaN/am;->p:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    invoke-virtual {v0}, LaN/i;->av()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_24

    move v0, v1

    .line 3712
    :goto_1a
    if-nez v0, :cond_26

    invoke-virtual {p0}, LaN/am;->Q()Z

    move-result v0

    if-eqz v0, :cond_26

    move v0, v1

    :goto_23
    return v0

    :cond_24
    move v0, v2

    .line 3710
    goto :goto_1a

    :cond_26
    move v0, v2

    .line 3712
    goto :goto_23
.end method

.method public S()V
    .registers 2

    .prologue
    .line 3911
    const/4 v0, 0x0

    iput-object v0, p0, LaN/am;->D:Ljava/lang/String;

    .line 3913
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    if-nez v0, :cond_a

    .line 3931
    :goto_9
    return-void

    .line 3921
    :cond_a
    invoke-virtual {p0}, LaN/am;->A()LaN/bw;

    move-result-object v0

    .line 3922
    if-eqz v0, :cond_13

    .line 3923
    invoke-virtual {v0}, LaN/bw;->Y()V

    .line 3927
    :cond_13
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 3928
    invoke-virtual {v0}, LaN/i;->Y()V

    .line 3930
    iget-object v0, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->V()V

    goto :goto_9
.end method

.method public T()Z
    .registers 2

    .prologue
    .line 3937
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 3938
    if-eqz v0, :cond_c

    invoke-virtual {v0}, LaN/i;->aa()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public U()V
    .registers 7

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 3985
    invoke-direct {p0}, LaN/am;->ad()V

    .line 3988
    invoke-direct {p0, v5, v2}, LaN/am;->b(IZ)V

    .line 3991
    const/16 v0, 0x1a

    invoke-direct {p0, v0, v2}, LaN/am;->b(IZ)V

    .line 3994
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v3

    move v1, v2

    .line 3995
    :goto_12
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_3d

    .line 3996
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 3997
    invoke-virtual {v0}, LaN/i;->av()I

    move-result v4

    if-nez v4, :cond_39

    check-cast v0, LaN/bj;

    invoke-virtual {v0}, LaN/bj;->bl()Z

    move-result v0

    if-nez v0, :cond_39

    .line 3998
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3995
    :cond_39
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12

    .line 4001
    :cond_3d
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_41
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_51

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 4002
    invoke-virtual {p0, v0, v2, v5}, LaN/am;->b(LaN/i;ZZ)V

    goto :goto_41

    .line 4004
    :cond_51
    invoke-virtual {p0}, LaN/am;->d()V

    .line 4007
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/am;->e(LaN/i;)V

    .line 4008
    return-void
.end method

.method protected V()V
    .registers 2

    .prologue
    .line 4012
    const/4 v0, 0x1

    .line 4013
    invoke-virtual {p0, v0}, LaN/am;->a(Z)LaN/bx;

    .line 4014
    invoke-virtual {p0}, LaN/am;->s()V

    .line 4015
    invoke-virtual {p0}, LaN/am;->t()LaN/bw;

    .line 4017
    sget-object v0, Lal/g;->a:Lal/g;

    invoke-virtual {v0}, Lal/g;->e()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 4018
    invoke-virtual {p0}, LaN/am;->m()LaN/az;

    .line 4020
    :cond_15
    return-void
.end method

.method public W()V
    .registers 3

    .prologue
    .line 4039
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 4040
    if-eqz v0, :cond_26

    .line 4041
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->am()Z

    move-result v1

    if-eqz v1, :cond_27

    .line 4047
    invoke-virtual {v0}, LaN/i;->al()V

    .line 4053
    :goto_13
    invoke-virtual {v0}, LaN/i;->av()I

    move-result v0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_26

    invoke-virtual {p0}, LaN/am;->w()LaN/bJ;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 4055
    iget-object v0, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->H()V

    .line 4058
    :cond_26
    return-void

    .line 4049
    :cond_27
    invoke-virtual {v0}, LaN/i;->Z()V

    goto :goto_13
.end method

.method public X()Lcom/google/googlenav/E;
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 4066
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v1

    .line 4069
    if-eqz v1, :cond_e

    invoke-virtual {v1}, LaN/i;->av()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_10

    :cond_e
    move-object v0, v2

    .line 4084
    :goto_f
    return-object v0

    .line 4072
    :cond_10
    invoke-virtual {v1}, LaN/i;->av()I

    move-result v0

    const/16 v3, 0xd

    if-ne v0, v3, :cond_23

    move-object v0, v1

    check-cast v0, LaN/C;

    invoke-virtual {v0}, LaN/C;->bH()Z

    move-result v0

    if-eqz v0, :cond_23

    move-object v0, v2

    .line 4074
    goto :goto_f

    .line 4079
    :cond_23
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-virtual {v1}, LaN/i;->av()I

    move-result v0

    if-nez v0, :cond_48

    invoke-virtual {v1}, LaN/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_48

    .line 4082
    invoke-virtual {v1}, LaN/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    goto :goto_f

    .line 4084
    :cond_48
    invoke-virtual {v1}, LaN/i;->s()Lcom/google/googlenav/E;

    move-result-object v0

    goto :goto_f
.end method

.method public a(Lau/B;Lo/B;Ljava/lang/String;Z[Ljava/lang/String;)LaN/C;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1229
    new-instance v0, Lcom/google/googlenav/ai;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p3, v1}, Lcom/google/googlenav/ai;-><init>(Lau/g;Ljava/lang/String;B)V

    .line 1230
    invoke-virtual {v0, p2}, Lcom/google/googlenav/ai;->a(Lo/B;)V

    .line 1232
    new-instance v1, Lcom/google/googlenav/bl;

    invoke-direct {v1, v0}, Lcom/google/googlenav/bl;-><init>(Lcom/google/googlenav/ai;)V

    .line 1235
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0, p4, p5}, LaN/am;->a(Lcom/google/googlenav/bl;BZ[Ljava/lang/String;)LaN/C;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/bl;BZ[Ljava/lang/String;)LaN/C;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1257
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, LaN/am;->b(I)V

    .line 1259
    invoke-virtual {p1}, Lcom/google/googlenav/bl;->e()Lcom/google/googlenav/E;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()Lau/B;

    move-result-object v0

    .line 1260
    invoke-direct {p0, v0}, LaN/am;->c(Lau/B;)Z

    move-result v8

    .line 1261
    if-eqz v8, :cond_44

    .line 1264
    invoke-direct {p0}, LaN/am;->ae()Lcom/google/googlenav/ui/ap;

    move-result-object v0

    .line 1265
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ap;->h()Lau/B;

    .line 1268
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ap;->e()[Ljava/lang/String;

    move-result-object v7

    .line 1269
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ap;->y()V

    .line 1273
    :goto_21
    if-nez v8, :cond_27

    .line 1274
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaN/am;->d(Z)V

    .line 1279
    :cond_27
    const-string v0, "s"

    invoke-direct {p0, v0}, LaN/am;->i(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v9

    .line 1281
    new-instance v0, LaN/C;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    move-object v5, p1

    move v6, p3

    invoke-direct/range {v0 .. v9}, LaN/C;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/F;Z[Ljava/lang/String;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1285
    const/4 v1, 0x0

    .line 1286
    invoke-virtual {p0, v0, v1, v9}, LaN/am;->a(LaN/i;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1287
    invoke-virtual {v0, p2}, LaN/C;->a(B)V

    .line 1289
    return-object v0

    :cond_44
    move-object v7, p4

    goto :goto_21
.end method

.method public a(Lae/b;)LaN/O;
    .registers 9
    .parameter

    .prologue
    .line 1353
    invoke-virtual {p0}, LaN/am;->i()V

    .line 1355
    new-instance v0, LaN/O;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    iget-object v5, p0, LaN/am;->d:Lao/h;

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, LaN/O;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lao/h;Lcom/google/googlenav/F;)V

    .line 1357
    invoke-virtual {p0, v0}, LaN/am;->f(LaN/i;)V

    .line 1358
    return-object v0
.end method

.method public a(Lcom/google/googlenav/aZ;)LaN/aI;
    .registers 4
    .parameter

    .prologue
    .line 1119
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1120
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1123
    :cond_f
    invoke-direct {p0}, LaN/am;->ad()V

    .line 1125
    invoke-virtual {p0, p1}, LaN/am;->d(Lcom/google/googlenav/aZ;)LaN/aI;

    move-result-object v0

    .line 1126
    invoke-virtual {p0, v0}, LaN/am;->f(LaN/i;)V

    .line 1127
    return-object v0
.end method

.method public a(Lau/B;Lau/Y;Ljava/lang/String;)LaN/aL;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1541
    iget-object v0, p0, LaN/am;->f:Lau/u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lau/u;->a(I)V

    .line 1542
    iget-object v0, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v0, p1, p2}, Lau/u;->e(Lau/B;Lau/Y;)V

    .line 1543
    const/16 v0, 0x16

    invoke-direct {p0, v0}, LaN/am;->g(I)LaN/i;

    move-result-object v0

    check-cast v0, LaN/aL;

    .line 1545
    if-eqz v0, :cond_19

    .line 1546
    invoke-virtual {v0}, LaN/aL;->aW()V

    .line 1549
    :goto_18
    return-object v0

    :cond_19
    invoke-virtual {p0, p3}, LaN/am;->a(Ljava/lang/String;)LaN/aL;

    move-result-object v0

    goto :goto_18
.end method

.method public a(Ljava/lang/String;)LaN/aL;
    .registers 8
    .parameter

    .prologue
    .line 1532
    const/16 v0, 0x16

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1533
    new-instance v0, LaN/aL;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LaN/aL;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Ljava/lang/String;)V

    .line 1535
    invoke-virtual {p0, v0}, LaN/am;->f(LaN/i;)V

    .line 1536
    return-object v0
.end method

.method public a(Lcom/google/googlenav/F;)LaN/aT;
    .registers 9
    .parameter

    .prologue
    .line 1583
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, LaN/am;->b(I)V

    .line 1584
    new-instance v0, LaN/aT;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-static {}, LaN/am;->Y()I

    move-result v6

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, LaN/aT;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/F;I)V

    .line 1587
    invoke-virtual {p0, v0}, LaN/am;->f(LaN/i;)V

    .line 1588
    return-object v0
.end method

.method public a(Lcom/google/googlenav/ai;ZBZZ)LaN/ak;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1778
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, LaN/am;->b(I)V

    .line 1780
    new-instance v0, LaN/ak;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    iget-object v5, p0, LaN/am;->h:Lau/k;

    move-object v6, p1

    move v7, p2

    move v8, p3

    move v9, p4

    invoke-direct/range {v0 .. v9}, LaN/ak;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lau/k;Lcom/google/googlenav/ai;ZBZ)V

    .line 1782
    invoke-virtual {p0, v0, p5}, LaN/am;->a(LaN/i;Z)V

    .line 1783
    return-object v0
.end method

.method protected a(Lcom/google/googlenav/layer/m;)LaN/bD;
    .registers 9
    .parameter

    .prologue
    .line 1620
    new-instance v0, LaN/bD;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    new-instance v6, Lau/k;

    invoke-direct {v6}, Lau/k;-><init>()V

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, LaN/bD;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;)V

    .line 1622
    return-object v0
.end method

.method public a(Lcom/google/googlenav/bR;)LaN/bG;
    .registers 8
    .parameter

    .prologue
    .line 1432
    invoke-virtual {p0}, LaN/am;->k()V

    .line 1434
    new-instance v0, LaN/bG;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LaN/bG;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/F;)V

    .line 1436
    invoke-virtual {p0, v0}, LaN/am;->f(LaN/i;)V

    .line 1437
    return-object v0
.end method

.method public a(Lae/w;)LaN/bJ;
    .registers 8
    .parameter

    .prologue
    .line 1383
    invoke-virtual {p0}, LaN/am;->j()V

    .line 1385
    new-instance v0, LaN/bJ;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LaN/bJ;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/F;)V

    .line 1387
    invoke-virtual {p0, v0}, LaN/am;->f(LaN/i;)V

    .line 1388
    return-object v0
.end method

.method public a(Lcom/google/googlenav/Y;Z)LaN/bT;
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 1420
    invoke-virtual {p0}, LaN/am;->l()V

    .line 1422
    new-instance v0, LaN/bT;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    iget-object v5, p0, LaN/am;->h:Lau/k;

    move-object v6, p1

    move v7, p2

    invoke-direct/range {v0 .. v7}, LaN/bT;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lau/k;Lcom/google/googlenav/F;Z)V

    .line 1424
    invoke-virtual {p0, v0}, LaN/am;->f(LaN/i;)V

    .line 1425
    return-object v0
.end method

.method public a(Lcom/google/googlenav/aZ;Z)LaN/bj;
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1085
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    .line 1086
    if-nez p2, :cond_d

    if-eqz v0, :cond_10

    .line 1087
    :cond_d
    invoke-direct {p0, v1, v1}, LaN/am;->b(IZ)V

    .line 1093
    :cond_10
    if-eqz v0, :cond_16

    .line 1094
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1097
    :cond_16
    invoke-direct {p0}, LaN/am;->ad()V

    .line 1099
    invoke-virtual {p0, v1}, LaN/am;->b(I)V

    .line 1101
    invoke-direct {p0, p1}, LaN/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v5

    .line 1102
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    if-eqz v0, :cond_3e

    const/4 v8, 0x5

    .line 1103
    :goto_27
    new-instance v0, LaN/bj;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v6

    iget-object v7, p0, LaN/am;->h:Lau/k;

    invoke-direct/range {v0 .. v8}, LaN/bj;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;Lau/k;I)V

    .line 1105
    invoke-virtual {p0, v0}, LaN/am;->f(LaN/i;)V

    .line 1106
    return-object v0

    .line 1102
    :cond_3e
    invoke-static {}, LaN/am;->Y()I

    move-result v8

    goto :goto_27
.end method

.method public a(Z)LaN/bx;
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1557
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, LaN/am;->b(I)V

    .line 1558
    new-instance v1, LaN/bx;

    iget-object v2, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v3, p0, LaN/am;->e:Lau/p;

    iget-object v4, p0, LaN/am;->f:Lau/u;

    iget-object v5, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-direct {v1, v2, v3, v4, v5}, LaN/bx;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;)V

    .line 1563
    sget-object v2, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v2}, Lcom/google/googlenav/u;->c(Lcom/google/googlenav/z;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 1565
    invoke-virtual {p0, v1, v0, v0}, LaN/am;->a(LaN/i;ZZ)Z

    .line 1569
    :goto_1d
    return-object v1

    .line 1567
    :cond_1e
    if-nez p1, :cond_21

    const/4 v0, 0x1

    :cond_21
    invoke-virtual {p0, v1, v0}, LaN/am;->a(LaN/i;Z)V

    goto :goto_1d
.end method

.method protected a(Lcom/google/googlenav/layer/m;Z)LaN/y;
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 1637
    new-instance v0, LaN/y;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    iget-object v6, p0, LaN/am;->h:Lau/k;

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, LaN/y;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;)V

    .line 1639
    return-object v0
.end method

.method public a(Lcom/google/googlenav/layer/m;ZZZ)LaN/y;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1662
    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1663
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LaN/am;->a(Lcom/google/googlenav/layer/m;Z)LaN/y;

    move-result-object v3

    :goto_11
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move v5, p4

    .line 1678
    invoke-virtual/range {v0 .. v5}, LaN/am;->a(Lcom/google/googlenav/layer/m;ZLaN/y;ZZ)V

    .line 1680
    return-object v3

    .line 1669
    :cond_1a
    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->i()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_39

    .line 1670
    new-instance v6, Lau/k;

    invoke-direct {v6}, Lau/k;-><init>()V

    .line 1674
    :goto_29
    new-instance v0, LaN/y;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, LaN/y;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;)V

    move-object v3, v0

    goto :goto_11

    .line 1672
    :cond_39
    iget-object v6, p0, LaN/am;->h:Lau/k;

    goto :goto_29
.end method

.method public a()V
    .registers 3

    .prologue
    .line 4299
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, LaN/am;->r:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 4300
    iget-object v0, p0, LaN/am;->r:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/l;

    invoke-interface {v0}, Lcom/google/googlenav/layer/l;->a()V

    .line 4299
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 4302
    :cond_19
    return-void
.end method

.method public a(FF)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4312
    return-void
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 4317
    return-void
.end method

.method public a(ILjava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4164
    iput p1, p0, LaN/am;->B:I

    .line 4165
    iput-object p2, p0, LaN/am;->C:Ljava/lang/Object;

    .line 4166
    return-void
.end method

.method public a(LaJ/w;)V
    .registers 13
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2984
    invoke-direct {p0, v3, v3}, LaN/am;->b(IZ)V

    .line 2989
    invoke-direct {p0, v4, v3}, LaN/am;->b(IZ)V

    .line 2991
    invoke-direct {p0}, LaN/am;->ad()V

    .line 2993
    invoke-virtual {p1}, LaJ/w;->c()Ljava/lang/String;

    move-result-object v0

    .line 2994
    invoke-virtual {p1}, LaJ/w;->h()Lau/B;

    move-result-object v9

    .line 2995
    invoke-virtual {p1}, LaJ/w;->i()Lau/Y;

    move-result-object v10

    .line 2997
    new-instance v1, Lcom/google/googlenav/ai;

    const/16 v2, 0xa

    invoke-direct {v1, v9, v0, v2}, Lcom/google/googlenav/ai;-><init>(Lau/g;Ljava/lang/String;B)V

    .line 2998
    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    .line 3003
    new-array v2, v4, [Lcom/google/googlenav/ai;

    aput-object v1, v2, v3

    iget-object v1, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v1}, Lau/u;->f()Lau/H;

    move-result-object v1

    iget-object v3, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v3}, Lau/u;->a()I

    move-result v3

    iget-object v4, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v4}, Lau/u;->b()I

    move-result v4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;Lau/H;IILcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 3006
    new-instance v1, Lcom/google/googlenav/T;

    sget-object v2, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v3, p0, LaN/am;->h:Lau/k;

    iget-object v4, p0, LaN/am;->e:Lau/p;

    iget-object v5, p0, LaN/am;->f:Lau/u;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;Lau/o;Lau/p;Lau/u;)V

    .line 3008
    new-instance v5, Lcom/google/googlenav/n;

    invoke-direct {v5, v0, v1}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    .line 3009
    new-instance v0, LaN/bj;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    sget-object v6, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v7, p0, LaN/am;->h:Lau/k;

    invoke-static {}, LaN/am;->Y()I

    move-result v8

    invoke-direct/range {v0 .. v8}, LaN/bj;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;Lau/k;I)V

    .line 3011
    iget-object v1, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v1, v9, v10}, Lau/u;->d(Lau/B;Lau/Y;)V

    .line 3012
    invoke-virtual {p0, v0}, LaN/am;->f(LaN/i;)V

    .line 3013
    return-void
.end method

.method public a(LaN/X;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 2880
    iget v0, p0, LaN/am;->B:I

    iget-object v1, p0, LaN/am;->C:Ljava/lang/Object;

    invoke-virtual {p1, v0, v2, v1}, LaN/X;->a(IILjava/lang/Object;)Z

    .line 2884
    iput v2, p0, LaN/am;->B:I

    .line 2885
    const/4 v0, 0x0

    iput-object v0, p0, LaN/am;->C:Ljava/lang/Object;

    .line 2886
    return-void
.end method

.method public a(LaN/aT;)V
    .registers 4
    .parameter

    .prologue
    .line 3838
    invoke-virtual {p1}, LaN/aT;->a()I

    move-result v0

    if-nez v0, :cond_18

    invoke-virtual {p1}, LaN/aT;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->v()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 3840
    invoke-virtual {p1}, LaN/aT;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LaN/am;->b(Lcom/google/googlenav/aZ;Z)V

    .line 3842
    :cond_18
    return-void
.end method

.method public a(LaN/au;)V
    .registers 4
    .parameter

    .prologue
    .line 880
    iget-object v1, p0, LaN/am;->H:Ljava/util/Map;

    monitor-enter v1

    .line 881
    :try_start_3
    iget-object v0, p0, LaN/am;->H:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 882
    monitor-exit v1

    .line 883
    return-void

    .line 882
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public a(LaN/au;Lu/a;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 871
    iget-object v1, p0, LaN/am;->H:Ljava/util/Map;

    monitor-enter v1

    .line 872
    :try_start_3
    iget-object v0, p0, LaN/am;->H:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 873
    monitor-exit v1

    .line 874
    return-void

    .line 873
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method protected a(LaN/i;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2400
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->am()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 2401
    invoke-direct {p0, p1, p2}, LaN/am;->c(LaN/i;I)V

    .line 2409
    :goto_d
    invoke-virtual {p1}, LaN/i;->ag()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 2412
    iget-object v0, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v0}, Lau/u;->c()Lau/B;

    move-result-object v0

    iget-object v1, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v1}, Lau/u;->d()Lau/Y;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LaN/i;->a(Lau/B;Lau/Y;)Lau/B;

    move-result-object v0

    .line 2414
    iget-object v1, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v1, v0}, Lau/u;->b(Lau/B;)V

    .line 2416
    :cond_28
    return-void

    .line 2403
    :cond_29
    invoke-direct {p0, p1, p2}, LaN/am;->b(LaN/i;I)V

    goto :goto_d
.end method

.method public a(LaN/i;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 540
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LaN/am;->a(LaN/i;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 541
    return-void
.end method

.method public a(LaN/i;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 565
    if-nez p2, :cond_f

    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 566
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    invoke-virtual {v0}, LaN/i;->aX()V

    .line 569
    :cond_f
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, LaN/am;->a(LaN/i;ZZ)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 571
    invoke-virtual {p1}, LaN/i;->aW()V

    .line 574
    invoke-virtual {p1}, LaN/i;->aM()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 575
    invoke-static {}, LaN/am;->Z()I

    move-result v0

    invoke-virtual {p1, v0}, LaN/i;->e(I)V

    .line 576
    invoke-direct {p0, p1}, LaN/am;->b(LaN/i;)V

    .line 580
    :cond_29
    invoke-virtual {p0}, LaN/am;->d()V

    .line 583
    :cond_2c
    if-eqz p3, :cond_32

    .line 584
    invoke-direct {p0, p3}, LaN/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 588
    :goto_31
    return-void

    .line 586
    :cond_32
    const-string v0, "s"

    invoke-direct {p0, p1, v0}, LaN/am;->a(LaN/i;Ljava/lang/String;)V

    goto :goto_31
.end method

.method public a(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter

    .prologue
    .line 3074
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 3075
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    invoke-virtual {v0, p1}, LaN/i;->a(Landroid/content/res/Configuration;)V

    .line 3074
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 3077
    :cond_19
    return-void
.end method

.method protected a(Lau/B;)V
    .registers 6
    .parameter

    .prologue
    .line 2150
    iget-object v0, p0, LaN/am;->L:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    .line 2153
    iput-object p1, p0, LaN/am;->N:Lau/B;

    .line 2156
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_10
    if-ltz v1, :cond_25

    .line 2157
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 2158
    iget-object v2, p0, LaN/am;->L:Ljava/util/Vector;

    sget v3, LaN/am;->J:I

    invoke-virtual {v0, v2, p1, v3}, LaN/i;->a(Ljava/util/Vector;Lau/B;I)V

    .line 2156
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_10

    .line 2162
    :cond_25
    invoke-direct {p0}, LaN/am;->ae()Lcom/google/googlenav/ui/ap;

    move-result-object v0

    .line 2163
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ap;->h()Lau/B;

    move-result-object v0

    .line 2164
    if-eqz v0, :cond_49

    .line 2165
    iget-object v1, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v1}, Lau/u;->d()Lau/Y;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lau/B;->a(Lau/B;Lau/Y;)J

    move-result-wide v0

    .line 2166
    sget v2, LaN/am;->J:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_49

    .line 2167
    iget-object v2, p0, LaN/am;->L:Ljava/util/Vector;

    invoke-static {v0, v1}, LaN/ai;->a(J)LaN/ai;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 2170
    :cond_49
    return-void
.end method

.method public a(Lcom/google/googlenav/J;LaN/aT;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x4

    .line 3748
    invoke-virtual {p2}, LaN/aT;->f()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 3757
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->z()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->P()Lau/M;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(Lau/M;)Lcom/google/googlenav/bg;

    move-result-object v0

    iget-object v2, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v2}, Lau/u;->f()Lau/H;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(Lau/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    .line 3764
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->s()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 3774
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, LaN/aT;->be()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3776
    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    .line 3777
    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    .line 3807
    :cond_4c
    :goto_4c
    return-void

    .line 3778
    :cond_4d
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->F()Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 3781
    invoke-virtual {p2}, LaN/aT;->be()Ljava/lang/String;

    move-result-object v0

    .line 3782
    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    .line 3785
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()Lau/H;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Lau/H;)Lcom/google/googlenav/bg;

    .line 3786
    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    goto :goto_4c

    .line 3787
    :cond_6e
    invoke-virtual {p2}, LaN/aT;->b()Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 3788
    invoke-virtual {p2}, LaN/aT;->bf()Lcom/google/googlenav/ai;

    move-result-object v3

    .line 3789
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v0

    .line 3790
    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_86

    .line 3791
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->ak()Ljava/lang/String;

    move-result-object v0

    .line 3793
    :cond_86
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->E()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3795
    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    .line 3798
    invoke-virtual {p2}, LaN/aT;->bf()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/bg;

    .line 3803
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()Lau/H;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Lau/H;)Lcom/google/googlenav/bg;

    .line 3805
    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    goto :goto_4c
.end method

.method public a(Lcom/google/googlenav/aZ;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3023
    invoke-virtual {p0, p1}, LaN/am;->a(Lcom/google/googlenav/F;)LaN/aT;

    move-result-object v0

    .line 3024
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, LaN/aT;->a(Lcom/google/googlenav/aZ;ZI)V

    .line 3025
    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;ILcom/google/googlenav/ba;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3017
    invoke-virtual {p0, p1}, LaN/am;->a(Lcom/google/googlenav/F;)LaN/aT;

    move-result-object v0

    .line 3019
    invoke-virtual {v0, p1, p3, p2}, LaN/aT;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/ba;I)V

    .line 3020
    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/A;Z)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2765
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->k()Lcom/google/googlenav/ba;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 2766
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aC()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ne v0, v3, :cond_18

    .line 2768
    invoke-direct {p0, p1}, LaN/am;->i(Lcom/google/googlenav/aZ;)V

    .line 2820
    :goto_17
    return-void

    .line 2770
    :cond_18
    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->k()Lcom/google/googlenav/ba;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, LaN/am;->a(Lcom/google/googlenav/aZ;ILcom/google/googlenav/ba;)V

    goto :goto_17

    .line 2775
    :cond_21
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->s()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 2776
    invoke-virtual {p0, p1, v2}, LaN/am;->a(Lcom/google/googlenav/aZ;I)V

    .line 2813
    :cond_2a
    :goto_2a
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 2815
    invoke-virtual {p1, v2}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {p0, v0}, LaN/am;->a(Lcom/google/googlenav/ai;)V

    .line 2819
    :cond_39
    invoke-virtual {p2}, Lcom/google/googlenav/A;->d()V

    goto :goto_17

    .line 2777
    :cond_3d
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->H:Z

    if-eqz v0, :cond_4f

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->af()Z

    move-result v0

    if-nez v0, :cond_4f

    .line 2781
    invoke-direct {p0, p1}, LaN/am;->j(Lcom/google/googlenav/aZ;)V

    goto :goto_2a

    .line 2782
    :cond_4f
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ne v0, v3, :cond_68

    invoke-virtual {p2}, Lcom/google/googlenav/A;->a()Z

    move-result v0

    if-eqz v0, :cond_68

    .line 2787
    invoke-virtual {p0, p1, v2}, LaN/am;->a(Lcom/google/googlenav/aZ;Z)LaN/bj;

    .line 2788
    invoke-virtual {p1, v2}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {p2, v0}, Lcom/google/googlenav/A;->a(Lcom/google/googlenav/ai;)V

    goto :goto_2a

    .line 2789
    :cond_68
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-gtz v0, :cond_7a

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->w()Z

    move-result v0

    if-nez v0, :cond_7a

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 2795
    :cond_7a
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->z()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_85

    .line 2796
    invoke-direct {p0, p1, p3}, LaN/am;->c(Lcom/google/googlenav/aZ;Z)V

    goto :goto_2a

    .line 2797
    :cond_85
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->au()Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 2798
    invoke-direct {p0, p1}, LaN/am;->h(Lcom/google/googlenav/aZ;)V

    goto :goto_2a

    .line 2800
    :cond_8f
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->w()Z

    move-result v0

    if-nez v0, :cond_2a

    .line 2801
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->F()Z

    move-result v0

    if-eqz v0, :cond_ab

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->K()Z

    move-result v0

    if-nez v0, :cond_ab

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->N()Z

    move-result v0

    if-nez v0, :cond_ab

    .line 2806
    invoke-virtual {p0, p1, v2}, LaN/am;->a(Lcom/google/googlenav/aZ;I)V

    goto :goto_2a

    .line 2808
    :cond_ab
    invoke-virtual {p0, p1, v3}, LaN/am;->b(Lcom/google/googlenav/aZ;Z)V

    goto/16 :goto_2a
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .registers 4
    .parameter

    .prologue
    .line 3879
    const-string v0, "locationMemory"

    invoke-static {v0}, Lae/z;->b(Ljava/lang/String;)Lae/z;

    move-result-object v0

    .line 3881
    invoke-static {p1}, Lae/y;->b(Lcom/google/googlenav/ai;)Lae/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lae/z;->a(Lae/y;)Z

    .line 3882
    return-void
.end method

.method protected a(Lcom/google/googlenav/layer/m;ZLaN/y;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1685
    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, LaN/am;->a(Lcom/google/googlenav/layer/m;ZLaN/y;ZZ)V

    .line 1686
    return-void
.end method

.method protected a(Lcom/google/googlenav/layer/m;ZLaN/y;ZZ)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x6

    .line 1704
    if-eqz p2, :cond_7

    .line 1705
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, LaN/am;->b(IZ)V

    .line 1708
    :cond_7
    if-eqz p5, :cond_c

    .line 1709
    invoke-direct {p0}, LaN/am;->ad()V

    .line 1712
    :cond_c
    invoke-virtual {p0, v1}, LaN/am;->b(I)V

    .line 1715
    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LaN/am;->g(Ljava/lang/String;)V

    .line 1717
    invoke-virtual {p0, p3, p4}, LaN/am;->a(LaN/i;Z)V

    .line 1718
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/u;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 2688
    move v1, v2

    :goto_2
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 2689
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    invoke-virtual {v0, p1}, LaN/i;->b(Lcom/google/googlenav/ui/u;)V

    .line 2688
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2693
    :cond_19
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    invoke-virtual {v0, p1}, LaN/i;->e(Lcom/google/googlenav/ui/u;)V

    .line 2696
    :goto_20
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_36

    .line 2697
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    invoke-virtual {v0, p1}, LaN/i;->d(Lcom/google/googlenav/ui/u;)V

    .line 2696
    add-int/lit8 v2, v2, 0x1

    goto :goto_20

    .line 2699
    :cond_36
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/jt;LaN/aT;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3850
    invoke-virtual {p2}, LaN/aT;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_30

    .line 3871
    :goto_7
    :pswitch_7
    return-void

    .line 3852
    :pswitch_8
    const-string v0, "100"

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/wizard/jt;->a(Ljava/lang/String;)V

    goto :goto_7

    .line 3855
    :pswitch_e
    invoke-virtual {p2}, LaN/aT;->bg()Lae/b;

    move-result-object v0

    .line 3858
    invoke-virtual {v0}, Lae/b;->y()I

    move-result v1

    if-nez v1, :cond_1c

    .line 3860
    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/wizard/jt;->a(Lae/b;)V

    goto :goto_7

    .line 3862
    :cond_1c
    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/wizard/jt;->c(Lae/b;)V

    goto :goto_7

    .line 3866
    :pswitch_20
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jt;->L()Lcom/google/googlenav/ui/wizard/jA;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jA;->y()Lcom/google/googlenav/ui/wizard/dj;

    move-result-object v0

    .line 3868
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/dj;->j()V

    .line 3869
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/dj;->h()V

    goto :goto_7

    .line 3850
    nop

    :pswitch_data_30
    .packed-switch 0x0
        :pswitch_8
        :pswitch_e
        :pswitch_7
        :pswitch_7
        :pswitch_20
    .end packed-switch
.end method

.method public a(ZZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 3952
    invoke-direct {p0, p2}, LaN/am;->g(Z)V

    .line 3955
    if-eqz p1, :cond_f

    .line 3958
    iget-object v2, p0, LaN/am;->q:Ljava/util/Vector;

    monitor-enter v2

    .line 3959
    :try_start_9
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    .line 3960
    monitor-exit v2
    :try_end_f
    .catchall {:try_start_9 .. :try_end_f} :catchall_2a

    .line 3964
    :cond_f
    if-eqz p3, :cond_14

    .line 3965
    invoke-virtual {p0}, LaN/am;->V()V

    .line 3970
    :cond_14
    iget-object v0, p0, LaN/am;->e:Lau/p;

    invoke-virtual {v0}, Lau/p;->a()Lau/D;

    move-result-object v0

    invoke-virtual {v0}, Lau/D;->l()V

    move v0, v1

    .line 3973
    :goto_1e
    sget-object v2, LaN/am;->t:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2d

    .line 3974
    sget-object v2, LaN/am;->u:[Z

    aput-boolean v1, v2, v0

    .line 3973
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 3960
    :catchall_2a
    move-exception v0

    :try_start_2b
    monitor-exit v2
    :try_end_2c
    .catchall {:try_start_2b .. :try_end_2c} :catchall_2a

    throw v0

    .line 3976
    :cond_2d
    return-void
.end method

.method protected a(LaN/i;ZZ)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 600
    invoke-virtual {p1}, LaN/i;->aS()Z

    move-result v0

    if-nez v0, :cond_9

    .line 660
    :goto_8
    return v2

    .line 608
    :cond_9
    invoke-virtual {p1}, LaN/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_33

    iget-object v0, p0, LaN/am;->p:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    sget v1, LaN/am;->a:I

    if-lt v0, v1, :cond_33

    move v1, v2

    .line 610
    :goto_1a
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_33

    .line 611
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 612
    invoke-virtual {v0}, LaN/i;->aB()Z

    move-result v4

    if-eqz v4, :cond_69

    .line 613
    invoke-virtual {p0, v0, v2, v3}, LaN/am;->b(LaN/i;ZZ)V

    .line 619
    :cond_33
    if-eqz p2, :cond_6d

    .line 620
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1, v2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 629
    :goto_3a
    invoke-virtual {p1}, LaN/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_47

    .line 630
    if-eqz p2, :cond_76

    .line 631
    iget-object v0, p0, LaN/am;->p:Ljava/util/Vector;

    invoke-virtual {v0, p1, v2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 640
    :cond_47
    :goto_47
    invoke-virtual {p1}, LaN/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 641
    invoke-direct {p0, p1}, LaN/am;->o(LaN/i;)V

    .line 644
    :cond_50
    iget-object v0, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/ac;->a(Lcom/google/googlenav/ui/ad;)V

    .line 647
    invoke-virtual {p0, p1}, LaN/am;->j(LaN/i;)V

    .line 650
    if-eqz p3, :cond_67

    .line 651
    invoke-virtual {p1, v3}, LaN/i;->f(I)V

    .line 655
    invoke-virtual {p1}, LaN/i;->at()Z

    move-result v0

    if-eqz v0, :cond_67

    .line 656
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LaN/i;->f(I)V

    :cond_67
    move v2, v3

    .line 660
    goto :goto_8

    .line 610
    :cond_69
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1a

    .line 622
    :cond_6d
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 625
    invoke-virtual {p0, p1}, LaN/am;->d(LaN/i;)V

    goto :goto_3a

    .line 633
    :cond_76
    iget-object v0, p0, LaN/am;->p:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_47
.end method

.method public a(Laa/a;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2104
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v2

    .line 2108
    if-eqz v2, :cond_e

    invoke-virtual {v2}, LaN/i;->aa()Z

    move-result v3

    if-eqz v3, :cond_2f

    .line 2110
    :cond_e
    invoke-virtual {p1}, Laa/a;->e()C

    move-result v3

    const/16 v4, 0x32

    if-ne v3, v4, :cond_1c

    .line 2111
    iget-object v0, p0, LaN/am;->o:Lcom/google/googlenav/ui/wizard/jt;

    invoke-virtual {v0, p0, v1}, Lcom/google/googlenav/ui/wizard/jt;->a(LaN/am;Z)V

    .line 2127
    :goto_1b
    return v1

    .line 2113
    :cond_1c
    invoke-virtual {p1}, Laa/a;->e()C

    move-result v3

    const/16 v4, 0x2a

    if-ne v3, v4, :cond_2f

    .line 2114
    iget-object v2, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/v;->j()LaN/bx;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LaN/bx;->e(ILjava/lang/Object;)V

    goto :goto_1b

    .line 2119
    :cond_2f
    if-eqz v2, :cond_38

    invoke-virtual {v2, p1}, LaN/i;->a(Laa/a;)Z

    move-result v2

    if-eqz v2, :cond_38

    move v0, v1

    .line 2121
    :cond_38
    invoke-virtual {p1}, Laa/a;->c()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4e

    if-nez v0, :cond_4e

    invoke-virtual {p0}, LaN/am;->w()LaN/bJ;

    move-result-object v2

    if-eqz v2, :cond_4e

    .line 2124
    iget-object v0, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->H()V

    move v0, v1

    :cond_4e
    move v1, v0

    .line 2127
    goto :goto_1b
.end method

.method public a(Laa/b;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2457
    iget-object v0, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->D()Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 2460
    invoke-virtual {p0}, LaN/am;->n()LaN/az;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 2461
    invoke-virtual {p0}, LaN/am;->n()LaN/az;

    move-result-object v0

    invoke-virtual {v0, p1}, LaN/az;->a(Laa/b;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 2504
    :cond_1a
    :goto_1a
    return v2

    .line 2471
    :cond_1b
    invoke-virtual {p1}, Laa/b;->h()Z

    move-result v0

    if-nez v0, :cond_27

    invoke-virtual {p1}, Laa/b;->f()Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 2473
    :cond_27
    iget-object v0, p0, LaN/am;->e:Lau/p;

    invoke-virtual {p1}, Laa/b;->k()I

    move-result v3

    invoke-virtual {p1}, Laa/b;->l()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Lau/p;->b(II)Lau/B;

    move-result-object v3

    .line 2477
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 2478
    iget-boolean v4, p0, LaN/am;->M:Z

    if-nez v4, :cond_45

    if-eqz v0, :cond_7a

    invoke-virtual {v0}, LaN/i;->aj()Z

    move-result v0

    if-nez v0, :cond_7a

    :cond_45
    move v0, v2

    .line 2483
    :goto_46
    if-nez v0, :cond_5f

    iget-object v0, p0, LaN/am;->N:Lau/B;

    if-eqz v0, :cond_5f

    iget-object v0, p0, LaN/am;->N:Lau/B;

    iget-object v4, p0, LaN/am;->e:Lau/p;

    invoke-virtual {v4}, Lau/p;->c()Lau/Y;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lau/B;->a(Lau/B;Lau/Y;)J

    move-result-wide v4

    sget v0, LaN/am;->J:I

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-lez v0, :cond_7c

    .line 2486
    :cond_5f
    invoke-virtual {p0, v1}, LaN/am;->d(Z)V

    .line 2487
    invoke-virtual {p0, v3}, LaN/am;->a(Lau/B;)V

    move v0, v2

    .line 2494
    :goto_66
    invoke-virtual {p0, v0}, LaN/am;->c(Z)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 2503
    :cond_6c
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 2504
    if-eqz v0, :cond_78

    invoke-virtual {v0, p1}, LaN/i;->a(Laa/b;)Z

    move-result v0

    if-nez v0, :cond_1a

    :cond_78
    move v2, v1

    goto :goto_1a

    :cond_7a
    move v0, v1

    .line 2478
    goto :goto_46

    .line 2490
    :cond_7c
    invoke-virtual {p0, v3}, LaN/am;->b(Lau/B;)V

    move v0, v1

    goto :goto_66
.end method

.method public a(Lau/B;Lau/B;IIIIILT/e;)Z
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 2561
    if-nez p2, :cond_4

    .line 2637
    :goto_3
    return v2

    .line 2564
    :cond_4
    iget-object v0, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v0}, Lau/u;->d()Lau/Y;

    move-result-object v0

    .line 2565
    invoke-virtual {p1, v0}, Lau/B;->a(Lau/Y;)I

    move-result v1

    invoke-virtual {p2, v0}, Lau/B;->a(Lau/Y;)I

    move-result v3

    sub-int v4, v1, v3

    .line 2566
    invoke-virtual {p1, v0}, Lau/B;->b(Lau/Y;)I

    move-result v1

    invoke-virtual {p2, v0}, Lau/B;->b(Lau/Y;)I

    move-result v0

    sub-int v5, v1, v0

    .line 2573
    packed-switch p3, :pswitch_data_74

    .line 2595
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown Feature.ANCHOR_* type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2575
    :pswitch_3a
    div-int/lit8 v0, p4, 0x2

    neg-int v1, v0

    .line 2576
    neg-int v0, p5

    .line 2610
    :goto_3e
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->i()I

    move-result v3

    .line 2614
    if-ge p4, v3, :cond_4e

    .line 2615
    sub-int v6, v3, p4

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v1, v6

    move p4, v3

    .line 2619
    :cond_4e
    if-ge p5, v3, :cond_56

    .line 2620
    sub-int v6, v3, p5

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v0, v6

    move p5, v3

    .line 2632
    :cond_56
    add-int v3, v1, p4

    .line 2633
    add-int v6, v0, p5

    .line 2637
    if-lt v4, v1, :cond_72

    if-gt v4, v3, :cond_72

    if-lt v5, v0, :cond_72

    if-gt v5, v6, :cond_72

    const/4 v0, 0x1

    :goto_63
    move v2, v0

    goto :goto_3

    .line 2580
    :pswitch_65
    div-int/lit8 v0, p4, 0x2

    neg-int v1, v0

    .line 2581
    div-int/lit8 v0, p5, 0x2

    neg-int v0, v0

    .line 2582
    goto :goto_3e

    .line 2585
    :pswitch_6c
    neg-int v1, p4

    .line 2586
    neg-int v0, p5

    .line 2587
    goto :goto_3e

    .line 2590
    :pswitch_6f
    neg-int v1, p6

    .line 2591
    neg-int v0, p7

    .line 2592
    goto :goto_3e

    :cond_72
    move v0, v2

    .line 2637
    goto :goto_63

    .line 2573
    :pswitch_data_74
    .packed-switch 0x0
        :pswitch_3a
        :pswitch_65
        :pswitch_6c
        :pswitch_6f
    .end packed-switch
.end method

.method public aa()V
    .registers 2

    .prologue
    .line 4143
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 4144
    invoke-virtual {p0, v0}, LaN/am;->e(LaN/i;)V

    .line 4145
    if-eqz v0, :cond_c

    .line 4146
    invoke-virtual {v0}, LaN/i;->aV()V

    .line 4148
    :cond_c
    return-void
.end method

.method public ab()I
    .registers 2

    .prologue
    .line 4173
    iget v0, p0, LaN/am;->B:I

    return v0
.end method

.method public ac()LaN/a;
    .registers 2

    .prologue
    .line 4266
    iget-object v0, p0, LaN/am;->G:LaN/a;

    return-object v0
.end method

.method public b(Lae/b;)LaN/O;
    .registers 5
    .parameter

    .prologue
    .line 1444
    invoke-virtual {p0}, LaN/am;->v()LaN/O;

    move-result-object v0

    .line 1445
    if-eqz v0, :cond_c

    invoke-virtual {v0}, LaN/O;->ae()Z

    move-result v1

    if-nez v1, :cond_11

    .line 1446
    :cond_c
    invoke-virtual {p0, p1}, LaN/am;->a(Lae/b;)LaN/O;

    move-result-object v0

    .line 1469
    :cond_10
    :goto_10
    return-object v0

    .line 1452
    :cond_11
    iget-object v1, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 1453
    iget-object v1, p0, LaN/am;->p:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 1454
    invoke-virtual {p0, v0}, LaN/am;->i(LaN/i;)V

    .line 1455
    invoke-virtual {v0, p1}, LaN/O;->b(Lcom/google/googlenav/F;)V

    .line 1456
    invoke-virtual {v0}, LaN/O;->J()V

    .line 1457
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, LaN/am;->a(LaN/i;ZZ)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1460
    invoke-virtual {v0}, LaN/O;->aM()Z

    move-result v1

    if-eqz v1, :cond_3c

    .line 1461
    invoke-static {}, LaN/am;->Z()I

    move-result v1

    invoke-virtual {v0, v1}, LaN/O;->e(I)V

    .line 1462
    invoke-direct {p0, v0}, LaN/am;->b(LaN/i;)V

    .line 1466
    :cond_3c
    invoke-virtual {p0}, LaN/am;->d()V

    goto :goto_10
.end method

.method public b(Lcom/google/googlenav/aZ;)LaN/x;
    .registers 11
    .parameter

    .prologue
    const/16 v4, 0x28

    const/16 v3, 0x24

    .line 1137
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 1138
    if-eqz v0, :cond_7c

    invoke-virtual {v0}, LaN/i;->av()I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_7c

    const/4 v0, 0x1

    .line 1140
    :goto_13
    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    .line 1145
    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v2

    if-eq v2, v3, :cond_2e

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v2

    if-eq v2, v4, :cond_2e

    .line 1152
    if-nez v0, :cond_7e

    .line 1153
    invoke-direct {p0}, LaN/am;->ad()V

    .line 1162
    :cond_2e
    :goto_2e
    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v2, 0x11

    if-eq v0, v2, :cond_42

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    if-eq v0, v3, :cond_42

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    if-ne v0, v4, :cond_51

    .line 1165
    :cond_42
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 1166
    if-eqz v0, :cond_51

    invoke-virtual {v0}, LaN/i;->ah()Z

    move-result v1

    if-nez v1, :cond_51

    .line 1168
    invoke-virtual {v0}, LaN/i;->aQ()V

    .line 1172
    :cond_51
    invoke-static {}, LaN/am;->Y()I

    move-result v8

    .line 1173
    new-instance v5, Lcom/google/googlenav/n;

    new-instance v0, Lcom/google/googlenav/T;

    sget-object v1, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v2, p0, LaN/am;->h:Lau/k;

    iget-object v3, p0, LaN/am;->e:Lau/p;

    iget-object v4, p0, LaN/am;->f:Lau/u;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;Lau/o;Lau/p;Lau/u;)V

    invoke-direct {v5, p1, v0}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    .line 1175
    new-instance v0, LaN/x;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    sget-object v6, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v7, p0, LaN/am;->h:Lau/k;

    invoke-direct/range {v0 .. v8}, LaN/x;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;Lau/k;I)V

    .line 1178
    invoke-virtual {p0, v0}, LaN/am;->f(LaN/i;)V

    .line 1179
    return-object v0

    .line 1138
    :cond_7c
    const/4 v0, 0x0

    goto :goto_13

    .line 1155
    :cond_7e
    invoke-virtual {p0}, LaN/am;->e()V

    goto :goto_2e
.end method

.method public b(Lcom/google/googlenav/layer/m;Z)LaN/y;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1643
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, LaN/am;->a(Lcom/google/googlenav/layer/m;ZZZ)LaN/y;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)LaN/y;
    .registers 7
    .parameter

    .prologue
    .line 1734
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 1735
    const/4 v0, 0x0

    move v2, v0

    :goto_a
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_41

    .line 1736
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 1737
    invoke-virtual {v0}, LaN/i;->av()I

    move-result v1

    const/4 v4, 0x6

    if-ne v1, v4, :cond_3d

    move-object v1, v0

    .line 1738
    check-cast v1, LaN/y;

    invoke-virtual {v1}, LaN/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1739
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3d

    .line 1740
    check-cast v0, LaN/y;

    .line 1744
    :goto_3c
    return-object v0

    .line 1735
    :cond_3d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_a

    .line 1744
    :cond_41
    const/4 v0, 0x0

    goto :goto_3c
.end method

.method public b()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 452
    invoke-virtual {p0}, LaN/am;->V()V

    .line 456
    iget-object v0, p0, LaN/am;->e:Lau/p;

    invoke-virtual {v0}, Lau/p;->a()Lau/D;

    move-result-object v0

    invoke-virtual {v0}, Lau/D;->l()V

    move v0, v1

    .line 459
    :goto_e
    sget-object v2, LaN/am;->t:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1a

    .line 460
    sget-object v2, LaN/am;->u:[Z

    aput-boolean v1, v2, v0

    .line 459
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 462
    :cond_1a
    return-void
.end method

.method protected b(I)V
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 984
    invoke-static {p1}, LaN/am;->c(I)I

    move-result v0

    .line 986
    invoke-direct {p0, p1}, LaN/am;->f(I)I

    move-result v1

    if-lt v1, v0, :cond_26

    move v1, v2

    .line 987
    :goto_c
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_26

    .line 988
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 989
    invoke-virtual {v0}, LaN/i;->av()I

    move-result v3

    if-ne v3, p1, :cond_27

    .line 992
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, v1}, LaN/am;->b(LaN/i;ZZ)V

    .line 999
    :cond_26
    return-void

    .line 987
    :cond_27
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c
.end method

.method protected b(LaN/i;ZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 809
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 812
    invoke-virtual {p0, p1}, LaN/am;->k(LaN/i;)V

    .line 815
    invoke-virtual {p1, v2}, LaN/i;->h(Z)V

    .line 818
    invoke-virtual {p1}, LaN/i;->aX()V

    .line 821
    invoke-virtual {p1}, LaN/i;->aU()V

    .line 823
    iget-object v1, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 824
    iget-object v1, p0, LaN/am;->p:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 825
    invoke-virtual {p0, p1}, LaN/am;->i(LaN/i;)V

    .line 828
    if-eqz v0, :cond_33

    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    if-eqz v0, :cond_33

    if-eqz p2, :cond_33

    .line 829
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    invoke-virtual {v0}, LaN/i;->aW()V

    .line 833
    :cond_33
    invoke-virtual {p1}, LaN/i;->av()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3d

    .line 834
    invoke-virtual {p0, v2}, LaN/am;->f(Z)V

    .line 837
    :cond_3d
    invoke-virtual {p0, p1}, LaN/am;->d(LaN/i;)V

    .line 839
    invoke-direct {p0, p1}, LaN/am;->a(LaN/i;)V

    .line 844
    invoke-virtual {p1}, LaN/i;->aM()Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 845
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 846
    if-eqz p3, :cond_5c

    invoke-virtual {p1}, LaN/i;->aN()Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 847
    invoke-direct {p0, p1}, LaN/am;->b(LaN/i;)V

    .line 856
    :cond_5c
    :goto_5c
    if-eqz p3, :cond_62

    .line 857
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LaN/i;->f(I)V

    .line 859
    :cond_62
    return-void

    .line 850
    :cond_63
    invoke-direct {p0, p1}, LaN/am;->c(LaN/i;)V

    goto :goto_5c
.end method

.method protected b(Lau/B;)V
    .registers 9
    .parameter

    .prologue
    .line 2180
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, LaN/am;->L:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_6f

    .line 2181
    iget-object v0, p0, LaN/am;->L:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/ai;

    .line 2182
    invoke-virtual {v0}, LaN/ai;->f()Z

    move-result v2

    if-eqz v2, :cond_3c

    .line 2183
    invoke-direct {p0}, LaN/am;->ae()Lcom/google/googlenav/ui/ap;

    move-result-object v2

    .line 2184
    invoke-virtual {v2}, Lcom/google/googlenav/ui/ap;->h()Lau/B;

    move-result-object v2

    .line 2185
    if-eqz v2, :cond_33

    .line 2186
    iget-object v3, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v3}, Lau/u;->d()Lau/Y;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lau/B;->a(Lau/B;Lau/Y;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaN/ai;->b(J)V

    .line 2180
    :goto_2f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2189
    :cond_33
    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {v0, v2, v3}, LaN/ai;->b(J)V

    goto :goto_2f

    .line 2192
    :cond_3c
    invoke-virtual {v0}, LaN/ai;->a()LaN/i;

    move-result-object v4

    .line 2193
    invoke-virtual {v0}, LaN/ai;->b()Lcom/google/googlenav/E;

    move-result-object v2

    invoke-virtual {v4, v2, p1}, LaN/i;->a(Lcom/google/googlenav/E;Lau/B;)J

    move-result-wide v2

    .line 2196
    invoke-virtual {v4}, LaN/i;->ax()Z

    move-result v5

    if-nez v5, :cond_52

    .line 2197
    sget v5, LaN/am;->j:I

    int-to-long v5, v5

    add-long/2addr v2, v5

    .line 2202
    :cond_52
    invoke-virtual {v4}, LaN/i;->av()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5d

    .line 2203
    sget v5, LaN/am;->j:I

    int-to-long v5, v5

    add-long/2addr v2, v5

    .line 2209
    :cond_5d
    invoke-virtual {v4}, LaN/i;->av()I

    move-result v4

    const/16 v5, 0x10

    if-ne v4, v5, :cond_6b

    .line 2210
    sget v4, LaN/am;->j:I

    mul-int/lit8 v4, v4, 0x2

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 2213
    :cond_6b
    invoke-virtual {v0, v2, v3}, LaN/ai;->b(J)V

    goto :goto_2f

    .line 2216
    :cond_6f
    return-void
.end method

.method public b(Lcom/google/googlenav/J;LaN/aT;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3816
    invoke-virtual {p2}, LaN/aT;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_1e

    .line 3827
    :goto_7
    :pswitch_7
    return-void

    .line 3820
    :pswitch_8
    invoke-virtual {p0, p1, p2}, LaN/am;->a(Lcom/google/googlenav/J;LaN/aT;)V

    goto :goto_7

    .line 3823
    :pswitch_c
    iget-object v0, p0, LaN/am;->o:Lcom/google/googlenav/ui/wizard/jt;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jt;->L()Lcom/google/googlenav/ui/wizard/jA;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jA;->i()Lcom/google/googlenav/ui/wizard/bZ;

    move-result-object v0

    invoke-virtual {p2}, LaN/aT;->bg()Lae/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/bZ;->c(Lae/b;)V

    goto :goto_7

    .line 3816
    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_8
        :pswitch_c
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public b(Lcom/google/googlenav/aZ;Z)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2893
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->af()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 2894
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->l()Lcom/google/googlenav/bb;

    move-result-object v0

    check-cast v0, LaN/bj;

    .line 2897
    if-eqz v0, :cond_1f

    invoke-virtual {v0}, LaN/bj;->ax()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 2900
    invoke-virtual {v0}, LaN/bj;->ae()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 2901
    invoke-virtual {v0, p1, p2}, LaN/bj;->b(Lcom/google/googlenav/aZ;Z)V

    .line 2976
    :cond_1f
    :goto_1f
    return-void

    .line 2910
    :cond_20
    const-string v0, "20"

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_73

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-nez v0, :cond_73

    .line 2912
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->l()Lcom/google/googlenav/bb;

    move-result-object v0

    instance-of v0, v0, LaN/bj;

    if-eqz v0, :cond_73

    .line 2913
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->l()Lcom/google/googlenav/bb;

    move-result-object v0

    check-cast v0, LaN/bj;

    .line 2914
    invoke-virtual {v0}, LaN/bj;->ax()Z

    move-result v1

    if-eqz v1, :cond_73

    .line 2915
    invoke-virtual {v0}, LaN/bj;->ae()Z

    move-result v1

    if-eqz v1, :cond_57

    .line 2916
    invoke-direct {p0, p1}, LaN/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/bj;->c(Lcom/google/googlenav/F;)V

    .line 2917
    invoke-virtual {v0, p1, p2}, LaN/bj;->b(Lcom/google/googlenav/aZ;Z)V

    goto :goto_1f

    .line 2919
    :cond_57
    invoke-virtual {v0}, LaN/bj;->af()Z

    move-result v1

    if-eqz v1, :cond_73

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->W()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_73

    .line 2922
    invoke-virtual {v0}, LaN/bj;->h()V

    .line 2923
    invoke-direct {p0, p1}, LaN/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/bj;->c(Lcom/google/googlenav/F;)V

    .line 2924
    invoke-virtual {v0, p1, p2}, LaN/bj;->b(Lcom/google/googlenav/aZ;Z)V

    goto :goto_1f

    :cond_73
    move v2, v3

    .line 2933
    :goto_74
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_c4

    .line 2934
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 2935
    invoke-virtual {v0}, LaN/i;->av()I

    move-result v1

    if-nez v1, :cond_129

    .line 2936
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 2937
    invoke-virtual {v0}, LaN/i;->aK()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c1

    move-object v1, v0

    check-cast v1, LaN/bj;

    invoke-virtual {v1}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_129

    .line 2939
    :cond_c1
    invoke-virtual {p0, v0}, LaN/am;->h(LaN/i;)V

    :cond_c4
    move v2, v3

    .line 2949
    :goto_c5
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_ed

    .line 2950
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 2951
    invoke-virtual {v0}, LaN/i;->av()I

    move-result v1

    if-nez v1, :cond_12e

    move-object v1, v0

    check-cast v1, LaN/bj;

    invoke-virtual {v1}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ae()Z

    move-result v1

    if-eqz v1, :cond_12e

    .line 2953
    check-cast v0, LaN/bj;

    invoke-virtual {v0, v3}, LaN/bj;->k(I)V

    .line 2958
    :cond_ed
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v0

    if-eqz v0, :cond_f6

    .line 2959
    invoke-direct {p0}, LaN/am;->ak()V

    .line 2962
    :cond_f6
    iget-boolean v0, p0, LaN/am;->s:Z

    if-nez v0, :cond_132

    move v0, v4

    :goto_fb
    invoke-virtual {p0, p1, v0}, LaN/am;->a(Lcom/google/googlenav/aZ;Z)LaN/bj;

    move-result-object v0

    .line 2963
    invoke-virtual {v0, p1, p2}, LaN/bj;->b(Lcom/google/googlenav/aZ;Z)V

    .line 2966
    iget-boolean v1, p0, LaN/am;->s:Z

    if-nez v1, :cond_10c

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ae()Z

    move-result v1

    if-eqz v1, :cond_10f

    .line 2967
    :cond_10c
    invoke-virtual {v0, v4}, LaN/bj;->h(Z)V

    .line 2969
    :cond_10f
    invoke-virtual {p0, v3}, LaN/am;->e(Z)V

    .line 2973
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v0

    if-nez v0, :cond_122

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->M()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 2974
    :cond_122
    iget-object v0, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->i()V

    goto/16 :goto_1f

    .line 2933
    :cond_129
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_74

    .line 2949
    :cond_12e
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_c5

    :cond_132
    move v0, v3

    .line 2962
    goto :goto_fb
.end method

.method public b(Lcom/google/googlenav/ui/u;)V
    .registers 4
    .parameter

    .prologue
    .line 2713
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 2714
    if-eqz v0, :cond_26

    .line 2715
    invoke-virtual {v0, p1}, LaN/i;->a(Lcom/google/googlenav/ui/u;)V

    .line 2719
    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->B()Z

    move-result v1

    if-eqz v1, :cond_26

    invoke-virtual {v0}, LaN/i;->ai()Z

    move-result v0

    if-nez v0, :cond_26

    .line 2721
    invoke-virtual {p0}, LaN/am;->y()LaN/bx;

    move-result-object v0

    .line 2722
    if-eqz v0, :cond_26

    iget-object v1, p0, LaN/am;->D:Ljava/lang/String;

    if-eqz v1, :cond_26

    .line 2723
    iget-object v1, p0, LaN/am;->D:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LaN/bx;->a(Lcom/google/googlenav/ui/u;Ljava/lang/String;)V

    .line 2727
    :cond_26
    return-void
.end method

.method public b(Z)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1998
    iget-object v0, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/v;->a(I)Z

    .line 2001
    iget-object v0, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/v;->b_(Z)V

    .line 2002
    iget-object v0, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0, v2, v2}, Lcom/google/googlenav/ui/v;->a(ZZ)V

    .line 2009
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v3

    move v1, v2

    .line 2010
    :goto_16
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_59

    .line 2011
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 2012
    invoke-virtual {v0}, LaN/i;->av()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_3a

    .line 2013
    if-eqz p1, :cond_36

    .line 2014
    invoke-virtual {p0}, LaN/am;->W()V

    .line 2010
    :cond_32
    :goto_32
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_16

    .line 2016
    :cond_36
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_32

    .line 2020
    :cond_3a
    invoke-virtual {v0}, LaN/i;->aB()Z

    move-result v4

    if-eqz v4, :cond_44

    .line 2021
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_32

    .line 2022
    :cond_44
    invoke-virtual {v0}, LaN/i;->aj()Z

    move-result v4

    if-eqz v4, :cond_32

    .line 2023
    invoke-virtual {v0}, LaN/i;->av()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_55

    .line 2025
    invoke-virtual {v0}, LaN/i;->al()V

    goto :goto_32

    .line 2029
    :cond_55
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_32

    .line 2033
    :cond_59
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 2034
    invoke-virtual {p0, v0, v6, v6}, LaN/am;->b(LaN/i;ZZ)V

    goto :goto_5d

    .line 2039
    :cond_6d
    sget-object v0, Lal/g;->a:Lal/g;

    invoke-virtual {v0}, Lal/g;->e()Z

    move-result v0

    if-eqz v0, :cond_82

    invoke-virtual {p0}, LaN/am;->n()LaN/az;

    move-result-object v0

    if-eqz v0, :cond_82

    .line 2040
    invoke-virtual {p0}, LaN/am;->n()LaN/az;

    move-result-object v0

    invoke-virtual {v0, v2}, LaN/az;->k(Z)V

    .line 2045
    :cond_82
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    if-eqz v0, :cond_97

    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    invoke-virtual {v0}, LaN/i;->av()I

    move-result v0

    const/16 v1, 0x16

    if-ne v0, v1, :cond_97

    .line 2047
    invoke-virtual {p0}, LaN/am;->p()V

    .line 2051
    :cond_97
    iget-object v0, p0, LaN/am;->e:Lau/p;

    invoke-virtual {v0}, Lau/p;->a()Lau/D;

    move-result-object v0

    invoke-virtual {v0}, Lau/D;->l()V

    move v0, v2

    .line 2054
    :goto_a1
    sget-object v1, LaN/am;->t:[I

    array-length v1, v1

    if-ge v0, v1, :cond_ad

    .line 2055
    sget-object v1, LaN/am;->u:[Z

    aput-boolean v2, v1, v0

    .line 2054
    add-int/lit8 v0, v0, 0x1

    goto :goto_a1

    .line 2058
    :cond_ad
    invoke-virtual {p0}, LaN/am;->d()V

    .line 2062
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LaN/am;->d(LaN/i;)V

    .line 2063
    return-void
.end method

.method public b(Laa/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 2132
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 2133
    if-eqz v0, :cond_e

    invoke-virtual {v0, p1}, LaN/i;->b(Laa/a;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public c(Lcom/google/googlenav/aZ;)LaN/bj;
    .registers 11
    .parameter

    .prologue
    .line 1187
    invoke-direct {p0, p1}, LaN/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v5

    .line 1188
    invoke-static {}, LaN/am;->Y()I

    move-result v8

    .line 1189
    new-instance v0, LaN/bj;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v6

    iget-object v7, p0, LaN/am;->h:Lau/k;

    invoke-direct/range {v0 .. v8}, LaN/bj;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;Lau/k;I)V

    .line 1191
    return-object v0
.end method

.method public c(Lcom/google/googlenav/layer/m;Z)LaN/y;
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 1758
    new-instance v0, LaN/bC;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    iget-object v6, p0, LaN/am;->h:Lau/k;

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, LaN/bC;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;)V

    .line 1760
    invoke-virtual {p0, p1, p2, v0}, LaN/am;->a(Lcom/google/googlenav/layer/m;ZLaN/y;)V

    .line 1761
    return-object v0
.end method

.method public c()V
    .registers 1

    .prologue
    .line 466
    return-void
.end method

.method public c(Lae/b;)V
    .registers 4
    .parameter

    .prologue
    .line 3061
    invoke-virtual {p0, p1}, LaN/am;->a(Lcom/google/googlenav/F;)LaN/aT;

    move-result-object v0

    .line 3063
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LaN/aT;->a(Lae/b;I)V

    .line 3065
    return-void
.end method

.method public c(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 3305
    const-string v0, "PROTO_SAVED_LAYER_STATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 3306
    if-eqz v0, :cond_10

    iget-object v0, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->as()Z

    move-result v0

    if-nez v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method protected c(Z)Z
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2426
    iget v0, p0, LaN/am;->O:I

    .line 2427
    invoke-virtual {p0}, LaN/am;->D()I

    move-result v3

    iput v3, p0, LaN/am;->O:I

    .line 2428
    iget v3, p0, LaN/am;->O:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_49

    .line 2429
    if-nez p1, :cond_15

    iget v3, p0, LaN/am;->O:I

    if-eq v0, v3, :cond_3b

    .line 2430
    :cond_15
    iget-object v0, p0, LaN/am;->L:Ljava/util/Vector;

    iget v3, p0, LaN/am;->O:I

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/ai;

    .line 2431
    invoke-virtual {v0, v2}, LaN/ai;->a(Z)V

    .line 2433
    invoke-virtual {v0}, LaN/ai;->f()Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 2436
    invoke-virtual {p0}, LaN/am;->h()LaN/C;

    move-result-object v0

    .line 2437
    if-nez v0, :cond_30

    move v0, v1

    .line 2448
    :goto_2f
    return v0

    .line 2440
    :cond_30
    invoke-virtual {v0}, LaN/C;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LaN/am;->a(LaN/i;I)V

    :cond_3b
    :goto_3b
    move v0, v2

    .line 2446
    goto :goto_2f

    .line 2443
    :cond_3d
    invoke-virtual {v0}, LaN/ai;->a()LaN/i;

    move-result-object v1

    invoke-virtual {v0}, LaN/ai;->c()I

    move-result v0

    invoke-virtual {p0, v1, v0}, LaN/am;->a(LaN/i;I)V

    goto :goto_3b

    :cond_49
    move v0, v1

    .line 2448
    goto :goto_2f
.end method

.method public d(Lcom/google/googlenav/aZ;)LaN/aI;
    .registers 11
    .parameter

    .prologue
    .line 1199
    invoke-direct {p0, p1}, LaN/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v5

    .line 1200
    new-instance v0, LaN/aI;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v6

    iget-object v7, p0, LaN/am;->h:Lau/k;

    iget-object v8, p0, LaN/am;->P:Lcom/google/googlenav/offers/j;

    invoke-direct/range {v0 .. v8}, LaN/aI;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;Lau/k;Lcom/google/googlenav/ui/view/dialog/bl;)V

    .line 1202
    return-object v0
.end method

.method protected d()V
    .registers 2

    .prologue
    .line 773
    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/am;->z:Z

    .line 774
    iget-object v0, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-interface {v0}, Lcom/google/googlenav/ui/ac;->e()V

    .line 775
    iget-object v0, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->V()V

    .line 776
    return-void
.end method

.method public d(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 2070
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v3

    move v1, v2

    .line 2071
    :goto_6
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_29

    .line 2072
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 2074
    invoke-virtual {v0}, LaN/i;->av()I

    move-result v4

    if-ne v4, p1, :cond_25

    invoke-virtual {v0}, LaN/i;->aD()Z

    move-result v4

    if-eqz v4, :cond_25

    .line 2075
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2071
    :cond_25
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 2078
    :cond_29
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 2079
    const/4 v3, 0x1

    invoke-virtual {p0, v0, v2, v3}, LaN/am;->b(LaN/i;ZZ)V

    .line 2080
    invoke-direct {p0, v0}, LaN/am;->o(LaN/i;)V

    goto :goto_2d

    .line 2082
    :cond_41
    invoke-virtual {p0}, LaN/am;->d()V

    .line 2083
    return-void
.end method

.method d(LaN/i;)V
    .registers 4
    .parameter

    .prologue
    .line 469
    iget-object v0, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->n()Lcom/google/googlenav/ui/av;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 470
    if-eqz p1, :cond_10

    invoke-virtual {p1}, LaN/i;->aE()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 476
    :cond_10
    if-eqz p1, :cond_23

    invoke-virtual {p1}, LaN/i;->av()I

    move-result v0

    if-nez v0, :cond_23

    const/4 v0, 0x1

    .line 478
    :goto_19
    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->n()Lcom/google/googlenav/ui/av;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/av;->a(Z)V

    .line 481
    :cond_22
    return-void

    .line 476
    :cond_23
    const/4 v0, 0x0

    goto :goto_19
.end method

.method protected d(Z)V
    .registers 2
    .parameter

    .prologue
    .line 2674
    iput-boolean p1, p0, LaN/am;->M:Z

    .line 2675
    return-void
.end method

.method public d(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 3722
    invoke-virtual {p0, p1}, LaN/am;->e(Ljava/lang/String;)LaN/i;

    move-result-object v0

    check-cast v0, LaN/y;

    .line 3723
    if-eqz v0, :cond_d

    .line 3724
    invoke-virtual {v0}, LaN/y;->bG()Z

    move-result v0

    .line 3726
    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public e(Lcom/google/googlenav/aZ;)LaN/X;
    .registers 13
    .parameter

    .prologue
    .line 1479
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LaN/am;->b(I)V

    .line 1483
    new-instance v0, LaN/X;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->n:Lcom/google/googlenav/android/Y;

    iget-object v5, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    iget-object v6, p0, LaN/am;->k:Lcom/google/googlenav/friend/J;

    iget-object v7, p0, LaN/am;->l:Lcom/google/googlenav/friend/p;

    iget-object v8, p0, LaN/am;->m:Lcom/google/googlenav/friend/ag;

    iget-object v10, p0, LaN/am;->G:LaN/a;

    move-object v9, p1

    invoke-direct/range {v0 .. v10}, LaN/X;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/android/Y;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/F;LaN/a;)V

    .line 1486
    invoke-virtual {p0, v0}, LaN/am;->f(LaN/i;)V

    .line 1487
    invoke-virtual {p0, v0}, LaN/am;->a(LaN/X;)V

    .line 1488
    return-object v0
.end method

.method public e(Ljava/lang/String;)LaN/i;
    .registers 6
    .parameter

    .prologue
    .line 3735
    const/4 v0, 0x0

    move v2, v0

    :goto_2
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_2f

    .line 3736
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 3737
    invoke-virtual {v0}, LaN/i;->av()I

    move-result v1

    const/4 v3, 0x6

    if-ne v1, v3, :cond_2b

    move-object v1, v0

    check-cast v1, LaN/y;

    invoke-virtual {v1}, LaN/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 3742
    :goto_2a
    return-object v0

    .line 3735
    :cond_2b
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 3742
    :cond_2f
    const/4 v0, 0x0

    goto :goto_2a
.end method

.method public e()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1042
    const/16 v0, 0x12

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1043
    const/16 v0, 0xf

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1044
    return-void
.end method

.method public e(I)V
    .registers 3
    .parameter

    .prologue
    .line 4169
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LaN/am;->a(ILjava/lang/Object;)V

    .line 4170
    return-void
.end method

.method public e(LaN/i;)V
    .registers 5
    .parameter

    .prologue
    .line 488
    invoke-virtual {p0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 489
    if-eqz v0, :cond_2c

    .line 490
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2d

    .line 491
    invoke-virtual {v0}, LaN/i;->aX()V

    .line 492
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 493
    iget-object v0, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/ac;->b(Lcom/google/googlenav/ui/ad;)V

    .line 494
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 495
    iget-object v0, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/ac;->a(Lcom/google/googlenav/ui/ad;)V

    .line 496
    invoke-virtual {p1}, LaN/i;->aW()V

    .line 497
    invoke-virtual {p0}, LaN/am;->d()V

    .line 499
    invoke-virtual {p0, p1}, LaN/am;->d(LaN/i;)V

    .line 509
    :cond_2c
    :goto_2c
    return-void

    .line 500
    :cond_2d
    invoke-virtual {v0}, LaN/i;->ax()Z

    move-result v1

    if-nez v1, :cond_37

    .line 501
    invoke-virtual {v0}, LaN/i;->aW()V

    goto :goto_2c

    .line 502
    :cond_37
    invoke-virtual {v0}, LaN/i;->k()Lcom/google/googlenav/settings/e;

    move-result-object v1

    if-eqz v1, :cond_2c

    invoke-virtual {v0}, LaN/i;->k()Lcom/google/googlenav/settings/e;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->av()Lcom/google/googlenav/settings/e;

    move-result-object v2

    if-eq v1, v2, :cond_2c

    .line 505
    const/4 v1, 0x1

    .line 506
    invoke-virtual {v0, v1}, LaN/i;->g(Z)Z

    goto :goto_2c
.end method

.method public e(Z)V
    .registers 2
    .parameter

    .prologue
    .line 3896
    iput-boolean p1, p0, LaN/am;->s:Z

    .line 3897
    return-void
.end method

.method public f()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 1053
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1054
    const/16 v0, 0x1a

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1055
    return-void
.end method

.method public f(LaN/i;)V
    .registers 4
    .parameter

    .prologue
    .line 523
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LaN/am;->a(LaN/i;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 524
    return-void
.end method

.method public f(Lcom/google/googlenav/aZ;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1496
    iget-object v0, p0, LaN/am;->G:LaN/a;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1, v2, v2}, LaN/a;->a(ILcom/google/googlenav/F;Lcom/google/googlenav/android/Y;LaN/g;)V

    .line 1498
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 4294
    iput-object p1, p0, LaN/am;->D:Ljava/lang/String;

    .line 4295
    return-void
.end method

.method public f(Z)V
    .registers 2
    .parameter

    .prologue
    .line 3900
    sput-boolean p1, LaN/am;->v:Z

    .line 3901
    return-void
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 4321
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 4324
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v4

    move v1, v2

    .line 4325
    :goto_a
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_25

    .line 4326
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 4327
    invoke-virtual {v0}, LaN/i;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4325
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 4329
    :cond_25
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "visible layers"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4332
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 4333
    iget-object v5, p0, LaN/am;->q:Ljava/util/Vector;

    monitor-enter v5

    move v1, v2

    .line 4334
    :goto_37
    :try_start_37
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_52

    .line 4335
    iget-object v0, p0, LaN/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    .line 4336
    invoke-virtual {v0}, LaN/i;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4334
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_37

    .line 4338
    :cond_52
    monitor-exit v5
    :try_end_53
    .catchall {:try_start_37 .. :try_end_53} :catchall_65

    .line 4339
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "recent layers"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4342
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "LayerManager"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v0

    .line 4338
    :catchall_65
    move-exception v0

    :try_start_66
    monitor-exit v5
    :try_end_67
    .catchall {:try_start_66 .. :try_end_67} :catchall_65

    throw v0
.end method

.method public g(LaN/i;)V
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 673
    invoke-virtual {p1}, LaN/i;->av()I

    move-result v1

    invoke-virtual {p0, v1}, LaN/am;->b(I)V

    .line 676
    invoke-virtual {p1}, LaN/i;->av()I

    move-result v1

    sparse-switch v1, :sswitch_data_b8

    .line 731
    :goto_10
    return-void

    .line 678
    :sswitch_11
    invoke-virtual {p0, v5}, LaN/am;->e(Z)V

    .line 679
    check-cast p1, LaN/bj;

    invoke-virtual {p1}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 680
    new-instance v2, Lcom/google/googlenav/aZ;

    new-instance v3, Lcom/google/googlenav/bg;

    invoke-direct {v3}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->z()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->W()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v3

    const-string v4, "13"

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->C()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v3

    iget-object v4, p0, LaN/am;->f:Lau/u;

    invoke-virtual {v4}, Lau/u;->f()Lau/H;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->a(Lau/H;)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v3

    iget-object v4, p0, LaN/am;->f:Lau/u;

    invoke-direct {v2, v3, v4}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/bf;Lau/u;)V

    .line 689
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->N()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/aZ;->a(Z)V

    .line 690
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aN()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/aZ;->c(Z)V

    .line 692
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v3

    if-eqz v3, :cond_7b

    .line 695
    iget-object v3, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 699
    :cond_7b
    iget-object v3, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v1

    if-eqz v1, :cond_87

    :goto_83
    invoke-virtual {v3, v2, v0, v5}, Lcom/google/googlenav/ui/v;->a(Lcom/google/googlenav/aZ;IZ)V

    goto :goto_10

    :cond_87
    const/4 v0, 0x6

    goto :goto_83

    .line 708
    :sswitch_89
    invoke-virtual {p0, p1}, LaN/am;->f(LaN/i;)V

    .line 710
    invoke-virtual {p1, v5}, LaN/i;->h(Z)V

    goto :goto_10

    .line 713
    :sswitch_90
    check-cast p1, LaN/y;

    invoke-virtual {p1}, LaN/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    .line 715
    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "msid:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_ab

    .line 716
    invoke-virtual {p0, v1, v0}, LaN/am;->c(Lcom/google/googlenav/layer/m;Z)LaN/y;

    move-result-object v0

    .line 721
    :goto_a6
    invoke-virtual {v0, v5}, LaN/y;->h(Z)V

    goto/16 :goto_10

    .line 718
    :cond_ab
    invoke-virtual {p0, v1, v0}, LaN/am;->b(Lcom/google/googlenav/layer/m;Z)LaN/y;

    move-result-object v0

    goto :goto_a6

    .line 724
    :sswitch_b0
    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1, v5, v0}, Lcom/google/googlenav/ui/v;->a(ZZ)V

    goto/16 :goto_10

    .line 676
    nop

    :sswitch_data_b8
    .sparse-switch
        0x0 -> :sswitch_11
        0x1 -> :sswitch_89
        0x3 -> :sswitch_89
        0x6 -> :sswitch_90
        0x7 -> :sswitch_b0
        0x15 -> :sswitch_89
    .end sparse-switch
.end method

.method protected h()LaN/C;
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 1299
    invoke-direct {p0}, LaN/am;->ae()Lcom/google/googlenav/ui/ap;

    move-result-object v0

    .line 1300
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ap;->h()Lau/B;

    move-result-object v1

    .line 1301
    if-eqz v1, :cond_1d

    .line 1302
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ap;->g()Lao/s;

    move-result-object v2

    invoke-static {v2}, Lao/s;->d(Landroid/location/Location;)Lo/B;

    move-result-object v2

    .line 1303
    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ap;->e()[Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LaN/am;->a(Lau/B;Lo/B;Ljava/lang/String;Z[Ljava/lang/String;)LaN/C;

    move-result-object v3

    .line 1306
    :cond_1d
    return-object v3
.end method

.method public h(LaN/i;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 782
    if-nez p1, :cond_4

    .line 788
    :goto_3
    return-void

    .line 785
    :cond_4
    invoke-virtual {p0, p1, v0, v0}, LaN/am;->b(LaN/i;ZZ)V

    .line 786
    invoke-virtual {p0}, LaN/am;->d()V

    .line 787
    const-string v0, "h"

    invoke-direct {p0, p1, v0}, LaN/am;->a(LaN/i;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public i()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1335
    const/16 v0, 0x1a

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1336
    invoke-direct {p0, v1, v1}, LaN/am;->b(IZ)V

    .line 1337
    invoke-direct {p0, v2, v1}, LaN/am;->b(IZ)V

    .line 1340
    invoke-direct {p0}, LaN/am;->ad()V

    .line 1342
    invoke-virtual {p0, v2}, LaN/am;->b(I)V

    .line 1344
    return-void
.end method

.method protected i(LaN/i;)V
    .registers 3
    .parameter

    .prologue
    .line 794
    iget-object v0, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/ac;->b(Lcom/google/googlenav/ui/ad;)V

    .line 795
    return-void
.end method

.method public j()V
    .registers 4

    .prologue
    const/16 v2, 0x15

    const/4 v1, 0x0

    .line 1366
    const/16 v0, 0x1a

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1367
    invoke-direct {p0, v1, v1}, LaN/am;->b(IZ)V

    .line 1368
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1369
    invoke-direct {p0, v2, v1}, LaN/am;->b(IZ)V

    .line 1372
    invoke-direct {p0}, LaN/am;->ad()V

    .line 1374
    invoke-virtual {p0, v2}, LaN/am;->b(I)V

    .line 1375
    return-void
.end method

.method protected j(LaN/i;)V
    .registers 7
    .parameter

    .prologue
    .line 889
    iget-object v2, p0, LaN/am;->H:Ljava/util/Map;

    monitor-enter v2

    .line 890
    :try_start_3
    iget-object v0, p0, LaN/am;->H:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_d
    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/au;

    .line 891
    iget-object v1, p0, LaN/am;->H:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lu/a;

    invoke-virtual {p1}, LaN/i;->av()I

    move-result v4

    invoke-virtual {v1, v4}, Lu/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 892
    invoke-interface {v0, p1}, LaN/au;->a(LaN/i;)V

    goto :goto_d

    .line 895
    :catchall_2f
    move-exception v0

    monitor-exit v2
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    throw v0

    :cond_32
    :try_start_32
    monitor-exit v2
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_2f

    .line 896
    return-void
.end method

.method public k()V
    .registers 3

    .prologue
    .line 1396
    invoke-direct {p0}, LaN/am;->ad()V

    .line 1397
    const/16 v0, 0x17

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1398
    return-void
.end method

.method protected k(LaN/i;)V
    .registers 7
    .parameter

    .prologue
    .line 902
    iget-object v2, p0, LaN/am;->H:Ljava/util/Map;

    monitor-enter v2

    .line 903
    :try_start_3
    iget-object v0, p0, LaN/am;->H:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_d
    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/au;

    .line 904
    iget-object v1, p0, LaN/am;->H:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lu/a;

    invoke-virtual {p1}, LaN/i;->av()I

    move-result v4

    invoke-virtual {v1, v4}, Lu/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 905
    invoke-interface {v0, p1}, LaN/au;->b(LaN/i;)V

    goto :goto_d

    .line 908
    :catchall_2f
    move-exception v0

    monitor-exit v2
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    throw v0

    :cond_32
    :try_start_32
    monitor-exit v2
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_2f

    .line 909
    return-void
.end method

.method public l()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1405
    invoke-direct {p0}, LaN/am;->ad()V

    .line 1406
    const/16 v0, 0x18

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1407
    const/16 v0, 0x17

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1408
    return-void
.end method

.method protected l(LaN/i;)V
    .registers 7
    .parameter

    .prologue
    .line 915
    iget-object v2, p0, LaN/am;->H:Ljava/util/Map;

    monitor-enter v2

    .line 916
    :try_start_3
    iget-object v0, p0, LaN/am;->H:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_d
    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/au;

    .line 917
    iget-object v1, p0, LaN/am;->H:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lu/a;

    invoke-virtual {p1}, LaN/i;->av()I

    move-result v4

    invoke-virtual {v1, v4}, Lu/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 918
    invoke-interface {v0, p1}, LaN/au;->c(LaN/i;)V

    goto :goto_d

    .line 921
    :catchall_2f
    move-exception v0

    monitor-exit v2
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    throw v0

    :cond_32
    :try_start_32
    monitor-exit v2
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_2f

    .line 922
    return-void
.end method

.method public m()LaN/az;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 1502
    const/16 v0, 0x13

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1503
    const/16 v0, 0xd

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1505
    new-instance v0, LaN/az;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-direct {v0, v1, v2, v3, v4}, LaN/az;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;)V

    .line 1507
    invoke-virtual {p0, v0}, LaN/am;->f(LaN/i;)V

    .line 1508
    return-object v0
.end method

.method public n()LaN/az;
    .registers 2

    .prologue
    .line 1517
    const/16 v0, 0x13

    invoke-direct {p0, v0}, LaN/am;->g(I)LaN/i;

    move-result-object v0

    check-cast v0, LaN/az;

    return-object v0
.end method

.method public n(LaN/i;)V
    .registers 3
    .parameter

    .prologue
    .line 4306
    iget-object v0, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-interface {v0}, Lcom/google/googlenav/ui/ac;->e()V

    .line 4307
    return-void
.end method

.method public o()V
    .registers 3

    .prologue
    .line 1522
    const/16 v0, 0x13

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1523
    return-void
.end method

.method public p()V
    .registers 3

    .prologue
    .line 1527
    const/16 v0, 0x16

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1528
    return-void
.end method

.method public q()LaN/bD;
    .registers 3

    .prologue
    .line 1597
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/f;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    .line 1603
    if-nez v0, :cond_11

    .line 1604
    new-instance v0, Lcom/google/googlenav/layer/m;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/google/googlenav/layer/m;-><init>(Ljava/lang/String;)V

    .line 1607
    :cond_11
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, LaN/am;->b(I)V

    .line 1609
    invoke-virtual {p0, v0}, LaN/am;->a(Lcom/google/googlenav/layer/m;)LaN/bD;

    move-result-object v0

    .line 1614
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LaN/am;->a(LaN/i;Z)V

    .line 1615
    return-object v0
.end method

.method public r()V
    .registers 2

    .prologue
    .line 1627
    invoke-virtual {p0}, LaN/am;->z()LaN/bD;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/am;->h(LaN/i;)V

    .line 1628
    return-void
.end method

.method public s()V
    .registers 9

    .prologue
    .line 1790
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LaN/am;->b(IZ)V

    .line 1792
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/f;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_12
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/googlenav/layer/m;

    .line 1793
    new-instance v0, LaN/D;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    iget-object v6, p0, LaN/am;->h:Lau/k;

    invoke-direct/range {v0 .. v6}, LaN/D;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;)V

    .line 1795
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LaN/am;->a(LaN/i;Z)V

    goto :goto_12

    .line 1797
    :cond_32
    return-void
.end method

.method public t()LaN/bw;
    .registers 6

    .prologue
    .line 1805
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LaN/am;->b(I)V

    .line 1807
    new-instance v0, LaN/bw;

    iget-object v1, p0, LaN/am;->c:Lcom/google/googlenav/ui/v;

    iget-object v2, p0, LaN/am;->e:Lau/p;

    iget-object v3, p0, LaN/am;->f:Lau/u;

    iget-object v4, p0, LaN/am;->g:Lcom/google/googlenav/ui/ac;

    invoke-direct {v0, v1, v2, v3, v4}, LaN/bw;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;)V

    .line 1809
    invoke-virtual {p0, v0}, LaN/am;->f(LaN/i;)V

    .line 1810
    return-object v0
.end method

.method public u()LaN/bj;
    .registers 3

    .prologue
    .line 1859
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_26

    .line 1860
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/i;

    invoke-virtual {v0}, LaN/i;->av()I

    move-result v0

    if-nez v0, :cond_22

    .line 1861
    iget-object v0, p0, LaN/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/bj;

    .line 1864
    :goto_21
    return-object v0

    .line 1859
    :cond_22
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 1864
    :cond_26
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public v()LaN/O;
    .registers 2

    .prologue
    .line 1873
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaN/am;->g(I)LaN/i;

    move-result-object v0

    check-cast v0, LaN/O;

    return-object v0
.end method

.method public w()LaN/bJ;
    .registers 2

    .prologue
    .line 1882
    const/16 v0, 0x15

    invoke-direct {p0, v0}, LaN/am;->g(I)LaN/i;

    move-result-object v0

    check-cast v0, LaN/bJ;

    return-object v0
.end method

.method public x()LaN/bT;
    .registers 2

    .prologue
    .line 1891
    const/16 v0, 0x17

    invoke-direct {p0, v0}, LaN/am;->g(I)LaN/i;

    move-result-object v0

    check-cast v0, LaN/bT;

    return-object v0
.end method

.method public y()LaN/bx;
    .registers 2

    .prologue
    .line 1898
    const/4 v0, 0x2

    invoke-direct {p0, v0}, LaN/am;->g(I)LaN/i;

    move-result-object v0

    check-cast v0, LaN/bx;

    return-object v0
.end method

.method public z()LaN/bD;
    .registers 2

    .prologue
    .line 1907
    const/4 v0, 0x7

    invoke-direct {p0, v0}, LaN/am;->g(I)LaN/i;

    move-result-object v0

    check-cast v0, LaN/bD;

    return-object v0
.end method
