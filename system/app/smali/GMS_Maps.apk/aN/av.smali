.class public LaN/av;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/ax;


# instance fields
.field private final a:Lcom/google/googlenav/ui/v;

.field private final b:Lau/u;

.field private final c:Lcom/google/googlenav/ui/wizard/jt;

.field private final d:LaN/am;

.field private final e:Lcom/google/googlenav/ui/ap;

.field private final f:Lcom/google/googlenav/aA;

.field private final g:Lcom/google/android/maps/MapsActivity;

.field private final h:Lcom/google/googlenav/actionbar/b;

.field private i:LaN/ay;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/v;Lcom/google/android/maps/MapsActivity;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    sget-object v0, LaN/ay;->a:LaN/ay;

    iput-object v0, p0, LaN/av;->i:LaN/ay;

    .line 91
    iput-object p1, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    .line 92
    iget-object v0, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->av()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    iput-object v0, p0, LaN/av;->c:Lcom/google/googlenav/ui/wizard/jt;

    .line 93
    iget-object v0, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    iput-object v0, p0, LaN/av;->d:LaN/am;

    .line 94
    iget-object v0, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->t()Lau/u;

    move-result-object v0

    iput-object v0, p0, LaN/av;->b:Lau/u;

    .line 95
    iget-object v0, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->p()Lcom/google/googlenav/ui/ap;

    move-result-object v0

    iput-object v0, p0, LaN/av;->e:Lcom/google/googlenav/ui/ap;

    .line 96
    iget-object v0, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    iput-object v0, p0, LaN/av;->f:Lcom/google/googlenav/aA;

    .line 97
    iput-object p2, p0, LaN/av;->g:Lcom/google/android/maps/MapsActivity;

    .line 99
    new-instance v0, LaN/aw;

    invoke-direct {v0, p0}, LaN/aw;-><init>(LaN/av;)V

    iput-object v0, p0, LaN/av;->h:Lcom/google/googlenav/actionbar/b;

    .line 152
    return-void
.end method

.method static synthetic a(LaN/av;)LaN/ay;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, LaN/av;->i:LaN/ay;

    return-object v0
.end method

.method private a(I)V
    .registers 3
    .parameter

    .prologue
    .line 417
    packed-switch p1, :pswitch_data_1c

    .line 435
    :pswitch_3
    sget-object v0, LaN/ay;->a:LaN/ay;

    iput-object v0, p0, LaN/av;->i:LaN/ay;

    .line 437
    :goto_7
    return-void

    .line 419
    :pswitch_8
    sget-object v0, LaN/ay;->c:LaN/ay;

    iput-object v0, p0, LaN/av;->i:LaN/ay;

    goto :goto_7

    .line 422
    :pswitch_d
    sget-object v0, LaN/ay;->d:LaN/ay;

    iput-object v0, p0, LaN/av;->i:LaN/ay;

    goto :goto_7

    .line 427
    :pswitch_12
    sget-object v0, LaN/ay;->b:LaN/ay;

    iput-object v0, p0, LaN/av;->i:LaN/ay;

    goto :goto_7

    .line 432
    :pswitch_17
    sget-object v0, LaN/ay;->e:LaN/ay;

    iput-object v0, p0, LaN/av;->i:LaN/ay;

    goto :goto_7

    .line 417
    :pswitch_data_1c
    .packed-switch 0x7f11000b
        :pswitch_8
        :pswitch_d
        :pswitch_17
        :pswitch_17
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_12
        :pswitch_12
        :pswitch_12
    .end packed-switch
.end method

.method private a(Landroid/view/MenuItem;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 338
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 342
    if-nez p2, :cond_11

    .line 343
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 367
    :cond_10
    :goto_10
    return-void

    .line 347
    :cond_11
    iget-object v0, p0, LaN/av;->i:LaN/ay;

    sget-object v1, LaN/ay;->b:LaN/ay;

    if-ne v0, v1, :cond_41

    .line 348
    invoke-interface {p1}, Landroid/view/MenuItem;->expandActionView()Z

    .line 351
    iget-object v0, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v0}, LaN/am;->u()LaN/bj;

    move-result-object v0

    .line 352
    if-eqz v0, :cond_35

    .line 353
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    invoke-virtual {v0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/actionbar/a;->a(Ljava/lang/String;)V

    .line 361
    :cond_35
    :goto_35
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    .line 362
    if-eqz v0, :cond_10

    .line 363
    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    goto :goto_10

    .line 356
    :cond_41
    invoke-interface {p1}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 357
    invoke-interface {p1}, Landroid/view/MenuItem;->collapseActionView()Z

    goto :goto_35
.end method

.method static synthetic b(LaN/av;)LaN/am;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, LaN/av;->d:LaN/am;

    return-object v0
.end method

.method private b()LaN/i;
    .registers 5

    .prologue
    .line 381
    iget-object v0, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 382
    if-nez v0, :cond_a

    .line 383
    const/4 v0, 0x0

    .line 408
    :cond_9
    :goto_9
    return-object v0

    .line 385
    :cond_a
    invoke-virtual {v0}, LaN/i;->av()I

    move-result v1

    .line 386
    sget-object v2, LaN/ax;->a:[I

    iget-object v3, p0, LaN/av;->i:LaN/ay;

    invoke-virtual {v3}, LaN/ay;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_44

    goto :goto_9

    .line 388
    :pswitch_1c
    const/4 v2, 0x1

    if-eq v1, v2, :cond_9

    .line 389
    iget-object v0, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v0}, LaN/am;->v()LaN/O;

    move-result-object v0

    goto :goto_9

    .line 393
    :pswitch_26
    const/4 v2, 0x3

    if-eq v1, v2, :cond_9

    .line 394
    iget-object v0, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v0}, LaN/am;->B()LaN/X;

    move-result-object v0

    goto :goto_9

    .line 398
    :pswitch_30
    if-eqz v1, :cond_9

    .line 399
    iget-object v0, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v0}, LaN/am;->u()LaN/bj;

    move-result-object v0

    goto :goto_9

    .line 403
    :pswitch_39
    const/16 v2, 0x17

    if-eq v1, v2, :cond_9

    .line 404
    iget-object v0, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v0}, LaN/am;->x()LaN/bT;

    move-result-object v0

    goto :goto_9

    .line 386
    :pswitch_data_44
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_26
        :pswitch_30
        :pswitch_39
    .end packed-switch
.end method

.method private b(Landroid/view/MenuItem;)V
    .registers 6
    .parameter

    .prologue
    .line 216
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 217
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->b()Z

    move-result v0

    if-nez v0, :cond_20

    .line 218
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    iget-object v1, p0, LaN/av;->g:Lcom/google/android/maps/MapsActivity;

    const/4 v2, 0x0

    iget-object v3, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/actionbar/a;->a(Lcom/google/android/maps/MapsActivity;Landroid/app/Dialog;Lcom/google/googlenav/ui/v;)V

    .line 220
    :cond_20
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    .line 221
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    iget-object v2, p0, LaN/av;->h:Lcom/google/googlenav/actionbar/b;

    invoke-virtual {v1, v0, p1, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    .line 224
    :cond_2f
    return-void
.end method

.method static synthetic c(LaN/av;)Lcom/google/googlenav/ui/v;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/actionbar/b;
    .registers 2

    .prologue
    .line 155
    iget-object v0, p0, LaN/av;->h:Lcom/google/googlenav/actionbar/b;

    return-object v0
.end method

.method public a(Landroid/view/Menu;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 236
    iget-object v0, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 239
    iget-object v1, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/mylocationnotifier/k;->b()Z

    move-result v1

    if-nez v1, :cond_3e

    .line 240
    if-eqz v0, :cond_116

    invoke-virtual {v0}, LaN/i;->aG()I

    move-result v1

    move v6, v1

    .line 242
    :goto_1b
    if-eqz v0, :cond_11c

    invoke-virtual {v0}, LaN/i;->aH()Ljava/lang/CharSequence;

    move-result-object v1

    move-object v5, v1

    .line 244
    :goto_22
    if-eqz v0, :cond_125

    invoke-virtual {v0}, LaN/i;->aI()Ljava/lang/CharSequence;

    move-result-object v1

    .line 250
    :goto_28
    iget-object v2, p0, LaN/av;->i:LaN/ay;

    sget-object v7, LaN/ay;->a:LaN/ay;

    if-ne v2, v7, :cond_36

    iget-object v2, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v2}, LaN/am;->R()Z

    move-result v2

    if-eqz v2, :cond_128

    :cond_36
    move v2, v4

    .line 252
    :goto_37
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v7

    invoke-virtual {v7, v6, v5, v1, v2}, Lcom/google/googlenav/actionbar/a;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 257
    :cond_3e
    const v1, 0x7f1004c3

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 258
    if-eqz v1, :cond_50

    .line 259
    iget-object v2, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/v;->az()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 262
    :cond_50
    const v1, 0x7f1002f2

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 263
    if-eqz v1, :cond_62

    if-eqz v0, :cond_62

    .line 264
    invoke-virtual {v0}, LaN/i;->N()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 267
    :cond_62
    const v1, 0x7f1004c9

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 268
    if-eqz v1, :cond_74

    if-eqz v0, :cond_74

    .line 269
    invoke-virtual {v0}, LaN/i;->M()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 273
    :cond_74
    const v1, 0x7f1004ca

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 274
    if-eqz v1, :cond_8c

    instance-of v2, v0, LaN/O;

    if-eqz v2, :cond_8c

    check-cast v0, LaN/O;

    invoke-virtual {v0}, LaN/O;->bk()Z

    move-result v0

    if-nez v0, :cond_8c

    .line 276
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 281
    :cond_8c
    iget-object v0, p0, LaN/av;->g:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v1

    .line 283
    const v0, 0x7f1004a1

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 284
    if-eqz v2, :cond_a6

    .line 289
    if-eqz v1, :cond_12b

    const v0, 0x7f02025f

    :goto_a0
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 292
    invoke-direct {p0, v2, v1}, LaN/av;->a(Landroid/view/MenuItem;Z)V

    .line 295
    :cond_a6
    const v0, 0x7f10049e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 296
    if-eqz v2, :cond_bd

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_bd

    .line 297
    if-eqz v1, :cond_130

    const v0, 0x7f020232

    :goto_ba
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 301
    :cond_bd
    const v0, 0x7f1004c7

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 302
    if-eqz v2, :cond_d4

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_d4

    .line 303
    if-eqz v1, :cond_134

    const v0, 0x7f020251

    :goto_d1
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 307
    :cond_d4
    const v0, 0x7f1004c8

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 308
    if-eqz v2, :cond_eb

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_eb

    .line 309
    if-eqz v1, :cond_138

    const v0, 0x7f020242

    :goto_e8
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 313
    :cond_eb
    const v0, 0x7f1004c4

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 314
    if-eqz v2, :cond_100

    .line 316
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_13c

    if-eqz v1, :cond_13c

    move v0, v4

    :goto_fd
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 319
    :cond_100
    const v0, 0x7f1004c5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 320
    if-eqz v0, :cond_115

    .line 322
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v2

    if-eqz v2, :cond_112

    if-nez v1, :cond_112

    move v3, v4

    :cond_112
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 324
    :cond_115
    return v4

    .line 240
    :cond_116
    const v1, 0x7f02021f

    move v6, v1

    goto/16 :goto_1b

    .line 242
    :cond_11c
    const/16 v1, 0x2a2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    goto/16 :goto_22

    .line 244
    :cond_125
    const/4 v1, 0x0

    goto/16 :goto_28

    :cond_128
    move v2, v3

    .line 250
    goto/16 :goto_37

    .line 289
    :cond_12b
    const v0, 0x7f020260

    goto/16 :goto_a0

    .line 297
    :cond_130
    const v0, 0x7f020233

    goto :goto_ba

    .line 303
    :cond_134
    const v0, 0x7f020252

    goto :goto_d1

    .line 309
    :cond_138
    const v0, 0x7f020243

    goto :goto_e8

    :cond_13c
    move v0, v3

    .line 316
    goto :goto_fd
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 168
    iget-object v0, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v0}, LaN/am;->J()LaN/i;

    move-result-object v3

    .line 170
    if-eqz v3, :cond_7b

    invoke-virtual {v3}, LaN/i;->aF()I

    move-result v0

    .line 172
    :goto_e
    iget-object v4, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v4}, LaN/am;->w()LaN/bJ;

    move-result-object v4

    if-eqz v4, :cond_20

    .line 174
    iget-object v0, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v0}, LaN/am;->w()LaN/bJ;

    move-result-object v0

    invoke-virtual {v0}, LaN/bJ;->aF()I

    move-result v0

    .line 176
    :cond_20
    invoke-direct {p0, v0}, LaN/av;->a(I)V

    .line 177
    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 178
    invoke-static {p1}, Law/a;->a(Landroid/view/Menu;)V

    .line 180
    iget-object v0, p0, LaN/av;->g:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v4

    .line 182
    const v0, 0x7f1004c4

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 183
    if-eqz v5, :cond_44

    .line 184
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_84

    if-eqz v4, :cond_84

    move v0, v1

    :goto_41
    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 187
    :cond_44
    const v0, 0x7f1004c5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 188
    if-eqz v0, :cond_59

    .line 189
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v5

    if-eqz v5, :cond_56

    if-nez v4, :cond_56

    move v2, v1

    :cond_56
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 195
    :cond_59
    const v0, 0x7f10012c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_6b

    if-eqz v3, :cond_6b

    .line 197
    invoke-virtual {v3}, LaN/i;->aJ()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 200
    :cond_6b
    invoke-static {p1}, Law/a;->b(Landroid/view/Menu;)V

    .line 202
    const v0, 0x7f1004a1

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_7a

    .line 204
    invoke-direct {p0, v0}, LaN/av;->b(Landroid/view/MenuItem;)V

    .line 206
    :cond_7a
    return v1

    .line 170
    :cond_7b
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ao()I

    move-result v0

    goto :goto_e

    :cond_84
    move v0, v2

    .line 184
    goto :goto_41
.end method

.method public a(Landroid/view/MenuItem;)Z
    .registers 9
    .parameter

    .prologue
    const/16 v6, 0x36

    const/16 v5, 0x34

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 451
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 452
    const v3, 0x102002c

    if-ne v2, v3, :cond_29

    .line 453
    iget-object v1, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v1

    .line 454
    invoke-virtual {v1}, Lcom/google/googlenav/mylocationnotifier/k;->b()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 456
    invoke-virtual {v1}, Lcom/google/googlenav/mylocationnotifier/k;->h()V

    .line 457
    invoke-virtual {v1}, Lcom/google/googlenav/mylocationnotifier/k;->c()V

    .line 591
    :goto_22
    :sswitch_22
    return v0

    .line 461
    :cond_23
    iget-object v1, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v1, v0}, LaN/am;->b(Z)V

    goto :goto_22

    .line 466
    :cond_29
    const-string v3, "m"

    invoke-static {p1, v3}, Law/a;->a(Landroid/view/MenuItem;Ljava/lang/String;)V

    .line 469
    sget-object v3, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v3}, Lcom/google/googlenav/u;->c(Lcom/google/googlenav/z;)Z

    move-result v3

    if-eqz v3, :cond_38

    move v0, v1

    .line 471
    goto :goto_22

    .line 475
    :cond_38
    iget-object v3, p0, LaN/av;->g:Lcom/google/android/maps/MapsActivity;

    if-eqz v3, :cond_65

    iget-object v3, p0, LaN/av;->g:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v3}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v3

    if-nez v3, :cond_65

    .line 476
    const v3, 0x7f1004a1

    if-ne v2, v3, :cond_56

    .line 478
    :cond_49
    iget-object v0, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    const/16 v2, 0x327

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/v;->a(Ljava/lang/String;)V

    move v0, v1

    .line 479
    goto :goto_22

    .line 476
    :cond_56
    const v3, 0x7f10049e

    if-eq v2, v3, :cond_49

    const v3, 0x7f1004c7

    if-eq v2, v3, :cond_49

    const v3, 0x7f1004c8

    if-eq v2, v3, :cond_49

    .line 483
    :cond_65
    invoke-direct {p0}, LaN/av;->b()LaN/i;

    move-result-object v3

    .line 484
    sparse-switch v2, :sswitch_data_14c

    :cond_6c
    :goto_6c
    move v0, v1

    .line 591
    goto :goto_22

    .line 486
    :sswitch_6e
    if-eqz v3, :cond_74

    .line 487
    invoke-virtual {v3}, LaN/i;->aY()Z

    goto :goto_22

    :cond_74
    move v0, v1

    .line 490
    goto :goto_22

    .line 493
    :sswitch_76
    if-eqz v3, :cond_81

    .line 494
    new-instance v2, Laa/a;

    invoke-direct {v2, v5, v5, v1, v1}, Laa/a;-><init>(IIIZ)V

    invoke-virtual {v3, v2}, LaN/i;->e(Laa/a;)Z

    goto :goto_22

    :cond_81
    move v0, v1

    .line 498
    goto :goto_22

    .line 501
    :sswitch_83
    if-eqz v3, :cond_8e

    .line 502
    new-instance v2, Laa/a;

    invoke-direct {v2, v6, v6, v1, v1}, Laa/a;-><init>(IIIZ)V

    invoke-virtual {v3, v2}, LaN/i;->e(Laa/a;)Z

    goto :goto_22

    :cond_8e
    move v0, v1

    .line 506
    goto :goto_22

    .line 508
    :sswitch_90
    if-eqz v3, :cond_9f

    invoke-virtual {v3}, LaN/i;->av()I

    move-result v2

    if-ne v2, v0, :cond_9f

    .line 509
    const/16 v1, 0xec

    const/4 v2, -0x1

    invoke-virtual {v3, v1, v2, v4}, LaN/i;->a(IILjava/lang/Object;)Z

    goto :goto_22

    :cond_9f
    move v0, v1

    .line 513
    goto :goto_22

    .line 516
    :sswitch_a1
    iget-object v2, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v2, v1}, LaN/am;->b(Z)V

    .line 517
    iget-object v1, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->au()V

    goto/16 :goto_22

    .line 522
    :sswitch_ad
    iget-object v0, p0, LaN/av;->g:Lcom/google/android/maps/MapsActivity;

    if-eqz v0, :cond_6c

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-nez v0, :cond_6c

    .line 523
    iget-object v0, p0, LaN/av;->g:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->onSearchRequested()Z

    goto :goto_6c

    .line 528
    :sswitch_c1
    iget-object v1, p0, LaN/av;->c:Lcom/google/googlenav/ui/wizard/jt;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jt;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 529
    iget-object v1, p0, LaN/av;->c:Lcom/google/googlenav/ui/wizard/jt;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jt;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v2, v0, v4}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 531
    iget-object v1, p0, LaN/av;->c:Lcom/google/googlenav/ui/wizard/jt;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jt;->L()Lcom/google/googlenav/ui/wizard/jA;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jA;->i()Lcom/google/googlenav/ui/wizard/bZ;

    move-result-object v1

    iget-object v2, p0, LaN/av;->b:Lau/u;

    invoke-virtual {v2}, Lau/u;->p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/bZ;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_22

    .line 536
    :sswitch_ed
    iget-object v2, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    const/16 v3, 0x13c

    invoke-virtual {v2, v1, v3}, Lcom/google/googlenav/ui/v;->a(ZI)V

    goto/16 :goto_22

    .line 539
    :sswitch_f6
    iget-object v1, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/v;->b(I)V

    goto/16 :goto_22

    .line 542
    :sswitch_fe
    iget-object v1, p0, LaN/av;->e:Lcom/google/googlenav/ui/ap;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/ap;->a(Z)V

    goto/16 :goto_22

    .line 545
    :sswitch_105
    iget-object v1, p0, LaN/av;->c:Lcom/google/googlenav/ui/wizard/jt;

    iget-object v2, p0, LaN/av;->d:LaN/am;

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/jt;->a(LaN/am;Z)V

    goto/16 :goto_22

    .line 548
    :sswitch_10e
    iget-object v1, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/v;->g(Ljava/lang/String;)V

    goto/16 :goto_22

    .line 551
    :sswitch_115
    iget-object v1, p0, LaN/av;->f:Lcom/google/googlenav/aA;

    invoke-interface {v1}, Lcom/google/googlenav/aA;->h()V

    goto/16 :goto_22

    .line 554
    :sswitch_11c
    iget-object v1, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->J()V

    goto/16 :goto_22

    .line 557
    :sswitch_123
    iget-object v1, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->K()V

    goto/16 :goto_22

    .line 561
    :sswitch_12a
    iget-object v1, p0, LaN/av;->f:Lcom/google/googlenav/aA;

    invoke-static {}, Lcom/google/googlenav/K;->X()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    goto/16 :goto_22

    .line 564
    :sswitch_135
    const-string v2, "m"

    const-string v3, ""

    invoke-static {v2, v3}, LaA/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    iget-object v2, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/v;->q(Z)V

    goto/16 :goto_22

    .line 570
    :sswitch_143
    iget-object v1, p0, LaN/av;->a:Lcom/google/googlenav/ui/v;

    const-string v2, "offline"

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/v;->g(Ljava/lang/String;)V

    goto/16 :goto_22

    .line 484
    :sswitch_data_14c
    .sparse-switch
        0x66 -> :sswitch_22
        0x67 -> :sswitch_22
        0x7f10012c -> :sswitch_6e
        0x7f1002f2 -> :sswitch_83
        0x7f10049e -> :sswitch_c1
        0x7f1004a1 -> :sswitch_ad
        0x7f1004b3 -> :sswitch_c1
        0x7f1004bc -> :sswitch_12a
        0x7f1004c3 -> :sswitch_a1
        0x7f1004c4 -> :sswitch_135
        0x7f1004c5 -> :sswitch_143
        0x7f1004c6 -> :sswitch_115
        0x7f1004c7 -> :sswitch_f6
        0x7f1004c8 -> :sswitch_105
        0x7f1004c9 -> :sswitch_76
        0x7f1004ca -> :sswitch_90
        0x7f1004cb -> :sswitch_fe
        0x7f1004cc -> :sswitch_ed
        0x7f1004cd -> :sswitch_10e
        0x7f1004d6 -> :sswitch_11c
        0x7f1004d7 -> :sswitch_123
    .end sparse-switch
.end method
