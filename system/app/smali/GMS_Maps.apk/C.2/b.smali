.class public LC/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LC/c;


# instance fields
.field private final a:Lo/T;

.field private final b:F

.field private final c:F

.field private final d:F

.field private final e:F


# direct methods
.method public constructor <init>(Lo/T;FFFF)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Lo/T;

    invoke-virtual {p1}, Lo/T;->f()I

    move-result v1

    invoke-virtual {p1}, Lo/T;->g()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    iput-object v0, p0, LC/b;->a:Lo/T;

    .line 96
    const/high16 v0, 0x41a8

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v1, 0x4000

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LC/b;->b:F

    .line 97
    iput p3, p0, LC/b;->c:F

    .line 98
    iput p4, p0, LC/b;->d:F

    .line 99
    iput p5, p0, LC/b;->e:F

    .line 100
    return-void
.end method

.method public static a(F)F
    .registers 5
    .parameter

    .prologue
    .line 347
    const/4 v0, 0x0

    cmpl-float v0, p0, v0

    if-lez v0, :cond_1c

    float-to-double v0, p0

    const-wide v2, 0x3fb999999999999aL

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    neg-double v0, v0

    const-wide v2, 0x3ff7154760000000L

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4010

    add-double/2addr v0, v2

    double-to-float v0, v0

    :goto_1b
    return v0

    :cond_1c
    const/high16 v0, 0x4200

    goto :goto_1b
.end method

.method private static a(FFF)F
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 294
    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method

.method private static a(FFFF)F
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 323
    const v0, 0x40490fdb

    mul-float/2addr v0, p2

    invoke-static {v0}, Landroid/util/FloatMath;->sin(F)F

    move-result v0

    .line 324
    invoke-static {p0}, LC/b;->b(F)F

    move-result v1

    .line 325
    invoke-static {p1}, LC/b;->b(F)F

    move-result v2

    .line 329
    const/high16 v3, 0x3f80

    sub-float/2addr v3, p2

    mul-float/2addr v1, v3

    mul-float/2addr v2, p2

    add-float/2addr v1, v2

    const-wide/high16 v2, 0x3fe0

    float-to-double v4, v0

    const-wide v6, 0x3ff3333333333333L

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    float-to-double v4, p3

    const-wide v6, 0x3fd999999999999aL

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v0, v2

    add-float/2addr v0, v1

    .line 331
    const/high16 v1, 0x4320

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 332
    invoke-static {v0}, LC/b;->a(F)F

    move-result v0

    .line 333
    const/high16 v1, 0x4000

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 334
    return v0
.end method

.method public static a(LC/b;LC/b;FF)LC/b;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v6, 0x4334

    const/high16 v4, 0x3f80

    const/high16 v9, 0x43b4

    .line 255
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-nez v0, :cond_4c

    .line 256
    iget-object v0, p0, LC/b;->a:Lo/T;

    iget-object v1, p1, LC/b;->a:Lo/T;

    invoke-virtual {v0, v1, p2}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v1

    .line 257
    iget v0, p0, LC/b;->b:F

    iget v2, p1, LC/b;->b:F

    invoke-static {v0, v2, p2}, LC/b;->a(FFF)F

    move-result v2

    .line 267
    :goto_1b
    iget v0, p0, LC/b;->c:F

    iget v3, p1, LC/b;->c:F

    invoke-static {v0, v3, p2}, LC/b;->a(FFF)F

    move-result v3

    .line 272
    iget v4, p0, LC/b;->d:F

    .line 273
    iget v0, p1, LC/b;->d:F

    .line 274
    cmpl-float v5, v4, v0

    if-lez v5, :cond_6b

    .line 275
    sub-float v5, v4, v0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_32

    .line 276
    sub-float/2addr v4, v9

    .line 283
    :cond_32
    :goto_32
    invoke-static {v4, v0, p2}, LC/b;->a(FFF)F

    move-result v4

    .line 284
    float-to-double v5, v4

    const-wide/16 v7, 0x0

    cmpg-double v0, v5, v7

    if-gez v0, :cond_3e

    .line 285
    add-float/2addr v4, v9

    .line 288
    :cond_3e
    iget v0, p0, LC/b;->e:F

    iget v5, p1, LC/b;->e:F

    invoke-static {v0, v5, p2}, LC/b;->a(FFF)F

    move-result v5

    .line 290
    new-instance v0, LC/b;

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    return-object v0

    .line 262
    :cond_4c
    iget-object v0, p0, LC/b;->a:Lo/T;

    iget-object v1, p1, LC/b;->a:Lo/T;

    const v2, 0x40490fdb

    sub-float v3, p2, v4

    mul-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    add-float/2addr v2, v4

    const/high16 v3, 0x4000

    div-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v1

    .line 264
    iget v0, p0, LC/b;->b:F

    iget v2, p1, LC/b;->b:F

    invoke-static {v0, v2, p2, p3}, LC/b;->a(FFFF)F

    move-result v2

    goto :goto_1b

    .line 279
    :cond_6b
    sub-float v5, v0, v4

    cmpl-float v5, v5, v6

    if-lez v5, :cond_32

    .line 280
    sub-float/2addr v0, v9

    goto :goto_32
.end method

.method public static b(F)F
    .registers 7
    .parameter

    .prologue
    .line 358
    const-wide/high16 v0, 0x4024

    const-wide/high16 v2, 0x4010

    float-to-double v4, p0

    sub-double/2addr v2, v4

    const-wide v4, 0x3ff7154760000000L

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public static b(Landroid/os/Bundle;)LC/b;
    .registers 7
    .parameter

    .prologue
    .line 228
    new-instance v0, LC/b;

    const-string v1, "vector.lat"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "vector.lng"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, v2}, Lo/T;->b(II)Lo/T;

    move-result-object v1

    const-string v2, "vector.zoom"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v2

    const-string v3, "vector.viewing_angle"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v3

    const-string v4, "vector.bearing"

    invoke-virtual {p0, v4}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v4

    const-string v5, "vector.lookahead"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v5

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    return-object v0
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 147
    iget v0, p0, LC/b;->b:F

    return v0
.end method

.method public a(LC/b;)LC/b;
    .registers 8
    .parameter

    .prologue
    const/high16 v3, 0x4000

    .line 194
    iget-object v0, p0, LC/b;->a:Lo/T;

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v0

    iget-object v1, p1, LC/b;->a:Lo/T;

    invoke-virtual {v1}, Lo/T;->f()I

    move-result v1

    sub-int/2addr v0, v1

    .line 195
    const/high16 v1, 0x2000

    if-le v0, v1, :cond_33

    .line 196
    new-instance v0, LC/b;

    new-instance v1, Lo/T;

    iget-object v2, p0, LC/b;->a:Lo/T;

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v2

    sub-int/2addr v2, v3

    iget-object v3, p0, LC/b;->a:Lo/T;

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lo/T;-><init>(II)V

    iget v2, p0, LC/b;->b:F

    iget v3, p0, LC/b;->c:F

    iget v4, p0, LC/b;->d:F

    iget v5, p0, LC/b;->e:F

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    .line 202
    :goto_32
    return-object v0

    .line 198
    :cond_33
    const/high16 v1, -0x2000

    if-ge v0, v1, :cond_57

    .line 199
    new-instance v0, LC/b;

    new-instance v1, Lo/T;

    iget-object v2, p0, LC/b;->a:Lo/T;

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v2

    add-int/2addr v2, v3

    iget-object v3, p0, LC/b;->a:Lo/T;

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lo/T;-><init>(II)V

    iget v2, p0, LC/b;->b:F

    iget v3, p0, LC/b;->c:F

    iget v4, p0, LC/b;->d:F

    iget v5, p0, LC/b;->e:F

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    goto :goto_32

    :cond_57
    move-object v0, p0

    .line 202
    goto :goto_32
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 212
    const-string v0, "vector.lat"

    iget-object v1, p0, LC/b;->a:Lo/T;

    invoke-virtual {v1}, Lo/T;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 213
    const-string v0, "vector.lng"

    iget-object v1, p0, LC/b;->a:Lo/T;

    invoke-virtual {v1}, Lo/T;->c()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 214
    const-string v0, "vector.viewing_angle"

    iget v1, p0, LC/b;->c:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 215
    const-string v0, "vector.bearing"

    iget v1, p0, LC/b;->d:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 216
    const-string v0, "vector.zoom"

    iget v1, p0, LC/b;->b:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 217
    const-string v0, "vector.lookahead"

    iget v1, p0, LC/b;->e:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 218
    return-void
.end method

.method public b()LC/b;
    .registers 1

    .prologue
    .line 370
    return-object p0
.end method

.method public c()Lo/T;
    .registers 2

    .prologue
    .line 155
    iget-object v0, p0, LC/b;->a:Lo/T;

    invoke-static {v0}, Lo/T;->a(Lo/T;)Lo/T;

    move-result-object v0

    return-object v0
.end method

.method public d()F
    .registers 2

    .prologue
    .line 163
    iget v0, p0, LC/b;->c:F

    return v0
.end method

.method public e()F
    .registers 2

    .prologue
    .line 170
    iget v0, p0, LC/b;->d:F

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 117
    if-ne p0, p1, :cond_5

    .line 128
    :cond_4
    :goto_4
    return v0

    .line 120
    :cond_5
    instance-of v2, p1, LC/b;

    if-eqz v2, :cond_37

    .line 121
    check-cast p1, LC/b;

    .line 122
    iget-object v2, p0, LC/b;->a:Lo/T;

    iget-object v3, p1, LC/b;->a:Lo/T;

    invoke-virtual {v2, v3}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_35

    iget v2, p0, LC/b;->b:F

    iget v3, p1, LC/b;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_35

    iget v2, p0, LC/b;->c:F

    iget v3, p1, LC/b;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_35

    iget v2, p0, LC/b;->d:F

    iget v3, p1, LC/b;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_35

    iget v2, p0, LC/b;->e:F

    iget v3, p1, LC/b;->e:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_4

    :cond_35
    move v0, v1

    goto :goto_4

    :cond_37
    move v0, v1

    .line 128
    goto :goto_4
.end method

.method public f()F
    .registers 2

    .prologue
    .line 179
    iget v0, p0, LC/b;->e:F

    return v0
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 133
    .line 135
    iget v0, p0, LC/b;->b:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    add-int/lit8 v0, v0, 0x25

    .line 136
    mul-int/lit8 v0, v0, 0x25

    iget v1, p0, LC/b;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    mul-int/lit8 v0, v0, 0x25

    iget v1, p0, LC/b;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    mul-int/lit8 v0, v0, 0x25

    iget v1, p0, LC/b;->e:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    mul-int/lit8 v1, v0, 0x25

    iget-object v0, p0, LC/b;->a:Lo/T;

    if-nez v0, :cond_2c

    const/4 v0, 0x0

    :goto_2a
    add-int/2addr v0, v1

    .line 140
    return v0

    .line 139
    :cond_2c
    iget-object v0, p0, LC/b;->a:Lo/T;

    invoke-virtual {v0}, Lo/T;->hashCode()I

    move-result v0

    goto :goto_2a
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 363
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[target:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LC/b;->a:Lo/T;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " zoom:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LC/b;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " viewingAngle:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LC/b;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bearing:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LC/b;->d:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lookAhead:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LC/b;->e:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
