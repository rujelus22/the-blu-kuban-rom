.class public Lw/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private volatile a:F

.field private final b:F

.field private c:Lw/c;


# direct methods
.method public constructor <init>(F)V
    .registers 3
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/high16 v0, 0x4296

    iput v0, p0, Lw/b;->a:F

    .line 56
    iput p1, p0, Lw/b;->b:F

    .line 57
    return-void
.end method

.method public static b(F)F
    .registers 6
    .parameter

    .prologue
    const/high16 v4, 0x4160

    const/high16 v3, 0x4120

    const/high16 v0, 0x41f0

    .line 122
    const/high16 v1, 0x4180

    cmpl-float v1, p0, v1

    if-ltz v1, :cond_f

    .line 123
    const/high16 v0, 0x4296

    .line 135
    :cond_e
    :goto_e
    return v0

    .line 124
    :cond_f
    cmpl-float v1, p0, v4

    if-lez v1, :cond_1d

    .line 125
    const/high16 v1, 0x4000

    .line 127
    const/high16 v2, 0x4234

    sub-float v3, p0, v4

    mul-float/2addr v0, v3

    div-float/2addr v0, v1

    add-float/2addr v0, v2

    goto :goto_e

    .line 129
    :cond_1d
    cmpl-float v1, p0, v3

    if-lez v1, :cond_e

    .line 130
    const/high16 v1, 0x4080

    .line 131
    const/high16 v2, 0x4170

    .line 132
    sub-float v3, p0, v3

    mul-float/2addr v2, v3

    div-float v1, v2, v1

    add-float/2addr v0, v1

    goto :goto_e
.end method


# virtual methods
.method public a(LC/b;)LC/b;
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 87
    invoke-virtual {p1}, LC/b;->d()F

    move-result v4

    .line 88
    iget v0, p0, Lw/b;->a:F

    invoke-virtual {p1}, LC/b;->a()F

    move-result v1

    invoke-static {v1}, Lw/b;->b(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 89
    const/high16 v1, 0x41a8

    .line 90
    const/high16 v0, 0x4000

    .line 91
    iget-object v2, p0, Lw/b;->c:Lw/c;

    if-eqz v2, :cond_33

    .line 92
    iget-object v2, p0, Lw/b;->c:Lw/c;

    invoke-virtual {p1}, LC/b;->c()Lo/T;

    move-result-object v5

    invoke-interface {v2, v5}, Lw/c;->a(Lo/T;)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 93
    iget-object v2, p0, Lw/b;->c:Lw/c;

    invoke-interface {v2}, Lw/c;->a()F

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 96
    :cond_33
    invoke-virtual {p1}, LC/b;->a()F

    move-result v2

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 97
    invoke-virtual {p1}, LC/b;->c()Lo/T;

    move-result-object v1

    .line 98
    cmpl-float v0, v4, v3

    if-lez v0, :cond_58

    .line 99
    invoke-virtual {v1, v1}, Lo/T;->h(Lo/T;)V

    .line 100
    new-instance v0, LC/b;

    invoke-virtual {p1}, LC/b;->e()F

    move-result v4

    invoke-virtual {p1}, LC/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    .line 112
    :goto_57
    return-object v0

    .line 102
    :cond_58
    cmpg-float v0, v4, v6

    if-gez v0, :cond_6e

    .line 103
    invoke-virtual {v1, v1}, Lo/T;->h(Lo/T;)V

    .line 104
    new-instance v0, LC/b;

    invoke-virtual {p1}, LC/b;->e()F

    move-result v4

    invoke-virtual {p1}, LC/b;->f()F

    move-result v5

    move v3, v6

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    goto :goto_57

    .line 106
    :cond_6e
    invoke-virtual {p1}, LC/b;->a()F

    move-result v0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_80

    invoke-virtual {p1}, LC/b;->c()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->j()Z

    move-result v0

    if-eqz v0, :cond_95

    .line 108
    :cond_80
    invoke-virtual {v1, v1}, Lo/T;->h(Lo/T;)V

    .line 109
    new-instance v0, LC/b;

    invoke-virtual {p1}, LC/b;->d()F

    move-result v3

    invoke-virtual {p1}, LC/b;->e()F

    move-result v4

    invoke-virtual {p1}, LC/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    goto :goto_57

    :cond_95
    move-object v0, p1

    .line 112
    goto :goto_57
.end method

.method public a()Lw/c;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Lw/b;->c:Lw/c;

    return-object v0
.end method

.method public a(F)V
    .registers 2
    .parameter

    .prologue
    .line 78
    iput p1, p0, Lw/b;->a:F

    .line 79
    return-void
.end method

.method public a(Lw/c;)V
    .registers 2
    .parameter

    .prologue
    .line 61
    iput-object p1, p0, Lw/b;->c:Lw/c;

    .line 62
    return-void
.end method

.method public b()F
    .registers 2

    .prologue
    .line 70
    iget v0, p0, Lw/b;->a:F

    return v0
.end method
