.class public LK/N;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LK/T;


# instance fields
.field private final a:LK/T;


# direct methods
.method public constructor <init>(LK/T;)V
    .registers 2
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, LK/N;->a:LK/T;

    .line 53
    return-void
.end method


# virtual methods
.method public a(LK/V;)LK/a;
    .registers 7
    .parameter

    .prologue
    .line 70
    const/4 v0, 0x0

    .line 71
    instance-of v1, p1, LK/W;

    if-eqz v1, :cond_2e

    .line 72
    check-cast p1, LK/W;

    .line 73
    iget-object v1, p0, LK/N;->a:LK/T;

    invoke-virtual {p1}, LK/W;->e()LK/V;

    move-result-object v2

    invoke-interface {v1, v2}, LK/T;->a(LK/V;)LK/a;

    move-result-object v1

    .line 74
    iget-object v2, p0, LK/N;->a:LK/T;

    invoke-virtual {p1}, LK/W;->f()LK/V;

    move-result-object v3

    invoke-interface {v2, v3}, LK/T;->a(LK/V;)LK/a;

    move-result-object v2

    .line 75
    if-eqz v1, :cond_2d

    if-eqz v2, :cond_2d

    .line 76
    new-instance v0, LK/K;

    const/4 v3, 0x2

    new-array v3, v3, [LK/a;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v2, v3, v1

    invoke-direct {v0, v3}, LK/K;-><init>([LK/a;)V

    .line 81
    :cond_2d
    :goto_2d
    return-object v0

    .line 79
    :cond_2e
    iget-object v0, p0, LK/N;->a:LK/T;

    invoke-interface {v0, p1}, LK/T;->a(LK/V;)LK/a;

    move-result-object v0

    goto :goto_2d
.end method

.method public a()V
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, LK/N;->a:LK/T;

    invoke-interface {v0}, LK/T;->a()V

    .line 87
    return-void
.end method

.method public a(LK/V;LK/U;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 57
    instance-of v0, p1, LK/W;

    if-eqz v0, :cond_1e

    .line 58
    check-cast p1, LK/W;

    .line 59
    new-instance v0, LK/O;

    invoke-direct {v0, p1, p2}, LK/O;-><init>(LK/W;LK/U;)V

    .line 61
    iget-object v1, p0, LK/N;->a:LK/T;

    invoke-virtual {p1}, LK/W;->e()LK/V;

    move-result-object v2

    invoke-interface {v1, v2, v0}, LK/T;->a(LK/V;LK/U;)V

    .line 62
    iget-object v1, p0, LK/N;->a:LK/T;

    invoke-virtual {p1}, LK/W;->f()LK/V;

    move-result-object v2

    invoke-interface {v1, v2, v0}, LK/T;->a(LK/V;LK/U;)V

    .line 66
    :goto_1d
    return-void

    .line 64
    :cond_1e
    iget-object v0, p0, LK/N;->a:LK/T;

    invoke-interface {v0, p1, p2}, LK/T;->a(LK/V;LK/U;)V

    goto :goto_1d
.end method

.method public b()V
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, LK/N;->a:LK/T;

    invoke-interface {v0}, LK/T;->b()V

    .line 92
    return-void
.end method
