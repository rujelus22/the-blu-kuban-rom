.class public LK/w;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Ljava/util/Map;

.field private final c:Ljava/util/SortedMap;

.field private final d:Ljava/util/List;

.field private final e:Ljava/io/File;

.field private final f:Ljava/util/zip/ZipFile;


# direct methods
.method constructor <init>(Ljava/io/File;Ljava/util/zip/ZipFile;Ljava/io/InputStream;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    iput-object p1, p0, LK/w;->e:Ljava/io/File;

    .line 165
    iput-object p2, p0, LK/w;->f:Ljava/util/zip/ZipFile;

    .line 167
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LK/w;->a:Ljava/util/Map;

    .line 168
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LK/w;->b:Ljava/util/Map;

    .line 169
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LK/w;->c:Ljava/util/SortedMap;

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LK/w;->d:Ljava/util/List;

    .line 172
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 173
    invoke-direct {p0, v0}, LK/w;->a(Ljava/io/Reader;)V

    .line 174
    return-void
.end method

.method private a(I)LK/A;
    .registers 4
    .parameter

    .prologue
    .line 375
    iget-object v0, p0, LK/w;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 376
    iget-object v0, p0, LK/w;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, LK/w;->a(Ljava/lang/String;)LK/A;

    move-result-object v0

    .line 378
    :goto_1c
    return-object v0

    :cond_1d
    invoke-static {}, LK/A;->b()LK/A;

    move-result-object v0

    goto :goto_1c
.end method

.method private a(Ljava/lang/String;)LK/A;
    .registers 3
    .parameter

    .prologue
    .line 408
    if-nez p1, :cond_7

    .line 409
    invoke-static {}, LK/A;->a()LK/A;

    move-result-object v0

    .line 411
    :goto_6
    return-object v0

    :cond_7
    invoke-direct {p0, p1}, LK/w;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LK/A;->a(Ljava/io/File;)LK/A;

    move-result-object v0

    goto :goto_6
.end method

.method public static a(Ljava/io/File;)LK/w;
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 187
    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    .line 194
    :try_start_5
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v3, ".zip"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 195
    new-instance v0, Ljava/util/zip/ZipFile;

    const/4 v3, 0x1

    invoke-direct {v0, p0, v3}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;I)V

    .line 197
    const-string v3, "messages.xml"

    invoke-virtual {v0, v3}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v3

    .line 198
    invoke-virtual {v0, v3}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v1

    .line 203
    :goto_21
    new-instance v3, LK/w;

    invoke-direct {v3, v2, v0, v1}, LK/w;-><init>(Ljava/io/File;Ljava/util/zip/ZipFile;Ljava/io/InputStream;)V
    :try_end_26
    .catchall {:try_start_5 .. :try_end_26} :catchall_35

    .line 205
    if-eqz v1, :cond_2b

    .line 206
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 210
    :cond_2b
    return-object v3

    .line 201
    :cond_2c
    :try_start_2c
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_31
    .catchall {:try_start_2c .. :try_end_31} :catchall_35

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_21

    .line 205
    :catchall_35
    move-exception v0

    if-eqz v1, :cond_3b

    .line 206
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_3b
    throw v0
.end method

.method static synthetic a(LK/w;)Ljava/util/zip/ZipFile;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, LK/w;->f:Ljava/util/zip/ZipFile;

    return-object v0
.end method

.method private a(Ljava/io/Reader;)V
    .registers 4
    .parameter

    .prologue
    .line 216
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 217
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 218
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 219
    invoke-interface {v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 220
    invoke-direct {p0, v0}, LK/w;->a(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_12
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_12} :catch_13
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_12} :catch_23

    .line 228
    return-void

    .line 221
    :catch_13
    move-exception v0

    .line 222
    const-string v1, "Unable to parse messages.xml"

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 223
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 224
    :catch_23
    move-exception v0

    .line 225
    const-string v1, "Unable to parse messages.xml"

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 226
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 235
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 236
    :goto_6
    if-eq v0, v5, :cond_71

    .line 237
    const/4 v2, 0x2

    if-ne v0, v2, :cond_33

    .line 238
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 240
    const-string v2, "caption"

    invoke-interface {p1, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 241
    const-string v3, "maneuver_message"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_38

    .line 242
    invoke-direct {p0, p1}, LK/w;->b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 258
    :goto_21
    if-eqz v0, :cond_33

    if-eqz v2, :cond_33

    .line 259
    new-instance v3, LK/B;

    invoke-direct {p0, v0}, LK/w;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0, v2}, LK/B;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, LK/w;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    :cond_33
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_6

    .line 243
    :cond_38
    const-string v3, "distance_message"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_45

    .line 244
    invoke-direct {p0, p1}, LK/w;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    goto :goto_21

    .line 245
    :cond_45
    const-string v3, "predefined_message"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_52

    .line 246
    invoke-direct {p0, p1}, LK/w;->d(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    goto :goto_21

    .line 247
    :cond_52
    const-string v3, "voice_instructions"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5c

    move-object v0, v1

    goto :goto_21

    .line 251
    :cond_5c
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    .line 253
    :cond_60
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    .line 254
    if-eq v3, v5, :cond_6f

    const/4 v4, 0x3

    if-ne v3, v4, :cond_60

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v3

    if-ne v3, v0, :cond_60

    :cond_6f
    move-object v0, v1

    goto :goto_21

    .line 266
    :cond_71
    return-void
.end method

.method private b(I)LK/A;
    .registers 4
    .parameter

    .prologue
    .line 383
    iget-object v0, p0, LK/w;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 384
    iget-object v0, p0, LK/w;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, LK/w;->a(Ljava/lang/String;)LK/A;

    move-result-object v0

    .line 386
    :goto_1c
    return-object v0

    :cond_1d
    invoke-static {}, LK/A;->b()LK/A;

    move-result-object v0

    goto :goto_1c
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 423
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LK/w;->e:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/._"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 274
    const-string v1, "id"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 275
    const-string v2, "suppressed"

    invoke-interface {p1, v0, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 277
    const-string v3, "true"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 278
    iget-object v2, p0, LK/w;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    :cond_26
    :goto_26
    return-object v0

    .line 280
    :cond_27
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 281
    iget-object v2, p0, LK/w;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_50

    .line 282
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Duplicated maneuver message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 284
    :cond_50
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_26

    .line 285
    iget-object v2, p0, LK/w;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_26
.end method

.method private c(I)LK/A;
    .registers 4
    .parameter

    .prologue
    .line 391
    iget-object v0, p0, LK/w;->c:Ljava/util/SortedMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    .line 393
    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 394
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1d

    .line 395
    invoke-static {}, LK/A;->b()LK/A;

    move-result-object v0

    .line 403
    :goto_1c
    return-object v0

    .line 398
    :cond_1d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/z;

    .line 400
    invoke-virtual {v0, p1}, LK/z;->a(I)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 401
    invoke-virtual {v0}, LK/z;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LK/w;->a(Ljava/lang/String;)LK/A;

    move-result-object v0

    goto :goto_1c

    .line 403
    :cond_32
    invoke-static {}, LK/A;->b()LK/A;

    move-result-object v0

    goto :goto_1c
.end method

.method private c(Ljava/lang/String;)Ljava/io/File;
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 427
    new-instance v0, Ljava/io/File;

    invoke-direct {p0, p1}, LK/w;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 429
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 468
    :cond_10
    :goto_10
    return-object v0

    .line 435
    :cond_11
    iget-object v2, p0, LK/w;->f:Ljava/util/zip/ZipFile;

    if-nez v2, :cond_17

    move-object v0, v1

    .line 436
    goto :goto_10

    .line 441
    :cond_17
    :try_start_17
    iget-object v2, p0, LK/w;->f:Ljava/util/zip/ZipFile;

    invoke-virtual {v2, p1}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v2

    .line 442
    if-nez v2, :cond_21

    move-object v0, v1

    .line 444
    goto :goto_10

    .line 447
    :cond_21
    new-instance v3, LK/x;

    invoke-direct {v3, p0, v2}, LK/x;-><init>(LK/w;Ljava/util/zip/ZipEntry;)V

    new-instance v4, LK/y;

    invoke-direct {v4, p0, v0}, LK/y;-><init>(LK/w;Ljava/io/File;)V

    invoke-static {v3, v4}, Laa/a;->a(Laa/e;Laa/f;)J

    move-result-wide v3

    .line 459
    invoke-virtual {v2}, Ljava/util/zip/ZipEntry;->getSize()J

    move-result-wide v5

    cmp-long v2, v3, v5

    if-eqz v2, :cond_10

    .line 460
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Copy incomplete"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_3f
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_3f} :catch_3f

    .line 462
    :catch_3f
    move-exception v2

    .line 464
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-object v0, v1

    .line 465
    goto :goto_10
.end method

.method private c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 298
    const-string v0, "min"

    invoke-interface {p1, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 299
    const-string v0, "max"

    invoke-interface {p1, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 300
    const/high16 v1, -0x8000

    const v0, 0x7fffffff

    .line 301
    if-eqz v2, :cond_1c

    .line 302
    invoke-static {v2}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 304
    :cond_1c
    if-eqz v3, :cond_26

    .line 305
    invoke-static {v3}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 307
    :cond_26
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    .line 309
    iget-object v3, p0, LK/w;->c:Ljava/util/SortedMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/SortedMap;->size()I

    move-result v3

    if-nez v3, :cond_48

    invoke-direct {p0, v0}, LK/w;->c(I)LK/A;

    move-result-object v3

    invoke-virtual {v3}, LK/A;->e()Z

    move-result v3

    if-nez v3, :cond_6b

    .line 311
    :cond_48
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Overlapping distance message: min="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " max="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 316
    :cond_6b
    iget-object v3, p0, LK/w;->c:Ljava/util/SortedMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, LK/z;

    invoke-direct {v5, v1, v0, v2}, LK/z;-><init>(IILjava/lang/String;)V

    invoke-interface {v3, v4, v5}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    return-object v2
.end method

.method private d(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 329
    const-string v1, "type"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 330
    invoke-static {v1}, LK/m;->a(Ljava/lang/String;)I

    move-result v2

    .line 332
    if-gez v2, :cond_e

    .line 348
    :goto_d
    return-object v0

    .line 337
    :cond_e
    iget-object v3, p0, LK/w;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_33

    .line 338
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Duplicated predefined message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341
    :cond_33
    const-string v1, "suppressed"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 343
    const-string v3, "true"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_45

    .line 344
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 346
    :cond_45
    iget-object v1, p0, LK/w;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_d
.end method


# virtual methods
.method public a(LK/m;)LK/A;
    .registers 3
    .parameter

    .prologue
    .line 358
    instance-of v0, p1, LK/s;

    if-eqz v0, :cond_f

    .line 359
    check-cast p1, LK/s;

    invoke-virtual {p1}, LK/s;->b()I

    move-result v0

    .line 361
    invoke-direct {p0, v0}, LK/w;->b(I)LK/A;

    move-result-object v0

    .line 371
    :goto_e
    return-object v0

    .line 362
    :cond_f
    instance-of v0, p1, LK/q;

    if-eqz v0, :cond_1e

    .line 363
    check-cast p1, LK/q;

    invoke-virtual {p1}, LK/q;->b()I

    move-result v0

    .line 365
    invoke-direct {p0, v0}, LK/w;->c(I)LK/A;

    move-result-object v0

    goto :goto_e

    .line 366
    :cond_1e
    instance-of v0, p1, LK/t;

    if-eqz v0, :cond_2d

    .line 367
    check-cast p1, LK/t;

    invoke-virtual {p1}, LK/t;->b()I

    move-result v0

    .line 369
    invoke-direct {p0, v0}, LK/w;->a(I)LK/A;

    move-result-object v0

    goto :goto_e

    .line 371
    :cond_2d
    invoke-static {}, LK/A;->b()LK/A;

    move-result-object v0

    goto :goto_e
.end method

.method public a()Ljava/util/List;
    .registers 2

    .prologue
    .line 472
    iget-object v0, p0, LK/w;->d:Ljava/util/List;

    return-object v0
.end method
