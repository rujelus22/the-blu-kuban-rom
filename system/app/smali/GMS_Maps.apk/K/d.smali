.class public LK/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LK/T;


# static fields
.field private static final a:[B


# instance fields
.field private b:Z

.field private final c:Landroid/content/Context;

.field private final d:Landroid/os/Handler;

.field private e:LK/ab;

.field private volatile f:Z

.field private g:LK/i;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 44
    const/high16 v0, 0x1

    new-array v0, v0, [B

    sput-object v0, LK/d;->a:[B

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;LK/ab;)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-boolean v1, p0, LK/d;->f:Z

    .line 99
    new-instance v0, LK/i;

    invoke-direct {v0}, LK/i;-><init>()V

    iput-object v0, p0, LK/d;->g:LK/i;

    .line 291
    iput-object p1, p0, LK/d;->c:Landroid/content/Context;

    .line 292
    iput-object p2, p0, LK/d;->d:Landroid/os/Handler;

    .line 293
    iput-object p3, p0, LK/d;->e:LK/ab;

    .line 294
    iget-object v2, p0, LK/d;->g:LK/i;

    monitor-enter v2

    move v0, v1

    .line 295
    :goto_18
    const/16 v3, 0x14

    if-gt v0, v3, :cond_3f

    .line 296
    :try_start_1c
    iget-object v3, p0, LK/d;->g:LK/i;

    iget-object v3, v3, LK/i;->a:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "._speech_nav_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".wav"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 295
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 298
    :cond_3f
    monitor-exit v2
    :try_end_40
    .catchall {:try_start_1c .. :try_end_40} :catchall_59

    .line 305
    iput-boolean v1, p0, LK/d;->b:Z

    .line 320
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_5c

    .line 321
    iget-object v0, p0, LK/d;->e:LK/ab;

    new-instance v1, LK/k;

    invoke-direct {v1, p0, v6}, LK/k;-><init>(LK/d;LK/e;)V

    invoke-interface {v0, v1}, LK/ab;->a(Landroid/speech/tts/UtteranceProgressListener;)I

    move-result v0

    .line 325
    :goto_53
    if-eqz v0, :cond_58

    .line 326
    const/4 v0, 0x1

    iput-boolean v0, p0, LK/d;->f:Z

    .line 328
    :cond_58
    return-void

    .line 298
    :catchall_59
    move-exception v0

    :try_start_5a
    monitor-exit v2
    :try_end_5b
    .catchall {:try_start_5a .. :try_end_5b} :catchall_59

    throw v0

    .line 323
    :cond_5c
    iget-object v0, p0, LK/d;->e:LK/ab;

    new-instance v1, LK/h;

    invoke-direct {v1, p0, v6}, LK/h;-><init>(LK/d;LK/e;)V

    invoke-interface {v0, v1}, LK/ab;->a(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I

    move-result v0

    goto :goto_53
.end method

.method static synthetic a(LK/d;)LK/i;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, LK/d;->g:LK/i;

    return-object v0
.end method

.method private a(Ljava/lang/String;Z)Ljava/io/File;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 393
    if-eqz p2, :cond_e

    .line 394
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LK/d;->c:Landroid/content/Context;

    invoke-static {v1}, LJ/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 400
    :goto_d
    return-object v0

    .line 397
    :cond_e
    iget-object v0, p0, LK/d;->c:Landroid/content/Context;

    const-string v1, "da_speech"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 398
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_d
.end method

.method static synthetic a(LK/d;ZLjava/lang/String;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, LK/d;->a(ZLjava/lang/String;)V

    return-void
.end method

.method private a(LK/l;)V
    .registers 4
    .parameter

    .prologue
    .line 481
    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    .line 482
    :try_start_3
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 483
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_f

    .line 484
    invoke-direct {p0}, LK/d;->e()V

    .line 485
    return-void

    .line 483
    :catchall_f
    move-exception v0

    :try_start_10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_10 .. :try_end_11} :catchall_f

    throw v0
.end method

.method private a(ZLjava/lang/String;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 631
    if-nez p2, :cond_3

    .line 667
    :goto_2
    return-void

    .line 636
    :cond_3
    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    .line 639
    :try_start_6
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->c:LK/l;

    .line 640
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_6 .. :try_end_b} :catchall_2f

    .line 642
    if-eqz v0, :cond_22

    .line 643
    if-eqz p1, :cond_48

    invoke-virtual {v0}, LK/l;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_48

    .line 645
    invoke-virtual {v0}, LK/l;->d()LK/f;

    move-result-object v1

    .line 646
    if-nez v1, :cond_32

    .line 648
    invoke-virtual {v0}, LK/l;->a()V

    .line 663
    :cond_22
    :goto_22
    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    .line 664
    :try_start_25
    iget-object v0, p0, LK/d;->g:LK/i;

    const/4 v2, 0x0

    iput-object v2, v0, LK/i;->c:LK/l;

    .line 665
    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_25 .. :try_end_2b} :catchall_51

    .line 666
    invoke-direct {p0}, LK/d;->e()V

    goto :goto_2

    .line 640
    :catchall_2f
    move-exception v0

    :try_start_30
    monitor-exit v1
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2f

    throw v0

    .line 650
    :cond_32
    iget-object v2, p0, LK/d;->g:LK/i;

    monitor-enter v2

    .line 651
    :try_start_35
    iget-object v3, p0, LK/d;->g:LK/i;

    iget-object v3, v3, LK/i;->d:LR/h;

    invoke-virtual {v0}, LK/l;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 652
    monitor-exit v2
    :try_end_41
    .catchall {:try_start_35 .. :try_end_41} :catchall_45

    .line 653
    invoke-virtual {v0, v1}, LK/l;->a(LK/a;)V

    goto :goto_22

    .line 652
    :catchall_45
    move-exception v0

    :try_start_46
    monitor-exit v2
    :try_end_47
    .catchall {:try_start_46 .. :try_end_47} :catchall_45

    throw v0

    .line 657
    :cond_48
    const-string v1, "x"

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    .line 658
    invoke-virtual {v0}, LK/l;->a()V

    goto :goto_22

    .line 665
    :catchall_51
    move-exception v0

    :try_start_52
    monitor-exit v1
    :try_end_53
    .catchall {:try_start_52 .. :try_end_53} :catchall_51

    throw v0
.end method

.method public static a(Ljava/io/File;)Z
    .registers 9
    .parameter

    .prologue
    const/16 v6, 0x46

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 342
    :try_start_4
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_9} :catch_d0

    .line 347
    const/16 v3, 0x2c

    :try_start_b
    new-array v3, v3, [B

    .line 348
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    array-length v5, v3
    :try_end_12
    .catchall {:try_start_b .. :try_end_12} :catchall_c2
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_12} :catch_b9

    if-eq v4, v5, :cond_18

    .line 382
    :try_start_14
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_17} :catch_c7

    .line 388
    :goto_17
    return v0

    .line 352
    :cond_18
    const/4 v4, 0x0

    :try_start_19
    aget-byte v4, v3, v4

    const/16 v5, 0x52

    if-ne v4, v5, :cond_50

    const/4 v4, 0x1

    aget-byte v4, v3, v4

    const/16 v5, 0x49

    if-ne v4, v5, :cond_50

    const/4 v4, 0x2

    aget-byte v4, v3, v4

    if-ne v4, v6, :cond_50

    const/4 v4, 0x3

    aget-byte v4, v3, v4

    if-ne v4, v6, :cond_50

    const/16 v4, 0x8

    aget-byte v4, v3, v4

    const/16 v5, 0x57

    if-ne v4, v5, :cond_50

    const/16 v4, 0x9

    aget-byte v4, v3, v4

    const/16 v5, 0x41

    if-ne v4, v5, :cond_50

    const/16 v4, 0xa

    aget-byte v4, v3, v4

    const/16 v5, 0x56

    if-ne v4, v5, :cond_50

    const/16 v4, 0xb

    aget-byte v4, v3, v4
    :try_end_4c
    .catchall {:try_start_19 .. :try_end_4c} :catchall_c2
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_4c} :catch_b9

    const/16 v5, 0x45

    if-eq v4, v5, :cond_55

    .line 382
    :cond_50
    :try_start_50
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_53
    .catch Ljava/io/IOException; {:try_start_50 .. :try_end_53} :catch_ca

    :goto_53
    move v0, v1

    .line 385
    goto :goto_17

    .line 359
    :cond_55
    const/16 v4, 0x28

    :try_start_57
    aget-byte v4, v3, v4

    and-int/lit16 v4, v4, 0xff

    const/16 v5, 0x29

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    const/16 v5, 0x2a

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v4, v5

    const/16 v5, 0x2b

    aget-byte v3, v3, v5

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    or-int/2addr v3, v4

    .line 363
    if-lez v3, :cond_83

    add-int/lit8 v4, v3, 0x2c

    int-to-long v4, v4

    invoke-virtual {p0}, Ljava/io/File;->length()J
    :try_end_7e
    .catchall {:try_start_57 .. :try_end_7e} :catchall_c2
    .catch Ljava/io/IOException; {:try_start_57 .. :try_end_7e} :catch_b9

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_89

    .line 382
    :cond_83
    :try_start_83
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_86
    .catch Ljava/io/IOException; {:try_start_83 .. :try_end_86} :catch_87

    goto :goto_17

    .line 383
    :catch_87
    move-exception v1

    goto :goto_17

    .line 369
    :cond_89
    :try_start_89
    sget-object v4, LK/d;->a:[B

    array-length v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 370
    sget-object v4, LK/d;->a:[B

    array-length v4, v4

    new-array v4, v4, [B

    .line 371
    invoke-virtual {v2, v4}, Ljava/io/FileInputStream;->read([B)I
    :try_end_98
    .catchall {:try_start_89 .. :try_end_98} :catchall_c2
    .catch Ljava/io/IOException; {:try_start_89 .. :try_end_98} :catch_b9

    move-result v5

    if-eq v5, v3, :cond_a3

    .line 382
    :try_start_9b
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_9e
    .catch Ljava/io/IOException; {:try_start_9b .. :try_end_9e} :catch_a0

    goto/16 :goto_17

    .line 383
    :catch_a0
    move-exception v1

    goto/16 :goto_17

    .line 375
    :cond_a3
    :try_start_a3
    sget-object v3, LK/d;->a:[B

    invoke-static {v4, v3}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_a8
    .catchall {:try_start_a3 .. :try_end_a8} :catchall_c2
    .catch Ljava/io/IOException; {:try_start_a3 .. :try_end_a8} :catch_b9

    move-result v3

    if-eqz v3, :cond_b3

    .line 382
    :try_start_ab
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_ae
    .catch Ljava/io/IOException; {:try_start_ab .. :try_end_ae} :catch_b0

    goto/16 :goto_17

    .line 383
    :catch_b0
    move-exception v1

    goto/16 :goto_17

    .line 382
    :cond_b3
    :try_start_b3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_b6
    .catch Ljava/io/IOException; {:try_start_b3 .. :try_end_b6} :catch_cc

    :goto_b6
    move v0, v1

    .line 388
    goto/16 :goto_17

    .line 378
    :catch_b9
    move-exception v1

    .line 382
    :try_start_ba
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_bd
    .catch Ljava/io/IOException; {:try_start_ba .. :try_end_bd} :catch_bf

    goto/16 :goto_17

    .line 383
    :catch_bf
    move-exception v1

    goto/16 :goto_17

    .line 381
    :catchall_c2
    move-exception v0

    .line 382
    :try_start_c3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_c6
    .catch Ljava/io/IOException; {:try_start_c3 .. :try_end_c6} :catch_ce

    .line 385
    :goto_c6
    throw v0

    .line 383
    :catch_c7
    move-exception v1

    goto/16 :goto_17

    :catch_ca
    move-exception v0

    goto :goto_53

    :catch_cc
    move-exception v0

    goto :goto_b6

    :catch_ce
    move-exception v1

    goto :goto_c6

    .line 343
    :catch_d0
    move-exception v1

    goto/16 :goto_17
.end method

.method private a(Z)Z
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 604
    if-eqz p1, :cond_a

    .line 605
    :try_start_3
    invoke-virtual {p0}, LK/d;->c()Landroid/os/StatFs;

    move-result-object v0

    .line 614
    :goto_7
    if-nez v0, :cond_f

    .line 620
    :goto_9
    return v1

    .line 607
    :cond_a
    invoke-virtual {p0}, LK/d;->d()Landroid/os/StatFs;
    :try_end_d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_d} :catch_26

    move-result-object v0

    goto :goto_7

    .line 617
    :cond_f
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v2

    int-to-long v2, v2

    .line 618
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v4, v0

    .line 619
    mul-long/2addr v2, v4

    .line 620
    const-wide/32 v4, 0x80000

    cmp-long v0, v2, v4

    if-lez v0, :cond_24

    const/4 v0, 0x1

    :goto_22
    move v1, v0

    goto :goto_9

    :cond_24
    move v0, v1

    goto :goto_22

    .line 609
    :catch_26
    move-exception v0

    goto :goto_9
.end method

.method static synthetic b(LK/d;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, LK/d;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(LK/d;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, LK/d;->d:Landroid/os/Handler;

    return-object v0
.end method

.method private e()V
    .registers 9

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 489
    .line 490
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 492
    iget-object v4, p0, LK/d;->g:LK/i;

    monitor-enter v4

    .line 493
    :try_start_b
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->c:LK/l;

    if-eqz v0, :cond_13

    .line 494
    monitor-exit v4

    .line 570
    :cond_12
    :goto_12
    return-void

    .line 496
    :cond_13
    :goto_13
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_e3

    .line 498
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/l;

    .line 499
    iget-object v2, p0, LK/d;->g:LK/i;

    iget-object v2, v2, LK/i;->d:LR/h;

    invoke-virtual {v0}, LK/l;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3c

    .line 500
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 507
    :catchall_39
    move-exception v0

    monitor-exit v4
    :try_end_3b
    .catchall {:try_start_b .. :try_end_3b} :catchall_39

    throw v0

    .line 503
    :cond_3c
    :try_start_3c
    iget-object v1, p0, LK/d;->g:LK/i;

    iput-object v0, v1, LK/i;->c:LK/l;

    move-object v2, v0

    .line 507
    :goto_41
    monitor-exit v4
    :try_end_42
    .catchall {:try_start_3c .. :try_end_42} :catchall_39

    .line 511
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_46
    :goto_46
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, LK/l;

    .line 513
    iget-object v4, p0, LK/d;->g:LK/i;

    monitor-enter v4

    .line 514
    :try_start_56
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->d:LR/h;

    invoke-virtual {v1}, LK/l;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/a;

    .line 515
    monitor-exit v4
    :try_end_65
    .catchall {:try_start_56 .. :try_end_65} :catchall_6b

    .line 516
    if-eqz v0, :cond_46

    .line 517
    invoke-virtual {v1, v0}, LK/l;->a(LK/a;)V

    goto :goto_46

    .line 515
    :catchall_6b
    move-exception v0

    :try_start_6c
    monitor-exit v4
    :try_end_6d
    .catchall {:try_start_6c .. :try_end_6d} :catchall_6b

    throw v0

    .line 522
    :cond_6e
    if-eqz v2, :cond_12

    .line 533
    iget-boolean v0, p0, LK/d;->b:Z

    if-eqz v0, :cond_81

    invoke-direct {p0, v7}, LK/d;->a(Z)Z

    move-result v0

    if-nez v0, :cond_81

    .line 534
    const-string v0, "d"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    .line 535
    iput-boolean v6, p0, LK/d;->b:Z

    .line 537
    :cond_81
    iget-boolean v0, p0, LK/d;->b:Z

    if-nez v0, :cond_97

    invoke-direct {p0, v6}, LK/d;->a(Z)Z

    move-result v0

    if-nez v0, :cond_97

    .line 538
    const-string v0, "s"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    .line 542
    invoke-virtual {p0}, LK/d;->a()V

    .line 543
    iput-boolean v7, p0, LK/d;->f:Z

    goto/16 :goto_12

    .line 549
    :cond_97
    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    .line 550
    :try_start_9a
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_ab

    .line 555
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->d:LR/h;

    invoke-virtual {v0}, LR/h;->g()Ljava/lang/Object;

    .line 557
    :cond_ab
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->a:Ljava/util/ArrayList;

    iget-object v3, p0, LK/d;->g:LK/i;

    iget-object v3, v3, LK/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 558
    monitor-exit v1
    :try_end_c0
    .catchall {:try_start_9a .. :try_end_c0} :catchall_e0

    .line 560
    if-eqz v0, :cond_12

    .line 561
    iget-boolean v1, p0, LK/d;->b:Z

    invoke-direct {p0, v0, v1}, LK/d;->a(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v1

    .line 562
    invoke-virtual {v2, v0, v1}, LK/l;->a(Ljava/lang/String;Ljava/io/File;)V

    .line 564
    iget-object v3, p0, LK/d;->e:LK/ab;

    invoke-virtual {v2}, LK/l;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v2, v0, v1, v6}, LK/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v1

    .line 565
    if-eqz v1, :cond_12

    .line 567
    invoke-direct {p0, v6, v0}, LK/d;->a(ZLjava/lang/String;)V

    goto/16 :goto_12

    .line 558
    :catchall_e0
    move-exception v0

    :try_start_e1
    monitor-exit v1
    :try_end_e2
    .catchall {:try_start_e1 .. :try_end_e2} :catchall_e0

    throw v0

    :cond_e3
    move-object v2, v1

    goto/16 :goto_41
.end method


# virtual methods
.method public a(LK/V;)LK/a;
    .registers 5
    .parameter

    .prologue
    .line 439
    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    .line 440
    :try_start_3
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->d:LR/h;

    invoke-virtual {p1}, LK/V;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/a;

    monitor-exit v1

    return-object v0

    .line 441
    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    throw v0
.end method

.method public a()V
    .registers 5

    .prologue
    .line 446
    iget-boolean v0, p0, LK/d;->f:Z

    if-eqz v0, :cond_5

    .line 466
    :cond_4
    return-void

    .line 450
    :cond_5
    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    .line 451
    :try_start_8
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->b:Ljava/util/LinkedList;

    .line 452
    iget-object v2, p0, LK/d;->g:LK/i;

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, v2, LK/i;->b:Ljava/util/LinkedList;

    .line 453
    iget-object v2, p0, LK/d;->g:LK/i;

    iget-object v2, v2, LK/i;->c:LK/l;

    if-eqz v2, :cond_2c

    .line 454
    iget-object v2, p0, LK/d;->g:LK/i;

    iget-object v2, v2, LK/i;->c:LK/l;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 455
    iget-object v2, p0, LK/d;->e:LK/ab;

    invoke-interface {v2}, LK/ab;->c()I

    .line 456
    iget-object v2, p0, LK/d;->g:LK/i;

    const/4 v3, 0x0

    iput-object v3, v2, LK/i;->c:LK/l;

    .line 458
    :cond_2c
    monitor-exit v1
    :try_end_2d
    .catchall {:try_start_8 .. :try_end_2d} :catchall_43

    .line 459
    if-eqz v0, :cond_4

    .line 462
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_33
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LK/l;

    .line 463
    invoke-virtual {v0}, LK/l;->a()V

    goto :goto_33

    .line 458
    :catchall_43
    move-exception v0

    :try_start_44
    monitor-exit v1
    :try_end_45
    .catchall {:try_start_44 .. :try_end_45} :catchall_43

    throw v0
.end method

.method public a(LK/V;LK/U;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 405
    iget-boolean v0, p0, LK/d;->f:Z

    if-eqz v0, :cond_9

    .line 406
    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, LK/U;->a(LK/V;LK/a;)V

    .line 421
    :cond_8
    :goto_8
    return-void

    .line 409
    :cond_9
    invoke-virtual {p0, p1}, LK/d;->a(LK/V;)LK/a;

    move-result-object v0

    check-cast v0, LK/f;

    .line 410
    if-eqz v0, :cond_1c

    .line 411
    const-string v1, "h"

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    .line 414
    if-eqz p2, :cond_8

    .line 415
    invoke-interface {p2, p1, v0}, LK/U;->a(LK/V;LK/a;)V

    goto :goto_8

    .line 419
    :cond_1c
    const-string v0, "m"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dk;->a(Ljava/lang/String;)V

    .line 420
    new-instance v0, LK/l;

    invoke-direct {v0, p0, p1, p2}, LK/l;-><init>(LK/d;LK/V;LK/U;)V

    invoke-direct {p0, v0}, LK/d;->a(LK/l;)V

    goto :goto_8
.end method

.method public b()V
    .registers 3

    .prologue
    .line 470
    iget-object v0, p0, LK/d;->e:LK/ab;

    if-eqz v0, :cond_e

    .line 471
    iget-object v0, p0, LK/d;->e:LK/ab;

    invoke-interface {v0}, LK/ab;->c()I

    .line 472
    iget-object v0, p0, LK/d;->e:LK/ab;

    invoke-interface {v0}, LK/ab;->a()V

    .line 474
    :cond_e
    iget-object v1, p0, LK/d;->g:LK/i;

    monitor-enter v1

    .line 475
    :try_start_11
    iget-object v0, p0, LK/d;->g:LK/i;

    iget-object v0, v0, LK/i;->d:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 476
    monitor-exit v1

    .line 477
    return-void

    .line 476
    :catchall_1a
    move-exception v0

    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_11 .. :try_end_1c} :catchall_1a

    throw v0
.end method

.method protected c()Landroid/os/StatFs;
    .registers 3

    .prologue
    .line 580
    iget-object v0, p0, LK/d;->c:Landroid/content/Context;

    invoke-static {v0}, LJ/a;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    .line 581
    if-nez v1, :cond_a

    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_a
    new-instance v0, Landroid/os/StatFs;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    goto :goto_9
.end method

.method protected d()Landroid/os/StatFs;
    .registers 3

    .prologue
    .line 591
    new-instance v0, Landroid/os/StatFs;

    iget-object v1, p0, LK/d;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
