.class public LaH/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:F

.field private b:D

.field private c:F

.field private d:Landroid/os/Bundle;

.field private e:D

.field private f:D

.field private g:Ljava/lang/String;

.field private h:F

.field private i:J

.field private j:J

.field private k:Lo/D;

.field private l:LaN/B;

.field private m:LaH/l;

.field private n:LaH/k;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 703
    iput-boolean v0, p0, LaH/j;->o:Z

    .line 704
    iput-boolean v0, p0, LaH/j;->p:Z

    .line 705
    iput-boolean v0, p0, LaH/j;->q:Z

    .line 706
    iput-boolean v0, p0, LaH/j;->r:Z

    .line 707
    iput-boolean v0, p0, LaH/j;->s:Z

    .line 708
    iput-boolean v0, p0, LaH/j;->t:Z

    return-void
.end method

.method static synthetic a(LaH/j;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 687
    iget-object v0, p0, LaH/j;->g:Ljava/lang/String;

    return-object v0
.end method

.method private b(Landroid/location/Location;)V
    .registers 6
    .parameter

    .prologue
    .line 810
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 811
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p0, v0}, LaH/j;->a(F)LaH/j;

    .line 813
    :cond_d
    invoke-virtual {p1}, Landroid/location/Location;->hasAltitude()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 814
    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LaH/j;->a(D)LaH/j;

    .line 816
    :cond_1a
    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 817
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-virtual {p0, v0}, LaH/j;->b(F)LaH/j;

    .line 819
    :cond_27
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, LaH/j;->a(DD)LaH/j;

    .line 820
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaH/j;->a(Ljava/lang/String;)LaH/j;

    .line 821
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_46

    .line 822
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    invoke-virtual {p0, v0}, LaH/j;->c(F)LaH/j;

    .line 824
    :cond_46
    return-void
.end method

.method static synthetic b(LaH/j;)Z
    .registers 2
    .parameter

    .prologue
    .line 687
    iget-boolean v0, p0, LaH/j;->o:Z

    return v0
.end method

.method static synthetic c(LaH/j;)F
    .registers 2
    .parameter

    .prologue
    .line 687
    iget v0, p0, LaH/j;->a:F

    return v0
.end method

.method static synthetic d(LaH/j;)Z
    .registers 2
    .parameter

    .prologue
    .line 687
    iget-boolean v0, p0, LaH/j;->p:Z

    return v0
.end method

.method static synthetic e(LaH/j;)D
    .registers 3
    .parameter

    .prologue
    .line 687
    iget-wide v0, p0, LaH/j;->b:D

    return-wide v0
.end method

.method static synthetic f(LaH/j;)Z
    .registers 2
    .parameter

    .prologue
    .line 687
    iget-boolean v0, p0, LaH/j;->q:Z

    return v0
.end method

.method static synthetic g(LaH/j;)F
    .registers 2
    .parameter

    .prologue
    .line 687
    iget v0, p0, LaH/j;->c:F

    return v0
.end method

.method static synthetic h(LaH/j;)D
    .registers 3
    .parameter

    .prologue
    .line 687
    iget-wide v0, p0, LaH/j;->e:D

    return-wide v0
.end method

.method static synthetic i(LaH/j;)D
    .registers 3
    .parameter

    .prologue
    .line 687
    iget-wide v0, p0, LaH/j;->f:D

    return-wide v0
.end method

.method static synthetic j(LaH/j;)Z
    .registers 2
    .parameter

    .prologue
    .line 687
    iget-boolean v0, p0, LaH/j;->r:Z

    return v0
.end method

.method static synthetic k(LaH/j;)F
    .registers 2
    .parameter

    .prologue
    .line 687
    iget v0, p0, LaH/j;->h:F

    return v0
.end method

.method static synthetic l(LaH/j;)Z
    .registers 2
    .parameter

    .prologue
    .line 687
    iget-boolean v0, p0, LaH/j;->s:Z

    return v0
.end method

.method static synthetic m(LaH/j;)J
    .registers 3
    .parameter

    .prologue
    .line 687
    iget-wide v0, p0, LaH/j;->i:J

    return-wide v0
.end method

.method static synthetic n(LaH/j;)Z
    .registers 2
    .parameter

    .prologue
    .line 687
    iget-boolean v0, p0, LaH/j;->t:Z

    return v0
.end method

.method static synthetic o(LaH/j;)J
    .registers 3
    .parameter

    .prologue
    .line 687
    iget-wide v0, p0, LaH/j;->j:J

    return-wide v0
.end method

.method static synthetic p(LaH/j;)Landroid/os/Bundle;
    .registers 2
    .parameter

    .prologue
    .line 687
    iget-object v0, p0, LaH/j;->d:Landroid/os/Bundle;

    return-object v0
.end method

.method private q()LaH/l;
    .registers 2

    .prologue
    .line 941
    iget-object v0, p0, LaH/j;->m:LaH/l;

    if-nez v0, :cond_b

    .line 942
    new-instance v0, LaH/l;

    invoke-direct {v0}, LaH/l;-><init>()V

    iput-object v0, p0, LaH/j;->m:LaH/l;

    .line 944
    :cond_b
    iget-object v0, p0, LaH/j;->m:LaH/l;

    return-object v0
.end method

.method static synthetic q(LaH/j;)LaN/B;
    .registers 2
    .parameter

    .prologue
    .line 687
    iget-object v0, p0, LaH/j;->l:LaN/B;

    return-object v0
.end method

.method private r()LaH/k;
    .registers 2

    .prologue
    .line 948
    iget-object v0, p0, LaH/j;->n:LaH/k;

    if-nez v0, :cond_b

    .line 949
    new-instance v0, LaH/k;

    invoke-direct {v0}, LaH/k;-><init>()V

    iput-object v0, p0, LaH/j;->n:LaH/k;

    .line 951
    :cond_b
    iget-object v0, p0, LaH/j;->n:LaH/k;

    return-object v0
.end method

.method static synthetic r(LaH/j;)Lo/D;
    .registers 2
    .parameter

    .prologue
    .line 687
    iget-object v0, p0, LaH/j;->k:Lo/D;

    return-object v0
.end method

.method static synthetic s(LaH/j;)LaH/l;
    .registers 2
    .parameter

    .prologue
    .line 687
    iget-object v0, p0, LaH/j;->m:LaH/l;

    return-object v0
.end method

.method static synthetic t(LaH/j;)LaH/k;
    .registers 2
    .parameter

    .prologue
    .line 687
    iget-object v0, p0, LaH/j;->n:LaH/k;

    return-object v0
.end method


# virtual methods
.method public a()LaH/j;
    .registers 2

    .prologue
    .line 833
    const/4 v0, 0x0

    iput v0, p0, LaH/j;->c:F

    .line 834
    const/4 v0, 0x0

    iput-boolean v0, p0, LaH/j;->q:Z

    .line 835
    return-object p0
.end method

.method public a(D)LaH/j;
    .registers 4
    .parameter

    .prologue
    .line 717
    iput-wide p1, p0, LaH/j;->b:D

    .line 718
    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/j;->p:Z

    .line 719
    return-object p0
.end method

.method public a(DD)LaH/j;
    .registers 10
    .parameter
    .parameter

    .prologue
    const-wide v3, 0x412e848000000000L

    .line 738
    iput-wide p1, p0, LaH/j;->e:D

    .line 739
    iput-wide p3, p0, LaH/j;->f:D

    .line 740
    new-instance v0, LaN/B;

    mul-double v1, p1, v3

    double-to-int v1, v1

    mul-double v2, p3, v3

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    iput-object v0, p0, LaH/j;->l:LaN/B;

    .line 741
    return-object p0
.end method

.method public a(F)LaH/j;
    .registers 3
    .parameter

    .prologue
    .line 711
    iput p1, p0, LaH/j;->a:F

    .line 712
    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/j;->o:Z

    .line 713
    return-object p0
.end method

.method public a(I)LaH/j;
    .registers 3
    .parameter

    .prologue
    .line 869
    invoke-direct {p0}, LaH/j;->r()LaH/k;

    move-result-object v0

    iput p1, v0, LaH/k;->c:I

    .line 870
    return-object p0
.end method

.method public a(J)LaH/j;
    .registers 4
    .parameter

    .prologue
    .line 756
    iput-wide p1, p0, LaH/j;->i:J

    .line 757
    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/j;->s:Z

    .line 758
    return-object p0
.end method

.method public a(LO/H;)LaH/j;
    .registers 3
    .parameter

    .prologue
    .line 888
    invoke-direct {p0}, LaH/j;->q()LaH/l;

    move-result-object v0

    iput-object p1, v0, LaH/l;->d:LO/H;

    .line 889
    return-object p0
.end method

.method public a(LaN/B;)LaH/j;
    .registers 6
    .parameter

    .prologue
    const-wide v2, 0x412e848000000000L

    .line 777
    iput-object p1, p0, LaH/j;->l:LaN/B;

    .line 778
    invoke-virtual {p1}, LaN/B;->c()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, LaH/j;->e:D

    .line 779
    invoke-virtual {p1}, LaN/B;->e()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, LaH/j;->f:D

    .line 780
    return-object p0
.end method

.method public a(Landroid/location/Location;)LaH/j;
    .registers 4
    .parameter

    .prologue
    .line 784
    if-nez p1, :cond_3

    .line 806
    :cond_2
    :goto_2
    return-object p0

    .line 787
    :cond_3
    invoke-direct {p0, p1}, LaH/j;->b(Landroid/location/Location;)V

    .line 788
    invoke-virtual {p1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, LaH/j;->a(Landroid/os/Bundle;)LaH/j;

    .line 789
    invoke-static {p1}, LaH/b;->b(Landroid/location/Location;)Lo/D;

    move-result-object v0

    invoke-virtual {p0, v0}, LaH/j;->a(Lo/D;)LaH/j;

    .line 790
    instance-of v0, p1, LaH/h;

    if-eqz v0, :cond_53

    .line 791
    check-cast p1, LaH/h;

    .line 792
    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/j;->t:Z

    .line 793
    invoke-static {p1}, LaH/h;->b(LaH/h;)J

    move-result-wide v0

    iput-wide v0, p0, LaH/j;->j:J

    .line 794
    invoke-virtual {p1}, LaH/h;->d()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 795
    invoke-virtual {p1}, LaH/h;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LaH/j;->a(J)LaH/j;

    .line 797
    :cond_30
    invoke-static {p1}, LaH/h;->c(LaH/h;)LaH/l;

    move-result-object v0

    if-eqz v0, :cond_41

    .line 798
    new-instance v0, LaH/l;

    invoke-static {p1}, LaH/h;->c(LaH/h;)LaH/l;

    move-result-object v1

    invoke-direct {v0, v1}, LaH/l;-><init>(LaH/l;)V

    iput-object v0, p0, LaH/j;->m:LaH/l;

    .line 800
    :cond_41
    invoke-static {p1}, LaH/h;->d(LaH/h;)LaH/k;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 801
    new-instance v0, LaH/k;

    invoke-static {p1}, LaH/h;->d(LaH/h;)LaH/k;

    move-result-object v1

    invoke-direct {v0, v1}, LaH/k;-><init>(LaH/k;)V

    iput-object v0, p0, LaH/j;->n:LaH/k;

    goto :goto_2

    .line 804
    :cond_53
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LaH/j;->a(J)LaH/j;

    goto :goto_2
.end method

.method public a(Landroid/os/Bundle;)LaH/j;
    .registers 2
    .parameter

    .prologue
    .line 729
    iput-object p1, p0, LaH/j;->d:Landroid/os/Bundle;

    .line 730
    return-object p0
.end method

.method public a(Ljava/lang/String;)LaH/j;
    .registers 2
    .parameter

    .prologue
    .line 745
    iput-object p1, p0, LaH/j;->g:Ljava/lang/String;

    .line 746
    return-object p0
.end method

.method public a(Lo/D;)LaH/j;
    .registers 3
    .parameter

    .prologue
    .line 762
    iput-object p1, p0, LaH/j;->k:Lo/D;

    .line 763
    if-eqz p1, :cond_b

    .line 768
    invoke-static {p1}, LaK/a;->a(Lo/D;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, LaH/j;->a(Landroid/os/Bundle;)LaH/j;

    .line 770
    :cond_b
    return-object p0
.end method

.method public a(Lo/af;Lo/T;)LaH/j;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 878
    invoke-direct {p0}, LaH/j;->q()LaH/l;

    move-result-object v0

    .line 879
    iput-object p1, v0, LaH/l;->b:Lo/af;

    .line 880
    if-nez p1, :cond_9

    const/4 p2, 0x0

    :cond_9
    iput-object p2, v0, LaH/l;->c:Lo/T;

    .line 881
    return-object p0
.end method

.method public a(Z)LaH/j;
    .registers 3
    .parameter

    .prologue
    .line 854
    invoke-direct {p0}, LaH/j;->r()LaH/k;

    move-result-object v0

    iput-boolean p1, v0, LaH/k;->a:Z

    .line 855
    return-object p0
.end method

.method public b()LaH/j;
    .registers 2

    .prologue
    .line 840
    const/4 v0, 0x0

    iput v0, p0, LaH/j;->h:F

    .line 841
    const/4 v0, 0x0

    iput-boolean v0, p0, LaH/j;->r:Z

    .line 842
    return-object p0
.end method

.method public b(D)LaH/j;
    .registers 4
    .parameter

    .prologue
    .line 896
    invoke-direct {p0}, LaH/j;->q()LaH/l;

    move-result-object v0

    iput-wide p1, v0, LaH/l;->e:D

    .line 897
    return-object p0
.end method

.method public b(F)LaH/j;
    .registers 3
    .parameter

    .prologue
    .line 723
    iput p1, p0, LaH/j;->c:F

    .line 724
    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/j;->q:Z

    .line 725
    return-object p0
.end method

.method public b(I)LaH/j;
    .registers 3
    .parameter

    .prologue
    .line 912
    invoke-direct {p0}, LaH/j;->q()LaH/l;

    move-result-object v0

    iput p1, v0, LaH/l;->f:I

    .line 913
    return-object p0
.end method

.method public b(Z)LaH/j;
    .registers 3
    .parameter

    .prologue
    .line 864
    invoke-direct {p0}, LaH/j;->r()LaH/k;

    move-result-object v0

    iput-boolean p1, v0, LaH/k;->b:Z

    .line 865
    return-object p0
.end method

.method public c()LaH/j;
    .registers 2

    .prologue
    .line 846
    const/4 v0, 0x0

    iput-boolean v0, p0, LaH/j;->s:Z

    .line 847
    return-object p0
.end method

.method public c(F)LaH/j;
    .registers 3
    .parameter

    .prologue
    .line 750
    iput p1, p0, LaH/j;->h:F

    .line 751
    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/j;->r:Z

    .line 752
    return-object p0
.end method

.method public c(Z)LaH/j;
    .registers 3
    .parameter

    .prologue
    .line 904
    invoke-direct {p0}, LaH/j;->q()LaH/l;

    move-result-object v0

    iput-boolean p1, v0, LaH/l;->a:Z

    .line 905
    return-object p0
.end method

.method public d()LaH/h;
    .registers 3

    .prologue
    .line 934
    iget-object v0, p0, LaH/j;->l:LaN/B;

    if-nez v0, :cond_c

    .line 935
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 937
    :cond_c
    new-instance v0, LaH/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaH/h;-><init>(LaH/j;LaH/i;)V

    return-object v0
.end method

.method public d(Z)LaH/j;
    .registers 3
    .parameter

    .prologue
    .line 925
    invoke-direct {p0}, LaH/j;->q()LaH/l;

    move-result-object v0

    iput-boolean p1, v0, LaH/l;->g:Z

    .line 926
    return-object p0
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 955
    iget-boolean v0, p0, LaH/j;->o:Z

    return v0
.end method

.method public f()F
    .registers 2

    .prologue
    .line 959
    iget v0, p0, LaH/j;->a:F

    return v0
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, LaH/j;->n:LaH/k;

    if-eqz v0, :cond_c

    iget-object v0, p0, LaH/j;->n:LaH/k;

    iget v0, v0, LaH/k;->c:I

    if-ltz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public h()I
    .registers 2

    .prologue
    .line 967
    iget-object v0, p0, LaH/j;->n:LaH/k;

    if-eqz v0, :cond_9

    iget-object v0, p0, LaH/j;->n:LaH/k;

    iget v0, v0, LaH/k;->c:I

    :goto_8
    return v0

    :cond_9
    const/4 v0, -0x1

    goto :goto_8
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 971
    iget-boolean v0, p0, LaH/j;->q:Z

    return v0
.end method

.method public j()F
    .registers 2

    .prologue
    .line 975
    iget v0, p0, LaH/j;->c:F

    return v0
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 979
    iget-boolean v0, p0, LaH/j;->r:Z

    return v0
.end method

.method public l()F
    .registers 2

    .prologue
    .line 983
    iget v0, p0, LaH/j;->h:F

    return v0
.end method

.method public m()D
    .registers 3

    .prologue
    .line 987
    iget-wide v0, p0, LaH/j;->e:D

    return-wide v0
.end method

.method public n()D
    .registers 3

    .prologue
    .line 991
    iget-wide v0, p0, LaH/j;->f:D

    return-wide v0
.end method

.method public o()LO/H;
    .registers 2

    .prologue
    .line 1003
    iget-object v0, p0, LaH/j;->m:LaH/l;

    if-eqz v0, :cond_9

    iget-object v0, p0, LaH/j;->m:LaH/l;

    iget-object v0, v0, LaH/l;->d:LO/H;

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public p()Z
    .registers 2

    .prologue
    .line 1007
    iget-object v0, p0, LaH/j;->n:LaH/k;

    if-eqz v0, :cond_12

    iget-object v0, p0, LaH/j;->n:LaH/k;

    iget-boolean v0, v0, LaH/k;->a:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, LaH/j;->n:LaH/k;

    iget-boolean v0, v0, LaH/k;->b:Z

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method
