.class LaH/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaH/q;


# instance fields
.field private final a:Ljava/util/Map;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 749
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LaH/r;->a:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(LaH/p;)V
    .registers 2
    .parameter

    .prologue
    .line 744
    invoke-direct {p0}, LaH/r;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILaH/o;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 791
    iget-object v1, p0, LaH/r;->a:Ljava/util/Map;

    monitor-enter v1

    .line 792
    :try_start_3
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, LaH/r;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 793
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_25

    .line 795
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_13
    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/A;

    .line 796
    if-eqz v0, :cond_13

    .line 797
    invoke-interface {v0, p1, p2}, LaH/A;->a(ILaH/m;)V

    goto :goto_13

    .line 793
    :catchall_25
    move-exception v0

    :try_start_26
    monitor-exit v1
    :try_end_27
    .catchall {:try_start_26 .. :try_end_27} :catchall_25

    throw v0

    .line 800
    :cond_28
    return-void
.end method

.method public a(LaH/A;)V
    .registers 4
    .parameter

    .prologue
    .line 754
    iget-object v0, p0, LaH/r;->a:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 755
    return-void
.end method

.method public a(LaN/B;LaH/o;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 771
    iget-object v1, p0, LaH/r;->a:Ljava/util/Map;

    monitor-enter v1

    .line 772
    :try_start_3
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, LaH/r;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 773
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_25

    .line 775
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_13
    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/A;

    .line 776
    if-eqz v0, :cond_13

    .line 777
    invoke-interface {v0, p1, p2}, LaH/A;->a(LaN/B;LaH/m;)V

    goto :goto_13

    .line 773
    :catchall_25
    move-exception v0

    :try_start_26
    monitor-exit v1
    :try_end_27
    .catchall {:try_start_26 .. :try_end_27} :catchall_25

    throw v0

    .line 780
    :cond_28
    return-void
.end method

.method public b(LaH/A;)V
    .registers 3
    .parameter

    .prologue
    .line 759
    iget-object v0, p0, LaH/r;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 760
    return-void
.end method
