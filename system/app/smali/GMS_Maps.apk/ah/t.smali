.class public LaH/t;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static g:LaH/v;


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:I

.field private c:I

.field private d:I

.field private final e:Ljava/lang/Object;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 64
    new-instance v0, LaH/v;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LaH/v;-><init>(LaH/u;)V

    sput-object v0, LaH/t;->g:LaH/v;

    return-void
.end method

.method public constructor <init>()V
    .registers 5

    .prologue
    const/16 v3, 0x78

    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {v3}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, LaH/t;->a:Ljava/util/ArrayList;

    .line 30
    iput v0, p0, LaH/t;->b:I

    .line 35
    iput v0, p0, LaH/t;->c:I

    .line 37
    iput v0, p0, LaH/t;->d:I

    .line 43
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, LaH/t;->e:Ljava/lang/Object;

    .line 61
    iput-boolean v0, p0, LaH/t;->f:Z

    .line 68
    :goto_1b
    if-ge v0, v3, :cond_26

    .line 69
    iget-object v1, p0, LaH/t;->a:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b

    .line 71
    :cond_26
    return-void
.end method

.method private a(DDD)D
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    .line 255
    invoke-static {p1, p2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 256
    invoke-static {p3, p4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 257
    add-double v4, p1, p3

    sub-double/2addr v4, p5

    const-wide/high16 v6, 0x4000

    mul-double/2addr v0, v6

    mul-double/2addr v0, v2

    div-double v0, v4, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private a(Ljava/util/ArrayList;D)D
    .registers 14
    .parameter
    .parameter

    .prologue
    .line 237
    const-wide/16 v1, 0x0

    .line 238
    const/4 v0, 0x0

    move-wide v8, v1

    move-wide v2, v8

    move v1, v0

    :goto_6
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_28

    .line 239
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    sub-double/2addr v4, p2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    sub-double/2addr v6, p2

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 238
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 241
    :cond_28
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-double v0, v0

    div-double v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private a()V
    .registers 20

    .prologue
    .line 115
    move-object/from16 v0, p0

    iget v14, v0, LaH/t;->d:I

    .line 116
    const-wide/16 v10, 0x0

    .line 117
    const-wide/16 v8, 0x0

    .line 118
    const-wide/high16 v6, -0x4010

    .line 119
    const/4 v5, 0x0

    .line 120
    const-wide/16 v3, 0x0

    .line 121
    move-object/from16 v0, p0

    iget v2, v0, LaH/t;->b:I

    const/4 v1, 0x0

    move-wide v12, v10

    move-wide v10, v8

    move-wide v8, v6

    move-object v7, v5

    move v6, v2

    move v5, v1

    move-wide/from16 v17, v3

    move-wide/from16 v2, v17

    .line 122
    :goto_1c
    move-object/from16 v0, p0

    iget v1, v0, LaH/t;->d:I

    if-ge v5, v1, :cond_6f

    .line 124
    move-object/from16 v0, p0

    iget-object v1, v0, LaH/t;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaH/h;

    .line 126
    invoke-virtual {v1}, LaH/h;->getAccuracy()F

    move-result v4

    float-to-double v15, v4

    add-double/2addr v12, v15

    .line 129
    const-wide/high16 v15, -0x4010

    cmpl-double v4, v8, v15

    if-nez v4, :cond_55

    .line 130
    invoke-virtual {v1}, LaH/h;->getTime()J

    move-result-wide v8

    long-to-double v8, v8

    .line 137
    :goto_3d
    if-nez v7, :cond_63

    .line 138
    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v1

    move-wide/from16 v17, v2

    move-object v3, v1

    move-wide/from16 v1, v17

    .line 123
    :goto_48
    add-int/lit8 v4, v6, 0x1

    rem-int/lit8 v6, v4, 0x78

    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-object v7, v3

    move-wide/from16 v17, v1

    move-wide/from16 v2, v17

    goto :goto_1c

    .line 132
    :cond_55
    invoke-virtual {v1}, LaH/h;->getTime()J

    move-result-wide v15

    long-to-double v15, v15

    sub-double v8, v15, v8

    add-double/2addr v10, v8

    .line 133
    invoke-virtual {v1}, LaH/h;->getTime()J

    move-result-wide v8

    long-to-double v8, v8

    goto :goto_3d

    .line 140
    :cond_63
    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v4

    .line 143
    invoke-virtual {v4, v7}, LaN/B;->a(LaN/B;)J

    move-result-wide v15

    add-long v1, v2, v15

    move-object v3, v4

    .line 144
    goto :goto_48

    .line 149
    :cond_6f
    sget-object v1, LaH/t;->g:LaH/v;

    iput v14, v1, LaH/v;->a:I

    .line 152
    add-int/lit8 v1, v14, -0x1

    int-to-double v4, v1

    div-double v4, v10, v4

    .line 153
    sget-object v1, LaH/t;->g:LaH/v;

    iput-wide v4, v1, LaH/v;->g:D

    .line 154
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, LaH/t;->b(D)V

    .line 157
    add-int/lit8 v1, v14, -0x1

    int-to-long v4, v1

    div-long v1, v2, v4

    long-to-double v1, v1

    .line 158
    sget-object v3, LaH/t;->g:LaH/v;

    iput-wide v1, v3, LaH/v;->e:D

    .line 159
    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, LaH/t;->a(D)V

    .line 162
    int-to-double v1, v14

    div-double v1, v12, v1

    .line 163
    sget-object v3, LaH/t;->g:LaH/v;

    iput-wide v1, v3, LaH/v;->b:D

    .line 164
    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, LaH/t;->c(D)V

    .line 167
    invoke-direct/range {p0 .. p0}, LaH/t;->b()V

    .line 169
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, LaH/t;->f:Z

    .line 170
    return-void
.end method

.method private a(D)V
    .registers 13
    .parameter

    .prologue
    .line 290
    const-wide/16 v3, 0x0

    .line 291
    const/4 v2, 0x0

    .line 292
    iget v1, p0, LaH/t;->b:I

    const/4 v0, 0x0

    move-object v5, v2

    move v9, v1

    move-wide v1, v3

    move v3, v0

    move v4, v9

    .line 293
    :goto_b
    iget v0, p0, LaH/t;->d:I

    if-ge v3, v0, :cond_34

    .line 295
    iget-object v0, p0, LaH/t;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/h;

    .line 296
    if-nez v5, :cond_25

    .line 297
    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    .line 294
    :goto_1d
    add-int/lit8 v4, v4, 0x1

    rem-int/lit8 v4, v4, 0x78

    add-int/lit8 v3, v3, 0x1

    move-object v5, v0

    goto :goto_b

    .line 299
    :cond_25
    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    .line 300
    invoke-virtual {v0, v5}, LaN/B;->a(LaN/B;)J

    move-result-wide v5

    long-to-double v5, v5

    .line 301
    sub-double v7, v5, p1

    sub-double/2addr v5, p1

    mul-double/2addr v5, v7

    add-double/2addr v1, v5

    .line 304
    goto :goto_1d

    .line 307
    :cond_34
    sget-object v0, LaH/t;->g:LaH/v;

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    iget v3, p0, LaH/t;->d:I

    add-int/lit8 v3, v3, -0x1

    int-to-double v3, v3

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    iput-wide v1, v0, LaH/v;->f:D

    .line 309
    return-void
.end method

.method private b()V
    .registers 13

    .prologue
    .line 187
    sget-object v0, LaH/t;->g:LaH/v;

    const-wide/high16 v1, 0x7ff8

    iput-wide v1, v0, LaH/v;->d:D

    .line 189
    const-wide/16 v2, 0x0

    .line 191
    iget v0, p0, LaH/t;->d:I

    add-int/lit8 v0, v0, -0x2

    invoke-static {v0}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v11

    .line 192
    const/4 v1, 0x0

    iget v0, p0, LaH/t;->b:I

    move v8, v1

    move-wide v9, v2

    .line 193
    :goto_15
    iget v1, p0, LaH/t;->d:I

    add-int/lit8 v1, v1, -0x2

    if-ge v8, v1, :cond_86

    .line 196
    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v7, v0, 0x78

    .line 197
    add-int/lit8 v0, v7, -0x1

    add-int/lit8 v0, v0, 0x78

    rem-int/lit8 v0, v0, 0x78

    .line 198
    add-int/lit8 v1, v7, 0x1

    rem-int/lit8 v1, v1, 0x78

    .line 200
    iget-object v2, p0, LaH/t;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/h;

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v5

    .line 201
    iget-object v0, p0, LaH/t;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/h;

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v3

    .line 202
    iget-object v0, p0, LaH/t;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/h;

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    .line 204
    invoke-virtual {v3, v5}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a6

    invoke-virtual {v3, v0}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_60

    move-wide v1, v9

    .line 194
    :goto_5a
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    move-wide v9, v1

    move v0, v7

    goto :goto_15

    .line 208
    :cond_60
    invoke-virtual {v3, v5}, LaN/B;->a(LaN/B;)J

    move-result-wide v1

    long-to-double v1, v1

    .line 209
    invoke-virtual {v3, v0}, LaN/B;->a(LaN/B;)J

    move-result-wide v3

    long-to-double v3, v3

    .line 210
    invoke-virtual {v5, v0}, LaN/B;->a(LaN/B;)J

    move-result-wide v5

    long-to-double v5, v5

    move-object v0, p0

    .line 211
    invoke-direct/range {v0 .. v6}, LaH/t;->a(DDD)D

    move-result-wide v0

    .line 215
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-eqz v2, :cond_7c

    move-wide v1, v9

    .line 216
    goto :goto_5a

    .line 218
    :cond_7c
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    add-double/2addr v9, v0

    move-wide v1, v9

    goto :goto_5a

    .line 222
    :cond_86
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_a5

    .line 223
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-double v0, v0

    div-double v0, v9, v0

    .line 224
    invoke-direct {p0, v11, v0, v1}, LaH/t;->a(Ljava/util/ArrayList;D)D

    move-result-wide v0

    .line 225
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    .line 228
    const-wide v2, 0x3ff921fb54442d18L

    div-double/2addr v0, v2

    .line 229
    sget-object v2, LaH/t;->g:LaH/v;

    iput-wide v0, v2, LaH/v;->d:D

    .line 231
    :cond_a5
    return-void

    :cond_a6
    move-wide v1, v9

    goto :goto_5a
.end method

.method private b(D)V
    .registers 16
    .parameter

    .prologue
    const-wide/high16 v5, -0x4010

    .line 312
    const-wide/16 v2, 0x0

    .line 314
    iget v1, p0, LaH/t;->b:I

    const/4 v0, 0x0

    move-wide v7, v2

    move v2, v1

    move-wide v3, v5

    move v1, v0

    .line 315
    :goto_b
    iget v0, p0, LaH/t;->d:I

    if-ge v1, v0, :cond_3f

    .line 317
    iget-object v0, p0, LaH/t;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/h;

    .line 318
    cmpl-double v9, v3, v5

    if-nez v9, :cond_28

    .line 319
    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v3

    long-to-double v3, v3

    .line 316
    :goto_20
    add-int/lit8 v0, v2, 0x1

    rem-int/lit8 v2, v0, 0x78

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 321
    :cond_28
    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v9

    long-to-double v9, v9

    sub-double/2addr v9, v3

    sub-double/2addr v9, p1

    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v11

    long-to-double v11, v11

    sub-double v3, v11, v3

    sub-double/2addr v3, p1

    mul-double/2addr v3, v9

    add-double/2addr v7, v3

    .line 324
    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v3

    long-to-double v3, v3

    goto :goto_20

    .line 327
    :cond_3f
    sget-object v0, LaH/t;->g:LaH/v;

    iget v1, p0, LaH/t;->d:I

    add-int/lit8 v1, v1, -0x1

    int-to-double v1, v1

    div-double v1, v7, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    iput-wide v1, v0, LaH/v;->h:D

    .line 329
    return-void
.end method

.method private b(Ljava/lang/StringBuilder;)V
    .registers 5
    .parameter

    .prologue
    .line 265
    const-string v0, "nf"

    sget-object v1, LaH/t;->g:LaH/v;

    iget v1, v1, LaH/v;->a:I

    invoke-static {v0, v1, p1}, Lbm/m;->a(Ljava/lang/String;ILjava/lang/StringBuilder;)V

    .line 267
    const-string v0, "mt"

    sget-object v1, LaH/t;->g:LaH/v;

    iget-wide v1, v1, LaH/v;->g:D

    invoke-static {v0, v1, v2, p1}, Lbm/m;->a(Ljava/lang/String;DLjava/lang/StringBuilder;)V

    .line 270
    const-string v0, "md"

    sget-object v1, LaH/t;->g:LaH/v;

    iget-wide v1, v1, LaH/v;->e:D

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    invoke-static {v0, v1, v2, p1}, Lbm/m;->a(Ljava/lang/String;DLjava/lang/StringBuilder;)V

    .line 273
    const-string v0, "ma"

    sget-object v1, LaH/t;->g:LaH/v;

    iget-wide v1, v1, LaH/v;->b:D

    invoke-static {v0, v1, v2, p1}, Lbm/m;->a(Ljava/lang/String;DLjava/lang/StringBuilder;)V

    .line 275
    const-string v0, "st"

    sget-object v1, LaH/t;->g:LaH/v;

    iget-wide v1, v1, LaH/v;->h:D

    invoke-static {v0, v1, v2, p1}, Lbm/m;->a(Ljava/lang/String;DLjava/lang/StringBuilder;)V

    .line 278
    const-string v0, "sd"

    sget-object v1, LaH/t;->g:LaH/v;

    iget-wide v1, v1, LaH/v;->f:D

    invoke-static {v0, v1, v2, p1}, Lbm/m;->a(Ljava/lang/String;DLjava/lang/StringBuilder;)V

    .line 281
    const-string v0, "sa"

    sget-object v1, LaH/t;->g:LaH/v;

    iget-wide v1, v1, LaH/v;->c:D

    invoke-static {v0, v1, v2, p1}, Lbm/m;->a(Ljava/lang/String;DLjava/lang/StringBuilder;)V

    .line 283
    sget-object v0, LaH/t;->g:LaH/v;

    iget-wide v0, v0, LaH/v;->d:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_56

    .line 284
    const-string v0, "pe"

    sget-object v1, LaH/t;->g:LaH/v;

    iget-wide v1, v1, LaH/v;->d:D

    invoke-static {v0, v1, v2, p1}, Lbm/m;->a(Ljava/lang/String;DLjava/lang/StringBuilder;)V

    .line 287
    :cond_56
    return-void
.end method

.method private c()V
    .registers 10

    .prologue
    const/4 v2, 0x0

    .line 349
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v3

    move v1, v2

    .line 351
    :goto_e
    if-nez v1, :cond_47

    iget v0, p0, LaH/t;->d:I

    if-lez v0, :cond_47

    .line 352
    iget-object v0, p0, LaH/t;->a:Ljava/util/ArrayList;

    iget v5, p0, LaH/t;->b:I

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/h;

    .line 353
    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v5

    const-wide/32 v7, 0x1d4c0

    add-long/2addr v5, v7

    cmp-long v0, v5, v3

    if-gez v0, :cond_45

    .line 354
    iget-object v0, p0, LaH/t;->a:Ljava/util/ArrayList;

    iget v5, p0, LaH/t;->b:I

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 355
    iget v0, p0, LaH/t;->b:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x78

    iput v0, p0, LaH/t;->b:I

    .line 356
    iput-boolean v2, p0, LaH/t;->f:Z

    .line 357
    iget v0, p0, LaH/t;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LaH/t;->d:I

    move v0, v1

    :goto_43
    move v1, v0

    .line 361
    goto :goto_e

    .line 359
    :cond_45
    const/4 v0, 0x1

    goto :goto_43

    .line 362
    :cond_47
    return-void
.end method

.method private c(D)V
    .registers 15
    .parameter

    .prologue
    .line 332
    const-wide/16 v2, 0x0

    .line 333
    iget v1, p0, LaH/t;->b:I

    const/4 v0, 0x0

    move v9, v1

    move v1, v0

    move-wide v10, v2

    move-wide v3, v10

    move v2, v9

    .line 334
    :goto_a
    iget v0, p0, LaH/t;->d:I

    if-ge v1, v0, :cond_32

    .line 336
    iget-object v0, p0, LaH/t;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/h;

    .line 337
    invoke-virtual {v0}, LaH/h;->hasAccuracy()Z

    move-result v5

    if-eqz v5, :cond_2a

    .line 338
    invoke-virtual {v0}, LaH/h;->getAccuracy()F

    move-result v5

    float-to-double v5, v5

    sub-double/2addr v5, p1

    invoke-virtual {v0}, LaH/h;->getAccuracy()F

    move-result v0

    float-to-double v7, v0

    sub-double/2addr v7, p1

    mul-double/2addr v5, v7

    add-double/2addr v3, v5

    .line 335
    :cond_2a
    add-int/lit8 v0, v2, 0x1

    rem-int/lit8 v2, v0, 0x78

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 341
    :cond_32
    sget-object v0, LaH/t;->g:LaH/v;

    iget v1, p0, LaH/t;->d:I

    int-to-double v1, v1

    div-double v1, v3, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    iput-wide v1, v0, LaH/v;->c:D

    .line 342
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/StringBuilder;)V
    .registers 5
    .parameter

    .prologue
    .line 93
    iget-object v1, p0, LaH/t;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 94
    :try_start_3
    invoke-direct {p0}, LaH/t;->c()V

    .line 96
    iget v0, p0, LaH/t;->d:I

    const/4 v2, 0x3

    if-ge v0, v2, :cond_d

    .line 97
    monitor-exit v1

    .line 106
    :goto_c
    return-void

    .line 99
    :cond_d
    iget-boolean v0, p0, LaH/t;->f:Z

    if-nez v0, :cond_19

    .line 100
    sget-object v0, LaH/t;->g:LaH/v;

    invoke-virtual {v0}, LaH/v;->a()V

    .line 101
    invoke-direct {p0}, LaH/t;->a()V

    .line 104
    :cond_19
    invoke-direct {p0, p1}, LaH/t;->b(Ljava/lang/StringBuilder;)V

    .line 105
    monitor-exit v1

    goto :goto_c

    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    throw v0
.end method
