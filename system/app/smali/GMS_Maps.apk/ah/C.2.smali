.class LaH/C;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/concurrent/ConcurrentMap;

.field private final b:Landroid/location/LocationManager;

.field private c:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method constructor <init>(Landroid/location/LocationManager;)V
    .registers 3
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, LaH/C;->b:Landroid/location/LocationManager;

    .line 37
    invoke-static {}, Lcom/google/common/collect/Maps;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LaH/C;->a:Ljava/util/concurrent/ConcurrentMap;

    .line 38
    return-void
.end method

.method static synthetic a(LaH/C;)Ljava/util/concurrent/ConcurrentMap;
    .registers 2
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, LaH/C;->a:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method static synthetic a(LaH/C;Ljava/lang/String;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1}, LaH/C;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, LaH/C;->b:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_18

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, LaH/C;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method


# virtual methods
.method public a()V
    .registers 8

    .prologue
    .line 41
    iget-object v0, p0, LaH/C;->c:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_c

    iget-object v0, p0, LaH/C;->c:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 42
    :cond_c
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, LaH/C;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 45
    :cond_12
    new-instance v1, LaH/D;

    invoke-direct {v1, p0}, LaH/D;-><init>(LaH/C;)V

    .line 53
    iget-object v0, p0, LaH/C;->c:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 54
    return-void
.end method

.method declared-synchronized a(Ljava/lang/String;)Z
    .registers 4
    .parameter

    .prologue
    .line 69
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaH/C;->a:Ljava/util/concurrent/ConcurrentMap;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    iget-object v0, p0, LaH/C;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_19

    move-result v0

    monitor-exit p0

    return v0

    .line 69
    :catchall_19
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, LaH/C;->c:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_9

    .line 58
    iget-object v0, p0, LaH/C;->c:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    .line 60
    :cond_9
    return-void
.end method
