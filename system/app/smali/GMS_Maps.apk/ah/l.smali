.class LaH/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field b:Lo/af;

.field c:Lo/T;

.field d:LO/H;

.field e:D

.field f:I

.field g:Z


# direct methods
.method constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const-wide/high16 v0, -0x4010

    iput-wide v0, p0, LaH/l;->e:D

    .line 68
    iput v2, p0, LaH/l;->f:I

    .line 74
    iput-boolean v2, p0, LaH/l;->g:Z

    .line 87
    return-void
.end method

.method constructor <init>(LaH/l;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const-wide/high16 v0, -0x4010

    iput-wide v0, p0, LaH/l;->e:D

    .line 68
    iput v2, p0, LaH/l;->f:I

    .line 74
    iput-boolean v2, p0, LaH/l;->g:Z

    .line 77
    iget-boolean v0, p1, LaH/l;->a:Z

    iput-boolean v0, p0, LaH/l;->a:Z

    .line 78
    iget-object v0, p1, LaH/l;->b:Lo/af;

    iput-object v0, p0, LaH/l;->b:Lo/af;

    .line 79
    iget-object v0, p1, LaH/l;->c:Lo/T;

    iput-object v0, p0, LaH/l;->c:Lo/T;

    .line 80
    iget-object v0, p1, LaH/l;->d:LO/H;

    iput-object v0, p0, LaH/l;->d:LO/H;

    .line 81
    iget-wide v0, p1, LaH/l;->e:D

    iput-wide v0, p0, LaH/l;->e:D

    .line 82
    iget v0, p1, LaH/l;->f:I

    iput v0, p0, LaH/l;->f:I

    .line 83
    iget-boolean v0, p1, LaH/l;->g:Z

    iput-boolean v0, p0, LaH/l;->g:Z

    .line 84
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 91
    instance-of v1, p1, LaH/l;

    if-nez v1, :cond_6

    .line 95
    :cond_5
    :goto_5
    return v0

    .line 94
    :cond_6
    check-cast p1, LaH/l;

    .line 95
    iget-boolean v1, p0, LaH/l;->a:Z

    iget-boolean v2, p1, LaH/l;->a:Z

    if-ne v1, v2, :cond_5

    iget-object v1, p0, LaH/l;->b:Lo/af;

    iget-object v2, p1, LaH/l;->b:Lo/af;

    invoke-static {v1, v2}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LaH/l;->c:Lo/T;

    iget-object v2, p1, LaH/l;->c:Lo/T;

    invoke-static {v1, v2}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LaH/l;->d:LO/H;

    iget-object v2, p1, LaH/l;->d:LO/H;

    invoke-static {v1, v2}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget v1, p0, LaH/l;->f:I

    iget v2, p1, LaH/l;->f:I

    if-ne v1, v2, :cond_5

    iget-boolean v1, p0, LaH/l;->g:Z

    iget-boolean v2, p1, LaH/l;->g:Z

    if-ne v1, v2, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method
