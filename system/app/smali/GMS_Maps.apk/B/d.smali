.class LB/d;
.super LR/h;
.source "SourceFile"


# instance fields
.field final synthetic a:LB/a;

.field private final c:Ljava/lang/Long;

.field private final d:Ljava/util/List;


# direct methods
.method public constructor <init>(LB/a;J)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 779
    iput-object p1, p0, LB/d;->a:LB/a;

    .line 782
    const v0, 0x7fffffff

    invoke-direct {p0, v0}, LR/h;-><init>(I)V

    .line 777
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LB/d;->d:Ljava/util/List;

    .line 784
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LB/d;->c:Ljava/lang/Long;

    .line 785
    return-void
.end method

.method static synthetic a(LB/d;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 766
    iget-object v0, p0, LB/d;->d:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 6

    .prologue
    .line 813
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, LB/d;->f()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 814
    invoke-virtual {p0}, LB/d;->j()LR/i;

    move-result-object v1

    .line 815
    :goto_d
    invoke-virtual {v1}, LR/i;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 816
    invoke-virtual {v1}, LR/i;->a()LR/j;

    move-result-object v2

    .line 817
    iget-object v3, v2, LR/j;->a:Ljava/lang/Object;

    sget-object v4, LB/a;->a:Lo/aq;

    if-ne v3, v4, :cond_31

    .line 823
    :cond_1d
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_21
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    .line 824
    invoke-virtual {p0, v0}, LB/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_21

    .line 820
    :cond_31
    iget-object v2, v2, LR/j;->a:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 826
    :cond_37
    return-void
.end method

.method public a(LB/b;)V
    .registers 3
    .parameter

    .prologue
    .line 898
    iget-object v0, p0, LB/d;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 899
    return-void
.end method

.method public a(LD/a;)V
    .registers 6
    .parameter

    .prologue
    .line 908
    iget-object v0, p0, LB/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/b;

    .line 909
    iget-object v2, v0, LB/b;->a:LF/T;

    invoke-interface {v2, p1}, LF/T;->b(LD/a;)V

    .line 910
    iget-object v2, p0, LB/d;->a:LB/a;

    iget v3, v0, LB/b;->b:I

    invoke-static {v2, v3}, LB/a;->a(LB/a;I)I

    .line 911
    iget-object v2, p0, LB/d;->a:LB/a;

    iget v0, v0, LB/b;->c:I

    invoke-static {v2, v0}, LB/a;->b(LB/a;I)I

    goto :goto_6

    .line 913
    :cond_26
    iget-object v0, p0, LB/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 914
    return-void
.end method

.method protected a(Lo/aq;LB/b;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 794
    invoke-super {p0, p1, p2}, LR/h;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 795
    iget-object v0, p0, LB/d;->a:LB/a;

    iget v1, p2, LB/b;->b:I

    invoke-static {v0, v1}, LB/a;->a(LB/a;I)I

    .line 796
    iget-object v0, p0, LB/d;->a:LB/a;

    iget v1, p2, LB/b;->c:I

    invoke-static {v0, v1}, LB/a;->b(LB/a;I)I

    .line 798
    iget-object v0, p2, LB/b;->a:LF/T;

    if-eqz v0, :cond_1d

    .line 804
    iput v2, p2, LB/b;->b:I

    .line 805
    iput v2, p2, LB/b;->c:I

    .line 806
    invoke-virtual {p0, p2}, LB/d;->a(LB/b;)V

    .line 808
    :cond_1d
    return-void
.end method

.method public b()V
    .registers 8

    .prologue
    .line 834
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, LB/d;->f()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 835
    invoke-virtual {p0}, LB/d;->j()LR/i;

    move-result-object v2

    .line 836
    :goto_d
    invoke-virtual {v2}, LR/i;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 837
    invoke-virtual {v2}, LR/i;->a()LR/j;

    move-result-object v3

    .line 838
    iget-object v0, v3, LR/j;->a:Ljava/lang/Object;

    sget-object v4, LB/a;->a:Lo/aq;

    if-ne v0, v4, :cond_31

    .line 855
    :cond_1d
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_21
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    .line 856
    invoke-virtual {p0, v0}, LB/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_21

    .line 841
    :cond_31
    iget-object v0, v3, LR/j;->b:Ljava/lang/Object;

    check-cast v0, LB/b;

    .line 842
    iget-object v4, v0, LB/b;->a:LF/T;

    .line 843
    if-eqz v4, :cond_3f

    invoke-interface {v4}, LF/T;->a()Z

    move-result v5

    if-nez v5, :cond_45

    .line 844
    :cond_3f
    iget-object v0, v3, LR/j;->a:Ljava/lang/Object;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 849
    :cond_45
    iget-object v3, p0, LB/d;->c:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, LD/a;->b(J)LD/a;

    move-result-object v3

    invoke-interface {v4, v3}, LF/T;->a(LD/a;)V

    .line 850
    iget-object v3, p0, LB/d;->a:LB/a;

    iget v4, v0, LB/b;->b:I

    invoke-static {v3, v4}, LB/a;->a(LB/a;I)I

    .line 851
    const/4 v3, 0x0

    iput v3, v0, LB/b;->b:I

    goto :goto_d

    .line 858
    :cond_5d
    return-void
.end method

.method protected synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 766
    check-cast p1, Lo/aq;

    check-cast p2, LB/b;

    invoke-virtual {p0, p1, p2}, LB/d;->a(Lo/aq;LB/b;)V

    return-void
.end method

.method public c()LR/j;
    .registers 3

    .prologue
    .line 862
    invoke-virtual {p0}, LB/d;->j()LR/i;

    move-result-object v0

    .line 863
    invoke-virtual {v0}, LR/i;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-virtual {v0}, LR/i;->a()LR/j;

    move-result-object v0

    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public d()V
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 871
    iget-object v0, p0, LB/d;->a:LB/a;

    invoke-static {v0}, LB/a;->a(LB/a;)Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v4

    .line 872
    sget-object v0, LB/a;->a:Lo/aq;

    invoke-virtual {p0, v0}, LB/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/b;

    .line 873
    if-nez v0, :cond_22

    .line 874
    sget-object v6, LB/a;->a:Lo/aq;

    new-instance v0, LB/b;

    const/4 v1, 0x0

    move v3, v2

    invoke-direct/range {v0 .. v5}, LB/b;-><init>(LF/T;IIJ)V

    invoke-virtual {p0, v6, v0}, LB/d;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 878
    :goto_21
    return-void

    .line 876
    :cond_22
    iput-wide v4, v0, LB/b;->d:J

    goto :goto_21
.end method
