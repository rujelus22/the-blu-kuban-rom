.class public Lbe/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:Lcom/google/googlenav/friend/history/c;

.field private final b:Landroid/content/Context;

.field private final c:Z

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/history/c;Landroid/content/Context;ZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lbe/k;->a:Lcom/google/googlenav/friend/history/c;

    .line 59
    iput-object p2, p0, Lbe/k;->b:Landroid/content/Context;

    .line 60
    iput-boolean p3, p0, Lbe/k;->c:Z

    .line 61
    iput-boolean p4, p0, Lbe/k;->d:Z

    .line 62
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 93
    const/4 v0, 0x4

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .registers 4
    .parameter

    .prologue
    .line 83
    new-instance v1, Lbe/m;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbe/m;-><init>(Lbe/l;)V

    .line 84
    const v0, 0x7f1000ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbe/m;->a(Lbe/m;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 85
    const v0, 0x7f100259

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbe/m;->b(Lbe/m;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 86
    const v0, 0x7f100258

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lbe/m;->a(Lbe/m;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 88
    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 66
    check-cast p2, Lbe/m;

    .line 67
    invoke-static {p2}, Lbe/m;->a(Lbe/m;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbe/k;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/c;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lbe/k;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/c;->j()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 69
    invoke-static {p2}, Lbe/m;->b(Lbe/m;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x268

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lbe/k;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/history/c;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    :goto_34
    invoke-static {p2}, Lbe/m;->c(Lbe/m;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lbe/k;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lbe/k;->a:Lcom/google/googlenav/friend/history/c;

    iget-boolean v3, p0, Lbe/k;->c:Z

    iget-boolean v4, p0, Lbe/k;->d:Z

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/friend/history/c;->a(ZZ)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 79
    return-void

    .line 73
    :cond_50
    invoke-static {p2}, Lbe/m;->b(Lbe/m;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x262

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lbe/k;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v3}, Lcom/google/googlenav/friend/history/c;->h()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_34
.end method

.method public b()I
    .registers 2

    .prologue
    .line 98
    const v0, 0x7f0400b8

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method
