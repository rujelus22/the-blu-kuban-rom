.class public Lbe/D;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:Lcom/google/googlenav/friend/history/c;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/friend/history/c;)V
    .registers 2
    .parameter

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lbe/D;->a:Lcom/google/googlenav/friend/history/c;

    .line 26
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 50
    const/4 v0, 0x2

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .registers 4
    .parameter

    .prologue
    .line 42
    new-instance v1, Lbe/F;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbe/F;-><init>(Lbe/E;)V

    .line 43
    const v0, 0x7f100284

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbe/F;->a(Lbe/F;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 44
    const v0, 0x7f100285

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbe/F;->b(Lbe/F;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 45
    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 30
    check-cast p2, Lbe/F;

    .line 31
    invoke-static {p2}, Lbe/F;->a(Lbe/F;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbe/D;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/c;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 32
    iget-object v0, p0, Lbe/D;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/c;->a()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 33
    invoke-static {p2}, Lbe/F;->b(Lbe/F;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 34
    invoke-static {p2}, Lbe/F;->b(Lbe/F;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbe/D;->a:Lcom/google/googlenav/friend/history/c;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    :goto_2c
    return-void

    .line 36
    :cond_2d
    invoke-static {p2}, Lbe/F;->b(Lbe/F;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2c
.end method

.method public b()I
    .registers 2

    .prologue
    .line 55
    const v0, 0x7f0400d2

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method
