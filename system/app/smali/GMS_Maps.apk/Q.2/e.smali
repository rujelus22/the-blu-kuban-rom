.class public Lq/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lr/z;

.field private b:Ljava/util/List;


# direct methods
.method public constructor <init>(Lr/z;)V
    .registers 2
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lq/e;->a:Lr/z;

    .line 57
    return-void
.end method

.method static synthetic a(F)F
    .registers 2
    .parameter

    .prologue
    .line 26
    invoke-static {p0}, Lq/e;->b(F)F

    move-result v0

    return v0
.end method

.method private a(Lo/aL;Lq/f;I)Lq/f;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v9, 0x4234

    const/4 v2, 0x0

    .line 271
    invoke-virtual {p2}, Lq/f;->a()Lo/T;

    move-result-object v3

    .line 272
    invoke-virtual {p2}, Lq/f;->b()F

    move-result v4

    .line 273
    mul-int v0, p3, p3

    int-to-float v5, v0

    move v1, v2

    .line 274
    :goto_f
    invoke-virtual {p1}, Lo/aL;->j()I

    move-result v0

    if-ge v1, v0, :cond_7e

    .line 275
    invoke-virtual {p1, v1}, Lo/aL;->a(I)Lo/n;

    move-result-object v0

    .line 276
    instance-of v6, v0, Lo/af;

    if-eqz v6, :cond_23

    .line 277
    check-cast v0, Lo/af;

    .line 278
    iget-object v6, p2, Lq/f;->a:Lo/af;

    if-ne v0, v6, :cond_27

    .line 274
    :cond_23
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f

    .line 281
    :cond_27
    invoke-virtual {v0}, Lo/af;->b()Lo/X;

    move-result-object v6

    .line 283
    iget-object v7, p2, Lq/f;->a:Lo/af;

    invoke-virtual {v7}, Lo/af;->f()I

    move-result v7

    invoke-virtual {v0}, Lo/af;->f()I

    move-result v8

    if-ne v7, v8, :cond_23

    invoke-virtual {p2, v0}, Lq/f;->a(Lo/af;)Z

    move-result v7

    if-eqz v7, :cond_23

    .line 287
    invoke-virtual {v6, v2}, Lo/X;->a(I)Lo/T;

    move-result-object v7

    invoke-virtual {v3, v7}, Lo/T;->d(Lo/T;)F

    move-result v7

    cmpg-float v7, v7, v5

    if-gez v7, :cond_5b

    invoke-virtual {v6, v2}, Lo/X;->d(I)F

    move-result v7

    invoke-static {v4, v7}, Lo/V;->a(FF)F

    move-result v7

    cmpg-float v7, v7, v9

    if-gez v7, :cond_5b

    .line 291
    const/4 v2, 0x1

    .line 300
    :goto_56
    iput-boolean v2, p2, Lq/f;->b:Z

    .line 301
    iput-object v0, p2, Lq/f;->a:Lo/af;

    .line 305
    :goto_5a
    return-object p2

    .line 292
    :cond_5b
    invoke-virtual {v6}, Lo/X;->c()Lo/T;

    move-result-object v7

    invoke-virtual {v3, v7}, Lo/T;->d(Lo/T;)F

    move-result v7

    cmpg-float v7, v7, v5

    if-gez v7, :cond_23

    invoke-virtual {v6}, Lo/X;->b()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v6, v7}, Lo/X;->d(I)F

    move-result v6

    invoke-static {v6}, Lq/e;->b(F)F

    move-result v6

    invoke-static {v4, v6}, Lo/V;->a(FF)F

    move-result v6

    cmpg-float v6, v6, v9

    if-gez v6, :cond_23

    goto :goto_56

    .line 305
    :cond_7e
    const/4 p2, 0x0

    goto :goto_5a
.end method

.method private a(Lq/f;I)Lq/f;
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 244
    invoke-virtual {p1}, Lq/f;->a()Lo/T;

    move-result-object v0

    invoke-static {v0, p2}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v0

    invoke-static {v0}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v0

    const/16 v1, 0xe

    invoke-static {v0, v1}, Lo/aq;->a(Lo/aR;I)Ljava/util/ArrayList;

    move-result-object v2

    .line 249
    const/4 v0, 0x0

    move v1, v0

    :goto_14
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_36

    .line 253
    iget-object v3, p0, Lq/e;->a:Lr/z;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    const/4 v4, 0x1

    invoke-interface {v3, v0, v4}, Lr/z;->a(Lo/aq;Z)Lo/ap;

    move-result-object v0

    .line 254
    if-eqz v0, :cond_32

    .line 255
    check-cast v0, Lo/aL;

    invoke-direct {p0, v0, p1, p2}, Lq/e;->a(Lo/aL;Lq/f;I)Lq/f;

    move-result-object v0

    .line 257
    if-eqz v0, :cond_32

    .line 262
    :goto_31
    return-object v0

    .line 249
    :cond_32
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_14

    .line 262
    :cond_36
    const/4 v0, 0x0

    goto :goto_31
.end method

.method private a(Lo/aL;Lo/h;ZLq/d;)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 132
    .line 133
    new-instance v15, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v15, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 134
    const/4 v1, 0x0

    move v13, v1

    :goto_8
    invoke-virtual/range {p1 .. p1}, Lo/aL;->j()I

    move-result v1

    if-ge v13, v1, :cond_88

    .line 135
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lo/aL;->a(I)Lo/n;

    move-result-object v1

    .line 136
    instance-of v2, v1, Lo/af;

    if-eqz v2, :cond_41

    .line 137
    check-cast v1, Lo/af;

    .line 138
    invoke-virtual {v1}, Lo/af;->b()Lo/X;

    move-result-object v2

    .line 139
    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v15}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    .line 141
    const/4 v2, 0x0

    move v3, v2

    :goto_25
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_3a

    .line 142
    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lo/X;

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Lq/d;->a(Lo/af;Lo/X;)V

    .line 141
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_25

    .line 144
    :cond_3a
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    .line 134
    :cond_3d
    :goto_3d
    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto :goto_8

    .line 145
    :cond_41
    if-eqz p3, :cond_3d

    instance-of v2, v1, Lo/M;

    if-eqz v2, :cond_3d

    move-object v12, v1

    .line 149
    check-cast v12, Lo/M;

    .line 150
    invoke-virtual {v12}, Lo/M;->b()Lo/X;

    move-result-object v1

    .line 151
    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v15}, Lo/h;->a(Lo/X;Ljava/util/ArrayList;)V

    .line 152
    const/4 v1, 0x0

    new-array v11, v1, [I

    .line 154
    const/4 v1, 0x0

    move v14, v1

    :goto_58
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v14, v1, :cond_84

    .line 155
    new-instance v1, Lo/af;

    const/4 v2, 0x0

    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lo/X;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x0

    const/16 v8, 0x10

    invoke-virtual {v12}, Lo/M;->i()I

    move-result v9

    const/4 v10, 0x0

    invoke-direct/range {v1 .. v11}, Lo/af;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;III[I)V

    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lo/X;

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Lq/d;->a(Lo/af;Lo/X;)V

    .line 154
    add-int/lit8 v1, v14, 0x1

    move v14, v1

    goto :goto_58

    .line 162
    :cond_84
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    goto :goto_3d

    .line 165
    :cond_88
    return-void
.end method

.method private static b(F)F
    .registers 3
    .parameter

    .prologue
    const/high16 v1, 0x4334

    .line 309
    cmpl-float v0, p0, v1

    if-lez v0, :cond_9

    sub-float v0, p0, v1

    :goto_8
    return v0

    :cond_9
    add-float v0, p0, v1

    goto :goto_8
.end method


# virtual methods
.method public a(Lo/af;FFI)Ljava/util/List;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 227
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 229
    new-instance v0, Lq/f;

    invoke-direct {v0, p1, p2}, Lq/f;-><init>(Lo/af;F)V

    .line 231
    :goto_a
    if-eqz v0, :cond_26

    const/4 v2, 0x0

    cmpl-float v2, p3, v2

    if-lez v2, :cond_26

    .line 232
    iget-object v2, v0, Lq/f;->a:Lo/af;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    iget-object v2, v0, Lq/f;->a:Lo/af;

    invoke-virtual {v2}, Lo/af;->b()Lo/X;

    move-result-object v2

    invoke-virtual {v2}, Lo/X;->d()F

    move-result v2

    sub-float/2addr p3, v2

    .line 234
    invoke-direct {p0, v0, p4}, Lq/e;->a(Lq/f;I)Lq/f;

    move-result-object v0

    goto :goto_a

    .line 237
    :cond_26
    return-object v1
.end method

.method public a(Lo/ad;ZJ)Lq/d;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 74
    invoke-static {p1}, Lo/aR;->a(Lo/ad;)Lo/aR;

    move-result-object v0

    const/16 v1, 0xe

    invoke-static {v0, v1}, Lo/aq;->a(Lo/aR;I)Ljava/util/ArrayList;

    move-result-object v5

    .line 76
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 79
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v2

    move v3, v0

    .line 80
    :goto_17
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_37

    .line 81
    iget-object v7, p0, Lq/e;->a:Lr/z;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-interface {v7, v0, v2}, Lr/z;->a(Lo/aq;Z)Lo/ap;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_33

    .line 83
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    invoke-virtual {v5, v1, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 85
    add-int/lit8 v3, v3, -0x1

    .line 80
    :cond_33
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_17

    .line 91
    :cond_37
    if-lez v3, :cond_6a

    .line 92
    new-instance v7, Ls/a;

    invoke-direct {v7, v3}, Ls/a;-><init>(I)V

    move v1, v2

    .line 94
    :goto_3f
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5a

    .line 95
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_56

    .line 97
    iget-object v3, p0, Lq/e;->a:Lr/z;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-interface {v3, v0, v7}, Lr/z;->a(Lo/aq;Ls/e;)V

    .line 94
    :cond_56
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3f

    .line 100
    :cond_5a
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gez v0, :cond_8e

    .line 101
    invoke-virtual {v7}, Ls/a;->b()V

    .line 107
    :cond_63
    invoke-virtual {v7}, Ls/a;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 111
    :cond_6a
    iput-object v6, p0, Lq/e;->b:Ljava/util/List;

    .line 114
    new-instance v1, Lq/d;

    invoke-direct {v1}, Lq/d;-><init>()V

    .line 115
    new-instance v3, Lo/h;

    invoke-direct {v3, p1}, Lo/h;-><init>(Lo/ae;)V

    .line 116
    :goto_76
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_96

    .line 117
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/ap;

    .line 118
    instance-of v4, v0, Lo/aL;

    if-eqz v4, :cond_8b

    .line 119
    check-cast v0, Lo/aL;

    invoke-direct {p0, v0, v3, p2, v1}, Lq/e;->a(Lo/aL;Lo/h;ZLq/d;)V

    .line 116
    :cond_8b
    add-int/lit8 v2, v2, 0x1

    goto :goto_76

    .line 103
    :cond_8e
    invoke-virtual {v7, p3, p4}, Ls/a;->a(J)Z

    move-result v0

    if-nez v0, :cond_63

    move-object v0, v4

    .line 122
    :goto_95
    return-object v0

    :cond_96
    move-object v0, v1

    goto :goto_95
.end method
