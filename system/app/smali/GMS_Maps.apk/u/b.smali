.class Lu/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lu/a;

.field private b:Z

.field private c:I

.field private d:Lo/aq;

.field private e:Ljava/util/LinkedHashSet;

.field private f:Ljava/util/LinkedHashSet;

.field private g:Ljava/util/Iterator;


# direct methods
.method public constructor <init>(Lu/a;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 503
    iput-object p1, p0, Lu/b;->a:Lu/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483
    const/4 v0, 0x0

    iput v0, p0, Lu/b;->c:I

    .line 492
    invoke-static {}, Lcom/google/common/collect/dA;->b()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    .line 498
    invoke-static {}, Lcom/google/common/collect/dA;->b()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    .line 504
    iput-boolean p2, p0, Lu/b;->b:Z

    .line 505
    return-void
.end method

.method private b(Z)V
    .registers 5
    .parameter

    .prologue
    .line 537
    iget v0, p0, Lu/b;->c:I

    iget-object v1, p0, Lu/b;->a:Lu/a;

    invoke-static {v1}, Lu/a;->c(Lu/a;)I

    move-result v1

    if-lt v0, v1, :cond_b

    .line 562
    :cond_a
    :goto_a
    return-void

    .line 542
    :cond_b
    if-nez p1, :cond_26

    .line 543
    iget-object v0, p0, Lu/b;->a:Lu/a;

    invoke-static {v0}, Lu/a;->e(Lu/a;)Lg/a;

    move-result-object v0

    iget-object v1, p0, Lu/b;->d:Lo/aq;

    iget-object v2, p0, Lu/b;->a:Lu/a;

    invoke-static {v2}, Lu/a;->d(Lu/a;)Lo/T;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lg/a;->a(Lo/aq;Lo/T;)Lo/aq;

    move-result-object v0

    .line 544
    if-eqz v0, :cond_26

    .line 545
    iget-object v1, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 549
    :cond_26
    iget-object v0, p0, Lu/b;->g:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 551
    iget v0, p0, Lu/b;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lu/b;->c:I

    .line 554
    iget-object v0, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    .line 555
    iget-object v1, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    iput-object v1, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    .line 556
    iput-object v0, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    .line 559
    iget-object v0, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 560
    iget-object v0, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lu/b;->g:Ljava/util/Iterator;

    goto :goto_a
.end method


# virtual methods
.method public a(Z)Lu/c;
    .registers 7
    .parameter

    .prologue
    .line 525
    iget-object v0, p0, Lu/b;->d:Lo/aq;

    if-eqz v0, :cond_7

    .line 526
    invoke-direct {p0, p1}, Lu/b;->b(Z)V

    .line 528
    :cond_7
    iget-object v0, p0, Lu/b;->g:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_11

    .line 529
    const/4 v0, 0x0

    .line 532
    :goto_10
    return-object v0

    .line 531
    :cond_11
    iget-object v0, p0, Lu/b;->g:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    iput-object v0, p0, Lu/b;->d:Lo/aq;

    .line 532
    new-instance v1, Lu/c;

    iget-object v2, p0, Lu/b;->d:Lo/aq;

    iget-object v0, p0, Lu/b;->a:Lu/a;

    invoke-static {v0}, Lu/a;->b(Lu/a;)J

    move-result-wide v3

    iget-boolean v0, p0, Lu/b;->b:Z

    if-nez v0, :cond_33

    iget v0, p0, Lu/b;->c:I

    if-eqz v0, :cond_33

    const/4 v0, 0x1

    :goto_2e
    invoke-direct {v1, v2, v3, v4, v0}, Lu/c;-><init>(Lo/aq;JZ)V

    move-object v0, v1

    goto :goto_10

    :cond_33
    const/4 v0, 0x0

    goto :goto_2e
.end method

.method public a()V
    .registers 3

    .prologue
    .line 515
    const/4 v0, 0x0

    iput v0, p0, Lu/b;->c:I

    .line 516
    iget-object v0, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 517
    iget-object v0, p0, Lu/b;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 518
    const/4 v0, 0x0

    iput-object v0, p0, Lu/b;->d:Lo/aq;

    .line 520
    iget-object v0, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    iget-object v1, p0, Lu/b;->a:Lu/a;

    invoke-static {v1}, Lu/a;->a(Lu/a;)Ljava/util/LinkedHashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 521
    iget-object v0, p0, Lu/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lu/b;->g:Ljava/util/Iterator;

    .line 522
    return-void
.end method
