.class public Lu/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:LF/T;


# instance fields
.field protected volatile a:LD/a;

.field private c:LB/a;

.field private final d:Lr/z;

.field private volatile e:LB/e;

.field private final f:LR/a;

.field private final g:Ljava/util/List;

.field private h:LA/b;

.field private final i:LR/h;

.field private j:I

.field private k:I

.field private final l:Lr/A;

.field private m:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final n:Ljava/util/Set;

.field private final o:Ljava/util/LinkedList;

.field private p:Ljava/util/Map;

.field private q:Ljava/util/Map;

.field private volatile r:I

.field private final s:Lu/a;

.field private t:Lu/c;

.field private final u:Ls/e;

.field private final v:Ls/e;

.field private final w:Lu/h;

.field private volatile x:J

.field private y:Lcom/google/googlenav/common/a;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 118
    new-instance v0, LF/aa;

    invoke-direct {v0}, LF/aa;-><init>()V

    sput-object v0, Lu/d;->b:LF/T;

    return-void
.end method

.method public constructor <init>(LA/c;LB/e;Lu/a;LR/a;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    new-instance v0, LR/h;

    const/16 v1, 0x12c

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lu/d;->i:LR/h;

    .line 182
    iput v3, p0, Lu/d;->j:I

    .line 191
    iput v3, p0, Lu/d;->k:I

    .line 205
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lu/d;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 212
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lu/d;->n:Ljava/util/Set;

    .line 225
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lu/d;->p:Ljava/util/Map;

    .line 236
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lu/d;->q:Ljava/util/Map;

    .line 247
    iput v3, p0, Lu/d;->r:I

    .line 263
    new-instance v0, Lu/f;

    invoke-direct {v0, p0, v2}, Lu/f;-><init>(Lu/d;Lu/e;)V

    iput-object v0, p0, Lu/d;->u:Ls/e;

    .line 266
    new-instance v0, Lu/g;

    invoke-direct {v0, p0, v2}, Lu/g;-><init>(Lu/d;Lu/e;)V

    iput-object v0, p0, Lu/d;->v:Ls/e;

    .line 269
    new-instance v0, Lu/h;

    invoke-direct {v0, p0, v2}, Lu/h;-><init>(Lu/d;Lu/e;)V

    iput-object v0, p0, Lu/d;->w:Lu/h;

    .line 276
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    iput-object v0, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    .line 313
    iput-object p2, p0, Lu/d;->e:LB/e;

    .line 314
    iput-object v2, p0, Lu/d;->c:LB/a;

    .line 315
    iput-object p4, p0, Lu/d;->f:LR/a;

    .line 316
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lu/d;->g:Ljava/util/List;

    .line 317
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lu/d;->o:Ljava/util/LinkedList;

    .line 319
    invoke-static {p1}, Lr/C;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_82

    .line 320
    invoke-static {p1}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    iput-object v0, p0, Lu/d;->d:Lr/z;

    .line 321
    new-instance v0, Lu/e;

    invoke-direct {v0, p0}, Lu/e;-><init>(Lu/d;)V

    iput-object v0, p0, Lu/d;->l:Lr/A;

    .line 348
    iget-object v0, p0, Lu/d;->d:Lr/z;

    iget-object v1, p0, Lu/d;->l:Lr/A;

    invoke-interface {v0, v1}, Lr/z;->a(Lr/A;)V

    .line 354
    :goto_7f
    iput-object p3, p0, Lu/d;->s:Lu/a;

    .line 355
    return-void

    .line 350
    :cond_82
    iput-object v2, p0, Lu/d;->d:Lr/z;

    .line 351
    iput-object v2, p0, Lu/d;->l:Lr/A;

    goto :goto_7f
.end method

.method public constructor <init>(LA/c;Lu/a;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 296
    new-instance v0, LB/e;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, LB/e;-><init>(LA/c;Ljava/util/Set;)V

    sget-object v1, LR/a;->a:LR/a;

    invoke-direct {p0, p1, v0, p2, v1}, Lu/d;-><init>(LA/c;LB/e;Lu/a;LR/a;)V

    .line 298
    return-void
.end method

.method private a(Lo/aq;ILo/ap;)LF/T;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 793
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lu/d;->a(Lo/aq;ILo/ap;Z)LF/T;

    move-result-object v0

    return-object v0
.end method

.method private a(Lo/aq;ILo/ap;Z)LF/T;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 808
    iget-object v2, p0, Lu/d;->i:LR/h;

    monitor-enter v2

    .line 811
    :try_start_4
    iget-object v1, p0, Lu/d;->i:LR/h;

    invoke-virtual {v1, p1, p1}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 812
    monitor-exit v2
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_18

    .line 817
    iget-object v1, p0, Lu/d;->h:LA/b;

    invoke-virtual {p1}, Lo/aq;->k()Lo/aB;

    move-result-object v2

    invoke-interface {v1, v2}, LA/b;->a(Lo/aB;)Z

    move-result v1

    if-nez v1, :cond_1b

    .line 818
    const/4 v1, 0x0

    .line 860
    :cond_17
    :goto_17
    return-object v1

    .line 812
    :catchall_18
    move-exception v1

    :try_start_19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_18

    throw v1

    .line 823
    :cond_1b
    const/4 v4, 0x0

    .line 824
    iget-object v1, p0, Lu/d;->g:Ljava/util/List;

    if-eqz v1, :cond_6c

    iget-object v1, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6c

    invoke-virtual {p1}, Lo/aq;->k()Lo/aB;

    move-result-object v1

    if-eqz v1, :cond_6c

    instance-of v1, p3, Lo/aL;

    if-eqz v1, :cond_6c

    .line 837
    iget-object v6, p0, Lu/d;->g:Ljava/util/List;

    monitor-enter v6

    .line 838
    :try_start_35
    iget-object v1, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v2, p3

    :goto_3c
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lr/D;

    .line 839
    const/4 v3, 0x1

    invoke-virtual {v1, p1, v3}, Lr/D;->a(Lo/aq;Z)Lo/ap;

    move-result-object v3

    .line 843
    if-eqz v3, :cond_5e

    .line 845
    move-object v0, v2

    check-cast v0, Lo/aL;

    move-object v1, v0

    move-object v0, v3

    check-cast v0, Lo/aL;

    move-object v2, v0

    invoke-static {v1, v2}, Lo/P;->a(Lo/aL;Lo/aL;)Lo/aL;

    move-result-object v2

    move v1, v4

    :goto_5c
    move v4, v1

    .line 852
    goto :goto_3c

    .line 848
    :cond_5e
    move-object v0, p3

    check-cast v0, Lo/aL;

    move-object v3, v0

    invoke-virtual {v1, p1, v3}, Lr/D;->a(Lo/aq;Lo/aL;)Z

    move-result v1

    if-eqz v1, :cond_7b

    move v1, v5

    .line 850
    goto :goto_5c

    .line 853
    :cond_6a
    monitor-exit v6
    :try_end_6b
    .catchall {:try_start_35 .. :try_end_6b} :catchall_78

    move-object p3, v2

    .line 856
    :cond_6c
    invoke-direct {p0, p1, p2, p3}, Lu/d;->b(Lo/aq;ILo/ap;)LF/T;

    move-result-object v1

    .line 857
    if-eqz v4, :cond_17

    .line 858
    const-wide/16 v2, 0xbb8

    invoke-interface {v1, v2, v3}, LF/T;->a(J)V

    goto :goto_17

    .line 853
    :catchall_78
    move-exception v1

    :try_start_79
    monitor-exit v6
    :try_end_7a
    .catchall {:try_start_79 .. :try_end_7a} :catchall_78

    throw v1

    :cond_7b
    move v1, v4

    goto :goto_5c
.end method

.method static synthetic a(Lu/d;Lo/aq;)LF/T;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lu/d;->c(Lo/aq;)LF/T;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lu/d;Lo/aq;ILo/ap;)LF/T;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 114
    invoke-direct {p0, p1, p2, p3}, Lu/d;->a(Lo/aq;ILo/ap;)LF/T;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lu/d;)Ljava/util/Set;
    .registers 2
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lu/d;->n:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic a(Lu/d;Lu/c;)Lu/c;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 114
    iput-object p1, p0, Lu/d;->t:Lu/c;

    return-object p1
.end method

.method private a(LB/e;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 642
    iget-object v0, p0, Lu/d;->c:LB/a;

    if-eqz v0, :cond_d

    .line 643
    if-eqz p2, :cond_e

    .line 644
    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v1, p0, Lu/d;->a:LD/a;

    invoke-virtual {v0, v1, p1}, LB/a;->b(LD/a;LB/e;)V

    .line 649
    :cond_d
    :goto_d
    return-void

    .line 646
    :cond_e
    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v1, p0, Lu/d;->a:LD/a;

    invoke-virtual {v0, v1, p1}, LB/a;->a(LD/a;LB/e;)V

    goto :goto_d
.end method

.method private a(Lo/aq;LF/T;J)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1006
    if-eqz p2, :cond_11

    .line 1007
    sget-object v0, Lu/d;->b:LF/T;

    if-eq p2, v0, :cond_12

    move-object v2, p2

    .line 1008
    :goto_7
    invoke-virtual {p0}, Lu/d;->j()Z

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lu/d;->a(Lo/aq;LF/T;ZJ)V

    .line 1010
    :cond_11
    return-void

    .line 1007
    :cond_12
    const/4 v2, 0x0

    goto :goto_7
.end method

.method private a(Lo/aq;LF/T;ZJ)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1241
    iget-object v7, p0, Lu/d;->o:Ljava/util/LinkedList;

    monitor-enter v7

    .line 1242
    :try_start_4
    iget-object v1, p0, Lu/d;->o:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1f

    .line 1243
    iget-object v0, p0, Lu/d;->o:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu/i;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lu/i;->a(Lo/aq;LF/T;ZJ)V

    .line 1255
    :cond_1d
    monitor-exit v7

    .line 1256
    return-void

    .line 1248
    :cond_1f
    new-instance v8, Ljava/util/ArrayList;

    iget-object v1, p0, Lu/d;->o:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1250
    iget-object v1, p0, Lu/d;->o:Ljava/util/LinkedList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move v6, v0

    .line 1251
    :goto_30
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_1d

    .line 1252
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu/i;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lu/i;->a(Lo/aq;LF/T;ZJ)V

    .line 1251
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_30

    .line 1255
    :catchall_47
    move-exception v0

    monitor-exit v7
    :try_end_49
    .catchall {:try_start_4 .. :try_end_49} :catchall_47

    throw v0
.end method

.method private a(Lo/aq;ZLs/e;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 454
    iget-object v1, p0, Lu/d;->i:LR/h;

    monitor-enter v1

    .line 456
    :try_start_3
    iget-object v0, p0, Lu/d;->i:LR/h;

    invoke-virtual {v0, p1}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_18

    .line 459
    iget-object v0, p0, Lu/d;->d:Lr/z;

    if-eqz v0, :cond_14

    .line 460
    if-eqz p2, :cond_1b

    .line 461
    iget-object v0, p0, Lu/d;->d:Lr/z;

    invoke-interface {v0, p1, p3}, Lr/z;->c(Lo/aq;Ls/e;)V

    .line 466
    :cond_14
    :goto_14
    invoke-direct {p0, p1}, Lu/d;->d(Lo/aq;)V

    .line 467
    return-void

    .line 457
    :catchall_18
    move-exception v0

    :try_start_19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_18

    throw v0

    .line 463
    :cond_1b
    iget-object v0, p0, Lu/d;->d:Lr/z;

    invoke-interface {v0, p1, p3}, Lr/z;->a(Lo/aq;Ls/e;)V

    goto :goto_14
.end method

.method static synthetic a(Lu/d;Lo/aq;LF/T;J)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 114
    invoke-direct {p0, p1, p2, p3, p4}, Lu/d;->a(Lo/aq;LF/T;J)V

    return-void
.end method

.method static synthetic a(Lu/d;Lo/aq;ZLs/e;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 114
    invoke-direct {p0, p1, p2, p3}, Lu/d;->a(Lo/aq;ZLs/e;)V

    return-void
.end method

.method private a(Lo/aq;LF/T;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 428
    if-eqz p2, :cond_b

    iget-object v1, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    invoke-interface {p2, v1}, LF/T;->a(Lcom/google/googlenav/common/a;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 429
    :cond_b
    iget-object v1, p0, Lu/d;->p:Ljava/util/Map;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_20

    .line 431
    iget-object v1, p0, Lu/d;->u:Ls/e;

    invoke-direct {p0, p1, v0, v1}, Lu/d;->a(Lo/aq;ZLs/e;)V

    .line 433
    :cond_20
    const/4 v0, 0x1

    .line 436
    :goto_21
    return v0

    .line 435
    :cond_22
    invoke-direct {p0, p1, p2}, Lu/d;->b(Lo/aq;LF/T;)V

    goto :goto_21
.end method

.method static synthetic a(Lu/d;Lo/aq;LF/T;)Z
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Lu/d;->a(Lo/aq;LF/T;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lu/d;)LB/e;
    .registers 2
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lu/d;->e:LB/e;

    return-object v0
.end method

.method private b(Lo/aq;ILo/ap;)LF/T;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 869
    .line 874
    iget-object v2, p0, Lu/d;->a:LD/a;

    .line 875
    if-eqz v2, :cond_2f

    .line 876
    if-nez p2, :cond_2f

    .line 877
    instance-of v0, p3, Lo/aL;

    if-eqz v0, :cond_24

    .line 878
    iget-object v0, p0, Lu/d;->f:LR/a;

    invoke-static {p3, v0, v2}, LF/Y;->a(Lo/ap;LR/a;LD/a;)LF/Y;

    move-result-object v0

    .line 889
    :goto_11
    if-nez v0, :cond_18

    .line 890
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2d

    .line 891
    sget-object v0, Lu/d;->b:LF/T;

    .line 899
    :cond_18
    iget-object v1, p0, Lu/d;->c:LB/a;

    if-eqz v1, :cond_23

    .line 900
    iget-object v1, p0, Lu/d;->c:LB/a;

    iget-object v3, p0, Lu/d;->e:LB/e;

    invoke-virtual {v1, v2, v3, p1, v0}, LB/a;->a(LD/a;LB/e;Lo/aq;LF/T;)V

    .line 902
    :cond_23
    :goto_23
    return-object v0

    .line 879
    :cond_24
    instance-of v0, p3, Lo/x;

    if-eqz v0, :cond_2f

    .line 880
    invoke-static {p3, v2}, LF/k;->a(Lo/ap;LD/a;)LF/k;

    move-result-object v0

    goto :goto_11

    :cond_2d
    move-object v0, v1

    .line 893
    goto :goto_23

    :cond_2f
    move-object v0, v1

    goto :goto_11
.end method

.method static synthetic b(Lu/d;Lo/aq;ILo/ap;)LF/T;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 114
    invoke-direct {p0, p1, p2, p3}, Lu/d;->c(Lo/aq;ILo/ap;)LF/T;

    move-result-object v0

    return-object v0
.end method

.method private b(Lo/aq;LF/T;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 447
    return-void
.end method

.method private c(Lo/aq;)LF/T;
    .registers 6
    .parameter

    .prologue
    .line 393
    const/4 v0, 0x1

    .line 394
    iget-object v1, p0, Lu/d;->c:LB/a;

    iget-object v2, p0, Lu/d;->a:LD/a;

    iget-object v3, p0, Lu/d;->e:LB/e;

    invoke-virtual {v1, v2, v3, p1, v0}, LB/a;->a(LD/a;LB/e;Lo/aq;Z)LF/T;

    move-result-object v0

    .line 398
    sget-object v1, Lu/d;->b:LF/T;

    if-eq v0, v1, :cond_1a

    if-eqz v0, :cond_1a

    iget-object v1, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    invoke-interface {v0, v1}, LF/T;->b(Lcom/google/googlenav/common/a;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 399
    const/4 v0, 0x0

    .line 401
    :cond_1a
    return-object v0
.end method

.method private c(Lo/aq;ILo/ap;)LF/T;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 913
    if-nez p3, :cond_5

    move-object v0, v1

    .line 975
    :goto_4
    return-object v0

    :cond_5
    move-object v0, p3

    .line 918
    check-cast v0, Lo/aL;

    invoke-virtual {v0}, Lo/aL;->r()I

    move-result v0

    if-nez v0, :cond_3b

    move-object v0, p3

    .line 919
    check-cast v0, Lo/aL;

    invoke-virtual {v0}, Lo/aL;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gez v0, :cond_1d

    move-object v0, v1

    .line 921
    goto :goto_4

    .line 926
    :cond_1d
    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v2, p0, Lu/d;->a:LD/a;

    iget-object v3, p0, Lu/d;->e:LB/e;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, p1, v4}, LB/a;->a(LD/a;LB/e;Lo/aq;Z)LF/T;

    move-result-object v0

    .line 931
    if-eqz v0, :cond_3b

    sget-object v2, Lu/d;->b:LF/T;

    if-eq v0, v2, :cond_3b

    .line 932
    check-cast v0, LF/Y;

    check-cast p3, Lo/aL;

    invoke-virtual {p3}, Lo/aL;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LF/Y;->b(J)V

    move-object v0, v1

    .line 933
    goto :goto_4

    .line 937
    :cond_3b
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 939
    iget-object v2, p0, Lu/d;->i:LR/h;

    monitor-enter v2

    .line 940
    :try_start_44
    iget v0, p0, Lu/d;->k:I

    iget v3, p0, Lu/d;->j:I

    add-int/2addr v0, v3

    and-int/lit8 v0, v0, 0x7f

    const/16 v3, 0x7f

    if-ne v0, v3, :cond_4f

    .line 948
    :cond_4f
    monitor-exit v2
    :try_end_50
    .catchall {:try_start_44 .. :try_end_50} :catchall_64

    .line 951
    :cond_50
    iget-object v2, p0, Lu/d;->i:LR/h;

    monitor-enter v2

    .line 952
    :try_start_53
    iget-object v0, p0, Lu/d;->i:LR/h;

    invoke-virtual {v0, p1}, LR/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_67

    .line 955
    iget v0, p0, Lu/d;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lu/d;->k:I

    .line 956
    monitor-exit v2
    :try_end_62
    .catchall {:try_start_53 .. :try_end_62} :catchall_7c

    move-object v0, v1

    goto :goto_4

    .line 948
    :catchall_64
    move-exception v0

    :try_start_65
    monitor-exit v2
    :try_end_66
    .catchall {:try_start_65 .. :try_end_66} :catchall_64

    throw v0

    .line 958
    :cond_67
    :try_start_67
    iget v0, p0, Lu/d;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lu/d;->j:I

    .line 959
    monitor-exit v2
    :try_end_6e
    .catchall {:try_start_67 .. :try_end_6e} :catchall_7c

    .line 964
    const/4 v0, 0x1

    .line 965
    iget-object v2, p0, Lu/d;->d:Lr/z;

    invoke-interface {v2, p1, v0}, Lr/z;->a(Lo/aq;Z)Lo/ap;

    move-result-object v0

    .line 967
    if-eqz v0, :cond_7f

    .line 968
    invoke-direct {p0, p1, p2, v0}, Lu/d;->a(Lo/aq;ILo/ap;)LF/T;

    move-result-object v0

    goto :goto_4

    .line 959
    :catchall_7c
    move-exception v0

    :try_start_7d
    monitor-exit v2
    :try_end_7e
    .catchall {:try_start_7d .. :try_end_7e} :catchall_7c

    throw v0

    :cond_7f
    move-object v0, v1

    .line 975
    goto :goto_4
.end method

.method static synthetic c(Lu/d;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lu/d;->p:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d(Lu/d;)Lcom/google/googlenav/common/a;
    .registers 2
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    return-object v0
.end method

.method private d(Lo/aq;)V
    .registers 7
    .parameter

    .prologue
    .line 475
    iget-object v0, p0, Lu/d;->g:Ljava/util/List;

    if-eqz v0, :cond_3e

    sget-object v0, Lr/I;->h:Lo/aq;

    if-eq p1, v0, :cond_3e

    .line 477
    iget-object v2, p0, Lu/d;->g:Ljava/util/List;

    monitor-enter v2

    .line 482
    const/4 v0, 0x0

    .line 484
    :try_start_c
    iget-object v1, p0, Lu/d;->d:Lr/z;

    if-eqz v1, :cond_3f

    iget-object v1, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3f

    .line 485
    iget-object v0, p0, Lu/d;->d:Lr/z;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lr/z;->a(Lo/aq;Z)Lo/ap;

    move-result-object v0

    check-cast v0, Lo/aL;

    move-object v1, v0

    .line 487
    :goto_22
    iget-object v0, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_28
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/D;

    .line 488
    iget-object v4, p0, Lu/d;->w:Lu/h;

    invoke-virtual {v0, p1, v1, v4}, Lr/D;->a(Lo/aq;Lo/aL;Ls/e;)V

    goto :goto_28

    .line 490
    :catchall_3a
    move-exception v0

    monitor-exit v2
    :try_end_3c
    .catchall {:try_start_c .. :try_end_3c} :catchall_3a

    throw v0

    :cond_3d
    :try_start_3d
    monitor-exit v2
    :try_end_3e
    .catchall {:try_start_3d .. :try_end_3e} :catchall_3a

    .line 492
    :cond_3e
    return-void

    :cond_3f
    move-object v1, v0

    goto :goto_22
.end method

.method static synthetic e(Lu/d;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lu/d;->q:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lu/d;)I
    .registers 3
    .parameter

    .prologue
    .line 114
    iget v0, p0, Lu/d;->r:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lu/d;->r:I

    return v0
.end method

.method static synthetic g(Lu/d;)Lu/a;
    .registers 2
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lu/d;->s:Lu/a;

    return-object v0
.end method

.method static synthetic h(Lu/d;)I
    .registers 3
    .parameter

    .prologue
    .line 114
    iget v0, p0, Lu/d;->r:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lu/d;->r:I

    return v0
.end method

.method static synthetic i(Lu/d;)Ls/e;
    .registers 2
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lu/d;->v:Ls/e;

    return-object v0
.end method

.method static synthetic j(Lu/d;)Lu/c;
    .registers 2
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lu/d;->t:Lu/c;

    return-object v0
.end method

.method static synthetic l()LF/T;
    .registers 1

    .prologue
    .line 114
    sget-object v0, Lu/d;->b:LF/T;

    return-object v0
.end method

.method private final m()V
    .registers 5

    .prologue
    .line 1280
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    iget-wide v2, p0, Lu/d;->x:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1c

    .line 1282
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation must be called from renderer thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1284
    :cond_1c
    return-void
.end method


# virtual methods
.method public a()LA/c;
    .registers 2

    .prologue
    .line 495
    iget-object v0, p0, Lu/d;->d:Lr/z;

    invoke-interface {v0}, Lr/z;->a()LA/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Lo/aq;)LF/T;
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 364
    const/4 v2, 0x0

    .line 366
    iget-object v1, p0, Lu/d;->c:LB/a;

    iget-object v3, p0, Lu/d;->a:LD/a;

    iget-object v4, p0, Lu/d;->e:LB/e;

    invoke-virtual {v1, v3, v4, p1, v2}, LB/a;->a(LD/a;LB/e;Lo/aq;Z)LF/T;

    move-result-object v1

    .line 367
    sget-object v3, Lu/d;->b:LF/T;

    if-ne v1, v3, :cond_11

    .line 381
    :cond_10
    :goto_10
    return-object v0

    .line 369
    :cond_11
    if-eqz v1, :cond_1b

    iget-object v3, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    invoke-interface {v1, v3}, LF/T;->b(Lcom/google/googlenav/common/a;)Z

    move-result v3

    if-eqz v3, :cond_3f

    .line 371
    :cond_1b
    iget-object v1, p0, Lu/d;->c:LB/a;

    iget-object v3, p0, Lu/d;->a:LD/a;

    iget-object v4, p0, Lu/d;->e:LB/e;

    invoke-virtual {p1}, Lo/aq;->a()Lo/aq;

    move-result-object v5

    invoke-virtual {v1, v3, v4, v5, v2}, LB/a;->a(LD/a;LB/e;Lo/aq;Z)LF/T;

    move-result-object v1

    .line 372
    sget-object v2, Lu/d;->b:LF/T;

    if-eq v1, v2, :cond_10

    .line 374
    if-eqz v1, :cond_37

    iget-object v2, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    invoke-interface {v1, v2}, LF/T;->b(Lcom/google/googlenav/common/a;)Z

    move-result v2

    if-eqz v2, :cond_3d

    .line 375
    :cond_37
    iget-object v1, p0, Lu/d;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    goto :goto_10

    :cond_3d
    move-object v0, v1

    .line 378
    goto :goto_10

    :cond_3f
    move-object v0, v1

    .line 381
    goto :goto_10
.end method

.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 1287
    iput-wide p1, p0, Lu/d;->x:J

    .line 1288
    iget-object v0, p0, Lu/d;->c:LB/a;

    if-eqz v0, :cond_b

    .line 1289
    iget-object v0, p0, Lu/d;->c:LB/a;

    invoke-virtual {v0, p1, p2}, LB/a;->a(J)V

    .line 1291
    :cond_b
    return-void
.end method

.method public a(LA/b;)V
    .registers 2
    .parameter

    .prologue
    .line 1300
    iput-object p1, p0, Lu/d;->h:LA/b;

    .line 1301
    return-void
.end method

.method public a(LA/c;)V
    .registers 6
    .parameter

    .prologue
    .line 717
    iget-object v0, p0, Lu/d;->d:Lr/z;

    instance-of v0, v0, Lr/I;

    if-nez v0, :cond_2b

    .line 718
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Modifiers not supported on store \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lu/d;->d:Lr/z;

    invoke-interface {v2}, Lr/z;->a()LA/c;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 721
    :cond_2b
    iget-boolean v0, p1, LA/c;->x:Z

    if-nez v0, :cond_48

    .line 722
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Only modifiers may be added, not "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 724
    :cond_48
    invoke-static {p1}, Lr/C;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_bb

    .line 725
    invoke-static {p1}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    .line 726
    instance-of v1, v0, Lr/D;

    if-nez v1, :cond_79

    .line 727
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Modifier store \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lr/z;->a()LA/c;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' must be a vector modifier store"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 730
    :cond_79
    iget-object v1, p0, Lu/d;->g:Ljava/util/List;

    monitor-enter v1

    .line 731
    :try_start_7c
    iget-object v2, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_86

    .line 732
    monitor-exit v1

    .line 755
    :goto_85
    return-void

    .line 734
    :cond_86
    iget-object v2, p0, Lu/d;->l:Lr/A;

    invoke-interface {v0, v2}, Lr/z;->a(Lr/A;)V

    .line 735
    iget-object v2, p0, Lu/d;->g:Ljava/util/List;

    check-cast v0, Lr/D;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 736
    monitor-exit v1
    :try_end_93
    .catchall {:try_start_7c .. :try_end_93} :catchall_b8

    .line 740
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 741
    iget-object v2, p0, Lu/d;->g:Ljava/util/List;

    monitor-enter v2

    .line 742
    :try_start_9b
    iget-object v0, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_a1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/D;

    .line 743
    invoke-virtual {v0}, Lr/D;->a()LA/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_a1

    .line 745
    :catchall_b5
    move-exception v0

    monitor-exit v2
    :try_end_b7
    .catchall {:try_start_9b .. :try_end_b7} :catchall_b5

    throw v0

    .line 736
    :catchall_b8
    move-exception v0

    :try_start_b9
    monitor-exit v1
    :try_end_ba
    .catchall {:try_start_b9 .. :try_end_ba} :catchall_b8

    throw v0

    .line 738
    :cond_bb
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown tile store "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 745
    :cond_d4
    :try_start_d4
    monitor-exit v2
    :try_end_d5
    .catchall {:try_start_d4 .. :try_end_d5} :catchall_b5

    .line 750
    iget-object v2, p0, Lu/d;->n:Ljava/util/Set;

    monitor-enter v2

    .line 751
    :try_start_d8
    iget-object v0, p0, Lu/d;->n:Ljava/util/Set;

    iget-object v3, p0, Lu/d;->e:LB/e;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 752
    monitor-exit v2
    :try_end_e0
    .catchall {:try_start_d8 .. :try_end_e0} :catchall_f0

    .line 753
    new-instance v0, LB/e;

    iget-object v2, p0, Lu/d;->d:Lr/z;

    invoke-interface {v2}, Lr/z;->a()LA/c;

    move-result-object v2

    iget-object v3, p0, Lu/d;->f:LR/a;

    invoke-direct {v0, v2, v1, v3}, LB/e;-><init>(LA/c;Ljava/util/Set;LR/a;)V

    iput-object v0, p0, Lu/d;->e:LB/e;

    goto :goto_85

    .line 752
    :catchall_f0
    move-exception v0

    :try_start_f1
    monitor-exit v2
    :try_end_f2
    .catchall {:try_start_f1 .. :try_end_f2} :catchall_f0

    throw v0
.end method

.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 614
    const-string v0, "GLState should not be null"

    invoke-static {p1, v0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 615
    iput-object p1, p0, Lu/d;->a:LD/a;

    .line 616
    new-instance v0, Lal/a;

    invoke-direct {v0}, Lal/a;-><init>()V

    invoke-static {v0}, LB/a;->a(Lcom/google/googlenav/common/a;)V

    .line 617
    invoke-static {}, LB/a;->a()LB/a;

    move-result-object v0

    iput-object v0, p0, Lu/d;->c:LB/a;

    .line 618
    return-void
.end method

.method public a(Lg/a;Lo/T;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 546
    invoke-direct {p0}, Lu/d;->m()V

    .line 547
    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v1, p0, Lu/d;->a:LD/a;

    iget-object v2, p0, Lu/d;->e:LB/e;

    invoke-virtual {v0, v1, v2}, LB/a;->e(LD/a;LB/e;)V

    .line 549
    iget-object v7, p0, Lu/d;->s:Lu/a;

    monitor-enter v7

    .line 553
    :try_start_f
    iget-object v0, p0, Lu/d;->s:Lu/a;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lu/a;->a(Lg/a;Lo/T;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Z)V

    .line 559
    iget-object v0, p0, Lu/d;->s:Lu/a;

    invoke-virtual {v0}, Lu/a;->b()Lu/c;

    move-result-object v0

    .line 561
    iget-object v1, p0, Lu/d;->t:Lu/c;

    if-nez v1, :cond_2d

    .line 563
    iget-object v1, v0, Lu/c;->a:Lo/aq;

    iget-boolean v2, v0, Lu/c;->b:Z

    iget-object v3, p0, Lu/d;->v:Ls/e;

    invoke-direct {p0, v1, v2, v3}, Lu/d;->a(Lo/aq;ZLs/e;)V

    .line 565
    :cond_2d
    iput-object v0, p0, Lu/d;->t:Lu/c;

    .line 566
    monitor-exit v7

    .line 567
    return-void

    .line 566
    :catchall_31
    move-exception v0

    monitor-exit v7
    :try_end_33
    .catchall {:try_start_f .. :try_end_33} :catchall_31

    throw v0
.end method

.method public a(Ljava/util/List;)V
    .registers 6
    .parameter

    .prologue
    .line 584
    invoke-direct {p0}, Lu/d;->m()V

    .line 585
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->c(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 586
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_f
    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    .line 587
    sget-object v3, Lu/d;->b:LF/T;

    if-eq v0, v3, :cond_f

    .line 588
    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 591
    :cond_27
    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v2, p0, Lu/d;->a:LD/a;

    iget-object v3, p0, Lu/d;->e:LB/e;

    invoke-virtual {v0, v2, v3, v1}, LB/a;->a(LD/a;LB/e;Ljava/util/List;)V

    .line 592
    return-void
.end method

.method public a(Lu/i;)V
    .registers 4
    .parameter

    .prologue
    .line 1217
    iget-object v1, p0, Lu/d;->o:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1218
    :try_start_3
    iget-object v0, p0, Lu/d;->o:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1219
    iget-object v0, p0, Lu/d;->o:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1220
    monitor-exit v1

    .line 1221
    return-void

    .line 1220
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 630
    iget-object v0, p0, Lu/d;->e:LB/e;

    invoke-direct {p0, v0, p1}, Lu/d;->a(LB/e;Z)V

    .line 631
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 503
    iget-object v0, p0, Lu/d;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public b(Lo/aq;)LF/T;
    .registers 4
    .parameter

    .prologue
    .line 412
    invoke-direct {p0, p1}, Lu/d;->c(Lo/aq;)LF/T;

    move-result-object v0

    .line 413
    sget-object v1, Lu/d;->b:LF/T;

    if-ne v0, v1, :cond_a

    .line 414
    const/4 v0, 0x0

    .line 419
    :cond_9
    :goto_9
    return-object v0

    .line 416
    :cond_a
    invoke-direct {p0, p1, v0}, Lu/d;->a(Lo/aq;LF/T;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 417
    iget-object v1, p0, Lu/d;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    goto :goto_9
.end method

.method public b(LA/c;)V
    .registers 6
    .parameter

    .prologue
    .line 763
    invoke-static {p1}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    .line 764
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 765
    iget-object v2, p0, Lu/d;->g:Ljava/util/List;

    monitor-enter v2

    .line 766
    :try_start_c
    iget-object v3, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    .line 768
    monitor-exit v2

    .line 784
    :goto_15
    return-void

    .line 770
    :cond_16
    iget-object v3, p0, Lu/d;->l:Lr/A;

    invoke-interface {v0, v3}, Lr/z;->b(Lr/A;)V

    .line 771
    iget-object v0, p0, Lu/d;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_21
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/D;

    .line 772
    invoke-virtual {v0}, Lr/I;->a()LA/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_21

    .line 774
    :catchall_35
    move-exception v0

    monitor-exit v2
    :try_end_37
    .catchall {:try_start_c .. :try_end_37} :catchall_35

    throw v0

    :cond_38
    :try_start_38
    monitor-exit v2
    :try_end_39
    .catchall {:try_start_38 .. :try_end_39} :catchall_35

    .line 779
    iget-object v2, p0, Lu/d;->n:Ljava/util/Set;

    monitor-enter v2

    .line 780
    :try_start_3c
    iget-object v0, p0, Lu/d;->n:Ljava/util/Set;

    iget-object v3, p0, Lu/d;->e:LB/e;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 781
    monitor-exit v2
    :try_end_44
    .catchall {:try_start_3c .. :try_end_44} :catchall_54

    .line 782
    new-instance v0, LB/e;

    iget-object v2, p0, Lu/d;->d:Lr/z;

    invoke-interface {v2}, Lr/z;->a()LA/c;

    move-result-object v2

    iget-object v3, p0, Lu/d;->f:LR/a;

    invoke-direct {v0, v2, v1, v3}, LB/e;-><init>(LA/c;Ljava/util/Set;LR/a;)V

    iput-object v0, p0, Lu/d;->e:LB/e;

    goto :goto_15

    .line 781
    :catchall_54
    move-exception v0

    :try_start_55
    monitor-exit v2
    :try_end_56
    .catchall {:try_start_55 .. :try_end_56} :catchall_54

    throw v0
.end method

.method public b(Ljava/util/List;)V
    .registers 6
    .parameter

    .prologue
    .line 599
    invoke-direct {p0}, Lu/d;->m()V

    .line 600
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->c(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 601
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_f
    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    .line 602
    sget-object v3, Lu/d;->b:LF/T;

    if-eq v0, v3, :cond_f

    .line 603
    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 606
    :cond_27
    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v2, p0, Lu/d;->a:LD/a;

    iget-object v3, p0, Lu/d;->e:LB/e;

    invoke-virtual {v0, v2, v3, v1}, LB/a;->b(LD/a;LB/e;Ljava/util/List;)V

    .line 607
    return-void
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 656
    iget-object v0, p0, Lu/d;->c:LB/a;

    if-eqz v0, :cond_9

    .line 657
    iget-object v0, p0, Lu/d;->c:LB/a;

    invoke-virtual {v0, p1}, LB/a;->a(Z)V

    .line 659
    :cond_9
    return-void
.end method

.method public c()V
    .registers 4

    .prologue
    .line 513
    invoke-direct {p0}, Lu/d;->m()V

    .line 514
    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v1, p0, Lu/d;->a:LD/a;

    iget-object v2, p0, Lu/d;->e:LB/e;

    invoke-virtual {v0, v1, v2}, LB/a;->e(LD/a;LB/e;)V

    .line 515
    return-void
.end method

.method public d()V
    .registers 4

    .prologue
    .line 522
    invoke-direct {p0}, Lu/d;->m()V

    .line 523
    sget-boolean v0, Lcom/google/googlenav/android/E;->a:Z

    if-eqz v0, :cond_10

    .line 526
    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v1, p0, Lu/d;->a:LD/a;

    iget-object v2, p0, Lu/d;->e:LB/e;

    invoke-virtual {v0, v1, v2}, LB/a;->d(LD/a;LB/e;)V

    .line 528
    :cond_10
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    .line 574
    invoke-direct {p0}, Lu/d;->m()V

    .line 575
    iget-object v0, p0, Lu/d;->s:Lu/a;

    invoke-virtual {v0}, Lu/a;->a()V

    .line 576
    return-void
.end method

.method public f()LB/e;
    .registers 2

    .prologue
    .line 621
    iget-object v0, p0, Lu/d;->e:LB/e;

    return-object v0
.end method

.method public g()V
    .registers 2

    .prologue
    .line 666
    invoke-direct {p0}, Lu/d;->m()V

    .line 667
    iget-object v0, p0, Lu/d;->c:LB/a;

    if-eqz v0, :cond_c

    .line 668
    iget-object v0, p0, Lu/d;->c:LB/a;

    invoke-virtual {v0}, LB/a;->b()V

    .line 670
    :cond_c
    return-void
.end method

.method public h()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 680
    iget-object v1, p0, Lu/d;->n:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2d

    .line 683
    iget-object v1, p0, Lu/d;->n:Ljava/util/Set;

    monitor-enter v1

    .line 685
    const/4 v2, 0x0

    .line 686
    :try_start_d
    iget-object v0, p0, Lu/d;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_13
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LB/e;

    .line 687
    invoke-direct {p0, v0, v2}, Lu/d;->a(LB/e;Z)V

    goto :goto_13

    .line 690
    :catchall_23
    move-exception v0

    monitor-exit v1
    :try_end_25
    .catchall {:try_start_d .. :try_end_25} :catchall_23

    throw v0

    .line 689
    :cond_26
    :try_start_26
    iget-object v0, p0, Lu/d;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 690
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_26 .. :try_end_2c} :catchall_23

    .line 691
    const/4 v0, 0x1

    .line 693
    :cond_2d
    return v0
.end method

.method public i()V
    .registers 4

    .prologue
    .line 703
    iget-object v0, p0, Lu/d;->c:LB/a;

    if-eqz v0, :cond_10

    .line 704
    iget-object v0, p0, Lu/d;->c:LB/a;

    iget-object v1, p0, Lu/d;->a:LD/a;

    iget-object v2, p0, Lu/d;->e:LB/e;

    invoke-virtual {v0, v1, v2}, LB/a;->c(LD/a;LB/e;)V

    .line 705
    invoke-virtual {p0}, Lu/d;->h()Z

    .line 707
    :cond_10
    return-void
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 1260
    iget-object v0, p0, Lu/d;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_18

    iget v0, p0, Lu/d;->r:I

    if-nez v0, :cond_18

    iget-object v0, p0, Lu/d;->s:Lu/a;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lu/d;->s:Lu/a;

    invoke-virtual {v0}, Lu/a;->c()Z

    move-result v0

    if-nez v0, :cond_1a

    :cond_18
    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public k()Lcom/google/googlenav/common/a;
    .registers 2

    .prologue
    .line 1272
    iget-object v0, p0, Lu/d;->y:Lcom/google/googlenav/common/a;

    return-object v0
.end method
