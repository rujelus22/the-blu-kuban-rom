.class public LQ/k;
.super LQ/s;
.source "SourceFile"


# instance fields
.field private c:Ljava/lang/Runnable;

.field private d:[LO/z;

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 18
    invoke-direct {p0}, LQ/s;-><init>()V

    return-void
.end method

.method static synthetic a(LQ/k;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 18
    iput-object p1, p0, LQ/k;->c:Ljava/lang/Runnable;

    return-object p1
.end method

.method private a(Ljava/lang/String;)V
    .registers 7
    .parameter

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    const-string v1, "|A="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v1

    if-eqz v1, :cond_51

    .line 106
    iget-object v1, p0, LQ/k;->d:[LO/z;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, LO/z;->o()I

    move-result v1

    div-int/lit8 v1, v1, 0x3c

    .line 107
    iget-object v2, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v2}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/aQ;->g()I

    move-result v2

    div-int/lit8 v2, v2, 0x3c

    .line 108
    iget-object v3, p0, LQ/k;->d:[LO/z;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v3}, LO/z;->o()I

    move-result v3

    div-int/lit8 v3, v3, 0x3c

    .line 110
    const-string v4, "|t="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 111
    const-string v1, "|o="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 112
    const-string v1, "|a="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 115
    :cond_51
    const-string v1, "Q"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method private ap()Z
    .registers 3

    .prologue
    .line 79
    iget-object v0, p0, LQ/k;->d:[LO/z;

    if-eqz v0, :cond_c

    iget-object v0, p0, LQ/k;->d:[LO/z;

    array-length v0, v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private aq()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 83
    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 84
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/k;->d:[LO/z;

    aget-object v1, v1, v3

    iget-object v2, p0, LQ/k;->d:[LO/z;

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;[LO/z;)V

    .line 85
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/k;->d:[LO/z;

    aget-object v1, v1, v3

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;)V

    .line 86
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/k;->d:[LO/z;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->b(LO/z;)V

    .line 88
    :cond_31
    return-void
.end method

.method private c(LO/z;[LO/z;)Z
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x2

    const/4 v0, 0x0

    .line 44
    if-eqz p1, :cond_12

    if-eqz p2, :cond_12

    array-length v2, p2

    if-lt v2, v8, :cond_12

    aget-object v2, p2, v0

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 75
    :cond_12
    :goto_12
    return v0

    .line 50
    :cond_13
    aget-object v2, p2, v1

    .line 51
    iget-object v3, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v3}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/aQ;->g()I

    move-result v3

    invoke-virtual {v2}, LO/z;->o()I

    move-result v4

    sub-int/2addr v3, v4

    .line 53
    if-ltz v3, :cond_12

    .line 58
    iget-object v4, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->b()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2}, LO/z;->p()I

    move-result v5

    invoke-virtual {v2, v4, v0, v5}, LO/z;->a(Landroid/content/Context;II)LO/E;

    move-result-object v2

    .line 62
    invoke-static {}, Lcom/google/android/maps/driveabout/app/dx;->a()Lcom/google/android/maps/driveabout/app/dx;

    move-result-object v4

    invoke-virtual {v4, v3, v0}, Lcom/google/android/maps/driveabout/app/dx;->a(IZ)Landroid/text/Spannable;

    move-result-object v4

    .line 64
    invoke-static {}, Lcom/google/android/maps/driveabout/app/dx;->a()Lcom/google/android/maps/driveabout/app/dx;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/google/android/maps/driveabout/app/dx;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 67
    iget-object v5, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v5}, LQ/p;->b()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0d008f

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v4, v7, v0

    invoke-virtual {v2}, LO/E;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v1

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, LQ/k;->f:Ljava/lang/String;

    .line 69
    iget-object v4, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v4}, LQ/p;->b()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0d0090

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v3, v6, v0

    invoke-virtual {v2}, LO/E;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LQ/k;->g:Ljava/lang/String;

    .line 73
    new-array v2, v8, [LO/z;

    iput-object v2, p0, LQ/k;->d:[LO/z;

    .line 74
    iget-object v2, p0, LQ/k;->d:[LO/z;

    invoke-static {p2, v0, v2, v0, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v0, v1

    .line 75
    goto :goto_12
.end method


# virtual methods
.method protected A()V
    .registers 2

    .prologue
    .line 264
    const-string v0, "t"

    invoke-direct {p0, v0}, LQ/k;->a(Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    .line 266
    return-void
.end method

.method protected B()V
    .registers 3

    .prologue
    .line 275
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->h(Z)V

    .line 278
    return-void
.end method

.method public a()V
    .registers 5

    .prologue
    .line 122
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    iget-object v1, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->m()[LO/z;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LQ/k;->c(LO/z;[LO/z;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 125
    new-instance v0, LQ/l;

    invoke-direct {v0, p0}, LQ/l;-><init>(LQ/k;)V

    iput-object v0, p0, LQ/k;->c:Ljava/lang/Runnable;

    .line 134
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    iget-object v1, p0, LQ/k;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/aQ;->a(Ljava/lang/Runnable;J)V

    .line 136
    const-string v0, "s"

    invoke-direct {p0, v0}, LQ/k;->a(Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LO/t;->a(Z)V

    .line 143
    invoke-virtual {p0}, LQ/k;->ab()Z

    move-result v0

    if-nez v0, :cond_52

    .line 144
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    iget-object v1, p0, LQ/k;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/a;->b(Ljava/lang/String;)V

    .line 148
    :cond_52
    invoke-super {p0}, LQ/s;->a()V

    .line 152
    :goto_55
    return-void

    .line 150
    :cond_56
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    goto :goto_55
.end method

.method public a(LO/z;[LO/z;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 213
    invoke-direct {p0, p1, p2}, LQ/k;->c(LO/z;[LO/z;)Z

    move-result v0

    if-eqz v0, :cond_19

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->z()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 215
    invoke-direct {p0}, LQ/k;->aq()V

    .line 216
    invoke-virtual {p0}, LQ/k;->u()V

    .line 220
    :goto_18
    return-void

    .line 218
    :cond_19
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    goto :goto_18
.end method

.method protected a(Lcom/google/android/maps/driveabout/vector/c;)V
    .registers 2
    .parameter

    .prologue
    .line 271
    return-void
.end method

.method protected a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 202
    iget-boolean v0, p0, LQ/k;->e:Z

    if-nez v0, :cond_6

    if-eqz p1, :cond_28

    :cond_6
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 204
    const/4 v0, 0x0

    iput-boolean v0, p0, LQ/k;->e:Z

    .line 205
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->c()Lcom/google/android/maps/driveabout/app/cS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v0

    iget-object v1, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cq;->b(Lcom/google/android/maps/driveabout/app/aQ;)V

    .line 208
    :cond_28
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 156
    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v0

    if-eqz v0, :cond_5b

    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->z()Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, LQ/k;->e:Z

    .line 160
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setViewMode(I)V

    .line 163
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->r()V

    .line 164
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->s()V

    .line 165
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->E()V

    .line 166
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->I()V

    .line 167
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->w()V

    .line 168
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->t()V

    .line 171
    invoke-direct {p0}, LQ/k;->aq()V

    .line 174
    invoke-virtual {p0}, LQ/k;->u()V

    .line 176
    :cond_5b
    return-void
.end method

.method public d()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 180
    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 181
    iget-object v0, p0, LQ/k;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_16

    .line 182
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    iget-object v1, p0, LQ/k;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/aQ;->b(Ljava/lang/Runnable;)V

    .line 184
    :cond_16
    iput-object v2, p0, LQ/k;->c:Ljava/lang/Runnable;

    .line 186
    iput-object v2, p0, LQ/k;->d:[LO/z;

    .line 187
    iput-object v2, p0, LQ/k;->f:Ljava/lang/String;

    .line 188
    iput-object v2, p0, LQ/k;->g:Ljava/lang/String;

    .line 190
    :cond_1e
    invoke-super {p0}, LQ/s;->d()V

    .line 191
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    .line 195
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->J()V

    .line 196
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->n()V

    .line 197
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->u()V

    .line 198
    return-void
.end method

.method protected u()V
    .registers 3

    .prologue
    .line 229
    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 230
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/k;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->b(Ljava/lang/String;)V

    .line 232
    :cond_11
    return-void
.end method

.method protected v()V
    .registers 3

    .prologue
    .line 241
    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 242
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTrafficMode(I)V

    .line 244
    :cond_10
    return-void
.end method

.method protected x()V
    .registers 3

    .prologue
    .line 236
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTopOverlayText(Ljava/lang/CharSequence;)V

    .line 237
    return-void
.end method

.method protected y()V
    .registers 4

    .prologue
    .line 248
    const-string v0, "a"

    invoke-direct {p0, v0}, LQ/k;->a(Ljava/lang/String;)V

    .line 250
    invoke-direct {p0}, LQ/k;->ap()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 251
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->f()LO/t;

    move-result-object v0

    iget-object v1, p0, LQ/k;->d:[LO/z;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, LO/t;->a(LO/z;)V

    .line 253
    :cond_19
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    .line 254
    return-void
.end method

.method protected z()V
    .registers 2

    .prologue
    .line 258
    const-string v0, "c"

    invoke-direct {p0, v0}, LQ/k;->a(Ljava/lang/String;)V

    .line 259
    iget-object v0, p0, LQ/k;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->i()Z

    .line 260
    return-void
.end method
