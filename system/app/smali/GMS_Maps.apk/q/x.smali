.class public final enum LQ/x;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LQ/x;

.field public static final enum b:LQ/x;

.field public static final enum c:LQ/x;

.field public static final enum d:LQ/x;

.field public static final enum e:LQ/x;

.field public static final enum f:LQ/x;

.field public static final enum g:LQ/x;

.field public static final enum h:LQ/x;

.field public static final enum i:LQ/x;

.field public static final enum j:LQ/x;

.field public static final enum k:LQ/x;

.field public static final enum l:LQ/x;

.field public static final enum m:LQ/x;

.field private static final synthetic o:[LQ/x;


# instance fields
.field private final n:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 138
    new-instance v0, LQ/x;

    const-string v1, "START"

    const-class v2, LQ/o;

    invoke-direct {v0, v1, v4, v2}, LQ/x;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LQ/x;->a:LQ/x;

    .line 139
    new-instance v0, LQ/x;

    const-string v1, "WAIT_FOR_LOCATION"

    const-class v2, LQ/B;

    invoke-direct {v0, v1, v5, v2}, LQ/x;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LQ/x;->b:LQ/x;

    .line 140
    new-instance v0, LQ/x;

    const-string v1, "WAIT_FOR_NAV_AVAILABILITY"

    const-class v2, LQ/C;

    invoke-direct {v0, v1, v6, v2}, LQ/x;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LQ/x;->c:LQ/x;

    .line 141
    new-instance v0, LQ/x;

    const-string v1, "WAIT_FOR_DIRECTIONS"

    const-class v2, LQ/z;

    invoke-direct {v0, v1, v7, v2}, LQ/x;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LQ/x;->d:LQ/x;

    .line 142
    new-instance v0, LQ/x;

    const-string v1, "WAIT_FOR_ACTIVITY"

    const-class v2, LQ/y;

    invoke-direct {v0, v1, v8, v2}, LQ/x;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LQ/x;->e:LQ/x;

    .line 143
    new-instance v0, LQ/x;

    const-string v1, "ROUTE_OVERVIEW"

    const/4 v2, 0x5

    const-class v3, LQ/m;

    invoke-direct {v0, v1, v2, v3}, LQ/x;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LQ/x;->f:LQ/x;

    .line 144
    new-instance v0, LQ/x;

    const-string v1, "ROUTE_AROUND_TRAFFIC_OVERVIEW"

    const/4 v2, 0x6

    const-class v3, LQ/k;

    invoke-direct {v0, v1, v2, v3}, LQ/x;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LQ/x;->g:LQ/x;

    .line 145
    new-instance v0, LQ/x;

    const-string v1, "LIST_VIEW"

    const/4 v2, 0x7

    const-class v3, LQ/j;

    invoke-direct {v0, v1, v2, v3}, LQ/x;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LQ/x;->h:LQ/x;

    .line 146
    new-instance v0, LQ/x;

    const-string v1, "FOLLOW_LOCATION"

    const/16 v2, 0x8

    const-class v3, LQ/a;

    invoke-direct {v0, v1, v2, v3}, LQ/x;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LQ/x;->i:LQ/x;

    .line 147
    new-instance v0, LQ/x;

    const-string v1, "FREE_MOVEMENT"

    const/16 v2, 0x9

    const-class v3, LQ/c;

    invoke-direct {v0, v1, v2, v3}, LQ/x;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LQ/x;->j:LQ/x;

    .line 148
    new-instance v0, LQ/x;

    const-string v1, "INSPECT_STEP_MAP"

    const/16 v2, 0xa

    const-class v3, LQ/e;

    invoke-direct {v0, v1, v2, v3}, LQ/x;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LQ/x;->k:LQ/x;

    .line 149
    new-instance v0, LQ/x;

    const-string v1, "INSPECT_TRAFFIC"

    const/16 v2, 0xb

    const-class v3, LQ/g;

    invoke-direct {v0, v1, v2, v3}, LQ/x;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LQ/x;->l:LQ/x;

    .line 150
    new-instance v0, LQ/x;

    const-string v1, "INSPECT_NAVIGATION_IMAGE"

    const/16 v2, 0xc

    const-class v3, LQ/d;

    invoke-direct {v0, v1, v2, v3}, LQ/x;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LQ/x;->m:LQ/x;

    .line 137
    const/16 v0, 0xd

    new-array v0, v0, [LQ/x;

    sget-object v1, LQ/x;->a:LQ/x;

    aput-object v1, v0, v4

    sget-object v1, LQ/x;->b:LQ/x;

    aput-object v1, v0, v5

    sget-object v1, LQ/x;->c:LQ/x;

    aput-object v1, v0, v6

    sget-object v1, LQ/x;->d:LQ/x;

    aput-object v1, v0, v7

    sget-object v1, LQ/x;->e:LQ/x;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LQ/x;->f:LQ/x;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LQ/x;->g:LQ/x;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LQ/x;->h:LQ/x;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LQ/x;->i:LQ/x;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LQ/x;->j:LQ/x;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LQ/x;->k:LQ/x;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LQ/x;->l:LQ/x;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LQ/x;->m:LQ/x;

    aput-object v2, v0, v1

    sput-object v0, LQ/x;->o:[LQ/x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 154
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 155
    iput-object p3, p0, LQ/x;->n:Ljava/lang/Class;

    .line 156
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LQ/x;
    .registers 2
    .parameter

    .prologue
    .line 137
    const-class v0, LQ/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LQ/x;

    return-object v0
.end method

.method public static values()[LQ/x;
    .registers 1

    .prologue
    .line 137
    sget-object v0, LQ/x;->o:[LQ/x;

    invoke-virtual {v0}, [LQ/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LQ/x;

    return-object v0
.end method


# virtual methods
.method a(LQ/p;)LQ/s;
    .registers 4
    .parameter

    .prologue
    .line 163
    :try_start_0
    iget-object v0, p0, LQ/x;->n:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQ/s;

    .line 164
    invoke-static {v0, p0}, LQ/s;->a(LQ/s;LQ/x;)LQ/x;

    .line 165
    iput-object p1, v0, LQ/s;->a:LQ/p;
    :try_end_d
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_d} :catch_e
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_d} :catch_15

    .line 166
    return-object v0

    .line 167
    :catch_e
    move-exception v0

    .line 168
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 169
    :catch_15
    move-exception v0

    .line 170
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
