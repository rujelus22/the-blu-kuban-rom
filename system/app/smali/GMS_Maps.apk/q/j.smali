.class public LQ/j;
.super LQ/s;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 14
    invoke-direct {p0}, LQ/s;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(LO/N;LO/N;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/maps/driveabout/app/cQ;->setCurrentDirectionsListStep(LO/N;)V

    .line 47
    return-void
.end method

.method protected a(LO/z;[LO/z;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, LQ/s;->a(LO/z;[LO/z;)V

    .line 39
    iget-object v0, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(LO/z;)Z

    .line 41
    iget-object v0, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->D()V

    .line 42
    return-void
.end method

.method protected a(Lcom/google/android/maps/driveabout/app/bS;)V
    .registers 3
    .parameter

    .prologue
    .line 66
    invoke-super {p0, p1}, LQ/s;->a(Lcom/google/android/maps/driveabout/app/bS;)V

    .line 67
    const v0, 0x7f100118

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->b(I)V

    .line 68
    const v0, 0x7f1004a2

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bS;->a(I)V

    .line 69
    return-void
.end method

.method protected a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 33
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 17
    iget-object v0, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->y()V

    .line 18
    iget-object v0, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->E()V

    .line 19
    iget-object v0, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(LO/z;)Z

    .line 21
    iget-object v0, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->k()LO/N;

    move-result-object v0

    .line 22
    if-eqz v0, :cond_45

    .line 23
    iget-object v1, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/maps/driveabout/app/cQ;->setCurrentDirectionsListStep(LO/N;)V

    .line 27
    :goto_3a
    iget-object v0, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setViewMode(I)V

    .line 28
    return-void

    .line 25
    :cond_45
    iget-object v0, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cQ;->D()V

    goto :goto_3a
.end method

.method protected r()V
    .registers 4

    .prologue
    .line 51
    iget-object v0, p0, LQ/j;->a:LQ/p;

    sget-object v1, LQ/x;->i:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    .line 52
    return-void
.end method

.method protected u()V
    .registers 3

    .prologue
    .line 56
    iget-object v0, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    iget-object v1, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v1}, LQ/p;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(Lcom/google/android/maps/driveabout/app/aQ;)V

    .line 57
    return-void
.end method

.method protected x()V
    .registers 3

    .prologue
    .line 61
    iget-object v0, p0, LQ/j;->a:LQ/p;

    invoke-virtual {v0}, LQ/p;->d()Lcom/google/android/maps/driveabout/app/cQ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setTopOverlayText(Ljava/lang/CharSequence;)V

    .line 62
    return-void
.end method
