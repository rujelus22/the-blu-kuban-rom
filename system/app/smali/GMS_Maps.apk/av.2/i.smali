.class Lav/i;
.super Lcom/google/android/maps/driveabout/vector/c;
.source "SourceFile"


# instance fields
.field private b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/l;)V
    .registers 2
    .parameter

    .prologue
    .line 415
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/c;-><init>(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 416
    return-void
.end method


# virtual methods
.method public declared-synchronized a()I
    .registers 2

    .prologue
    .line 453
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lav/i;->b:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_a

    if-eqz v0, :cond_8

    .line 454
    const/4 v0, 0x0

    .line 456
    :goto_6
    monitor-exit p0

    return v0

    :cond_8
    const/4 v0, 0x1

    goto :goto_6

    .line 453
    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/k;)Lcom/google/android/maps/driveabout/vector/m;
    .registers 3
    .parameter

    .prologue
    .line 445
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lav/i;->b:Z

    if-eqz v0, :cond_9

    .line 446
    iget-object v0, p0, Lav/i;->a:Lcom/google/android/maps/driveabout/vector/l;
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_b

    .line 448
    :goto_7
    monitor-exit p0

    return-object v0

    :cond_9
    move-object v0, p0

    goto :goto_7

    .line 445
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/l;)Z
    .registers 8
    .parameter

    .prologue
    .line 428
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lav/i;->b:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_2d

    if-eqz v0, :cond_8

    .line 429
    const/4 v0, 0x0

    .line 434
    :goto_6
    monitor-exit p0

    return v0

    .line 432
    :cond_8
    :try_start_8
    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/l;->b()Lo/Q;

    move-result-object v1

    iget-object v2, p0, Lav/i;->a:Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v2

    iget-object v3, p0, Lav/i;->a:Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/l;->d()F

    move-result v3

    iget-object v4, p0, Lav/i;->a:Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->e()F

    move-result v4

    iget-object v5, p0, Lav/i;->a:Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/vector/l;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    iput-object v0, p0, Lav/i;->a:Lcom/google/android/maps/driveabout/vector/l;
    :try_end_2b
    .catchall {:try_start_8 .. :try_end_2b} :catchall_2d

    .line 434
    const/4 v0, 0x1

    goto :goto_6

    .line 428
    :catchall_2d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .registers 2

    .prologue
    .line 420
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lav/i;->b:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    .line 421
    monitor-exit p0

    return-void

    .line 420
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method
