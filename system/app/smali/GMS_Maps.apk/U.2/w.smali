.class public final LU/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LU/u;


# static fields
.field static final a:Lcom/google/common/collect/ImmutableSet;


# instance fields
.field private final b:LU/v;

.field private final c:Lcom/google/common/collect/ImmutableSet;

.field private d:LV/m;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 33
    const-string v0, "gps"

    const-string v1, "network"

    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, LU/w;->a:Lcom/google/common/collect/ImmutableSet;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;LU/l;)LU/b;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 54
    const-string v0, "location_recorder"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    new-instance v0, LV/c;

    iget-object v1, p0, LU/w;->d:LV/m;

    invoke-direct {v0, v1}, LV/c;-><init>(LV/m;)V

    :goto_f
    return-object v0

    :cond_10
    iget-object v0, p0, LU/w;->b:LU/v;

    invoke-virtual {v0, p1, p2}, LU/v;->a(Ljava/lang/String;LU/l;)LU/b;

    move-result-object v0

    goto :goto_f
.end method

.method public a(Landroid/os/Handler$Callback;)LX/i;
    .registers 3
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, LU/w;->b:LU/v;

    invoke-virtual {v0, p1}, LU/v;->a(Landroid/os/Handler$Callback;)LX/i;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, LU/w;->c:Lcom/google/common/collect/ImmutableSet;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Set;
    .registers 3
    .parameter

    .prologue
    .line 69
    const-string v0, "location_recorder"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, LU/w;->a:Lcom/google/common/collect/ImmutableSet;

    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, LU/w;->b:LU/v;

    invoke-virtual {v0, p1}, LU/v;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    goto :goto_a
.end method
