.class public final enum LU/j;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LU/j;

.field public static final enum b:LU/j;

.field public static final enum c:LU/j;

.field public static final enum d:LU/j;

.field private static final synthetic e:[LU/j;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, LU/j;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v2}, LU/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/j;->a:LU/j;

    .line 57
    new-instance v0, LU/j;

    const-string v1, "ALMOST_INVALID"

    invoke-direct {v0, v1, v3}, LU/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/j;->b:LU/j;

    .line 66
    new-instance v0, LU/j;

    const-string v1, "COULD_BE_VALID"

    invoke-direct {v0, v1, v4}, LU/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/j;->c:LU/j;

    .line 72
    new-instance v0, LU/j;

    const-string v1, "VALID"

    invoke-direct {v0, v1, v5}, LU/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU/j;->d:LU/j;

    .line 43
    const/4 v0, 0x4

    new-array v0, v0, [LU/j;

    sget-object v1, LU/j;->a:LU/j;

    aput-object v1, v0, v2

    sget-object v1, LU/j;->b:LU/j;

    aput-object v1, v0, v3

    sget-object v1, LU/j;->c:LU/j;

    aput-object v1, v0, v4

    sget-object v1, LU/j;->d:LU/j;

    aput-object v1, v0, v5

    sput-object v0, LU/j;->e:[LU/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LU/j;
    .registers 2
    .parameter

    .prologue
    .line 43
    const-class v0, LU/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LU/j;

    return-object v0
.end method

.method public static values()[LU/j;
    .registers 1

    .prologue
    .line 43
    sget-object v0, LU/j;->e:[LU/j;

    invoke-virtual {v0}, [LU/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LU/j;

    return-object v0
.end method
