.class LU/Y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:LU/z;

.field private b:LU/z;

.field private c:LU/z;

.field private d:LU/R;

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method


# virtual methods
.method public a()LU/z;
    .registers 2

    .prologue
    .line 48
    iget-object v0, p0, LU/Y;->a:LU/z;

    return-object v0
.end method

.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 72
    iput-wide p1, p0, LU/Y;->f:J

    .line 73
    return-void
.end method

.method public a(LU/z;)V
    .registers 5
    .parameter

    .prologue
    .line 81
    iget-object v0, p1, LU/z;->a:LU/R;

    .line 82
    iget-object v1, p1, LU/z;->b:LU/A;

    sget-object v2, LU/A;->a:LU/A;

    if-ne v1, v2, :cond_2e

    .line 83
    iput-object v0, p0, LU/Y;->d:LU/R;

    .line 84
    iget-object v1, p0, LU/Y;->b:LU/z;

    if-eqz v1, :cond_1e

    iget-object v1, p0, LU/Y;->b:LU/z;

    iget-object v1, v1, LU/z;->c:Lbi/a;

    iget-object v1, v1, Lbi/a;->a:Lbi/t;

    iget-object v2, p1, LU/z;->c:Lbi/a;

    iget-object v2, v2, Lbi/a;->a:Lbi/t;

    invoke-virtual {v1, v2}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_24

    .line 86
    :cond_1e
    invoke-virtual {v0}, LU/R;->getTime()J

    move-result-wide v1

    iput-wide v1, p0, LU/Y;->e:J

    .line 88
    :cond_24
    iput-object p1, p0, LU/Y;->b:LU/z;

    .line 89
    invoke-virtual {v0}, LU/R;->b()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 90
    iput-object p1, p0, LU/Y;->c:LU/z;

    .line 93
    :cond_2e
    iput-object p1, p0, LU/Y;->a:LU/z;

    .line 94
    return-void
.end method

.method public b()LU/z;
    .registers 2

    .prologue
    .line 56
    iget-object v0, p0, LU/Y;->c:LU/z;

    return-object v0
.end method

.method public c()LU/R;
    .registers 2

    .prologue
    .line 60
    iget-object v0, p0, LU/Y;->d:LU/R;

    return-object v0
.end method

.method public d()J
    .registers 3

    .prologue
    .line 64
    iget-wide v0, p0, LU/Y;->e:J

    return-wide v0
.end method

.method public e()J
    .registers 3

    .prologue
    .line 68
    iget-wide v0, p0, LU/Y;->f:J

    return-wide v0
.end method
