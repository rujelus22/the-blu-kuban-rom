.class public final enum LD/H;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LD/H;

.field public static final enum b:LD/H;

.field public static final enum c:LD/H;

.field public static final enum d:LD/H;

.field public static final enum e:LD/H;

.field private static final synthetic f:[LD/H;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, LD/H;

    const-string v1, "UNAVAIL"

    invoke-direct {v0, v1, v2}, LD/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/H;->a:LD/H;

    .line 29
    new-instance v0, LD/H;

    const-string v1, "OFF_ROUTE"

    invoke-direct {v0, v1, v3}, LD/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/H;->b:LD/H;

    .line 35
    new-instance v0, LD/H;

    const-string v1, "NEEDS_MORE_FIXES"

    invoke-direct {v0, v1, v4}, LD/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/H;->c:LD/H;

    .line 40
    new-instance v0, LD/H;

    const-string v1, "ALMOST_SURE"

    invoke-direct {v0, v1, v5}, LD/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/H;->d:LD/H;

    .line 45
    new-instance v0, LD/H;

    const-string v1, "CERTAIN"

    invoke-direct {v0, v1, v6}, LD/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/H;->e:LD/H;

    .line 20
    const/4 v0, 0x5

    new-array v0, v0, [LD/H;

    sget-object v1, LD/H;->a:LD/H;

    aput-object v1, v0, v2

    sget-object v1, LD/H;->b:LD/H;

    aput-object v1, v0, v3

    sget-object v1, LD/H;->c:LD/H;

    aput-object v1, v0, v4

    sget-object v1, LD/H;->d:LD/H;

    aput-object v1, v0, v5

    sget-object v1, LD/H;->e:LD/H;

    aput-object v1, v0, v6

    sput-object v0, LD/H;->f:[LD/H;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LD/H;
    .registers 2
    .parameter

    .prologue
    .line 20
    const-class v0, LD/H;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LD/H;

    return-object v0
.end method

.method public static values()[LD/H;
    .registers 1

    .prologue
    .line 20
    sget-object v0, LD/H;->f:[LD/H;

    invoke-virtual {v0}, [LD/H;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LD/H;

    return-object v0
.end method
