.class public LD/b;
.super Lz/J;
.source "SourceFile"


# instance fields
.field private i:Z

.field private j:Landroid/graphics/Bitmap;

.field private k:I

.field private l:I

.field private final m:[I

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:F

.field private s:F

.field private final t:J

.field private u:I

.field private v:I

.field private w:Z


# direct methods
.method public constructor <init>(LD/a;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 84
    invoke-direct {p0}, Lz/J;-><init>()V

    .line 51
    new-array v0, v3, [I

    iput-object v0, p0, LD/b;->m:[I

    .line 54
    iput-boolean v2, p0, LD/b;->n:Z

    .line 55
    iput-boolean v2, p0, LD/b;->o:Z

    .line 56
    iput-boolean v2, p0, LD/b;->p:Z

    .line 57
    iput-boolean v3, p0, LD/b;->q:Z

    .line 77
    iput v2, p0, LD/b;->v:I

    .line 82
    iput-boolean v2, p0, LD/b;->w:Z

    .line 85
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LD/b;->t:J

    .line 86
    iget-object v0, p0, LD/b;->m:[I

    aput v2, v0, v2

    .line 87
    iput v3, p0, LD/b;->u:I

    .line 88
    return-void
.end method

.method public constructor <init>(LD/a;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 94
    invoke-direct {p0, p1}, LD/b;-><init>(LD/a;)V

    .line 95
    iput-boolean p2, p0, LD/b;->w:Z

    .line 96
    return-void
.end method

.method static a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Lx/k;)Landroid/graphics/Bitmap;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    const/4 v10, 0x0

    .line 431
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 432
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 433
    invoke-static {v0, v3}, LD/b;->c(II)I

    move-result v2

    .line 434
    invoke-static {v1, v3}, LD/b;->c(II)I

    move-result v3

    .line 436
    invoke-virtual {p2, v2, v3, p1}, Lx/k;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 437
    invoke-virtual {v4, v10}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 438
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 439
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 440
    invoke-virtual {v5, p0, v7, v7, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 442
    if-le v2, v0, :cond_3a

    .line 443
    new-instance v7, Landroid/graphics/Rect;

    add-int/lit8 v8, v0, -0x1

    invoke-direct {v7, v8, v10, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v8, Landroid/graphics/Rect;

    add-int/lit8 v9, v0, 0x1

    invoke-direct {v8, v0, v10, v9, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v5, p0, v7, v8, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 446
    :cond_3a
    if-le v3, v1, :cond_4d

    .line 447
    new-instance v7, Landroid/graphics/Rect;

    add-int/lit8 v8, v1, -0x1

    invoke-direct {v7, v10, v8, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v8, Landroid/graphics/Rect;

    add-int/lit8 v9, v1, 0x1

    invoke-direct {v8, v10, v1, v0, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v5, p0, v7, v8, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 450
    :cond_4d
    if-le v2, v0, :cond_66

    if-le v3, v1, :cond_66

    .line 451
    new-instance v2, Landroid/graphics/Rect;

    add-int/lit8 v3, v0, -0x1

    add-int/lit8 v7, v1, -0x1

    invoke-direct {v2, v3, v7, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v3, Landroid/graphics/Rect;

    add-int/lit8 v7, v0, 0x1

    add-int/lit8 v8, v1, 0x1

    invoke-direct {v3, v0, v1, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v5, p0, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 454
    :cond_66
    return-object v4
.end method

.method public static c(II)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 408
    .line 409
    :goto_0
    if-ge p1, p0, :cond_5

    .line 410
    shl-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 412
    :cond_5
    return p1
.end method

.method static c(Landroid/graphics/Bitmap;)Z
    .registers 4
    .parameter

    .prologue
    .line 419
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 420
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 421
    add-int/lit8 v2, v0, -0x1

    and-int/2addr v0, v2

    if-nez v0, :cond_14

    add-int/lit8 v0, v1, -0x1

    and-int/2addr v0, v1

    if-nez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public static e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 393
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 394
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 395
    iget v2, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 396
    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 397
    invoke-static {p0, p1, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private j()LD/a;
    .registers 3

    .prologue
    .line 99
    iget-wide v0, p0, LD/b;->t:J

    invoke-static {v0, v1}, LD/a;->b(J)LD/a;

    move-result-object v0

    .line 100
    if-nez v0, :cond_10

    .line 101
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Texture is out of date."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_10
    return-object v0
.end method


# virtual methods
.method public a()Ljavax/microedition/khronos/opengles/GL10;
    .registers 2

    .prologue
    .line 121
    invoke-direct {p0}, LD/b;->j()LD/a;

    move-result-object v0

    iget-object v0, v0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    return-object v0
.end method

.method public a(Landroid/content/res/Resources;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 143
    invoke-static {p1, p2}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LD/b;->a(Landroid/graphics/Bitmap;II)V

    .line 145
    iget-boolean v1, p0, LD/b;->i:Z

    if-nez v1, :cond_16

    .line 146
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 148
    :cond_16
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .registers 10
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 173
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 174
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 176
    invoke-static {p1}, LD/b;->c(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_2e

    .line 177
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0}, LD/b;->j()LD/a;

    move-result-object v1

    invoke-virtual {v1}, LD/a;->k()Lx/k;

    move-result-object v1

    invoke-static {p1, v0, v1}, LD/b;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Lx/k;)Landroid/graphics/Bitmap;

    move-result-object v1

    move v7, v6

    :goto_1f
    move-object v0, p0

    move v5, v4

    .line 184
    invoke-virtual/range {v0 .. v6}, LD/b;->a(Landroid/graphics/Bitmap;IIZZZ)V

    .line 185
    if-eqz v7, :cond_2d

    iget-boolean v0, p0, LD/b;->i:Z

    if-nez v0, :cond_2d

    .line 186
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 188
    :cond_2d
    return-void

    :cond_2e
    move v7, v4

    move-object v1, p1

    goto :goto_1f
.end method

.method public a(Landroid/graphics/Bitmap;II)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 205
    .line 206
    invoke-static {p1}, LD/b;->c(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_29

    .line 207
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0}, LD/b;->j()LD/a;

    move-result-object v1

    invoke-virtual {v1}, LD/a;->k()Lx/k;

    move-result-object v1

    invoke-static {p1, v0, v1}, LD/b;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Lx/k;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 209
    const/4 v0, 0x1

    move v7, v0

    :goto_17
    move-object v0, p0

    move v2, p2

    move v3, p3

    move v5, v4

    move v6, v4

    .line 214
    invoke-virtual/range {v0 .. v6}, LD/b;->a(Landroid/graphics/Bitmap;IIZZZ)V

    .line 215
    if-eqz v7, :cond_28

    iget-boolean v0, p0, LD/b;->i:Z

    if-nez v0, :cond_28

    .line 216
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 218
    :cond_28
    return-void

    :cond_29
    move v7, v4

    move-object v1, p1

    goto :goto_17
.end method

.method declared-synchronized a(Landroid/graphics/Bitmap;IIZZZ)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 477
    monitor-enter p0

    if-eqz p5, :cond_10

    if-eqz p6, :cond_10

    .line 478
    :try_start_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot have both isMipMap and autoGenerateMipMap be true."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_d

    .line 477
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 482
    :cond_10
    :try_start_10
    invoke-direct {p0}, LD/b;->j()LD/a;

    move-result-object v8

    .line 483
    iget-object v0, v8, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    .line 486
    if-eqz p6, :cond_75

    invoke-virtual {v8}, LD/a;->H()Z

    move-result v1

    if-eqz v1, :cond_75

    const/4 v1, 0x1

    move v3, v1

    .line 489
    :goto_20
    if-nez p1, :cond_78

    .line 490
    iput p2, p0, LD/b;->k:I

    .line 491
    iput p3, p0, LD/b;->l:I

    .line 492
    const/4 v1, 0x1

    invoke-static {p2, v1}, LD/b;->c(II)I

    move-result v6

    .line 493
    const/4 v1, 0x1

    invoke-static {p3, v1}, LD/b;->c(II)I

    move-result v7

    .line 500
    :goto_30
    invoke-virtual {v8}, LD/a;->E()I

    move-result v1

    .line 501
    if-gt v6, v1, :cond_38

    if-le v7, v1, :cond_8b

    .line 502
    :cond_38
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Textures with dimensions"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " are larger than "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " the maximum supported size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 486
    :cond_75
    const/4 v1, 0x0

    move v3, v1

    goto :goto_20

    .line 495
    :cond_78
    iput p2, p0, LD/b;->k:I

    .line 496
    if-eqz p5, :cond_89

    div-int/lit8 v1, p3, 0x2

    :goto_7e
    iput v1, p0, LD/b;->l:I

    .line 497
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 498
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    goto :goto_30

    :cond_89
    move v1, p3

    .line 496
    goto :goto_7e

    .line 507
    :cond_8b
    int-to-float v1, p2

    int-to-float v2, v6

    div-float/2addr v1, v2

    iput v1, p0, LD/b;->r:F

    .line 508
    int-to-float v1, p3

    int-to-float v2, v7

    div-float/2addr v1, v2

    iput v1, p0, LD/b;->s:F

    .line 510
    iget-boolean v1, p0, LD/b;->w:Z

    if-eqz v1, :cond_127

    .line 511
    iget-boolean v1, p0, LD/b;->n:Z

    if-eqz v1, :cond_106

    const/16 v1, 0x2901

    move v2, v1

    :goto_a0
    iget-boolean v1, p0, LD/b;->o:Z

    if-eqz v1, :cond_10b

    const/16 v1, 0x2901

    :goto_a6
    invoke-virtual {p0, v2, v1}, LD/b;->a(II)V

    .line 514
    iget-boolean v1, p0, LD/b;->p:Z

    if-eqz v1, :cond_11f

    .line 515
    if-nez p5, :cond_b1

    if-eqz v3, :cond_117

    .line 516
    :cond_b1
    iget-boolean v1, p0, LD/b;->q:Z

    if-eqz v1, :cond_10f

    .line 517
    const/16 v1, 0x2703

    const/16 v2, 0x2601

    invoke-virtual {p0, v1, v2}, LD/b;->b(II)V

    .line 577
    :goto_bc
    if-eqz p1, :cond_230

    .line 578
    if-eqz p5, :cond_1e1

    .line 580
    const/4 v3, 0x0

    .line 581
    const/4 v2, 0x0

    .line 590
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    move v5, v2

    move v2, v3

    :goto_c8
    if-lez v1, :cond_1f0

    .line 593
    new-instance v9, Landroid/graphics/Canvas;

    invoke-direct {v9}, Landroid/graphics/Canvas;-><init>()V

    .line 594
    new-instance v10, Landroid/graphics/Rect;

    const/4 v3, 0x0

    add-int v4, v1, v5

    invoke-direct {v10, v3, v5, v1, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 595
    new-instance v11, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v11, v3, v4, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 597
    if-eqz p4, :cond_1bd

    .line 598
    invoke-virtual {v8}, LD/a;->k()Lx/k;

    move-result-object v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v3, v1, v1, v4}, Lx/k;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object v4, v3

    .line 604
    :goto_eb
    invoke-virtual {v9, v4}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 605
    const/4 v3, 0x0

    invoke-virtual {v9, p1, v10, v11, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 606
    iget-boolean v3, p0, LD/b;->w:Z

    if-eqz v3, :cond_1ca

    .line 607
    const/4 v1, 0x1

    .line 608
    new-instance v3, Lz/u;

    invoke-direct {v3, v4}, Lz/u;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v3, v1}, LD/b;->a(Lz/u;Z)V

    .line 612
    const/4 v1, 0x0

    .line 618
    :goto_100
    add-int v3, v5, v1

    .line 590
    div-int/lit8 v1, v1, 0x2

    move v5, v3

    goto :goto_c8

    .line 511
    :cond_106
    const v1, 0x812f

    move v2, v1

    goto :goto_a0

    :cond_10b
    const v1, 0x812f

    goto :goto_a6

    .line 520
    :cond_10f
    const/16 v1, 0x2701

    const/16 v2, 0x2601

    invoke-virtual {p0, v1, v2}, LD/b;->b(II)V

    goto :goto_bc

    .line 524
    :cond_117
    const/16 v1, 0x2601

    const/16 v2, 0x2601

    invoke-virtual {p0, v1, v2}, LD/b;->b(II)V

    goto :goto_bc

    .line 528
    :cond_11f
    const/16 v1, 0x2600

    const/16 v2, 0x2600

    invoke-virtual {p0, v1, v2}, LD/b;->b(II)V

    goto :goto_bc

    .line 533
    :cond_127
    iget-object v1, p0, LD/b;->m:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    if-nez v1, :cond_135

    .line 534
    const/4 v1, 0x1

    iget-object v2, p0, LD/b;->m:[I

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 536
    :cond_135
    const/16 v1, 0xde1

    iget-object v2, p0, LD/b;->m:[I

    const/4 v4, 0x0

    aget v2, v2, v4

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 539
    iget-boolean v1, p0, LD/b;->n:Z

    if-eqz v1, :cond_17d

    .line 540
    const/16 v1, 0xde1

    const/16 v2, 0x2802

    const v4, 0x46240400

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 546
    :goto_14d
    iget-boolean v1, p0, LD/b;->o:Z

    if-eqz v1, :cond_188

    .line 547
    const/16 v1, 0xde1

    const/16 v2, 0x2803

    const v4, 0x46240400

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 554
    :goto_15b
    iget-boolean v1, p0, LD/b;->p:Z

    if-eqz v1, :cond_1a9

    .line 555
    if-nez p5, :cond_163

    if-eqz v3, :cond_19e

    .line 556
    :cond_163
    iget-boolean v1, p0, LD/b;->q:Z

    if-eqz v1, :cond_193

    .line 557
    const/16 v1, 0xde1

    const/16 v2, 0x2801

    const v4, 0x461c0c00

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 567
    :goto_171
    const/16 v1, 0xde1

    const/16 v2, 0x2800

    const v4, 0x46180400

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    goto/16 :goto_bc

    .line 543
    :cond_17d
    const/16 v1, 0xde1

    const/16 v2, 0x2802

    const v4, 0x47012f00

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    goto :goto_14d

    .line 550
    :cond_188
    const/16 v1, 0xde1

    const/16 v2, 0x2803

    const v4, 0x47012f00

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    goto :goto_15b

    .line 560
    :cond_193
    const/16 v1, 0xde1

    const/16 v2, 0x2801

    const v4, 0x461c0400

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    goto :goto_171

    .line 564
    :cond_19e
    const/16 v1, 0xde1

    const/16 v2, 0x2801

    const v4, 0x46180400

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    goto :goto_171

    .line 570
    :cond_1a9
    const/16 v1, 0xde1

    const/16 v2, 0x2801

    const/high16 v4, 0x4618

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 572
    const/16 v1, 0xde1

    const/16 v2, 0x2800

    const/high16 v4, 0x4618

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    goto/16 :goto_bc

    .line 601
    :cond_1bd
    invoke-virtual {v8}, LD/a;->k()Lx/k;

    move-result-object v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v3, v1, v1, v4}, Lx/k;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object v4, v3

    goto/16 :goto_eb

    .line 614
    :cond_1ca
    const/16 v3, 0xde1

    const v9, 0x8191

    const/4 v10, 0x0

    invoke-interface {v0, v3, v9, v10}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 615
    const/16 v9, 0xde1

    add-int/lit8 v3, v2, 0x1

    const/4 v10, 0x0

    invoke-static {v9, v2, v4, v10}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 616
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    move v2, v3

    goto/16 :goto_100

    .line 620
    :cond_1e1
    if-eqz v3, :cond_211

    .line 621
    iget-boolean v1, p0, LD/b;->w:Z

    if-eqz v1, :cond_200

    .line 622
    const/4 v0, 0x1

    .line 623
    new-instance v1, Lz/u;

    invoke-direct {v1, p1}, Lz/u;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v1, v0}, LD/b;->a(Lz/u;Z)V

    .line 669
    :cond_1f0
    :goto_1f0
    if-nez p1, :cond_248

    mul-int v0, v6, v7

    mul-int/lit8 v0, v0, 0x3

    :goto_1f6
    iput v0, p0, LD/b;->v:I

    .line 672
    iget-boolean v0, p0, LD/b;->i:Z

    if-eqz v0, :cond_1fe

    .line 673
    iput-object p1, p0, LD/b;->j:Landroid/graphics/Bitmap;
    :try_end_1fe
    .catchall {:try_start_10 .. :try_end_1fe} :catchall_d

    .line 675
    :cond_1fe
    monitor-exit p0

    return-void

    .line 629
    :cond_200
    const/16 v1, 0xde1

    const v2, 0x8191

    const/4 v3, 0x1

    :try_start_206
    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterx(III)V

    .line 631
    const/16 v0, 0xde1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    goto :goto_1f0

    .line 646
    :cond_211
    iget-boolean v1, p0, LD/b;->w:Z

    if-eqz v1, :cond_21f

    .line 647
    const/4 v0, 0x0

    .line 648
    new-instance v1, Lz/u;

    invoke-direct {v1, p1}, Lz/u;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v1, v0}, LD/b;->a(Lz/u;Z)V

    goto :goto_1f0

    .line 650
    :cond_21f
    const/16 v1, 0xde1

    const v2, 0x8191

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 651
    const/16 v0, 0xde1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    goto :goto_1f0

    .line 664
    :cond_230
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glFinish()V

    .line 665
    const/16 v1, 0xde1

    const v2, 0x8191

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 666
    const/16 v1, 0xde1

    const/4 v2, 0x0

    const/16 v3, 0x1907

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-interface/range {v0 .. v8}, Ljavax/microedition/khronos/opengles/GL10;->glCopyTexImage2D(IIIIIIII)V

    goto :goto_1f0

    .line 669
    :cond_248
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I
    :try_end_24f
    .catchall {:try_start_206 .. :try_end_24f} :catchall_d

    move-result v1

    mul-int/2addr v0, v1

    goto :goto_1f6
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 110
    invoke-direct {p0}, LD/b;->j()LD/a;

    move-result-object v0

    iget-object v0, v0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    if-eq p1, v0, :cond_11

    .line 111
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempted to bind texture into an OpenGL context other than the one it was created from."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_11
    iget-object v0, p0, LD/b;->m:[I

    aget v0, v0, v2

    if-eqz v0, :cond_20

    .line 116
    const/16 v0, 0xde1

    iget-object v1, p0, LD/b;->m:[I

    aget v1, v1, v2

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 118
    :cond_20
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 291
    iput-boolean p1, p0, LD/b;->n:Z

    .line 292
    return-void
.end method

.method public b()F
    .registers 2

    .prologue
    .line 323
    iget v0, p0, LD/b;->r:F

    return v0
.end method

.method public b(Landroid/content/res/Resources;I)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 156
    invoke-static {p1, p2}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 158
    const/4 v5, 0x1

    .line 160
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move-object v0, p0

    move v6, v4

    invoke-virtual/range {v0 .. v6}, LD/b;->a(Landroid/graphics/Bitmap;IIZZZ)V

    .line 162
    iget-boolean v0, p0, LD/b;->i:Z

    if-nez v0, :cond_1a

    .line 163
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 165
    :cond_1a
    return-void
.end method

.method public b(Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter

    .prologue
    .line 195
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, LD/b;->a(Landroid/graphics/Bitmap;II)V

    .line 196
    return-void
.end method

.method public b(Landroid/graphics/Bitmap;II)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 267
    .line 268
    invoke-static {p1}, LD/b;->c(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_2c

    .line 269
    sget-object v0, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0}, LD/b;->j()LD/a;

    move-result-object v1

    invoke-virtual {v1}, LD/a;->k()Lx/k;

    move-result-object v1

    invoke-static {p1, v0, v1}, LD/b;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Lx/k;)Landroid/graphics/Bitmap;

    move-result-object p1

    move v7, v4

    .line 276
    :goto_17
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->extractAlpha()Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v6, v5

    invoke-virtual/range {v0 .. v6}, LD/b;->a(Landroid/graphics/Bitmap;IIZZZ)V

    .line 277
    if-eqz v7, :cond_2b

    iget-boolean v0, p0, LD/b;->i:Z

    if-nez v0, :cond_2b

    .line 278
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 280
    :cond_2b
    return-void

    :cond_2c
    move v7, v5

    goto :goto_17
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 295
    iput-boolean p1, p0, LD/b;->o:Z

    .line 296
    return-void
.end method

.method public c()F
    .registers 2

    .prologue
    .line 331
    iget v0, p0, LD/b;->s:F

    return v0
.end method

.method public c(Landroid/content/res/Resources;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 226
    invoke-static {p1, p2}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 227
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LD/b;->b(Landroid/graphics/Bitmap;II)V

    .line 228
    iget-boolean v1, p0, LD/b;->i:Z

    if-nez v1, :cond_16

    .line 229
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 231
    :cond_16
    return-void
.end method

.method public c(Z)V
    .registers 2
    .parameter

    .prologue
    .line 303
    iput-boolean p1, p0, LD/b;->p:Z

    .line 304
    return-void
.end method

.method public d()I
    .registers 2

    .prologue
    .line 340
    iget v0, p0, LD/b;->k:I

    return v0
.end method

.method public d(Landroid/content/res/Resources;I)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 240
    invoke-static {p1, p2}, LD/b;->e(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 243
    const/4 v6, 0x0

    .line 244
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v6}, LD/b;->a(Landroid/graphics/Bitmap;IIZZZ)V

    .line 246
    iget-boolean v0, p0, LD/b;->i:Z

    if-nez v0, :cond_1a

    .line 247
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 249
    :cond_1a
    return-void
.end method

.method public d(Z)V
    .registers 2
    .parameter

    .prologue
    .line 311
    iput-boolean p1, p0, LD/b;->q:Z

    .line 312
    return-void
.end method

.method public e()I
    .registers 2

    .prologue
    .line 349
    iget v0, p0, LD/b;->l:I

    return v0
.end method

.method public e(Z)V
    .registers 2
    .parameter

    .prologue
    .line 684
    iput-boolean p1, p0, LD/b;->i:Z

    .line 685
    return-void
.end method

.method public declared-synchronized f()V
    .registers 2

    .prologue
    .line 356
    monitor-enter p0

    :try_start_1
    iget v0, p0, LD/b;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LD/b;->u:I
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 357
    monitor-exit p0

    return-void

    .line 356
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()V
    .registers 4

    .prologue
    .line 364
    monitor-enter p0

    :try_start_1
    iget v0, p0, LD/b;->u:I

    if-gtz v0, :cond_36

    .line 365
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "releaseRef called on Texture with "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LD/b;->u:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " references!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 367
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 368
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2c
    .catchall {:try_start_1 .. :try_end_2c} :catchall_2c

    .line 364
    :catchall_2c
    move-exception v0

    monitor-exit p0

    throw v0

    .line 370
    :cond_2f
    :try_start_2f
    const-string v1, "Texture"

    invoke-static {v1, v0}, LJ/a;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_34
    .catchall {:try_start_2f .. :try_end_34} :catchall_2c

    .line 384
    :cond_34
    :goto_34
    monitor-exit p0

    return-void

    .line 375
    :cond_36
    :try_start_36
    iget-wide v0, p0, LD/b;->t:J

    invoke-static {v0, v1}, LD/a;->b(J)LD/a;

    move-result-object v0

    .line 376
    iget v1, p0, LD/b;->u:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LD/b;->u:I

    if-nez v1, :cond_34

    iget-object v1, p0, LD/b;->m:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    if-eqz v1, :cond_34

    .line 377
    if-eqz v0, :cond_55

    .line 378
    iget-object v1, p0, LD/b;->m:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, LD/a;->c(I)V

    .line 382
    :cond_55
    const/4 v0, 0x0

    iput v0, p0, LD/b;->v:I
    :try_end_58
    .catchall {:try_start_36 .. :try_end_58} :catchall_2c

    goto :goto_34
.end method

.method public h()I
    .registers 2

    .prologue
    .line 388
    iget v0, p0, LD/b;->u:I

    return v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 678
    iget v0, p0, LD/b;->v:I

    return v0
.end method
