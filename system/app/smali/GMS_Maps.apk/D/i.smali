.class public final enum LD/i;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LD/i;

.field public static final enum b:LD/i;

.field public static final enum c:LD/i;

.field private static final synthetic d:[LD/i;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 88
    new-instance v0, LD/i;

    const-string v1, "FORWARD"

    invoke-direct {v0, v1, v2}, LD/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/i;->a:LD/i;

    .line 93
    new-instance v0, LD/i;

    const-string v1, "NOT_SURE"

    invoke-direct {v0, v1, v3}, LD/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/i;->b:LD/i;

    .line 98
    new-instance v0, LD/i;

    const-string v1, "BACKWARD"

    invoke-direct {v0, v1, v4}, LD/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/i;->c:LD/i;

    .line 84
    const/4 v0, 0x3

    new-array v0, v0, [LD/i;

    sget-object v1, LD/i;->a:LD/i;

    aput-object v1, v0, v2

    sget-object v1, LD/i;->b:LD/i;

    aput-object v1, v0, v3

    sget-object v1, LD/i;->c:LD/i;

    aput-object v1, v0, v4

    sput-object v0, LD/i;->d:[LD/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LD/i;
    .registers 2
    .parameter

    .prologue
    .line 84
    const-class v0, LD/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LD/i;

    return-object v0
.end method

.method public static values()[LD/i;
    .registers 1

    .prologue
    .line 84
    sget-object v0, LD/i;->d:[LD/i;

    invoke-virtual {v0}, [LD/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LD/i;

    return-object v0
.end method
