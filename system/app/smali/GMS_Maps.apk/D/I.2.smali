.class public LD/I;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LD/x;


# instance fields
.field private final a:LaQ/d;

.field private final b:Ljava/util/ArrayList;

.field private c:LaQ/a;

.field private d:F

.field private final e:Landroid/os/Handler;

.field private final f:Ljava/lang/Runnable;

.field private g:Z


# direct methods
.method public constructor <init>(LaQ/d;LD/L;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, LD/I;->a:LaQ/d;

    .line 85
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LD/I;->b:Ljava/util/ArrayList;

    .line 86
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LD/I;->e:Landroid/os/Handler;

    .line 87
    new-instance v0, LD/J;

    invoke-direct {v0, p0, p2}, LD/J;-><init>(LD/I;LD/L;)V

    iput-object v0, p0, LD/I;->f:Ljava/lang/Runnable;

    .line 118
    return-void
.end method

.method static synthetic a(LD/I;)Ljava/lang/Runnable;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, LD/I;->f:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic a(LD/I;LD/z;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, LD/I;->a(LD/z;)V

    return-void
.end method

.method private a(LD/z;)V
    .registers 4
    .parameter

    .prologue
    .line 155
    iget-object v0, p0, LD/I;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/y;

    .line 156
    invoke-interface {v0, p1}, LD/y;->a(LD/z;)V

    goto :goto_6

    .line 158
    :cond_16
    return-void
.end method

.method static synthetic b(LD/I;)Z
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-boolean v0, p0, LD/I;->g:Z

    return v0
.end method

.method private c()LD/z;
    .registers 5

    .prologue
    const-wide/high16 v2, 0x3fe0

    .line 121
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_12

    .line 122
    invoke-direct {p0}, LD/I;->h()V

    .line 123
    invoke-direct {p0}, LD/I;->d()LD/z;

    move-result-object v0

    .line 127
    :goto_11
    return-object v0

    .line 124
    :cond_12
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1f

    .line 125
    invoke-direct {p0}, LD/I;->e()LD/z;

    move-result-object v0

    goto :goto_11

    .line 127
    :cond_1f
    invoke-direct {p0}, LD/I;->f()LD/z;

    move-result-object v0

    goto :goto_11
.end method

.method static synthetic c(LD/I;)LD/z;
    .registers 2
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, LD/I;->c()LD/z;

    move-result-object v0

    return-object v0
.end method

.method private d()LD/z;
    .registers 6

    .prologue
    .line 135
    invoke-direct {p0}, LD/I;->g()LD/R;

    move-result-object v0

    iget-object v1, p0, LD/I;->c:LaQ/a;

    new-instance v2, LaQ/c;

    iget-object v3, p0, LD/I;->c:LaQ/a;

    iget-object v4, p0, LD/I;->c:LaQ/a;

    invoke-direct {v2, v3, v4}, LaQ/c;-><init>(LaQ/a;LaQ/a;)V

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, LD/z;->a(LD/R;LaQ/a;LaQ/c;Z)LD/z;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(LD/I;)V
    .registers 1
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, LD/I;->h()V

    return-void
.end method

.method private e()LD/z;
    .registers 2

    .prologue
    .line 140
    invoke-direct {p0}, LD/I;->g()LD/R;

    move-result-object v0

    invoke-static {v0}, LD/z;->a(LD/R;)LD/z;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(LD/I;)LD/z;
    .registers 2
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, LD/I;->d()LD/z;

    move-result-object v0

    return-object v0
.end method

.method private f()LD/z;
    .registers 3

    .prologue
    .line 144
    invoke-direct {p0}, LD/I;->g()LD/R;

    move-result-object v0

    sget-object v1, LD/B;->c:LD/B;

    invoke-static {v0, v1}, LD/z;->a(LD/R;LD/B;)LD/z;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(LD/I;)V
    .registers 1
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, LD/I;->i()V

    return-void
.end method

.method private g()LD/R;
    .registers 4

    .prologue
    .line 148
    new-instance v0, LD/R;

    new-instance v1, Landroid/location/Location;

    const-string v2, "gps"

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, LD/R;-><init>(Landroid/location/Location;)V

    .line 149
    sget-object v1, LD/S;->a:LD/S;

    invoke-virtual {v0, v1}, LD/R;->a(LD/S;)V

    .line 150
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LD/R;->setTime(J)V

    .line 151
    return-object v0
.end method

.method static synthetic g(LD/I;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, LD/I;->e:Landroid/os/Handler;

    return-object v0
.end method

.method private h()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 186
    iget-object v0, p0, LD/I;->c:LaQ/a;

    if-nez v0, :cond_e

    .line 187
    new-instance v0, LaQ/a;

    invoke-direct {v0, v1, v1, v4}, LaQ/a;-><init>(IIF)V

    iput-object v0, p0, LD/I;->c:LaQ/a;

    .line 201
    :cond_d
    :goto_d
    return-void

    .line 190
    :cond_e
    iget-object v0, p0, LD/I;->c:LaQ/a;

    iget-object v0, v0, LaQ/a;->a:LaQ/t;

    .line 191
    new-instance v1, LaQ/v;

    iget-object v2, p0, LD/I;->a:LaQ/d;

    invoke-direct {v1, v2}, LaQ/v;-><init>(LaQ/d;)V

    invoke-virtual {v1, v0}, LaQ/v;->a(LaQ/t;)LaQ/v;

    move-result-object v1

    .line 192
    iget-object v2, p0, LD/I;->a:LaQ/d;

    invoke-virtual {v2, v0}, LaQ/d;->e(LaQ/t;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 193
    iget v2, p0, LD/I;->d:F

    const v3, 0x3e4ccccd

    add-float/2addr v2, v3

    iput v2, p0, LD/I;->d:F

    .line 194
    iget v2, p0, LD/I;->d:F

    const/high16 v3, 0x3f80

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_3b

    .line 195
    invoke-virtual {v1}, LaQ/v;->a()LaQ/t;

    move-result-object v0

    .line 196
    iput v4, p0, LD/I;->d:F

    .line 198
    :cond_3b
    new-instance v1, LaQ/a;

    iget v2, p0, LD/I;->d:F

    invoke-direct {v1, v0, v2}, LaQ/a;-><init>(LaQ/t;F)V

    iput-object v1, p0, LD/I;->c:LaQ/a;

    goto :goto_d
.end method

.method private i()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 204
    iget-object v0, p0, LD/I;->c:LaQ/a;

    if-nez v0, :cond_e

    .line 205
    new-instance v0, LaQ/a;

    invoke-direct {v0, v1, v1, v4}, LaQ/a;-><init>(IIF)V

    iput-object v0, p0, LD/I;->c:LaQ/a;

    .line 227
    :goto_d
    return-void

    .line 208
    :cond_e
    iget-object v0, p0, LD/I;->c:LaQ/a;

    iget-object v1, v0, LaQ/a;->a:LaQ/t;

    .line 210
    iget-object v0, p0, LD/I;->a:LaQ/d;

    invoke-virtual {v0, v1}, LaQ/d;->a(LaQ/t;)LaQ/h;

    move-result-object v0

    invoke-virtual {v0}, LaQ/h;->c()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 211
    iget-object v0, p0, LD/I;->a:LaQ/d;

    iget-object v2, p0, LD/I;->c:LaQ/a;

    const/high16 v3, 0x41a0

    invoke-static {v0, v2, v3}, LG/g;->a(LaQ/d;LaQ/a;F)LaQ/a;

    move-result-object v0

    .line 218
    :goto_28
    iget-object v2, v0, LaQ/a;->a:LaQ/t;

    invoke-virtual {v2, v1}, LaQ/t;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4d

    .line 219
    new-instance v0, LaQ/v;

    iget-object v2, p0, LD/I;->a:LaQ/d;

    invoke-direct {v0, v2}, LaQ/v;-><init>(LaQ/d;)V

    invoke-virtual {v0, v1}, LaQ/v;->a(LaQ/t;)LaQ/v;

    move-result-object v0

    .line 220
    iget-object v2, p0, LD/I;->a:LaQ/d;

    invoke-virtual {v2, v1}, LaQ/d;->e(LaQ/t;)Z

    move-result v2

    if-nez v2, :cond_5b

    .line 221
    invoke-virtual {v0}, LaQ/v;->a()LaQ/t;

    move-result-object v0

    .line 223
    :goto_47
    new-instance v1, LaQ/a;

    invoke-direct {v1, v0, v4}, LaQ/a;-><init>(LaQ/t;F)V

    move-object v0, v1

    .line 225
    :cond_4d
    iput-object v0, p0, LD/I;->c:LaQ/a;

    goto :goto_d

    .line 213
    :cond_50
    iget-object v0, p0, LD/I;->a:LaQ/d;

    iget-object v2, p0, LD/I;->c:LaQ/a;

    const/high16 v3, 0x4316

    invoke-static {v0, v2, v3}, LG/g;->a(LaQ/d;LaQ/a;F)LaQ/a;

    move-result-object v0

    goto :goto_28

    :cond_5b
    move-object v0, v1

    goto :goto_47
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 176
    iget-object v0, p0, LD/I;->e:Landroid/os/Handler;

    iget-object v1, p0, LD/I;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 177
    iget-object v1, p0, LD/I;->f:Ljava/lang/Runnable;

    monitor-enter v1

    .line 178
    const/4 v0, 0x1

    :try_start_b
    iput-boolean v0, p0, LD/I;->g:Z

    .line 182
    monitor-exit v1

    .line 183
    return-void

    .line 182
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_b .. :try_end_11} :catchall_f

    throw v0
.end method

.method public a(LD/y;)V
    .registers 3
    .parameter

    .prologue
    .line 162
    iget-object v0, p0, LD/I;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 171
    iget-object v0, p0, LD/I;->e:Landroid/os/Handler;

    iget-object v1, p0, LD/I;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 172
    return-void
.end method

.method public b(LD/y;)V
    .registers 3
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, LD/I;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 168
    return-void
.end method
