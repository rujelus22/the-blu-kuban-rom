.class public LD/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final I:Ljava/util/concurrent/atomic/AtomicLong;

.field private static final K:Ljava/util/Map;


# instance fields
.field private A:Z

.field private final B:I

.field private C:Z

.field private final D:I

.field private final E:Lz/k;

.field private final F:Lx/r;

.field private final G:Lx/a;

.field private final H:LE/n;

.field private final J:J

.field final a:Ljavax/microedition/khronos/opengles/GL10;

.field final b:Lcom/google/android/maps/driveabout/vector/u;

.field final c:Z

.field public final d:LE/i;

.field public final e:LE/g;

.field public final f:LE/o;

.field public final g:LE/o;

.field public final h:LE/o;

.field public final i:LE/o;

.field public final j:LE/o;

.field public final k:[F

.field public final l:[F

.field public final m:Lo/T;

.field public final n:Lo/T;

.field private final o:Lx/k;

.field private final p:[I

.field private q:I

.field private r:I

.field private s:[I

.field private t:I

.field private u:Ljava/lang/Integer;

.field private v:Ljava/lang/Integer;

.field private w:Z

.field private x:J

.field private y:J

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 257
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, LD/a;->I:Ljava/util/concurrent/atomic/AtomicLong;

    .line 270
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, LD/a;->K:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Ljavax/microedition/khronos/opengles/GL10;Lx/k;Lcom/google/android/maps/driveabout/vector/u;Lz/k;)V
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 302
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 104
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LD/a;->z:Z

    .line 108
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LD/a;->A:Z

    .line 114
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, LD/a;->C:Z

    .line 134
    new-instance v2, LE/m;

    const/16 v3, 0x8

    new-array v3, v3, [I

    fill-array-data v3, :array_2a8

    invoke-direct {v2, v3}, LE/m;-><init>([I)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->d:LE/i;

    .line 146
    new-instance v2, LE/h;

    const/16 v3, 0x14

    new-array v3, v3, [F

    fill-array-data v3, :array_2bc

    const/16 v4, 0x9

    invoke-direct {v2, v3, v4}, LE/h;-><init>([FI)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->e:LE/g;

    .line 159
    new-instance v2, LE/s;

    const/16 v3, 0xc

    new-array v3, v3, [I

    fill-array-data v3, :array_2e8

    invoke-direct {v2, v3}, LE/s;-><init>([I)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->f:LE/o;

    .line 172
    new-instance v2, LE/s;

    const/16 v3, 0xc

    new-array v3, v3, [I

    fill-array-data v3, :array_304

    invoke-direct {v2, v3}, LE/s;-><init>([I)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->g:LE/o;

    .line 185
    new-instance v2, LE/s;

    const/16 v3, 0xc

    new-array v3, v3, [I

    fill-array-data v3, :array_320

    invoke-direct {v2, v3}, LE/s;-><init>([I)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->h:LE/o;

    .line 198
    new-instance v2, LE/s;

    const/16 v3, 0xc

    new-array v3, v3, [I

    fill-array-data v3, :array_33c

    invoke-direct {v2, v3}, LE/s;-><init>([I)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->i:LE/o;

    .line 211
    new-instance v2, LE/s;

    const/16 v3, 0xc

    new-array v3, v3, [I

    fill-array-data v3, :array_358

    invoke-direct {v2, v3}, LE/s;-><init>([I)V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->j:LE/o;

    .line 223
    const/16 v2, 0x8

    new-array v2, v2, [F

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->k:[F

    .line 229
    const/4 v2, 0x4

    new-array v2, v2, [F

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->l:[F

    .line 235
    new-instance v2, Lo/T;

    invoke-direct {v2}, Lo/T;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->m:Lo/T;

    .line 243
    new-instance v2, Lo/T;

    invoke-direct {v2}, Lo/T;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->n:Lo/T;

    .line 245
    new-instance v2, Lx/a;

    invoke-direct {v2}, Lx/a;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->G:Lx/a;

    .line 251
    new-instance v2, LE/n;

    invoke-direct {v2}, LE/n;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->H:LE/n;

    .line 263
    sget-object v2, LD/a;->I:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, LD/a;->J:J

    .line 303
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    .line 304
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    .line 305
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, LD/a;->o:Lx/k;

    .line 306
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, LD/a;->E:Lz/k;

    .line 311
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->F:Lx/r;

    .line 314
    invoke-direct/range {p0 .. p0}, LD/a;->M()V

    .line 315
    const/16 v2, 0x100

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->p:[I

    .line 316
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LD/a;->q:I

    .line 317
    const/16 v2, 0x20

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, LD/a;->s:[I

    .line 318
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LD/a;->t:I

    .line 320
    move-object/from16 v0, p1

    instance-of v2, v0, Ljavax/microedition/khronos/opengles/GL11;

    if-eqz v2, :cond_111

    const/16 v2, 0x1f02

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "1.1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_117

    :cond_111
    move-object/from16 v0, p1

    instance-of v2, v0, Lx/e;

    if-eqz v2, :cond_29e

    :cond_117
    const/4 v2, 0x1

    :goto_118
    move-object/from16 v0, p0

    iput-boolean v2, v0, LD/a;->c:Z

    .line 324
    const/4 v2, 0x1

    new-array v3, v2, [I

    .line 325
    const/16 v2, 0xd57

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    .line 326
    const/4 v2, 0x0

    aget v2, v3, v2

    move-object/from16 v0, p0

    iput v2, v0, LD/a;->B:I

    .line 329
    const/16 v2, 0xd52

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    .line 330
    const/4 v2, 0x0

    aget v2, v3, v2

    .line 331
    const/16 v4, 0xd53

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    .line 332
    const/4 v4, 0x0

    aget v4, v3, v4

    .line 333
    const/16 v5, 0xd54

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v5, v3, v6}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    .line 334
    const/4 v5, 0x0

    aget v5, v3, v5

    .line 335
    const/16 v6, 0xd55

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v6, v3, v7}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    .line 336
    const/4 v6, 0x0

    aget v6, v3, v6

    .line 337
    const/16 v7, 0xd56

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v7, v3, v8}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    .line 338
    const/4 v7, 0x0

    aget v7, v3, v7

    .line 340
    const/16 v8, 0x1f00

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v8

    .line 341
    const/16 v9, 0x1f02

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v9

    .line 342
    const/16 v10, 0x1f01

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v10

    .line 347
    const/16 v11, 0x3a

    const-string v12, "gl"

    const/16 v13, 0xa

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "r="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v13, v14

    const/4 v2, 0x1

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "g="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/4 v2, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "b="

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/4 v2, 0x3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "a="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/4 v2, 0x4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "d="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/4 v2, 0x5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, LD/a;->B:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/4 v2, 0x6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "v="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/4 v2, 0x7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/16 v2, 0x8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "c="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, v2

    const/16 v4, 0x9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "e="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/google/googlenav/android/E;->b()Z

    move-result v2

    if-eqz v2, :cond_2a1

    const-string v2, "t"

    :goto_265
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v13, v4

    invoke-static {v13}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v12, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 355
    const/16 v2, 0xd33

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    .line 356
    const/4 v2, 0x0

    aget v2, v3, v2

    move-object/from16 v0, p0

    iput v2, v0, LD/a;->D:I

    .line 357
    sget-object v3, LD/a;->K:Ljava/util/Map;

    monitor-enter v3

    .line 358
    :try_start_288
    sget-object v2, LD/a;->K:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-wide v4, v0, LD/a;->J:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Ljava/lang/ref/WeakReference;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    monitor-exit v3
    :try_end_29d
    .catchall {:try_start_288 .. :try_end_29d} :catchall_2a4

    .line 360
    return-void

    .line 320
    :cond_29e
    const/4 v2, 0x0

    goto/16 :goto_118

    .line 347
    :cond_2a1
    const-string v2, "f"

    goto :goto_265

    .line 359
    :catchall_2a4
    move-exception v2

    :try_start_2a5
    monitor-exit v3
    :try_end_2a6
    .catchall {:try_start_2a5 .. :try_end_2a6} :catchall_2a4

    throw v2

    .line 134
    nop

    :array_2a8
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x1t 0x0t
    .end array-data

    .line 146
    :array_2bc
    .array-data 0x4
        0x0t 0x0t 0x80t 0xbft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0xbft
        0x0t 0x0t 0x80t 0xbft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0xbft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    .line 159
    :array_2e8
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 172
    :array_304
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 185
    :array_320
    .array-data 0x4
        0x0t 0x0t 0xfft 0xfft
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0xfft 0xfft
        0x0t 0x0t 0xfft 0xfft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0xfft 0xfft
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 198
    :array_33c
    .array-data 0x4
        0x0t 0x0t 0xfft 0xfft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0xfft 0xfft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0xfft 0xfft
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0xfft 0xfft
    .end array-data

    .line 211
    :array_358
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x1t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private M()V
    .registers 4

    .prologue
    .line 612
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xbd0

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 613
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb44

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 614
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb71

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 615
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb90

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 617
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0x405

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glCullFace(I)V

    .line 618
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0x901

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glFrontFace(I)V

    .line 619
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0x1d01

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glShadeModel(I)V

    .line 622
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xc50

    const/16 v2, 0x1102

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glHint(II)V

    .line 624
    const/4 v0, 0x0

    iput v0, p0, LD/a;->r:I

    .line 625
    return-void
.end method

.method private N()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 830
    iget-object v0, p0, LD/a;->s:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [I

    .line 831
    iget-object v1, p0, LD/a;->s:[I

    iget-object v2, p0, LD/a;->s:[I

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 832
    iput-object v0, p0, LD/a;->s:[I

    .line 833
    return-void
.end method

.method public static a(LD/a;)J
    .registers 3
    .parameter

    .prologue
    .line 886
    if-nez p0, :cond_5

    const-wide/16 v0, -0x1

    :goto_4
    return-wide v0

    :cond_5
    iget-wide v0, p0, LD/a;->J:J

    goto :goto_4
.end method

.method private a(IZ)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 745
    if-eqz p2, :cond_12

    .line 746
    iget v0, p0, LD/a;->r:I

    or-int/2addr v0, p1

    iput v0, p0, LD/a;->r:I

    .line 747
    iget-object v0, p0, LD/a;->p:[I

    iget v1, p0, LD/a;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LD/a;->q:I

    aput p1, v0, v1

    .line 752
    :goto_11
    return-void

    .line 749
    :cond_12
    iget v0, p0, LD/a;->r:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, LD/a;->r:I

    .line 750
    iget-object v0, p0, LD/a;->p:[I

    iget v1, p0, LD/a;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LD/a;->q:I

    const/high16 v2, 0x4000

    or-int/2addr v2, p1

    aput v2, v0, v1

    goto :goto_11
.end method

.method public static b(J)LD/a;
    .registers 6
    .parameter

    .prologue
    .line 894
    sget-object v2, LD/a;->K:Ljava/util/Map;

    monitor-enter v2

    .line 895
    :try_start_3
    sget-object v0, LD/a;->K:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 896
    const/4 v1, 0x0

    .line 897
    if-eqz v0, :cond_28

    .line 898
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/a;

    .line 899
    if-nez v0, :cond_23

    .line 901
    sget-object v1, LD/a;->K:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 904
    :cond_23
    :goto_23
    monitor-exit v2

    return-object v0

    .line 905
    :catchall_25
    move-exception v0

    monitor-exit v2
    :try_end_27
    .catchall {:try_start_3 .. :try_end_27} :catchall_25

    throw v0

    :cond_28
    move-object v0, v1

    goto :goto_23
.end method


# virtual methods
.method public A()V
    .registers 4

    .prologue
    .line 636
    iget-object v0, p0, LD/a;->p:[I

    iget v1, p0, LD/a;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LD/a;->q:I

    const v2, 0x7fffffff

    aput v2, v0, v1

    .line 637
    return-void
.end method

.method public B()V
    .registers 10

    .prologue
    const/16 v8, 0xde1

    const/16 v7, 0xbe2

    const/16 v6, 0xbd0

    const/16 v5, 0xb90

    const/16 v4, 0xb71

    .line 657
    :goto_a
    iget v0, p0, LD/a;->q:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LD/a;->q:I

    if-ltz v0, :cond_df

    iget-object v0, p0, LD/a;->p:[I

    iget v1, p0, LD/a;->q:I

    aget v0, v0, v1

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_df

    .line 660
    iget-object v0, p0, LD/a;->p:[I

    iget v1, p0, LD/a;->q:I

    aget v0, v0, v1

    const/high16 v1, 0x4000

    and-int/2addr v0, v1

    if-eqz v0, :cond_3e

    const/4 v0, 0x1

    .line 661
    :goto_29
    iget-object v1, p0, LD/a;->p:[I

    iget v2, p0, LD/a;->q:I

    aget v1, v1, v2

    const v2, 0x3fffffff

    and-int/2addr v1, v2

    .line 662
    sparse-switch v1, :sswitch_data_e0

    .line 732
    :goto_36
    if-eqz v0, :cond_d6

    .line 733
    iget v0, p0, LD/a;->r:I

    or-int/2addr v0, v1

    iput v0, p0, LD/a;->r:I

    goto :goto_a

    .line 660
    :cond_3e
    const/4 v0, 0x0

    goto :goto_29

    .line 664
    :sswitch_40
    if-eqz v0, :cond_4b

    .line 665
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8074

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    goto :goto_36

    .line 667
    :cond_4b
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8074

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    goto :goto_36

    .line 672
    :sswitch_54
    if-eqz v0, :cond_64

    .line 673
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v8}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 674
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8078

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    goto :goto_36

    .line 676
    :cond_64
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v8}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 677
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8078

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    goto :goto_36

    .line 682
    :sswitch_72
    if-eqz v0, :cond_7a

    .line 683
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v7}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    goto :goto_36

    .line 685
    :cond_7a
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v7}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto :goto_36

    .line 690
    :sswitch_80
    if-eqz v0, :cond_8b

    .line 691
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8076

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    goto :goto_36

    .line 693
    :cond_8b
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8076

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    goto :goto_36

    .line 698
    :sswitch_94
    if-eqz v0, :cond_9c

    .line 699
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v6}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    goto :goto_36

    .line 701
    :cond_9c
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v6}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto :goto_36

    .line 706
    :sswitch_a2
    if-eqz v0, :cond_aa

    .line 707
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    goto :goto_36

    .line 709
    :cond_aa
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto :goto_36

    .line 714
    :sswitch_b0
    if-eqz v0, :cond_bc

    .line 715
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8037

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    goto/16 :goto_36

    .line 717
    :cond_bc
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v3, 0x8037

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto/16 :goto_36

    .line 722
    :sswitch_c6
    if-eqz v0, :cond_cf

    .line 723
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v5}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    goto/16 :goto_36

    .line 725
    :cond_cf
    iget-object v2, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v5}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto/16 :goto_36

    .line 735
    :cond_d6
    iget v0, p0, LD/a;->r:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, LD/a;->r:I

    goto/16 :goto_a

    .line 738
    :cond_df
    return-void

    .line 662
    :sswitch_data_e0
    .sparse-switch
        0x1 -> :sswitch_40
        0x2 -> :sswitch_54
        0x4 -> :sswitch_72
        0x8 -> :sswitch_80
        0x10 -> :sswitch_94
        0x20 -> :sswitch_a2
        0x40 -> :sswitch_b0
        0x80 -> :sswitch_c6
    .end sparse-switch
.end method

.method public declared-synchronized C()V
    .registers 5

    .prologue
    .line 770
    monitor-enter p0

    :try_start_1
    iget v0, p0, LD/a;->t:I

    if-lez v0, :cond_12

    .line 771
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    iget v1, p0, LD/a;->t:I

    iget-object v2, p0, LD/a;->s:[I

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDeleteTextures(I[II)V

    .line 772
    const/4 v0, 0x0

    iput v0, p0, LD/a;->t:I
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_14

    .line 774
    :cond_12
    monitor-exit p0

    return-void

    .line 770
    :catchall_14
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public D()V
    .registers 2

    .prologue
    .line 778
    iget-object v0, p0, LD/a;->h:LE/o;

    invoke-virtual {v0, p0}, LE/o;->c(LD/a;)V

    .line 779
    iget-object v0, p0, LD/a;->e:LE/g;

    invoke-virtual {v0, p0}, LE/g;->c(LD/a;)V

    .line 780
    iget-object v0, p0, LD/a;->d:LE/i;

    invoke-virtual {v0, p0}, LE/i;->c(LD/a;)V

    .line 781
    iget-object v0, p0, LD/a;->j:LE/o;

    invoke-virtual {v0, p0}, LE/o;->c(LD/a;)V

    .line 782
    iget-object v0, p0, LD/a;->f:LE/o;

    invoke-virtual {v0, p0}, LE/o;->c(LD/a;)V

    .line 783
    iget-object v0, p0, LD/a;->i:LE/o;

    invoke-virtual {v0, p0}, LE/o;->c(LD/a;)V

    .line 784
    iget-object v0, p0, LD/a;->g:LE/o;

    invoke-virtual {v0, p0}, LE/o;->c(LD/a;)V

    .line 785
    return-void
.end method

.method public E()I
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 791
    iget-object v0, p0, LD/a;->u:Ljava/lang/Integer;

    if-nez v0, :cond_17

    .line 792
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 793
    iget-object v1, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v2, 0xd33

    invoke-interface {v1, v2, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    .line 794
    aget v0, v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LD/a;->u:Ljava/lang/Integer;

    .line 796
    :cond_17
    iget-object v0, p0, LD/a;->u:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public F()I
    .registers 5

    .prologue
    .line 800
    iget-object v0, p0, LD/a;->v:Ljava/lang/Integer;

    if-nez v0, :cond_19

    .line 801
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 802
    iget-object v1, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v2, 0x846e

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    .line 803
    const/4 v1, 0x1

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LD/a;->v:Ljava/lang/Integer;

    .line 805
    :cond_19
    iget-object v0, p0, LD/a;->v:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public G()Lx/a;
    .registers 2

    .prologue
    .line 809
    iget-object v0, p0, LD/a;->G:Lx/a;

    return-object v0
.end method

.method public H()Z
    .registers 2

    .prologue
    .line 817
    iget-boolean v0, p0, LD/a;->c:Z

    return v0
.end method

.method public I()I
    .registers 2

    .prologue
    .line 826
    iget v0, p0, LD/a;->B:I

    return v0
.end method

.method public J()Z
    .registers 2

    .prologue
    .line 851
    iget-boolean v0, p0, LD/a;->C:Z

    return v0
.end method

.method public K()I
    .registers 2

    .prologue
    .line 858
    iget v0, p0, LD/a;->D:I

    return v0
.end method

.method public L()LE/n;
    .registers 2

    .prologue
    .line 865
    iget-object v0, p0, LD/a;->H:LE/n;

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 367
    const/4 v0, 0x1

    iput-boolean v0, p0, LD/a;->w:Z

    .line 368
    return-void
.end method

.method public a(J)V
    .registers 7
    .parameter

    .prologue
    .line 375
    iget-wide v0, p0, LD/a;->x:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_12

    .line 376
    iput-wide p1, p0, LD/a;->x:J

    .line 382
    :goto_a
    iget-object v0, p0, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    iget-wide v1, p0, LD/a;->x:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(J)V

    .line 383
    return-void

    .line 380
    :cond_12
    iget-wide v0, p0, LD/a;->x:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LD/a;->x:J

    goto :goto_a
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 844
    iput-boolean p1, p0, LD/a;->C:Z

    .line 845
    return-void
.end method

.method public a(I)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 433
    iget-object v1, p0, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v1, p1}, Lcom/google/android/maps/driveabout/vector/u;->a(I)Z

    move-result v1

    if-nez v1, :cond_c

    .line 434
    iput-boolean v0, p0, LD/a;->w:Z

    .line 435
    const/4 v0, 0x0

    .line 437
    :cond_c
    return v0
.end method

.method public b()J
    .registers 7

    .prologue
    const-wide/16 v4, 0x0

    .line 391
    iget-wide v0, p0, LD/a;->x:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_b

    .line 392
    const-wide/16 v0, -0x1

    .line 394
    :goto_a
    return-wide v0

    :cond_b
    iget-wide v0, p0, LD/a;->x:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_a
.end method

.method public b(I)V
    .registers 3
    .parameter

    .prologue
    .line 442
    iget-object v0, p0, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/u;->b(I)V

    .line 443
    return-void
.end method

.method public declared-synchronized c(I)V
    .registers 5
    .parameter

    .prologue
    .line 759
    monitor-enter p0

    :try_start_1
    iget v0, p0, LD/a;->t:I

    iget-object v1, p0, LD/a;->s:[I

    array-length v1, v1

    if-ne v0, v1, :cond_b

    .line 760
    invoke-direct {p0}, LD/a;->N()V

    .line 762
    :cond_b
    iget-object v0, p0, LD/a;->s:[I

    iget v1, p0, LD/a;->t:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LD/a;->t:I

    aput p1, v0, v1
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_17

    .line 763
    monitor-exit p0

    return-void

    .line 759
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 402
    iget-boolean v0, p0, LD/a;->w:Z

    return v0
.end method

.method public d()J
    .registers 3

    .prologue
    .line 407
    iget-wide v0, p0, LD/a;->y:J

    return-wide v0
.end method

.method public e()V
    .registers 7

    .prologue
    const-wide/16 v4, 0x0

    .line 414
    iget-object v0, p0, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->a()V

    .line 415
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LD/a;->y:J

    .line 416
    const/4 v0, 0x0

    iput-boolean v0, p0, LD/a;->w:Z

    .line 417
    iget-wide v0, p0, LD/a;->x:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_20

    iget-wide v0, p0, LD/a;->y:J

    iget-wide v2, p0, LD/a;->x:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_20

    .line 418
    iput-wide v4, p0, LD/a;->x:J

    .line 420
    :cond_20
    return-void
.end method

.method public f()V
    .registers 2

    .prologue
    .line 424
    iget-object v0, p0, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->b()V

    .line 425
    return-void
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 450
    iget-boolean v0, p0, LD/a;->z:Z

    return v0
.end method

.method public h()V
    .registers 2

    .prologue
    .line 455
    const/4 v0, 0x0

    iput-boolean v0, p0, LD/a;->z:Z

    .line 456
    return-void
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 463
    iget-boolean v0, p0, LD/a;->A:Z

    return v0
.end method

.method public j()V
    .registers 2

    .prologue
    .line 468
    const/4 v0, 0x0

    iput-boolean v0, p0, LD/a;->A:Z

    .line 469
    return-void
.end method

.method public k()Lx/k;
    .registers 2

    .prologue
    .line 475
    iget-object v0, p0, LD/a;->o:Lx/k;

    return-object v0
.end method

.method public l()Lz/k;
    .registers 2

    .prologue
    .line 483
    iget-object v0, p0, LD/a;->E:Lz/k;

    return-object v0
.end method

.method public m()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 496
    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_12

    .line 497
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v1, 0x8074

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 498
    invoke-direct {p0, v2, v2}, LD/a;->a(IZ)V

    .line 500
    :cond_12
    return-void
.end method

.method public n()V
    .registers 3

    .prologue
    .line 510
    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_14

    .line 511
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v1, 0x8076

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 512
    const/16 v0, 0x8

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    .line 514
    :cond_14
    return-void
.end method

.method public o()V
    .registers 3

    .prologue
    .line 517
    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_14

    .line 518
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v1, 0x8076

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    .line 519
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    .line 521
    :cond_14
    return-void
.end method

.method public p()V
    .registers 3

    .prologue
    .line 524
    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1a

    .line 525
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xde1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 526
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v1, 0x8078

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 527
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    .line 529
    :cond_1a
    return-void
.end method

.method public q()V
    .registers 3

    .prologue
    .line 532
    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1a

    .line 533
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xde1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 534
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v1, 0x8078

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    .line 535
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    .line 537
    :cond_1a
    return-void
.end method

.method public r()V
    .registers 3

    .prologue
    .line 540
    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_12

    .line 541
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xbe2

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 542
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    .line 544
    :cond_12
    return-void
.end method

.method public s()V
    .registers 3

    .prologue
    .line 547
    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_12

    .line 548
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xbe2

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 549
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    .line 551
    :cond_12
    return-void
.end method

.method public t()V
    .registers 3

    .prologue
    .line 554
    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_13

    .line 555
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xbd0

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 556
    const/16 v0, 0x10

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    .line 558
    :cond_13
    return-void
.end method

.method public u()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 568
    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_15

    .line 569
    iput-boolean v2, p0, LD/a;->z:Z

    .line 570
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb71

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 571
    const/16 v0, 0x20

    invoke-direct {p0, v0, v2}, LD/a;->a(IZ)V

    .line 573
    :cond_15
    return-void
.end method

.method public v()V
    .registers 3

    .prologue
    .line 583
    iget v0, p0, LD/a;->r:I

    and-int/lit8 v0, v0, 0x40

    if-nez v0, :cond_14

    .line 584
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const v1, 0x8037

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 585
    const/16 v0, 0x40

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    .line 587
    :cond_14
    return-void
.end method

.method public w()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 597
    iget v0, p0, LD/a;->r:I

    and-int/lit16 v0, v0, 0x80

    if-nez v0, :cond_15

    .line 598
    iput-boolean v2, p0, LD/a;->A:Z

    .line 599
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb90

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 600
    const/16 v0, 0x80

    invoke-direct {p0, v0, v2}, LD/a;->a(IZ)V

    .line 602
    :cond_15
    return-void
.end method

.method public x()V
    .registers 3

    .prologue
    .line 605
    iget v0, p0, LD/a;->r:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_13

    .line 606
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v1, 0xb90

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 607
    const/16 v0, 0x80

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LD/a;->a(IZ)V

    .line 609
    :cond_13
    return-void
.end method

.method public y()Ljavax/microedition/khronos/opengles/GL10;
    .registers 2

    .prologue
    .line 628
    iget-object v0, p0, LD/a;->a:Ljavax/microedition/khronos/opengles/GL10;

    return-object v0
.end method

.method public z()Lcom/google/android/maps/driveabout/vector/u;
    .registers 2

    .prologue
    .line 632
    iget-object v0, p0, LD/a;->b:Lcom/google/android/maps/driveabout/vector/u;

    return-object v0
.end method
