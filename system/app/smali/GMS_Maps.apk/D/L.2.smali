.class public final enum LD/L;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LD/L;

.field public static final enum b:LD/L;

.field public static final enum c:LD/L;

.field private static final synthetic d:[LD/L;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-instance v0, LD/L;

    const-string v1, "RANDOMIZE_EVENTS"

    invoke-direct {v0, v1, v2}, LD/L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/L;->a:LD/L;

    .line 48
    new-instance v0, LD/L;

    const-string v1, "CONSTANT_PROGRESS_INCREMENT"

    invoke-direct {v0, v1, v3}, LD/L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/L;->b:LD/L;

    .line 54
    new-instance v0, LD/L;

    const-string v1, "CONSTANT_SPEED"

    invoke-direct {v0, v1, v4}, LD/L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/L;->c:LD/L;

    .line 37
    const/4 v0, 0x3

    new-array v0, v0, [LD/L;

    sget-object v1, LD/L;->a:LD/L;

    aput-object v1, v0, v2

    sget-object v1, LD/L;->b:LD/L;

    aput-object v1, v0, v3

    sget-object v1, LD/L;->c:LD/L;

    aput-object v1, v0, v4

    sput-object v0, LD/L;->d:[LD/L;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LD/L;
    .registers 2
    .parameter

    .prologue
    .line 37
    const-class v0, LD/L;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LD/L;

    return-object v0
.end method

.method public static values()[LD/L;
    .registers 1

    .prologue
    .line 37
    sget-object v0, LD/L;->d:[LD/L;

    invoke-virtual {v0}, [LD/L;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LD/L;

    return-object v0
.end method
