.class public LD/N;
.super LD/b;
.source "SourceFile"


# instance fields
.field a:I

.field final b:Ljava/lang/Runnable;

.field private final c:LG/i;


# direct methods
.method public constructor <init>(LD/l;)V
    .registers 3
    .parameter

    .prologue
    .line 40
    const-string v0, "network_fixup_provider"

    invoke-direct {p0, v0, p1}, LD/b;-><init>(Ljava/lang/String;LD/T;)V

    .line 21
    const/4 v0, 0x1

    iput v0, p0, LD/N;->a:I

    .line 29
    new-instance v0, LD/O;

    invoke-direct {v0, p0}, LD/O;-><init>(LD/N;)V

    iput-object v0, p0, LD/N;->b:Ljava/lang/Runnable;

    .line 41
    invoke-interface {p1}, LD/l;->a()LG/i;

    move-result-object v0

    iput-object v0, p0, LD/N;->c:LG/i;

    .line 42
    return-void
.end method

.method static a()F
    .registers 1

    .prologue
    .line 66
    invoke-static {}, LG/j;->a()LG/q;

    move-result-object v0

    iget v0, v0, LG/q;->A:F

    return v0
.end method

.method static g()J
    .registers 2

    .prologue
    .line 83
    invoke-static {}, LG/j;->a()LG/q;

    move-result-object v0

    iget v0, v0, LG/q;->s:I

    int-to-long v0, v0

    return-wide v0
.end method

.method private h()V
    .registers 3

    .prologue
    .line 70
    iget-object v0, p0, LD/N;->c:LG/i;

    iget-object v1, p0, LD/N;->b:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, LG/i;->a(Ljava/lang/Runnable;)V

    .line 71
    return-void
.end method

.method private i()V
    .registers 5

    .prologue
    .line 74
    iget-object v0, p0, LD/N;->c:LG/i;

    iget-object v1, p0, LD/N;->b:Ljava/lang/Runnable;

    invoke-static {}, LD/N;->g()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LG/i;->b(Ljava/lang/Runnable;J)Z

    .line 75
    return-void
.end method


# virtual methods
.method public a(LD/R;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x2

    .line 46
    invoke-virtual {p1}, LD/R;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p1}, LD/R;->getAccuracy()F

    move-result v0

    invoke-static {}, LD/N;->a()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-lez v0, :cond_16

    .line 58
    :goto_15
    return-void

    .line 49
    :cond_16
    iget v0, p0, LD/N;->a:I

    if-eq v0, v2, :cond_25

    .line 50
    iput v2, p0, LD/N;->a:I

    .line 51
    const-string v0, "network"

    const-string v1, "network"

    iget v2, p0, LD/N;->a:I

    invoke-virtual {p0, v0, v1, v2}, LD/N;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 54
    :cond_25
    invoke-direct {p0}, LD/N;->h()V

    .line 55
    invoke-direct {p0}, LD/N;->i()V

    .line 57
    invoke-super {p0, p1}, LD/b;->a(LD/R;)V

    goto :goto_15
.end method
