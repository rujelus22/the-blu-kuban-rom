.class public final enum LD/j;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LD/j;

.field public static final enum b:LD/j;

.field public static final enum c:LD/j;

.field public static final enum d:LD/j;

.field private static final synthetic e:[LD/j;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, LD/j;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v2}, LD/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/j;->a:LD/j;

    .line 57
    new-instance v0, LD/j;

    const-string v1, "ALMOST_INVALID"

    invoke-direct {v0, v1, v3}, LD/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/j;->b:LD/j;

    .line 66
    new-instance v0, LD/j;

    const-string v1, "COULD_BE_VALID"

    invoke-direct {v0, v1, v4}, LD/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/j;->c:LD/j;

    .line 72
    new-instance v0, LD/j;

    const-string v1, "VALID"

    invoke-direct {v0, v1, v5}, LD/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LD/j;->d:LD/j;

    .line 43
    const/4 v0, 0x4

    new-array v0, v0, [LD/j;

    sget-object v1, LD/j;->a:LD/j;

    aput-object v1, v0, v2

    sget-object v1, LD/j;->b:LD/j;

    aput-object v1, v0, v3

    sget-object v1, LD/j;->c:LD/j;

    aput-object v1, v0, v4

    sget-object v1, LD/j;->d:LD/j;

    aput-object v1, v0, v5

    sput-object v0, LD/j;->e:[LD/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LD/j;
    .registers 2
    .parameter

    .prologue
    .line 43
    const-class v0, LD/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LD/j;

    return-object v0
.end method

.method public static values()[LD/j;
    .registers 1

    .prologue
    .line 43
    sget-object v0, LD/j;->e:[LD/j;

    invoke-virtual {v0}, [LD/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LD/j;

    return-object v0
.end method
