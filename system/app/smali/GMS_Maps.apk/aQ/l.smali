.class public LaQ/l;
.super LaQ/h;
.source "SourceFile"


# instance fields
.field private final d:Lae/t;


# direct methods
.method public constructor <init>(Lae/w;Lae/t;Lae/t;ILjava/util/List;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 577
    sget-object v1, LaQ/q;->a:LaQ/q;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LaQ/h;-><init>(LaQ/q;Lae/w;Lae/t;ILjava/util/List;)V

    .line 578
    iput-object p3, p0, LaQ/l;->d:Lae/t;

    .line 579
    invoke-virtual {p0}, LaQ/l;->C()V

    .line 580
    return-void
.end method


# virtual methods
.method protected C()V
    .registers 3

    .prologue
    .line 633
    iget-object v0, p0, LaQ/l;->b:Lae/w;

    iget v1, p0, LaQ/l;->c:I

    invoke-static {v0, v1, p0}, LaQ/h;->b(Lae/w;ILaQ/h;)V

    .line 634
    return-void
.end method

.method public i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 584
    const/16 v0, 0x4bc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()Ljava/util/Date;
    .registers 2

    .prologue
    .line 618
    iget-object v0, p0, LaQ/l;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaQ/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/util/Date;
    .registers 8

    .prologue
    .line 623
    invoke-virtual {p0}, LaQ/l;->n()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_23

    invoke-virtual {p0}, LaQ/l;->p()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 624
    invoke-virtual {p0}, LaQ/l;->n()Ljava/util/Date;

    move-result-object v1

    .line 625
    invoke-virtual {p0}, LaQ/l;->q()I

    move-result v2

    .line 626
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    int-to-long v1, v2

    const-wide/16 v5, 0x3e8

    mul-long/2addr v1, v5

    add-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 628
    :goto_22
    return-object v0

    :cond_23
    const/4 v0, 0x0

    goto :goto_22
.end method

.method public u()Lau/B;
    .registers 2

    .prologue
    .line 602
    iget-object v0, p0, LaQ/l;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->q()Lau/B;

    move-result-object v0

    if-nez v0, :cond_f

    .line 603
    iget-object v0, p0, LaQ/l;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->g()Lau/B;

    move-result-object v0

    .line 605
    :goto_e
    return-object v0

    :cond_f
    iget-object v0, p0, LaQ/l;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->q()Lau/B;

    move-result-object v0

    goto :goto_e
.end method

.method public v()Lau/B;
    .registers 2

    .prologue
    .line 610
    iget-object v0, p0, LaQ/l;->d:Lae/t;

    if-nez v0, :cond_b

    .line 611
    iget-object v0, p0, LaQ/l;->b:Lae/w;

    invoke-virtual {v0}, Lae/w;->aC()Lau/B;

    move-result-object v0

    .line 613
    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, LaQ/l;->d:Lae/t;

    invoke-virtual {v0}, Lae/t;->g()Lau/B;

    move-result-object v0

    goto :goto_a
.end method

.method public w()Ljava/lang/String;
    .registers 3

    .prologue
    .line 589
    iget-object v0, p0, LaQ/l;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->i()Ljava/lang/String;

    move-result-object v0

    .line 590
    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 591
    iget-object v0, p0, LaQ/l;->d:Lae/t;

    if-eqz v0, :cond_20

    iget-object v0, p0, LaQ/l;->d:Lae/t;

    invoke-virtual {v0}, Lae/t;->E()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_20

    .line 592
    iget-object v0, p0, LaQ/l;->d:Lae/t;

    invoke-virtual {v0}, Lae/t;->l()Ljava/lang/String;

    move-result-object v0

    .line 597
    :cond_1f
    :goto_1f
    return-object v0

    .line 594
    :cond_20
    iget-object v0, p0, LaQ/l;->b:Lae/w;

    invoke-virtual {v0}, Lae/w;->as()Lae/y;

    move-result-object v0

    invoke-static {v0}, LaQ/l;->a(Lae/y;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1f
.end method
