.class public LaQ/j;
.super LaQ/s;
.source "SourceFile"


# instance fields
.field private final e:Lae/t;


# direct methods
.method public constructor <init>(Lae/w;Lae/t;Lae/t;ILjava/util/List;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 652
    sget-object v1, LaQ/q;->b:LaQ/q;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LaQ/s;-><init>(LaQ/q;Lae/w;Lae/t;ILjava/util/List;)V

    .line 653
    iput-object p2, p0, LaQ/j;->e:Lae/t;

    .line 654
    invoke-virtual {p3}, Lae/t;->a()Lae/v;

    move-result-object v0

    iput-object v0, p0, LaQ/j;->d:Lae/v;

    .line 655
    invoke-virtual {p0}, LaQ/j;->C()V

    .line 656
    return-void
.end method


# virtual methods
.method protected C()V
    .registers 2

    .prologue
    .line 695
    invoke-virtual {p0}, LaQ/j;->s()V

    .line 696
    invoke-virtual {p0}, LaQ/j;->t()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 697
    invoke-virtual {p0}, LaQ/j;->y()F

    move-result v0

    invoke-virtual {p0, v0}, LaQ/j;->a(F)V

    .line 699
    :cond_10
    return-void
.end method

.method public D()Ljava/lang/String;
    .registers 2

    .prologue
    .line 703
    invoke-virtual {p0}, LaQ/j;->N()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()Ljava/util/Date;
    .registers 2

    .prologue
    .line 680
    iget-object v0, p0, LaQ/j;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaQ/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/util/Date;
    .registers 3

    .prologue
    .line 685
    iget-object v0, p0, LaQ/j;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->Q()[Lae/u;

    move-result-object v0

    .line 686
    array-length v1, v0

    if-lez v1, :cond_11

    .line 687
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lae/u;->b()Ljava/util/Date;

    move-result-object v0

    .line 689
    :goto_10
    return-object v0

    :cond_11
    iget-object v0, p0, LaQ/j;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaQ/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    goto :goto_10
.end method

.method public u()Lau/B;
    .registers 2

    .prologue
    .line 665
    iget-object v0, p0, LaQ/j;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->q()Lau/B;

    move-result-object v0

    return-object v0
.end method

.method public v()Lau/B;
    .registers 3

    .prologue
    .line 670
    iget-object v0, p0, LaQ/j;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->Q()[Lae/u;

    move-result-object v0

    .line 671
    array-length v1, v0

    if-lez v1, :cond_11

    .line 672
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lae/u;->d()Lau/B;

    move-result-object v0

    .line 674
    :goto_10
    return-object v0

    :cond_11
    iget-object v0, p0, LaQ/j;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->k()Lau/B;

    move-result-object v0

    goto :goto_10
.end method
