.class public abstract LaQ/s;
.super LaQ/h;
.source "SourceFile"


# instance fields
.field protected d:Lae/v;

.field private e:Z

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method protected constructor <init>(LaQ/q;Lae/w;Lae/t;ILjava/util/List;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 451
    invoke-direct/range {p0 .. p5}, LaQ/h;-><init>(LaQ/q;Lae/w;Lae/t;ILjava/util/List;)V

    .line 453
    if-eqz p3, :cond_b

    .line 454
    invoke-direct {p0}, LaQ/s;->C()V

    .line 455
    invoke-direct {p0}, LaQ/s;->E()V

    .line 457
    :cond_b
    return-void
.end method

.method private C()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 519
    iget-object v0, p0, LaQ/s;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bl;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaQ/s;->g:Ljava/lang/String;

    .line 521
    iget-object v0, p0, LaQ/s;->g:Ljava/lang/String;

    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 522
    const/16 v0, 0x57d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, LaQ/s;->g:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaQ/s;->g:Ljava/lang/String;

    .line 524
    iput-boolean v3, p0, LaQ/s;->f:Z

    .line 530
    :cond_2a
    :goto_2a
    return-void

    .line 525
    :cond_2b
    iget-object v0, p0, LaQ/s;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->w()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 526
    const/16 v0, 0x57c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, LaQ/s;->a:Lae/t;

    invoke-virtual {v2}, Lae/t;->x()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bl;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaQ/s;->g:Ljava/lang/String;

    goto :goto_2a
.end method

.method private E()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 533
    iget-object v0, p0, LaQ/s;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bl;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaQ/s;->h:Ljava/lang/String;

    .line 534
    iget-object v0, p0, LaQ/s;->h:Ljava/lang/String;

    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 535
    const/16 v0, 0x57e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, LaQ/s;->h:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaQ/s;->h:Ljava/lang/String;

    .line 537
    iput-boolean v3, p0, LaQ/s;->e:Z

    .line 542
    :cond_2a
    :goto_2a
    return-void

    .line 538
    :cond_2b
    iget-object v0, p0, LaQ/s;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->s()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 539
    const/16 v0, 0x57f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, LaQ/s;->a:Lae/t;

    invoke-virtual {v2}, Lae/t;->t()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bl;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaQ/s;->h:Ljava/lang/String;

    goto :goto_2a
.end method


# virtual methods
.method public D()Ljava/lang/String;
    .registers 2

    .prologue
    .line 492
    invoke-virtual {p0}, LaQ/s;->N()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public F()Z
    .registers 2

    .prologue
    .line 460
    iget-object v0, p0, LaQ/s;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->R()Z

    move-result v0

    return v0
.end method

.method public G()Ljava/lang/String;
    .registers 2

    .prologue
    .line 464
    iget-object v0, p0, LaQ/s;->a:Lae/t;

    invoke-static {v0}, LaQ/h;->a(Lae/t;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public H()Ljava/lang/String;
    .registers 3

    .prologue
    .line 478
    iget-object v0, p0, LaQ/s;->a:Lae/t;

    iget-object v1, p0, LaQ/s;->b:Lae/w;

    invoke-virtual {v0, v1}, Lae/t;->a(Lae/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public I()I
    .registers 3

    .prologue
    const/4 v0, -0x1

    .line 482
    iget-object v1, p0, LaQ/s;->d:Lae/v;

    iget v1, v1, Lae/v;->b:I

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    iget-object v0, p0, LaQ/s;->d:Lae/v;

    iget v0, v0, Lae/v;->b:I

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/rideabout/r;->c(I)I

    move-result v0

    goto :goto_7
.end method

.method public J()I
    .registers 3

    .prologue
    const/4 v0, -0x1

    .line 487
    iget-object v1, p0, LaQ/s;->d:Lae/v;

    iget v1, v1, Lae/v;->c:I

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    iget-object v0, p0, LaQ/s;->d:Lae/v;

    iget v0, v0, Lae/v;->c:I

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/rideabout/r;->c(I)I

    move-result v0

    goto :goto_7
.end method

.method public K()Ljava/lang/String;
    .registers 2

    .prologue
    .line 496
    iget-object v0, p0, LaQ/s;->d:Lae/v;

    iget-object v0, v0, Lae/v;->f:Ljava/lang/String;

    return-object v0
.end method

.method public L()Ljava/lang/String;
    .registers 2

    .prologue
    .line 510
    iget-object v0, p0, LaQ/s;->d:Lae/v;

    iget-object v0, v0, Lae/v;->j:Ljava/lang/String;

    return-object v0
.end method

.method protected M()Ljava/lang/String;
    .registers 2

    .prologue
    .line 545
    iget-object v0, p0, LaQ/s;->g:Ljava/lang/String;

    return-object v0
.end method

.method protected N()Ljava/lang/String;
    .registers 2

    .prologue
    .line 549
    iget-object v0, p0, LaQ/s;->h:Ljava/lang/String;

    return-object v0
.end method

.method public O()Z
    .registers 2

    .prologue
    .line 557
    iget-boolean v0, p0, LaQ/s;->e:Z

    return v0
.end method

.method public P()Z
    .registers 2

    .prologue
    .line 565
    iget-boolean v0, p0, LaQ/s;->f:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 501
    iget-object v0, p0, LaQ/s;->d:Lae/v;

    iget-object v0, v0, Lae/v;->e:Ljava/lang/String;

    return-object v0
.end method

.method public h()J
    .registers 3

    .prologue
    .line 506
    iget-object v0, p0, LaQ/s;->d:Lae/v;

    iget-wide v0, v0, Lae/v;->h:J

    return-wide v0
.end method

.method public w()Ljava/lang/String;
    .registers 3

    .prologue
    .line 469
    iget-object v0, p0, LaQ/s;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->Q()[Lae/u;

    move-result-object v0

    .line 470
    array-length v1, v0

    if-lez v1, :cond_11

    .line 471
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lae/u;->a()Ljava/lang/String;

    move-result-object v0

    .line 473
    :goto_10
    return-object v0

    :cond_11
    iget-object v0, p0, LaQ/s;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_10
.end method
