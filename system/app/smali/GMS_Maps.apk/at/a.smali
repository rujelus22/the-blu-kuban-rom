.class public LaT/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
.implements Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;


# static fields
.field static a:Ljava/lang/String;

.field private static f:LaT/a;


# instance fields
.field private final b:Ljava/util/List;

.field private volatile c:LaT/f;

.field private final d:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private final e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

.field private volatile g:Li/f;

.field private volatile h:I

.field private i:J

.field private final j:Ljava/util/List;

.field private volatile k:I

.field private final l:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private volatile m:Lo/aq;

.field private volatile n:I

.field private volatile o:Ljava/util/concurrent/CountDownLatch;

.field private volatile p:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 96
    const-string v0, "offlineAreaManagerStorage"

    sput-object v0, LaT/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;)V
    .registers 4
    .parameter

    .prologue
    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaT/a;->i:J

    .line 171
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 214
    iput-object p1, p0, LaT/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    .line 215
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaT/a;->b:Ljava/util/List;

    .line 216
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaT/a;->j:Ljava/util/List;

    .line 217
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 218
    invoke-direct {p0}, LaT/a;->r()V

    .line 219
    return-void
.end method

.method static synthetic a(LaT/a;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 90
    iput p1, p0, LaT/a;->h:I

    return p1
.end method

.method static synthetic a(LaT/a;J)J
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 90
    iput-wide p1, p0, LaT/a;->i:J

    return-wide p1
.end method

.method static synthetic a(LaT/a;)LaT/f;
    .registers 2
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, LaT/a;->c:LaT/f;

    return-object v0
.end method

.method static synthetic a(LaT/a;LaT/f;)LaT/f;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 90
    iput-object p1, p0, LaT/a;->c:LaT/f;

    return-object p1
.end method

.method static synthetic a(LaT/a;Lo/aq;)Lo/aq;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 90
    iput-object p1, p0, LaT/a;->m:Lo/aq;

    return-object p1
.end method

.method static synthetic a(LaT/a;LaT/l;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 90
    invoke-direct {p0, p1}, LaT/a;->a(LaT/l;)V

    return-void
.end method

.method private a(LaT/l;)V
    .registers 5
    .parameter

    .prologue
    .line 778
    iget-object v1, p0, LaT/a;->j:Ljava/util/List;

    monitor-enter v1

    .line 779
    :try_start_3
    iget-object v0, p0, LaT/a;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaT/m;

    .line 780
    invoke-interface {v0, p1}, LaT/m;->onOfflineDataUpdate(LaT/l;)V

    goto :goto_9

    .line 782
    :catchall_19
    move-exception v0

    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    throw v0

    :cond_1c
    :try_start_1c
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_1c .. :try_end_1d} :catchall_19

    .line 783
    return-void
.end method

.method public static declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;)V
    .registers 3
    .parameter

    .prologue
    .line 276
    const-class v1, LaT/a;

    monitor-enter v1

    :try_start_3
    sget-object v0, LaT/a;->f:LaT/a;

    if-nez v0, :cond_e

    .line 279
    new-instance v0, LaT/a;

    invoke-direct {v0, p0}, LaT/a;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;)V

    sput-object v0, LaT/a;->f:LaT/a;
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_10

    .line 281
    :cond_e
    monitor-exit v1

    return-void

    .line 276
    :catchall_10
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 820
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "t="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v1}, LaT/f;->e()Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, LaT/a;->u()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "c"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LaT/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 972
    const/4 v0, 0x0

    .line 973
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v1

    if-eqz v1, :cond_21

    .line 974
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v1

    if-eqz v1, :cond_21

    .line 975
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v0

    .line 980
    :cond_21
    if-eqz v0, :cond_29

    .line 985
    :goto_23
    const/16 v0, 0x79

    invoke-static {v0, p0, p1}, Lbm/m;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 988
    return-void

    .line 980
    :cond_29
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    const-string p1, "o"

    goto :goto_23

    :cond_32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "o"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_23
.end method

.method private a(Z)V
    .registers 13
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 674
    iput v2, p0, LaT/a;->n:I

    .line 675
    iget-object v0, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 677
    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget-object v1, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    new-array v1, v1, [LaT/f;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaT/f;

    .line 682
    if-eqz p1, :cond_1f

    iget-object v1, p0, LaT/a;->m:Lo/aq;

    if-nez v1, :cond_7c

    :cond_1f
    move v1, v3

    :goto_20
    move v4, v1

    .line 683
    :goto_21
    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-static {v1}, LaT/f;->c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->c()Lo/aq;

    move-result-object v6

    if-eqz v6, :cond_90

    .line 685
    iget-object v1, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_33
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_93

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaT/f;

    .line 688
    invoke-static {v1}, LaT/f;->b(LaT/f;)I

    move-result v7

    if-nez v7, :cond_33

    invoke-static {v1}, LaT/f;->c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v1

    invoke-interface {v1, v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->a(Lo/aq;)Z

    move-result v1

    if-eqz v1, :cond_33

    move v1, v3

    .line 694
    :goto_50
    if-nez v1, :cond_91

    .line 695
    array-length v7, v0

    move v5, v2

    :goto_54
    if-ge v5, v7, :cond_6a

    aget-object v8, v0, v5

    .line 699
    invoke-virtual {v8}, LaT/f;->a()I

    move-result v9

    const/4 v10, 0x3

    if-ne v9, v10, :cond_7e

    invoke-static {v8}, LaT/f;->c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v8

    invoke-interface {v8, v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->a(Lo/aq;)Z

    move-result v8

    if-eqz v8, :cond_7e

    move v1, v3

    .line 705
    :cond_6a
    if-nez v1, :cond_91

    .line 706
    iget v1, p0, LaT/a;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LaT/a;->n:I

    .line 707
    if-eqz v4, :cond_81

    .line 708
    iget-object v1, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, v6}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    move v1, v4

    :goto_7a
    move v4, v1

    .line 715
    goto :goto_21

    :cond_7c
    move v1, v2

    .line 682
    goto :goto_20

    .line 695
    :cond_7e
    add-int/lit8 v5, v5, 0x1

    goto :goto_54

    .line 709
    :cond_81
    iget-object v1, p0, LaT/a;->m:Lo/aq;

    invoke-virtual {v6, v1}, Lo/aq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_91

    .line 711
    iget-object v1, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, v6}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    move v1, v3

    goto :goto_7a

    .line 716
    :cond_90
    return-void

    :cond_91
    move v1, v4

    goto :goto_7a

    :cond_93
    move v1, v2

    goto :goto_50
.end method

.method static synthetic b(LaT/a;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 90
    iput p1, p0, LaT/a;->p:I

    return p1
.end method

.method private static b(I)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 885
    packed-switch p0, :pswitch_data_18

    .line 892
    :pswitch_3
    const/16 v0, 0x351

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_9
    return-object v0

    .line 888
    :pswitch_a
    const/16 v0, 0x354

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 890
    :pswitch_11
    const/16 v0, 0x33a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 885
    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_a
        :pswitch_11
        :pswitch_3
        :pswitch_3
        :pswitch_a
    .end packed-switch
.end method

.method static synthetic b(LaT/a;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, LaT/a;->b:Ljava/util/List;

    return-object v0
.end method

.method private static c(I)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 897
    packed-switch p0, :pswitch_data_1c

    .line 914
    const-string v0, "0"

    :goto_5
    return-object v0

    .line 899
    :pswitch_6
    const-string v0, "1"

    goto :goto_5

    .line 901
    :pswitch_9
    const-string v0, "2"

    goto :goto_5

    .line 903
    :pswitch_c
    const-string v0, "3"

    goto :goto_5

    .line 905
    :pswitch_f
    const-string v0, "4"

    goto :goto_5

    .line 907
    :pswitch_12
    const-string v0, "5"

    goto :goto_5

    .line 909
    :pswitch_15
    const-string v0, "6"

    goto :goto_5

    .line 911
    :pswitch_18
    const-string v0, "7"

    goto :goto_5

    .line 897
    nop

    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_18
    .end packed-switch
.end method

.method static synthetic c(LaT/a;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .registers 2
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic d(LaT/a;)I
    .registers 2
    .parameter

    .prologue
    .line 90
    iget v0, p0, LaT/a;->h:I

    return v0
.end method

.method static synthetic e(LaT/a;)Lo/aq;
    .registers 2
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, LaT/a;->m:Lo/aq;

    return-object v0
.end method

.method static synthetic f(LaT/a;)J
    .registers 3
    .parameter

    .prologue
    .line 90
    iget-wide v0, p0, LaT/a;->i:J

    return-wide v0
.end method

.method static synthetic g(LaT/a;)I
    .registers 2
    .parameter

    .prologue
    .line 90
    iget v0, p0, LaT/a;->p:I

    return v0
.end method

.method static synthetic h(LaT/a;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, LaT/a;->o:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method public static i()V
    .registers 2

    .prologue
    .line 249
    invoke-static {}, Lcom/google/googlenav/clientparam/f;->a()Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOfflineMapsEnabled()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 272
    :goto_a
    return-void

    .line 254
    :cond_b
    new-instance v0, LaT/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1}, LaT/b;-><init>(Las/c;)V

    invoke-virtual {v0}, LaT/b;->g()V

    goto :goto_a
.end method

.method static synthetic i(LaT/a;)[LaT/f;
    .registers 2
    .parameter

    .prologue
    .line 90
    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v0

    return-object v0
.end method

.method public static j()LaT/a;
    .registers 1

    .prologue
    .line 286
    sget-object v0, LaT/a;->f:LaT/a;

    if-nez v0, :cond_7

    .line 287
    sget-object v0, LaT/a;->f:LaT/a;

    .line 296
    :goto_6
    return-object v0

    .line 292
    :cond_7
    :try_start_7
    sget-object v0, LaT/a;->f:LaT/a;

    iget-object v0, v0, LaT/a;->o:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_e} :catch_11

    .line 296
    :goto_e
    sget-object v0, LaT/a;->f:LaT/a;

    goto :goto_6

    .line 293
    :catch_11
    move-exception v0

    goto :goto_e
.end method

.method static synthetic j(LaT/a;)[LaT/f;
    .registers 2
    .parameter

    .prologue
    .line 90
    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v0

    return-object v0
.end method

.method public static k()LaT/a;
    .registers 1

    .prologue
    .line 304
    sget-object v0, LaT/a;->f:LaT/a;

    return-object v0
.end method

.method static synthetic k(LaT/a;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 90
    invoke-direct {p0}, LaT/a;->y()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(LaT/a;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;
    .registers 2
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, LaT/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    return-object v0
.end method

.method static synthetic q()LaT/a;
    .registers 1

    .prologue
    .line 90
    sget-object v0, LaT/a;->f:LaT/a;

    return-object v0
.end method

.method private r()V
    .registers 4

    .prologue
    .line 376
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LaT/a;->o:Ljava/util/concurrent/CountDownLatch;

    .line 377
    new-instance v0, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, LaT/d;

    invoke-direct {v2, p0}, LaT/d;-><init>(LaT/a;)V

    invoke-direct {v0, v1, v2}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Las/b;->g()V

    .line 467
    return-void
.end method

.method private declared-synchronized s()[LaT/f;
    .registers 3

    .prologue
    .line 526
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaT/a;->b:Ljava/util/List;

    iget-object v1, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [LaT/f;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaT/f;
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    monitor-exit p0

    return-object v0

    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized t()[LaT/f;
    .registers 3

    .prologue
    .line 530
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget-object v1, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    new-array v1, v1, [LaT/f;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaT/f;
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    monitor-exit p0

    return-object v0

    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private u()I
    .registers 3

    .prologue
    .line 534
    iget-object v0, p0, LaT/a;->g:Li/f;

    if-nez v0, :cond_14

    const/4 v0, 0x0

    :goto_5
    iget-object v1, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_14
    const/4 v0, 0x1

    goto :goto_5
.end method

.method private v()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 719
    iput v2, p0, LaT/a;->h:I

    .line 720
    const/4 v0, -0x1

    iput v0, p0, LaT/a;->k:I

    .line 721
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    .line 722
    iget-object v0, p0, LaT/a;->c:LaT/f;

    invoke-static {v0, v2}, LaT/f;->a(LaT/f;I)I

    .line 723
    iget-object v0, p0, LaT/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    invoke-interface {v0, p0, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    .line 724
    return-void
.end method

.method private w()V
    .registers 3

    .prologue
    .line 727
    const/4 v0, 0x2

    iput v0, p0, LaT/a;->h:I

    .line 728
    const/4 v0, -0x1

    iput v0, p0, LaT/a;->k:I

    .line 729
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    .line 730
    invoke-virtual {p0}, LaT/a;->d()V

    .line 731
    iget-object v0, p0, LaT/a;->c:LaT/f;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LaT/f;->a(LaT/f;I)I

    .line 732
    iget-object v0, p0, LaT/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    invoke-interface {v0, p0, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    .line 733
    return-void
.end method

.method private declared-synchronized x()V
    .registers 4

    .prologue
    .line 806
    monitor-enter p0

    const/4 v0, 0x4

    :try_start_2
    iput v0, p0, LaT/a;->h:I

    .line 807
    const/4 v0, 0x0

    iput-object v0, p0, LaT/a;->c:LaT/f;

    .line 808
    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_21

    .line 809
    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaT/f;

    .line 810
    invoke-static {v0}, LaT/f;->b(LaT/f;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_23

    .line 811
    invoke-virtual {p0, v0}, LaT/a;->b(LaT/f;)Z
    :try_end_21
    .catchall {:try_start_2 .. :try_end_21} :catchall_27

    .line 816
    :cond_21
    :goto_21
    monitor-exit p0

    return-void

    .line 813
    :cond_23
    :try_start_23
    invoke-virtual {p0, v0}, LaT/a;->a(LaT/f;)I
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_27

    goto :goto_21

    .line 806
    :catchall_27
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private y()Ljava/lang/String;
    .registers 2

    .prologue
    .line 935
    iget v0, p0, LaT/a;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LaT/a;->p:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public H_()V
    .registers 3

    .prologue
    .line 872
    const/4 v0, 0x0

    iput-object v0, p0, LaT/a;->g:Li/f;

    .line 876
    iget v0, p0, LaT/a;->h:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_b

    .line 877
    const/4 v0, 0x0

    iput v0, p0, LaT/a;->h:I

    .line 879
    :cond_b
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    .line 880
    invoke-virtual {p0}, LaT/a;->m()V

    .line 882
    return-void
.end method

.method public declared-synchronized a(LaT/f;)I
    .registers 4
    .parameter

    .prologue
    .line 548
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 549
    invoke-direct {p0}, LaT/a;->u()I
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_50

    move-result v0

    const/4 v1, 0x6

    if-lt v0, v1, :cond_10

    .line 550
    const/4 v0, 0x1

    .line 567
    :goto_e
    monitor-exit p0

    return v0

    .line 552
    :cond_10
    const/4 v0, 0x2

    :try_start_11
    invoke-static {p1, v0}, LaT/f;->a(LaT/f;I)I

    .line 553
    iget v0, p0, LaT/a;->h:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_47

    .line 554
    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 560
    :goto_1e
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->a([LaT/f;)LaT/l;

    move-result-object v0

    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v0, v1}, LaT/l;->a(LaT/f;)LaT/l;

    move-result-object v0

    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->b([LaT/f;)LaT/l;

    move-result-object v0

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    .line 565
    invoke-virtual {p0}, LaT/a;->m()V

    .line 567
    const/4 v0, 0x0

    goto :goto_e

    .line 556
    :cond_47
    iput-object p1, p0, LaT/a;->c:LaT/f;

    .line 557
    invoke-virtual {p0}, LaT/a;->d()V

    .line 558
    invoke-direct {p0}, LaT/a;->v()V
    :try_end_4f
    .catchall {:try_start_11 .. :try_end_4f} :catchall_50

    goto :goto_1e

    .line 548
    :catchall_50
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 774
    iget-object v0, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    iput-object v0, p0, LaT/a;->m:Lo/aq;

    .line 775
    return-void
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 920
    iget-object v0, p0, LaT/a;->g:Li/f;

    if-eqz v0, :cond_c

    .line 921
    iget-object v0, p0, LaT/a;->g:Li/f;

    invoke-interface {v0}, Li/f;->a()V

    .line 922
    const/4 v0, 0x0

    iput-object v0, p0, LaT/a;->g:Li/f;

    .line 924
    :cond_c
    const/4 v0, 0x3

    iput v0, p0, LaT/a;->h:I

    .line 925
    const-string v0, "f"

    invoke-static {p1}, LaT/a;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LaT/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-static {p1}, LaT/a;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->a(Ljava/lang/String;)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    .line 929
    invoke-virtual {p0}, LaT/a;->m()V

    .line 931
    return-void
.end method

.method public a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 793
    sub-int v0, p1, p2

    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, p1

    .line 794
    iget v1, p0, LaT/a;->k:I

    if-eq v0, v1, :cond_2b

    .line 795
    new-instance v1, LaT/l;

    invoke-direct {v1}, LaT/l;-><init>()V

    iget v2, p0, LaT/a;->h:I

    invoke-virtual {v1, v2}, LaT/l;->a(I)LaT/l;

    move-result-object v1

    invoke-virtual {v1, v0}, LaT/l;->b(I)LaT/l;

    move-result-object v1

    sub-int v2, p1, p2

    mul-int/lit8 v2, v2, 0xf

    invoke-virtual {v1, v2}, LaT/l;->c(I)LaT/l;

    move-result-object v1

    mul-int/lit8 v2, p1, 0xf

    invoke-virtual {v1, v2}, LaT/l;->d(I)LaT/l;

    move-result-object v1

    invoke-direct {p0, v1}, LaT/a;->a(LaT/l;)V

    .line 800
    iput v0, p0, LaT/a;->k:I

    .line 803
    :cond_2b
    return-void
.end method

.method public a(LaT/m;)V
    .registers 5
    .parameter

    .prologue
    .line 487
    if-nez p1, :cond_3

    .line 510
    :goto_2
    return-void

    .line 491
    :cond_3
    iget-object v1, p0, LaT/a;->j:Ljava/util/List;

    monitor-enter v1

    .line 492
    :try_start_6
    iget-object v0, p0, LaT/a;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 493
    iget-object v0, p0, LaT/a;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 495
    :cond_13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_6 .. :try_end_14} :catchall_26

    .line 499
    new-instance v0, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, LaT/e;

    invoke-direct {v2, p0, p1}, LaT/e;-><init>(LaT/a;LaT/m;)V

    invoke-direct {v0, v1, v2}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Las/b;->g()V

    goto :goto_2

    .line 495
    :catchall_26
    move-exception v0

    :try_start_27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_27 .. :try_end_28} :catchall_26

    throw v0
.end method

.method public a(Li/f;)V
    .registers 2
    .parameter

    .prologue
    .line 787
    iput-object p1, p0, LaT/a;->g:Li/f;

    .line 789
    return-void
.end method

.method public a(Lo/aq;)Z
    .registers 3
    .parameter

    .prologue
    .line 747
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized b()V
    .registers 4

    .prologue
    .line 833
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, LaT/a;->g:Li/f;

    .line 834
    iget v0, p0, LaT/a;->h:I

    packed-switch v0, :pswitch_data_8c

    .line 861
    :goto_9
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->a([LaT/f;)LaT/l;

    move-result-object v0

    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v0, v1}, LaT/l;->a(LaT/f;)LaT/l;

    move-result-object v0

    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->b([LaT/f;)LaT/l;

    move-result-object v0

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    .line 866
    invoke-virtual {p0}, LaT/a;->m()V
    :try_end_30
    .catchall {:try_start_2 .. :try_end_30} :catchall_6e

    .line 868
    monitor-exit p0

    return-void

    .line 838
    :pswitch_32
    :try_start_32
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, LaT/l;->b(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    .line 840
    iget-object v0, p0, LaT/a;->c:LaT/f;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LaT/f;->a(LaT/f;I)I

    .line 841
    iget-object v0, p0, LaT/a;->c:LaT/f;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LaT/f;->b(J)V

    .line 845
    iget-object v0, p0, LaT/a;->b:Ljava/util/List;

    const/4 v1, 0x0

    iget-object v2, p0, LaT/a;->c:LaT/f;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 846
    const-string v0, "t"

    invoke-direct {p0, v0}, LaT/a;->a(Ljava/lang/String;)V

    .line 847
    invoke-direct {p0}, LaT/a;->x()V
    :try_end_6d
    .catchall {:try_start_32 .. :try_end_6d} :catchall_6e

    goto :goto_9

    .line 833
    :catchall_6e
    move-exception v0

    monitor-exit p0

    throw v0

    .line 850
    :pswitch_71
    :try_start_71
    iget-object v0, p0, LaT/a;->c:LaT/f;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LaT/f;->a(LaT/f;I)I

    .line 853
    iget-object v0, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v0}, LaT/f;->g()Ljava/io/File;

    move-result-object v0

    .line 854
    if-eqz v0, :cond_82

    .line 855
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 857
    :cond_82
    const-string v0, "l"

    invoke-direct {p0, v0}, LaT/a;->a(Ljava/lang/String;)V

    .line 858
    invoke-direct {p0}, LaT/a;->x()V
    :try_end_8a
    .catchall {:try_start_71 .. :try_end_8a} :catchall_6e

    goto/16 :goto_9

    .line 834
    :pswitch_data_8c
    .packed-switch 0x1
        :pswitch_32
        :pswitch_71
    .end packed-switch
.end method

.method public b(LaT/m;)V
    .registers 4
    .parameter

    .prologue
    .line 516
    if-nez p1, :cond_3

    .line 523
    :goto_2
    return-void

    .line 520
    :cond_3
    iget-object v1, p0, LaT/a;->j:Ljava/util/List;

    monitor-enter v1

    .line 521
    :try_start_6
    iget-object v0, p0, LaT/a;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 522
    monitor-exit v1

    goto :goto_2

    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public declared-synchronized b(LaT/f;)Z
    .registers 4
    .parameter

    .prologue
    .line 578
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaT/a;->c:LaT/f;

    if-ne p1, v0, :cond_b

    .line 579
    invoke-virtual {p0}, LaT/a;->p()Z
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_4d

    move-result v0

    .line 596
    :goto_9
    monitor-exit p0

    return v0

    .line 581
    :cond_b
    :try_start_b
    iget-object v0, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 582
    const/4 v0, 0x3

    invoke-static {p1, v0}, LaT/f;->a(LaT/f;I)I

    .line 583
    iget v0, p0, LaT/a;->h:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_47

    .line 584
    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 589
    :goto_1e
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->a([LaT/f;)LaT/l;

    move-result-object v0

    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v0, v1}, LaT/l;->a(LaT/f;)LaT/l;

    move-result-object v0

    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->b([LaT/f;)LaT/l;

    move-result-object v0

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    .line 594
    invoke-virtual {p0}, LaT/a;->m()V

    .line 596
    const/4 v0, 0x1

    goto :goto_9

    .line 586
    :cond_47
    iput-object p1, p0, LaT/a;->c:LaT/f;

    .line 587
    invoke-direct {p0}, LaT/a;->w()V
    :try_end_4c
    .catchall {:try_start_b .. :try_end_4c} :catchall_4d

    goto :goto_1e

    .line 578
    :catchall_4d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Lo/aq;
    .registers 2

    .prologue
    .line 757
    iget-object v0, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    return-object v0
.end method

.method public declared-synchronized c(LaT/f;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 603
    monitor-enter p0

    :try_start_3
    iget-object v2, p0, LaT/a;->c:LaT/f;

    if-ne p1, v2, :cond_d

    .line 604
    invoke-virtual {p0}, LaT/a;->p()Z
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_4f

    move-result v0

    .line 632
    :goto_b
    monitor-exit p0

    return v0

    .line 606
    :cond_d
    :try_start_d
    iget-object v2, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_52

    .line 607
    iget-object v1, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 608
    invoke-virtual {p1}, LaT/f;->a()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2a

    .line 609
    const/4 v1, 0x0

    invoke-static {p1, v1}, LaT/f;->a(LaT/f;I)I

    .line 610
    iget-object v1, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 612
    :cond_2a
    new-instance v1, LaT/l;

    invoke-direct {v1}, LaT/l;-><init>()V

    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v2

    invoke-virtual {v1, v2}, LaT/l;->a([LaT/f;)LaT/l;

    move-result-object v1

    iget-object v2, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v1, v2}, LaT/l;->a(LaT/f;)LaT/l;

    move-result-object v1

    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v2

    invoke-virtual {v1, v2}, LaT/l;->b([LaT/f;)LaT/l;

    move-result-object v1

    iget v2, p0, LaT/a;->h:I

    invoke-virtual {v1, v2}, LaT/l;->a(I)LaT/l;

    move-result-object v1

    invoke-direct {p0, v1}, LaT/a;->a(LaT/l;)V
    :try_end_4e
    .catchall {:try_start_d .. :try_end_4e} :catchall_4f

    goto :goto_b

    .line 603
    :catchall_4f
    move-exception v0

    monitor-exit p0

    throw v0

    .line 622
    :cond_52
    :try_start_52
    invoke-virtual {p1}, LaT/f;->a()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8b

    iget-object v2, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8b

    .line 624
    iget-object v1, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 625
    new-instance v1, LaT/l;

    invoke-direct {v1}, LaT/l;-><init>()V

    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v2

    invoke-virtual {v1, v2}, LaT/l;->a([LaT/f;)LaT/l;

    move-result-object v1

    iget-object v2, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v1, v2}, LaT/l;->a(LaT/f;)LaT/l;

    move-result-object v1

    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v2

    invoke-virtual {v1, v2}, LaT/l;->b([LaT/f;)LaT/l;

    move-result-object v1

    iget v2, p0, LaT/a;->h:I

    invoke-virtual {v1, v2}, LaT/l;->a(I)LaT/l;

    move-result-object v1

    invoke-direct {p0, v1}, LaT/a;->a(LaT/l;)V
    :try_end_8a
    .catchall {:try_start_52 .. :try_end_8a} :catchall_4f

    goto :goto_b

    :cond_8b
    move v0, v1

    .line 632
    goto/16 :goto_b
.end method

.method public declared-synchronized d()V
    .registers 2

    .prologue
    .line 767
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaT/a;->c:LaT/f;

    invoke-static {v0}, LaT/f;->c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->d()V

    .line 768
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaT/a;->a(Z)V

    .line 769
    const/4 v0, 0x0

    iput-object v0, p0, LaT/a;->m:Lo/aq;
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    .line 770
    monitor-exit p0

    return-void

    .line 767
    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 737
    iget-object v0, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    return v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 742
    iget-object v0, p0, LaT/a;->c:LaT/f;

    invoke-static {v0}, LaT/f;->c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->f()I

    move-result v0

    return v0
.end method

.method public g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 752
    const/4 v0, 0x0

    return-object v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 762
    iget v0, p0, LaT/a;->n:I

    return v0
.end method

.method public declared-synchronized l()V
    .registers 3

    .prologue
    .line 312
    monitor-enter p0

    const/4 v0, 0x4

    :try_start_2
    iput v0, p0, LaT/a;->h:I

    .line 313
    invoke-virtual {p0}, LaT/a;->n()V

    .line 315
    const/4 v0, 0x0

    iput-object v0, p0, LaT/a;->c:LaT/f;

    .line 316
    iget-object v0, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 317
    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 318
    invoke-virtual {p0}, LaT/a;->m()V

    .line 319
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->a([LaT/f;)LaT/l;

    move-result-object v0

    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v0, v1}, LaT/l;->a(LaT/f;)LaT/l;

    move-result-object v0

    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->b([LaT/f;)LaT/l;

    move-result-object v0

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V
    :try_end_3b
    .catchall {:try_start_2 .. :try_end_3b} :catchall_3d

    .line 324
    monitor-exit p0

    return-void

    .line 312
    :catchall_3d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method m()V
    .registers 3

    .prologue
    .line 333
    new-instance v0, LaT/c;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LaT/c;-><init>(LaT/a;Las/c;)V

    invoke-virtual {v0}, LaT/c;->g()V

    .line 370
    return-void
.end method

.method public n()V
    .registers 3

    .prologue
    .line 473
    iget-object v0, p0, LaT/a;->g:Li/f;

    if-eqz v0, :cond_b

    .line 476
    iget-object v0, p0, LaT/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    iget-object v1, p0, LaT/a;->g:Li/f;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;->a(Li/f;)V

    .line 479
    :cond_b
    return-void
.end method

.method public declared-synchronized o()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 641
    monitor-enter p0

    :try_start_2
    iget v1, p0, LaT/a;->h:I

    if-eqz v1, :cond_e

    iget v1, p0, LaT/a;->h:I
    :try_end_8
    .catchall {:try_start_2 .. :try_end_8} :catchall_22

    const/4 v2, 0x3

    if-eq v1, v2, :cond_e

    .line 642
    const/4 v0, 0x0

    .line 649
    :goto_c
    monitor-exit p0

    return v0

    .line 644
    :cond_e
    :try_start_e
    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-static {v1}, LaT/f;->c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->d()V

    .line 645
    const/4 v1, 0x1

    invoke-direct {p0, v1}, LaT/a;->a(Z)V

    .line 646
    invoke-direct {p0}, LaT/a;->v()V

    .line 647
    invoke-virtual {p0}, LaT/a;->m()V
    :try_end_21
    .catchall {:try_start_e .. :try_end_21} :catchall_22

    goto :goto_c

    .line 641
    :catchall_22
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized p()Z
    .registers 3

    .prologue
    .line 659
    monitor-enter p0

    :try_start_1
    iget v0, p0, LaT/a;->h:I

    if-eqz v0, :cond_d

    iget v0, p0, LaT/a;->h:I
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_15

    const/4 v1, 0x3

    if-eq v0, v1, :cond_d

    .line 660
    const/4 v0, 0x0

    .line 665
    :goto_b
    monitor-exit p0

    return v0

    .line 662
    :cond_d
    :try_start_d
    invoke-direct {p0}, LaT/a;->w()V

    .line 663
    invoke-virtual {p0}, LaT/a;->m()V
    :try_end_13
    .catchall {:try_start_d .. :try_end_13} :catchall_15

    .line 665
    const/4 v0, 0x1

    goto :goto_b

    .line 659
    :catchall_15
    move-exception v0

    monitor-exit p0

    throw v0
.end method
