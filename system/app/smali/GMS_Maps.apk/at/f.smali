.class public LaT/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/friend/bf;


# static fields
.field private static final h:I

.field private static final i:I


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Landroid/graphics/Bitmap;

.field private f:Z

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/16 v1, 0x40

    .line 1007
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, LaT/f;->h:I

    .line 1012
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, LaT/f;->i:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Ljava/lang/String;ILandroid/graphics/Bitmap;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1017
    iput-object p1, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    .line 1018
    iput-object p2, p0, LaT/f;->b:Ljava/lang/String;

    .line 1019
    iput p3, p0, LaT/f;->c:I

    .line 1020
    const/4 v0, 0x0

    iput-object v0, p0, LaT/f;->e:Landroid/graphics/Bitmap;

    .line 1021
    if-eqz p4, :cond_39

    .line 1022
    new-instance v0, Lan/f;

    invoke-direct {v0, p4}, Lan/f;-><init>(Landroid/graphics/Bitmap;)V

    sget v1, LaT/f;->i:I

    sget v2, LaT/f;->h:I

    invoke-static {v0, v1, v2}, Lam/j;->a(Lam/f;II)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    .line 1028
    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eq p4, v1, :cond_26

    .line 1029
    invoke-virtual {p4}, Landroid/graphics/Bitmap;->recycle()V

    .line 1031
    :cond_26
    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, LaT/f;->e:Landroid/graphics/Bitmap;

    .line 1034
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, LaT/g;

    invoke-direct {v2, p0, v0}, LaT/g;-><init>(LaT/f;Lan/f;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1053
    :cond_39
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Ljava/lang/String;ILjava/lang/String;J)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1062
    iput-object p1, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    .line 1063
    iput-object p2, p0, LaT/f;->b:Ljava/lang/String;

    .line 1064
    iput p3, p0, LaT/f;->c:I

    .line 1065
    iput-object p4, p0, LaT/f;->d:Ljava/lang/String;

    .line 1068
    const/4 v0, 0x0

    iput-object v0, p0, LaT/f;->e:Landroid/graphics/Bitmap;

    .line 1069
    iput-wide p5, p0, LaT/f;->g:J

    .line 1070
    return-void
.end method

.method static synthetic a(LaT/f;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 993
    iput p1, p0, LaT/f;->c:I

    return p1
.end method

.method static synthetic a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaT/f;
    .registers 2
    .parameter

    .prologue
    .line 993
    invoke-static {p0}, LaT/f;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaT/f;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LaT/f;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 993
    invoke-direct {p0}, LaT/f;->i()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LaT/f;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 993
    iput-object p1, p0, LaT/f;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(LaT/f;)I
    .registers 2
    .parameter

    .prologue
    .line 993
    iget v0, p0, LaT/f;->c:I

    return v0
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x4

    .line 1099
    const/4 v0, 0x0

    invoke-static {p0, v2, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v1

    .line 1101
    const/4 v0, 0x1

    invoke-static {p0, v2, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    .line 1107
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 1112
    :goto_11
    return-object v0

    .line 1109
    :cond_12
    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1a

    move-object v0, v1

    .line 1110
    goto :goto_11

    .line 1112
    :cond_1a
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaT/f;
    .registers 8
    .parameter

    .prologue
    .line 1198
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    new-instance v0, LaT/f;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/prefetch/android/k;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/prefetch/android/k;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    const/4 v4, 0x4

    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x5

    invoke-static {p0, v5}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v5

    invoke-direct/range {v0 .. v6}, LaT/f;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Ljava/lang/String;ILjava/lang/String;J)V

    goto :goto_3
.end method

.method static synthetic c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
    .registers 2
    .parameter

    .prologue
    .line 993
    iget-object v0, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    return-object v0
.end method

.method private i()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 1187
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbM/r;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1188
    const/4 v1, 0x1

    iget-object v2, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1190
    const/4 v1, 0x2

    iget-object v2, p0, LaT/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1191
    const/4 v1, 0x3

    iget v2, p0, LaT/f;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1192
    const/4 v1, 0x4

    iget-object v2, p0, LaT/f;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1193
    const/4 v1, 0x5

    iget-wide v2, p0, LaT/f;->g:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1194
    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 1056
    iget v0, p0, LaT/f;->c:I

    return v0
.end method

.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 1143
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .registers 2
    .parameter

    .prologue
    .line 1183
    iput-object p1, p0, LaT/f;->e:Landroid/graphics/Bitmap;

    .line 1184
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1123
    iget-boolean v0, p0, LaT/f;->f:Z

    if-eqz v0, :cond_5

    .line 1138
    :cond_4
    :goto_4
    return-void

    .line 1126
    :cond_5
    const/16 v0, 0x9

    invoke-static {p3, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    .line 1130
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 1131
    invoke-static {p1}, LaT/f;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    .line 1134
    :cond_15
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1135
    invoke-virtual {p0, v0}, LaT/f;->a(Ljava/lang/String;)V

    .line 1136
    invoke-static {}, LaT/a;->q()LaT/a;

    move-result-object v0

    new-instance v1, LaT/l;

    invoke-direct {v1}, LaT/l;-><init>()V

    invoke-static {v0, v1}, LaT/a;->a(LaT/a;LaT/l;)V

    goto :goto_4
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1158
    const/4 v0, 0x1

    iput-boolean v0, p0, LaT/f;->f:Z

    .line 1159
    iput-object p1, p0, LaT/f;->b:Ljava/lang/String;

    .line 1160
    return-void
.end method

.method public b()V
    .registers 7

    .prologue
    const/4 v5, 0x1

    .line 1076
    iget-object v0, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    check-cast v0, Lcom/google/googlenav/prefetch/android/k;

    .line 1077
    invoke-virtual {v0}, Lcom/google/googlenav/prefetch/android/k;->k()LaN/B;

    move-result-object v1

    .line 1079
    const/16 v2, 0x57

    const-string v3, "stp"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 1084
    new-instance v3, Lcom/google/googlenav/friend/bg;

    invoke-direct {v3}, Lcom/google/googlenav/friend/bg;-><init>()V

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/friend/bg;->a(I)Lcom/google/googlenav/friend/bg;

    move-result-object v3

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    invoke-virtual {v3, v1}, Lcom/google/googlenav/friend/bg;->b(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/prefetch/android/k;->i()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/friend/bg;->d(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/prefetch/android/k;->j()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/friend/bg;->e(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->h(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->k()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->c(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/friend/bg;->d(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/friend/bg;->e(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    .line 1095
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/friend/bg;->a()Lcom/google/googlenav/friend/be;

    move-result-object v0

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    .line 1096
    return-void
.end method

.method public b(J)V
    .registers 3
    .parameter

    .prologue
    .line 1146
    iput-wide p1, p0, LaT/f;->g:J

    .line 1147
    return-void
.end method

.method public c()J
    .registers 3

    .prologue
    .line 1150
    iget-wide v0, p0, LaT/f;->g:J

    return-wide v0
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1154
    iget-object v0, p0, LaT/f;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
    .registers 2

    .prologue
    .line 1163
    iget-object v0, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1209
    instance-of v1, p1, LaT/f;

    if-nez v1, :cond_6

    .line 1213
    :cond_5
    :goto_5
    return v0

    .line 1212
    :cond_6
    check-cast p1, LaT/f;

    .line 1213
    iget-object v1, p0, LaT/f;->b:Ljava/lang/String;

    invoke-virtual {p1}, LaT/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    iget-object v2, p1, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget v1, p0, LaT/f;->c:I

    iget v2, p1, LaT/f;->c:I

    if-ne v1, v2, :cond_5

    iget-wide v1, p0, LaT/f;->g:J

    iget-wide v3, p1, LaT/f;->g:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_5

    iget-object v1, p0, LaT/f;->d:Ljava/lang/String;

    iget-object v2, p1, LaT/f;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1167
    iget-object v0, p0, LaT/f;->d:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/io/File;
    .registers 4

    .prologue
    .line 1171
    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 1172
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaT/f;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1175
    :goto_20
    return-object v0

    .line 1174
    :cond_21
    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    invoke-static {v0}, LaT/a;->l(LaT/a;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;->d()Landroid/content/Context;

    move-result-object v0

    .line 1175
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OfflineMapArea_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaT/f;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_20
.end method

.method public h()Landroid/graphics/Bitmap;
    .registers 2

    .prologue
    .line 1179
    iget-object v0, p0, LaT/f;->e:Landroid/graphics/Bitmap;

    return-object v0
.end method
