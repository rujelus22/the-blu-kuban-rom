.class public final enum Lt/v;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lt/v;

.field public static final enum b:Lt/v;

.field public static final enum c:Lt/v;

.field public static final enum d:Lt/v;

.field public static final enum e:Lt/v;

.field public static final enum f:Lt/v;

.field public static final enum g:Lt/v;

.field public static final enum h:Lt/v;

.field public static final enum i:Lt/v;

.field public static final enum j:Lt/v;

.field public static final enum k:Lt/v;

.field public static final enum l:Lt/v;

.field private static final synthetic n:[Lt/v;


# instance fields
.field private final m:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 138
    new-instance v0, Lt/v;

    const-string v1, "START"

    const-class v2, Lt/m;

    invoke-direct {v0, v1, v4, v2}, Lt/v;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lt/v;->a:Lt/v;

    .line 139
    new-instance v0, Lt/v;

    const-string v1, "WAIT_FOR_LOCATION"

    const-class v2, Lt/z;

    invoke-direct {v0, v1, v5, v2}, Lt/v;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lt/v;->b:Lt/v;

    .line 140
    new-instance v0, Lt/v;

    const-string v1, "WAIT_FOR_NAV_AVAILABILITY"

    const-class v2, Lt/A;

    invoke-direct {v0, v1, v6, v2}, Lt/v;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lt/v;->c:Lt/v;

    .line 141
    new-instance v0, Lt/v;

    const-string v1, "WAIT_FOR_DIRECTIONS"

    const-class v2, Lt/x;

    invoke-direct {v0, v1, v7, v2}, Lt/v;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lt/v;->d:Lt/v;

    .line 142
    new-instance v0, Lt/v;

    const-string v1, "WAIT_FOR_ACTIVITY"

    const-class v2, Lt/w;

    invoke-direct {v0, v1, v8, v2}, Lt/v;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lt/v;->e:Lt/v;

    .line 143
    new-instance v0, Lt/v;

    const-string v1, "ROUTE_OVERVIEW"

    const/4 v2, 0x5

    const-class v3, Lt/k;

    invoke-direct {v0, v1, v2, v3}, Lt/v;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lt/v;->f:Lt/v;

    .line 144
    new-instance v0, Lt/v;

    const-string v1, "LIST_VIEW"

    const/4 v2, 0x6

    const-class v3, Lt/j;

    invoke-direct {v0, v1, v2, v3}, Lt/v;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lt/v;->g:Lt/v;

    .line 145
    new-instance v0, Lt/v;

    const-string v1, "FOLLOW_LOCATION"

    const/4 v2, 0x7

    const-class v3, Lt/a;

    invoke-direct {v0, v1, v2, v3}, Lt/v;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lt/v;->h:Lt/v;

    .line 146
    new-instance v0, Lt/v;

    const-string v1, "FREE_MOVEMENT"

    const/16 v2, 0x8

    const-class v3, Lt/c;

    invoke-direct {v0, v1, v2, v3}, Lt/v;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lt/v;->i:Lt/v;

    .line 147
    new-instance v0, Lt/v;

    const-string v1, "INSPECT_STEP_MAP"

    const/16 v2, 0x9

    const-class v3, Lt/e;

    invoke-direct {v0, v1, v2, v3}, Lt/v;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lt/v;->j:Lt/v;

    .line 148
    new-instance v0, Lt/v;

    const-string v1, "INSPECT_TRAFFIC"

    const/16 v2, 0xa

    const-class v3, Lt/g;

    invoke-direct {v0, v1, v2, v3}, Lt/v;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lt/v;->k:Lt/v;

    .line 149
    new-instance v0, Lt/v;

    const-string v1, "INSPECT_NAVIGATION_IMAGE"

    const/16 v2, 0xb

    const-class v3, Lt/d;

    invoke-direct {v0, v1, v2, v3}, Lt/v;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lt/v;->l:Lt/v;

    .line 137
    const/16 v0, 0xc

    new-array v0, v0, [Lt/v;

    sget-object v1, Lt/v;->a:Lt/v;

    aput-object v1, v0, v4

    sget-object v1, Lt/v;->b:Lt/v;

    aput-object v1, v0, v5

    sget-object v1, Lt/v;->c:Lt/v;

    aput-object v1, v0, v6

    sget-object v1, Lt/v;->d:Lt/v;

    aput-object v1, v0, v7

    sget-object v1, Lt/v;->e:Lt/v;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lt/v;->f:Lt/v;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lt/v;->g:Lt/v;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lt/v;->h:Lt/v;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lt/v;->i:Lt/v;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lt/v;->j:Lt/v;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lt/v;->k:Lt/v;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lt/v;->l:Lt/v;

    aput-object v2, v0, v1

    sput-object v0, Lt/v;->n:[Lt/v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 154
    iput-object p3, p0, Lt/v;->m:Ljava/lang/Class;

    .line 155
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lt/v;
    .registers 2
    .parameter

    .prologue
    .line 137
    const-class v0, Lt/v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lt/v;

    return-object v0
.end method

.method public static values()[Lt/v;
    .registers 1

    .prologue
    .line 137
    sget-object v0, Lt/v;->n:[Lt/v;

    invoke-virtual {v0}, [Lt/v;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lt/v;

    return-object v0
.end method


# virtual methods
.method a(Lt/n;)Lt/q;
    .registers 4
    .parameter

    .prologue
    .line 162
    :try_start_0
    iget-object v0, p0, Lt/v;->m:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/q;

    .line 163
    invoke-static {v0, p0}, Lt/q;->a(Lt/q;Lt/v;)Lt/v;

    .line 164
    iput-object p1, v0, Lt/q;->a:Lt/n;
    :try_end_d
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_d} :catch_e
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_d} :catch_15

    .line 165
    return-object v0

    .line 166
    :catch_e
    move-exception v0

    .line 167
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 168
    :catch_15
    move-exception v0

    .line 169
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
