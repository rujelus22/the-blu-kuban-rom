.class public Lt/c;
.super Lt/q;
.source "SourceFile"


# instance fields
.field private c:Lcom/google/android/maps/driveabout/vector/l;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 19
    invoke-direct {p0}, Lt/q;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lt/c;->d:Z

    return-void
.end method


# virtual methods
.method protected a(FF)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-virtual {p0}, Lt/c;->aa()V

    .line 67
    return-void
.end method

.method protected a(FFF)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 71
    invoke-virtual {p0}, Lt/c;->aa()V

    .line 72
    return-void
.end method

.method protected a(Landroid/os/Bundle;Lo/ab;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-static {p1, v0, p2}, Lcom/google/android/maps/driveabout/app/SearchActivity;->a(Landroid/os/Bundle;Lcom/google/android/maps/driveabout/app/aQ;Lo/ab;)V

    .line 113
    return-void
.end method

.method protected a(Lp/J;Lp/J;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 82
    if-eqz p1, :cond_e

    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->b()Lp/J;

    move-result-object v0

    if-ne p1, v0, :cond_11

    .line 83
    :cond_e
    invoke-virtual {p0, p2}, Lt/c;->a(Lp/J;)V

    .line 85
    :cond_11
    return-void
.end method

.method protected a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 57
    iget-boolean v0, p0, Lt/c;->d:Z

    if-eqz v0, :cond_1a

    .line 58
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->a()Lcom/google/android/maps/driveabout/app/cn;

    move-result-object v0

    iget-object v1, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cn;->a(Lcom/google/android/maps/driveabout/app/aQ;)V

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lt/c;->d:Z

    .line 62
    :cond_1a
    return-void
.end method

.method public b()V
    .registers 6

    .prologue
    const/4 v2, 0x1

    .line 25
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/maps/driveabout/app/cN;->setViewMode(I)V

    .line 26
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->p()V

    .line 27
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->D()V

    .line 28
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->w()V

    .line 29
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->z()Z

    move-result v0

    if-eqz v0, :cond_53

    .line 30
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v1

    new-array v2, v2, [Lp/y;

    const/4 v3, 0x0

    iget-object v4, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v4}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/cN;->b(Lp/y;[Lp/y;)V

    .line 34
    :cond_53
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->b()Lp/J;

    move-result-object v0

    invoke-virtual {p0, v0}, Lt/c;->a(Lp/J;)V

    .line 35
    invoke-virtual {p0}, Lt/c;->aa()V

    .line 36
    return-void
.end method

.method public e()V
    .registers 3

    .prologue
    .line 40
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->q()V

    .line 41
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->f(Z)V

    .line 42
    return-void
.end method

.method protected g()V
    .registers 2

    .prologue
    .line 76
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->f()V

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt/c;->d:Z

    .line 78
    return-void
.end method

.method public p()V
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->a()Lcom/google/android/maps/driveabout/app/cn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cn;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    iput-object v0, p0, Lt/c;->c:Lcom/google/android/maps/driveabout/vector/l;

    .line 48
    return-void
.end method

.method public q()V
    .registers 3

    .prologue
    .line 52
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->c()Lcom/google/android/maps/driveabout/app/cP;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cP;->a()Lcom/google/android/maps/driveabout/app/cn;

    move-result-object v0

    iget-object v1, p0, Lt/c;->c:Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cn;->a(Lcom/google/android/maps/driveabout/vector/m;)V

    .line 53
    return-void
.end method

.method protected r()V
    .registers 4

    .prologue
    .line 89
    iget-object v0, p0, Lt/c;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->w()V

    .line 90
    iget-object v0, p0, Lt/c;->a:Lt/n;

    sget-object v1, Lt/v;->h:Lt/v;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    .line 91
    return-void
.end method

.method protected s()V
    .registers 4

    .prologue
    .line 100
    iget-object v0, p0, Lt/c;->a:Lt/n;

    sget-object v1, Lt/v;->h:Lt/v;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    .line 101
    return-void
.end method

.method protected t()V
    .registers 4

    .prologue
    .line 105
    iget-object v0, p0, Lt/c;->a:Lt/n;

    sget-object v1, Lt/v;->h:Lt/v;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    .line 106
    return-void
.end method
