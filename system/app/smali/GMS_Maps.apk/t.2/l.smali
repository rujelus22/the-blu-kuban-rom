.class Lt/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/app/aH;


# instance fields
.field final synthetic a:[Lp/b;

.field final synthetic b:Lt/k;


# direct methods
.method constructor <init>(Lt/k;[Lp/b;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 336
    iput-object p1, p0, Lt/l;->b:Lt/k;

    iput-object p2, p0, Lt/l;->a:[Lp/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a([Lp/b;)V
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 339
    .line 340
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v0

    .line 341
    :goto_8
    iget-object v4, p0, Lt/l;->a:[Lp/b;

    array-length v4, v4

    if-ge v0, v4, :cond_4e

    .line 342
    iget-object v4, p0, Lt/l;->a:[Lp/b;

    aget-object v4, v4, v0

    aget-object v5, p1, v0

    invoke-virtual {v4, v5}, Lp/b;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4b

    .line 344
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_24

    .line 345
    const-string v1, ","

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    :cond_24
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lp/b;->b()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lp/b;->c()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v2

    .line 341
    :cond_4b
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 350
    :cond_4e
    if-eqz v1, :cond_91

    .line 351
    const-string v0, "R"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dm;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    new-instance v0, Ll/D;

    const-string v1, "changedRouteOptions"

    invoke-static {p1}, Lp/c;->c([Lp/b;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Ll/D;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, LB/f;->b(LB/j;)V

    .line 355
    iget-object v0, p0, Lt/l;->b:Lt/k;

    invoke-static {v0, v2}, Lt/k;->a(Lt/k;Z)Z

    .line 356
    iget-object v0, p0, Lt/l;->b:Lt/k;

    const v1, 0x7f0d00d0

    invoke-virtual {v0, v1}, Lt/k;->c(I)V

    .line 361
    iget-object v0, p0, Lt/l;->b:Lt/k;

    iget-object v0, v0, Lt/k;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->f()Lp/s;

    move-result-object v0

    iget-object v1, p0, Lt/l;->b:Lt/k;

    iget-object v1, v1, Lt/k;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->b()Ln/b;

    move-result-object v1

    iget-object v2, p0, Lt/l;->b:Lt/k;

    invoke-static {v2}, Lt/k;->a(Lt/k;)Lp/y;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lp/s;->a(Ln/b;Lp/y;[Lp/b;)V

    .line 365
    :cond_91
    return-void
.end method
