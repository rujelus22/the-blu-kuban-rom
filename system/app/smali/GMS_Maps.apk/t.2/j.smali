.class public Lt/j;
.super Lt/q;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 14
    invoke-direct {p0}, Lt/q;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/maps/driveabout/app/bP;)V
    .registers 3
    .parameter

    .prologue
    .line 66
    invoke-super {p0, p1}, Lt/q;->a(Lcom/google/android/maps/driveabout/app/bP;)V

    .line 67
    const v0, 0x7f10011b

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->b(I)V

    .line 68
    const v0, 0x7f1004a8

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/bP;->a(I)V

    .line 69
    return-void
.end method

.method protected a(Lp/J;Lp/J;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/maps/driveabout/app/cN;->setCurrentDirectionsListStep(Lp/J;)V

    .line 47
    return-void
.end method

.method protected a(Lp/y;[Lp/y;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Lt/q;->a(Lp/y;[Lp/y;)V

    .line 39
    iget-object v0, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->a(Lp/y;)Z

    .line 41
    iget-object v0, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->B()V

    .line 42
    return-void
.end method

.method protected a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 33
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 17
    iget-object v0, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->w()V

    .line 18
    iget-object v0, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->C()V

    .line 19
    iget-object v0, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/aQ;->l()Lp/y;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->a(Lp/y;)Z

    .line 21
    iget-object v0, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->k()Lp/J;

    move-result-object v0

    .line 22
    if-eqz v0, :cond_45

    .line 23
    iget-object v1, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/maps/driveabout/app/cN;->setCurrentDirectionsListStep(Lp/J;)V

    .line 27
    :goto_3a
    iget-object v0, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->setViewMode(I)V

    .line 28
    return-void

    .line 25
    :cond_45
    iget-object v0, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/cN;->B()V

    goto :goto_3a
.end method

.method protected r()V
    .registers 4

    .prologue
    .line 51
    iget-object v0, p0, Lt/j;->a:Lt/n;

    sget-object v1, Lt/v;->h:Lt/v;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lt/n;->a(Lt/v;Z)Z

    .line 52
    return-void
.end method

.method protected u()V
    .registers 3

    .prologue
    .line 56
    iget-object v0, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    iget-object v1, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v1}, Lt/n;->e()Lcom/google/android/maps/driveabout/app/aQ;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->a(Lcom/google/android/maps/driveabout/app/aQ;)V

    .line 57
    return-void
.end method

.method protected x()V
    .registers 3

    .prologue
    .line 61
    iget-object v0, p0, Lt/j;->a:Lt/n;

    invoke-virtual {v0}, Lt/n;->d()Lcom/google/android/maps/driveabout/app/cN;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cN;->setTopOverlayText(Ljava/lang/CharSequence;)V

    .line 62
    return-void
.end method
