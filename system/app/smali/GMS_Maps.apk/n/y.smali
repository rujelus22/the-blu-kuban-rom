.class Ln/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ln/w;


# instance fields
.field private final a:Landroid/location/LocationManager;

.field private b:Landroid/location/GpsStatus;

.field private final c:Ln/x;

.field private final d:LaC/h;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter

    .prologue
    .line 850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 846
    new-instance v0, Ln/x;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ln/x;-><init>(Ln/p;)V

    iput-object v0, p0, Ln/y;->c:Ln/x;

    .line 851
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Ln/y;->a:Landroid/location/LocationManager;

    .line 854
    new-instance v0, Ln/z;

    invoke-direct {v0, p0}, Ln/z;-><init>(Ln/y;)V

    .line 874
    new-instance v1, LaC/a;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->u()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LaC/a;-><init>(LaC/e;Lcom/google/googlenav/common/a;)V

    .line 876
    invoke-virtual {v1, p1}, LaC/a;->a(Landroid/content/Context;)V

    .line 877
    iput-object v1, p0, Ln/y;->d:LaC/h;

    .line 878
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ln/p;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 843
    invoke-direct {p0, p1}, Ln/y;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Ln/y;)Landroid/location/LocationManager;
    .registers 2
    .parameter

    .prologue
    .line 843
    iget-object v0, p0, Ln/y;->a:Landroid/location/LocationManager;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/location/Location;
    .registers 3
    .parameter

    .prologue
    .line 882
    iget-object v0, p0, Ln/y;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/List;
    .registers 2

    .prologue
    .line 887
    iget-object v0, p0, Ln/y;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;JFLn/c;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 909
    iget-object v0, p0, Ln/y;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 910
    iget-object v0, p0, Ln/y;->a:Landroid/location/LocationManager;

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 913
    :cond_15
    return-void
.end method

.method public a(Ln/c;)V
    .registers 3
    .parameter

    .prologue
    .line 898
    iget-object v0, p0, Ln/y;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 899
    return-void
.end method

.method public a(Ln/t;)V
    .registers 3
    .parameter

    .prologue
    .line 968
    iget-object v0, p0, Ln/y;->c:Ln/x;

    invoke-virtual {v0, p1}, Ln/x;->a(Ln/t;)V

    .line 969
    return-void
.end method

.method public a(Landroid/location/GpsStatus$Listener;)Z
    .registers 3
    .parameter

    .prologue
    .line 923
    iget-object v0, p0, Ln/y;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .registers 3

    .prologue
    .line 903
    iget-object v0, p0, Ln/y;->d:LaC/h;

    iget-object v1, p0, Ln/y;->c:Ln/x;

    invoke-virtual {v0, v1}, LaC/h;->b(LaC/i;)V

    .line 904
    return-void
.end method

.method public b(Landroid/location/GpsStatus$Listener;)V
    .registers 3
    .parameter

    .prologue
    .line 928
    iget-object v0, p0, Ln/y;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    .line 929
    return-void
.end method

.method public b(Ln/c;)V
    .registers 4
    .parameter

    .prologue
    .line 917
    iget-object v0, p0, Ln/y;->c:Ln/x;

    invoke-virtual {v0, p1}, Ln/x;->a(Ln/c;)V

    .line 918
    iget-object v0, p0, Ln/y;->d:LaC/h;

    iget-object v1, p0, Ln/y;->c:Ln/x;

    invoke-virtual {v0, v1}, LaC/h;->a(LaC/i;)V

    .line 919
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 892
    iget-object v0, p0, Ln/y;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Ln/y;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public c()Ln/u;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 934
    iget-object v1, p0, Ln/y;->a:Landroid/location/LocationManager;

    iget-object v2, p0, Ln/y;->b:Landroid/location/GpsStatus;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getGpsStatus(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v1

    iput-object v1, p0, Ln/y;->b:Landroid/location/GpsStatus;

    .line 937
    iget-object v1, p0, Ln/y;->b:Landroid/location/GpsStatus;

    invoke-virtual {v1}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    move v2, v0

    :goto_17
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/GpsSatellite;

    .line 938
    add-int/lit8 v2, v2, 0x1

    .line 939
    invoke-virtual {v0}, Landroid/location/GpsSatellite;->usedInFix()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 940
    add-int/lit8 v0, v1, 0x1

    :goto_2d
    move v1, v0

    goto :goto_17

    .line 943
    :cond_2f
    new-instance v0, Ln/u;

    invoke-direct {v0, v2, v1}, Ln/u;-><init>(II)V

    return-object v0

    :cond_35
    move v0, v1

    goto :goto_2d
.end method

.method public d()F
    .registers 2

    .prologue
    .line 948
    iget-object v0, p0, Ln/y;->d:LaC/h;

    invoke-virtual {v0}, LaC/h;->c()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Ln/y;->d:LaC/h;

    invoke-virtual {v0}, LaC/h;->d()F

    move-result v0

    :goto_e
    return v0

    :cond_f
    const/high16 v0, -0x4080

    goto :goto_e
.end method

.method public e()V
    .registers 2

    .prologue
    .line 954
    iget-object v0, p0, Ln/y;->d:LaC/h;

    invoke-virtual {v0}, LaC/h;->g()V

    .line 955
    return-void
.end method

.method public f()V
    .registers 3

    .prologue
    .line 959
    iget-object v0, p0, Ln/y;->d:LaC/h;

    invoke-virtual {v0}, LaC/h;->h()V

    .line 962
    iget-object v0, p0, Ln/y;->d:LaC/h;

    sget-object v1, LaC/j;->c:LaC/j;

    invoke-virtual {v0, v1}, LaC/h;->a(LaC/j;)V

    .line 964
    return-void
.end method
