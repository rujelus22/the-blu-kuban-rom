.class public Ln/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ls/e;


# static fields
.field public static final a:Ln/l;


# instance fields
.field private final b:Lr/z;

.field private final c:Lr/A;

.field private final d:LR/h;

.field private e:I

.field private final f:Ljava/util/Set;

.field private final g:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 61
    new-instance v0, Ln/l;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {v0, v1}, Ln/l;-><init>(Ljava/util/List;)V

    sput-object v0, Ln/n;->a:Ln/l;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Ln/n;->f:Ljava/util/Set;

    .line 55
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Ln/n;->g:Ljava/util/Set;

    .line 70
    sget-object v0, LA/c;->n:LA/c;

    invoke-static {v0}, Lr/C;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 71
    sget-object v0, LA/c;->n:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    iput-object v0, p0, Ln/n;->b:Lr/z;

    .line 72
    new-instance v0, LR/h;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Ln/n;->d:LR/h;

    .line 73
    new-instance v0, Ln/o;

    invoke-direct {v0, p0}, Ln/o;-><init>(Ln/n;)V

    iput-object v0, p0, Ln/n;->c:Lr/A;

    .line 90
    iget-object v0, p0, Ln/n;->b:Lr/z;

    iget-object v1, p0, Ln/n;->c:Lr/A;

    invoke-interface {v0, v1}, Lr/z;->a(Lr/A;)V

    .line 97
    :goto_38
    return-void

    .line 92
    :cond_39
    iput-object v1, p0, Ln/n;->b:Lr/z;

    .line 93
    iput-object v1, p0, Ln/n;->d:LR/h;

    .line 94
    iput-object v1, p0, Ln/n;->c:Lr/A;

    goto :goto_38
.end method

.method static a(Lo/aL;)Ln/l;
    .registers 5
    .parameter

    .prologue
    .line 216
    new-instance v1, Ln/m;

    invoke-direct {v1}, Ln/m;-><init>()V

    .line 217
    invoke-virtual {p0}, Lo/aL;->k()Lo/aO;

    move-result-object v2

    .line 219
    :cond_9
    :goto_9
    invoke-interface {v2}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 220
    invoke-interface {v2}, Lo/aO;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/n;

    .line 221
    instance-of v3, v0, Lo/f;

    if-eqz v3, :cond_9

    .line 222
    check-cast v0, Lo/f;

    .line 223
    invoke-virtual {v0}, Lo/f;->j()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 224
    invoke-virtual {v1, v0}, Ln/m;->a(Lo/f;)V

    goto :goto_9

    .line 228
    :cond_25
    invoke-virtual {v1}, Ln/m;->a()Ln/l;

    move-result-object v0

    return-object v0
.end method

.method private a(Lo/aq;Ln/l;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 313
    iget-object v0, p0, Ln/n;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/p;

    .line 314
    invoke-interface {v0, p1, p2}, Ln/p;->a(Lo/aq;Ln/l;)V

    goto :goto_6

    .line 316
    :cond_16
    return-void
.end method

.method private declared-synchronized b(Lo/aq;)V
    .registers 3
    .parameter

    .prologue
    .line 206
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Ln/n;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_16

    move-result v0

    if-eqz v0, :cond_b

    .line 212
    :goto_9
    monitor-exit p0

    return-void

    .line 209
    :cond_b
    :try_start_b
    iget-object v0, p0, Ln/n;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v0, p0, Ln/n;->b:Lr/z;

    invoke-interface {v0, p1, p0}, Lr/z;->a(Lo/aq;Ls/e;)V
    :try_end_15
    .catchall {:try_start_b .. :try_end_15} :catchall_16

    goto :goto_9

    .line 206
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(Lo/aq;)Ln/l;
    .registers 3
    .parameter

    .prologue
    .line 110
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Ln/n;->d:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/l;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_18

    .line 111
    if-eqz v0, :cond_d

    .line 117
    :goto_b
    monitor-exit p0

    return-object v0

    .line 115
    :cond_d
    :try_start_d
    invoke-direct {p0, p1}, Ln/n;->b(Lo/aq;)V

    .line 116
    iget v0, p0, Ln/n;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ln/n;->e:I
    :try_end_16
    .catchall {:try_start_d .. :try_end_16} :catchall_18

    .line 117
    const/4 v0, 0x0

    goto :goto_b

    .line 110
    :catchall_18
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .registers 2

    .prologue
    .line 189
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Ln/n;->d:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 190
    iget-object v0, p0, Ln/n;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_d

    .line 191
    monitor-exit p0

    return-void

    .line 189
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ln/p;)V
    .registers 3
    .parameter

    .prologue
    .line 302
    iget-object v0, p0, Ln/n;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 303
    return-void
.end method

.method public a(Lo/aq;ILo/ap;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 238
    const/4 v2, 0x0

    .line 243
    packed-switch p2, :pswitch_data_38

    :pswitch_6
    move v1, v0

    .line 269
    :goto_7
    :pswitch_7
    if-eqz v0, :cond_10

    .line 270
    monitor-enter p0

    .line 271
    :try_start_a
    iget-object v0, p0, Ln/n;->d:LR/h;

    invoke-virtual {v0, p1, v2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 272
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_a .. :try_end_10} :catchall_32

    .line 274
    :cond_10
    if-eqz v1, :cond_1c

    .line 275
    invoke-direct {p0, p1, v2}, Ln/n;->a(Lo/aq;Ln/l;)V

    .line 276
    monitor-enter p0

    .line 277
    :try_start_16
    iget-object v0, p0, Ln/n;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 278
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_16 .. :try_end_1c} :catchall_35

    .line 280
    :cond_1c
    return-void

    .line 246
    :pswitch_1d
    instance-of v0, p3, Lo/aL;

    if-eqz v0, :cond_2a

    .line 247
    check-cast p3, Lo/aL;

    invoke-static {p3}, Ln/n;->a(Lo/aL;)Ln/l;

    move-result-object v0

    :goto_27
    move-object v2, v0

    move v0, v1

    .line 253
    goto :goto_7

    .line 249
    :cond_2a
    sget-object v0, Ln/n;->a:Ln/l;

    goto :goto_27

    .line 256
    :pswitch_2d
    sget-object v0, Ln/n;->a:Ln/l;

    move-object v2, v0

    move v0, v1

    .line 259
    goto :goto_7

    .line 272
    :catchall_32
    move-exception v0

    :try_start_33
    monitor-exit p0
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_32

    throw v0

    .line 278
    :catchall_35
    move-exception v0

    :try_start_36
    monitor-exit p0
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    throw v0

    .line 243
    :pswitch_data_38
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_7
        :pswitch_2d
        :pswitch_6
        :pswitch_1d
    .end packed-switch
.end method
