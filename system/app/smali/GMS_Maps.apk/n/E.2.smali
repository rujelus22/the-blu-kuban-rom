.class public Ln/E;
.super Ln/f;
.source "SourceFile"

# interfaces
.implements Ln/d;


# instance fields
.field private c:Lp/y;

.field private d:I


# direct methods
.method constructor <init>(Ln/c;)V
    .registers 3
    .parameter

    .prologue
    .line 39
    const-string v0, "driveabout_polyline_snapping"

    invoke-direct {p0, v0, p1}, Ln/f;-><init>(Ljava/lang/String;Ln/c;)V

    .line 36
    const/4 v0, 0x2

    iput v0, p0, Ln/E;->d:I

    .line 40
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 88
    const-string v0, "driveabout_polyline_snapping"

    return-object v0
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 83
    iput p1, p0, Ln/E;->d:I

    .line 84
    return-void
.end method

.method public a(Lp/y;)V
    .registers 2
    .parameter

    .prologue
    .line 78
    iput-object p1, p0, Ln/E;->c:Lp/y;

    .line 79
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .registers 9
    .parameter

    .prologue
    .line 45
    move-object v0, p1

    check-cast v0, Ln/b;

    iget v1, p0, Ln/E;->d:I

    invoke-virtual {v0, v1}, Ln/b;->b(I)V

    .line 48
    iget-object v0, p0, Ln/E;->c:Lp/y;

    if-nez v0, :cond_10

    .line 49
    invoke-super {p0, p1}, Ln/f;->onLocationChanged(Landroid/location/Location;)V

    .line 74
    :goto_f
    return-void

    .line 54
    :cond_10
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-static {v0, v1}, Lo/Q;->a(D)D

    move-result-wide v0

    .line 55
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lo/Q;->a(DD)Lo/Q;

    move-result-object v2

    .line 56
    invoke-static {}, Lu/m;->a()Lu/k;

    move-result-object v3

    .line 57
    invoke-virtual {v3}, Lu/k;->E()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    add-float/2addr v3, v4

    .line 59
    iget-object v4, p0, Ln/E;->c:Lp/y;

    float-to-double v5, v3

    mul-double/2addr v0, v5

    invoke-virtual {v4, v2, v0, v1}, Lp/y;->a(Lo/Q;D)Lp/C;

    move-result-object v0

    .line 64
    new-instance v1, Ln/b;

    invoke-direct {v1, p1}, Ln/b;-><init>(Landroid/location/Location;)V

    .line 65
    if-eqz v0, :cond_55

    .line 66
    const-wide/high16 v2, 0x3ff0

    invoke-virtual {v1, v2, v3}, Ln/b;->a(D)V

    .line 67
    invoke-virtual {v0}, Lp/C;->c()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v1, v2}, Ln/b;->setBearing(F)V

    .line 68
    invoke-virtual {v1, v0}, Ln/b;->a(Lp/C;)V

    .line 73
    :goto_51
    invoke-super {p0, v1}, Ln/f;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_f

    .line 70
    :cond_55
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ln/b;->a(D)V

    goto :goto_51
.end method
