.class public Ln/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ls/c;


# static fields
.field private static b:Ln/q;

.field private static final p:Lo/D;


# instance fields
.field private final a:Ljava/util/Map;

.field private final c:LR/h;

.field private final d:LR/h;

.field private e:Lo/r;

.field private f:Lo/D;

.field private g:Lo/y;

.field private final h:Ljava/util/Set;

.field private final i:Ljava/util/List;

.field private final j:Ljava/util/Set;

.field private final k:Ljava/lang/Object;

.field private final l:Lr/n;

.field private final m:Ljava/util/Map;

.field private final n:Ljava/util/Map;

.field private final o:Ln/e;

.field private volatile q:Lo/D;

.field private volatile r:Lo/D;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const-wide/16 v2, 0x0

    .line 228
    new-instance v0, Lo/D;

    new-instance v1, Lo/r;

    invoke-direct {v1, v2, v3, v2, v3}, Lo/r;-><init>(JJ)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lo/D;-><init>(Lo/r;I)V

    sput-object v0, Ln/q;->p:Lo/D;

    return-void
.end method

.method constructor <init>(Lr/n;)V
    .registers 4
    .parameter

    .prologue
    const/16 v1, 0x64

    .line 286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Ln/q;->a:Ljava/util/Map;

    .line 181
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Ln/q;->h:Ljava/util/Set;

    .line 187
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Ln/q;->i:Ljava/util/List;

    .line 195
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Ln/q;->j:Ljava/util/Set;

    .line 201
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ln/q;->k:Ljava/lang/Object;

    .line 212
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ln/q;->m:Ljava/util/Map;

    .line 217
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Ln/q;->n:Ljava/util/Map;

    .line 287
    new-instance v0, LR/h;

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Ln/q;->c:LR/h;

    .line 288
    new-instance v0, LR/h;

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Ln/q;->d:LR/h;

    .line 289
    iput-object p1, p0, Ln/q;->l:Lr/n;

    .line 290
    new-instance v0, Ln/g;

    invoke-direct {v0}, Ln/g;-><init>()V

    iput-object v0, p0, Ln/q;->o:Ln/e;

    .line 291
    return-void
.end method

.method public static a()Ln/q;
    .registers 1

    .prologue
    .line 270
    sget-object v0, Ln/q;->b:Ln/q;

    return-object v0
.end method

.method public static a(Lr/n;)Ln/q;
    .registers 2
    .parameter

    .prologue
    .line 252
    sget-object v0, Ln/q;->b:Ln/q;

    if-nez v0, :cond_b

    .line 253
    new-instance v0, Ln/q;

    invoke-direct {v0, p0}, Ln/q;-><init>(Lr/n;)V

    sput-object v0, Ln/q;->b:Ln/q;

    .line 255
    :cond_b
    sget-object v0, Ln/q;->b:Ln/q;

    return-object v0
.end method

.method static synthetic a(Ln/q;Lo/y;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1}, Ln/q;->d(Lo/y;)V

    return-void
.end method

.method private a(Lo/r;Lo/D;Lo/D;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 807
    iget-object v2, p0, Ln/q;->c:LR/h;

    monitor-enter v2

    .line 808
    :try_start_3
    iget-object v0, p0, Ln/q;->d:LR/h;

    invoke-virtual {v0, p1, p3}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 809
    iget-object v0, p0, Ln/q;->l:Lr/n;

    invoke-virtual {p3}, Lo/D;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lr/n;->c(Lo/r;)Lo/z;

    move-result-object v3

    .line 811
    if-nez v3, :cond_16

    .line 812
    monitor-exit v2

    .line 840
    :goto_15
    return-void

    .line 817
    :cond_16
    sget-object v0, Ln/q;->p:Lo/D;

    if-ne p2, v0, :cond_55

    .line 820
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    move-object v1, v0

    .line 829
    :goto_1f
    invoke-virtual {v3}, Lo/z;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_27
    :goto_27
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_69

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/r;

    .line 830
    invoke-virtual {v0, p1}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_27

    .line 833
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_27

    .line 834
    iget-object v4, p0, Ln/q;->d:LR/h;

    iget-object v5, p0, Ln/q;->c:LR/h;

    invoke-virtual {v5, v0}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 836
    iget-object v4, p0, Ln/q;->c:LR/h;

    sget-object v5, Ln/q;->p:Lo/D;

    invoke-virtual {v4, v0, v5}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_27

    .line 839
    :catchall_52
    move-exception v0

    monitor-exit v2
    :try_end_54
    .catchall {:try_start_3 .. :try_end_54} :catchall_52

    throw v0

    .line 822
    :cond_55
    :try_start_55
    iget-object v0, p0, Ln/q;->l:Lr/n;

    invoke-virtual {p2}, Lo/D;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lr/n;->c(Lo/r;)Lo/z;

    move-result-object v0

    .line 824
    if-nez v0, :cond_63

    .line 825
    monitor-exit v2

    goto :goto_15

    .line 827
    :cond_63
    invoke-virtual {v0}, Lo/z;->c()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    goto :goto_1f

    .line 839
    :cond_69
    monitor-exit v2
    :try_end_6a
    .catchall {:try_start_55 .. :try_end_6a} :catchall_52

    goto :goto_15
.end method

.method private a(Lo/r;Ls/c;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 386
    iget-object v0, p0, Ln/q;->l:Lr/n;

    invoke-virtual {v0, p1}, Lr/n;->b(Lo/r;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 393
    :goto_8
    return-void

    .line 392
    :cond_9
    iget-object v0, p0, Ln/q;->l:Lr/n;

    invoke-virtual {v0, p1, p2}, Lr/n;->a(Lo/r;Ls/c;)V

    goto :goto_8
.end method

.method private a(Lo/y;Lo/D;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 847
    invoke-virtual {p1, p2}, Lo/y;->a(Lo/D;)Lo/z;

    move-result-object v1

    .line 848
    if-eqz v1, :cond_2d

    .line 849
    invoke-virtual {v1}, Lo/z;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_e
    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/r;

    .line 853
    invoke-virtual {v1}, Lo/z;->a()Lo/D;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Ln/q;->a(Lo/r;Lo/D;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 854
    new-instance v3, Ln/r;

    invoke-direct {v3, p0}, Ln/r;-><init>(Ln/q;)V

    invoke-direct {p0, v0, v3}, Ln/q;->a(Lo/r;Ls/c;)V

    goto :goto_e

    .line 871
    :cond_2d
    return-void
.end method

.method private c(Lo/y;)Lo/D;
    .registers 7
    .parameter

    .prologue
    .line 743
    const/4 v1, 0x0

    .line 745
    iget-object v2, p0, Ln/q;->c:LR/h;

    monitor-enter v2

    .line 746
    :try_start_4
    iget-object v0, p0, Ln/q;->c:LR/h;

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v3

    invoke-virtual {v0, v3}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    .line 747
    if-nez v0, :cond_28

    .line 748
    invoke-virtual {p1}, Lo/y;->c()Lo/z;

    move-result-object v0

    .line 749
    if-nez v0, :cond_2f

    sget-object v0, Ln/q;->p:Lo/D;

    .line 750
    :goto_1a
    iget-object v3, p0, Ln/q;->c:LR/h;

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 751
    sget-object v3, Ln/q;->p:Lo/D;

    if-eq v0, v3, :cond_28

    .line 752
    const/4 v1, 0x1

    .line 755
    :cond_28
    monitor-exit v2
    :try_end_29
    .catchall {:try_start_4 .. :try_end_29} :catchall_34

    .line 757
    if-eqz v1, :cond_2e

    .line 758
    invoke-direct {p0, p1}, Ln/q;->d(Lo/y;)V

    .line 760
    :cond_2e
    return-object v0

    .line 749
    :cond_2f
    :try_start_2f
    invoke-virtual {v0}, Lo/z;->a()Lo/D;

    move-result-object v0

    goto :goto_1a

    .line 755
    :catchall_34
    move-exception v0

    monitor-exit v2
    :try_end_36
    .catchall {:try_start_2f .. :try_end_36} :catchall_34

    throw v0
.end method

.method private d(Lo/y;)V
    .registers 5
    .parameter

    .prologue
    .line 764
    invoke-direct {p0}, Ln/q;->m()V

    .line 766
    iget-object v1, p0, Ln/q;->a:Ljava/util/Map;

    monitor-enter v1

    .line 767
    :try_start_6
    iget-object v0, p0, Ln/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/s;

    .line 768
    invoke-interface {v0, p0, p1}, Ln/s;->a(Ln/q;Lo/y;)V

    goto :goto_10

    .line 770
    :catchall_20
    move-exception v0

    monitor-exit v1
    :try_end_22
    .catchall {:try_start_6 .. :try_end_22} :catchall_20

    throw v0

    :cond_23
    :try_start_23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_20

    .line 771
    return-void
.end method

.method private k()V
    .registers 4

    .prologue
    .line 587
    iget-object v1, p0, Ln/q;->a:Ljava/util/Map;

    monitor-enter v1

    .line 588
    :try_start_3
    iget-object v0, p0, Ln/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/s;

    .line 589
    invoke-interface {v0, p0}, Ln/s;->a(Ln/q;)V

    goto :goto_d

    .line 591
    :catchall_1d
    move-exception v0

    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_1d

    throw v0

    :cond_20
    :try_start_20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_20 .. :try_end_21} :catchall_1d

    .line 592
    return-void
.end method

.method private l()V
    .registers 4

    .prologue
    .line 727
    invoke-direct {p0}, Ln/q;->m()V

    .line 729
    iget-object v1, p0, Ln/q;->a:Ljava/util/Map;

    monitor-enter v1

    .line 730
    :try_start_6
    iget-object v0, p0, Ln/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/s;

    .line 731
    invoke-interface {v0, p0}, Ln/s;->b(Ln/q;)V

    goto :goto_10

    .line 733
    :catchall_20
    move-exception v0

    monitor-exit v1
    :try_end_22
    .catchall {:try_start_6 .. :try_end_22} :catchall_20

    throw v0

    :cond_23
    :try_start_23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_20

    .line 734
    return-void
.end method

.method private m()V
    .registers 7

    .prologue
    .line 1008
    iget-object v2, p0, Ln/q;->n:Ljava/util/Map;

    monitor-enter v2

    .line 1009
    :try_start_3
    iget-object v0, p0, Ln/q;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1010
    iget-object v0, p0, Ln/q;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1011
    invoke-virtual {p0}, Ln/q;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_15
    :goto_15
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_60

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    .line 1014
    invoke-virtual {v0}, Lo/z;->a()Lo/D;

    move-result-object v1

    invoke-virtual {p0, v1}, Ln/q;->b(Lo/D;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 1017
    iget-object v1, p0, Ln/q;->m:Ljava/util/Map;

    invoke-virtual {v0}, Lo/z;->e()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ln/k;

    .line 1019
    if-nez v1, :cond_5c

    .line 1020
    new-instance v1, Ln/k;

    invoke-direct {v1, v0}, Ln/k;-><init>(Lo/z;)V

    .line 1021
    iget-object v4, p0, Ln/q;->m:Ljava/util/Map;

    invoke-virtual {v0}, Lo/z;->e()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1025
    :goto_4f
    iget-object v4, p0, Ln/q;->n:Ljava/util/Map;

    invoke-virtual {v0}, Lo/z;->b()Lo/r;

    move-result-object v0

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_15

    .line 1027
    :catchall_59
    move-exception v0

    monitor-exit v2
    :try_end_5b
    .catchall {:try_start_3 .. :try_end_5b} :catchall_59

    throw v0

    .line 1023
    :cond_5c
    :try_start_5c
    invoke-virtual {v1, v0}, Ln/k;->a(Lo/z;)Z

    goto :goto_4f

    .line 1027
    :cond_60
    monitor-exit v2
    :try_end_61
    .catchall {:try_start_5c .. :try_end_61} :catchall_59

    .line 1028
    return-void
.end method


# virtual methods
.method public a(Lo/r;ZZZ)Ln/k;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 910
    iget-object v3, p0, Ln/q;->n:Ljava/util/Map;

    monitor-enter v3

    .line 911
    :try_start_4
    iget-object v0, p0, Ln/q;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/k;

    .line 914
    if-nez v0, :cond_13

    if-nez p2, :cond_13

    .line 915
    monitor-exit v3

    move-object v0, v1

    .line 962
    :goto_12
    return-object v0

    .line 921
    :cond_13
    if-eqz p3, :cond_72

    if-eqz v0, :cond_72

    invoke-virtual {v0}, Ln/k;->g()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v4, 0x1

    if-le v2, v4, :cond_72

    move-object v2, v0

    move-object v0, v1

    .line 927
    :goto_24
    if-eqz v0, :cond_2b

    .line 928
    monitor-exit v3

    goto :goto_12

    .line 963
    :catchall_28
    move-exception v0

    monitor-exit v3
    :try_end_2a
    .catchall {:try_start_4 .. :try_end_2a} :catchall_28

    throw v0

    .line 933
    :cond_2b
    :try_start_2b
    iget-object v0, p0, Ln/q;->l:Lr/n;

    invoke-virtual {v0, p1}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v0

    .line 934
    if-nez v0, :cond_3a

    .line 937
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ln/q;->a(Lo/r;Ls/c;)V

    .line 938
    monitor-exit v3

    move-object v0, v1

    goto :goto_12

    .line 941
    :cond_3a
    invoke-virtual {v0, p1}, Lo/y;->a(Lo/r;)Lo/z;

    move-result-object v0

    .line 942
    if-nez v0, :cond_43

    .line 943
    monitor-exit v3

    move-object v0, v1

    goto :goto_12

    .line 946
    :cond_43
    new-instance v1, Ln/k;

    invoke-direct {v1, v0}, Ln/k;-><init>(Lo/z;)V

    .line 948
    if-eqz p4, :cond_6f

    .line 949
    iget-object v0, p0, Ln/q;->n:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 953
    if-eqz v2, :cond_6f

    .line 954
    invoke-virtual {v2, p1}, Ln/k;->a(Lo/r;)Ln/k;

    move-result-object v2

    .line 956
    invoke-virtual {v2}, Ln/k;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/r;

    .line 957
    iget-object v5, p0, Ln/q;->n:Ljava/util/Map;

    invoke-interface {v5, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5d

    .line 962
    :cond_6f
    monitor-exit v3
    :try_end_70
    .catchall {:try_start_2b .. :try_end_70} :catchall_28

    move-object v0, v1

    goto :goto_12

    :cond_72
    move-object v2, v1

    goto :goto_24
.end method

.method public a(Lo/r;)Lo/D;
    .registers 4
    .parameter

    .prologue
    .line 486
    iget-object v1, p0, Ln/q;->c:LR/h;

    monitor-enter v1

    .line 487
    :try_start_3
    iget-object v0, p0, Ln/q;->c:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    .line 488
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_20

    .line 491
    if-nez v0, :cond_1a

    .line 492
    iget-object v1, p0, Ln/q;->l:Lr/n;

    invoke-virtual {v1, p1}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v1

    .line 493
    if-eqz v1, :cond_23

    .line 494
    invoke-direct {p0, v1}, Ln/q;->c(Lo/y;)Lo/D;

    move-result-object v0

    .line 502
    :cond_1a
    :goto_1a
    sget-object v1, Ln/q;->p:Lo/D;

    if-ne v0, v1, :cond_1f

    const/4 v0, 0x0

    :cond_1f
    return-object v0

    .line 488
    :catchall_20
    move-exception v0

    :try_start_21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_20

    throw v0

    .line 498
    :cond_23
    invoke-direct {p0, p1, p0}, Ln/q;->a(Lo/r;Ls/c;)V

    goto :goto_1a
.end method

.method public a(Ljava/util/Set;)V
    .registers 7
    .parameter

    .prologue
    .line 607
    iget-object v1, p0, Ln/q;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 608
    if-nez p1, :cond_9

    .line 609
    :try_start_5
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object p1

    .line 611
    :cond_9
    iget-object v0, p0, Ln/q;->j:Ljava/util/Set;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 612
    monitor-exit v1

    .line 643
    :goto_12
    return-void

    .line 614
    :cond_13
    iget-object v0, p0, Ln/q;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 615
    iget-object v0, p0, Ln/q;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 616
    iget-object v0, p0, Ln/q;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 617
    iget-object v0, p0, Ln/q;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 623
    iget-object v0, p0, Ln/q;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 626
    iget-object v0, p0, Ln/q;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_32
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_58

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/r;

    .line 627
    iget-object v3, p0, Ln/q;->l:Lr/n;

    invoke-virtual {v3, v0}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v3

    .line 628
    if-eqz v3, :cond_54

    .line 629
    iget-object v4, p0, Ln/q;->i:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 630
    iget-object v3, p0, Ln/q;->h:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_32

    .line 635
    :catchall_51
    move-exception v0

    monitor-exit v1
    :try_end_53
    .catchall {:try_start_5 .. :try_end_53} :catchall_51

    throw v0

    .line 632
    :cond_54
    :try_start_54
    invoke-direct {p0, v0, p0}, Ln/q;->a(Lo/r;Ls/c;)V

    goto :goto_32

    .line 635
    :cond_58
    monitor-exit v1
    :try_end_59
    .catchall {:try_start_54 .. :try_end_59} :catchall_51

    .line 642
    invoke-direct {p0}, Ln/q;->l()V

    goto :goto_12
.end method

.method public a(Ln/s;)V
    .registers 4
    .parameter

    .prologue
    .line 301
    iget-object v0, p0, Ln/q;->a:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    return-void
.end method

.method public a(Lo/D;)V
    .registers 4
    .parameter

    .prologue
    .line 402
    iget-object v1, p0, Ln/q;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 404
    :try_start_3
    iget-object v0, p0, Ln/q;->f:Lo/D;

    invoke-virtual {p1, v0}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    iget-object v0, p0, Ln/q;->c:LR/h;

    invoke-virtual {v0}, LR/h;->i()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 405
    :cond_17
    monitor-exit v1

    .line 410
    :goto_18
    return-void

    .line 407
    :cond_19
    iput-object p1, p0, Ln/q;->f:Lo/D;

    .line 408
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_24

    .line 409
    invoke-virtual {p1}, Lo/D;->a()Lo/r;

    move-result-object v0

    invoke-direct {p0, v0, p0}, Ln/q;->a(Lo/r;Ls/c;)V

    goto :goto_18

    .line 408
    :catchall_24
    move-exception v0

    :try_start_25
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_24

    throw v0
.end method

.method public a(Lo/D;Lo/D;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 445
    iput-object p1, p0, Ln/q;->q:Lo/D;

    .line 446
    iput-object p2, p0, Ln/q;->r:Lo/D;

    .line 447
    invoke-direct {p0}, Ln/q;->m()V

    .line 448
    return-void
.end method

.method public a(Lo/r;ILo/y;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 315
    const/4 v3, 0x2

    if-ne p2, v3, :cond_7

    .line 382
    :cond_6
    :goto_6
    return-void

    .line 317
    :cond_7
    if-nez p2, :cond_6

    .line 322
    iget-object v3, p0, Ln/q;->k:Ljava/lang/Object;

    monitor-enter v3

    .line 323
    :try_start_c
    iget-object v4, p0, Ln/q;->f:Lo/D;

    if-eqz v4, :cond_21

    iget-object v4, p0, Ln/q;->f:Lo/D;

    invoke-virtual {v4}, Lo/D;->a()Lo/r;

    move-result-object v4

    invoke-virtual {v4, p1}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 325
    iget-object v2, p0, Ln/q;->f:Lo/D;

    .line 326
    const/4 v4, 0x0

    iput-object v4, p0, Ln/q;->f:Lo/D;

    .line 328
    :cond_21
    monitor-exit v3
    :try_end_22
    .catchall {:try_start_c .. :try_end_22} :catchall_80

    .line 330
    if-eqz v2, :cond_27

    .line 331
    invoke-direct {p0, p3, v2}, Ln/q;->a(Lo/y;Lo/D;)V

    .line 336
    :cond_27
    invoke-direct {p0, p3}, Ln/q;->c(Lo/y;)Lo/D;

    .line 342
    iget-object v3, p0, Ln/q;->k:Ljava/lang/Object;

    monitor-enter v3

    .line 343
    :try_start_2d
    iget-object v2, p0, Ln/q;->e:Lo/r;

    invoke-virtual {p1, v2}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_91

    .line 345
    iget-object v2, p0, Ln/q;->g:Lo/y;

    if-eqz v2, :cond_49

    invoke-virtual {p3}, Lo/y;->a()Lo/r;

    move-result-object v2

    iget-object v4, p0, Ln/q;->g:Lo/y;

    invoke-virtual {v4}, Lo/y;->a()Lo/r;

    move-result-object v4

    invoke-virtual {v2, v4}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8f

    .line 349
    :cond_49
    invoke-virtual {p3}, Lo/y;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_83

    .line 350
    iget-object v2, p0, Ln/q;->g:Lo/y;

    if-eqz v2, :cond_8f

    .line 351
    const/4 v2, 0x0

    iput-object v2, p0, Ln/q;->g:Lo/y;

    move v2, v0

    .line 359
    :goto_5b
    const/4 v4, 0x0

    iput-object v4, p0, Ln/q;->e:Lo/r;

    .line 361
    :goto_5e
    monitor-exit v3
    :try_end_5f
    .catchall {:try_start_2d .. :try_end_5f} :catchall_87

    .line 363
    if-eqz v2, :cond_64

    .line 364
    invoke-direct {p0}, Ln/q;->k()V

    .line 369
    :cond_64
    iget-object v2, p0, Ln/q;->k:Ljava/lang/Object;

    monitor-enter v2

    .line 370
    :try_start_67
    iget-object v3, p0, Ln/q;->h:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8d

    .line 371
    iget-object v1, p0, Ln/q;->h:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 372
    iget-object v1, p0, Ln/q;->i:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 375
    :goto_79
    monitor-exit v2
    :try_end_7a
    .catchall {:try_start_67 .. :try_end_7a} :catchall_8a

    .line 378
    if-eqz v0, :cond_6

    .line 379
    invoke-direct {p0}, Ln/q;->l()V

    goto :goto_6

    .line 328
    :catchall_80
    move-exception v0

    :try_start_81
    monitor-exit v3
    :try_end_82
    .catchall {:try_start_81 .. :try_end_82} :catchall_80

    throw v0

    .line 355
    :cond_83
    :try_start_83
    iput-object p3, p0, Ln/q;->g:Lo/y;

    move v2, v0

    .line 356
    goto :goto_5b

    .line 361
    :catchall_87
    move-exception v0

    monitor-exit v3
    :try_end_89
    .catchall {:try_start_83 .. :try_end_89} :catchall_87

    throw v0

    .line 375
    :catchall_8a
    move-exception v0

    :try_start_8b
    monitor-exit v2
    :try_end_8c
    .catchall {:try_start_8b .. :try_end_8c} :catchall_8a

    throw v0

    :cond_8d
    move v0, v1

    goto :goto_79

    :cond_8f
    move v2, v1

    goto :goto_5b

    :cond_91
    move v2, v1

    goto :goto_5e
.end method

.method public a(Lo/y;)V
    .registers 4
    .parameter

    .prologue
    .line 416
    if-eqz p1, :cond_e

    .line 417
    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v0

    sget-object v1, Ln/q;->p:Lo/D;

    invoke-virtual {p0, v0, v1}, Ln/q;->a(Lo/r;Lo/D;)Z

    .line 418
    invoke-direct {p0, p1}, Ln/q;->d(Lo/y;)V

    .line 420
    :cond_e
    return-void
.end method

.method a(Lo/r;Lo/D;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 785
    iget-object v1, p0, Ln/q;->c:LR/h;

    monitor-enter v1

    .line 786
    :try_start_3
    iget-object v0, p0, Ln/q;->c:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    .line 787
    invoke-virtual {p2, v0}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 788
    const/4 v0, 0x0

    monitor-exit v1

    .line 795
    :goto_13
    return v0

    .line 790
    :cond_14
    iget-object v2, p0, Ln/q;->c:LR/h;

    invoke-virtual {v2, p1, p2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 791
    if-eqz v0, :cond_1e

    .line 792
    invoke-direct {p0, p1, p2, v0}, Ln/q;->a(Lo/r;Lo/D;Lo/D;)V

    .line 794
    :cond_1e
    monitor-exit v1

    .line 795
    const/4 v0, 0x1

    goto :goto_13

    .line 794
    :catchall_21
    move-exception v0

    monitor-exit v1
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    throw v0
.end method

.method public b(Lo/r;)Lo/E;
    .registers 4
    .parameter

    .prologue
    .line 511
    invoke-virtual {p0, p1}, Ln/q;->a(Lo/r;)Lo/D;

    move-result-object v0

    .line 512
    if-nez v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    new-instance v1, Lo/F;

    invoke-direct {v1}, Lo/F;-><init>()V

    invoke-virtual {v1, v0}, Lo/F;->a(Lo/D;)Lo/F;

    move-result-object v0

    invoke-virtual {v0}, Lo/F;->a()Lo/E;

    move-result-object v0

    goto :goto_7
.end method

.method public b(Lo/y;)Lo/z;
    .registers 3
    .parameter

    .prologue
    .line 430
    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Ln/q;->a(Lo/r;)Lo/D;

    move-result-object v0

    .line 431
    if-eqz v0, :cond_f

    .line 432
    invoke-virtual {p1, v0}, Lo/y;->a(Lo/D;)Lo/z;

    move-result-object v0

    .line 434
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public b()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 454
    iput-object v0, p0, Ln/q;->q:Lo/D;

    .line 455
    iput-object v0, p0, Ln/q;->r:Lo/D;

    .line 456
    invoke-direct {p0}, Ln/q;->m()V

    .line 457
    return-void
.end method

.method public b(Ln/s;)V
    .registers 3
    .parameter

    .prologue
    .line 305
    iget-object v0, p0, Ln/q;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    return-void
.end method

.method public b(Lo/D;)Z
    .registers 3
    .parameter

    .prologue
    .line 463
    if-eqz p1, :cond_14

    iget-object v0, p0, Ln/q;->q:Lo/D;

    invoke-virtual {p1, v0}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Ln/q;->r:Lo/D;

    invoke-virtual {p1, v0}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_12
    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public b(Lo/D;Lo/D;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 474
    if-eqz p1, :cond_16

    if-eqz p2, :cond_16

    iget-object v0, p0, Ln/q;->q:Lo/D;

    invoke-virtual {p1, v0}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Ln/q;->r:Lo/D;

    invoke-virtual {p2, v0}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public c(Lo/r;)Lo/D;
    .registers 5
    .parameter

    .prologue
    .line 533
    iget-object v1, p0, Ln/q;->c:LR/h;

    monitor-enter v1

    .line 534
    :try_start_3
    iget-object v0, p0, Ln/q;->d:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    .line 535
    sget-object v2, Ln/q;->p:Lo/D;

    if-ne v0, v2, :cond_10

    const/4 v0, 0x0

    :cond_10
    monitor-exit v1

    return-object v0

    .line 536
    :catchall_12
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    throw v0
.end method

.method public c()Lo/y;
    .registers 3

    .prologue
    .line 580
    iget-object v1, p0, Ln/q;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 581
    :try_start_3
    iget-object v0, p0, Ln/q;->g:Lo/y;

    monitor-exit v1

    return-object v0

    .line 582
    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    throw v0
.end method

.method public c(Lo/D;)Z
    .registers 5
    .parameter

    .prologue
    .line 718
    invoke-virtual {p0}, Ln/q;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    .line 719
    invoke-virtual {v0}, Lo/z;->b()Lo/r;

    move-result-object v0

    invoke-virtual {p1}, Lo/D;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v0, v2}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 720
    const/4 v0, 0x1

    .line 723
    :goto_23
    return v0

    :cond_24
    const/4 v0, 0x0

    goto :goto_23
.end method

.method public d()Ljava/util/List;
    .registers 3

    .prologue
    .line 664
    iget-object v1, p0, Ln/q;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 665
    :try_start_3
    iget-object v0, p0, Ln/q;->i:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 666
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    throw v0
.end method

.method public d(Lo/r;)V
    .registers 5
    .parameter

    .prologue
    .line 547
    if-nez p1, :cond_1b

    .line 549
    const/4 v0, 0x0

    .line 550
    iget-object v1, p0, Ln/q;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 551
    :try_start_6
    iget-object v2, p0, Ln/q;->g:Lo/y;

    if-eqz v2, :cond_11

    .line 553
    const/4 v0, 0x0

    iput-object v0, p0, Ln/q;->e:Lo/r;

    .line 554
    const/4 v0, 0x0

    iput-object v0, p0, Ln/q;->g:Lo/y;

    .line 555
    const/4 v0, 0x1

    .line 557
    :cond_11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_6 .. :try_end_12} :catchall_18

    .line 558
    if-eqz v0, :cond_17

    .line 559
    invoke-direct {p0}, Ln/q;->k()V

    .line 574
    :cond_17
    :goto_17
    return-void

    .line 557
    :catchall_18
    move-exception v0

    :try_start_19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_18

    throw v0

    .line 564
    :cond_1b
    iget-object v1, p0, Ln/q;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 566
    :try_start_1e
    iget-object v0, p0, Ln/q;->e:Lo/r;

    invoke-virtual {p1, v0}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_36

    iget-object v0, p0, Ln/q;->g:Lo/y;

    if-eqz v0, :cond_3b

    iget-object v0, p0, Ln/q;->g:Lo/y;

    invoke-virtual {v0}, Lo/y;->a()Lo/r;

    move-result-object v0

    invoke-virtual {v0, p1}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 568
    :cond_36
    monitor-exit v1

    goto :goto_17

    .line 571
    :catchall_38
    move-exception v0

    monitor-exit v1
    :try_end_3a
    .catchall {:try_start_1e .. :try_end_3a} :catchall_38

    throw v0

    .line 570
    :cond_3b
    :try_start_3b
    iput-object p1, p0, Ln/q;->e:Lo/r;

    .line 571
    monitor-exit v1
    :try_end_3e
    .catchall {:try_start_3b .. :try_end_3e} :catchall_38

    .line 572
    invoke-direct {p0, p1, p0}, Ln/q;->a(Lo/r;Ls/c;)V

    goto :goto_17
.end method

.method public d(Lo/D;)Z
    .registers 4
    .parameter

    .prologue
    .line 877
    iget-object v1, p0, Ln/q;->c:LR/h;

    monitor-enter v1

    .line 878
    :try_start_3
    iget-object v0, p0, Ln/q;->c:LR/h;

    invoke-virtual {v0}, LR/h;->i()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 879
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public e()Ljava/util/Set;
    .registers 4

    .prologue
    .line 674
    invoke-virtual {p0}, Ln/q;->d()Ljava/util/List;

    move-result-object v0

    .line 675
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 676
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/y;

    .line 677
    invoke-virtual {p0, v0}, Ln/q;->b(Lo/y;)Lo/z;

    move-result-object v0

    .line 678
    if-eqz v0, :cond_c

    .line 679
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 682
    :cond_22
    return-object v1
.end method

.method public e(Lo/r;)Ln/k;
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 980
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v1, v1}, Ln/q;->a(Lo/r;ZZZ)Ln/k;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .registers 3

    .prologue
    .line 689
    invoke-virtual {p0}, Ln/q;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/y;

    .line 690
    invoke-virtual {p0, v0}, Ln/q;->b(Lo/y;)Lo/z;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 691
    const/4 v0, 0x1

    .line 694
    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public g()Ljava/util/Set;
    .registers 4

    .prologue
    .line 702
    invoke-virtual {p0}, Ln/q;->d()Ljava/util/List;

    move-result-object v0

    .line 703
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 704
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/y;

    .line 705
    invoke-virtual {p0, v0}, Ln/q;->b(Lo/y;)Lo/z;

    move-result-object v0

    .line 706
    if-eqz v0, :cond_c

    .line 707
    invoke-virtual {v0}, Lo/z;->a()Lo/D;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 710
    :cond_26
    return-object v1
.end method

.method public h()Z
    .registers 3

    .prologue
    .line 886
    invoke-virtual {p0}, Ln/q;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    .line 887
    invoke-virtual {v0}, Lo/z;->e()I

    move-result v0

    if-gez v0, :cond_8

    .line 888
    const/4 v0, 0x1

    .line 891
    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public i()Ljava/util/Set;
    .registers 3

    .prologue
    .line 987
    iget-object v1, p0, Ln/q;->n:Ljava/util/Map;

    monitor-enter v1

    .line 988
    :try_start_3
    iget-object v0, p0, Ln/q;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 989
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public j()Ln/e;
    .registers 2

    .prologue
    .line 1036
    iget-object v0, p0, Ln/q;->o:Ln/e;

    return-object v0
.end method
