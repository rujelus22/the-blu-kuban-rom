.class public Lk/f;
.super Lk/g;
.source "SourceFile"


# instance fields
.field private d:Z

.field private e:J

.field private f:Lo/aQ;

.field private g:Lo/aQ;

.field private h:Ljava/util/List;

.field private i:Ljava/util/List;


# direct methods
.method public constructor <init>(LA/c;LA/b;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lk/g;-><init>(LA/c;LA/b;)V

    .line 39
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lk/f;->e:J

    .line 54
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->x()D

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lk/f;->a(LA/c;D)Z

    move-result v0

    iput-boolean v0, p0, Lk/f;->d:Z

    .line 56
    return-void
.end method

.method private static a(LA/c;D)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 63
    sget-object v0, LA/c;->j:LA/c;

    if-eq p0, v0, :cond_c

    sget-object v0, LA/c;->k:LA/c;

    if-eq p0, v0, :cond_c

    sget-object v0, LA/c;->l:LA/c;

    if-ne p0, v0, :cond_14

    :cond_c
    const-wide/high16 v0, 0x3ff0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private b()Z
    .registers 3

    .prologue
    .line 93
    iget-object v0, p0, Lk/f;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 94
    const/4 v0, 0x1

    .line 96
    :goto_9
    return v0

    :cond_a
    iget-object v0, p0, Lk/f;->h:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->k()Lo/aB;

    move-result-object v0

    iget-object v1, p0, Lk/f;->b:LA/b;

    invoke-interface {v1}, LA/b;->a()Lo/aB;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/aB;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_9
.end method


# virtual methods
.method public a(Lo/T;)F
    .registers 4
    .parameter

    .prologue
    .line 150
    iget-boolean v0, p0, Lk/f;->d:Z

    if-eqz v0, :cond_c

    .line 151
    invoke-super {p0, p1}, Lk/g;->a(Lo/T;)F

    move-result v0

    const/high16 v1, 0x3f80

    sub-float/2addr v0, v1

    .line 153
    :goto_b
    return v0

    :cond_c
    invoke-super {p0, p1}, Lk/g;->a(Lo/T;)F

    move-result v0

    goto :goto_b
.end method

.method public a()J
    .registers 3

    .prologue
    .line 101
    iget-wide v0, p0, Lk/f;->e:J

    return-wide v0
.end method

.method public a(LC/a;)Ljava/util/List;
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 76
    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v1

    .line 77
    iget-object v0, p0, Lk/f;->f:Lo/aQ;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lk/f;->f:Lo/aQ;

    invoke-virtual {v1, v0}, Lo/aQ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-direct {p0}, Lk/f;->b()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 78
    iget-object v0, p0, Lk/f;->h:Ljava/util/List;

    .line 89
    :goto_19
    return-object v0

    .line 80
    :cond_1a
    iget-wide v2, p0, Lk/f;->e:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lk/f;->e:J

    .line 81
    invoke-virtual {v1}, Lo/aQ;->a()Lo/aR;

    move-result-object v0

    invoke-virtual {p0, p1}, Lk/f;->c(LC/a;)I

    move-result v2

    iget-object v3, p0, Lk/f;->b:LA/b;

    invoke-interface {v3}, LA/b;->a()Lo/aB;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lo/aq;->a(Lo/aR;ILo/aB;)Ljava/util/ArrayList;

    move-result-object v2

    .line 83
    invoke-virtual {p1}, LC/a;->q()F

    move-result v0

    cmpl-float v0, v0, v6

    if-nez v0, :cond_50

    invoke-virtual {p1}, LC/a;->p()F

    move-result v0

    cmpl-float v0, v0, v6

    if-nez v0, :cond_50

    const/4 v0, 0x1

    .line 84
    :goto_44
    if-nez v0, :cond_49

    .line 85
    invoke-virtual {p0, v1, v2}, Lk/f;->a(Lo/aQ;Ljava/util/ArrayList;)V

    .line 87
    :cond_49
    iput-object v2, p0, Lk/f;->h:Ljava/util/List;

    .line 88
    iput-object v1, p0, Lk/f;->f:Lo/aQ;

    .line 89
    iget-object v0, p0, Lk/f;->h:Ljava/util/List;

    goto :goto_19

    .line 83
    :cond_50
    const/4 v0, 0x0

    goto :goto_44
.end method

.method a(Lo/aQ;Ljava/util/ArrayList;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 133
    .line 134
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    .line 135
    :goto_7
    if-ge v3, v4, :cond_24

    .line 136
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    .line 137
    invoke-virtual {v0}, Lo/aq;->i()Lo/ad;

    move-result-object v1

    .line 138
    invoke-virtual {p1, v1}, Lo/aQ;->b(Lo/ae;)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 139
    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p2, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 135
    :goto_1f
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_7

    .line 143
    :cond_24
    add-int/lit8 v0, v4, -0x1

    :goto_26
    if-lt v0, v2, :cond_2e

    .line 144
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 143
    add-int/lit8 v0, v0, -0x1

    goto :goto_26

    .line 146
    :cond_2e
    return-void

    :cond_2f
    move v0, v2

    goto :goto_1f
.end method

.method public b(LC/a;)Ljava/util/List;
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 112
    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v1

    .line 113
    iget-object v0, p0, Lk/f;->g:Lo/aQ;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lk/f;->g:Lo/aQ;

    invoke-virtual {v1, v0}, Lo/aQ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 114
    iget-object v0, p0, Lk/f;->i:Ljava/util/List;

    .line 125
    :goto_13
    return-object v0

    .line 116
    :cond_14
    invoke-virtual {v1}, Lo/aQ;->a()Lo/aR;

    move-result-object v0

    invoke-virtual {p0, p1}, Lk/f;->c(LC/a;)I

    move-result v2

    invoke-static {v0, v2}, Lo/aq;->b(Lo/aR;I)Ljava/util/ArrayList;

    move-result-object v2

    .line 119
    invoke-virtual {p1}, LC/a;->q()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_3d

    invoke-virtual {p1}, LC/a;->p()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_3d

    const/4 v0, 0x1

    .line 120
    :goto_31
    if-nez v0, :cond_36

    .line 121
    invoke-virtual {p0, v1, v2}, Lk/f;->a(Lo/aQ;Ljava/util/ArrayList;)V

    .line 123
    :cond_36
    iput-object v1, p0, Lk/f;->g:Lo/aQ;

    .line 124
    iput-object v2, p0, Lk/f;->i:Ljava/util/List;

    .line 125
    iget-object v0, p0, Lk/f;->i:Ljava/util/List;

    goto :goto_13

    .line 119
    :cond_3d
    const/4 v0, 0x0

    goto :goto_31
.end method

.method protected c(LC/a;)I
    .registers 6
    .parameter

    .prologue
    .line 160
    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    .line 161
    iget-object v1, p0, Lk/f;->c:Lcom/google/android/maps/driveabout/vector/bF;

    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v2

    iget-object v3, p0, Lk/f;->a:LA/c;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/bF;->a(Lo/T;LA/c;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v1

    .line 162
    if-eqz v1, :cond_17

    .line 163
    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/bE;->a(F)I

    move-result v0

    .line 166
    :goto_16
    return v0

    :cond_17
    float-to-int v0, v0

    goto :goto_16
.end method
