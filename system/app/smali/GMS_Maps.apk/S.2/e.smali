.class public LS/e;
.super LS/a;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private b:LD/b;

.field private c:LE/i;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, LS/a;-><init>()V

    .line 36
    iput-object v1, p0, LS/e;->b:LD/b;

    .line 37
    iput-object v1, p0, LS/e;->c:LE/i;

    .line 45
    iput v0, p0, LS/e;->d:I

    .line 46
    iput v0, p0, LS/e;->e:I

    .line 52
    iput-object p1, p0, LS/e;->a:Landroid/content/res/Resources;

    .line 56
    return-void
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/high16 v5, 0x4780

    const/4 v4, 0x0

    .line 104
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->G()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 153
    :goto_10
    return-void

    .line 109
    :cond_11
    iget-object v0, p0, LS/e;->b:LD/b;

    if-nez v0, :cond_6a

    .line 110
    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    iput-object v0, p0, LS/e;->b:LD/b;

    .line 111
    iget-object v0, p0, LS/e;->b:LD/b;

    invoke-virtual {v0, v6}, LD/b;->c(Z)V

    .line 112
    iget-object v0, p0, LS/e;->b:LD/b;

    iget-object v1, p0, LS/e;->a:Landroid/content/res/Resources;

    const v2, 0x7f0201eb

    invoke-virtual {v0, v1, v2}, LD/b;->a(Landroid/content/res/Resources;I)V

    .line 116
    new-instance v0, LE/j;

    const/16 v1, 0x8

    new-array v1, v1, [I

    aput v4, v1, v4

    aput v4, v1, v6

    const/4 v2, 0x2

    aput v4, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, LS/e;->b:LD/b;

    invoke-virtual {v3}, LD/b;->c()F

    move-result v3

    mul-float/2addr v3, v5

    float-to-int v3, v3

    aput v3, v1, v2

    iget-object v2, p0, LS/e;->b:LD/b;

    invoke-virtual {v2}, LD/b;->b()F

    move-result v2

    mul-float/2addr v2, v5

    float-to-int v2, v2

    aput v2, v1, v7

    aput v4, v1, v8

    const/4 v2, 0x6

    iget-object v3, p0, LS/e;->b:LD/b;

    invoke-virtual {v3}, LD/b;->b()F

    move-result v3

    mul-float/2addr v3, v5

    float-to-int v3, v3

    aput v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, LS/e;->b:LD/b;

    invoke-virtual {v3}, LD/b;->c()F

    move-result v3

    mul-float/2addr v3, v5

    float-to-int v3, v3

    aput v3, v1, v2

    invoke-direct {v0, v1}, LE/j;-><init>([I)V

    iput-object v0, p0, LS/e;->c:LE/i;

    .line 126
    :cond_6a
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 127
    invoke-virtual {p1}, LD/a;->A()V

    .line 128
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 131
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 135
    iget v1, p0, LS/e;->d:I

    int-to-float v1, v1

    iget v2, p0, LS/e;->e:I

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 138
    iget-object v1, p0, LS/e;->b:LD/b;

    invoke-virtual {v1}, LD/b;->d()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, LS/e;->b:LD/b;

    invoke-virtual {v2}, LD/b;->e()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x3f80

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 140
    invoke-virtual {p1}, LD/a;->p()V

    .line 141
    invoke-virtual {p1}, LD/a;->r()V

    .line 142
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 143
    const/16 v1, 0x303

    invoke-interface {v0, v6, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 144
    iget-object v1, p0, LS/e;->c:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    .line 147
    iget-object v1, p0, LS/e;->b:LD/b;

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 149
    iget-object v1, p1, LD/a;->f:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 150
    invoke-interface {v0, v8, v4, v7}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 151
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 152
    invoke-virtual {p1}, LD/a;->B()V

    goto/16 :goto_10
.end method

.method public c(LD/a;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 172
    iget-object v0, p0, LS/e;->b:LD/b;

    if-eqz v0, :cond_c

    .line 173
    iget-object v0, p0, LS/e;->b:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 174
    iput-object v1, p0, LS/e;->b:LD/b;

    .line 177
    :cond_c
    iget-object v0, p0, LS/e;->c:LE/i;

    if-eqz v0, :cond_17

    .line 178
    iget-object v0, p0, LS/e;->c:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    .line 179
    iput-object v1, p0, LS/e;->c:LE/i;

    .line 188
    :cond_17
    return-void
.end method

.method public i()V
    .registers 2

    .prologue
    .line 158
    const/4 v0, 0x0

    iput v0, p0, LS/e;->e:I

    .line 160
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->at()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 162
    const/16 v0, 0x60

    iput v0, p0, LS/e;->e:I

    .line 168
    :cond_11
    return-void
.end method
