.class public LS/c;
.super LS/a;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lcom/google/android/maps/driveabout/vector/aV;

.field private final c:F

.field private d:[F

.field private e:LD/b;

.field private final f:LE/i;

.field private volatile g:Ljava/lang/String;

.field private volatile h:I

.field private volatile i:I

.field private j:Lcom/google/android/maps/driveabout/vector/q;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, LS/a;-><init>()V

    .line 37
    iput-object v0, p0, LS/c;->e:LD/b;

    .line 39
    iput-object v0, p0, LS/c;->g:Ljava/lang/String;

    .line 40
    const/16 v0, 0x32

    iput v0, p0, LS/c;->h:I

    .line 41
    const/4 v0, 0x4

    iput v0, p0, LS/c;->i:I

    .line 42
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->f:Lcom/google/android/maps/driveabout/vector/q;

    iput-object v0, p0, LS/c;->j:Lcom/google/android/maps/driveabout/vector/q;

    .line 46
    iput-object p1, p0, LS/c;->a:Landroid/content/res/Resources;

    .line 47
    const v0, 0x7f0b0050

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LS/c;->c:F

    .line 48
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/aV;-><init>(F)V

    iput-object v0, p0, LS/c;->b:Lcom/google/android/maps/driveabout/vector/aV;

    .line 49
    new-instance v0, LE/i;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, LE/i;-><init>(I)V

    iput-object v0, p0, LS/c;->f:LE/i;

    .line 50
    return-void
.end method

.method private static a(Ljava/util/HashSet;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 134
    invoke-virtual {p0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-nez v0, :cond_9

    .line 135
    const-string v0, ""

    .line 148
    :goto_8
    return-object v0

    .line 137
    :cond_9
    invoke-virtual {p0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 138
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    const/4 v0, 0x1

    .line 140
    :goto_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 141
    if-nez v0, :cond_2c

    .line 142
    const-string v1, ", "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    .line 146
    :goto_21
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_13

    .line 144
    :cond_2c
    const/4 v1, 0x0

    goto :goto_21

    .line 148
    :cond_2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method private a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x0

    .line 103
    const/high16 v6, -0x100

    .line 104
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_51

    .line 105
    const/4 v6, -0x1

    .line 109
    :cond_9
    :goto_9
    iget-object v0, p0, LS/c;->b:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v2, p0, LS/c;->g:Ljava/lang/String;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    iget v5, p0, LS/c;->c:F

    move-object v1, p1

    move v8, v7

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/maps/driveabout/vector/aV;->a(LD/a;Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v0

    iput-object v0, p0, LS/c;->e:LD/b;

    .line 113
    iget-object v2, p0, LS/c;->b:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v3, p0, LS/c;->g:Ljava/lang/String;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v5, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    iget v6, p0, LS/c;->c:F

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZ)[F

    move-result-object v0

    iput-object v0, p0, LS/c;->d:[F

    .line 116
    iget-object v0, p0, LS/c;->f:LE/i;

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    .line 117
    iget-object v0, p0, LS/c;->e:LD/b;

    invoke-virtual {v0}, LD/b;->b()F

    move-result v0

    .line 118
    iget-object v1, p0, LS/c;->e:LD/b;

    invoke-virtual {v1}, LD/b;->c()F

    move-result v1

    .line 120
    iget-object v2, p0, LS/c;->f:LE/i;

    invoke-virtual {v2, v9, v9}, LE/i;->a(FF)V

    .line 121
    iget-object v2, p0, LS/c;->f:LE/i;

    invoke-virtual {v2, v9, v1}, LE/i;->a(FF)V

    .line 122
    iget-object v2, p0, LS/c;->f:LE/i;

    invoke-virtual {v2, v0, v9}, LE/i;->a(FF)V

    .line 123
    iget-object v2, p0, LS/c;->f:LE/i;

    invoke-virtual {v2, v0, v1}, LE/i;->a(FF)V

    .line 124
    return-void

    .line 106
    :cond_51
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_9

    .line 107
    const v6, -0x3f3f40

    goto :goto_9
.end method

.method private j()V
    .registers 2

    .prologue
    .line 127
    iget-object v0, p0, LS/c;->e:LD/b;

    if-eqz v0, :cond_c

    .line 128
    iget-object v0, p0, LS/c;->e:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, LS/c;->e:LD/b;

    .line 131
    :cond_c
    return-void
.end method


# virtual methods
.method public a(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 53
    iput p1, p0, LS/c;->h:I

    .line 54
    iput p2, p0, LS/c;->i:I

    .line 55
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 63
    iget-object v0, p0, LS/c;->g:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 100
    :goto_5
    return-void

    .line 67
    :cond_6
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 69
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v1

    iget-object v2, p0, LS/c;->j:Lcom/google/android/maps/driveabout/vector/q;

    if-eq v1, v2, :cond_1b

    .line 71
    invoke-direct {p0}, LS/c;->j()V

    .line 72
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v1

    iput-object v1, p0, LS/c;->j:Lcom/google/android/maps/driveabout/vector/q;

    .line 75
    :cond_1b
    iget-object v1, p0, LS/c;->e:LD/b;

    if-nez v1, :cond_26

    .line 76
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v1

    invoke-direct {p0, p1, v1}, LS/c;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V

    .line 79
    :cond_26
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 86
    invoke-virtual {p2}, LC/a;->k()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, LS/c;->d:[F

    aget v2, v2, v4

    sub-float/2addr v1, v2

    iget v2, p0, LS/c;->h:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, p0, LS/c;->i:I

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 89
    iget-object v1, p0, LS/c;->d:[F

    aget v1, v1, v4

    iget-object v2, p0, LS/c;->d:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    const/high16 v3, 0x3f80

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 91
    invoke-virtual {p1}, LD/a;->p()V

    .line 92
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 93
    iget-object v1, p0, LS/c;->f:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    .line 94
    iget-object v1, p1, LD/a;->f:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 96
    iget-object v1, p0, LS/c;->e:LD/b;

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 97
    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-interface {v0, v1, v4, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 98
    invoke-virtual {p1}, LD/a;->q()V

    .line 99
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_5
.end method

.method public a(Ljava/util/HashSet;Ljava/util/HashSet;ILcom/google/android/maps/driveabout/vector/q;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 153
    const/4 v0, -0x1

    if-eq p3, v0, :cond_4c

    .line 155
    :goto_7
    invoke-static {p1}, LS/c;->a(Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v0

    .line 156
    invoke-static {p2}, LS/c;->a(Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v1

    .line 158
    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_55

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_55

    .line 159
    iget-object v2, p0, LS/c;->a:Landroid/content/res/Resources;

    const v3, 0x7f0d0113

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v1, v4, v8

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v9

    const/4 v1, 0x4

    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 171
    :goto_3e
    iget-object v1, p0, LS/c;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4b

    .line 172
    iput-object v0, p0, LS/c;->g:Ljava/lang/String;

    .line 173
    invoke-direct {p0}, LS/c;->j()V

    .line 175
    :cond_4b
    return-void

    .line 153
    :cond_4c
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result p3

    goto :goto_7

    .line 161
    :cond_55
    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_73

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_73

    .line 162
    iget-object v0, p0, LS/c;->a:Landroid/content/res/Resources;

    const v1, 0x7f0d0112

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3e

    .line 164
    :cond_73
    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_93

    .line 165
    iget-object v1, p0, LS/c;->a:Landroid/content/res/Resources;

    const v2, 0x7f0d0115

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    aput-object v0, v3, v8

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3e

    .line 168
    :cond_93
    iget-object v0, p0, LS/c;->a:Landroid/content/res/Resources;

    const v2, 0x7f0d0114

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    aput-object v1, v3, v8

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3e
.end method

.method public c(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 179
    invoke-direct {p0}, LS/c;->j()V

    .line 180
    iget-object v0, p0, LS/c;->b:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->a()V

    .line 181
    return-void
.end method
