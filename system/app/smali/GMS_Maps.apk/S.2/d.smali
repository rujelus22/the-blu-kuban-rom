.class public LS/d;
.super LS/a;
.source "SourceFile"


# static fields
.field private static b:LE/o;

.field private static c:LE/d;

.field private static d:LE/d;

.field private static e:LE/d;


# instance fields
.field private a:I

.field private f:F

.field private g:D

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:[F

.field private k:[F

.field private l:F

.field private m:F

.field private final n:Lcom/google/android/maps/driveabout/vector/aV;

.field private o:LD/b;

.field private p:LD/b;

.field private final q:LE/i;

.field private final r:LE/i;

.field private volatile s:Lh/r;

.field private t:F


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 92
    new-instance v0, LE/o;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LE/o;-><init>(I)V

    sput-object v0, LS/d;->b:LE/o;

    .line 93
    new-instance v0, LE/d;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    sput-object v0, LS/d;->c:LE/d;

    .line 94
    new-instance v0, LE/d;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    sput-object v0, LS/d;->d:LE/d;

    .line 95
    new-instance v0, LE/d;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    sput-object v0, LS/d;->e:LE/d;

    .line 97
    sget-object v0, LS/d;->b:LE/o;

    invoke-virtual {v0, v3, v3, v3}, LE/o;->a(FFF)V

    .line 98
    sget-object v0, LS/d;->b:LE/o;

    const/high16 v1, 0x4170

    invoke-virtual {v0, v3, v1, v3}, LE/o;->a(FFF)V

    .line 99
    sget-object v0, LS/d;->b:LE/o;

    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1, v3, v3}, LE/o;->a(FFF)V

    .line 100
    sget-object v0, LS/d;->b:LE/o;

    const/high16 v1, 0x3f80

    const/high16 v2, 0x4170

    invoke-virtual {v0, v1, v2, v3}, LE/o;->a(FFF)V

    .line 102
    sget-object v0, LS/d;->c:LE/d;

    invoke-virtual {v0, v5, v4}, LE/d;->a(SS)V

    .line 103
    sget-object v0, LS/d;->c:LE/d;

    invoke-virtual {v0, v4, v6}, LE/d;->a(SS)V

    .line 104
    sget-object v0, LS/d;->c:LE/d;

    invoke-virtual {v0, v6, v7}, LE/d;->a(SS)V

    .line 106
    sget-object v0, LS/d;->d:LE/d;

    invoke-virtual {v0, v4, v5}, LE/d;->a(SS)V

    .line 107
    sget-object v0, LS/d;->d:LE/d;

    invoke-virtual {v0, v5, v7}, LE/d;->a(SS)V

    .line 108
    sget-object v0, LS/d;->d:LE/d;

    invoke-virtual {v0, v7, v6}, LE/d;->a(SS)V

    .line 110
    sget-object v0, LS/d;->e:LE/d;

    invoke-virtual {v0, v4}, LE/d;->a(S)V

    .line 111
    sget-object v0, LS/d;->e:LE/d;

    invoke-virtual {v0, v5}, LE/d;->a(S)V

    .line 112
    sget-object v0, LS/d;->e:LE/d;

    invoke-virtual {v0, v6}, LE/d;->a(S)V

    .line 113
    sget-object v0, LS/d;->e:LE/d;

    invoke-virtual {v0, v7}, LE/d;->a(S)V

    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    const/16 v2, 0x8

    .line 116
    invoke-direct {p0}, LS/a;-><init>()V

    .line 68
    iput-object v0, p0, LS/d;->h:Ljava/lang/String;

    .line 69
    iput-object v0, p0, LS/d;->i:Ljava/lang/String;

    .line 117
    const v0, 0x7f0b004f

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LS/d;->f:F

    .line 118
    const v0, 0x7f0b004e

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LS/d;->a:I

    .line 119
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/aV;-><init>(F)V

    iput-object v0, p0, LS/d;->n:Lcom/google/android/maps/driveabout/vector/aV;

    .line 121
    new-instance v0, LE/i;

    invoke-direct {v0, v2}, LE/i;-><init>(I)V

    iput-object v0, p0, LS/d;->q:LE/i;

    .line 122
    new-instance v0, LE/i;

    invoke-direct {v0, v2}, LE/i;-><init>(I)V

    iput-object v0, p0, LS/d;->r:LE/i;

    .line 123
    new-instance v0, Lh/r;

    const-wide/16 v1, 0x96

    invoke-direct {v0, v1, v2}, Lh/r;-><init>(J)V

    iput-object v0, p0, LS/d;->s:Lh/r;

    .line 124
    iget-object v0, p0, LS/d;->s:Lh/r;

    const/16 v1, 0xa

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Lh/r;->b(II)V

    .line 126
    const v0, 0x7f0b0085

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LS/d;->t:F

    .line 128
    return-void
.end method

.method private a(LD/a;IFFFFZ)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/high16 v2, 0x3f80

    .line 238
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 240
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 243
    invoke-interface {v0, p3, p4, p5, p6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 244
    int-to-float v1, p2

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidth(F)V

    .line 245
    int-to-float v1, p2

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glPointSize(F)V

    .line 248
    iget v1, p0, LS/d;->m:F

    invoke-interface {v0, v1, v2, v2}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 250
    sget-object v1, LS/d;->c:LE/d;

    invoke-virtual {v1, p1, v5}, LE/d;->a(LD/a;I)V

    .line 251
    if-eqz p7, :cond_28

    .line 252
    sget-object v1, LS/d;->e:LE/d;

    invoke-virtual {v1, p1, v4}, LE/d;->a(LD/a;I)V

    .line 256
    :cond_28
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 257
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 260
    iget v1, p0, LS/d;->l:F

    invoke-interface {v0, v1, v2, v2}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 263
    const/high16 v1, -0x3e90

    invoke-interface {v0, v3, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 265
    sget-object v1, LS/d;->d:LE/d;

    invoke-virtual {v1, p1, v5}, LE/d;->a(LD/a;I)V

    .line 266
    if-eqz p7, :cond_44

    .line 268
    sget-object v1, LS/d;->e:LE/d;

    invoke-virtual {v1, p1, v4}, LE/d;->a(LD/a;I)V

    .line 271
    :cond_44
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 272
    return-void
.end method

.method private d(LD/a;)V
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x1

    const/high16 v7, 0x4000

    const/high16 v6, 0x3f80

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 204
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 205
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 208
    invoke-virtual {p1}, LD/a;->p()V

    .line 210
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 212
    iget-object v1, p1, LD/a;->f:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 213
    iget-object v1, p0, LS/d;->q:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    .line 214
    invoke-interface {v0, v7, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 215
    iget-object v1, p0, LS/d;->k:[F

    aget v1, v1, v4

    iget-object v2, p0, LS/d;->k:[F

    aget v2, v2, v8

    invoke-interface {v0, v1, v2, v6}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 216
    iget-object v1, p0, LS/d;->o:LD/b;

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 217
    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-interface {v0, v1, v4, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 220
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 221
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 224
    iget-object v1, p1, LD/a;->f:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 225
    iget-object v1, p0, LS/d;->r:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    .line 226
    const/high16 v1, 0x3fc0

    iget v2, p0, LS/d;->f:F

    mul-float/2addr v1, v2

    neg-float v1, v1

    invoke-interface {v0, v7, v1, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 227
    iget-object v1, p0, LS/d;->j:[F

    aget v1, v1, v4

    iget-object v2, p0, LS/d;->j:[F

    aget v2, v2, v8

    invoke-interface {v0, v1, v2, v6}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 228
    iget-object v1, p0, LS/d;->p:LD/b;

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 229
    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-interface {v0, v1, v4, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 231
    invoke-virtual {p1}, LD/a;->q()V

    .line 232
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 233
    return-void
.end method

.method private j()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 275
    iget-object v0, p0, LS/d;->o:LD/b;

    if-eqz v0, :cond_c

    .line 276
    iget-object v0, p0, LS/d;->o:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 277
    iput-object v1, p0, LS/d;->o:LD/b;

    .line 279
    :cond_c
    iget-object v0, p0, LS/d;->p:LD/b;

    if-eqz v0, :cond_17

    .line 280
    iget-object v0, p0, LS/d;->p:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 281
    iput-object v1, p0, LS/d;->p:LD/b;

    .line 283
    :cond_17
    return-void
.end method

.method private k()V
    .registers 12

    .prologue
    .line 307
    iget v0, p0, LS/d;->a:I

    int-to-double v0, v0

    iget-wide v2, p0, LS/d;->g:D

    div-double v3, v0, v2

    .line 308
    const-wide v0, 0x400a3f28fd4f4b98L

    mul-double v5, v3, v0

    .line 311
    const-string v0, ""

    .line 312
    const-string v1, ""

    .line 314
    sget-object v2, Lcom/google/googlenav/common/util/u;->a:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    :goto_17
    if-ltz v2, :cond_49

    .line 315
    sget-object v7, Lcom/google/googlenav/common/util/u;->a:[I

    aget v7, v7, v2

    int-to-double v7, v7

    sub-double v7, v3, v7

    const-wide/16 v9, 0x0

    cmpl-double v7, v7, v9

    if-ltz v7, :cond_cc

    .line 316
    sget-object v0, Lcom/google/googlenav/common/util/u;->a:[I

    aget v2, v0, v2

    .line 317
    const/16 v0, 0x3e8

    if-ge v2, v0, :cond_b4

    .line 318
    const/16 v0, 0x101

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v4

    invoke-static {v0, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 327
    :goto_42
    int-to-double v2, v2

    iget-wide v7, p0, LS/d;->g:D

    mul-double/2addr v2, v7

    double-to-float v2, v2

    iput v2, p0, LS/d;->l:F

    .line 333
    :cond_49
    sget-object v2, Lcom/google/googlenav/common/util/u;->b:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    :goto_4e
    if-ltz v2, :cond_87

    .line 334
    sget-object v3, Lcom/google/googlenav/common/util/u;->b:[I

    aget v3, v3, v2

    int-to-double v3, v3

    sub-double v3, v5, v3

    const-wide/16 v7, 0x0

    cmpl-double v3, v3, v7

    if-ltz v3, :cond_e7

    .line 335
    sget-object v1, Lcom/google/googlenav/common/util/u;->b:[I

    aget v2, v1, v2

    .line 336
    const/16 v1, 0x14a0

    if-ge v2, v1, :cond_d0

    .line 337
    const/16 v1, 0xff

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 345
    :goto_79
    iget-wide v3, p0, LS/d;->g:D

    const-wide v5, 0x400a3f28fd4f4b98L

    div-double/2addr v3, v5

    int-to-double v5, v2

    mul-double v2, v3, v5

    double-to-float v2, v2

    iput v2, p0, LS/d;->m:F

    .line 351
    :cond_87
    iget-object v2, p0, LS/d;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9d

    .line 352
    iput-object v1, p0, LS/d;->i:Ljava/lang/String;

    .line 353
    iget-object v1, p0, LS/d;->o:LD/b;

    if-eqz v1, :cond_9d

    .line 354
    iget-object v1, p0, LS/d;->o:LD/b;

    invoke-virtual {v1}, LD/b;->g()V

    .line 355
    const/4 v1, 0x0

    iput-object v1, p0, LS/d;->o:LD/b;

    .line 359
    :cond_9d
    iget-object v1, p0, LS/d;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b3

    .line 360
    iput-object v0, p0, LS/d;->h:Ljava/lang/String;

    .line 361
    iget-object v0, p0, LS/d;->p:LD/b;

    if-eqz v0, :cond_b3

    .line 362
    iget-object v0, p0, LS/d;->p:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 363
    const/4 v0, 0x0

    iput-object v0, p0, LS/d;->p:LD/b;

    .line 366
    :cond_b3
    return-void

    .line 322
    :cond_b4
    const/16 v0, 0x100

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    div-int/lit16 v7, v2, 0x3e8

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v4

    invoke-static {v0, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_42

    .line 314
    :cond_cc
    add-int/lit8 v2, v2, -0x1

    goto/16 :goto_17

    .line 341
    :cond_d0
    const/16 v1, 0x102

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    div-int/lit16 v5, v2, 0x14a0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_79

    .line 333
    :cond_e7
    add-int/lit8 v2, v2, -0x1

    goto/16 :goto_4e
.end method


# virtual methods
.method a(LD/a;LC/a;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 184
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 186
    iget-object v1, p0, LS/d;->s:Lh/r;

    invoke-virtual {v1, p1}, Lh/r;->a(LD/a;)V

    .line 187
    iget-object v1, p0, LS/d;->s:Lh/r;

    invoke-virtual {v1}, Lh/r;->a()F

    move-result v1

    iget-object v2, p0, LS/d;->s:Lh/r;

    invoke-virtual {v2}, Lh/r;->b()F

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 190
    invoke-direct {p0, p1}, LS/d;->d(LD/a;)V

    .line 193
    sget-object v0, LS/d;->b:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    .line 196
    const/4 v2, 0x5

    const/high16 v3, 0x3f80

    const/high16 v4, 0x3f80

    const/high16 v5, 0x3f80

    const v6, 0x3e4ccccd

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, LS/d;->a(LD/a;IFFFFZ)V

    .line 199
    const/4 v2, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f80

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, LS/d;->a(LD/a;IFFFFZ)V

    .line 200
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v11, 0x1

    const/4 v8, 0x0

    const/4 v7, -0x1

    const/high16 v6, -0x100

    const/4 v10, 0x0

    .line 133
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->G()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-wide v0, p0, LS/d;->g:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_19

    .line 181
    :cond_18
    :goto_18
    return-void

    .line 137
    :cond_19
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v9

    .line 142
    iget-object v0, p0, LS/d;->o:LD/b;

    if-nez v0, :cond_68

    .line 143
    iget-object v0, p0, LS/d;->n:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v2, p0, LS/d;->i:Ljava/lang/String;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    iget v5, p0, LS/d;->f:F

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/maps/driveabout/vector/aV;->a(LD/a;Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v0

    iput-object v0, p0, LS/d;->o:LD/b;

    .line 147
    iget-object v0, p0, LS/d;->n:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v1, p0, LS/d;->i:Ljava/lang/String;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    iget v4, p0, LS/d;->f:F

    move v5, v11

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZ)[F

    move-result-object v0

    iput-object v0, p0, LS/d;->k:[F

    .line 150
    iget-object v0, p0, LS/d;->q:LE/i;

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    .line 151
    iget-object v0, p0, LS/d;->o:LD/b;

    invoke-virtual {v0}, LD/b;->b()F

    move-result v0

    .line 152
    iget-object v1, p0, LS/d;->o:LD/b;

    invoke-virtual {v1}, LD/b;->c()F

    move-result v1

    .line 154
    iget-object v2, p0, LS/d;->q:LE/i;

    invoke-virtual {v2, v10, v10}, LE/i;->a(FF)V

    .line 155
    iget-object v2, p0, LS/d;->q:LE/i;

    invoke-virtual {v2, v10, v1}, LE/i;->a(FF)V

    .line 156
    iget-object v2, p0, LS/d;->q:LE/i;

    invoke-virtual {v2, v0, v10}, LE/i;->a(FF)V

    .line 157
    iget-object v2, p0, LS/d;->q:LE/i;

    invoke-virtual {v2, v0, v1}, LE/i;->a(FF)V

    .line 160
    :cond_68
    iget-object v0, p0, LS/d;->p:LD/b;

    if-nez v0, :cond_b3

    .line 161
    iget-object v0, p0, LS/d;->n:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v2, p0, LS/d;->h:Ljava/lang/String;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    iget v5, p0, LS/d;->f:F

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/maps/driveabout/vector/aV;->a(LD/a;Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v0

    iput-object v0, p0, LS/d;->p:LD/b;

    .line 165
    iget-object v0, p0, LS/d;->n:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v1, p0, LS/d;->h:Ljava/lang/String;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    iget v4, p0, LS/d;->f:F

    move v5, v11

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZ)[F

    move-result-object v0

    iput-object v0, p0, LS/d;->j:[F

    .line 168
    iget-object v0, p0, LS/d;->r:LE/i;

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    .line 169
    iget-object v0, p0, LS/d;->p:LD/b;

    invoke-virtual {v0}, LD/b;->b()F

    move-result v0

    .line 170
    iget-object v1, p0, LS/d;->p:LD/b;

    invoke-virtual {v1}, LD/b;->c()F

    move-result v1

    .line 172
    iget-object v2, p0, LS/d;->r:LE/i;

    invoke-virtual {v2, v10, v10}, LE/i;->a(FF)V

    .line 173
    iget-object v2, p0, LS/d;->r:LE/i;

    invoke-virtual {v2, v10, v1}, LE/i;->a(FF)V

    .line 174
    iget-object v2, p0, LS/d;->r:LE/i;

    invoke-virtual {v2, v0, v10}, LE/i;->a(FF)V

    .line 175
    iget-object v2, p0, LS/d;->r:LE/i;

    invoke-virtual {v2, v0, v1}, LE/i;->a(FF)V

    .line 178
    :cond_b3
    invoke-interface {v9}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 179
    invoke-virtual {p0, p1, p2}, LS/d;->a(LD/a;LC/a;)V

    .line 180
    invoke-interface {v9}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_18
.end method

.method public b(LC/a;LD/a;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 291
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->G()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 293
    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->e()D

    move-result-wide v0

    invoke-virtual {p1}, LC/a;->y()F

    move-result v2

    float-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, LS/d;->g:D

    .line 295
    invoke-direct {p0}, LS/d;->k()V

    .line 297
    :cond_1d
    invoke-super {p0, p1, p2}, LS/a;->b(LC/a;LD/a;)Z

    move-result v0

    return v0
.end method

.method public c(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 302
    invoke-direct {p0}, LS/d;->j()V

    .line 303
    iget-object v0, p0, LS/d;->n:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->a()V

    .line 304
    return-void
.end method

.method public i()V
    .registers 4

    .prologue
    .line 371
    const/16 v0, 0x1e

    .line 373
    invoke-virtual {p0}, LS/d;->h()LS/b;

    move-result-object v1

    invoke-virtual {v1}, LS/b;->e()Z

    move-result v1

    if-eqz v1, :cond_27

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-nez v1, :cond_27

    .line 376
    const/16 v0, 0x64

    .line 378
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->au()Z

    move-result v1

    if-eqz v1, :cond_27

    .line 379
    int-to-float v0, v0

    iget v1, p0, LS/d;->t:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 383
    :cond_27
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->at()Z

    move-result v1

    if-eqz v1, :cond_33

    .line 385
    add-int/lit8 v0, v0, 0x60

    .line 391
    :cond_33
    iget-object v1, p0, LS/d;->s:Lh/r;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v0}, Lh/r;->a(II)V

    .line 392
    return-void
.end method
