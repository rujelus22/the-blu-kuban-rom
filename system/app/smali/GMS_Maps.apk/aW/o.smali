.class public Law/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/p;


# instance fields
.field final synthetic a:Law/h;

.field private b:Ljava/util/Vector;

.field private c:Z

.field private final d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method private constructor <init>(Law/h;)V
    .registers 4
    .parameter

    .prologue
    .line 602
    iput-object p1, p0, Law/o;->a:Law/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 589
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Law/o;->b:Ljava/util/Vector;

    .line 594
    const/4 v0, 0x0

    iput-boolean v0, p0, Law/o;->c:Z

    .line 603
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ab;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 604
    return-void
.end method

.method synthetic constructor <init>(Law/h;Law/i;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 583
    invoke-direct {p0, p1}, Law/o;-><init>(Law/h;)V

    return-void
.end method

.method private constructor <init>(Law/h;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 606
    iput-object p1, p0, Law/o;->a:Law/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 589
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Law/o;->b:Ljava/util/Vector;

    .line 594
    const/4 v0, 0x0

    iput-boolean v0, p0, Law/o;->c:Z

    .line 607
    invoke-virtual {p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->clone()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 608
    return-void
.end method

.method synthetic constructor <init>(Law/h;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Law/i;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 583
    invoke-direct {p0, p1, p2}, Law/o;-><init>(Law/h;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method static synthetic a(Law/o;)V
    .registers 1
    .parameter

    .prologue
    .line 583
    invoke-direct {p0}, Law/o;->b()V

    return-void
.end method

.method static synthetic a(Law/o;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 583
    invoke-direct {p0, p1}, Law/o;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Law/o;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 583
    invoke-direct {p0, p1}, Law/o;->b(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 616
    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 617
    return-void
.end method

.method static synthetic b(Law/o;Z)Law/m;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 583
    invoke-direct {p0, p1}, Law/o;->c(Z)Law/m;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 583
    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    const/16 v2, 0x19

    .line 647
    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 648
    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    .line 650
    :cond_10
    return-void
.end method

.method private b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 636
    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x24

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 639
    return-void
.end method

.method private c(Z)Law/m;
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 732
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 734
    monitor-enter p0

    .line 735
    :try_start_6
    iget-object v2, p0, Law/o;->b:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_10

    .line 736
    monitor-exit p0

    .line 764
    :goto_f
    return-object v0

    .line 738
    :cond_10
    if-eqz p1, :cond_1b

    iget-boolean v2, p0, Law/o;->c:Z

    if-nez v2, :cond_1b

    .line 739
    monitor-exit p0

    goto :goto_f

    .line 755
    :catchall_18
    move-exception v0

    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_6 .. :try_end_1a} :catchall_18

    throw v0

    .line 741
    :cond_1b
    :try_start_1b
    new-instance v2, Law/m;

    iget-object v0, p0, Law/o;->a:Law/h;

    iget-object v3, p0, Law/o;->b:Ljava/util/Vector;

    iget-object v4, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v2, v0, v3, v4}, Law/m;-><init>(Law/h;Ljava/util/Vector;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 742
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Law/o;->b:Ljava/util/Vector;

    .line 743
    const/4 v0, 0x0

    iput-boolean v0, p0, Law/o;->c:Z

    .line 755
    monitor-exit p0
    :try_end_31
    .catchall {:try_start_1b .. :try_end_31} :catchall_18

    .line 760
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_35
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 761
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_35

    :cond_4d
    move-object v0, v2

    .line 764
    goto :goto_f
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 768
    iget-boolean v0, p0, Law/o;->c:Z

    return v0
.end method

.method static synthetic c(Law/o;)Z
    .registers 2
    .parameter

    .prologue
    .line 583
    invoke-direct {p0}, Law/o;->c()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(I[BZZ)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 692
    .line 693
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Law/o;->a(I[BZZZ)V

    .line 694
    return-void
.end method

.method public final a(I[BZZZ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 699
    new-instance v0, Law/t;

    const/4 v6, 0x0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Law/t;-><init>(I[BZZZLjava/lang/Object;)V

    invoke-virtual {p0, v0}, Law/o;->c(Law/g;)V

    .line 701
    return-void
.end method

.method public a(Law/q;)V
    .registers 3
    .parameter

    .prologue
    .line 773
    invoke-static {}, Law/h;->z()Law/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Law/h;->a(Law/q;)V

    .line 774
    return-void
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 654
    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 655
    return-void
.end method

.method public declared-synchronized a()Z
    .registers 2

    .prologue
    .line 808
    monitor-enter p0

    :try_start_1
    invoke-static {}, Law/h;->z()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->f()Z
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_b

    move-result v0

    monitor-exit p0

    return v0

    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Law/q;)V
    .registers 3
    .parameter

    .prologue
    .line 778
    invoke-static {}, Law/h;->z()Law/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Law/h;->b(Law/q;)V

    .line 779
    return-void
.end method

.method public c(I)V
    .registers 4
    .parameter

    .prologue
    .line 643
    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x19

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 644
    return-void
.end method

.method public c(Law/g;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 674
    iget-object v0, p0, Law/o;->a:Law/h;

    iget-boolean v0, v0, Law/h;->g:Z

    if-eqz v0, :cond_e

    .line 675
    iget-object v0, p0, Law/o;->a:Law/h;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Law/h;->a(IZLjava/lang/String;)V

    .line 678
    :cond_e
    monitor-enter p0

    .line 679
    :try_start_f
    invoke-interface {p1}, Law/g;->s_()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 680
    const/4 v0, 0x1

    iput-boolean v0, p0, Law/o;->c:Z

    .line 682
    :cond_18
    iget-object v0, p0, Law/o;->b:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 683
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_f .. :try_end_1e} :catchall_32

    .line 684
    invoke-interface {p1}, Law/g;->s_()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-virtual {p0}, Law/o;->a()Z

    move-result v0

    if-nez v0, :cond_31

    .line 685
    iget-object v0, p0, Law/o;->a:Law/h;

    iget-object v0, v0, Law/h;->e:Law/l;

    invoke-static {v0}, Law/l;->a(Law/l;)V

    .line 687
    :cond_31
    return-void

    .line 683
    :catchall_32
    move-exception v0

    :try_start_33
    monitor-exit p0
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_32

    throw v0
.end method

.method public n()Z
    .registers 2

    .prologue
    .line 788
    invoke-static {}, Law/h;->z()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->n()Z

    move-result v0

    return v0
.end method

.method public p()J
    .registers 3

    .prologue
    .line 783
    invoke-static {}, Law/h;->z()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->p()J

    move-result-wide v0

    return-wide v0
.end method

.method public w()J
    .registers 3

    .prologue
    .line 793
    invoke-static {}, Law/h;->z()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->w()J

    move-result-wide v0

    return-wide v0
.end method

.method public x()Ljava/lang/String;
    .registers 3

    .prologue
    .line 621
    iget-object v0, p0, Law/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
