.class public Law/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/p;


# static fields
.field private static I:I

.field private static volatile K:Law/h;


# instance fields
.field private volatile A:I

.field private volatile B:I

.field private volatile C:I

.field private D:Lcom/google/googlenav/bk;

.field private final E:Lcom/google/googlenav/common/a;

.field private volatile F:I

.field private G:Ljava/lang/Throwable;

.field private H:I

.field private J:I

.field private L:Z

.field protected volatile a:Ljava/lang/String;

.field protected final b:Ljava/lang/String;

.field protected final c:Ljava/lang/String;

.field protected final d:Z

.field protected final e:Law/l;

.field protected f:Law/o;

.field protected volatile g:Z

.field protected h:I

.field protected i:I

.field protected j:Lcom/google/googlenav/common/io/g;

.field protected k:Law/f;

.field private volatile l:Z

.field private final m:Ljava/util/List;

.field private n:Law/p;

.field private final o:Ljava/lang/String;

.field private p:Ljava/lang/Long;

.field private final q:Ljava/util/List;

.field private final r:Ljava/util/Random;

.field private s:J

.field private volatile t:Z

.field private volatile u:I

.field private volatile v:J

.field private volatile w:J

.field private volatile x:Z

.field private y:J

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 367
    const/4 v0, 0x0

    sput v0, Law/h;->I:I

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const-wide/high16 v4, -0x8000

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 1462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Law/h;->q:Ljava/util/List;

    .line 217
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Law/h;->r:Ljava/util/Random;

    .line 261
    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Law/h;->s:J

    .line 273
    iput-boolean v2, p0, Law/h;->t:Z

    .line 287
    iput-wide v4, p0, Law/h;->v:J

    .line 293
    iput-wide v4, p0, Law/h;->w:J

    .line 315
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Law/h;->y:J

    .line 322
    iput-wide v4, p0, Law/h;->z:J

    .line 325
    iput v2, p0, Law/h;->A:I

    .line 331
    iput v2, p0, Law/h;->B:I

    .line 338
    iput v2, p0, Law/h;->C:I

    .line 352
    iput v3, p0, Law/h;->F:I

    .line 364
    iput v3, p0, Law/h;->H:I

    .line 373
    iput v3, p0, Law/h;->J:I

    .line 386
    iput-boolean v2, p0, Law/h;->L:Z

    .line 1463
    if-eqz p1, :cond_3b

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_41

    .line 1464
    :cond_3b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1466
    :cond_41
    invoke-static {p1}, Law/h;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Law/h;->a:Ljava/lang/String;

    .line 1467
    iput-object p3, p0, Law/h;->c:Ljava/lang/String;

    .line 1468
    iput-object p2, p0, Law/h;->b:Ljava/lang/String;

    .line 1469
    iput-object p4, p0, Law/h;->o:Ljava/lang/String;

    .line 1470
    iput-boolean p5, p0, Law/h;->d:Z

    .line 1471
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->l()Lcom/google/googlenav/common/io/g;

    move-result-object v0

    iput-object v0, p0, Law/h;->j:Lcom/google/googlenav/common/io/g;

    .line 1472
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    iput-object v0, p0, Law/h;->E:Lcom/google/googlenav/common/a;

    .line 1473
    new-instance v0, Law/f;

    iget-object v1, p0, Law/h;->E:Lcom/google/googlenav/common/a;

    invoke-direct {v0, p0, v1}, Law/f;-><init>(Law/h;Lcom/google/googlenav/common/a;)V

    iput-object v0, p0, Law/h;->k:Law/f;

    .line 1474
    iput v2, p0, Law/h;->h:I

    .line 1475
    iput v2, p0, Law/h;->i:I

    .line 1476
    new-instance v0, Law/l;

    iget-object v1, p0, Law/h;->a:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v6}, Law/l;-><init>(Law/h;Ljava/lang/String;Law/i;)V

    iput-object v0, p0, Law/h;->e:Law/l;

    .line 1477
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Law/h;->m:Ljava/util/List;

    .line 1478
    new-instance v0, Law/o;

    invoke-direct {v0, p0, v6}, Law/o;-><init>(Law/h;Law/i;)V

    iput-object v0, p0, Law/h;->f:Law/o;

    .line 1479
    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    iget-object v1, p0, Law/h;->f:Law/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1487
    return-void
.end method

.method static synthetic A()I
    .registers 2

    .prologue
    .line 64
    sget v0, Law/h;->I:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Law/h;->I:I

    return v0
.end method

.method private B()Law/o;
    .registers 4

    .prologue
    .line 1339
    new-instance v0, Law/o;

    iget-object v1, p0, Law/h;->f:Law/o;

    invoke-static {v1}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Law/o;-><init>(Law/h;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Law/i;)V

    .line 1340
    iget-object v1, p0, Law/h;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1341
    return-object v0
.end method

.method private static C()J
    .registers 3

    .prologue
    .line 1579
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "SessionID"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v0

    .line 1581
    if-eqz v0, :cond_24

    .line 1584
    :try_start_10
    invoke-interface {v0}, Ljava/io/DataInput;->readLong()J
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_13} :catch_15

    move-result-wide v0

    .line 1592
    :goto_14
    return-wide v0

    .line 1585
    :catch_15
    move-exception v0

    .line 1588
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "SessionID"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 1592
    :cond_24
    const-wide/16 v0, 0x0

    goto :goto_14
.end method

.method private declared-synchronized D()V
    .registers 3

    .prologue
    .line 1849
    monitor-enter p0

    const-wide/high16 v0, -0x8000

    :try_start_3
    iput-wide v0, p0, Law/h;->z:J

    .line 1850
    const/4 v0, 0x0

    iput-boolean v0, p0, Law/h;->x:Z

    .line 1851
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Law/h;->y:J

    .line 1852
    const/4 v0, -0x1

    iput v0, p0, Law/h;->H:I
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_11

    .line 1853
    monitor-exit p0

    return-void

    .line 1849
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private E()Lcom/google/googlenav/bk;
    .registers 2

    .prologue
    .line 2208
    iget-object v0, p0, Law/h;->D:Lcom/google/googlenav/bk;

    if-nez v0, :cond_b

    .line 2209
    new-instance v0, Lcom/google/googlenav/bk;

    invoke-direct {v0}, Lcom/google/googlenav/bk;-><init>()V

    iput-object v0, p0, Law/h;->D:Lcom/google/googlenav/bk;

    .line 2211
    :cond_b
    iget-object v0, p0, Law/h;->D:Lcom/google/googlenav/bk;

    return-object v0
.end method

.method static synthetic a(Law/h;J)J
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 64
    iput-wide p1, p0, Law/h;->v:J

    return-wide p1
.end method

.method public static a()Law/h;
    .registers 1

    .prologue
    .line 1414
    sget-object v0, Law/h;->K:Law/h;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Law/h;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-static {p0, p1, p2, p3, p4}, Law/h;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Law/h;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Law/h;)Law/o;
    .registers 2
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Law/h;->B()Law/o;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Law/h;Ljava/lang/Long;)Ljava/lang/Long;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    iput-object p1, p0, Law/h;->p:Ljava/lang/Long;

    return-object p1
.end method

.method private a(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1502
    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/o;

    .line 1503
    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_6

    .line 1505
    :cond_1a
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1496
    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/o;

    .line 1497
    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_6

    .line 1499
    :cond_1a
    return-void
.end method

.method private a(ILjava/lang/Throwable;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const-wide/16 v3, 0x7d0

    .line 1864
    const/4 v0, 0x0

    .line 1865
    monitor-enter p0

    .line 1868
    if-eqz p2, :cond_f

    :try_start_6
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1869
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 1875
    :cond_f
    iget-object v1, p0, Law/h;->j:Lcom/google/googlenav/common/io/g;

    invoke-interface {v1}, Lcom/google/googlenav/common/io/g;->c()V

    .line 1876
    iput-object p2, p0, Law/h;->G:Ljava/lang/Throwable;

    .line 1877
    iput p1, p0, Law/h;->H:I

    .line 1879
    const/4 v1, 0x4

    if-ne p1, v1, :cond_4a

    .line 1881
    iget-wide v1, p0, Law/h;->y:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_27

    iget-boolean v1, p0, Law/h;->x:Z

    if-eqz v1, :cond_37

    .line 1882
    :cond_27
    invoke-direct {p0}, Law/h;->D()V

    .line 1883
    iput p1, p0, Law/h;->H:I

    .line 1884
    const-wide/16 v1, 0xc8

    iput-wide v1, p0, Law/h;->y:J

    .line 1913
    :cond_30
    :goto_30
    monitor-exit p0
    :try_end_31
    .catchall {:try_start_6 .. :try_end_31} :catchall_47

    .line 1915
    if-eqz v0, :cond_36

    .line 1916
    invoke-virtual {p0, p1}, Law/h;->a(I)V

    .line 1918
    :cond_36
    return-void

    .line 1885
    :cond_37
    :try_start_37
    iget-wide v1, p0, Law/h;->y:J

    iget-wide v3, p0, Law/h;->s:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_30

    .line 1886
    iget-wide v1, p0, Law/h;->y:J

    const-wide/16 v3, 0x2

    mul-long/2addr v1, v3

    iput-wide v1, p0, Law/h;->y:J

    goto :goto_30

    .line 1913
    :catchall_47
    move-exception v0

    monitor-exit p0
    :try_end_49
    .catchall {:try_start_37 .. :try_end_49} :catchall_47

    throw v0

    .line 1889
    :cond_4a
    :try_start_4a
    iget-boolean v1, p0, Law/h;->x:Z

    if-nez v1, :cond_74

    .line 1890
    const-wide/16 v1, 0xc8

    iput-wide v1, p0, Law/h;->y:J

    .line 1892
    iget-wide v1, p0, Law/h;->z:J

    const-wide/high16 v3, -0x8000

    cmp-long v1, v1, v3

    if-nez v1, :cond_63

    .line 1893
    iget-object v1, p0, Law/h;->E:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    iput-wide v1, p0, Law/h;->z:J

    goto :goto_30

    .line 1894
    :cond_63
    iget-wide v1, p0, Law/h;->z:J

    const-wide/16 v3, 0x3a98

    add-long/2addr v1, v3

    iget-object v3, p0, Law/h;->E:Lcom/google/googlenav/common/a;

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_30

    .line 1898
    const/4 v0, 0x1

    goto :goto_30

    .line 1901
    :cond_74
    iget-wide v1, p0, Law/h;->y:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_8b

    .line 1902
    const-wide/16 v1, 0x7d0

    iput-wide v1, p0, Law/h;->y:J

    .line 1909
    :goto_7e
    iget-wide v1, p0, Law/h;->y:J

    iget-wide v3, p0, Law/h;->s:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_30

    .line 1910
    iget-wide v1, p0, Law/h;->s:J

    iput-wide v1, p0, Law/h;->y:J

    goto :goto_30

    .line 1905
    :cond_8b
    iget-wide v1, p0, Law/h;->y:J

    const-wide/16 v3, 0x5

    mul-long/2addr v1, v3

    const-wide/16 v3, 0x4

    div-long/2addr v1, v3

    iput-wide v1, p0, Law/h;->y:J
    :try_end_95
    .catchall {:try_start_4a .. :try_end_95} :catchall_47

    goto :goto_7e
.end method

.method private a(IZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1490
    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/o;

    .line 1491
    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_6

    .line 1493
    :cond_1a
    return-void
.end method

.method private a(I[I)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 2172
    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/o;

    .line 2173
    array-length v3, p2

    const/4 v1, 0x0

    :goto_14
    if-ge v1, v3, :cond_6

    aget v4, p2, v1

    .line 2174
    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-virtual {v5, p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 2173
    add-int/lit8 v1, v1, 0x1

    goto :goto_14

    .line 2178
    :cond_22
    return-void
.end method

.method static synthetic a(Law/h;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Law/h;->d(I)V

    return-void
.end method

.method static synthetic a(Law/h;ILjava/lang/Throwable;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Law/h;->a(ILjava/lang/Throwable;)V

    return-void
.end method

.method static synthetic a(Law/h;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Law/h;->g(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Law/h;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Law/h;->c(Z)V

    return-void
.end method

.method static synthetic a(Law/h;ZZ)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Law/h;->a(ZZ)V

    return-void
.end method

.method private declared-synchronized a(ZZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1352
    monitor-enter p0

    :try_start_1
    iget v0, p0, Law/h;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Law/h;->A:I

    .line 1353
    if-eqz p1, :cond_f

    .line 1354
    iget v0, p0, Law/h;->B:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Law/h;->B:I

    .line 1356
    :cond_f
    if-eqz p2, :cond_17

    .line 1357
    iget v0, p0, Law/h;->C:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Law/h;->C:I
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_19

    .line 1359
    :cond_17
    monitor-exit p0

    return-void

    .line 1352
    :catchall_19
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected static a(Ljava/util/Vector;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1937
    move v1, v2

    :goto_2
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_15

    .line 1938
    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/g;

    .line 1939
    invoke-interface {v0}, Law/g;->t_()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1940
    const/4 v2, 0x1

    .line 1944
    :cond_15
    return v2

    .line 1937
    :cond_16
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method static synthetic b(Law/h;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    iput p1, p0, Law/h;->F:I

    return p1
.end method

.method static synthetic b(Law/h;J)J
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 64
    iput-wide p1, p0, Law/h;->w:J

    return-wide p1
.end method

.method private static declared-synchronized b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Law/h;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1385
    const-class v6, Law/h;

    monitor-enter v6

    :try_start_3
    sget-object v0, Law/h;->K:Law/h;

    if-eqz v0, :cond_12

    .line 1386
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempting to create multiple DataRequestDispatchers"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_f

    .line 1385
    :catchall_f
    move-exception v0

    monitor-exit v6

    throw v0

    .line 1389
    :cond_12
    :try_start_12
    invoke-static {p0}, Law/h;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1390
    new-instance v0, Law/h;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Law/h;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    sput-object v0, Law/h;->K:Law/h;

    .line 1392
    sget-object v0, Law/h;->K:Law/h;
    :try_end_23
    .catchall {:try_start_12 .. :try_end_23} :catchall_f

    monitor-exit v6

    return-object v0
.end method

.method public static b()Law/p;
    .registers 1

    .prologue
    .line 1431
    sget-object v0, Law/h;->K:Law/h;

    iget-object v0, v0, Law/h;->n:Law/p;

    return-object v0
.end method

.method static synthetic b(Law/h;)Lcom/google/googlenav/common/a;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Law/h;->E:Lcom/google/googlenav/common/a;

    return-object v0
.end method

.method static b(J)V
    .registers 5
    .parameter

    .prologue
    .line 1976
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1977
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1978
    invoke-interface {v1, p0, p1}, Ljava/io/DataOutput;->writeLong(J)V

    .line 1979
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    .line 1980
    const-string v2, "SessionID"

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 1984
    invoke-interface {v1}, Lcom/google/googlenav/common/io/j;->a()V
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_21} :catch_22

    .line 1989
    return-void

    .line 1985
    :catch_22
    move-exception v0

    .line 1987
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static synthetic b(Law/h;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Law/h;->d(Z)V

    return-void
.end method

.method static synthetic b(Law/h;ZZ)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Law/h;->b(ZZ)V

    return-void
.end method

.method private declared-synchronized b(ZZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1369
    monitor-enter p0

    :try_start_1
    iget v0, p0, Law/h;->A:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Law/h;->A:I

    .line 1370
    if-eqz p1, :cond_f

    .line 1371
    iget v0, p0, Law/h;->B:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Law/h;->B:I

    .line 1373
    :cond_f
    if-eqz p2, :cond_17

    .line 1374
    iget v0, p0, Law/h;->C:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Law/h;->C:I
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_19

    .line 1376
    :cond_17
    monitor-exit p0

    return-void

    .line 1369
    :catchall_19
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected static b(Ljava/util/Vector;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1949
    move v1, v2

    :goto_2
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_15

    .line 1950
    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/g;

    .line 1951
    invoke-interface {v0}, Law/g;->s()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1952
    const/4 v2, 0x1

    .line 1955
    :cond_15
    return v2

    .line 1949
    :cond_16
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method public static c()V
    .registers 1

    .prologue
    .line 1438
    const/4 v0, 0x0

    sput-object v0, Law/h;->K:Law/h;

    .line 1439
    return-void
.end method

.method static synthetic c(Law/h;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Law/h;->e(Z)V

    return-void
.end method

.method private c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 2041
    const/4 v0, 0x4

    invoke-direct {p0, v0, p1}, Law/h;->a(IZ)V

    .line 2042
    return-void
.end method

.method static synthetic c(Law/h;)Z
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-boolean v0, p0, Law/h;->t:Z

    return v0
.end method

.method protected static c(Ljava/util/Vector;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1960
    move v1, v2

    :goto_2
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_15

    .line 1961
    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/g;

    .line 1962
    invoke-interface {v0}, Law/g;->c_()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1963
    const/4 v2, 0x1

    .line 1967
    :cond_15
    return v2

    .line 1960
    :cond_16
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method private d(I)V
    .registers 4
    .parameter

    .prologue
    .line 2141
    const/16 v0, 0xc8

    if-le p1, v0, :cond_b

    const/4 v0, 0x3

    .line 2144
    :goto_5
    const/16 v1, 0x16

    invoke-direct {p0, v1, v0}, Law/h;->a(II)V

    .line 2145
    return-void

    .line 2141
    :cond_b
    const/4 v0, 0x1

    goto :goto_5
.end method

.method static synthetic d(Law/h;)V
    .registers 1
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Law/h;->D()V

    return-void
.end method

.method static synthetic d(Law/h;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Law/h;->f(Z)V

    return-void
.end method

.method private d(Z)V
    .registers 3
    .parameter

    .prologue
    .line 2102
    const/16 v0, 0x1d

    invoke-direct {p0, v0, p1}, Law/h;->a(IZ)V

    .line 2103
    return-void
.end method

.method private e(I)V
    .registers 6
    .parameter

    .prologue
    .line 2155
    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/o;

    .line 2156
    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2157
    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_26
    if-ltz v1, :cond_6

    .line 2158
    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v3, p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    .line 2157
    add-int/lit8 v1, v1, -0x1

    goto :goto_26

    .line 2162
    :cond_32
    return-void
.end method

.method private e(Z)V
    .registers 3
    .parameter

    .prologue
    .line 2123
    const/16 v0, 0x20

    invoke-direct {p0, v0, p1}, Law/h;->a(IZ)V

    .line 2124
    return-void
.end method

.method static synthetic e(Law/h;)Z
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-boolean v0, p0, Law/h;->l:Z

    return v0
.end method

.method static synthetic f(Law/h;)Lcom/google/googlenav/bk;
    .registers 2
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Law/h;->E()Lcom/google/googlenav/bk;

    move-result-object v0

    return-object v0
.end method

.method private static f(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 1403
    if-eqz p0, :cond_22

    const-string v0, "http:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 1404
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1407
    :cond_22
    return-object p0
.end method

.method private f(Z)V
    .registers 3
    .parameter

    .prologue
    .line 2128
    const/16 v0, 0x21

    invoke-direct {p0, v0, p1}, Law/h;->a(IZ)V

    .line 2129
    return-void
.end method

.method static synthetic g(Law/h;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Law/h;->o:Ljava/lang/String;

    return-object v0
.end method

.method private g(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 2098
    const/16 v0, 0x1b

    invoke-direct {p0, v0, p1}, Law/h;->a(ILjava/lang/String;)V

    .line 2099
    return-void
.end method

.method static synthetic h(Law/h;)Ljava/lang/Long;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Law/h;->p:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic i(Law/h;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Law/h;->m:Ljava/util/List;

    return-object v0
.end method

.method static synthetic j(Law/h;)I
    .registers 2
    .parameter

    .prologue
    .line 64
    iget v0, p0, Law/h;->H:I

    return v0
.end method

.method static synthetic k(Law/h;)J
    .registers 3
    .parameter

    .prologue
    .line 64
    iget-wide v0, p0, Law/h;->y:J

    return-wide v0
.end method

.method static synthetic l(Law/h;)Ljava/util/Random;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Law/h;->r:Ljava/util/Random;

    return-object v0
.end method

.method static synthetic z()Law/h;
    .registers 1

    .prologue
    .line 64
    sget-object v0, Law/h;->K:Law/h;

    return-object v0
.end method


# virtual methods
.method protected final a(I)V
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 1697
    const/4 v1, 0x0

    .line 1698
    monitor-enter p0

    .line 1699
    :try_start_3
    iget-boolean v2, p0, Law/h;->x:Z

    if-nez v2, :cond_1f

    .line 1701
    const/4 v1, 0x1

    iput-boolean v1, p0, Law/h;->x:Z

    .line 1702
    const-wide/high16 v1, -0x8000

    iput-wide v1, p0, Law/h;->z:J

    .line 1705
    :goto_e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_1c

    .line 1707
    iget-object v1, p0, Law/h;->j:Lcom/google/googlenav/common/io/g;

    invoke-interface {v1}, Lcom/google/googlenav/common/io/g;->d()Z

    move-result v1

    .line 1709
    if-eqz v0, :cond_1b

    .line 1710
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v0}, Law/h;->a(IZLjava/lang/String;)V

    .line 1712
    :cond_1b
    return-void

    .line 1705
    :catchall_1c
    move-exception v0

    :try_start_1d
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_1d .. :try_end_1e} :catchall_1c

    throw v0

    :cond_1f
    move v0, v1

    goto :goto_e
.end method

.method protected a(IZLjava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1649
    invoke-virtual {p0}, Law/h;->j()[Law/q;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_10

    aget-object v3, v1, v0

    .line 1650
    invoke-interface {v3, p1, p2, p3}, Law/q;->a(IZLjava/lang/String;)V

    .line 1649
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1652
    :cond_10
    return-void
.end method

.method public a(I[BZZ)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1923
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    .line 1924
    invoke-virtual/range {v0 .. v5}, Law/h;->a(I[BZZZ)V

    .line 1925
    return-void
.end method

.method public a(I[BZZZ)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1930
    iget-object v0, p0, Law/h;->f:Law/o;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Law/o;->a(I[BZZZ)V

    .line 1932
    return-void
.end method

.method public declared-synchronized a(J)V
    .registers 5
    .parameter

    .prologue
    const-wide/16 v0, 0x7d0

    .line 1539
    monitor-enter p0

    cmp-long v0, p1, v0

    if-gez v0, :cond_d

    .line 1540
    const-wide/16 v0, 0x7d0

    :try_start_9
    iput-wide v0, p0, Law/h;->s:J
    :try_end_b
    .catchall {:try_start_9 .. :try_end_b} :catchall_10

    .line 1544
    :goto_b
    monitor-exit p0

    return-void

    .line 1542
    :cond_d
    :try_start_d
    iput-wide p1, p0, Law/h;->s:J
    :try_end_f
    .catchall {:try_start_d .. :try_end_f} :catchall_10

    goto :goto_b

    .line 1539
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Law/g;)V
    .registers 6
    .parameter

    .prologue
    .line 1637
    invoke-virtual {p0}, Law/h;->j()[Law/q;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_10

    aget-object v3, v1, v0

    .line 1638
    invoke-interface {v3, p1}, Law/q;->a(Law/g;)V

    .line 1637
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1640
    :cond_10
    return-void
.end method

.method public a(Law/p;)V
    .registers 2
    .parameter

    .prologue
    .line 1422
    iput-object p1, p0, Law/h;->n:Law/p;

    .line 1423
    return-void
.end method

.method public declared-synchronized a(Law/q;)V
    .registers 3
    .parameter

    .prologue
    .line 1609
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Law/h;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 1610
    iget-object v0, p0, Law/h;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    .line 1612
    :cond_e
    monitor-exit p0

    return-void

    .line 1609
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 2222
    invoke-direct {p0}, Law/h;->E()Lcom/google/googlenav/bk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bk;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2223
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 1531
    if-eqz p1, :cond_f

    .line 1532
    invoke-static {p1}, Law/h;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Law/h;->a:Ljava/lang/String;

    .line 1533
    iget-object v0, p0, Law/h;->e:Law/l;

    iget-object v1, p0, Law/h;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Law/l;->a(Law/l;Ljava/lang/String;)Ljava/lang/String;

    .line 1535
    :cond_f
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 2107
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Law/h;->a(IZ)V

    .line 2108
    return-void
.end method

.method public a([I)V
    .registers 3
    .parameter

    .prologue
    const/16 v0, 0x17

    .line 2185
    invoke-direct {p0, v0}, Law/h;->e(I)V

    .line 2186
    invoke-direct {p0, v0, p1}, Law/h;->a(I[I)V

    .line 2187
    return-void
.end method

.method public b(I)V
    .registers 3
    .parameter

    .prologue
    .line 2115
    iget v0, p0, Law/h;->J:I

    if-eq v0, p1, :cond_a

    .line 2116
    iput p1, p0, Law/h;->J:I

    .line 2117
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Law/h;->a(II)V

    .line 2119
    :cond_a
    return-void
.end method

.method protected b(Law/g;)V
    .registers 6
    .parameter

    .prologue
    .line 1661
    invoke-virtual {p0}, Law/h;->j()[Law/q;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_10

    aget-object v3, v1, v0

    .line 1662
    invoke-interface {v3, p1}, Law/q;->b(Law/g;)V

    .line 1661
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1664
    :cond_10
    return-void
.end method

.method public declared-synchronized b(Law/q;)V
    .registers 3
    .parameter

    .prologue
    .line 1616
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Law/h;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 1617
    monitor-exit p0

    return-void

    .line 1616
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 2033
    const/16 v0, 0x14

    invoke-direct {p0, v0, p1}, Law/h;->a(ILjava/lang/String;)V

    .line 2034
    return-void
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 2260
    iput-boolean p1, p0, Law/h;->g:Z

    .line 2261
    if-eqz p1, :cond_8

    .line 2262
    invoke-virtual {p0}, Law/h;->u()V

    .line 2266
    :goto_7
    return-void

    .line 2264
    :cond_8
    invoke-virtual {p0}, Law/h;->v()V

    goto :goto_7
.end method

.method public b([I)V
    .registers 3
    .parameter

    .prologue
    const/16 v0, 0x1a

    .line 2194
    invoke-direct {p0, v0}, Law/h;->e(I)V

    .line 2195
    invoke-direct {p0, v0, p1}, Law/h;->a(I[I)V

    .line 2196
    return-void
.end method

.method public c(I)V
    .registers 3
    .parameter

    .prologue
    .line 2200
    const/16 v0, 0x19

    invoke-direct {p0, v0, p1}, Law/h;->a(II)V

    .line 2201
    return-void
.end method

.method public c(Law/g;)V
    .registers 3
    .parameter

    .prologue
    .line 1722
    iget-object v0, p0, Law/h;->f:Law/o;

    invoke-virtual {v0, p1}, Law/o;->c(Law/g;)V

    .line 1723
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 2049
    const/16 v0, 0x12

    invoke-direct {p0, v0, p1}, Law/h;->a(ILjava/lang/String;)V

    .line 2050
    return-void
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1521
    iget-object v0, p0, Law/h;->a:Ljava/lang/String;

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 2067
    const/16 v0, 0x13

    invoke-direct {p0, v0, p1}, Law/h;->a(ILjava/lang/String;)V

    .line 2068
    return-void
.end method

.method public e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1526
    iget-object v0, p0, Law/h;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 2283
    iget-boolean v0, p0, Law/h;->t:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Law/h;->j:Lcom/google/googlenav/common/io/g;

    invoke-interface {v0}, Lcom/google/googlenav/common/io/g;->d()Z

    move-result v0

    if-nez v0, :cond_d

    .line 2288
    :cond_c
    :goto_c
    return-void

    .line 2287
    :cond_d
    iget-object v0, p0, Law/h;->k:Law/f;

    invoke-virtual {v0, p1}, Law/f;->a(Ljava/lang/String;)V

    goto :goto_c
.end method

.method public declared-synchronized f()Z
    .registers 2

    .prologue
    .line 1548
    monitor-enter p0

    :try_start_1
    iget v0, p0, Law/h;->u:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_a

    if-lez v0, :cond_8

    const/4 v0, 0x1

    :goto_6
    monitor-exit p0

    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_6

    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()V
    .registers 2

    .prologue
    .line 1553
    monitor-enter p0

    :try_start_1
    iget v0, p0, Law/h;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Law/h;->u:I
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 1554
    monitor-exit p0

    return-void

    .line 1553
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()V
    .registers 2

    .prologue
    .line 1558
    monitor-enter p0

    .line 1559
    :try_start_1
    iget v0, p0, Law/h;->u:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Law/h;->u:I

    .line 1564
    invoke-virtual {p0}, Law/h;->f()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1565
    monitor-exit p0

    .line 1571
    :goto_e
    return-void

    .line 1567
    :cond_f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_1b

    .line 1569
    iget-object v0, p0, Law/h;->e:Law/l;

    invoke-static {v0}, Law/l;->c(Law/l;)V

    .line 1570
    iget-object v0, p0, Law/h;->k:Law/f;

    invoke-virtual {v0}, Law/f;->a()V

    goto :goto_e

    .line 1567
    :catchall_1b
    move-exception v0

    :try_start_1c
    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_1c .. :try_end_1d} :catchall_1b

    throw v0
.end method

.method protected i()J
    .registers 6

    .prologue
    .line 1600
    invoke-static {}, Law/h;->C()J

    move-result-wide v0

    .line 1601
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_15

    .line 1602
    iget-object v2, p0, Law/h;->f:Law/o;

    new-instance v3, Law/k;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Law/k;-><init>(Law/h;Law/i;)V

    invoke-virtual {v2, v3}, Law/o;->c(Law/g;)V

    .line 1604
    :cond_15
    return-wide v0
.end method

.method protected declared-synchronized j()[Law/q;
    .registers 3

    .prologue
    .line 1624
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Law/h;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Law/q;

    .line 1625
    iget-object v1, p0, Law/h;->q:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    .line 1626
    monitor-exit p0

    return-object v0

    .line 1624
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected k()V
    .registers 5

    .prologue
    .line 1673
    invoke-virtual {p0}, Law/h;->j()[Law/q;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_10

    aget-object v3, v1, v0

    .line 1674
    invoke-interface {v3}, Law/q;->k()V

    .line 1673
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1676
    :cond_10
    return-void
.end method

.method public declared-synchronized l()Z
    .registers 2

    .prologue
    .line 1729
    monitor-enter p0

    :try_start_1
    iget v0, p0, Law/h;->A:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_a

    if-lez v0, :cond_8

    const/4 v0, 0x1

    :goto_6
    monitor-exit p0

    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_6

    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized m()Z
    .registers 2

    .prologue
    .line 1751
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Law/h;->x:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized n()Z
    .registers 3

    .prologue
    .line 1759
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Law/h;->t:Z

    if-eqz v0, :cond_1a

    iget v0, p0, Law/h;->A:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1a

    iget-object v0, p0, Law/h;->j:Lcom/google/googlenav/common/io/g;

    invoke-interface {v0}, Lcom/google/googlenav/common/io/g;->e()Z

    move-result v0

    if-nez v0, :cond_17

    iget v0, p0, Law/h;->A:I
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_1c

    if-nez v0, :cond_1a

    :cond_17
    const/4 v0, 0x1

    :goto_18
    monitor-exit p0

    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_18

    :catchall_1c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public o()J
    .registers 3

    .prologue
    .line 1770
    iget-wide v0, p0, Law/h;->v:J

    return-wide v0
.end method

.method public p()J
    .registers 3

    .prologue
    .line 1775
    iget-wide v0, p0, Law/h;->w:J

    return-wide v0
.end method

.method public q()Z
    .registers 5

    .prologue
    .line 1782
    iget-wide v0, p0, Law/h;->w:J

    const-wide/high16 v2, -0x8000

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final r()I
    .registers 2

    .prologue
    .line 1789
    iget v0, p0, Law/h;->h:I

    return v0
.end method

.method public final s()I
    .registers 2

    .prologue
    .line 1797
    iget v0, p0, Law/h;->i:I

    return v0
.end method

.method public t()I
    .registers 2

    .prologue
    .line 1831
    iget v0, p0, Law/h;->F:I

    return v0
.end method

.method public u()V
    .registers 2

    .prologue
    .line 1835
    const/4 v0, 0x0

    iput-boolean v0, p0, Law/h;->t:Z

    .line 1836
    return-void
.end method

.method public v()V
    .registers 2

    .prologue
    .line 1839
    const/4 v0, 0x1

    iput-boolean v0, p0, Law/h;->t:Z

    .line 1840
    iget-object v0, p0, Law/h;->e:Law/l;

    invoke-static {v0}, Law/l;->e(Law/l;)V

    .line 1841
    return-void
.end method

.method public declared-synchronized w()J
    .registers 3

    .prologue
    .line 1993
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Law/h;->p:Ljava/lang/Long;

    if-nez v0, :cond_f

    .line 1994
    invoke-virtual {p0}, Law/h;->i()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Law/h;->p:Ljava/lang/Long;

    .line 1997
    :cond_f
    iget-object v0, p0, Law/h;->p:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_17

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    .line 1993
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public x()Ljava/lang/String;
    .registers 3

    .prologue
    .line 2093
    iget-object v0, p0, Law/h;->f:Law/o;

    invoke-static {v0}, Law/o;->b(Law/o;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public y()Ljava/lang/Throwable;
    .registers 2

    .prologue
    .line 2301
    iget-object v0, p0, Law/h;->G:Ljava/lang/Throwable;

    return-object v0
.end method
