.class public Law/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Law/h;

.field private volatile b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Law/h;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2349
    iput-object p1, p0, Law/l;->a:Law/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2350
    iput-object p2, p0, Law/l;->b:Ljava/lang/String;

    .line 2351
    return-void
.end method

.method synthetic constructor <init>(Law/h;Ljava/lang/String;Law/i;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2345
    invoke-direct {p0, p1, p2}, Law/l;-><init>(Law/h;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Law/l;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 2345
    iput-object p1, p0, Law/l;->b:Ljava/lang/String;

    return-object p1
.end method

.method private declared-synchronized a()V
    .registers 4

    .prologue
    .line 2359
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Law/l;->a:Law/h;

    invoke-virtual {v0}, Law/h;->n()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_2c

    move-result v0

    if-nez v0, :cond_b

    .line 2377
    :cond_9
    :goto_9
    monitor-exit p0

    return-void

    .line 2368
    :cond_b
    :try_start_b
    iget-object v0, p0, Law/l;->a:Law/h;

    invoke-static {v0}, Law/h;->i(Law/h;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/o;

    .line 2369
    const/4 v2, 0x1

    .line 2370
    invoke-static {v0, v2}, Law/o;->b(Law/o;Z)Law/m;

    move-result-object v0

    .line 2372
    if-eqz v0, :cond_15

    .line 2373
    invoke-virtual {v0}, Law/m;->a()V
    :try_end_2b
    .catchall {:try_start_b .. :try_end_2b} :catchall_2c

    goto :goto_9

    .line 2359
    :catchall_2c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Law/l;)V
    .registers 1
    .parameter

    .prologue
    .line 2345
    invoke-direct {p0}, Law/l;->a()V

    return-void
.end method

.method static synthetic a(Law/l;Ljava/lang/Exception;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 2345
    invoke-direct {p0, p1}, Law/l;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .registers 4
    .parameter

    .prologue
    .line 2437
    const-string v0, "REQUEST"

    invoke-static {v0, p1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2441
    iget-object v0, p0, Law/l;->a:Law/h;

    invoke-virtual {v0}, Law/h;->u()V

    .line 2444
    iget-object v0, p0, Law/l;->a:Law/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Law/h;->a(I)V

    .line 2445
    return-void
.end method

.method static synthetic b(Law/l;)J
    .registers 3
    .parameter

    .prologue
    .line 2345
    invoke-direct {p0}, Law/l;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method private b()V
    .registers 2

    .prologue
    .line 2388
    invoke-direct {p0}, Law/l;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2389
    invoke-direct {p0}, Law/l;->a()V

    .line 2391
    :cond_9
    return-void
.end method

.method static synthetic c(Law/l;)V
    .registers 1
    .parameter

    .prologue
    .line 2345
    invoke-direct {p0}, Law/l;->b()V

    return-void
.end method

.method private c()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 2399
    iget-object v0, p0, Law/l;->a:Law/h;

    invoke-virtual {v0}, Law/h;->f()Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    .line 2407
    :goto_a
    return v0

    .line 2402
    :cond_b
    iget-object v0, p0, Law/l;->a:Law/h;

    invoke-static {v0}, Law/h;->i(Law/h;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Law/o;

    .line 2403
    invoke-static {v0}, Law/o;->c(Law/o;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2404
    const/4 v0, 0x1

    goto :goto_a

    :cond_29
    move v0, v1

    .line 2407
    goto :goto_a
.end method

.method private declared-synchronized d()J
    .registers 7

    .prologue
    .line 2417
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Law/l;->a:Law/h;

    invoke-static {v0}, Law/h;->j(Law/h;)I

    move-result v0

    packed-switch v0, :pswitch_data_32

    .line 2427
    iget-object v0, p0, Law/l;->a:Law/h;

    invoke-static {v0}, Law/h;->k(Law/h;)J
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_2e

    move-result-wide v0

    :goto_10
    monitor-exit p0

    return-wide v0

    .line 2424
    :pswitch_12
    :try_start_12
    iget-object v0, p0, Law/l;->a:Law/h;

    invoke-static {v0}, Law/h;->k(Law/h;)J

    move-result-wide v0

    const-wide/16 v2, 0x320

    add-long/2addr v0, v2

    iget-object v2, p0, Law/l;->a:Law/h;

    invoke-static {v2}, Law/h;->l(Law/h;)Ljava/util/Random;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    iget-object v4, p0, Law/l;->a:Law/h;

    invoke-static {v4}, Law/h;->k(Law/h;)J

    move-result-wide v4

    rem-long/2addr v2, v4
    :try_end_2c
    .catchall {:try_start_12 .. :try_end_2c} :catchall_2e

    add-long/2addr v0, v2

    goto :goto_10

    .line 2417
    :catchall_2e
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_32
    .packed-switch 0x4
        :pswitch_12
    .end packed-switch
.end method

.method static synthetic d(Law/l;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 2345
    iget-object v0, p0, Law/l;->b:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .registers 1

    .prologue
    .line 2453
    invoke-direct {p0}, Law/l;->b()V

    .line 2454
    return-void
.end method

.method static synthetic e(Law/l;)V
    .registers 1
    .parameter

    .prologue
    .line 2345
    invoke-direct {p0}, Law/l;->e()V

    return-void
.end method
