.class public LR/s;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;Ljava/lang/String;)LR/u;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 158
    new-instance v0, LR/u;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, LR/u;-><init>(Landroid/content/Context;Ljava/lang/String;ZLR/t;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LR/u;->a(Ljava/lang/String;Ljava/lang/Object;)LR/u;

    .line 114
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LR/u;->a(Ljava/lang/String;Ljava/lang/Object;)LR/u;

    .line 119
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LR/u;->a(Ljava/lang/String;Ljava/lang/Object;)LR/u;

    .line 109
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;I)I
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LR/u;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)LR/u;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 172
    new-instance v0, LR/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, LR/u;-><init>(Landroid/content/Context;Ljava/lang/String;ZLR/t;)V

    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 123
    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LR/u;->c(Ljava/lang/String;Ljava/lang/String;)LR/u;

    .line 124
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Z)Z
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 127
    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LR/u;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 135
    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LR/u;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 139
    const/4 v0, 0x0

    invoke-static {p0, v0}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LR/u;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
