.class public LR/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/HashMap;

.field protected final b:I

.field private c:LR/k;

.field private d:LR/k;


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    .line 21
    iput p1, p0, LR/h;->b:I

    .line 22
    return-void
.end method

.method private a(Ljava/lang/Object;Z)Ljava/lang/Object;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 147
    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LR/k;

    .line 148
    if-nez v0, :cond_c

    .line 149
    const/4 v0, 0x0

    .line 159
    :goto_b
    return-object v0

    .line 151
    :cond_c
    invoke-direct {p0, v0}, LR/h;->b(LR/k;)V

    .line 152
    if-eqz p2, :cond_16

    .line 154
    iget-object v1, v0, LR/k;->d:Ljava/lang/Object;

    invoke-virtual {p0, p1, v1}, LR/h;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 159
    :cond_16
    iget-object v0, v0, LR/k;->d:Ljava/lang/Object;

    goto :goto_b
.end method

.method private a(LR/k;)V
    .registers 3
    .parameter

    .prologue
    .line 191
    iget-object v0, p0, LR/h;->d:LR/k;

    if-nez v0, :cond_9

    .line 192
    iput-object p1, p0, LR/h;->c:LR/k;

    .line 193
    iput-object p1, p0, LR/h;->d:LR/k;

    .line 200
    :goto_8
    return-void

    .line 195
    :cond_9
    iget-object v0, p0, LR/h;->d:LR/k;

    .line 196
    iput-object v0, p1, LR/k;->a:LR/k;

    .line 197
    iput-object p1, v0, LR/k;->b:LR/k;

    .line 198
    iput-object p1, p0, LR/h;->d:LR/k;

    goto :goto_8
.end method

.method private b(LR/k;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 203
    iget-object v0, p1, LR/k;->a:LR/k;

    .line 204
    iget-object v1, p1, LR/k;->b:LR/k;

    .line 205
    if-eqz v0, :cond_9

    .line 206
    iput-object v1, v0, LR/k;->b:LR/k;

    .line 208
    :cond_9
    if-eqz v1, :cond_d

    .line 209
    iput-object v0, v1, LR/k;->a:LR/k;

    .line 212
    :cond_d
    iput-object v2, p1, LR/k;->a:LR/k;

    .line 213
    iput-object v2, p1, LR/k;->b:LR/k;

    .line 215
    iget-object v2, p0, LR/h;->c:LR/k;

    if-ne v2, p1, :cond_17

    .line 216
    iput-object v1, p0, LR/h;->c:LR/k;

    .line 219
    :cond_17
    iget-object v1, p0, LR/h;->d:LR/k;

    if-ne v1, p1, :cond_1d

    .line 220
    iput-object v0, p0, LR/h;->d:LR/k;

    .line 222
    :cond_1d
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LR/k;

    .line 94
    if-nez v0, :cond_c

    const/4 v0, 0x0

    :goto_b
    return-object v0

    :cond_c
    iget-object v0, v0, LR/k;->d:Ljava/lang/Object;

    goto :goto_b
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 182
    :goto_0
    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-le v0, p1, :cond_18

    .line 183
    iget-object v0, p0, LR/h;->c:LR/k;

    iget-object v0, v0, LR/k;->c:Ljava/lang/Object;

    .line 184
    iget-object v1, p0, LR/h;->c:LR/k;

    iget-object v1, v1, LR/k;->c:Ljava/lang/Object;

    invoke-virtual {p0, v1}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 186
    invoke-virtual {p0, v0, v1}, LR/h;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 188
    :cond_18
    return-void
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    return-void
.end method

.method public b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 101
    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LR/k;

    .line 102
    if-nez v0, :cond_c

    .line 103
    const/4 v0, 0x0

    .line 111
    :goto_b
    return-object v0

    .line 107
    :cond_c
    invoke-direct {p0, v0}, LR/h;->b(LR/k;)V

    .line 108
    invoke-direct {p0, v0}, LR/h;->a(LR/k;)V

    .line 111
    iget-object v0, v0, LR/k;->d:Ljava/lang/Object;

    goto :goto_b
.end method

.method protected b(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 49
    return-void
.end method

.method public c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 133
    const/4 v0, 0x1

    .line 134
    invoke-direct {p0, p1, v0}, LR/h;->a(Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LR/k;

    .line 67
    if-nez v0, :cond_11

    .line 68
    iget v1, p0, LR/h;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, LR/h;->a(I)V

    .line 73
    :cond_11
    new-instance v1, LR/k;

    invoke-direct {v1}, LR/k;-><init>()V

    .line 74
    iput-object p2, v1, LR/k;->d:Ljava/lang/Object;

    .line 75
    iput-object p1, v1, LR/k;->c:Ljava/lang/Object;

    .line 77
    if-eqz v0, :cond_29

    .line 78
    invoke-direct {p0, v0}, LR/h;->b(LR/k;)V

    .line 81
    iget-object v2, v0, LR/k;->d:Ljava/lang/Object;

    invoke-virtual {p0, p1, v2}, LR/h;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 82
    iget-object v0, v0, LR/k;->d:Ljava/lang/Object;

    invoke-virtual {p0, p1, v0}, LR/h;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 84
    :cond_29
    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    iget-object v2, v1, LR/k;->c:Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    invoke-direct {p0, v1}, LR/h;->a(LR/k;)V

    .line 89
    return-void
.end method

.method public final e()V
    .registers 2

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LR/h;->a(I)V

    .line 30
    return-void
.end method

.method public final f()I
    .registers 2

    .prologue
    .line 33
    iget-object v0, p0, LR/h;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public g()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 115
    iget-object v0, p0, LR/h;->c:LR/k;

    if-nez v0, :cond_6

    .line 116
    const/4 v0, 0x0

    .line 118
    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, LR/h;->c:LR/k;

    iget-object v0, v0, LR/k;->c:Ljava/lang/Object;

    invoke-virtual {p0, v0}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_5
.end method

.method public h()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 122
    iget-object v0, p0, LR/h;->d:LR/k;

    if-nez v0, :cond_6

    .line 123
    const/4 v0, 0x0

    .line 125
    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, LR/h;->d:LR/k;

    iget-object v0, v0, LR/k;->c:Ljava/lang/Object;

    invoke-virtual {p0, v0}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_5
.end method

.method public final i()Ljava/util/Collection;
    .registers 4

    .prologue
    .line 172
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, LR/h;->f()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 173
    iget-object v0, p0, LR/h;->c:LR/k;

    :goto_b
    if-eqz v0, :cond_15

    .line 175
    iget-object v2, v0, LR/k;->d:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    iget-object v0, v0, LR/k;->b:LR/k;

    goto :goto_b

    .line 177
    :cond_15
    return-object v1
.end method

.method public j()LR/i;
    .registers 3

    .prologue
    .line 278
    new-instance v0, LR/i;

    iget-object v1, p0, LR/h;->c:LR/k;

    invoke-direct {v0, v1}, LR/i;-><init>(LR/k;)V

    return-object v0
.end method
