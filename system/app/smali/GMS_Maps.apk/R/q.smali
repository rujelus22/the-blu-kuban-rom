.class LR/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/q;


# instance fields
.field private final a:Law/h;

.field private final b:Ljava/lang/Runnable;

.field private final c:Z


# direct methods
.method public constructor <init>(Law/h;Ljava/lang/Runnable;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    iput-object p1, p0, LR/q;->a:Law/h;

    .line 247
    iput-object p2, p0, LR/q;->b:Ljava/lang/Runnable;

    .line 248
    invoke-virtual {p1}, Law/h;->f()Z

    move-result v0

    iput-boolean v0, p0, LR/q;->c:Z

    .line 249
    iget-boolean v0, p0, LR/q;->c:Z

    if-eqz v0, :cond_14

    .line 250
    invoke-virtual {p1}, Law/h;->h()V

    .line 252
    :cond_14
    return-void
.end method


# virtual methods
.method public a(IZLjava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 267
    const/4 v1, 0x3

    if-ne p1, v1, :cond_2a

    if-eqz p2, :cond_2a

    .line 269
    :goto_6
    if-nez v0, :cond_29

    invoke-static {}, LR/o;->g()Lcom/google/android/maps/driveabout/vector/bF;

    move-result-object v0

    if-nez v0, :cond_29

    .line 270
    iget-object v0, p0, LR/q;->a:Law/h;

    invoke-virtual {v0, p0}, Law/h;->b(Law/q;)V

    .line 271
    iget-object v0, p0, LR/q;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_1c

    .line 272
    iget-object v0, p0, LR/q;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 274
    :cond_1c
    const-class v1, LR/o;

    monitor-enter v1

    .line 275
    const/4 v0, 0x1

    :try_start_20
    invoke-static {v0}, LR/o;->a(Z)Z

    .line 276
    const-class v0, LR/o;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 277
    monitor-exit v1

    .line 279
    :cond_29
    return-void

    .line 267
    :cond_2a
    const/4 v0, 0x0

    goto :goto_6

    .line 277
    :catchall_2c
    move-exception v0

    monitor-exit v1
    :try_end_2e
    .catchall {:try_start_20 .. :try_end_2e} :catchall_2c

    throw v0
.end method

.method public a(Law/g;)V
    .registers 3
    .parameter

    .prologue
    .line 257
    instance-of v0, p1, LR/r;

    if-eqz v0, :cond_12

    .line 258
    iget-object v0, p0, LR/q;->a:Law/h;

    invoke-virtual {v0, p0}, Law/h;->b(Law/q;)V

    .line 259
    iget-boolean v0, p0, LR/q;->c:Z

    if-eqz v0, :cond_12

    .line 260
    iget-object v0, p0, LR/q;->a:Law/h;

    invoke-virtual {v0}, Law/h;->g()V

    .line 263
    :cond_12
    return-void
.end method

.method public b(Law/g;)V
    .registers 2
    .parameter

    .prologue
    .line 284
    return-void
.end method

.method public k()V
    .registers 1

    .prologue
    .line 289
    return-void
.end method
