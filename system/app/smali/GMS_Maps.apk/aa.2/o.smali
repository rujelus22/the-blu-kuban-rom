.class public LaA/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaA/d;


# instance fields
.field private final a:Lcom/google/googlenav/ui/s;

.field private final b:LaA/g;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/s;LaA/g;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    .line 40
    iput-object p2, p0, LaA/o;->b:LaA/g;

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/s;LaA/g;LaA/p;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, LaA/o;-><init>(Lcom/google/googlenav/ui/s;LaA/g;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v7, 0xbbd

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaA/c;

    .line 48
    const/16 v3, 0x6c

    const-string v4, "c"

    invoke-virtual {v0}, LaA/c;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v3

    if-eqz v3, :cond_23

    .line 53
    invoke-virtual {v0}, LaA/c;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Laj/a;->a(Ljava/lang/String;)V

    .line 56
    :cond_23
    iget-object v3, p0, LaA/o;->b:LaA/g;

    if-eqz v3, :cond_30

    iget-object v3, p0, LaA/o;->b:LaA/g;

    invoke-virtual {v3, v0}, LaA/g;->b(LaA/c;)Z

    move-result v3

    if-eqz v3, :cond_30

    .line 149
    :cond_2f
    :goto_2f
    return-void

    .line 60
    :cond_30
    iget v3, v0, LaA/c;->d:I

    const/16 v4, 0xbb9

    if-eq v3, v4, :cond_45

    .line 61
    iget-object v3, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v3

    .line 62
    invoke-virtual {v3}, Lbf/am;->u()Lbf/bk;

    move-result-object v4

    if-eqz v4, :cond_45

    .line 63
    invoke-virtual {v3, v1}, Lbf/am;->b(Z)V

    .line 67
    :cond_45
    iget v3, v0, LaA/c;->d:I

    sparse-switch v3, :sswitch_data_110

    .line 138
    :cond_4a
    :goto_4a
    iget-object v1, p0, LaA/o;->b:LaA/g;

    if-eqz v1, :cond_2f

    .line 142
    iget-object v1, p0, LaA/o;->b:LaA/g;

    invoke-virtual {v1}, LaA/g;->a()LaA/f;

    move-result-object v1

    .line 144
    if-eqz v1, :cond_59

    .line 145
    invoke-interface {v1, v0}, LaA/f;->a(LaA/c;)V

    .line 147
    :cond_59
    iget-object v1, p0, LaA/o;->b:LaA/g;

    invoke-virtual {v1, v0}, LaA/g;->a(LaA/c;)V

    goto :goto_2f

    .line 71
    :sswitch_5f
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v2, v2}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    .line 72
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->f(Z)V

    .line 75
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    .line 76
    if-eqz v1, :cond_4a

    .line 77
    invoke-virtual {v1, v2}, Lbf/i;->a(B)V

    goto :goto_4a

    .line 81
    :sswitch_79
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->b(I)V

    goto :goto_4a

    .line 84
    :sswitch_80
    iget-object v3, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3, v1, v1}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    .line 85
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->f(Z)V

    goto :goto_4a

    .line 88
    :sswitch_8b
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/aA;->k()V

    goto :goto_4a

    .line 92
    :sswitch_95
    iget-object v3, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    iget v4, v0, LaA/c;->d:I

    if-ne v4, v7, :cond_a1

    :goto_9b
    const/16 v2, 0x13c

    invoke-virtual {v3, v1, v2}, Lcom/google/googlenav/ui/s;->a(ZI)V

    goto :goto_4a

    :cond_a1
    move v1, v2

    goto :goto_9b

    .line 99
    :sswitch_a3
    iget v3, v0, LaA/c;->d:I

    if-eq v3, v7, :cond_ad

    .line 100
    :goto_a7
    iget-object v2, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-static {v2, v1}, Lcom/google/googlenav/friend/ac;->a(Lcom/google/googlenav/ui/s;Z)V

    goto :goto_4a

    :cond_ad
    move v1, v2

    .line 99
    goto :goto_a7

    .line 103
    :sswitch_af
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v6}, Lcom/google/googlenav/ui/s;->g(Ljava/lang/String;)V

    goto :goto_4a

    .line 106
    :sswitch_b5
    iget-object v3, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3, v1}, Lcom/google/googlenav/ui/s;->f(Z)V

    .line 107
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v2, v2}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    goto :goto_4a

    .line 110
    :sswitch_c0
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    new-instance v2, Lax/j;

    invoke-direct {v2}, Lax/j;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->b(Lax/k;)V

    goto :goto_4a

    .line 113
    :sswitch_cb
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v6, v6}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/R;)V

    goto/16 :goto_4a

    .line 118
    :sswitch_d2
    sget-object v3, LaA/b;->k:LaA/c;

    invoke-virtual {v3}, LaA/c;->a()I

    move-result v3

    if-lez v3, :cond_f0

    .line 121
    :goto_da
    sget-object v3, LaA/b;->k:LaA/c;

    sget v4, LaA/c;->a:I

    invoke-virtual {v3, v4}, LaA/c;->a(I)V

    .line 125
    if-eqz v1, :cond_f2

    const-string v1, "fn"

    .line 128
    :goto_e5
    iget-object v3, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->aA()Lcom/google/googlenav/offers/a;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/google/googlenav/offers/a;->a(ZLjava/lang/String;)V

    goto/16 :goto_4a

    :cond_f0
    move v1, v2

    .line 118
    goto :goto_da

    .line 125
    :cond_f2
    const-string v1, "f"

    goto :goto_e5

    .line 131
    :sswitch_f5
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/aA;->h()V

    goto/16 :goto_4a

    .line 134
    :sswitch_100
    iget-object v1, p0, LaA/o;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->Y()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    goto/16 :goto_4a

    .line 67
    nop

    :sswitch_data_110
    .sparse-switch
        0x9cd -> :sswitch_100
        0x9d4 -> :sswitch_f5
        0xbb9 -> :sswitch_5f
        0xbba -> :sswitch_79
        0xbbb -> :sswitch_8b
        0xbbc -> :sswitch_80
        0xbbd -> :sswitch_95
        0xbbe -> :sswitch_95
        0xbbf -> :sswitch_b5
        0xbc0 -> :sswitch_af
        0xbc1 -> :sswitch_c0
        0xbc2 -> :sswitch_cb
        0xbc3 -> :sswitch_a3
        0xbc4 -> :sswitch_d2
    .end sparse-switch
.end method
