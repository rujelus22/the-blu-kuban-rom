.class public LaP/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 37
    const v0, 0x7f040158

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    sput-object v0, LaP/a;->a:Landroid/view/View;

    return-void
.end method

.method public static a(Lcom/google/googlenav/android/BaseMapsActivity;I)Landroid/graphics/drawable/Drawable;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 51
    packed-switch p1, :pswitch_data_52

    .line 59
    invoke-virtual {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_4d

    const v0, 0x7f020087

    :goto_14
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_18
    return-object v0

    .line 53
    :pswitch_19
    invoke-virtual {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_2f

    const v0, 0x7f020040

    :goto_2a
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_18

    :cond_2f
    const v0, 0x7f02003f

    goto :goto_2a

    .line 56
    :pswitch_33
    invoke-virtual {p0}, Lcom/google/googlenav/android/BaseMapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_49

    const v0, 0x7f020042

    :goto_44
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_18

    :cond_49
    const v0, 0x7f020041

    goto :goto_44

    .line 59
    :cond_4d
    const v0, 0x7f020086

    goto :goto_14

    .line 51
    nop

    :pswitch_data_52
    .packed-switch 0x4
        :pswitch_19
        :pswitch_33
    .end packed-switch
.end method

.method public static a(I)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 112
    const/4 v0, 0x0

    .line 113
    sparse-switch p0, :sswitch_data_8c

    .line 172
    :goto_4
    return-object v0

    .line 115
    :sswitch_5
    const/16 v0, 0x4f5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 118
    :sswitch_c
    const/16 v0, 0x2c3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 121
    :sswitch_13
    const/16 v0, 0x38c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 124
    :sswitch_1a
    const/16 v0, 0xf1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 127
    :sswitch_21
    const/16 v0, 0x1ba

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 130
    :sswitch_28
    const/16 v0, 0x2f5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 133
    :sswitch_2f
    const/16 v0, 0x230

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 136
    :sswitch_36
    const/16 v0, 0xd1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 139
    :sswitch_3d
    const/16 v0, 0x349

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 142
    :sswitch_44
    const/16 v0, 0x34e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 145
    :sswitch_4b
    const/16 v0, 0x13c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 148
    :sswitch_52
    const/16 v0, 0x2db

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 151
    :sswitch_59
    const/16 v0, 0x31c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 154
    :sswitch_60
    const/16 v0, 0x49c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 157
    :sswitch_67
    const/16 v0, 0x4b2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 160
    :sswitch_6e
    const/16 v0, 0x524

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 163
    :sswitch_75
    const/16 v0, 0x1d8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 166
    :sswitch_7c
    const/16 v0, 0x3e6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 169
    :sswitch_83
    const/16 v0, 0x306

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 113
    nop

    :sswitch_data_8c
    .sparse-switch
        0x7f10007a -> :sswitch_59
        0x7f1002ef -> :sswitch_83
        0x7f100498 -> :sswitch_1a
        0x7f10049b -> :sswitch_5
        0x7f1004ad -> :sswitch_21
        0x7f1004b6 -> :sswitch_75
        0x7f1004bd -> :sswitch_36
        0x7f1004be -> :sswitch_3d
        0x7f1004bf -> :sswitch_44
        0x7f1004c0 -> :sswitch_6e
        0x7f1004c1 -> :sswitch_13
        0x7f1004c2 -> :sswitch_2f
        0x7f1004c3 -> :sswitch_7c
        0x7f1004c4 -> :sswitch_28
        0x7f1004c5 -> :sswitch_c
        0x7f1004c6 -> :sswitch_4b
        0x7f1004c7 -> :sswitch_52
        0x7f1004d1 -> :sswitch_60
        0x7f1004d2 -> :sswitch_67
    .end sparse-switch
.end method

.method public static a(Landroid/view/Menu;)V
    .registers 5
    .parameter

    .prologue
    .line 89
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p0}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_14

    .line 90
    invoke-interface {p0, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 91
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 92
    const v3, 0x102002c

    if-ne v2, v3, :cond_15

    .line 101
    :cond_14
    return-void

    .line 97
    :cond_15
    invoke-static {v2}, LaP/a;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 99
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static a(Landroid/view/MenuItem;)V
    .registers 2
    .parameter

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 70
    return-void
.end method

.method public static a(Landroid/view/MenuItem;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 179
    if-eqz p0, :cond_2f

    .line 180
    invoke-static {p0}, LaP/a;->c(Landroid/view/MenuItem;)I

    move-result v0

    .line 181
    if-lez v0, :cond_2f

    .line 182
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "a=s"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "i="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 184
    const/16 v1, 0x10

    invoke-static {v1, p1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 188
    :cond_2f
    return-void
.end method

.method public static b(Landroid/view/Menu;)V
    .registers 1
    .parameter

    .prologue
    .line 249
    return-void
.end method

.method public static b(Landroid/view/MenuItem;)V
    .registers 2
    .parameter

    .prologue
    .line 77
    sget-object v0, LaP/a;->a:Landroid/view/View;

    invoke-interface {p0, v0}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 78
    return-void
.end method

.method private static c(Landroid/view/MenuItem;)I
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x2

    .line 195
    invoke-interface {p0}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 196
    const/4 v1, -0x1

    .line 197
    sparse-switch v2, :sswitch_data_28

    move v0, v1

    .line 235
    :goto_a
    :sswitch_a
    return v0

    .line 199
    :sswitch_b
    const/4 v0, 0x1

    .line 200
    goto :goto_a

    .line 208
    :sswitch_d
    const/16 v0, 0x11

    .line 209
    goto :goto_a

    .line 211
    :sswitch_10
    const/16 v0, 0xe

    .line 212
    goto :goto_a

    .line 214
    :sswitch_13
    const/16 v0, 0x2e

    .line 215
    goto :goto_a

    .line 217
    :sswitch_16
    const/4 v0, 0x5

    .line 218
    goto :goto_a

    .line 220
    :sswitch_18
    const/16 v0, 0x43

    .line 221
    goto :goto_a

    .line 223
    :sswitch_1b
    const/16 v0, 0x44

    .line 224
    goto :goto_a

    .line 226
    :sswitch_1e
    const/16 v0, 0x2f

    .line 227
    goto :goto_a

    .line 229
    :sswitch_21
    const/16 v0, 0x34

    .line 230
    goto :goto_a

    .line 232
    :sswitch_24
    const/16 v0, 0x13

    goto :goto_a

    .line 197
    nop

    :sswitch_data_28
    .sparse-switch
        0x7f100498 -> :sswitch_a
        0x7f10049b -> :sswitch_b
        0x7f1004ad -> :sswitch_a
        0x7f1004b6 -> :sswitch_24
        0x7f1004bd -> :sswitch_16
        0x7f1004be -> :sswitch_18
        0x7f1004bf -> :sswitch_1b
        0x7f1004c0 -> :sswitch_21
        0x7f1004c1 -> :sswitch_10
        0x7f1004c2 -> :sswitch_13
        0x7f1004c5 -> :sswitch_d
        0x7f1004c6 -> :sswitch_1e
    .end sparse-switch
.end method
