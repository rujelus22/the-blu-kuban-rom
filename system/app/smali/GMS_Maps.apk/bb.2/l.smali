.class Lbb/l;
.super Law/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lbb/j;

.field private final b:Lbm/i;

.field private final c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final d:LaN/B;

.field private final e:Lbb/s;

.field private final f:I

.field private final g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method public constructor <init>(Lbb/j;Lbb/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 254
    iput-object p1, p0, Lbb/l;->a:Lbb/j;

    invoke-direct {p0}, Law/a;-><init>()V

    .line 255
    iput-object p2, p0, Lbb/l;->e:Lbb/s;

    .line 256
    iput-object p3, p0, Lbb/l;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 257
    iput-object p4, p0, Lbb/l;->d:LaN/B;

    .line 258
    iput p5, p0, Lbb/l;->f:I

    .line 259
    iput-object p6, p0, Lbb/l;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "remoteSuggest ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 261
    new-instance v1, Lbm/i;

    const-string v2, "rsd"

    const/16 v3, 0x16

    invoke-direct {v1, v0, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v1, p0, Lbb/l;->b:Lbm/i;

    .line 264
    iget-object v0, p0, Lbb/l;->b:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    .line 265
    return-void
.end method

.method private a(I)I
    .registers 3
    .parameter

    .prologue
    .line 319
    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->e(Lbb/j;)I

    move-result v0

    return v0
.end method

.method private k()Lbb/z;
    .registers 15

    .prologue
    const/4 v13, 0x7

    const/4 v12, 0x3

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v11, 0x2

    .line 329
    new-instance v5, Lbb/z;

    iget-object v0, p0, Lbb/l;->e:Lbb/s;

    invoke-direct {v5, v0}, Lbb/z;-><init>(Lbb/s;)V

    .line 331
    iget-object v0, p0, Lbb/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-eqz v0, :cond_c6

    .line 332
    iget-object v0, p0, Lbb/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v11, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    move v0, v1

    move v2, v1

    .line 333
    :goto_1c
    invoke-virtual {v6, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-ge v0, v3, :cond_ab

    .line 334
    invoke-virtual {v6, v11, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    .line 335
    invoke-virtual {v7, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v8

    .line 336
    new-instance v3, Lbb/y;

    invoke-direct {v3}, Lbb/y;-><init>()V

    invoke-virtual {v7, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lbb/y;->a(Ljava/lang/String;)Lbb/y;

    move-result-object v3

    invoke-virtual {v3, v8}, Lbb/y;->a(I)Lbb/y;

    move-result-object v3

    invoke-virtual {v3, v4}, Lbb/y;->b(I)Lbb/y;

    move-result-object v3

    invoke-direct {p0, v8}, Lbb/l;->a(I)I

    move-result v9

    invoke-virtual {v3, v9}, Lbb/y;->c(I)Lbb/y;

    move-result-object v3

    iget-object v9, p0, Lbb/l;->a:Lbb/j;

    invoke-virtual {v9, v8}, Lbb/j;->a(I)I

    move-result v9

    invoke-virtual {v3, v9}, Lbb/y;->d(I)Lbb/y;

    move-result-object v3

    invoke-virtual {v3, v7}, Lbb/y;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbb/y;

    move-result-object v9

    .line 343
    invoke-virtual {v7, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_66

    .line 344
    invoke-virtual {v7, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 345
    invoke-virtual {v9, v3}, Lbb/y;->b(Ljava/lang/String;)Lbb/y;

    move-result-object v10

    invoke-virtual {v10, v3}, Lbb/y;->d(Ljava/lang/String;)Lbb/y;

    .line 347
    :cond_66
    const/16 v3, 0xf

    invoke-virtual {v7, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_77

    .line 348
    const/16 v3, 0xf

    invoke-virtual {v7, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Lbb/y;->c(Ljava/lang/String;)Lbb/y;

    .line 350
    :cond_77
    invoke-virtual {v7, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_88

    .line 351
    invoke-virtual {v7, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 352
    invoke-static {v3}, LaN/C;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v3

    invoke-virtual {v9, v3}, Lbb/y;->a(LaN/B;)Lbb/y;

    :cond_88
    move v3, v1

    .line 354
    :goto_89
    const/16 v10, 0xb

    invoke-virtual {v7, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v10

    if-ge v3, v10, :cond_9d

    .line 355
    const/16 v10, 0xb

    invoke-virtual {v7, v10, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v10

    invoke-virtual {v9, v10}, Lbb/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbb/y;

    .line 354
    add-int/lit8 v3, v3, 0x1

    goto :goto_89

    .line 357
    :cond_9d
    invoke-virtual {v9}, Lbb/y;->a()Lbb/w;

    move-result-object v3

    invoke-virtual {v5, v3}, Lbb/z;->a(Lbb/w;)V

    .line 359
    if-ne v8, v12, :cond_a7

    move v2, v4

    .line 333
    :cond_a7
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1c

    .line 365
    :cond_ab
    if-eqz v2, :cond_c6

    .line 366
    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->f(Lbb/j;)Lbb/w;

    move-result-object v0

    if-eqz v0, :cond_c6

    iget-object v0, p0, Lbb/l;->e:Lbb/s;

    invoke-virtual {v0}, Lbb/s;->h()Z

    move-result v0

    if-eqz v0, :cond_c6

    .line 367
    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->f(Lbb/j;)Lbb/w;

    move-result-object v0

    invoke-virtual {v5, v0}, Lbb/z;->a(Lbb/w;)V

    .line 371
    :cond_c6
    return-object v5
.end method

.method private l()V
    .registers 2

    .prologue
    .line 381
    monitor-enter p0

    .line 382
    :goto_1
    :try_start_1
    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->b(Lbb/j;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_24

    .line 383
    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->b(Lbb/j;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/l;

    .line 384
    if-ne v0, p0, :cond_1d

    .line 385
    monitor-exit p0

    .line 390
    :goto_1c
    return-void

    .line 387
    :cond_1d
    invoke-virtual {v0}, Lbb/l;->Z()V

    goto :goto_1

    .line 389
    :catchall_21
    move-exception v0

    monitor-exit p0
    :try_end_23
    .catchall {:try_start_1 .. :try_end_23} :catchall_21

    throw v0

    :cond_24
    :try_start_24
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_21

    goto :goto_1c
.end method


# virtual methods
.method public Z()V
    .registers 2

    .prologue
    .line 324
    invoke-super {p0}, Law/a;->Z()V

    .line 325
    iget-object v0, p0, Lbb/l;->b:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->c()V

    .line 326
    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 287
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gR;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 290
    const/4 v1, 0x5

    iget-object v2, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v2}, Lbb/j;->d(Lbb/j;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 292
    iget v1, p0, Lbb/l;->f:I

    if-eqz v1, :cond_1c

    .line 293
    const/4 v1, 0x4

    iget v2, p0, Lbb/l;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 295
    :cond_1c
    iget-object v1, p0, Lbb/l;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_26

    .line 296
    const/4 v1, 0x2

    iget-object v2, p0, Lbb/l;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 298
    :cond_26
    iget-object v1, p0, Lbb/l;->d:LaN/B;

    if-eqz v1, :cond_34

    .line 299
    const/4 v1, 0x3

    iget-object v2, p0, Lbb/l;->d:LaN/B;

    invoke-static {v2}, LaN/C;->c(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 302
    :cond_34
    iget-object v1, p0, Lbb/l;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_3f

    .line 303
    const/16 v1, 0xc

    iget-object v2, p0, Lbb/l;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 305
    :cond_3f
    iget-object v1, p0, Lbb/l;->e:Lbb/s;

    invoke-virtual {v1}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 306
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 307
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 308
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 309
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 310
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 3
    .parameter

    .prologue
    .line 448
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gR;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lbb/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 450
    const/4 v0, 0x1

    return v0
.end method

.method public a_()Z
    .registers 2

    .prologue
    .line 274
    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 269
    const/16 v0, 0x4c

    return v0
.end method

.method public b_()Z
    .registers 2

    .prologue
    .line 282
    const/4 v0, 0x0

    return v0
.end method

.method public d_()V
    .registers 4

    .prologue
    const/4 v1, 0x3

    const/4 v2, 0x1

    .line 394
    iget-object v0, p0, Lbb/l;->b:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    .line 397
    iget-object v0, p0, Lbb/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 398
    iget-object v0, p0, Lbb/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 399
    const/4 v1, -0x1

    invoke-static {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    .line 401
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1, v0}, LaM/f;->a(I)V

    .line 406
    :cond_21
    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->b(Lbb/j;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-virtual {p0}, Lbb/l;->z_()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 430
    :cond_33
    :goto_33
    return-void

    .line 411
    :cond_34
    iget-object v0, p0, Lbb/l;->h:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    .line 413
    iget-object v1, p0, Lbb/l;->e:Lbb/s;

    invoke-virtual {v1}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lau/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4a

    .line 416
    invoke-virtual {p0}, Lbb/l;->Z()V

    goto :goto_33

    .line 420
    :cond_4a
    invoke-direct {p0}, Lbb/l;->l()V

    .line 422
    invoke-direct {p0}, Lbb/l;->k()Lbb/z;

    move-result-object v0

    .line 423
    iget-object v1, p0, Lbb/l;->e:Lbb/s;

    invoke-static {v1}, Lbb/f;->f(Lbb/s;)Z

    move-result v1

    if-nez v1, :cond_60

    .line 426
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v1

    invoke-virtual {v1}, Lbb/o;->b()V

    .line 429
    :cond_60
    iget-object v1, p0, Lbb/l;->a:Lbb/j;

    invoke-virtual {v1, v0, v2}, Lbb/j;->a(Lbb/z;Z)V

    goto :goto_33
.end method

.method public u_()V
    .registers 5

    .prologue
    .line 434
    iget-object v0, p0, Lbb/l;->b:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->c()V

    .line 435
    invoke-direct {p0}, Lbb/l;->l()V

    .line 438
    iget-object v0, p0, Lbb/l;->a:Lbb/j;

    invoke-virtual {v0}, Lbb/j;->i()Z

    .line 441
    const/4 v0, 0x1

    .line 442
    iget-object v1, p0, Lbb/l;->a:Lbb/j;

    new-instance v2, Lbb/z;

    iget-object v3, p0, Lbb/l;->e:Lbb/s;

    invoke-direct {v2, v3}, Lbb/z;-><init>(Lbb/s;)V

    invoke-virtual {v1, v2, v0}, Lbb/j;->a(Lbb/z;Z)V

    .line 443
    return-void
.end method
