.class public abstract Lbb/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbb/r;


# instance fields
.field private a:I

.field private b:Las/d;

.field private volatile c:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v1, p0, Lbb/f;->a:I

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lbb/f;->b:Las/d;

    .line 38
    iput-boolean v1, p0, Lbb/f;->c:Z

    return-void
.end method

.method public static a(II)LaN/B;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 187
    invoke-static {p0, p1}, Lbb/f;->b(II)Z

    move-result v0

    if-eqz v0, :cond_c

    new-instance v0, LaN/B;

    invoke-direct {v0, p0, p1}, LaN/B;-><init>(II)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private static b(II)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    const v0, 0xbebc200

    .line 197
    if-eq p0, v0, :cond_9

    if-eq p1, v0, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method protected static f(Lbb/s;)Z
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 209
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    if-eqz v1, :cond_12

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->k()Z

    move-result v1

    if-nez v1, :cond_12

    .line 228
    :cond_11
    :goto_11
    return v0

    .line 222
    :cond_12
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 228
    const/4 v0, 0x0

    goto :goto_11
.end method


# virtual methods
.method public L_()Z
    .registers 2

    .prologue
    .line 173
    iget-boolean v0, p0, Lbb/f;->c:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public a(I)I
    .registers 3
    .parameter

    .prologue
    .line 153
    const/4 v0, 0x0

    return v0
.end method

.method protected final declared-synchronized a(Lbb/z;)V
    .registers 5
    .parameter

    .prologue
    .line 107
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lbb/f;->i()Z

    .line 109
    new-instance v0, Lbb/g;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lbb/g;-><init>(Lbb/f;Las/c;Lbb/z;)V

    iput-object v0, p0, Lbb/f;->b:Las/d;

    .line 125
    iget-object v0, p0, Lbb/f;->b:Las/d;

    const-wide/16 v1, 0xbb8

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    .line 126
    iget-object v0, p0, Lbb/f;->b:Las/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Las/d;->b(I)V

    .line 127
    iget-object v0, p0, Lbb/f;->b:Las/d;

    invoke-virtual {v0}, Las/d;->g()V
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_23

    .line 128
    monitor-exit p0

    return-void

    .line 107
    :catchall_23
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Lbb/z;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lbb/f;->a(Lbb/z;ZZ)V

    .line 97
    return-void
.end method

.method protected a(Lbb/z;ZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 74
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, Lbb/f;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lbb/o;->a(I)V

    .line 84
    invoke-virtual {p1}, Lbb/z;->b()Lbb/s;

    move-result-object v1

    invoke-virtual {v1}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lbb/o;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lau/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 85
    invoke-virtual {p0}, Lbb/f;->c()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lbb/o;->a(Lbb/z;ZI)V

    .line 87
    :cond_24
    return-void
.end method

.method protected abstract a_(Lbb/s;)V
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 148
    const-string v0, "o"

    return-object v0
.end method

.method public b(I)V
    .registers 2
    .parameter

    .prologue
    .line 178
    iput p1, p0, Lbb/f;->a:I

    .line 179
    return-void
.end method

.method public b(Lbb/s;)Z
    .registers 3
    .parameter

    .prologue
    .line 163
    const/4 v0, 0x1

    return v0
.end method

.method public c(Lbb/s;)Z
    .registers 3
    .parameter

    .prologue
    .line 168
    const/4 v0, 0x1

    return v0
.end method

.method public d()[I
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 158
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v1, v0, v1

    return-object v0
.end method

.method public final e(Lbb/s;)V
    .registers 3
    .parameter

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbb/f;->c:Z

    .line 56
    invoke-virtual {p0, p1}, Lbb/f;->a_(Lbb/s;)V

    .line 57
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbb/f;->c:Z

    .line 62
    return-void
.end method

.method public declared-synchronized i()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 135
    monitor-enter p0

    :try_start_2
    iget-object v1, p0, Lbb/f;->b:Las/d;

    if-eqz v1, :cond_12

    .line 136
    iget-object v1, p0, Lbb/f;->b:Las/d;

    invoke-virtual {v1}, Las/d;->c()I

    move-result v1

    if-eqz v1, :cond_f

    const/4 v0, 0x1

    .line 137
    :cond_f
    const/4 v1, 0x0

    iput-object v1, p0, Lbb/f;->b:Las/d;
    :try_end_12
    .catchall {:try_start_2 .. :try_end_12} :catchall_14

    .line 140
    :cond_12
    monitor-exit p0

    return v0

    .line 135
    :catchall_14
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public j()I
    .registers 2

    .prologue
    .line 183
    iget v0, p0, Lbb/f;->a:I

    return v0
.end method
