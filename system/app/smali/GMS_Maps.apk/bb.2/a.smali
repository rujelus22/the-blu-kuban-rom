.class public Lbb/a;
.super Lbb/d;
.source "SourceFile"


# static fields
.field private static b:Lbb/a;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method static synthetic a(Lbb/a;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lbb/a;->a:Landroid/content/Context;

    return-object v0
.end method

.method public static a()Lbb/a;
    .registers 1

    .prologue
    .line 50
    sget-object v0, Lbb/a;->b:Lbb/a;

    return-object v0
.end method

.method private a(LaR/D;)V
    .registers 5
    .parameter

    .prologue
    .line 230
    const-string v0, "name=? AND address=?"

    .line 232
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1}, LaR/D;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, LaR/D;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 235
    new-instance v1, Lbb/b;

    invoke-direct {v1, p0, v0, p1}, Lbb/b;-><init>(Lbb/a;[Ljava/lang/String;LaR/D;)V

    .line 255
    new-instance v0, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    .line 256
    invoke-virtual {v0}, Las/b;->g()V

    .line 257
    return-void
.end method

.method private static a(Ljava/lang/String;Landroid/database/Cursor;IIIILbb/z;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x3

    .line 272
    if-nez p1, :cond_5

    .line 300
    :goto_4
    return-void

    .line 275
    :cond_5
    const/4 v0, 0x0

    :goto_6
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_83

    .line 276
    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 277
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 280
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_38

    .line 282
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 284
    :cond_38
    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 285
    const/4 v3, 0x6

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 286
    invoke-static {v2, v3}, Lbb/a;->a(II)LaN/B;

    move-result-object v2

    .line 288
    new-instance v3, Lbb/y;

    invoke-direct {v3}, Lbb/y;-><init>()V

    invoke-virtual {v3, v1}, Lbb/y;->a(Ljava/lang/String;)Lbb/y;

    move-result-object v3

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbb/y;->b(Ljava/lang/String;)Lbb/y;

    move-result-object v3

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbb/y;->c(Ljava/lang/String;)Lbb/y;

    move-result-object v3

    invoke-virtual {v3, p2}, Lbb/y;->a(I)Lbb/y;

    move-result-object v3

    invoke-virtual {v3, p3}, Lbb/y;->b(I)Lbb/y;

    move-result-object v3

    add-int v4, p4, v0

    invoke-virtual {v3, v4}, Lbb/y;->c(I)Lbb/y;

    move-result-object v3

    invoke-virtual {v3, v1}, Lbb/y;->d(Ljava/lang/String;)Lbb/y;

    move-result-object v1

    invoke-virtual {v1, v2}, Lbb/y;->a(LaN/B;)Lbb/y;

    move-result-object v1

    invoke-virtual {v1, p5}, Lbb/y;->d(I)Lbb/y;

    move-result-object v1

    invoke-virtual {v1}, Lbb/y;->a()Lbb/w;

    move-result-object v1

    invoke-virtual {p6, v1}, Lbb/z;->b(Lbb/w;)V

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 299
    :cond_83
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    goto/16 :goto_4
.end method

.method private static b(Lcom/google/googlenav/ai;J)LaR/D;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 306
    if-nez p0, :cond_4

    .line 323
    :goto_3
    return-object v4

    .line 311
    :cond_4
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    .line 312
    const-string v0, ""

    .line 313
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 314
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v3

    .line 320
    :goto_18
    new-instance v0, LaR/D;

    const-string v1, ""

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LaR/D;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    .line 321
    invoke-virtual {v0, p1, p2}, LaR/D;->a(J)V

    .line 322
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaR/D;->a(LaN/B;)V

    move-object v4, v0

    .line 323
    goto :goto_3

    .line 315
    :cond_2c
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 316
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v3

    goto :goto_18

    .line 318
    :cond_3b
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v3

    goto :goto_18
.end method


# virtual methods
.method public a(I)I
    .registers 3
    .parameter

    .prologue
    .line 125
    const/4 v0, 0x7

    return v0
.end method

.method public a(Ljava/lang/String;)Landroid/database/Cursor;
    .registers 3
    .parameter

    .prologue
    .line 149
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lbb/a;->a(Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;I)Landroid/database/Cursor;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 155
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 156
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 159
    const-string v1, "name like ? OR name like ? OR address like ? OR address like ?"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 165
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "%"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 166
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "% "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 167
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 176
    const/4 v4, -0x1

    if-eq v2, v4, :cond_d6

    .line 177
    invoke-virtual {v0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 178
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 179
    invoke-static {v4}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_73

    invoke-static {v0}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_d6

    .line 181
    :cond_73
    const-string v2, " OR ("

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    invoke-static {v4}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9f

    .line 183
    const-string v2, "name like ?"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "%"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 186
    :cond_9f
    invoke-static {v0}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_d1

    .line 187
    invoke-static {v4}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b0

    .line 188
    const-string v2, " AND "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :cond_b0
    const-string v2, "address like ?"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "%"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    :cond_d1
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    :cond_d6
    new-array v0, v6, [Ljava/lang/String;

    .line 199
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "lastUpdated desc LIMIT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 203
    iget-object v0, p0, Lbb/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/provider/LocalActivePlacesProvider;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 205
    return-object v0
.end method

.method protected declared-synchronized a(Lbb/s;)Lbb/z;
    .registers 9
    .parameter

    .prologue
    .line 75
    monitor-enter p0

    :try_start_1
    new-instance v6, Lbb/z;

    invoke-direct {v6, p1}, Lbb/z;-><init>(Lbb/s;)V

    .line 76
    const/4 v1, 0x0

    .line 79
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->s()Z

    move-result v0

    if-nez v0, :cond_16

    .line 80
    invoke-virtual {p0}, Lbb/a;->g()V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_39

    .line 99
    :cond_14
    :goto_14
    monitor-exit p0

    return-object v6

    .line 84
    :cond_16
    :try_start_16
    invoke-virtual {p1}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbb/a;->a(Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1d
    .catchall {:try_start_16 .. :try_end_1d} :catchall_39
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_16 .. :try_end_1d} :catch_3c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_16 .. :try_end_1d} :catch_45

    move-result-object v1

    .line 94
    :goto_1e
    :try_start_1e
    invoke-virtual {p1}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lbb/a;->c()I

    move-result v3

    invoke-virtual {p0}, Lbb/a;->j()I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lbb/a;->a(I)I

    move-result v5

    invoke-static/range {v0 .. v6}, Lbb/a;->a(Ljava/lang/String;Landroid/database/Cursor;IIIILbb/z;)V

    .line 96
    if-eqz v1, :cond_14

    .line 97
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_38
    .catchall {:try_start_1e .. :try_end_38} :catchall_39

    goto :goto_14

    .line 75
    :catchall_39
    move-exception v0

    monitor-exit p0

    throw v0

    .line 85
    :catch_3c
    move-exception v0

    .line 86
    :try_start_3d
    invoke-virtual {p0}, Lbb/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1e

    .line 87
    :catch_45
    move-exception v0

    .line 88
    invoke-virtual {p0}, Lbb/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4d
    .catchall {:try_start_3d .. :try_end_4d} :catchall_39

    goto :goto_1e
.end method

.method public a(Lcom/google/googlenav/ai;J)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 213
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->s()Z

    move-result v0

    if-nez v0, :cond_e

    .line 216
    invoke-virtual {p0}, Lbb/a;->g()V

    .line 223
    :cond_d
    :goto_d
    return-void

    .line 219
    :cond_e
    invoke-static {p1, p2, p3}, Lbb/a;->b(Lcom/google/googlenav/ai;J)LaR/D;

    move-result-object v0

    .line 220
    if-eqz v0, :cond_d

    .line 221
    invoke-direct {p0, v0}, Lbb/a;->a(LaR/D;)V

    goto :goto_d
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 115
    const-string v0, "p"

    return-object v0
.end method

.method public b(Lbb/s;)Z
    .registers 3
    .parameter

    .prologue
    .line 104
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->s()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-static {p1}, Lbb/a;->f(Lbb/s;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public c()I
    .registers 2

    .prologue
    .line 120
    const/4 v0, 0x5

    return v0
.end method

.method public c(Lbb/s;)Z
    .registers 3
    .parameter

    .prologue
    .line 110
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->s()Z

    move-result v0

    return v0
.end method

.method public d()[I
    .registers 4

    .prologue
    .line 130
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x7

    aput v2, v0, v1

    return-object v0
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 140
    const/4 v0, 0x1

    return v0
.end method

.method public g()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 145
    iget-object v0, p0, Lbb/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/provider/LocalActivePlacesProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 146
    return-void
.end method
