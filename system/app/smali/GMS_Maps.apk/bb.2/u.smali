.class public Lbb/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Z

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const-string v0, ""

    iput-object v0, p0, Lbb/u;->a:Ljava/lang/String;

    .line 78
    const/4 v0, 0x0

    iput v0, p0, Lbb/u;->b:I

    .line 80
    iput-boolean v1, p0, Lbb/u;->c:Z

    .line 82
    const/4 v0, -0x1

    iput v0, p0, Lbb/u;->d:I

    .line 84
    iput-boolean v1, p0, Lbb/u;->e:Z

    return-void
.end method


# virtual methods
.method public a()Lbb/s;
    .registers 11

    .prologue
    const/4 v2, 0x1

    .line 146
    iget v0, p0, Lbb/u;->b:I

    if-eqz v0, :cond_35

    .line 147
    new-array v0, v2, [Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/dA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v3

    .line 152
    :goto_12
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    if-eqz v0, :cond_3a

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v6

    .line 155
    :goto_24
    new-instance v0, Lbb/s;

    iget-object v1, p0, Lbb/u;->a:Ljava/lang/String;

    iget v2, p0, Lbb/u;->b:I

    iget v4, p0, Lbb/u;->d:I

    iget-boolean v5, p0, Lbb/u;->c:Z

    iget-boolean v8, p0, Lbb/u;->e:Z

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lbb/s;-><init>(Ljava/lang/String;ILjava/util/Set;IZJZLbb/t;)V

    return-object v0

    .line 149
    :cond_35
    invoke-static {}, Lbb/s;->a()Lcom/google/common/collect/ImmutableSet;

    move-result-object v3

    goto :goto_12

    .line 152
    :cond_3a
    const-wide/16 v6, 0x0

    goto :goto_24
.end method

.method public a(I)Lbb/u;
    .registers 2
    .parameter

    .prologue
    .line 122
    iput p1, p0, Lbb/u;->b:I

    .line 123
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbb/u;
    .registers 2
    .parameter

    .prologue
    .line 117
    iput-object p1, p0, Lbb/u;->a:Ljava/lang/String;

    .line 118
    return-object p0
.end method

.method public a(Z)Lbb/u;
    .registers 2
    .parameter

    .prologue
    .line 127
    iput-boolean p1, p0, Lbb/u;->c:Z

    .line 128
    return-object p0
.end method

.method public a([Ljava/lang/String;)Lbb/u;
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x2

    .line 92
    new-instance v0, Lbb/u;

    invoke-direct {v0}, Lbb/u;-><init>()V

    .line 94
    array-length v0, p1

    if-nez v0, :cond_13

    .line 95
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "At least one argument (= query) is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_13
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {p0, v0}, Lbb/u;->a(Ljava/lang/String;)Lbb/u;

    .line 100
    :try_start_19
    array-length v0, p1

    if-le v0, v1, :cond_26

    .line 101
    const/4 v0, 0x2

    aget-object v0, p1, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lbb/u;->a(I)Lbb/u;

    .line 103
    :cond_26
    array-length v0, p1

    if-le v0, v2, :cond_33

    .line 104
    const/4 v0, 0x3

    aget-object v0, p1, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lbb/u;->b(I)Lbb/u;

    .line 106
    :cond_33
    array-length v0, p1

    if-le v0, v3, :cond_40

    .line 107
    const/4 v0, 0x4

    aget-object v0, p1, v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lbb/u;->b(Z)Lbb/u;
    :try_end_40
    .catch Ljava/lang/NumberFormatException; {:try_start_19 .. :try_end_40} :catch_41

    .line 113
    :cond_40
    return-object p0

    .line 109
    :catch_41
    move-exception v0

    .line 110
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot parse feature type restrict"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public b(I)Lbb/u;
    .registers 2
    .parameter

    .prologue
    .line 132
    iput p1, p0, Lbb/u;->d:I

    .line 133
    return-object p0
.end method

.method public b(Z)Lbb/u;
    .registers 2
    .parameter

    .prologue
    .line 137
    iput-boolean p1, p0, Lbb/u;->e:Z

    .line 138
    return-object p0
.end method
