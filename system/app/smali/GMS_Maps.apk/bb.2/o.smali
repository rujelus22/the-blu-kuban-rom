.class public Lbb/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lbb/o;


# instance fields
.field private final b:Lbb/z;

.field private c:Lbb/q;

.field private d:Lbb/p;

.field private final e:Ljava/util/List;

.field private f:Ljava/util/Set;

.field private g:Ljava/util/Set;

.field private h:Lbb/v;

.field private i:Lcom/google/googlenav/android/aa;

.field private final j:Ljava/lang/Object;

.field private k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private l:J

.field private m:Lbb/s;

.field private n:Lbb/s;

.field private final o:Ljava/util/Set;

.field private p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbb/o;->j:Ljava/lang/Object;

    .line 195
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lbb/o;->e:Ljava/util/List;

    .line 196
    new-instance v0, Lbb/z;

    invoke-direct {v0}, Lbb/z;-><init>()V

    iput-object v0, p0, Lbb/o;->b:Lbb/z;

    .line 197
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lbb/o;->o:Ljava/util/Set;

    .line 198
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lbb/o;->g:Ljava/util/Set;

    .line 199
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lbb/o;->f:Ljava/util/Set;

    .line 200
    return-void
.end method

.method public static a()Lbb/o;
    .registers 1

    .prologue
    .line 224
    sget-object v0, Lbb/o;->a:Lbb/o;

    return-object v0
.end method

.method private a(Lbb/m;)V
    .registers 6
    .parameter

    .prologue
    .line 779
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 780
    :try_start_3
    invoke-direct {p0}, Lbb/o;->p()Z

    move-result v0

    if-nez v0, :cond_b

    .line 781
    monitor-exit v1

    .line 798
    :goto_a
    return-void

    .line 783
    :cond_b
    invoke-direct {p0}, Lbb/o;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_42

    .line 784
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lbb/o;->l:J

    .line 785
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lbb/m;->a(I)Lbb/m;

    .line 786
    iget-object v0, p0, Lbb/o;->c:Lbb/q;

    if-eqz v0, :cond_30

    .line 787
    iget-object v0, p0, Lbb/o;->c:Lbb/q;

    invoke-interface {v0}, Lbb/q;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbb/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbb/m;

    .line 793
    :cond_30
    :goto_30
    invoke-virtual {p1}, Lbb/m;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 794
    iget-object v2, p0, Lbb/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 796
    invoke-static {v0}, Lbb/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 797
    monitor-exit v1

    goto :goto_a

    :catchall_3f
    move-exception v0

    monitor-exit v1
    :try_end_41
    .catchall {:try_start_3 .. :try_end_41} :catchall_3f

    throw v0

    .line 790
    :cond_42
    :try_start_42
    invoke-direct {p0}, Lbb/o;->o()I

    move-result v0

    invoke-virtual {p1, v0}, Lbb/m;->a(I)Lbb/m;
    :try_end_49
    .catchall {:try_start_42 .. :try_end_49} :catchall_3f

    goto :goto_30
.end method

.method public static a(Lbb/q;Lbb/p;Lcom/google/googlenav/android/aa;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 206
    sget-object v0, Lbb/o;->a:Lbb/o;

    if-nez v0, :cond_b

    .line 207
    new-instance v0, Lbb/o;

    invoke-direct {v0}, Lbb/o;-><init>()V

    sput-object v0, Lbb/o;->a:Lbb/o;

    .line 209
    :cond_b
    if-eqz p0, :cond_11

    .line 210
    sget-object v0, Lbb/o;->a:Lbb/o;

    iput-object p0, v0, Lbb/o;->c:Lbb/q;

    .line 212
    :cond_11
    if-eqz p1, :cond_17

    .line 213
    sget-object v0, Lbb/o;->a:Lbb/o;

    iput-object p1, v0, Lbb/o;->d:Lbb/p;

    .line 215
    :cond_17
    if-eqz p2, :cond_1d

    .line 216
    sget-object v0, Lbb/o;->a:Lbb/o;

    iput-object p2, v0, Lbb/o;->i:Lcom/google/googlenav/android/aa;

    .line 218
    :cond_1d
    return-void
.end method

.method private a(Lbb/z;)V
    .registers 5
    .parameter

    .prologue
    .line 291
    invoke-virtual {p1}, Lbb/z;->b()Lbb/s;

    move-result-object v0

    invoke-virtual {v0}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v0

    .line 293
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 296
    :try_start_b
    iget-object v2, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v2}, Lbb/z;->b()Lbb/s;

    move-result-object v2

    invoke-virtual {v2}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lbb/o;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 297
    iget-object v0, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v0, p1}, Lbb/z;->a(Lbb/z;)V

    .line 299
    :cond_20
    monitor-exit v1

    .line 300
    return-void

    .line 299
    :catchall_22
    move-exception v0

    monitor-exit v1
    :try_end_24
    .catchall {:try_start_b .. :try_end_24} :catchall_22

    throw v0
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 10
    .parameter

    .prologue
    const/4 v5, 0x6

    const/4 v8, 0x4

    const/4 v4, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 668
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 669
    const-string v0, "<<<<\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 670
    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 671
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prefix: ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 673
    :cond_35
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_13d

    .line 674
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prefix_delta: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 679
    :goto_5b
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_81

    .line 680
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "timestamp:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    :cond_81
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_cd

    .line 683
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 684
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action timestamp:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 685
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 687
    :cond_cd
    invoke-virtual {p0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_144

    .line 688
    invoke-virtual {p0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 689
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "timestamp for impression:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 691
    const/4 v0, 0x0

    :goto_f8
    invoke-virtual {v2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-ge v0, v3, :cond_144

    .line 692
    invoke-virtual {v2, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 693
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "impression("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") count:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " source:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 691
    add-int/lit8 v0, v0, 0x1

    goto :goto_f8

    .line 677
    :cond_13d
    const-string v0, "prefix_delta: 1\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5b

    .line 698
    :cond_144
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_15d

    .line 699
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 700
    const-string v2, "input_index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 702
    :cond_15d
    const-string v0, ">>>>\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 704
    return-void
.end method

.method private a(Ljava/util/List;)V
    .registers 4
    .parameter

    .prologue
    .line 402
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/r;

    .line 404
    invoke-interface {v0}, Lbb/r;->h()V

    goto :goto_4

    .line 406
    :cond_14
    return-void
.end method

.method private a(Ljava/util/Set;)V
    .registers 6
    .parameter

    .prologue
    .line 764
    iget-object v0, p0, Lbb/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_5

    .line 770
    :cond_4
    return-void

    .line 767
    :cond_5
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 768
    iget-object v2, p0, Lbb/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    goto :goto_9
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 930
    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v2, v1

    .line 931
    :goto_7
    if-ge v2, v3, :cond_1f

    .line 932
    invoke-virtual {p0, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 933
    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-nez v5, :cond_1b

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 938
    :cond_1b
    :goto_1b
    return v0

    .line 931
    :cond_1c
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_1f
    move v0, v1

    .line 938
    goto :goto_1b
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 303
    if-nez p0, :cond_17

    const-string v0, ""

    move-object v1, v0

    .line 304
    :goto_5
    if-nez p1, :cond_1d

    const-string v0, ""

    .line 305
    :goto_9
    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_15

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    :cond_15
    const/4 v0, 0x1

    :goto_16
    return v0

    .line 303
    :cond_17
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_5

    .line 304
    :cond_1d
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 305
    :cond_22
    const/4 v0, 0x0

    goto :goto_16
.end method

.method private c(Lbb/s;)Ljava/util/List;
    .registers 5
    .parameter

    .prologue
    .line 414
    invoke-virtual {p0, p1}, Lbb/o;->b(Lbb/s;)Ljava/util/Set;

    move-result-object v0

    .line 415
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 416
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_10
    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 417
    invoke-virtual {p0, v0}, Lbb/o;->b(I)Lbb/r;

    move-result-object v0

    .line 418
    if-eqz v0, :cond_10

    .line 419
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 422
    :cond_2a
    return-object v1
.end method

.method private d(Lbb/s;)V
    .registers 7
    .parameter

    .prologue
    .line 431
    invoke-virtual {p1}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v1

    .line 432
    invoke-direct {p0, p1}, Lbb/o;->c(Lbb/s;)Ljava/util/List;

    move-result-object v2

    .line 434
    iget-object v3, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v3

    .line 435
    :try_start_b
    iget-object v0, p0, Lbb/o;->m:Lbb/s;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lbb/o;->m:Lbb/s;

    invoke-virtual {v0}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lau/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 438
    monitor-exit v3

    .line 479
    :goto_1c
    return-void

    .line 440
    :cond_1d
    invoke-direct {p0}, Lbb/o;->p()Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 442
    new-instance v4, Lbb/m;

    invoke-direct {v4}, Lbb/m;-><init>()V

    .line 445
    iget-object v0, p0, Lbb/o;->m:Lbb/s;

    if-eqz v0, :cond_8a

    iget-object v0, p0, Lbb/o;->m:Lbb/s;

    invoke-virtual {v0}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->i(Ljava/lang/String;)I

    move-result v0

    .line 447
    :goto_36
    invoke-static {v1}, Lau/b;->i(Ljava/lang/String;)I

    move-result v1

    sub-int v0, v1, v0

    invoke-virtual {v4, v0}, Lbb/m;->b(I)Lbb/m;

    .line 449
    invoke-virtual {p1}, Lbb/s;->f()Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 450
    invoke-virtual {p1}, Lbb/s;->e()I

    move-result v0

    invoke-virtual {v4, v0}, Lbb/m;->c(I)Lbb/m;

    .line 452
    :cond_4c
    invoke-direct {p0, v4}, Lbb/o;->a(Lbb/m;)V

    .line 458
    :cond_4f
    iput-object p1, p0, Lbb/o;->m:Lbb/s;

    .line 459
    monitor-exit v3
    :try_end_52
    .catchall {:try_start_b .. :try_end_52} :catchall_8c

    .line 462
    invoke-direct {p0, v2}, Lbb/o;->a(Ljava/util/List;)V

    .line 464
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 466
    :try_start_58
    iget-object v0, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v0, p1}, Lbb/z;->a(Lbb/s;)V

    .line 467
    monitor-exit v1
    :try_end_5e
    .catchall {:try_start_58 .. :try_end_5e} :catchall_8f

    .line 469
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_62
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_92

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/r;

    .line 473
    :try_start_6e
    invoke-interface {v0, p1}, Lbb/r;->e(Lbb/s;)V
    :try_end_71
    .catch Ljava/lang/Exception; {:try_start_6e .. :try_end_71} :catch_72

    goto :goto_62

    .line 474
    :catch_72
    move-exception v2

    .line 475
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SuggestManager, Provider:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_62

    .line 445
    :cond_8a
    const/4 v0, 0x0

    goto :goto_36

    .line 459
    :catchall_8c
    move-exception v0

    :try_start_8d
    monitor-exit v3
    :try_end_8e
    .catchall {:try_start_8d .. :try_end_8e} :catchall_8c

    throw v0

    .line 467
    :catchall_8f
    move-exception v0

    :try_start_90
    monitor-exit v1
    :try_end_91
    .catchall {:try_start_90 .. :try_end_91} :catchall_8f

    throw v0

    .line 478
    :cond_92
    invoke-direct {p0}, Lbb/o;->l()V

    goto :goto_1c
.end method

.method private e(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 970
    invoke-virtual {p0, p1}, Lbb/o;->b(I)Lbb/r;

    move-result-object v0

    .line 971
    if-eqz v0, :cond_b

    .line 972
    invoke-interface {v0}, Lbb/r;->b()Ljava/lang/String;

    move-result-object v0

    .line 975
    :goto_a
    return-object v0

    :cond_b
    const-string v0, "o"

    goto :goto_a
.end method

.method private declared-synchronized l()V
    .registers 4

    .prologue
    .line 558
    monitor-enter p0

    :try_start_1
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_1d

    .line 559
    :try_start_4
    iget-object v0, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v0}, Lbb/z;->c()Lbb/z;

    move-result-object v0

    .line 560
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_1a

    .line 561
    :try_start_b
    iget-object v1, p0, Lbb/o;->h:Lbb/v;

    if-eqz v1, :cond_18

    .line 564
    iget-object v1, p0, Lbb/o;->h:Lbb/v;

    invoke-virtual {p0}, Lbb/o;->h()Z

    move-result v2

    invoke-interface {v1, v0, v2}, Lbb/v;->a(Lbb/z;Z)V
    :try_end_18
    .catchall {:try_start_b .. :try_end_18} :catchall_1d

    .line 566
    :cond_18
    monitor-exit p0

    return-void

    .line 560
    :catchall_1a
    move-exception v0

    :try_start_1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_1b .. :try_end_1c} :catchall_1a

    :try_start_1c
    throw v0
    :try_end_1d
    .catchall {:try_start_1c .. :try_end_1d} :catchall_1d

    .line 558
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private m()V
    .registers 8

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v0, 0x1

    .line 712
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 713
    :try_start_7
    iget-object v2, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v2}, Lbb/z;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 714
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_1c

    .line 715
    invoke-direct {p0}, Lbb/o;->o()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 717
    invoke-direct {p0}, Lbb/o;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 718
    if-nez v1, :cond_1f

    .line 748
    :goto_1b
    return-void

    .line 714
    :catchall_1c
    move-exception v0

    :try_start_1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_1d .. :try_end_1e} :catchall_1c

    throw v0

    .line 724
    :cond_1f
    invoke-virtual {p0}, Lbb/o;->h()Z

    move-result v3

    if-nez v3, :cond_55

    .line 728
    :goto_25
    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_57

    .line 729
    new-instance v3, Lbb/m;

    invoke-direct {v3}, Lbb/m;-><init>()V

    .line 730
    invoke-virtual {v3, v2}, Lbb/m;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbb/m;

    move-result-object v2

    invoke-virtual {v2, v0}, Lbb/m;->a(Z)Lbb/m;

    .line 733
    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 734
    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-virtual {v3, v0}, Lbb/m;->b(I)Lbb/m;

    .line 737
    :cond_44
    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 738
    invoke-virtual {v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-virtual {v3, v0}, Lbb/m;->c(I)Lbb/m;

    .line 741
    :cond_51
    invoke-direct {p0, v3}, Lbb/o;->a(Lbb/m;)V

    goto :goto_1b

    .line 724
    :cond_55
    const/4 v0, 0x0

    goto :goto_25

    .line 743
    :cond_57
    invoke-virtual {v1, v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 744
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 746
    invoke-static {v1}, Lbb/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_1b
.end method

.method private n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 751
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 752
    :try_start_3
    iget-object v0, p0, Lbb/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_1b

    .line 753
    iget-object v0, p0, Lbb/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    .line 754
    if-lez v0, :cond_1b

    .line 755
    iget-object v2, p0, Lbb/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    monitor-exit v1

    .line 760
    :goto_1a
    return-object v0

    .line 759
    :cond_1b
    monitor-exit v1

    .line 760
    const/4 v0, 0x0

    goto :goto_1a

    .line 759
    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    throw v0
.end method

.method private o()I
    .registers 7

    .prologue
    .line 802
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 803
    :try_start_3
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lbb/o;->l:J

    sub-long/2addr v2, v4

    .line 804
    iget-wide v4, p0, Lbb/o;->l:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lbb/o;->l:J

    .line 805
    long-to-int v0, v2

    monitor-exit v1

    return v0

    .line 806
    :catchall_1a
    move-exception v0

    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    throw v0
.end method

.method private p()Z
    .registers 3

    .prologue
    .line 919
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 920
    :try_start_3
    iget-object v0, p0, Lbb/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_8
    monitor-exit v1

    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_8

    .line 921
    :catchall_c
    move-exception v0

    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    throw v0
.end method


# virtual methods
.method a(Ljava/lang/String;Z)I
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 579
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 603
    :goto_8
    return v0

    .line 582
    :cond_9
    iget-object v3, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v3

    move v1, v2

    .line 583
    :goto_d
    :try_start_d
    iget-object v4, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v4}, Lbb/z;->d()I

    move-result v4

    if-ge v1, v4, :cond_4a

    .line 584
    iget-object v4, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v4, v1}, Lbb/z;->a(I)Lbb/w;

    move-result-object v4

    .line 585
    invoke-virtual {v4}, Lbb/w;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_47

    .line 588
    if-eqz p2, :cond_44

    .line 590
    invoke-virtual {v4}, Lbb/w;->a()I

    move-result v4

    move v0, v2

    .line 591
    :goto_2c
    if-ge v2, v1, :cond_3f

    .line 592
    iget-object v5, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v5, v2}, Lbb/z;->a(I)Lbb/w;

    move-result-object v5

    invoke-virtual {v5}, Lbb/w;->a()I

    move-result v5

    if-ne v5, v4, :cond_3c

    .line 593
    add-int/lit8 v0, v0, 0x1

    .line 591
    :cond_3c
    add-int/lit8 v2, v2, 0x1

    goto :goto_2c

    .line 596
    :cond_3f
    monitor-exit v3

    goto :goto_8

    .line 601
    :catchall_41
    move-exception v0

    monitor-exit v3
    :try_end_43
    .catchall {:try_start_d .. :try_end_43} :catchall_41

    throw v0

    .line 598
    :cond_44
    :try_start_44
    monitor-exit v3

    move v0, v1

    goto :goto_8

    .line 583
    :cond_47
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 601
    :cond_4a
    monitor-exit v3
    :try_end_4b
    .catchall {:try_start_44 .. :try_end_4b} :catchall_41

    goto :goto_8
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 312
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 313
    :try_start_3
    iget-object v0, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v0, p1}, Lbb/z;->b(I)V

    .line 314
    monitor-exit v1

    .line 315
    return-void

    .line 314
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public a(III)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    .line 820
    :try_start_0
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_3} :catch_ac

    .line 821
    :try_start_3
    iget-object v0, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v0, p2}, Lbb/z;->a(I)Lbb/w;

    move-result-object v0

    .line 822
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_a9

    .line 830
    :goto_a
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/hb;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 831
    const/4 v1, 0x1

    invoke-direct {p0}, Lbb/o;->o()I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 832
    const/4 v1, 0x2

    invoke-virtual {v2, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 833
    const/4 v1, 0x3

    invoke-virtual {v2, v1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 834
    const/4 v1, 0x5

    invoke-virtual {v2, v1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 837
    if-eqz v0, :cond_b0

    .line 838
    const/4 v1, 0x0

    :goto_28
    invoke-virtual {v0}, Lbb/w;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    array-length v3, v3

    if-ge v1, v3, :cond_b0

    .line 839
    invoke-virtual {v0}, Lbb/w;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    aget-object v3, v3, v1

    .line 840
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v5, Lcom/google/wireless/googlenav/proto/j2me/hb;->f:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 841
    const/4 v5, 0x1

    invoke-virtual {v0}, Lbb/w;->b()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v7

    const/4 v8, 0x2

    invoke-virtual {v3, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 843
    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_62

    .line 844
    const/4 v5, 0x2

    const/4 v6, 0x5

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 846
    :cond_62
    const/4 v5, 0x6

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_72

    .line 847
    const/4 v5, 0x3

    const/4 v6, 0x6

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 849
    :cond_72
    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_82

    .line 850
    const/4 v5, 0x4

    const/4 v6, 0x4

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 853
    :cond_82
    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_92

    .line 854
    const/4 v5, 0x5

    const/4 v6, 0x3

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 856
    :cond_92
    const/4 v5, 0x7

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_a2

    .line 857
    const/4 v5, 0x6

    const/4 v6, 0x7

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v3

    invoke-virtual {v4, v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 860
    :cond_a2
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 838
    add-int/lit8 v1, v1, 0x1

    goto :goto_28

    .line 822
    :catchall_a9
    move-exception v0

    :try_start_aa
    monitor-exit v1
    :try_end_ab
    .catchall {:try_start_aa .. :try_end_ab} :catchall_a9

    :try_start_ab
    throw v0
    :try_end_ac
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_ab .. :try_end_ac} :catch_ac

    .line 823
    :catch_ac
    move-exception v0

    .line 826
    const/4 v0, 0x0

    goto/16 :goto_a

    .line 863
    :cond_b0
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 864
    :try_start_b3
    invoke-direct {p0}, Lbb/o;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 866
    if-eqz v0, :cond_c0

    .line 867
    const/4 v3, 0x7

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 869
    invoke-static {v0}, Lbb/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 871
    :cond_c0
    monitor-exit v1

    .line 872
    return-void

    .line 871
    :catchall_c2
    move-exception v0

    monitor-exit v1
    :try_end_c4
    .catchall {:try_start_b3 .. :try_end_c4} :catchall_c2

    throw v0
.end method

.method public a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 889
    invoke-virtual {p0, p1}, Lbb/o;->d(I)V

    .line 890
    iput-object p2, p0, Lbb/o;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 891
    return-void
.end method

.method public a(Lbb/r;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 243
    invoke-interface {p1, p2}, Lbb/r;->b(I)V

    .line 244
    iget-object v0, p0, Lbb/o;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    invoke-interface {p1}, Lbb/r;->e()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 246
    iget-object v0, p0, Lbb/o;->f:Ljava/util/Set;

    invoke-interface {p1}, Lbb/r;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 248
    :cond_1b
    invoke-interface {p1}, Lbb/r;->f()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 249
    iget-object v0, p0, Lbb/o;->g:Ljava/util/Set;

    invoke-interface {p1}, Lbb/r;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 251
    :cond_2e
    return-void
.end method

.method public a(Lbb/s;)V
    .registers 5
    .parameter

    .prologue
    .line 489
    invoke-virtual {p0, p1}, Lbb/o;->b(Lbb/s;)Ljava/util/Set;

    move-result-object v0

    .line 490
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 491
    :try_start_7
    iget-object v2, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v2, v0}, Lbb/z;->a(Ljava/util/Set;)V

    .line 492
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_21

    .line 496
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbb/o;->b(I)Lbb/r;

    move-result-object v0

    check-cast v0, Lbb/j;

    .line 497
    if-eqz v0, :cond_1d

    .line 498
    invoke-virtual {p1}, Lbb/s;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lbb/j;->c(I)V

    .line 501
    :cond_1d
    invoke-direct {p0, p1}, Lbb/o;->d(Lbb/s;)V

    .line 502
    return-void

    .line 492
    :catchall_21
    move-exception v0

    :try_start_22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_22 .. :try_end_23} :catchall_21

    throw v0
.end method

.method public a(Lbb/v;)V
    .registers 2
    .parameter

    .prologue
    .line 983
    iput-object p1, p0, Lbb/o;->h:Lbb/v;

    .line 984
    return-void
.end method

.method public a(Lbb/z;ZI)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 341
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    invoke-direct {p0, p1}, Lbb/o;->a(Lbb/z;)V

    .line 343
    invoke-direct {p0}, Lbb/o;->p()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 344
    invoke-direct {p0}, Lbb/o;->m()V

    .line 346
    :cond_f
    if-eqz p2, :cond_14

    .line 347
    invoke-direct {p0}, Lbb/o;->l()V

    .line 350
    :cond_14
    invoke-virtual {p0}, Lbb/o;->h()Z

    move-result v0

    if-eqz v0, :cond_40

    .line 352
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 353
    :try_start_1d
    iget-object v0, p0, Lbb/o;->n:Lbb/s;

    if-eqz v0, :cond_39

    invoke-virtual {p1}, Lbb/z;->b()Lbb/s;

    move-result-object v0

    if-eqz v0, :cond_3f

    invoke-virtual {p1}, Lbb/z;->b()Lbb/s;

    move-result-object v0

    invoke-virtual {v0}, Lbb/s;->g()J

    move-result-wide v2

    iget-object v0, p0, Lbb/o;->n:Lbb/s;

    invoke-virtual {v0}, Lbb/s;->g()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_3f

    .line 356
    :cond_39
    invoke-virtual {p1}, Lbb/z;->b()Lbb/s;

    move-result-object v0

    iput-object v0, p0, Lbb/o;->n:Lbb/s;

    .line 358
    :cond_3f
    monitor-exit v1
    :try_end_40
    .catchall {:try_start_1d .. :try_end_40} :catchall_5c

    .line 362
    :cond_40
    invoke-virtual {p0, p3}, Lbb/o;->b(I)Lbb/r;

    move-result-object v0

    .line 363
    if-eqz v0, :cond_5f

    .line 364
    invoke-interface {v0}, Lbb/r;->d()[I

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4c
    if-ge v0, v2, :cond_5f

    aget v3, v1, v0

    .line 365
    iget-object v4, p0, Lbb/o;->o:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 364
    add-int/lit8 v0, v0, 0x1

    goto :goto_4c

    .line 358
    :catchall_5c
    move-exception v0

    :try_start_5d
    monitor-exit v1
    :try_end_5e
    .catchall {:try_start_5d .. :try_end_5e} :catchall_5c

    throw v0

    .line 368
    :cond_5f
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 625
    sget-object v0, Lcom/google/googlenav/ag;->a:Lcom/google/googlenav/ag;

    invoke-virtual {p0, p1, p2, v0}, Lbb/o;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    .line 626
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 642
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 643
    const/4 v0, 0x0

    :try_start_4
    invoke-virtual {p0, p1, v0}, Lbb/o;->a(Ljava/lang/String;Z)I

    move-result v0

    .line 644
    const/4 v2, -0x1

    if-ne v0, v2, :cond_d

    .line 645
    monitor-exit v1

    .line 665
    :goto_c
    return-void

    .line 647
    :cond_d
    invoke-virtual {p0, v0}, Lbb/o;->c(I)Lbb/w;

    move-result-object v2

    .line 648
    invoke-virtual {p0}, Lbb/o;->c()Ljava/lang/String;

    move-result-object v3

    .line 649
    const/4 v4, 0x1

    invoke-virtual {p0, p1, v4}, Lbb/o;->a(Ljava/lang/String;Z)I

    move-result v4

    .line 650
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_4 .. :try_end_1b} :catchall_e9

    .line 652
    const/16 v1, 0x49

    const-string v5, "s"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "i="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3}, Lau/b;->i(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "s"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lau/b;->i(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "o"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lbb/w;->a()I

    move-result v6

    invoke-direct {p0, v6}, Lbb/o;->e(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "y"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lbb/w;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "p"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "t"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "u"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "c"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/googlenav/ag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v5, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 650
    :catchall_e9
    move-exception v0

    :try_start_ea
    monitor-exit v1
    :try_end_eb
    .catchall {:try_start_ea .. :try_end_eb} :catchall_e9

    throw v0
.end method

.method public b(I)Lbb/r;
    .registers 5
    .parameter

    .prologue
    .line 508
    iget-object v0, p0, Lbb/o;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/r;

    .line 509
    invoke-interface {v0}, Lbb/r;->c()I

    move-result v2

    if-ne v2, p1, :cond_6

    .line 513
    :goto_18
    return-object v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public b(Lbb/s;)Ljava/util/Set;
    .registers 3
    .parameter

    .prologue
    .line 520
    invoke-virtual {p1}, Lbb/s;->d()Ljava/util/Set;

    move-result-object v0

    .line 526
    return-object v0
.end method

.method public b()V
    .registers 4

    .prologue
    .line 323
    iget-object v0, p0, Lbb/o;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/r;

    .line 324
    invoke-interface {v0}, Lbb/r;->i()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 327
    invoke-interface {v0}, Lbb/r;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lbb/o;->a(I)V

    goto :goto_6

    .line 330
    :cond_20
    return-void
.end method

.method public c(I)Lbb/w;
    .registers 4
    .parameter

    .prologue
    .line 613
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 614
    :try_start_3
    iget-object v0, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v0, p1}, Lbb/z;->a(I)Lbb/w;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 615
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    throw v0
.end method

.method public c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 375
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 376
    :try_start_3
    iget-object v0, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v0}, Lbb/z;->b()Lbb/s;

    move-result-object v0

    invoke-virtual {v0}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 377
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public d()Lbb/s;
    .registers 3

    .prologue
    .line 381
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 382
    :try_start_3
    iget-object v0, p0, Lbb/o;->n:Lbb/s;

    monitor-exit v1

    return-object v0

    .line 383
    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    throw v0
.end method

.method public d(I)V
    .registers 5
    .parameter

    .prologue
    .line 899
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 900
    :try_start_3
    invoke-direct {p0}, Lbb/o;->p()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 901
    invoke-virtual {p0}, Lbb/o;->j()V

    .line 903
    :cond_c
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/hb;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Lbb/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 904
    if-eqz p1, :cond_1d

    .line 905
    iget-object v0, p0, Lbb/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 907
    :cond_1d
    iget-object v0, p0, Lbb/o;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 908
    iget-object v0, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v0}, Lbb/z;->a()V

    .line 909
    const/4 v0, 0x0

    iput-object v0, p0, Lbb/o;->m:Lbb/s;

    .line 910
    const/4 v0, 0x0

    iput-object v0, p0, Lbb/o;->n:Lbb/s;

    .line 911
    monitor-exit v1

    .line 912
    return-void

    .line 911
    :catchall_2f
    move-exception v0

    monitor-exit v1
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    throw v0
.end method

.method public e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 390
    iget-object v0, p0, Lbb/o;->c:Lbb/q;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lbb/o;->c:Lbb/q;

    invoke-interface {v0}, Lbb/q;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_5
.end method

.method public f()LaN/B;
    .registers 2

    .prologue
    .line 394
    iget-object v0, p0, Lbb/o;->d:Lbb/p;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lbb/o;->d:Lbb/p;

    invoke-interface {v0}, Lbb/p;->a()LaN/B;

    move-result-object v0

    goto :goto_5
.end method

.method public g()Lcom/google/common/collect/ImmutableSet;
    .registers 4

    .prologue
    .line 533
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->g()Lcom/google/common/collect/aD;

    move-result-object v1

    .line 534
    iget-object v0, p0, Lbb/o;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/r;

    .line 535
    invoke-interface {v0}, Lbb/r;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/common/collect/aD;->b(Ljava/lang/Object;)Lcom/google/common/collect/aD;

    goto :goto_a

    .line 537
    :cond_22
    invoke-virtual {v1}, Lcom/google/common/collect/aD;->a()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .registers 3

    .prologue
    .line 544
    iget-object v0, p0, Lbb/o;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/r;

    .line 545
    invoke-interface {v0}, Lbb/r;->L_()Z

    move-result v0

    if-nez v0, :cond_6

    .line 546
    const/4 v0, 0x0

    .line 549
    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x1

    goto :goto_19
.end method

.method public i()V
    .registers 4

    .prologue
    .line 879
    invoke-direct {p0}, Lbb/o;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 880
    if-eqz v0, :cond_e

    .line 881
    const/4 v1, 0x3

    invoke-virtual {p0}, Lbb/o;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 883
    :cond_e
    return-void
.end method

.method public j()V
    .registers 3

    .prologue
    .line 946
    iget-object v1, p0, Lbb/o;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 947
    :try_start_3
    invoke-direct {p0}, Lbb/o;->p()Z

    move-result v0

    if-nez v0, :cond_21

    .line 956
    :cond_9
    :goto_9
    const/4 v0, 0x0

    iput-object v0, p0, Lbb/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 957
    const/4 v0, 0x0

    iput-object v0, p0, Lbb/o;->m:Lbb/s;

    .line 958
    const/4 v0, 0x0

    iput-object v0, p0, Lbb/o;->n:Lbb/s;

    .line 959
    iget-object v0, p0, Lbb/o;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 960
    iget-object v0, p0, Lbb/o;->b:Lbb/z;

    invoke-virtual {v0}, Lbb/z;->a()V

    .line 961
    const/4 v0, 0x0

    iput-object v0, p0, Lbb/o;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 963
    monitor-exit v1

    .line 964
    return-void

    .line 949
    :cond_21
    iget-object v0, p0, Lbb/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lbb/o;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 952
    iget-object v0, p0, Lbb/o;->o:Ljava/util/Set;

    invoke-direct {p0, v0}, Lbb/o;->a(Ljava/util/Set;)V

    .line 953
    iget-object v0, p0, Lbb/o;->k:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lbm/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_9

    .line 963
    :catchall_34
    move-exception v0

    monitor-exit v1
    :try_end_36
    .catchall {:try_start_3 .. :try_end_36} :catchall_34

    throw v0
.end method

.method public k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 979
    iget-object v0, p0, Lbb/o;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method
