.class public Lbb/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:Z

.field private final d:Ljava/util/Set;

.field private final e:J

.field private final f:Z

.field private final g:I


# direct methods
.method private constructor <init>(Ljava/lang/String;ILjava/util/Set;IZJZ)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    iput-object p1, p0, Lbb/s;->a:Ljava/lang/String;

    .line 165
    iput p2, p0, Lbb/s;->b:I

    .line 166
    iput-object p3, p0, Lbb/s;->d:Ljava/util/Set;

    .line 167
    iput-boolean p5, p0, Lbb/s;->c:Z

    .line 168
    iput p4, p0, Lbb/s;->g:I

    .line 169
    iput-wide p6, p0, Lbb/s;->e:J

    .line 170
    iput-boolean p8, p0, Lbb/s;->f:Z

    .line 171
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/util/Set;IZJZLbb/t;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct/range {p0 .. p8}, Lbb/s;-><init>(Ljava/lang/String;ILjava/util/Set;IZJZ)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lbb/s;
    .registers 2
    .parameter

    .prologue
    .line 213
    new-instance v0, Lbb/u;

    invoke-direct {v0}, Lbb/u;-><init>()V

    invoke-virtual {v0, p0}, Lbb/u;->a(Ljava/lang/String;)Lbb/u;

    move-result-object v0

    invoke-virtual {v0}, Lbb/u;->a()Lbb/s;

    move-result-object v0

    return-object v0
.end method

.method static a()Lcom/google/common/collect/ImmutableSet;
    .registers 1

    .prologue
    .line 55
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    .line 56
    if-nez v0, :cond_b

    .line 57
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    .line 59
    :goto_a
    return-object v0

    :cond_b
    invoke-virtual {v0}, Lbb/o;->g()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    goto :goto_a
.end method


# virtual methods
.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 174
    iget-object v0, p0, Lbb/s;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 178
    iget v0, p0, Lbb/s;->b:I

    return v0
.end method

.method public d()Ljava/util/Set;
    .registers 2

    .prologue
    .line 186
    iget-object v0, p0, Lbb/s;->d:Ljava/util/Set;

    return-object v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 194
    iget v0, p0, Lbb/s;->g:I

    return v0
.end method

.method public f()Z
    .registers 3

    .prologue
    .line 198
    iget v0, p0, Lbb/s;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public g()J
    .registers 3

    .prologue
    .line 202
    iget-wide v0, p0, Lbb/s;->e:J

    return-wide v0
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 206
    iget-boolean v0, p0, Lbb/s;->f:Z

    return v0
.end method

.method public i()[Ljava/lang/String;
    .registers 8

    .prologue
    .line 221
    const/4 v0, 0x0

    .line 222
    iget v1, p0, Lbb/s;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 223
    iget v2, p0, Lbb/s;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 224
    iget-boolean v3, p0, Lbb/s;->f:Z

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    .line 225
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lbb/s;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    const/4 v0, 0x2

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object v2, v4, v0

    const/4 v0, 0x4

    aput-object v3, v4, v0

    return-object v4
.end method
