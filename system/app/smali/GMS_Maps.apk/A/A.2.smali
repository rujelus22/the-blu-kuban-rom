.class public abstract LA/A;
.super LA/m;
.source "SourceFile"


# instance fields
.field protected g:LA/z;

.field protected final h:Ljava/lang/Class;


# virtual methods
.method public a(LA/j;LA/m;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 104
    if-nez p2, :cond_d

    .line 105
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 107
    const-string v0, "ShaderState"

    const-string v1, "glUseProgram"

    invoke-static {v0, v1}, LA/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_d
    return-void
.end method

.method public a(LA/j;LA/i;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-super {p0, p1, p2}, LA/m;->a(LA/j;LA/i;)Z

    move-result v0

    .line 75
    iget-object v1, p0, LA/A;->g:LA/z;

    if-nez v1, :cond_14

    .line 76
    invoke-virtual {p1}, LA/j;->d()LA/y;

    move-result-object v1

    iget-object v2, p0, LA/A;->h:Ljava/lang/Class;

    invoke-interface {v1, v2}, LA/y;->a(Ljava/lang/Class;)LA/z;

    move-result-object v1

    iput-object v1, p0, LA/A;->g:LA/z;

    .line 80
    :cond_14
    if-eqz v0, :cond_1b

    .line 81
    iget-object v1, p0, LA/A;->g:LA/z;

    invoke-virtual {v1, p1, p2}, LA/z;->a(LA/j;LA/i;)Z

    .line 84
    :cond_1b
    return v0
.end method
