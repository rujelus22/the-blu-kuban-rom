.class public abstract LA/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field private static final A:Ljava/util/Map;

.field private static final G:Lo/ai;

.field private static final H:Lo/an;

.field private static final I:Lo/an;

.field private static final J:Lo/an;

.field private static final K:Lo/ao;

.field private static final L:Lo/aj;

.field private static final M:Lo/aj;

.field private static final N:Lo/aj;

.field private static O:LA/r;

.field public static final a:LA/c;

.field public static final b:LA/c;

.field public static final c:LA/c;

.field public static final d:LA/c;

.field public static final e:LA/c;

.field public static final f:LA/c;

.field public static final g:LA/c;

.field public static final h:LA/c;

.field public static final i:LA/c;

.field public static final j:LA/c;

.field public static final k:LA/c;

.field public static final l:LA/c;

.field public static final m:LA/c;

.field public static final n:LA/c;

.field public static final o:LA/c;

.field public static final p:LA/c;

.field public static final q:LA/c;

.field public static final r:LA/c;

.field public static final s:LA/c;

.field public static final t:LA/c;

.field public static final u:LA/c;


# instance fields
.field private final B:I

.field private final C:Z

.field private final D:Z

.field private final E:Z

.field private final F:Lt/J;

.field public final v:I

.field public final w:I

.field public final x:Z

.field public final y:I

.field public final z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    .prologue
    const/16 v9, 0xc

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LA/c;->A:Ljava/util/Map;

    .line 76
    new-instance v0, LA/f;

    const/16 v3, 0xa

    invoke-direct {v0, v3, v8}, LA/f;-><init>(ILA/d;)V

    invoke-virtual {v0, v4}, LA/f;->f(Z)LA/t;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/t;->d(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->a:LA/c;

    .line 81
    new-instance v0, LA/f;

    const/16 v3, 0x16

    invoke-direct {v0, v3, v8}, LA/f;-><init>(ILA/d;)V

    invoke-virtual {v0, v4}, LA/f;->f(Z)LA/t;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/t;->d(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->b:LA/c;

    .line 87
    new-instance v0, LA/f;

    const/16 v3, 0x14

    invoke-direct {v0, v3, v8}, LA/f;-><init>(ILA/d;)V

    invoke-virtual {v0, v4}, LA/f;->f(Z)LA/t;

    move-result-object v0

    const-string v3, "_tran_base"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->d(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->c:LA/c;

    .line 93
    new-instance v0, LA/i;

    const/4 v3, 0x3

    invoke-direct {v0, v3, v8}, LA/i;-><init>(ILA/d;)V

    invoke-virtual {v0, v2}, LA/i;->c(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->d:LA/c;

    .line 97
    new-instance v0, LA/i;

    invoke-direct {v0, v9, v8}, LA/i;-><init>(ILA/d;)V

    const-string v3, "_ter"

    invoke-virtual {v0, v3}, LA/i;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v2}, LA/g;->c(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->e:LA/c;

    .line 102
    new-instance v0, LA/t;

    const/4 v3, 0x4

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_traf"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->f:LA/c;

    .line 105
    new-instance v0, LA/t;

    const/16 v3, 0x17

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_traf"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->g:LA/c;

    .line 108
    new-instance v0, LA/n;

    const/16 v3, 0x8

    invoke-direct {v0, v3, v8}, LA/n;-><init>(ILA/d;)V

    invoke-virtual {v0}, LA/n;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->h:LA/c;

    .line 110
    new-instance v0, LA/p;

    const/16 v3, 0xb

    invoke-direct {v0, v3, v8}, LA/p;-><init>(ILA/d;)V

    invoke-virtual {v0, v4}, LA/p;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->i:LA/c;

    .line 116
    new-instance v0, LA/f;

    const/16 v3, 0x12

    invoke-direct {v0, v3, v8}, LA/f;-><init>(ILA/d;)V

    invoke-virtual {v0, v4}, LA/f;->a(Z)LA/f;

    move-result-object v0

    const-string v3, "_vec_bic"

    invoke-virtual {v0, v3}, LA/f;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->j:LA/c;

    .line 127
    new-instance v0, LA/i;

    const/4 v3, 0x7

    invoke-direct {v0, v3, v8}, LA/i;-><init>(ILA/d;)V

    const/16 v3, 0x80

    invoke-virtual {v0, v3}, LA/i;->a(I)LA/g;

    move-result-object v0

    const-string v3, "_ter_bic"

    invoke-virtual {v0, v3}, LA/g;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->k:LA/c;

    .line 131
    new-instance v0, LA/i;

    const/4 v3, 0x6

    invoke-direct {v0, v3, v8}, LA/i;-><init>(ILA/d;)V

    const/16 v3, 0x80

    invoke-virtual {v0, v3}, LA/i;->a(I)LA/g;

    move-result-object v0

    const-string v3, "_hy_bic"

    invoke-virtual {v0, v3}, LA/g;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->l:LA/c;

    .line 135
    new-instance v0, LA/t;

    const/16 v3, 0xd

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_tran"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->m:LA/c;

    .line 139
    new-instance v0, LA/t;

    const/16 v3, 0xe

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    invoke-virtual {v0, v4}, LA/t;->e(Z)LA/g;

    move-result-object v0

    const-string v3, "_inaka"

    invoke-virtual {v0, v3}, LA/g;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->n:LA/c;

    .line 143
    new-instance v0, LA/l;

    const/16 v3, 0xf

    invoke-direct {v0, v3, v8}, LA/l;-><init>(ILA/d;)V

    const-string v3, "_labl"

    invoke-virtual {v0, v3}, LA/l;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->d(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->o:LA/c;

    .line 148
    new-instance v0, LA/l;

    const/16 v3, 0x15

    invoke-direct {v0, v3, v8}, LA/l;-><init>(ILA/d;)V

    const-string v3, "_tran_labl"

    invoke-virtual {v0, v3}, LA/l;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->d(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->p:LA/c;

    .line 153
    new-instance v0, LA/t;

    const/16 v3, 0x10

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_psm"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->b(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->q:LA/c;

    .line 158
    new-instance v0, LA/t;

    const/16 v3, 0x11

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_related"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->b(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->r:LA/c;

    .line 162
    new-instance v0, LA/t;

    const/16 v3, 0x18

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_high"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->b(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->s:LA/c;

    .line 167
    new-instance v0, LA/t;

    const/16 v3, 0x19

    invoke-direct {v0, v3, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_api"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->t:LA/c;

    .line 170
    new-instance v0, LA/t;

    invoke-direct {v0, v2, v8}, LA/t;-><init>(ILA/d;)V

    const-string v3, "_star"

    invoke-virtual {v0, v3}, LA/t;->a(Ljava/lang/String;)LA/g;

    move-result-object v0

    invoke-virtual {v0, v4}, LA/g;->b(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0, v2}, LA/g;->e(Z)LA/g;

    move-result-object v0

    invoke-virtual {v0}, LA/g;->a()LA/c;

    move-result-object v0

    sput-object v0, LA/c;->u:LA/c;

    .line 221
    new-instance v0, Lo/ai;

    const/4 v3, 0x0

    new-array v4, v2, [I

    invoke-direct {v0, v2, v3, v4, v2}, Lo/ai;-><init>(IF[II)V

    sput-object v0, LA/c;->G:Lo/ai;

    .line 222
    new-instance v0, Lo/an;

    const/high16 v3, -0x100

    sget-object v4, LA/c;->G:Lo/ai;

    invoke-direct {v0, v3, v4}, Lo/an;-><init>(ILo/ai;)V

    sput-object v0, LA/c;->H:Lo/an;

    .line 223
    new-instance v0, Lo/an;

    const v3, -0xffff01

    sget-object v4, LA/c;->G:Lo/ai;

    invoke-direct {v0, v3, v4}, Lo/an;-><init>(ILo/ai;)V

    sput-object v0, LA/c;->I:Lo/an;

    .line 224
    new-instance v0, Lo/an;

    const/high16 v3, -0x1

    sget-object v4, LA/c;->G:Lo/ai;

    invoke-direct {v0, v3, v4}, Lo/an;-><init>(ILo/ai;)V

    sput-object v0, LA/c;->J:Lo/an;

    .line 225
    new-instance v0, Lo/ao;

    const/16 v3, 0xa

    const v4, 0x3f99999a

    const/high16 v5, 0x3f80

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lo/ao;-><init>(IIIFFI)V

    sput-object v0, LA/c;->K:Lo/ao;

    .line 227
    new-instance v0, Lo/aj;

    sget-object v5, LA/c;->K:Lo/ao;

    sget-object v6, LA/c;->H:Lo/an;

    move v2, v9

    move-object v3, v8

    move-object v4, v8

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    sput-object v0, LA/c;->L:Lo/aj;

    .line 230
    new-instance v0, Lo/aj;

    sget-object v5, LA/c;->K:Lo/ao;

    sget-object v6, LA/c;->J:Lo/an;

    move v2, v9

    move-object v3, v8

    move-object v4, v8

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    sput-object v0, LA/c;->M:Lo/aj;

    .line 233
    new-instance v0, Lo/aj;

    sget-object v5, LA/c;->K:Lo/ao;

    sget-object v6, LA/c;->I:Lo/an;

    move v2, v9

    move-object v3, v8

    move-object v4, v8

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lo/aj;-><init>(II[I[Lo/ai;Lo/ao;Lo/an;Lo/ai;)V

    sput-object v0, LA/c;->N:Lo/aj;

    return-void
.end method

.method private constructor <init>(LA/g;)V
    .registers 6
    .parameter

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    invoke-static {p1}, LA/g;->a(LA/g;)I

    move-result v0

    iput v0, p0, LA/c;->v:I

    .line 244
    invoke-static {p1}, LA/g;->b(LA/g;)I

    move-result v0

    iput v0, p0, LA/c;->w:I

    .line 245
    invoke-static {p1}, LA/g;->c(LA/g;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LA/c;->z:Ljava/lang/String;

    .line 246
    invoke-static {p1}, LA/g;->d(LA/g;)Z

    move-result v0

    iput-boolean v0, p0, LA/c;->x:Z

    .line 247
    invoke-static {p1}, LA/g;->e(LA/g;)Z

    move-result v0

    iput-boolean v0, p0, LA/c;->C:Z

    .line 248
    invoke-static {p1}, LA/g;->f(LA/g;)Z

    move-result v0

    iput-boolean v0, p0, LA/c;->D:Z

    .line 249
    invoke-static {p1}, LA/g;->g(LA/g;)Z

    move-result v0

    iput-boolean v0, p0, LA/c;->E:Z

    .line 251
    iget-boolean v0, p0, LA/c;->E:Z

    if-eqz v0, :cond_7e

    invoke-virtual {p0}, LA/c;->l()Lt/J;

    move-result-object v0

    :goto_35
    iput-object v0, p0, LA/c;->F:Lt/J;

    .line 255
    iget v0, p0, LA/c;->v:I

    iget v1, p0, LA/c;->w:I

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    iput v0, p0, LA/c;->y:I

    .line 257
    sget-object v0, LA/c;->A:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, LA/c;->B:I

    .line 258
    iget v0, p0, LA/c;->v:I

    iget v1, p0, LA/c;->w:I

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 259
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_80

    sget-object v1, LA/c;->A:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_80

    .line 260
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Tile type with key "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " already defined"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 251
    :cond_7e
    const/4 v0, 0x0

    goto :goto_35

    .line 262
    :cond_80
    sget-object v1, LA/c;->A:Ljava/util/Map;

    invoke-interface {v1, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_9d

    sget-object v0, LA/c;->A:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x20

    if-le v0, v1, :cond_9d

    .line 267
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Currently maximum 32 tile types allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :cond_9d
    return-void
.end method

.method synthetic constructor <init>(LA/g;LA/d;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, LA/c;-><init>(LA/g;)V

    return-void
.end method

.method public static a(I)LA/c;
    .registers 3
    .parameter

    .prologue
    .line 405
    sget-object v0, LA/c;->A:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LA/c;

    return-object v0
.end method

.method static b()I
    .registers 2

    .prologue
    .line 370
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->h()I

    move-result v0

    shr-int/lit8 v0, v0, 0x3

    .line 371
    const/16 v1, 0x80

    mul-int/lit8 v0, v0, 0x12

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x24

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method static synthetic b(I)I
    .registers 2
    .parameter

    .prologue
    .line 64
    invoke-static {p0}, LA/c;->c(I)I

    move-result v0

    return v0
.end method

.method static synthetic b(LA/c;)Z
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-boolean v0, p0, LA/c;->C:Z

    return v0
.end method

.method static c()I
    .registers 2

    .prologue
    .line 382
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->h()I

    move-result v0

    shr-int/lit8 v0, v0, 0x3

    .line 383
    const/16 v1, 0x100

    mul-int/lit8 v0, v0, 0x20

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x40

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private static c(I)I
    .registers 2
    .parameter

    .prologue
    .line 357
    const/16 v0, 0xa0

    if-le p0, v0, :cond_6

    const/4 v0, 0x3

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x1

    goto :goto_5
.end method

.method static synthetic c(LA/c;)Z
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-boolean v0, p0, LA/c;->D:Z

    return v0
.end method

.method public static e()Ljava/lang/Iterable;
    .registers 1

    .prologue
    .line 412
    sget-object v0, LA/c;->A:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m()LA/r;
    .registers 1

    .prologue
    .line 64
    sget-object v0, LA/c;->O:LA/r;

    return-object v0
.end method

.method static synthetic n()Lo/aj;
    .registers 1

    .prologue
    .line 64
    sget-object v0, LA/c;->L:Lo/aj;

    return-object v0
.end method

.method static synthetic o()Lo/aj;
    .registers 1

    .prologue
    .line 64
    sget-object v0, LA/c;->M:Lo/aj;

    return-object v0
.end method

.method static synthetic p()Lo/aj;
    .registers 1

    .prologue
    .line 64
    sget-object v0, LA/c;->N:Lo/aj;

    return-object v0
.end method


# virtual methods
.method abstract a()I
.end method

.method public a(ILcom/google/android/maps/driveabout/vector/q;)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 454
    return p1
.end method

.method public a(LA/c;)I
    .registers 4
    .parameter

    .prologue
    .line 273
    iget v0, p0, LA/c;->B:I

    iget v1, p1, LA/c;->B:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public a(Lo/ad;)Lo/T;
    .registers 3
    .parameter

    .prologue
    .line 486
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a(Law/p;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lr/z;
.end method

.method public a(Ljava/lang/String;ZLt/g;)Lt/f;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 337
    iget-boolean v0, p0, LA/c;->E:Z

    if-nez v0, :cond_6

    .line 338
    const/4 v0, 0x0

    .line 340
    :goto_5
    return-object v0

    :cond_6
    new-instance v0, Lt/y;

    if-eqz p2, :cond_14

    const/4 v2, -0x1

    :goto_b
    iget-object v3, p0, LA/c;->F:Lt/J;

    move-object v1, p1

    move-object v4, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lt/y;-><init>(Ljava/lang/String;ILt/J;LA/c;Lt/g;)V

    goto :goto_5

    :cond_14
    invoke-virtual {p0}, LA/c;->a()I

    move-result v2

    goto :goto_b
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .registers 2
    .parameter

    .prologue
    .line 479
    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 64
    check-cast p1, LA/c;

    invoke-virtual {p0, p1}, LA/c;->a(LA/c;)I

    move-result v0

    return v0
.end method

.method public d()Lt/M;
    .registers 3

    .prologue
    .line 392
    new-instance v0, Lt/K;

    invoke-static {}, LA/c;->c()I

    move-result v1

    invoke-direct {v0, v1}, Lt/K;-><init>(I)V

    return-object v0
.end method

.method public f()I
    .registers 3

    .prologue
    .line 419
    iget v0, p0, LA/c;->v:I

    iget v1, p0, LA/c;->w:I

    add-int/2addr v0, v1

    return v0
.end method

.method public g()I
    .registers 2

    .prologue
    .line 426
    iget v0, p0, LA/c;->B:I

    return v0
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 461
    const/4 v0, 0x0

    return v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 469
    const/4 v0, 0x0

    return v0
.end method

.method public j()LF/O;
    .registers 2

    .prologue
    .line 493
    const/4 v0, 0x0

    return-object v0
.end method

.method public k()Lo/aj;
    .registers 2

    .prologue
    .line 500
    const/4 v0, 0x0

    return-object v0
.end method

.method abstract l()Lt/J;
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 434
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v2, :cond_1d

    aget-object v3, v1, v0

    .line 436
    :try_start_e
    invoke-virtual {v3, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-ne p0, v4, :cond_1a

    .line 437
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;
    :try_end_17
    .catch Ljava/lang/IllegalAccessException; {:try_start_e .. :try_end_17} :catch_19

    move-result-object v0

    .line 443
    :goto_18
    return-object v0

    .line 439
    :catch_19
    move-exception v3

    .line 434
    :cond_1a
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 443
    :cond_1d
    const-string v0, "unknown"

    goto :goto_18
.end method
