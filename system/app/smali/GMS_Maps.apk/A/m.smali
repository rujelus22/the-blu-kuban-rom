.class LA/m;
.super LA/c;
.source "SourceFile"


# direct methods
.method private constructor <init>(LA/n;)V
    .registers 3
    .parameter

    .prologue
    .line 944
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LA/c;-><init>(LA/g;LA/d;)V

    .line 945
    return-void
.end method

.method synthetic constructor <init>(LA/n;LA/d;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 916
    invoke-direct {p0, p1}, LA/m;-><init>(LA/n;)V

    return-void
.end method


# virtual methods
.method a()I
    .registers 3

    .prologue
    .line 937
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 938
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "LayerTileType does not support disk caches."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 940
    :cond_e
    const/4 v0, 0x0

    return v0
.end method

.method public a(Law/p;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lr/z;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 925
    const/16 v0, 0x100

    invoke-static {p3, v0}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v0

    .line 927
    new-instance v1, Lr/q;

    invoke-direct {v1, p1, v0, p4, p5}, Lr/q;-><init>(Law/p;ILjava/util/Locale;Ljava/io/File;)V

    return-object v1
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 916
    check-cast p1, LA/c;

    invoke-super {p0, p1}, LA/c;->a(LA/c;)I

    move-result v0

    return v0
.end method

.method public l()Lt/J;
    .registers 2

    .prologue
    .line 932
    const/4 v0, 0x0

    return-object v0
.end method
