.class public Lbj/aE;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# static fields
.field private static final b:Lcom/google/googlenav/ui/aV;


# instance fields
.field protected final a:Lbj/aP;

.field private final c:I

.field private final d:Ljava/util/List;

.field private final e:Ljava/util/List;

.field private final f:Lcom/google/googlenav/ui/aa;

.field private final g:Lbf/aQ;

.field private final h:Z

.field private final i:Z

.field private final j:Lcom/google/googlenav/ai;

.field private final k:I

.field private l:Lcom/google/googlenav/ui/e;

.field private m:Lbj/aM;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 90
    sget-object v0, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    sput-object v0, Lbj/aE;->b:Lcom/google/googlenav/ui/aV;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZ)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 546
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lbj/aE;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZZ)V

    .line 548
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZLcom/google/googlenav/ai;ILcom/google/googlenav/ui/e;Z)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 569
    if-eqz p6, :cond_22

    const/4 v0, 0x1

    .line 570
    :goto_6
    new-instance v1, Lbj/aP;

    invoke-direct {v1, v0, p1, p2}, Lbj/aP;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v1, p0, Lbj/aE;->a:Lbj/aP;

    .line 571
    iput-object p3, p0, Lbj/aE;->d:Ljava/util/List;

    .line 572
    iput-object p4, p0, Lbj/aE;->e:Ljava/util/List;

    .line 573
    iput-object p5, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    .line 574
    iput-object p6, p0, Lbj/aE;->g:Lbf/aQ;

    .line 575
    iput p7, p0, Lbj/aE;->c:I

    .line 576
    iput-boolean p8, p0, Lbj/aE;->h:Z

    .line 577
    iput-object p9, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    .line 578
    iput p10, p0, Lbj/aE;->k:I

    .line 579
    iput-object p11, p0, Lbj/aE;->l:Lcom/google/googlenav/ui/e;

    .line 580
    iput-boolean p12, p0, Lbj/aE;->i:Z

    .line 581
    return-void

    .line 569
    :cond_22
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZZ)V
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 559
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v12, p9

    invoke-direct/range {v0 .. v12}, Lbj/aE;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZLcom/google/googlenav/ai;ILcom/google/googlenav/ui/e;Z)V

    .line 561
    return-void
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Landroid/util/Pair;
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 171
    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v3

    .line 173
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    .line 175
    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 176
    :cond_16
    const/4 v0, 0x0

    .line 202
    :goto_17
    return-object v0

    .line 182
    :cond_18
    const/16 v0, 0x28

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 185
    if-lez v0, :cond_6a

    .line 186
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    :goto_27
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 189
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v1, v0, :cond_40

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    if-nez v0, :cond_68

    .line 192
    :cond_40
    :try_start_40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 193
    const/16 v1, 0x2b9

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v1, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_5f
    .catch Ljava/lang/RuntimeException; {:try_start_40 .. :try_end_5f} :catch_65

    move-result-object v0

    .line 202
    :goto_60
    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_17

    .line 195
    :catch_65
    move-exception v0

    move-object v0, v2

    goto :goto_60

    :cond_68
    move v0, v1

    goto :goto_27

    :cond_6a
    move-object v0, v2

    goto :goto_60
.end method

.method private a(Landroid/view/View;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 796
    if-eqz p2, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 797
    return-void

    .line 796
    :cond_7
    const/16 v0, 0x8

    goto :goto_3
.end method

.method static synthetic a(Lbj/aE;)V
    .registers 1
    .parameter

    .prologue
    .line 67
    invoke-direct {p0}, Lbj/aE;->e()V

    return-void
.end method

.method static synthetic a(Lbj/aE;Lbj/aM;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lbj/aE;->c(Lbj/aM;)V

    return-void
.end method

.method static synthetic a(Lbj/aE;Lcom/google/googlenav/ui/ab;Landroid/widget/ImageView;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lbj/aE;->a(Lcom/google/googlenav/ui/ab;Landroid/widget/ImageView;)V

    return-void
.end method

.method private a(Lbj/aL;)V
    .registers 6
    .parameter

    .prologue
    .line 1087
    new-instance v1, Lbj/aN;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbj/aN;-><init>(Lbj/aF;)V

    .line 1088
    const v0, 0x7f040134

    iget-object v2, p1, Lbj/aL;->j:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/aN;->a:Landroid/view/ViewGroup;

    .line 1090
    iget-object v0, p1, Lbj/aL;->j:Landroid/view/ViewGroup;

    iget-object v2, v1, Lbj/aN;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1091
    iget-object v0, v1, Lbj/aN;->a:Landroid/view/ViewGroup;

    const v2, 0x7f1002a8

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/aN;->b:Landroid/widget/ImageView;

    .line 1093
    iget-object v0, p1, Lbj/aL;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1094
    return-void
.end method

.method private a(Lbj/aM;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1029
    invoke-direct {p0, p1, p2}, Lbj/aE;->b(Lbj/aM;I)Lbj/aL;

    move-result-object v0

    .line 1031
    iget-object v1, p1, Lbj/aM;->d:Landroid/view/ViewGroup;

    iget-object v2, v0, Lbj/aL;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1032
    iget-object v1, p1, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1033
    iget-object v0, p1, Lbj/aM;->i:Ljava/util/List;

    invoke-direct {p0, p2}, Lbj/aE;->a(I)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1034
    return-void
.end method

.method private a(Lcom/google/googlenav/ai;Landroid/widget/ImageView;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 1199
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ay()Lcom/google/googlenav/ap;

    move-result-object v0

    .line 1200
    invoke-static {v0}, Lbf/m;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/bs;

    move-result-object v1

    .line 1201
    if-nez v1, :cond_b

    .line 1221
    :goto_a
    return-void

    .line 1204
    :cond_b
    iget-object v0, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    check-cast v0, Lcom/google/googlenav/ui/br;

    .line 1205
    invoke-virtual {v0}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v2

    .line 1208
    invoke-virtual {v2, v1}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v3

    if-nez v3, :cond_22

    .line 1209
    new-instance v0, Lbj/aK;

    invoke-direct {v0, p0, v2, v1, p2}, Lbj/aK;-><init>(Lbj/aE;LaB/s;Lcom/google/googlenav/ui/bs;Landroid/widget/ImageView;)V

    .line 1216
    invoke-virtual {v2, v1, v0}, LaB/s;->a(Lcom/google/googlenav/ui/bs;LaB/p;)V

    goto :goto_a

    .line 1218
    :cond_22
    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    .line 1219
    invoke-static {p2, v0}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    goto :goto_a
.end method

.method private a(Lcom/google/googlenav/ui/ab;Landroid/widget/ImageView;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1159
    iget-object v0, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    if-nez v0, :cond_5

    .line 1166
    :cond_4
    :goto_4
    return-void

    .line 1162
    :cond_5
    iget-object v0, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/aa;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    .line 1163
    if-eqz v0, :cond_4

    if-eqz p2, :cond_4

    .line 1164
    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_4
.end method

.method private a(Lcom/google/googlenav/ui/e;Landroid/view/ViewGroup;[Lcom/google/googlenav/aw;I)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1189
    new-instance v0, Lbj/aJ;

    invoke-direct {v0, p0, p1, p4, p3}, Lbj/aJ;-><init>(Lbj/aE;Lcom/google/googlenav/ui/e;I[Lcom/google/googlenav/aw;)V

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1196
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/e;Lbj/aL;Lbj/aQ;Ljava/util/List;[Lcom/google/googlenav/aw;Z)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 717
    iget-object v0, p2, Lbj/aL;->b:Landroid/view/View;

    if-eqz v0, :cond_22

    .line 718
    invoke-static {p3}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    if-eqz v0, :cond_c5

    if-nez p6, :cond_c5

    move v0, v2

    .line 719
    :goto_f
    iget v1, p0, Lbj/aE;->k:I

    if-lez v1, :cond_c8

    iget-object v1, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v1, :cond_c8

    move v1, v2

    .line 720
    :goto_18
    if-nez v0, :cond_1c

    if-eqz v1, :cond_cb

    :cond_1c
    move v0, v2

    .line 721
    :goto_1d
    iget-object v1, p2, Lbj/aL;->b:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 725
    :cond_22
    iget-boolean v0, p0, Lbj/aE;->h:Z

    if-nez v0, :cond_26

    .line 731
    :cond_26
    iget-boolean v0, p0, Lbj/aE;->h:Z

    if-eqz v0, :cond_42

    .line 732
    invoke-static {}, Lcom/google/googlenav/bm;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, Lbj/aQ;->b:Ljava/lang/String;

    .line 733
    invoke-static {}, Lcom/google/googlenav/bm;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, Lbj/aQ;->c:Ljava/lang/String;

    .line 735
    iget-object v0, p3, Lbj/aQ;->c:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 737
    const-string v0, ""

    iput-object v0, p3, Lbj/aQ;->b:Ljava/lang/String;

    .line 741
    :cond_42
    iget-object v0, p2, Lbj/aL;->d:Landroid/widget/ImageView;

    invoke-direct {p0, p2, p3}, Lbj/aE;->a(Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 742
    iget-object v0, p2, Lbj/aL;->c:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, Lbj/aE;->b(Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 743
    iget-object v0, p2, Lbj/aL;->e:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, Lbj/aE;->c(Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 748
    iget-object v0, p2, Lbj/aL;->f:Landroid/widget/TextView;

    invoke-direct {p0, p1, p2, p3}, Lbj/aE;->a(Lcom/google/googlenav/ui/e;Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 751
    iget-object v0, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_96

    .line 752
    iget-object v0, p2, Lbj/aL;->k:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v2}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 753
    iget-object v0, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    iget-object v1, p2, Lbj/aL;->l:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Lcom/google/googlenav/ai;Landroid/widget/ImageView;)V

    .line 754
    iget-object v0, p2, Lbj/aL;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 755
    iget-object v0, p2, Lbj/aL;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 756
    iget-object v0, p2, Lbj/aL;->k:Landroid/view/ViewGroup;

    new-instance v1, Lbj/aG;

    invoke-direct {v1, p0, p1}, Lbj/aG;-><init>(Lbj/aE;Lcom/google/googlenav/ui/e;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 764
    :cond_96
    invoke-static {p3}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    if-eqz v0, :cond_ce

    .line 765
    iget-object v0, p2, Lbj/aL;->g:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, Lbj/aE;->d(Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 771
    :goto_a5
    if-eqz p4, :cond_126

    .line 772
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p2, Lbj/aL;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_e1

    .line 775
    iget-object v0, p2, Lbj/aL;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_b9
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_e1

    .line 776
    invoke-direct {p0, p2}, Lbj/aE;->a(Lbj/aL;)V

    .line 775
    add-int/lit8 v0, v0, 0x1

    goto :goto_b9

    :cond_c5
    move v0, v3

    .line 718
    goto/16 :goto_f

    :cond_c8
    move v1, v3

    .line 719
    goto/16 :goto_18

    :cond_cb
    move v0, v3

    .line 720
    goto/16 :goto_1d

    .line 767
    :cond_ce
    iget-object v0, p2, Lbj/aL;->h:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, Lbj/aE;->e(Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 768
    iget-object v0, p2, Lbj/aL;->i:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, Lbj/aE;->f(Lbj/aL;Lbj/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    goto :goto_a5

    :cond_e1
    move v4, v3

    .line 779
    :goto_e2
    iget-object v0, p2, Lbj/aL;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_11b

    .line 780
    iget-object v0, p2, Lbj/aL;->p:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/aN;

    .line 781
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-lt v4, v1, :cond_101

    .line 782
    iget-object v0, v0, Lbj/aN;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v3}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 779
    :goto_fd
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_e2

    .line 784
    :cond_101
    iget-object v1, v0, Lbj/aN;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v2}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 785
    invoke-interface {p4, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/bs;

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Lbj/aN;Lcom/google/googlenav/ui/bs;)Z

    move-result v1

    .line 786
    iget-object v5, v0, Lbj/aN;->b:Landroid/widget/ImageView;

    invoke-direct {p0, v5, v1}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 787
    iget-object v0, v0, Lbj/aN;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, p1, v0, p5, v4}, Lbj/aE;->a(Lcom/google/googlenav/ui/e;Landroid/view/ViewGroup;[Lcom/google/googlenav/aw;I)V

    goto :goto_fd

    .line 791
    :cond_11b
    iget-object v0, p2, Lbj/aL;->j:Landroid/view/ViewGroup;

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_127

    :goto_123
    invoke-direct {p0, v0, v2}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 793
    :cond_126
    return-void

    :cond_127
    move v2, v3

    .line 791
    goto :goto_123
.end method

.method private a(I)Z
    .registers 3
    .parameter

    .prologue
    .line 1127
    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    invoke-virtual {v0}, Lbj/aP;->a()I

    move-result v0

    if-le v0, p1, :cond_1a

    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/aQ;

    invoke-static {v0}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 955
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_9

    .line 956
    invoke-static {p1, p2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 958
    :cond_9
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method private a(Lbj/aL;Lbj/aQ;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 805
    iget-object v0, p2, Lbj/aQ;->b:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 806
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-static {p2}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    if-eqz v0, :cond_24

    const/4 v0, 0x2

    :goto_14
    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/ui/bi;->a(ZI)Lam/f;

    move-result-object v0

    .line 809
    iget-object v1, p1, Lbj/aL;->d:Landroid/widget/ImageView;

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 826
    :goto_23
    return v3

    .line 806
    :cond_24
    const/4 v0, 0x0

    goto :goto_14

    .line 811
    :cond_26
    new-instance v1, Lcom/google/googlenav/ui/bs;

    iget-object v0, p2, Lbj/aQ;->b:Ljava/lang/String;

    sget v2, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    .line 814
    iget-object v0, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    check-cast v0, Lcom/google/googlenav/ui/br;

    .line 815
    invoke-virtual {v0}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v2

    invoke-virtual {v2, v1}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v2

    if-nez v2, :cond_4a

    .line 816
    invoke-virtual {v0}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v0

    new-instance v2, Lbj/aH;

    invoke-direct {v2, p0, v1, p1}, Lbj/aH;-><init>(Lbj/aE;Lcom/google/googlenav/ui/bs;Lbj/aL;)V

    invoke-virtual {v0, v1, v2}, LaB/s;->a(Lcom/google/googlenav/ui/bs;LaB/p;)V

    goto :goto_23

    .line 823
    :cond_4a
    iget-object v0, p1, Lbj/aL;->d:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Lcom/google/googlenav/ui/ab;Landroid/widget/ImageView;)V

    goto :goto_23
.end method

.method private a(Lbj/aM;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 655
    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->b:Ljava/lang/String;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    invoke-virtual {v1}, Lbj/aP;->a()I

    move-result v1

    if-nez v1, :cond_10

    .line 661
    :cond_f
    :goto_f
    return v0

    .line 659
    :cond_10
    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 660
    iget-object v2, p1, Lbj/aM;->b:Landroid/widget/TextView;

    sget-object v3, Lcom/google/googlenav/ui/aV;->bY:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v1, v3}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    .line 661
    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->b:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_f

    const/4 v0, 0x1

    goto :goto_f
.end method

.method private a(Lbj/aN;Lcom/google/googlenav/ui/bs;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/16 v4, 0x30

    const/4 v0, 0x1

    .line 966
    iget-object v1, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    if-nez v1, :cond_9

    .line 967
    const/4 v0, 0x0

    .line 980
    :goto_8
    return v0

    .line 971
    :cond_9
    iget-object v1, p1, Lbj/aN;->b:Landroid/widget/ImageView;

    .line 972
    iget-object v2, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    invoke-interface {v2, p2}, Lcom/google/googlenav/ui/aa;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v2

    invoke-interface {v2}, Lam/f;->a()I

    move-result v2

    .line 973
    iget-object v3, p0, Lbj/aE;->f:Lcom/google/googlenav/ui/aa;

    invoke-interface {v3, p2}, Lcom/google/googlenav/ui/aa;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v3

    invoke-interface {v3}, Lam/f;->b()I

    move-result v3

    .line 974
    mul-int/lit8 v2, v2, 0x30

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    div-int/2addr v2, v3

    .line 975
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setMaxWidth(I)V

    .line 976
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setMaxHeight(I)V

    .line 977
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 979
    iget-object v1, p1, Lbj/aN;->b:Landroid/widget/ImageView;

    invoke-direct {p0, p2, v1}, Lbj/aE;->a(Lcom/google/googlenav/ui/ab;Landroid/widget/ImageView;)V

    goto :goto_8
.end method

.method private static a(Lbj/aQ;)Z
    .registers 2
    .parameter

    .prologue
    .line 1135
    iget-object v0, p0, Lbj/aQ;->e:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private a(Lcom/google/googlenav/ui/e;Lbj/aL;Lbj/aQ;)Z
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 876
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 878
    iget-object v0, p3, Lbj/aQ;->f:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_65

    move v0, v1

    .line 879
    :goto_10
    iget-object v3, p0, Lbj/aE;->g:Lbf/aQ;

    if-eqz v3, :cond_67

    move v3, v1

    .line 880
    :goto_15
    iget-object v4, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v4, :cond_69

    move v4, v1

    .line 881
    :goto_1a
    iget-object v5, p3, Lbj/aQ;->h:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    iget-object v7, p3, Lbj/aQ;->g:Ljava/lang/CharSequence;

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-le v5, v7, :cond_6b

    move v5, v1

    .line 884
    :goto_29
    iget-boolean v7, p0, Lbj/aE;->h:Z

    if-nez v7, :cond_33

    if-nez v4, :cond_6d

    if-nez v3, :cond_6d

    if-eqz v5, :cond_6d

    .line 885
    :cond_33
    iget-object v4, p3, Lbj/aQ;->h:Ljava/lang/CharSequence;

    .line 891
    :goto_35
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-lez v5, :cond_70

    move v5, v1

    .line 893
    :goto_3c
    if-eqz v0, :cond_43

    .line 894
    iget-object v7, p3, Lbj/aQ;->f:Ljava/lang/CharSequence;

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 897
    :cond_43
    iget-object v7, p3, Lbj/aQ;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 898
    new-instance v8, Lbj/aI;

    invoke-direct {v8, p0, p1, p2, v7}, Lbj/aI;-><init>(Lbj/aE;Lcom/google/googlenav/ui/e;Lbj/aL;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 920
    if-eqz v5, :cond_4f

    .line 921
    invoke-virtual {v6, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 925
    :cond_4f
    if-nez v3, :cond_55

    iget-object v3, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v3, :cond_5a

    .line 927
    :cond_55
    iget-object v3, p2, Lbj/aL;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3, v8}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 930
    :cond_5a
    iget-object v3, p2, Lbj/aL;->f:Landroid/widget/TextView;

    invoke-static {v3, v6}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 931
    if-nez v0, :cond_63

    if-eqz v5, :cond_64

    :cond_63
    move v2, v1

    :cond_64
    return v2

    :cond_65
    move v0, v2

    .line 878
    goto :goto_10

    :cond_67
    move v3, v2

    .line 879
    goto :goto_15

    :cond_69
    move v4, v2

    .line 880
    goto :goto_1a

    :cond_6b
    move v5, v2

    .line 881
    goto :goto_29

    .line 889
    :cond_6d
    iget-object v4, p3, Lbj/aQ;->g:Ljava/lang/CharSequence;

    goto :goto_35

    :cond_70
    move v5, v2

    .line 891
    goto :goto_3c
.end method

.method private a(Lcom/google/googlenav/ui/e;Lbj/aM;)Z
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 669
    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_d

    .line 705
    :goto_c
    return v4

    .line 672
    :cond_d
    iget-object v0, p2, Lbj/aM;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->d:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 673
    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p2, Lbj/aM;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_3c

    .line 676
    iget-object v0, p2, Lbj/aM;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_2c
    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3c

    .line 678
    invoke-direct {p0, p2}, Lbj/aE;->b(Lbj/aM;)V

    .line 677
    add-int/lit8 v0, v0, 0x1

    goto :goto_2c

    :cond_3c
    move v3, v4

    .line 681
    :goto_3d
    iget-object v0, p2, Lbj/aM;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_96

    .line 682
    iget-object v0, p2, Lbj/aM;->j:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/aO;

    .line 684
    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v3, v1, :cond_60

    .line 685
    iget-object v0, v0, Lbj/aO;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v4}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 681
    :goto_5c
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3d

    .line 687
    :cond_60
    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v1, v1, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 688
    iget-object v2, v0, Lbj/aO;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v2, v5}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 689
    iget-object v2, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v2, v2, Lbj/aP;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-eq v3, v2, :cond_80

    .line 690
    iget-object v2, v0, Lbj/aO;->b:Landroid/view/View;

    invoke-direct {p0, v2, v5}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 692
    :cond_80
    iget-object v6, v0, Lbj/aO;->c:Landroid/widget/TextView;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    sget-object v7, Lbj/aE;->b:Lcom/google/googlenav/ui/aV;

    invoke-static {v6, v2, v7}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    .line 695
    iget-object v0, v0, Lbj/aO;->a:Landroid/view/ViewGroup;

    .line 696
    new-instance v2, Lbj/aF;

    invoke-direct {v2, p0, p1, v1}, Lbj/aF;-><init>(Lbj/aE;Lcom/google/googlenav/ui/e;Landroid/util/Pair;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5c

    :cond_96
    move v4, v5

    .line 705
    goto/16 :goto_c
.end method

.method static synthetic b(Lbj/aE;)I
    .registers 2
    .parameter

    .prologue
    .line 67
    iget v0, p0, Lbj/aE;->k:I

    return v0
.end method

.method private b(Lbj/aM;I)Lbj/aL;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1038
    new-instance v2, Lbj/aL;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, Lbj/aL;-><init>(Lbj/aF;)V

    .line 1040
    invoke-direct {p0, p2}, Lbj/aE;->a(I)Z

    move-result v0

    if-eqz v0, :cond_be

    const v0, 0x7f040143

    :goto_10
    iget-object v3, p1, Lbj/aM;->d:Landroid/view/ViewGroup;

    invoke-static {v0, v3, v1}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    .line 1044
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10037a

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lbj/aL;->b:Landroid/view/View;

    .line 1046
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100348

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->c:Landroid/widget/TextView;

    .line 1048
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100347

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lbj/aL;->d:Landroid/widget/ImageView;

    .line 1050
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100349

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->e:Landroid/widget/TextView;

    .line 1053
    iget-object v0, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_84

    .line 1054
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10037b

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lbj/aL;->k:Landroid/view/ViewGroup;

    .line 1056
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10037c

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lbj/aL;->l:Landroid/widget/ImageView;

    .line 1058
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10037d

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->m:Landroid/widget/TextView;

    .line 1060
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10037e

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->n:Landroid/widget/TextView;

    .line 1064
    :cond_84
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10034a

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->f:Landroid/widget/TextView;

    .line 1066
    invoke-direct {p0, p2}, Lbj/aE;->a(I)Z

    move-result v0

    if-eqz v0, :cond_c3

    .line 1067
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10037f

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->g:Landroid/widget/TextView;

    .line 1075
    :goto_a4
    iput p2, v2, Lbj/aL;->o:I

    .line 1077
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10034e

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lbj/aL;->j:Landroid/view/ViewGroup;

    move v0, v1

    .line 1079
    :goto_b4
    const/16 v1, 0xa

    if-ge v0, v1, :cond_de

    .line 1080
    invoke-direct {p0, v2}, Lbj/aE;->a(Lbj/aL;)V

    .line 1079
    add-int/lit8 v0, v0, 0x1

    goto :goto_b4

    .line 1040
    :cond_be
    const v0, 0x7f040133

    goto/16 :goto_10

    .line 1070
    :cond_c3
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10034c

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->h:Landroid/widget/TextView;

    .line 1072
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10034d

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aL;->i:Landroid/widget/TextView;

    goto :goto_a4

    .line 1083
    :cond_de
    return-object v2
.end method

.method private b(Lbj/aM;)V
    .registers 6
    .parameter

    .prologue
    .line 1097
    new-instance v1, Lbj/aO;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbj/aO;-><init>(Lbj/aF;)V

    .line 1098
    const v0, 0x7f04013f

    iget-object v2, p1, Lbj/aM;->g:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/aO;->a:Landroid/view/ViewGroup;

    .line 1100
    iget-object v0, p1, Lbj/aM;->g:Landroid/view/ViewGroup;

    iget-object v2, v1, Lbj/aO;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1102
    iget-object v0, v1, Lbj/aO;->a:Landroid/view/ViewGroup;

    const v2, 0x7f10036a

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/aO;->b:Landroid/view/View;

    .line 1105
    iget-object v0, v1, Lbj/aO;->a:Landroid/view/ViewGroup;

    const v2, 0x7f100369

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/aO;->c:Landroid/widget/TextView;

    .line 1108
    iget-object v0, p1, Lbj/aM;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1109
    return-void
.end method

.method private b(Lbj/aL;Lbj/aQ;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 834
    iget-object v0, p2, Lbj/aQ;->c:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_20

    const/4 v0, 0x1

    move v1, v0

    .line 835
    :goto_a
    if-eqz v1, :cond_1f

    .line 837
    iget-boolean v0, p2, Lbj/aQ;->k:Z

    if-eqz v0, :cond_23

    sget-object v0, Lcom/google/googlenav/ui/aV;->bg:Lcom/google/googlenav/ui/aV;

    .line 839
    :goto_12
    iget-object v2, p1, Lbj/aL;->c:Landroid/widget/TextView;

    iget-object v3, p2, Lbj/aQ;->c:Ljava/lang/String;

    invoke-static {p2}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v4

    if-eqz v4, :cond_26

    :goto_1c
    invoke-static {v2, v3, v0}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    .line 842
    :cond_1f
    return v1

    .line 834
    :cond_20
    const/4 v0, 0x0

    move v1, v0

    goto :goto_a

    .line 837
    :cond_23
    sget-object v0, Lcom/google/googlenav/ui/aV;->bf:Lcom/google/googlenav/ui/aV;

    goto :goto_12

    .line 839
    :cond_26
    sget-object v0, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    goto :goto_1c
.end method

.method static synthetic b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 2
    .parameter

    .prologue
    .line 67
    invoke-static {p0}, Lbj/aE;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lbj/aE;)Lcom/google/googlenav/ai;
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lbj/aE;->j:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method private c(Lbj/aM;)V
    .registers 2
    .parameter

    .prologue
    .line 1173
    iput-object p1, p0, Lbj/aE;->m:Lbj/aM;

    .line 1174
    return-void
.end method

.method private c(Lbj/aL;Lbj/aQ;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 850
    iget-object v0, p2, Lbj/aQ;->d:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1c

    const/4 v0, 0x1

    move v1, v0

    .line 851
    :goto_a
    if-eqz v1, :cond_1b

    .line 852
    iget-object v2, p1, Lbj/aL;->e:Landroid/widget/TextView;

    iget-object v3, p2, Lbj/aQ;->d:Ljava/lang/String;

    invoke-static {p2}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    if-eqz v0, :cond_1f

    sget-object v0, Lcom/google/googlenav/ui/aV;->ba:Lcom/google/googlenav/ui/aV;

    :goto_18
    invoke-static {v2, v3, v0}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)V

    .line 855
    :cond_1b
    return v1

    .line 850
    :cond_1c
    const/4 v0, 0x0

    move v1, v0

    goto :goto_a

    .line 852
    :cond_1f
    sget-object v0, Lcom/google/googlenav/ui/aV;->aY:Lcom/google/googlenav/ui/aV;

    goto :goto_18
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 2
    .parameter

    .prologue
    .line 1131
    const/16 v0, 0x12

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lbj/aE;)Z
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-boolean v0, p0, Lbj/aE;->h:Z

    return v0
.end method

.method private d(Lbj/aL;Lbj/aQ;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 860
    iget-object v0, p2, Lbj/aQ;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_17

    const/4 v0, 0x1

    .line 861
    :goto_d
    if-eqz v0, :cond_16

    .line 862
    iget-object v1, p1, Lbj/aL;->g:Landroid/widget/TextView;

    iget-object v2, p2, Lbj/aQ;->e:Ljava/lang/CharSequence;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 864
    :cond_16
    return v0

    .line 860
    :cond_17
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private e()V
    .registers 2

    .prologue
    .line 1181
    const/4 v0, 0x0

    iput-object v0, p0, Lbj/aE;->m:Lbj/aM;

    .line 1182
    return-void
.end method

.method private e(Lbj/aL;Lbj/aQ;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 939
    iget-object v0, p1, Lbj/aL;->h:Landroid/widget/TextView;

    iget-object v1, p2, Lbj/aQ;->i:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private f(Lbj/aL;Lbj/aQ;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 947
    iget-object v0, p1, Lbj/aL;->i:Landroid/widget/TextView;

    iget-object v1, p2, Lbj/aQ;->j:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, Lbj/aE;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 1113
    iget v0, p0, Lbj/aE;->c:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 985
    iget-boolean v0, p0, Lbj/aE;->i:Z

    if-eqz v0, :cond_8

    .line 986
    invoke-static {p1}, Lbf/aS;->a(Landroid/view/View;)V

    .line 989
    :cond_8
    iget-object v0, p0, Lbj/aE;->g:Lbf/aQ;

    if-eqz v0, :cond_5e

    const/4 v0, 0x1

    .line 990
    :goto_d
    if-nez v0, :cond_1e

    .line 991
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 994
    :cond_1e
    new-instance v2, Lbj/aM;

    invoke-direct {v2}, Lbj/aM;-><init>()V

    .line 997
    const v0, 0x7f10025d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    .line 998
    iget-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10025e

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aM;->b:Landroid/widget/TextView;

    .line 1000
    iget-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100233

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lbj/aM;->c:Landroid/view/View;

    .line 1002
    iget-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10025f

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lbj/aM;->d:Landroid/view/ViewGroup;

    move v0, v1

    .line 1005
    :goto_54
    const/16 v3, 0x8

    if-ge v0, v3, :cond_60

    .line 1006
    invoke-direct {p0, v2, v0}, Lbj/aE;->a(Lbj/aM;I)V

    .line 1005
    add-int/lit8 v0, v0, 0x1

    goto :goto_54

    :cond_5e
    move v0, v1

    .line 989
    goto :goto_d

    .line 1009
    :cond_60
    iget-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100260

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lbj/aM;->e:Landroid/widget/TextView;

    .line 1011
    iget-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100234

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lbj/aM;->f:Landroid/view/View;

    .line 1013
    iget-object v0, v2, Lbj/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100261

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Lbj/aM;->g:Landroid/view/ViewGroup;

    move v0, v1

    .line 1016
    :goto_86
    const/4 v1, 0x5

    if-ge v0, v1, :cond_8f

    .line 1017
    invoke-direct {p0, v2}, Lbj/aE;->b(Lbj/aM;)V

    .line 1016
    add-int/lit8 v0, v0, 0x1

    goto :goto_86

    .line 1020
    :cond_8f
    return-object v2
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 585
    check-cast p2, Lbj/aM;

    .line 587
    if-eqz p1, :cond_9

    .line 588
    iput-object p1, p0, Lbj/aE;->l:Lcom/google/googlenav/ui/e;

    .line 592
    :cond_9
    invoke-virtual {p2, p0}, Lbj/aM;->a(Lbj/aE;)V

    .line 595
    invoke-direct {p0, p2}, Lbj/aE;->a(Lbj/aM;)Z

    move-result v0

    .line 596
    iget-object v1, p2, Lbj/aM;->b:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 597
    iget-object v1, p2, Lbj/aM;->c:Landroid/view/View;

    if-eqz v1, :cond_1e

    .line 598
    iget-object v1, p2, Lbj/aM;->c:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 602
    :cond_1e
    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    invoke-virtual {v0}, Lbj/aP;->a()I

    move-result v0

    iget-object v1, p2, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_40

    .line 605
    iget-object v0, p2, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_32
    iget-object v1, p0, Lbj/aE;->a:Lbj/aP;

    invoke-virtual {v1}, Lbj/aP;->a()I

    move-result v1

    if-ge v0, v1, :cond_40

    .line 606
    invoke-direct {p0, p2, v0}, Lbj/aE;->a(Lbj/aM;I)V

    .line 605
    add-int/lit8 v0, v0, 0x1

    goto :goto_32

    :cond_40
    move v1, v7

    .line 612
    :goto_41
    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_97

    .line 613
    iget-object v0, p2, Lbj/aM;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 614
    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/aQ;

    invoke-static {v0}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    .line 615
    if-eq v0, v2, :cond_93

    .line 616
    invoke-direct {p0, p2, v1}, Lbj/aE;->b(Lbj/aM;I)Lbj/aL;

    move-result-object v2

    .line 617
    iget-object v0, p2, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 618
    iget-object v3, p2, Lbj/aM;->i:Ljava/util/List;

    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbj/aQ;

    invoke-static {v0}, Lbj/aE;->a(Lbj/aQ;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v3, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 619
    iget-object v0, p2, Lbj/aM;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 620
    iget-object v0, p2, Lbj/aM;->d:Landroid/view/ViewGroup;

    iget-object v2, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 612
    :cond_93
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_41

    :cond_97
    move v8, v7

    .line 625
    :goto_98
    iget-object v0, p2, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v8, v0, :cond_102

    .line 626
    iget-object v0, p2, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbj/aL;

    .line 627
    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    invoke-virtual {v0}, Lbj/aP;->a()I

    move-result v0

    if-ge v8, v0, :cond_fc

    .line 628
    iget-object v0, p0, Lbj/aE;->d:Ljava/util/List;

    if-eqz v0, :cond_bc

    iget-object v0, p0, Lbj/aE;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e6

    :cond_bc
    move-object v4, v9

    .line 630
    :goto_bd
    iget-object v0, p0, Lbj/aE;->e:Ljava/util/List;

    if-eqz v0, :cond_c9

    iget-object v0, p0, Lbj/aE;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f0

    :cond_c9
    move-object v5, v9

    .line 632
    :goto_ca
    iget-object v0, p0, Lbj/aE;->a:Lbj/aP;

    iget-object v0, v0, Lbj/aP;->c:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbj/aQ;

    .line 633
    iget-object v1, p0, Lbj/aE;->l:Lcom/google/googlenav/ui/e;

    if-nez v8, :cond_fa

    move v6, v10

    :goto_d9
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbj/aE;->a(Lcom/google/googlenav/ui/e;Lbj/aL;Lbj/aQ;Ljava/util/List;[Lcom/google/googlenav/aw;Z)V

    .line 635
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v10}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 625
    :goto_e2
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_98

    .line 628
    :cond_e6
    iget-object v0, p0, Lbj/aE;->d:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move-object v4, v0

    goto :goto_bd

    .line 630
    :cond_f0
    iget-object v0, p0, Lbj/aE;->e:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/aw;

    move-object v5, v0

    goto :goto_ca

    :cond_fa
    move v6, v7

    .line 633
    goto :goto_d9

    .line 637
    :cond_fc
    iget-object v0, v2, Lbj/aL;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v7}, Lbj/aE;->a(Landroid/view/View;Z)V

    goto :goto_e2

    .line 642
    :cond_102
    iget-object v0, p0, Lbj/aE;->l:Lcom/google/googlenav/ui/e;

    invoke-direct {p0, v0, p2}, Lbj/aE;->a(Lcom/google/googlenav/ui/e;Lbj/aM;)Z

    move-result v0

    .line 644
    iget-object v1, p2, Lbj/aM;->e:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 645
    iget-object v1, p2, Lbj/aM;->f:Landroid/view/View;

    if-eqz v1, :cond_116

    .line 646
    iget-object v1, p2, Lbj/aM;->f:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 648
    :cond_116
    iget-object v1, p2, Lbj/aM;->g:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v0}, Lbj/aE;->a(Landroid/view/View;Z)V

    .line 649
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 1118
    const v0, 0x7f0400ba

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 1123
    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 1146
    iget-object v0, p0, Lbj/aE;->d:Ljava/util/List;

    if-eqz v0, :cond_4f

    iget-object v0, p0, Lbj/aE;->m:Lbj/aM;

    if-eqz v0, :cond_4f

    move v3, v4

    .line 1147
    :goto_a
    iget-object v0, p0, Lbj/aE;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_46

    .line 1148
    iget-object v0, p0, Lbj/aE;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move v5, v4

    .line 1149
    :goto_1b
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v5, v1, :cond_42

    .line 1150
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/ab;

    iget-object v2, p0, Lbj/aE;->m:Lbj/aM;

    iget-object v2, v2, Lbj/aM;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbj/aL;

    iget-object v2, v2, Lbj/aL;->p:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbj/aN;

    iget-object v2, v2, Lbj/aN;->b:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v2}, Lbj/aE;->a(Lcom/google/googlenav/ui/ab;Landroid/widget/ImageView;)V

    .line 1149
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1b

    .line 1147
    :cond_42
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_a

    .line 1154
    :cond_46
    iget-object v0, p0, Lbj/aE;->l:Lcom/google/googlenav/ui/e;

    const/16 v1, 0x18

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    .line 1156
    :cond_4f
    return-void
.end method
