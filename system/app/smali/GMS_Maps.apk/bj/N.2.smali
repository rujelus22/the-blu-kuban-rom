.class public Lbj/N;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:I

.field private final b:Lcom/google/googlenav/ui/br;

.field private final c:Lcom/google/googlenav/ai;

.field private final d:Lcom/google/googlenav/ui/view/c;


# direct methods
.method public constructor <init>(ILcom/google/googlenav/ui/br;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/view/c;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput p1, p0, Lbj/N;->a:I

    .line 64
    iput-object p2, p0, Lbj/N;->b:Lcom/google/googlenav/ui/br;

    .line 65
    iput-object p3, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    .line 66
    iput-object p4, p0, Lbj/N;->d:Lcom/google/googlenav/ui/view/c;

    .line 69
    invoke-virtual {p0}, Lbj/N;->d()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 70
    const/16 v0, 0x2d

    invoke-virtual {p3, v0}, Lcom/google/googlenav/ai;->o(I)V

    .line 73
    :cond_16
    return-void
.end method

.method private a(Lbj/O;)V
    .registers 8
    .parameter

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 159
    iget-object v0, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ay()Lcom/google/googlenav/ap;

    move-result-object v4

    .line 160
    if-eqz v4, :cond_53

    invoke-virtual {v4}, Lcom/google/googlenav/ap;->b()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_53

    move v0, v1

    .line 162
    :goto_14
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->I()Z

    move-result v3

    if-eqz v3, :cond_55

    if-eqz v4, :cond_22

    if-eqz v0, :cond_55

    :cond_22
    move v3, v1

    .line 166
    :goto_23
    if-eqz v3, :cond_8e

    .line 167
    iget-object v0, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->at()Lcom/google/googlenav/ay;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_57

    .line 169
    invoke-virtual {v0}, Lcom/google/googlenav/ay;->a()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    .line 170
    iget-object v1, p1, Lbj/O;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lbm/a;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 172
    iget-object v0, p1, Lbj/O;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 173
    iget-object v0, p1, Lbj/O;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 174
    iget-object v0, p1, Lbj/O;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lbj/N;->d:Lcom/google/googlenav/ui/view/c;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/android/aF;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aF;

    .line 207
    :cond_52
    :goto_52
    return-void

    :cond_53
    move v0, v2

    .line 160
    goto :goto_14

    :cond_55
    move v3, v2

    .line 162
    goto :goto_23

    .line 179
    :cond_57
    iget-object v0, p1, Lbj/O;->a:Landroid/view/ViewGroup;

    const v1, 0x7f1002a0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 180
    const/16 v1, 0x3c8

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    new-instance v0, Lcom/google/googlenav/ui/view/a;

    const/16 v1, 0x909

    const/4 v3, -0x1

    const-string v4, "lp"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    .line 183
    iget-object v1, p1, Lbj/O;->a:Landroid/view/ViewGroup;

    iget-object v3, p0, Lbj/N;->d:Lcom/google/googlenav/ui/view/c;

    invoke-static {v1, v3, v0}, Lcom/google/googlenav/ui/android/aF;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aF;

    .line 184
    iget-object v0, p1, Lbj/O;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 185
    iget-object v0, p1, Lbj/O;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    goto :goto_52

    .line 191
    :cond_8e
    if-eqz v4, :cond_52

    .line 193
    invoke-static {v4}, Lbf/m;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/bs;

    move-result-object v2

    .line 194
    iget-object v3, p0, Lbj/N;->b:Lcom/google/googlenav/ui/br;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v3

    invoke-virtual {v3, v2}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v3

    if-eqz v3, :cond_52

    .line 195
    iget-object v3, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-static {v3, v0, v1}, Lbf/aS;->b(Lcom/google/googlenav/ai;ZZ)Lcom/google/googlenav/ui/view/a;

    move-result-object v1

    .line 197
    iget-object v3, p1, Lbj/O;->b:Landroid/widget/ImageView;

    iget-object v4, p0, Lbj/N;->d:Lcom/google/googlenav/ui/view/c;

    invoke-static {v3, v4, v1}, Lcom/google/googlenav/ui/android/aF;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aF;

    .line 198
    iget-object v1, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-static {v1, v0}, Lbf/aS;->b(Lcom/google/googlenav/ai;Z)V

    .line 199
    iget-object v0, p0, Lbj/N;->b:Lcom/google/googlenav/ui/br;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    .line 200
    iget-object v1, p1, Lbj/O;->b:Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    goto :goto_52
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 87
    iget v0, p0, Lbj/N;->a:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .registers 4
    .parameter

    .prologue
    .line 92
    new-instance v1, Lbj/O;

    invoke-direct {v1, p0}, Lbj/O;-><init>(Lbj/N;)V

    .line 93
    const v0, 0x7f1002a7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lbj/O;->a:Landroid/view/ViewGroup;

    .line 94
    const v0, 0x7f1002a8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/O;->b:Landroid/widget/ImageView;

    .line 95
    const v0, 0x7f10019b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/DistanceView;

    iput-object v0, v1, Lbj/O;->c:Lcom/google/googlenav/ui/view/android/DistanceView;

    .line 96
    const v0, 0x7f10019c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/HeadingView;

    iput-object v0, v1, Lbj/O;->d:Lcom/google/googlenav/ui/view/android/HeadingView;

    .line 97
    const v0, 0x7f10001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/O;->e:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f10019f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/O;->f:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f1001a0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/O;->g:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f1002a9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/O;->h:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f10022f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v1, Lbj/O;->i:Landroid/widget/Button;

    .line 102
    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 107
    check-cast p2, Lbj/O;

    .line 110
    invoke-direct {p0, p2}, Lbj/N;->a(Lbj/O;)V

    .line 113
    iget-object v0, p2, Lbj/O;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 116
    iget-object v0, p2, Lbj/O;->c:Lcom/google/googlenav/ui/view/android/DistanceView;

    iget-object v2, p2, Lbj/O;->d:Lcom/google/googlenav/ui/view/android/HeadingView;

    iget-object v3, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v3

    invoke-static {v0, v2, v3}, LaV/g;->a(Lcom/google/googlenav/ui/view/android/DistanceView;Lcom/google/googlenav/ui/view/android/HeadingView;LaN/B;)V

    .line 119
    iget-object v0, p2, Lbj/O;->f:Landroid/widget/TextView;

    iget-object v2, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p2, Lbj/O;->g:Landroid/widget/TextView;

    iget-object v2, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 123
    iget-object v0, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bz()Z

    move-result v0

    if-eqz v0, :cond_80

    const/16 v0, 0x3bf

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->br:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 125
    :goto_48
    iget-object v2, p2, Lbj/O;->h:Landroid/widget/TextView;

    invoke-static {v2, v0}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 127
    invoke-virtual {p0}, Lbj/N;->d()Z

    move-result v0

    if-eqz v0, :cond_85

    .line 128
    iget-object v0, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->b()Z

    move-result v0

    if-nez v0, :cond_82

    const/16 v0, 0x420

    .line 130
    :goto_61
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 132
    iget-object v2, p2, Lbj/O;->i:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p2, Lbj/O;->i:Landroid/widget/Button;

    iget-object v2, p0, Lbj/N;->d:Lcom/google/googlenav/ui/view/c;

    new-instance v3, Lcom/google/googlenav/ui/view/a;

    const/16 v4, 0x6a4

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5, v1}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-static {v0, v2, v3}, Lcom/google/googlenav/ui/android/aF;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aF;

    .line 140
    :goto_7f
    return-void

    :cond_80
    move-object v0, v1

    .line 123
    goto :goto_48

    .line 128
    :cond_82
    const/16 v0, 0x3f6

    goto :goto_61

    .line 138
    :cond_85
    iget-object v0, p2, Lbj/O;->i:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_7f
.end method

.method public b()I
    .registers 2

    .prologue
    .line 77
    const v0, 0x7f0400e3

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .registers 5

    .prologue
    .line 210
    iget-object v0, p0, Lbj/N;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method
