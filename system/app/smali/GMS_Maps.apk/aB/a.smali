.class public LaB/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:LaB/m;

.field private static final i:Ljava/lang/Object;


# instance fields
.field protected final a:Ljava/util/Hashtable;

.field private final b:Ljava/util/concurrent/ConcurrentMap;

.field private c:Ljava/util/Vector;

.field private volatile d:Z

.field private final f:Lcom/google/googlenav/android/aa;

.field private final g:Las/c;

.field private final h:Ljava/util/Hashtable;

.field private final j:Ljava/util/Vector;

.field private final k:LaB/q;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 85
    new-instance v0, LaB/m;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v6}, LaB/m;-><init>(Lam/f;Lam/f;Lam/f;Lam/f;Ljava/lang/String;Ljava/lang/Long;)V

    sput-object v0, LaB/a;->e:LaB/m;

    .line 97
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LaB/a;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/android/aa;Las/c;LaB/q;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaB/a;->a:Ljava/util/Hashtable;

    .line 73
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaB/a;->c:Ljava/util/Vector;

    .line 94
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaB/a;->h:Ljava/util/Hashtable;

    .line 135
    iput-object p1, p0, LaB/a;->f:Lcom/google/googlenav/android/aa;

    .line 136
    iput-object p2, p0, LaB/a;->g:Las/c;

    .line 137
    iput-object p3, p0, LaB/a;->k:LaB/q;

    .line 138
    new-instance v0, Lcom/google/common/collect/bE;

    invoke-direct {v0}, Lcom/google/common/collect/bE;-><init>()V

    invoke-virtual {v0}, Lcom/google/common/collect/bE;->k()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 140
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaB/a;->j:Ljava/util/Vector;

    .line 141
    invoke-direct {p0}, LaB/a;->e()V

    .line 142
    return-void
.end method

.method static synthetic a(LaB/a;)V
    .registers 1
    .parameter

    .prologue
    .line 56
    invoke-direct {p0}, LaB/a;->c()V

    return-void
.end method

.method static synthetic a(LaB/a;LaB/p;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0, p1}, LaB/a;->c(LaB/p;)V

    return-void
.end method

.method static synthetic a(LaB/a;Ljava/lang/Long;LaB/m;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, LaB/a;->a(Ljava/lang/Long;LaB/m;)V

    return-void
.end method

.method static synthetic a(LaB/a;Ljava/util/Vector;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0, p1}, LaB/a;->b(Ljava/util/Vector;)V

    return-void
.end method

.method private a(Ljava/lang/Long;LaB/m;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 365
    invoke-virtual {p2}, LaB/m;->h()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 366
    iget-object v0, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, p2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    :cond_b
    :goto_b
    iget-object v0, p0, LaB/a;->h:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    return-void

    .line 367
    :cond_11
    iget-object v0, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 369
    iget-object v0, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    sget-object v1, LaB/a;->e:LaB/m;

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_b
.end method

.method private a(Ljava/util/Vector;)V
    .registers 5
    .parameter

    .prologue
    .line 345
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    move v1, v0

    :goto_6
    if-ge v1, v2, :cond_15

    .line 346
    invoke-virtual {p1, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaB/p;

    invoke-interface {v0}, LaB/p;->Q_()V

    .line 345
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 348
    :cond_15
    return-void
.end method

.method static synthetic a(LaB/a;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 56
    iput-boolean p1, p0, LaB/a;->d:Z

    return p1
.end method

.method private static a(LaB/m;Ljava/lang/String;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 261
    if-eqz p0, :cond_e

    invoke-virtual {p0}, LaB/m;->b()LaB/n;

    move-result-object v2

    invoke-virtual {v2}, LaB/n;->a()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_12

    .line 262
    :cond_e
    if-nez p1, :cond_11

    move v0, v1

    .line 284
    :cond_11
    :goto_11
    return v0

    .line 272
    :cond_12
    if-eqz p1, :cond_11

    .line 277
    invoke-virtual {p0}, LaB/m;->b()LaB/n;

    move-result-object v2

    invoke-virtual {v2}, LaB/n;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    move v0, v1

    .line 280
    goto :goto_11
.end method

.method static synthetic b()LaB/m;
    .registers 1

    .prologue
    .line 56
    sget-object v0, LaB/a;->e:LaB/m;

    return-object v0
.end method

.method static synthetic b(LaB/a;)Las/c;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, LaB/a;->g:Las/c;

    return-object v0
.end method

.method static synthetic b(LaB/a;Ljava/util/Vector;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0, p1}, LaB/a;->a(Ljava/util/Vector;)V

    return-void
.end method

.method private b(Ljava/util/Vector;)V
    .registers 6
    .parameter

    .prologue
    .line 351
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    move v1, v0

    :goto_6
    if-ge v1, v2, :cond_1b

    .line 352
    invoke-virtual {p1, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaB/m;

    .line 357
    if-eqz v0, :cond_17

    .line 358
    invoke-virtual {v0}, LaB/m;->a()Ljava/lang/Long;

    move-result-object v3

    invoke-direct {p0, v3, v0}, LaB/a;->a(Ljava/lang/Long;LaB/m;)V

    .line 351
    :cond_17
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 361
    :cond_1b
    return-void
.end method

.method private b(Ljava/util/Vector;LaB/p;)Z
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 179
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 180
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 182
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v6

    move v3, v2

    :goto_10
    if-ge v3, v6, :cond_6f

    .line 183
    invoke-virtual {p1, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaB/n;

    .line 186
    iget-object v1, p0, LaB/a;->h:Ljava/util/Hashtable;

    invoke-virtual {v0}, LaB/n;->b()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4e

    .line 189
    iget-object v1, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {v0}, LaB/n;->b()Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaB/m;

    .line 190
    if-nez v1, :cond_56

    .line 193
    iget-object v1, p0, LaB/a;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, LaB/n;->b()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaB/n;

    .line 194
    if-eqz v1, :cond_52

    .line 197
    invoke-virtual {v5, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 205
    :goto_43
    iget-object v1, p0, LaB/a;->h:Ljava/util/Hashtable;

    invoke-virtual {v0}, LaB/n;->b()Ljava/lang/Long;

    move-result-object v0

    sget-object v7, LaB/a;->i:Ljava/lang/Object;

    invoke-virtual {v1, v0, v7}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    :cond_4e
    :goto_4e
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_10

    .line 201
    :cond_52
    invoke-virtual {v4, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_43

    .line 207
    :cond_56
    invoke-virtual {v0}, LaB/n;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, LaB/a;->a(LaB/m;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4e

    .line 211
    invoke-virtual {v4, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 214
    iget-object v1, p0, LaB/a;->h:Ljava/util/Hashtable;

    invoke-virtual {v0}, LaB/n;->b()Ljava/lang/Long;

    move-result-object v0

    sget-object v7, LaB/a;->i:Ljava/lang/Object;

    invoke-virtual {v1, v0, v7}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4e

    .line 219
    :cond_6f
    invoke-virtual {v5}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7b

    invoke-virtual {v4}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a2

    :cond_7b
    const/4 v0, 0x1

    .line 221
    :goto_7c
    invoke-virtual {v5}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_91

    .line 222
    new-instance v1, LaB/h;

    iget-object v2, p0, LaB/a;->g:Las/c;

    new-instance v3, LaB/b;

    invoke-direct {v3, p0, p2}, LaB/b;-><init>(LaB/a;LaB/p;)V

    invoke-direct {v1, v2, v5, p0, v3}, LaB/h;-><init>(Las/c;Ljava/util/Vector;LaB/a;LaB/i;)V

    invoke-virtual {v1}, LaB/h;->g()V

    .line 242
    :cond_91
    invoke-virtual {v4}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a1

    .line 243
    new-instance v1, LaB/c;

    invoke-direct {v1, p0, p2}, LaB/c;-><init>(LaB/a;LaB/p;)V

    .line 253
    iget-object v2, p0, LaB/a;->k:LaB/q;

    invoke-interface {v2, v4, v1}, LaB/q;->a(Ljava/util/Vector;Lcom/google/googlenav/friend/aA;)V

    .line 256
    :cond_a1
    return v0

    :cond_a2
    move v0, v2

    .line 219
    goto :goto_7c
.end method

.method static synthetic c(LaB/a;)Ljava/util/Vector;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, LaB/a;->j:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic c(LaB/a;Ljava/util/Vector;)Ljava/util/Vector;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 56
    iput-object p1, p0, LaB/a;->c:Ljava/util/Vector;

    return-object p1
.end method

.method private c()V
    .registers 4

    .prologue
    .line 323
    iget-object v0, p0, LaB/a;->f:Lcom/google/googlenav/android/aa;

    if-eqz v0, :cond_10

    .line 324
    const/4 v0, 0x1

    .line 326
    iget-object v1, p0, LaB/a;->f:Lcom/google/googlenav/android/aa;

    new-instance v2, LaB/e;

    invoke-direct {v2, p0}, LaB/e;-><init>(LaB/a;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 335
    :goto_f
    return-void

    .line 333
    :cond_10
    iget-object v0, p0, LaB/a;->j:Ljava/util/Vector;

    invoke-direct {p0, v0}, LaB/a;->a(Ljava/util/Vector;)V

    goto :goto_f
.end method

.method private c(LaB/p;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 298
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0, v3}, Ljava/util/Vector;-><init>(I)V

    .line 299
    if-eqz p1, :cond_1a

    iget-object v1, p0, LaB/a;->f:Lcom/google/googlenav/android/aa;

    if-eqz v1, :cond_1a

    .line 301
    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 303
    iget-object v1, p0, LaB/a;->f:Lcom/google/googlenav/android/aa;

    new-instance v2, LaB/d;

    invoke-direct {v2, p0, v0}, LaB/d;-><init>(LaB/a;Ljava/util/Vector;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 314
    :cond_19
    :goto_19
    return-void

    .line 309
    :cond_1a
    if-eqz p1, :cond_19

    .line 311
    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 312
    invoke-direct {p0, v0}, LaB/a;->a(Ljava/util/Vector;)V

    goto :goto_19
.end method

.method private d()V
    .registers 5

    .prologue
    .line 528
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 529
    iget-object v1, p0, LaB/a;->a:Ljava/util/Hashtable;

    monitor-enter v1

    .line 530
    :try_start_8
    iget-object v2, p0, LaB/a;->a:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v2

    .line 531
    :goto_e
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 532
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_e

    .line 534
    :catchall_1c
    move-exception v0

    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_1c

    throw v0

    :cond_1f
    :try_start_1f
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_1c

    .line 536
    new-instance v1, LaB/f;

    iget-object v2, p0, LaB/a;->g:Las/c;

    invoke-direct {v1, p0, v2, v0}, LaB/f;-><init>(LaB/a;Las/c;Ljava/util/Vector;)V

    invoke-virtual {v1}, LaB/f;->g()V

    .line 556
    return-void
.end method

.method static synthetic d(LaB/a;)V
    .registers 1
    .parameter

    .prologue
    .line 56
    invoke-direct {p0}, LaB/a;->d()V

    return-void
.end method

.method private e()V
    .registers 3

    .prologue
    .line 634
    new-instance v0, LaB/g;

    iget-object v1, p0, LaB/a;->g:Las/c;

    invoke-direct {v0, p0, v1}, LaB/g;-><init>(LaB/a;Las/c;)V

    invoke-virtual {v0}, LaB/g;->g()V

    .line 674
    return-void
.end method

.method static synthetic e(LaB/a;)V
    .registers 1
    .parameter

    .prologue
    .line 56
    invoke-direct {p0}, LaB/a;->f()V

    return-void
.end method

.method static synthetic f(LaB/a;)Ljava/util/Vector;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, LaB/a;->c:Ljava/util/Vector;

    return-object v0
.end method

.method private f()V
    .registers 3

    .prologue
    .line 681
    iget-object v0, p0, LaB/a;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 684
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 685
    const-string v1, "PROTO_CLIENT_SAVED_PHOTO_CACHE"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 686
    const-string v1, "PHOTO_"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->c(Ljava/lang/String;)V

    .line 687
    return-void
.end method


# virtual methods
.method public a()I
    .registers 5

    .prologue
    .line 376
    const/4 v0, 0x0

    .line 377
    iget-object v1, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaB/m;

    .line 378
    sget-object v3, LaB/a;->e:LaB/m;

    if-eq v0, v3, :cond_24

    .line 379
    invoke-virtual {v0}, LaB/m;->i()I

    move-result v0

    add-int/2addr v0, v1

    :goto_21
    move v1, v0

    goto :goto_c

    .line 382
    :cond_23
    return v1

    :cond_24
    move v0, v1

    goto :goto_21
.end method

.method public a(Ljava/lang/Long;)LaB/m;
    .registers 3
    .parameter

    .prologue
    .line 614
    iget-object v0, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaB/m;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/ui/friend/E;)Lam/f;
    .registers 4
    .parameter

    .prologue
    .line 587
    invoke-virtual {p1}, Lcom/google/googlenav/ui/friend/E;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, LaB/a;->a(Ljava/lang/Long;)LaB/m;

    move-result-object v0

    .line 588
    if-eqz v0, :cond_15

    sget-object v1, LaB/a;->e:LaB/m;

    if-eq v0, v1, :cond_15

    .line 589
    invoke-virtual {p1}, Lcom/google/googlenav/ui/friend/E;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_2c

    .line 605
    :cond_15
    const/4 v0, 0x0

    :goto_16
    return-object v0

    .line 591
    :pswitch_17
    invoke-virtual {v0}, LaB/m;->f()Lam/f;

    move-result-object v0

    goto :goto_16

    .line 593
    :pswitch_1c
    invoke-virtual {v0}, LaB/m;->e()Lam/f;

    move-result-object v0

    goto :goto_16

    .line 595
    :pswitch_21
    invoke-virtual {v0}, LaB/m;->d()Lam/f;

    move-result-object v0

    goto :goto_16

    .line 597
    :pswitch_26
    invoke-virtual {v0}, LaB/m;->c()Lam/f;

    move-result-object v0

    goto :goto_16

    .line 589
    nop

    :pswitch_data_2c
    .packed-switch 0x0
        :pswitch_26
        :pswitch_21
        :pswitch_1c
        :pswitch_17
    .end packed-switch
.end method

.method public a(LaB/p;)V
    .registers 3
    .parameter

    .prologue
    .line 515
    iget-object v0, p0, LaB/a;->j:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 516
    return-void
.end method

.method public a(Ljava/util/Vector;LaB/p;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 159
    iget-boolean v1, p0, LaB/a;->d:Z

    if-nez v1, :cond_b

    .line 160
    iget-object v1, p0, LaB/a;->c:Ljava/util/Vector;

    invoke-static {p1, v0, v1}, Lcom/google/googlenav/common/util/a;->a(Ljava/util/List;ILjava/util/List;)V

    .line 163
    :goto_a
    return v0

    :cond_b
    invoke-direct {p0, p1, p2}, LaB/a;->b(Ljava/util/Vector;LaB/p;)Z

    move-result v0

    goto :goto_a
.end method

.method public b(LaB/p;)V
    .registers 3
    .parameter

    .prologue
    .line 519
    iget-object v0, p0, LaB/a;->j:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 520
    return-void
.end method
