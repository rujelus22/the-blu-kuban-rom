.class public final Laa/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Laa/e;Laa/f;)J
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 115
    .line 116
    invoke-interface {p0}, Laa/e;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 118
    :try_start_8
    invoke-interface {p1}, Laa/f;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/OutputStream;
    :try_end_e
    .catchall {:try_start_8 .. :try_end_e} :catchall_2d

    .line 120
    :try_start_e
    invoke-static {v0, v1}, Laa/a;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_11
    .catchall {:try_start_e .. :try_end_11} :catchall_1a

    move-result-wide v5

    move v4, v3

    .line 124
    :try_start_13
    invoke-static {v1, v4}, Laa/b;->a(Ljava/io/Closeable;Z)V
    :try_end_16
    .catchall {:try_start_13 .. :try_end_16} :catchall_20

    .line 128
    invoke-static {v0, v3}, Laa/b;->a(Ljava/io/Closeable;Z)V

    return-wide v5

    .line 124
    :catchall_1a
    move-exception v5

    move v4, v2

    :try_start_1c
    invoke-static {v1, v4}, Laa/b;->a(Ljava/io/Closeable;Z)V
    :try_end_1f
    .catchall {:try_start_1c .. :try_end_1f} :catchall_2d

    .line 125
    :try_start_1f
    throw v5
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_20

    .line 128
    :catchall_20
    move-exception v1

    move-object v4, v1

    move v1, v2

    :goto_23
    const/4 v5, 0x2

    if-ge v1, v5, :cond_2b

    move v1, v2

    :goto_27
    invoke-static {v0, v1}, Laa/b;->a(Ljava/io/Closeable;Z)V

    throw v4

    :cond_2b
    move v1, v3

    goto :goto_27

    :catchall_2d
    move-exception v1

    move-object v4, v1

    move v1, v3

    goto :goto_23
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 190
    const/16 v0, 0x1000

    new-array v2, v0, [B

    .line 191
    const-wide/16 v0, 0x0

    .line 193
    :goto_6
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 194
    const/4 v4, -0x1

    if-ne v3, v4, :cond_e

    .line 200
    return-wide v0

    .line 197
    :cond_e
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 198
    int-to-long v3, v3

    add-long/2addr v0, v3

    .line 199
    goto :goto_6
.end method
