.class LaD/C;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:LaD/A;

.field private b:Ljava/util/Set;


# direct methods
.method private constructor <init>(LaD/A;)V
    .registers 2
    .parameter

    .prologue
    .line 364
    iput-object p1, p0, LaD/C;->a:LaD/A;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LaD/A;LaD/B;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 364
    invoke-direct {p0, p1}, LaD/C;-><init>(LaD/A;)V

    return-void
.end method

.method private a(LaD/E;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 390
    invoke-virtual {p1}, LaD/E;->a()Lay/C;

    move-result-object v1

    if-eqz v1, :cond_1b

    invoke-virtual {p1}, LaD/E;->a()Lay/C;

    move-result-object v1

    invoke-virtual {v1}, Lay/C;->l()Z

    move-result v1

    if-nez v1, :cond_1b

    invoke-virtual {p1}, LaD/E;->a()Lay/C;

    move-result-object v1

    invoke-virtual {v1}, Lay/C;->m()Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 398
    :cond_1b
    :goto_1b
    return v0

    .line 395
    :cond_1c
    invoke-virtual {p1}, LaD/E;->b()Lay/G;

    move-result-object v1

    if-eqz v1, :cond_2c

    invoke-virtual {p1}, LaD/E;->b()Lay/G;

    move-result-object v1

    invoke-virtual {v1}, Lay/G;->c()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 396
    :cond_2c
    const/4 v0, 0x1

    goto :goto_1b
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .registers 6
    .parameter

    .prologue
    .line 370
    iget-object v0, p0, LaD/C;->a:LaD/A;

    invoke-static {v0}, LaD/A;->c(LaD/A;)Ljava/util/List;

    move-result-object v0

    .line 371
    invoke-static {}, Lcom/google/common/collect/dm;->a()Ljava/util/HashSet;

    move-result-object v1

    iput-object v1, p0, LaD/C;->b:Ljava/util/Set;

    .line 372
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_10
    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/E;

    .line 373
    iget-object v2, p0, LaD/C;->b:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_36

    .line 380
    :cond_25
    iget-object v0, p0, LaD/C;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_34

    .line 381
    iget-object v0, p0, LaD/C;->a:LaD/A;

    iget-object v1, p0, LaD/C;->b:Ljava/util/Set;

    invoke-static {v0, v1}, LaD/A;->a(LaD/A;Ljava/util/Set;)V

    .line 383
    :cond_34
    const/4 v0, 0x0

    return-object v0

    .line 376
    :cond_36
    invoke-direct {p0, v0}, LaD/C;->a(LaD/E;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 377
    iget-object v2, p0, LaD/C;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_10
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 364
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LaD/C;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
