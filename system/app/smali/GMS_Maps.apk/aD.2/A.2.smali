.class public LaD/A;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lay/aa;
.implements Lay/n;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/ViewGroup;

.field private c:Ljava/util/Set;

.field private d:Lay/m;

.field private e:Lay/m;

.field private f:Ljava/util/List;

.field private final g:Lcom/google/googlenav/ui/wizard/gh;

.field private h:Lcom/google/googlenav/aV;


# direct methods
.method static synthetic a(LaD/A;)Lay/m;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LaD/A;->e:Lay/m;

    return-object v0
.end method

.method private a(I)V
    .registers 5
    .parameter

    .prologue
    .line 251
    iget-object v1, p0, LaD/A;->f:Ljava/util/List;

    monitor-enter v1

    .line 252
    :try_start_3
    iget-object v0, p0, LaD/A;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_23

    .line 253
    iget-object v0, p0, LaD/A;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/D;

    .line 254
    iget-object v2, p0, LaD/A;->h:Lcom/google/googlenav/aV;

    iget-object v0, v0, LaD/D;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/aV;->a(Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, LaD/A;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 257
    :cond_23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_2c

    .line 258
    invoke-direct {p0}, LaD/A;->c()V

    .line 259
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaD/A;->a(Z)V

    .line 260
    return-void

    .line 257
    :catchall_2c
    move-exception v0

    :try_start_2d
    monitor-exit v1
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_2c

    throw v0
.end method

.method static synthetic a(LaD/A;Ljava/util/Set;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1}, LaD/A;->a(Ljava/util/Set;)V

    return-void
.end method

.method private a(Ljava/util/Set;)V
    .registers 7
    .parameter

    .prologue
    .line 403
    iget-object v1, p0, LaD/A;->c:Ljava/util/Set;

    monitor-enter v1

    .line 404
    :try_start_3
    iget-object v0, p0, LaD/A;->d:Lay/m;

    invoke-interface {v0, p0}, Lay/m;->a(Lay/n;)V

    .line 405
    iget-object v0, p0, LaD/A;->e:Lay/m;

    invoke-interface {v0, p0}, Lay/m;->a(Lay/n;)V

    .line 406
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/E;

    .line 407
    iget-object v3, p0, LaD/A;->c:Ljava/util/Set;

    iget-object v4, v0, LaD/E;->a:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 408
    iget-boolean v3, v0, LaD/E;->b:Z

    if-eqz v3, :cond_33

    .line 409
    iget-object v3, p0, LaD/A;->d:Lay/m;

    iget-object v0, v0, LaD/E;->a:Ljava/lang/String;

    invoke-interface {v3, v0}, Lay/m;->a(Ljava/lang/String;)V

    goto :goto_11

    .line 414
    :catchall_30
    move-exception v0

    monitor-exit v1
    :try_end_32
    .catchall {:try_start_3 .. :try_end_32} :catchall_30

    throw v0

    .line 411
    :cond_33
    :try_start_33
    iget-object v3, p0, LaD/A;->e:Lay/m;

    iget-object v0, v0, LaD/E;->a:Ljava/lang/String;

    invoke-interface {v3, v0}, Lay/m;->a(Ljava/lang/String;)V

    goto :goto_11

    .line 414
    :cond_3b
    monitor-exit v1
    :try_end_3c
    .catchall {:try_start_33 .. :try_end_3c} :catchall_30

    .line 415
    return-void
.end method

.method private a(Z)V
    .registers 6
    .parameter

    .prologue
    .line 170
    new-instance v0, LaD/F;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaD/F;-><init>(LaD/A;LaD/B;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LaD/F;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 171
    return-void
.end method

.method static synthetic b(LaD/A;)Lay/m;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LaD/A;->d:Lay/m;

    return-object v0
.end method

.method private b(I)V
    .registers 4
    .parameter

    .prologue
    .line 263
    iget-object v0, p0, LaD/A;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/D;

    iget-object v0, v0, LaD/D;->a:Lcom/google/googlenav/ai;

    .line 264
    iget-object v1, p0, LaD/A;->g:Lcom/google/googlenav/ui/wizard/gh;

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/wizard/gh;->a(Lcom/google/googlenav/ai;)V

    .line 265
    return-void
.end method

.method static synthetic c(LaD/A;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 43
    invoke-direct {p0}, LaD/A;->d()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 178
    iget-object v0, p0, LaD/A;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 180
    iget-object v0, p0, LaD/A;->f:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    move v1, v2

    .line 181
    :goto_d
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_29

    const/4 v0, 0x3

    if-ge v1, v0, :cond_29

    .line 182
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/D;

    .line 183
    invoke-virtual {p0, v0}, LaD/A;->a(LaD/D;)Landroid/view/View;

    move-result-object v0

    .line 184
    iget-object v4, p0, LaD/A;->b:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 181
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    .line 188
    :cond_29
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_47

    .line 189
    iget-object v0, p0, LaD/A;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 196
    :goto_34
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x6

    if-ge v0, v1, :cond_46

    .line 197
    new-instance v0, LaD/C;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaD/C;-><init>(LaD/A;LaD/B;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LaD/C;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 199
    :cond_46
    return-void

    .line 191
    :cond_47
    iget-object v0, p0, LaD/A;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_34
.end method

.method static synthetic d(LaD/A;)Lcom/google/googlenav/aV;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LaD/A;->h:Lcom/google/googlenav/aV;

    return-object v0
.end method

.method private d()Ljava/util/List;
    .registers 8

    .prologue
    .line 338
    invoke-static {}, Lay/l;->a()Lay/l;

    move-result-object v0

    invoke-virtual {v0}, Lay/l;->g()Lay/m;

    move-result-object v1

    .line 340
    invoke-static {}, Lay/l;->a()Lay/l;

    move-result-object v0

    invoke-virtual {v0}, Lay/l;->h()Lay/m;

    move-result-object v0

    .line 343
    invoke-static {}, Lcom/google/common/collect/dm;->a()Ljava/util/HashSet;

    move-result-object v2

    .line 344
    invoke-interface {v0}, Lay/m;->d()Lay/t;

    move-result-object v0

    invoke-interface {v0}, Lay/t;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_20
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lay/C;

    .line 345
    new-instance v4, LaD/E;

    invoke-virtual {v0}, Lay/s;->h()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, p0, v5, v6, v0}, LaD/E;-><init>(LaD/A;Ljava/lang/String;ZLay/s;)V

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_20

    .line 347
    :cond_3a
    invoke-interface {v1}, Lay/m;->d()Lay/t;

    move-result-object v0

    invoke-interface {v0}, Lay/t;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_46
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_60

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lay/C;

    .line 348
    new-instance v3, LaD/E;

    invoke-virtual {v0}, Lay/s;->h()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v3, p0, v4, v5, v0}, LaD/E;-><init>(LaD/A;Ljava/lang/String;ZLay/s;)V

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_46

    .line 351
    :cond_60
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/cx;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 352
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 353
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 355
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(LaD/A;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LaD/A;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(LaD/A;)V
    .registers 1
    .parameter

    .prologue
    .line 43
    invoke-direct {p0}, LaD/A;->c()V

    return-void
.end method


# virtual methods
.method protected a()I
    .registers 2

    .prologue
    .line 202
    const v0, 0x7f04013d

    return v0
.end method

.method public a(LaD/D;)Landroid/view/View;
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 207
    invoke-virtual {p0}, LaD/A;->a()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1, v4}, Lcom/google/googlenav/ui/bq;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 209
    const v0, 0x7f1000fe

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 210
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    const v0, 0x7f100020

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 213
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    const v0, 0x7f100195

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 216
    iget-object v2, p1, LaD/D;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ak()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    const v0, 0x7f100047

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 218
    iget-object v2, p1, LaD/D;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    const v0, 0x7f100338

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 221
    iget-boolean v2, p1, LaD/D;->b:Z

    if-eqz v2, :cond_78

    .line 222
    const/16 v2, 0x3a1

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    :goto_5a
    const v0, 0x7f100337

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 229
    iget-object v2, p1, LaD/D;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bm()Ljava/lang/String;

    move-result-object v2

    .line 230
    invoke-static {v2}, Lcom/google/googlenav/ui/bK;->a(Ljava/lang/String;)I

    move-result v3

    .line 232
    invoke-static {v2, v4}, Lcom/google/googlenav/ui/bK;->a(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v2

    .line 233
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 234
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    return-object v1

    .line 225
    :cond_78
    const/16 v2, 0x3a2

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5a
.end method

.method public a(Lay/O;)V
    .registers 2
    .parameter

    .prologue
    .line 431
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 440
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 419
    iget-object v1, p0, LaD/A;->c:Ljava/util/Set;

    monitor-enter v1

    .line 420
    :try_start_4
    iget-object v0, p0, LaD/A;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 421
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_2a

    .line 422
    iget-object v0, p0, LaD/A;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_29

    iget-object v0, p0, LaD/A;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_29

    .line 423
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaD/A;->a(Z)V

    .line 424
    iget-object v0, p0, LaD/A;->d:Lay/m;

    invoke-interface {v0, v2}, Lay/m;->a(Lay/n;)V

    .line 425
    iget-object v0, p0, LaD/A;->e:Lay/m;

    invoke-interface {v0, v2}, Lay/m;->a(Lay/n;)V

    .line 427
    :cond_29
    return-void

    .line 421
    :catchall_2a
    move-exception v0

    :try_start_2b
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_2b .. :try_end_2c} :catchall_2a

    throw v0
.end method

.method public b()V
    .registers 3

    .prologue
    .line 435
    new-instance v0, LaD/C;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaD/C;-><init>(LaD/A;LaD/B;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LaD/C;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 436
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 241
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 242
    iget-object v1, p0, LaD/A;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 243
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f1000fe

    if-ne v1, v2, :cond_19

    .line 244
    invoke-direct {p0, v0}, LaD/A;->a(I)V

    .line 248
    :goto_18
    return-void

    .line 246
    :cond_19
    invoke-direct {p0, v0}, LaD/A;->b(I)V

    goto :goto_18
.end method
