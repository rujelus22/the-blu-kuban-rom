.class LaD/E;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Lay/s;

.field final synthetic d:LaD/A;

.field private e:Lay/C;

.field private f:Lay/G;


# direct methods
.method public constructor <init>(LaD/A;Ljava/lang/String;ZLay/s;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 87
    iput-object p1, p0, LaD/E;->d:LaD/A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p2, p0, LaD/E;->a:Ljava/lang/String;

    .line 89
    iput-boolean p3, p0, LaD/E;->b:Z

    .line 90
    iput-object p4, p0, LaD/E;->c:Lay/s;

    .line 91
    return-void
.end method


# virtual methods
.method public a(LaD/E;)I
    .registers 7
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, LaD/E;->c:Lay/s;

    invoke-virtual {v0}, Lay/s;->i()J

    move-result-wide v0

    .line 96
    iget-object v2, p1, LaD/E;->c:Lay/s;

    invoke-virtual {v2}, Lay/s;->i()J

    move-result-wide v2

    .line 97
    cmp-long v4, v0, v2

    if-nez v4, :cond_12

    .line 98
    const/4 v0, 0x0

    .line 100
    :goto_11
    return v0

    :cond_12
    cmp-long v0, v0, v2

    if-lez v0, :cond_18

    const/4 v0, -0x1

    goto :goto_11

    :cond_18
    const/4 v0, 0x1

    goto :goto_11
.end method

.method public a()Lay/C;
    .registers 3

    .prologue
    .line 115
    iget-object v0, p0, LaD/E;->e:Lay/C;

    if-nez v0, :cond_1c

    .line 116
    iget-boolean v0, p0, LaD/E;->b:Z

    if-nez v0, :cond_1f

    .line 117
    iget-object v0, p0, LaD/E;->d:LaD/A;

    invoke-static {v0}, LaD/A;->a(LaD/A;)Lay/m;

    move-result-object v0

    invoke-interface {v0}, Lay/m;->d()Lay/t;

    move-result-object v0

    iget-object v1, p0, LaD/E;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lay/t;->a(Ljava/lang/String;)Lay/s;

    move-result-object v0

    check-cast v0, Lay/C;

    iput-object v0, p0, LaD/E;->e:Lay/C;

    .line 124
    :cond_1c
    :goto_1c
    iget-object v0, p0, LaD/E;->e:Lay/C;

    return-object v0

    .line 120
    :cond_1f
    iget-object v0, p0, LaD/E;->d:LaD/A;

    invoke-static {v0}, LaD/A;->b(LaD/A;)Lay/m;

    move-result-object v0

    invoke-interface {v0}, Lay/m;->d()Lay/t;

    move-result-object v0

    iget-object v1, p0, LaD/E;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lay/t;->a(Ljava/lang/String;)Lay/s;

    move-result-object v0

    check-cast v0, Lay/C;

    iput-object v0, p0, LaD/E;->e:Lay/C;

    goto :goto_1c
.end method

.method public b()Lay/G;
    .registers 3

    .prologue
    .line 128
    iget-boolean v0, p0, LaD/E;->b:Z

    if-nez v0, :cond_19

    .line 129
    iget-object v0, p0, LaD/E;->d:LaD/A;

    invoke-static {v0}, LaD/A;->a(LaD/A;)Lay/m;

    move-result-object v0

    invoke-interface {v0}, Lay/m;->d()Lay/t;

    move-result-object v0

    iget-object v1, p0, LaD/E;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lay/t;->b(Ljava/lang/String;)Lay/G;

    move-result-object v0

    iput-object v0, p0, LaD/E;->f:Lay/G;

    .line 135
    :goto_16
    iget-object v0, p0, LaD/E;->f:Lay/G;

    return-object v0

    .line 132
    :cond_19
    iget-object v0, p0, LaD/E;->d:LaD/A;

    invoke-static {v0}, LaD/A;->b(LaD/A;)Lay/m;

    move-result-object v0

    invoke-interface {v0}, Lay/m;->d()Lay/t;

    move-result-object v0

    iget-object v1, p0, LaD/E;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lay/t;->b(Ljava/lang/String;)Lay/G;

    move-result-object v0

    iput-object v0, p0, LaD/E;->f:Lay/G;

    goto :goto_16
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 80
    check-cast p1, LaD/E;

    invoke-virtual {p0, p1}, LaD/E;->a(LaD/E;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 106
    instance-of v0, p1, LaD/E;

    if-eqz v0, :cond_12

    iget-object v0, p0, LaD/E;->a:Ljava/lang/String;

    check-cast p1, LaD/E;

    iget-object v1, p1, LaD/E;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 111
    iget-object v0, p0, LaD/E;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
