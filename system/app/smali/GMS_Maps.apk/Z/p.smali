.class public final enum Lz/p;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lz/p;

.field public static final enum b:Lz/p;

.field public static final enum c:Lz/p;

.field public static final enum d:Lz/p;

.field public static final enum e:Lz/p;

.field public static final enum f:Lz/p;

.field public static final enum g:Lz/p;

.field private static final synthetic i:[Lz/p;


# instance fields
.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 29
    new-instance v0, Lz/p;

    const-string v1, "PICK"

    invoke-direct {v0, v1, v4, v4}, Lz/p;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lz/p;->a:Lz/p;

    .line 30
    new-instance v0, Lz/p;

    const-string v1, "TEXTURE0"

    invoke-direct {v0, v1, v5, v5}, Lz/p;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lz/p;->b:Lz/p;

    .line 31
    new-instance v0, Lz/p;

    const-string v1, "TEXTURE1"

    invoke-direct {v0, v1, v6, v6}, Lz/p;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lz/p;->c:Lz/p;

    .line 32
    new-instance v0, Lz/p;

    const-string v1, "SHADER"

    invoke-direct {v0, v1, v7, v7}, Lz/p;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lz/p;->d:Lz/p;

    .line 33
    new-instance v0, Lz/p;

    const-string v1, "STENCIL"

    invoke-direct {v0, v1, v8, v8}, Lz/p;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lz/p;->e:Lz/p;

    .line 34
    new-instance v0, Lz/p;

    const-string v1, "POLYGON"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lz/p;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lz/p;->f:Lz/p;

    .line 35
    new-instance v0, Lz/p;

    const-string v1, "BLEND"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lz/p;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lz/p;->g:Lz/p;

    .line 28
    const/4 v0, 0x7

    new-array v0, v0, [Lz/p;

    sget-object v1, Lz/p;->a:Lz/p;

    aput-object v1, v0, v4

    sget-object v1, Lz/p;->b:Lz/p;

    aput-object v1, v0, v5

    sget-object v1, Lz/p;->c:Lz/p;

    aput-object v1, v0, v6

    sget-object v1, Lz/p;->d:Lz/p;

    aput-object v1, v0, v7

    sget-object v1, Lz/p;->e:Lz/p;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lz/p;->f:Lz/p;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lz/p;->g:Lz/p;

    aput-object v2, v0, v1

    sput-object v0, Lz/p;->i:[Lz/p;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput p3, p0, Lz/p;->h:I

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lz/p;
    .registers 2
    .parameter

    .prologue
    .line 28
    const-class v0, Lz/p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lz/p;

    return-object v0
.end method

.method public static values()[Lz/p;
    .registers 1

    .prologue
    .line 28
    sget-object v0, Lz/p;->i:[Lz/p;

    invoke-virtual {v0}, [Lz/p;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lz/p;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 44
    iget v0, p0, Lz/p;->h:I

    return v0
.end method
