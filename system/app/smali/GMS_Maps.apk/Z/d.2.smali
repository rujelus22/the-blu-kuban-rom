.class public Lz/d;
.super Lz/o;
.source "SourceFile"


# instance fields
.field private g:I

.field private h:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 47
    sget-object v0, Lz/p;->g:Lz/p;

    invoke-direct {p0, v0}, Lz/o;-><init>(Lz/p;)V

    .line 39
    const/4 v0, 0x1

    iput v0, p0, Lz/d;->g:I

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lz/d;->h:I

    .line 48
    return-void
.end method

.method public constructor <init>(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 59
    sget-object v0, Lz/p;->g:Lz/p;

    invoke-direct {p0, v0}, Lz/o;-><init>(Lz/p;)V

    .line 39
    const/4 v0, 0x1

    iput v0, p0, Lz/d;->g:I

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lz/d;->h:I

    .line 60
    invoke-virtual {p0, p1, p2}, Lz/d;->a(II)V

    .line 61
    return-void
.end method


# virtual methods
.method public a(II)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/16 v5, 0x305

    const/16 v4, 0x304

    const/16 v3, 0x303

    const/16 v2, 0x302

    const/4 v1, 0x1

    .line 73
    iget-boolean v0, p0, Lz/d;->c:Z

    if-eqz v0, :cond_10

    .line 74
    invoke-static {}, Lz/k;->c()V

    .line 78
    :cond_10
    if-eqz p1, :cond_41

    if-eq p1, v1, :cond_41

    const/16 v0, 0x306

    if-eq p1, v0, :cond_41

    const/16 v0, 0x307

    if-eq p1, v0, :cond_41

    if-eq p1, v2, :cond_41

    if-eq p1, v3, :cond_41

    if-eq p1, v4, :cond_41

    if-eq p1, v5, :cond_41

    const/16 v0, 0x308

    if-eq p1, v0, :cond_41

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid srcFactor value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_41
    if-eqz p2, :cond_6e

    if-eq p2, v1, :cond_6e

    const/16 v0, 0x300

    if-eq p2, v0, :cond_6e

    const/16 v0, 0x301

    if-eq p2, v0, :cond_6e

    if-eq p2, v2, :cond_6e

    if-eq p2, v3, :cond_6e

    if-eq p2, v4, :cond_6e

    if-eq p2, v5, :cond_6e

    .line 92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid dstFactor value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_6e
    iput p1, p0, Lz/d;->g:I

    .line 97
    iput p2, p0, Lz/d;->h:I

    .line 98
    return-void
.end method

.method a(Lz/k;Lz/o;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 113
    if-nez p2, :cond_7

    .line 114
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 116
    :cond_7
    return-void
.end method
