.class public Lz/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field volatile a:Lz/K;

.field protected b:Z

.field protected c:[Lz/K;

.field protected d:[I

.field e:[[F

.field protected f:Z

.field private final g:[[Lz/o;

.field private final h:[Lz/M;

.field private i:Ljava/lang/String;

.field private j:B

.field private k:B

.field private l:Lz/k;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x5

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/i;->b:Z

    .line 118
    iput-boolean v1, p0, Lz/i;->f:Z

    .line 131
    iput-byte v1, p0, Lz/i;->j:B

    .line 138
    const/4 v0, -0x1

    iput-byte v0, p0, Lz/i;->k:B

    .line 147
    new-array v0, v2, [Lz/M;

    iput-object v0, p0, Lz/i;->h:[Lz/M;

    .line 148
    sget v0, Lz/o;->a:I

    filled-new-array {v2, v0}, [I

    move-result-object v0

    const-class v1, Lz/o;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lz/o;

    iput-object v0, p0, Lz/i;->g:[[Lz/o;

    .line 149
    new-instance v0, Lz/K;

    invoke-direct {v0}, Lz/K;-><init>()V

    iput-object v0, p0, Lz/i;->a:Lz/K;

    .line 150
    new-array v0, v2, [Lz/K;

    iput-object v0, p0, Lz/i;->c:[Lz/K;

    .line 151
    new-array v0, v2, [[F

    iput-object v0, p0, Lz/i;->e:[[F

    .line 152
    new-array v0, v2, [I

    iput-object v0, p0, Lz/i;->d:[I

    .line 153
    return-void
.end method


# virtual methods
.method public a()B
    .registers 2

    .prologue
    .line 375
    iget-byte v0, p0, Lz/i;->j:B

    return v0
.end method

.method public a(Lz/p;)I
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 281
    move v1, v0

    .line 282
    :goto_2
    const/4 v2, 0x5

    if-ge v1, v2, :cond_17

    .line 283
    iget-object v2, p0, Lz/i;->g:[[Lz/o;

    aget-object v2, v2, v1

    invoke-virtual {p1}, Lz/p;->a()I

    move-result v3

    aget-object v2, v2, v3

    if-eqz v2, :cond_14

    .line 284
    const/4 v2, 0x1

    shl-int/2addr v2, v1

    or-int/2addr v0, v2

    .line 282
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 287
    :cond_17
    return v0
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 394
    iget-boolean v0, p0, Lz/i;->f:Z

    if-eqz v0, :cond_7

    .line 395
    invoke-static {}, Lz/k;->c()V

    .line 397
    :cond_7
    int-to-byte v0, p1

    iput-byte v0, p0, Lz/i;->k:B

    .line 398
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 425
    iput-object p1, p0, Lz/i;->i:Ljava/lang/String;

    .line 426
    return-void
.end method

.method public a(Lz/K;)V
    .registers 3
    .parameter

    .prologue
    .line 402
    iget-boolean v0, p0, Lz/i;->f:Z

    if-eqz v0, :cond_7

    .line 403
    invoke-static {}, Lz/k;->c()V

    .line 405
    :cond_7
    iget-object v0, p0, Lz/i;->a:Lz/K;

    invoke-virtual {v0, p1}, Lz/K;->a(Lz/K;)Lz/K;

    .line 406
    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/i;->b:Z

    .line 407
    return-void
.end method

.method public a(Lz/M;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 161
    iget-boolean v0, p0, Lz/i;->f:Z

    if-eqz v0, :cond_7

    .line 162
    invoke-static {}, Lz/k;->c()V

    .line 167
    :cond_7
    iget-byte v0, p0, Lz/i;->j:B

    or-int/2addr v0, p2

    int-to-byte v0, v0

    iput-byte v0, p0, Lz/i;->j:B

    .line 168
    const/4 v0, 0x0

    :goto_e
    const/4 v1, 0x5

    if-ge v0, v1, :cond_5a

    .line 169
    const/4 v1, 0x1

    shl-int/2addr v1, v0

    .line 170
    and-int/2addr v1, p2

    if-eqz v1, :cond_57

    .line 171
    iget-object v1, p0, Lz/i;->h:[Lz/M;

    aget-object v1, v1, v0

    .line 172
    iget-object v2, p0, Lz/i;->h:[Lz/M;

    aput-object p1, v2, v0

    .line 174
    iget-object v2, p0, Lz/i;->c:[Lz/K;

    aget-object v2, v2, v0

    if-nez v2, :cond_2d

    .line 175
    iget-object v2, p0, Lz/i;->c:[Lz/K;

    new-instance v3, Lz/K;

    invoke-direct {v3}, Lz/K;-><init>()V

    aput-object v3, v2, v0

    .line 177
    :cond_2d
    iget-object v2, p0, Lz/i;->e:[[F

    aget-object v2, v2, v0

    if-nez v2, :cond_3b

    .line 178
    iget-object v2, p0, Lz/i;->e:[[F

    const/16 v3, 0x10

    new-array v3, v3, [F

    aput-object v3, v2, v0

    .line 180
    :cond_3b
    iget-boolean v2, p0, Lz/i;->f:Z

    if-eqz v2, :cond_57

    .line 182
    if-eqz v1, :cond_4b

    .line 183
    invoke-virtual {v1, p0}, Lz/M;->a(Lz/i;)V

    .line 184
    iget-object v2, p0, Lz/i;->l:Lz/k;

    sget-object v3, Lz/j;->a:Lz/j;

    invoke-virtual {v1, v2, v3}, Lz/M;->a(Lz/k;Lz/j;)Z

    .line 186
    :cond_4b
    if-eqz p1, :cond_57

    .line 187
    invoke-virtual {p1, p0}, Lz/M;->b(Lz/i;)V

    .line 188
    iget-object v1, p0, Lz/i;->l:Lz/k;

    sget-object v2, Lz/j;->c:Lz/j;

    invoke-virtual {p1, v1, v2}, Lz/M;->a(Lz/k;Lz/j;)Z

    .line 168
    :cond_57
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 193
    :cond_5a
    return-void
.end method

.method public a(Lz/o;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 204
    iget-boolean v0, p0, Lz/i;->f:Z

    if-eqz v0, :cond_7

    .line 205
    invoke-static {}, Lz/k;->c()V

    .line 210
    :cond_7
    const/4 v0, 0x0

    :goto_8
    const/4 v1, 0x5

    if-ge v0, v1, :cond_47

    .line 211
    const/4 v1, 0x1

    shl-int/2addr v1, v0

    .line 212
    and-int/2addr v1, p2

    if-eqz v1, :cond_44

    .line 213
    iget-object v1, p0, Lz/i;->g:[[Lz/o;

    aget-object v1, v1, v0

    iget-object v2, p1, Lz/o;->f:Lz/p;

    invoke-virtual {v2}, Lz/p;->a()I

    move-result v2

    aget-object v1, v1, v2

    .line 214
    iget-object v2, p0, Lz/i;->g:[[Lz/o;

    aget-object v2, v2, v0

    iget-object v3, p1, Lz/o;->f:Lz/p;

    invoke-virtual {v3}, Lz/p;->a()I

    move-result v3

    aput-object p1, v2, v3

    .line 216
    iget-boolean v2, p0, Lz/i;->f:Z

    if-eqz v2, :cond_44

    .line 219
    if-eqz v1, :cond_38

    .line 220
    invoke-virtual {v1, p0}, Lz/o;->b(Lz/i;)V

    .line 221
    iget-object v2, p0, Lz/i;->l:Lz/k;

    sget-object v3, Lz/j;->a:Lz/j;

    invoke-virtual {v1, v2, v3}, Lz/o;->a(Lz/k;Lz/j;)Z

    .line 223
    :cond_38
    if-eqz p1, :cond_44

    .line 224
    invoke-virtual {p1, p0}, Lz/o;->a(Lz/i;)V

    .line 225
    iget-object v1, p0, Lz/i;->l:Lz/k;

    sget-object v2, Lz/j;->c:Lz/j;

    invoke-virtual {p1, v1, v2}, Lz/o;->a(Lz/k;Lz/j;)Z

    .line 210
    :cond_44
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 230
    :cond_47
    return-void
.end method

.method a(Lz/k;Lz/j;)Z
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 306
    iget-boolean v1, p2, Lz/j;->e:Z

    iget-boolean v2, p0, Lz/i;->f:Z

    if-ne v1, v2, :cond_c

    iget-boolean v1, p2, Lz/j;->f:Z

    if-nez v1, :cond_c

    .line 346
    :goto_b
    return v0

    .line 310
    :cond_c
    iput-object p1, p0, Lz/i;->l:Lz/k;

    .line 311
    iget-object v2, p0, Lz/i;->h:[Lz/M;

    array-length v3, v2

    move v1, v0

    :goto_12
    if-ge v1, v3, :cond_2d

    aget-object v4, v2, v1

    .line 312
    if-eqz v4, :cond_26

    .line 313
    iget-boolean v5, p2, Lz/j;->f:Z

    if-nez v5, :cond_23

    .line 314
    iget-boolean v5, p2, Lz/j;->e:Z

    if-eqz v5, :cond_29

    .line 315
    invoke-virtual {v4, p0}, Lz/M;->b(Lz/i;)V

    .line 320
    :cond_23
    :goto_23
    invoke-virtual {v4, p1, p2}, Lz/M;->a(Lz/k;Lz/j;)Z

    .line 311
    :cond_26
    add-int/lit8 v1, v1, 0x1

    goto :goto_12

    .line 317
    :cond_29
    invoke-virtual {v4, p0}, Lz/M;->a(Lz/i;)V

    goto :goto_23

    .line 324
    :cond_2d
    const-string v1, "Entity"

    const-string v2, "vertex data setLive"

    invoke-static {v1, v2}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-object v3, p0, Lz/i;->g:[[Lz/o;

    array-length v4, v3

    move v2, v0

    :goto_38
    if-ge v2, v4, :cond_5d

    aget-object v5, v3, v2

    .line 328
    array-length v6, v5

    move v1, v0

    :goto_3e
    if-ge v1, v6, :cond_59

    aget-object v7, v5, v1

    .line 329
    if-eqz v7, :cond_52

    .line 330
    iget-boolean v8, p2, Lz/j;->f:Z

    if-nez v8, :cond_4f

    .line 331
    iget-boolean v8, p2, Lz/j;->e:Z

    if-eqz v8, :cond_55

    .line 332
    invoke-virtual {v7, p0}, Lz/o;->a(Lz/i;)V

    .line 337
    :cond_4f
    :goto_4f
    invoke-virtual {v7, p1, p2}, Lz/o;->a(Lz/k;Lz/j;)Z

    .line 328
    :cond_52
    add-int/lit8 v1, v1, 0x1

    goto :goto_3e

    .line 334
    :cond_55
    invoke-virtual {v7, p0}, Lz/o;->b(Lz/i;)V

    goto :goto_4f

    .line 327
    :cond_59
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_38

    .line 342
    :cond_5d
    const-string v0, "Entity"

    const-string v1, "entity state setLive"

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    iget-boolean v0, p2, Lz/j;->e:Z

    iput-boolean v0, p0, Lz/i;->f:Z

    .line 346
    const/4 v0, 0x1

    goto :goto_b
.end method
