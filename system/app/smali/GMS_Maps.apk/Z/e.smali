.class public Lz/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:[F

.field b:I

.field private c:Lz/A;

.field private volatile d:[F

.field private e:[F

.field private f:[F

.field private g:[F

.field private h:Z

.field private i:B

.field private j:Z

.field private final k:F

.field private final l:F

.field private final m:F

.field private final n:Lz/g;

.field private o:Ljava/util/List;


# direct methods
.method public constructor <init>(Lz/A;I[F)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x10

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-array v0, v1, [F

    iput-object v0, p0, Lz/e;->d:[F

    .line 41
    new-array v0, v1, [F

    iput-object v0, p0, Lz/e;->e:[F

    .line 48
    new-array v0, v1, [F

    iput-object v0, p0, Lz/e;->a:[F

    .line 53
    new-array v0, v1, [F

    iput-object v0, p0, Lz/e;->f:[F

    .line 59
    new-array v0, v1, [F

    iput-object v0, p0, Lz/e;->g:[F

    .line 64
    iput-boolean v2, p0, Lz/e;->h:Z

    .line 70
    iput v2, p0, Lz/e;->b:I

    .line 84
    iput-boolean v2, p0, Lz/e;->j:Z

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lz/e;->o:Ljava/util/List;

    .line 154
    iput-object p1, p0, Lz/e;->c:Lz/A;

    .line 155
    int-to-byte v0, p2

    iput-byte v0, p0, Lz/e;->i:B

    .line 156
    const/high16 v0, -0x4080

    iput v0, p0, Lz/e;->k:F

    .line 157
    const/high16 v0, 0x3f80

    iput v0, p0, Lz/e;->l:F

    .line 158
    const/4 v0, 0x0

    iput v0, p0, Lz/e;->m:F

    .line 159
    sget-object v0, Lz/g;->c:Lz/g;

    iput-object v0, p0, Lz/e;->n:Lz/g;

    .line 160
    iget-object v0, p0, Lz/e;->d:[F

    invoke-static {p3, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/e;->h:Z

    .line 162
    return-void
.end method


# virtual methods
.method protected a()V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 247
    iget-object v0, p0, Lz/e;->f:[F

    iget-object v2, p0, Lz/e;->a:[F

    iget-object v4, p0, Lz/e;->d:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 248
    iget v0, p0, Lz/e;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lz/e;->b:I

    .line 249
    return-void
.end method

.method a(Lz/A;II)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    .line 289
    iget-object v8, p0, Lz/e;->g:[F

    monitor-enter v8

    .line 290
    :try_start_3
    sget-object v0, Lz/f;->a:[I

    iget-object v1, p0, Lz/e;->n:Lz/g;

    invoke-virtual {v1}, Lz/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4a

    .line 302
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unimplemented projection type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lz/e;->n:Lz/g;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 306
    :catchall_2b
    move-exception v0

    monitor-exit v8
    :try_end_2d
    .catchall {:try_start_3 .. :try_end_2d} :catchall_2b

    throw v0

    .line 292
    :pswitch_2e
    :try_start_2e
    iget-object v0, p0, Lz/e;->g:[F

    invoke-virtual {p0, v0, p2, p3}, Lz/e;->a([FII)V

    .line 304
    :goto_33
    :pswitch_33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/e;->h:Z

    .line 305
    invoke-virtual {p0}, Lz/e;->f()V

    .line 306
    monitor-exit v8

    .line 307
    return-void

    .line 295
    :pswitch_3b
    iget-object v1, p0, Lz/e;->g:[F

    const/4 v2, 0x0

    int-to-float v3, p2

    const/4 v4, 0x0

    int-to-float v5, p3

    iget v6, p0, Lz/e;->k:F

    iget v7, p0, Lz/e;->l:F

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lz/e;->a([FFFFFFF)V
    :try_end_49
    .catchall {:try_start_2e .. :try_end_49} :catchall_2b

    goto :goto_33

    .line 290
    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_2e
        :pswitch_3b
        :pswitch_33
    .end packed-switch
.end method

.method public a([FFFFFFF)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x4000

    const/4 v3, 0x0

    .line 449
    cmpl-float v0, p2, p3

    if-eqz v0, :cond_f

    cmpl-float v0, p5, p4

    if-eqz v0, :cond_f

    cmpl-float v0, p6, p7

    if-nez v0, :cond_10

    .line 479
    :cond_f
    :goto_f
    return-void

    .line 457
    :cond_10
    const/4 v0, 0x0

    sub-float v1, p3, p2

    div-float v1, v2, v1

    aput v1, p1, v0

    .line 458
    const/4 v0, 0x1

    aput v3, p1, v0

    .line 459
    const/4 v0, 0x2

    aput v3, p1, v0

    .line 460
    const/4 v0, 0x3

    aput v3, p1, v0

    .line 463
    const/4 v0, 0x4

    aput v3, p1, v0

    .line 464
    const/4 v0, 0x5

    sub-float v1, p5, p4

    div-float v1, v2, v1

    aput v1, p1, v0

    .line 465
    const/4 v0, 0x6

    aput v3, p1, v0

    .line 466
    const/4 v0, 0x7

    aput v3, p1, v0

    .line 469
    const/16 v0, 0x8

    aput v3, p1, v0

    .line 470
    const/16 v0, 0x9

    aput v3, p1, v0

    .line 471
    const/16 v0, 0xa

    const/high16 v1, -0x4000

    sub-float v2, p7, p6

    div-float/2addr v1, v2

    aput v1, p1, v0

    .line 472
    const/16 v0, 0xb

    aput v3, p1, v0

    .line 475
    const/16 v0, 0xc

    add-float v1, p3, p2

    neg-float v1, v1

    sub-float v2, p3, p2

    div-float/2addr v1, v2

    aput v1, p1, v0

    .line 476
    const/16 v0, 0xd

    add-float v1, p5, p4

    neg-float v1, v1

    sub-float v2, p5, p4

    div-float/2addr v1, v2

    aput v1, p1, v0

    .line 477
    const/16 v0, 0xe

    add-float v1, p7, p6

    neg-float v1, v1

    sub-float v2, p7, p6

    div-float/2addr v1, v2

    aput v1, p1, v0

    .line 478
    const/16 v0, 0xf

    const/high16 v1, 0x3f80

    aput v1, p1, v0

    goto :goto_f
.end method

.method public a([FII)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v8, 0x4000

    const/4 v7, 0x0

    .line 398
    if-nez p3, :cond_88

    .line 399
    const/high16 v0, 0x3f80

    .line 408
    :goto_7
    iget v1, p0, Lz/e;->k:F

    iget v2, p0, Lz/e;->m:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x4000

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v1, v2

    .line 409
    neg-float v2, v1

    .line 411
    neg-float v3, v1

    div-float/2addr v3, v0

    .line 412
    div-float v0, v1, v0

    .line 415
    const/4 v4, 0x0

    iget v5, p0, Lz/e;->k:F

    mul-float/2addr v5, v8

    sub-float v6, v1, v2

    div-float/2addr v5, v6

    aput v5, p1, v4

    .line 416
    const/4 v4, 0x1

    aput v7, p1, v4

    .line 417
    const/4 v4, 0x2

    aput v7, p1, v4

    .line 418
    const/4 v4, 0x3

    aput v7, p1, v4

    .line 421
    const/4 v4, 0x4

    aput v7, p1, v4

    .line 422
    const/4 v4, 0x5

    iget v5, p0, Lz/e;->k:F

    mul-float/2addr v5, v8

    sub-float v6, v0, v3

    div-float/2addr v5, v6

    aput v5, p1, v4

    .line 423
    const/4 v4, 0x6

    aput v7, p1, v4

    .line 424
    const/4 v4, 0x7

    aput v7, p1, v4

    .line 427
    const/16 v4, 0x8

    add-float v5, v1, v2

    sub-float/2addr v1, v2

    div-float v1, v5, v1

    aput v1, p1, v4

    .line 428
    const/16 v1, 0x9

    add-float v2, v0, v3

    sub-float/2addr v0, v3

    div-float v0, v2, v0

    aput v0, p1, v1

    .line 429
    const/16 v0, 0xa

    iget v1, p0, Lz/e;->l:F

    iget v2, p0, Lz/e;->k:F

    add-float/2addr v1, v2

    neg-float v1, v1

    iget v2, p0, Lz/e;->l:F

    iget v3, p0, Lz/e;->k:F

    sub-float/2addr v2, v3

    div-float/2addr v1, v2

    aput v1, p1, v0

    .line 430
    const/16 v0, 0xb

    const/high16 v1, -0x4080

    aput v1, p1, v0

    .line 433
    const/16 v0, 0xc

    aput v7, p1, v0

    .line 434
    const/16 v0, 0xd

    aput v7, p1, v0

    .line 435
    const/16 v0, 0xe

    iget v1, p0, Lz/e;->l:F

    mul-float/2addr v1, v8

    iget v2, p0, Lz/e;->k:F

    mul-float/2addr v1, v2

    neg-float v1, v1

    iget v2, p0, Lz/e;->l:F

    iget v3, p0, Lz/e;->k:F

    sub-float/2addr v2, v3

    div-float/2addr v1, v2

    aput v1, p1, v0

    .line 436
    const/16 v0, 0xf

    aput v7, p1, v0

    .line 437
    return-void

    .line 401
    :cond_88
    int-to-float v0, p2

    int-to-float v1, p3

    div-float/2addr v0, v1

    goto/16 :goto_7
.end method

.method a(Lz/k;Lz/j;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 263
    iget-boolean v0, p2, Lz/j;->e:Z

    iget-boolean v1, p0, Lz/e;->j:Z

    if-ne v0, v1, :cond_c

    iget-boolean v0, p2, Lz/j;->f:Z

    if-nez v0, :cond_c

    .line 264
    const/4 v0, 0x0

    .line 277
    :goto_b
    return v0

    .line 267
    :cond_c
    iget-boolean v0, p2, Lz/j;->e:Z

    iput-boolean v0, p0, Lz/e;->j:Z

    .line 269
    iget-boolean v0, p0, Lz/e;->j:Z

    if-eqz v0, :cond_31

    .line 270
    iget-object v0, p0, Lz/e;->c:Lz/A;

    invoke-virtual {v0, p0}, Lz/A;->a(Lz/e;)V

    .line 271
    iget-object v0, p0, Lz/e;->c:Lz/A;

    iget-object v1, p0, Lz/e;->c:Lz/A;

    invoke-virtual {v1}, Lz/A;->b()I

    move-result v1

    iget-object v2, p0, Lz/e;->c:Lz/A;

    invoke-virtual {v2}, Lz/A;->c()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lz/e;->a(Lz/A;II)V

    .line 276
    :goto_2a
    iget-object v0, p0, Lz/e;->c:Lz/A;

    invoke-virtual {v0, p1, p2}, Lz/A;->a(Lz/k;Lz/j;)Z

    .line 277
    const/4 v0, 0x1

    goto :goto_b

    .line 273
    :cond_31
    iget-object v0, p0, Lz/e;->c:Lz/A;

    invoke-virtual {v0, p0}, Lz/A;->b(Lz/e;)V

    goto :goto_2a
.end method

.method public b()Lz/A;
    .registers 2

    .prologue
    .line 252
    iget-object v0, p0, Lz/e;->c:Lz/A;

    return-object v0
.end method

.method c()V
    .registers 7

    .prologue
    .line 330
    iget-object v1, p0, Lz/e;->g:[F

    monitor-enter v1

    .line 331
    :try_start_3
    iget-boolean v0, p0, Lz/e;->h:Z

    if-eqz v0, :cond_18

    .line 332
    iget-object v0, p0, Lz/e;->g:[F

    const/4 v2, 0x0

    iget-object v3, p0, Lz/e;->a:[F

    const/4 v4, 0x0

    const/16 v5, 0x10

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 333
    invoke-virtual {p0}, Lz/e;->a()V

    .line 334
    const/4 v0, 0x0

    iput-boolean v0, p0, Lz/e;->h:Z

    .line 336
    :cond_18
    monitor-exit v1

    .line 337
    return-void

    .line 336
    :catchall_1a
    move-exception v0

    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    throw v0
.end method

.method d()V
    .registers 1

    .prologue
    .line 340
    return-void
.end method

.method public e()B
    .registers 2

    .prologue
    .line 384
    iget-byte v0, p0, Lz/e;->i:B

    return v0
.end method

.method f()V
    .registers 4

    .prologue
    .line 514
    iget-object v0, p0, Lz/e;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/y;

    .line 515
    iget-object v2, p0, Lz/e;->g:[F

    invoke-interface {v0, p0, v2}, Lz/y;->a(Lz/e;[F)V

    goto :goto_6

    .line 517
    :cond_18
    return-void
.end method
