.class public Lz/v;
.super Lz/M;
.source "SourceFile"


# instance fields
.field protected final a:Ljava/nio/ByteBuffer;

.field protected final b:I

.field protected c:I

.field protected d:I

.field protected e:I

.field protected f:I

.field protected g:I

.field protected final h:I

.field protected final i:I

.field private final l:Ljava/nio/ShortBuffer;

.field private final m:I

.field private n:[I

.field private o:[I

.field private final p:Z

.field private final q:Z

.field private final r:Z

.field private final s:Z

.field private final t:Z


# direct methods
.method public constructor <init>([FII)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 211
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lz/v;-><init>([F[SII)V

    .line 212
    return-void
.end method

.method public constructor <init>([F[SII)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 226
    invoke-direct {p0}, Lz/M;-><init>()V

    .line 38
    iput v2, p0, Lz/v;->c:I

    .line 48
    iput v2, p0, Lz/v;->d:I

    .line 168
    new-array v0, v1, [I

    iput-object v0, p0, Lz/v;->n:[I

    .line 173
    new-array v0, v1, [I

    iput-object v0, p0, Lz/v;->o:[I

    .line 229
    const/4 v0, 0x4

    if-eq p4, v0, :cond_3b

    const/4 v0, 0x5

    if-eq p4, v0, :cond_3b

    const/4 v0, 0x6

    if-eq p4, v0, :cond_3b

    if-eq p4, v1, :cond_3b

    const/4 v0, 0x3

    if-eq p4, v0, :cond_3b

    const/4 v0, 0x2

    if-eq p4, v0, :cond_3b

    .line 232
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal dataType "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 236
    :cond_3b
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_80

    move v0, v1

    :goto_40
    iput-boolean v0, p0, Lz/v;->p:Z

    .line 237
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_82

    move v0, v1

    :goto_47
    iput-boolean v0, p0, Lz/v;->q:Z

    .line 238
    and-int/lit8 v0, p3, 0x4

    if-eqz v0, :cond_84

    move v0, v1

    :goto_4e
    iput-boolean v0, p0, Lz/v;->s:Z

    .line 239
    and-int/lit8 v0, p3, 0x10

    if-eqz v0, :cond_86

    move v0, v1

    :goto_55
    iput-boolean v0, p0, Lz/v;->t:Z

    .line 240
    and-int/lit8 v0, p3, 0x8

    if-eqz v0, :cond_88

    :goto_5b
    iput-boolean v1, p0, Lz/v;->r:Z

    .line 242
    iput p3, p0, Lz/v;->i:I

    .line 243
    iput p4, p0, Lz/v;->h:I

    .line 244
    iget-boolean v0, p0, Lz/v;->t:Z

    if-eqz v0, :cond_8a

    .line 245
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "COLORS_BYTE_MASK can not be used with this constructor "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lz/v;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_80
    move v0, v2

    .line 236
    goto :goto_40

    :cond_82
    move v0, v2

    .line 237
    goto :goto_47

    :cond_84
    move v0, v2

    .line 238
    goto :goto_4e

    :cond_86
    move v0, v2

    .line 239
    goto :goto_55

    :cond_88
    move v1, v2

    .line 240
    goto :goto_5b

    .line 248
    :cond_8a
    invoke-virtual {p0}, Lz/v;->a()V

    .line 250
    array-length v0, p1

    iget v1, p0, Lz/v;->c:I

    div-int/lit8 v1, v1, 0x4

    div-int/2addr v0, v1

    iput v0, p0, Lz/v;->b:I

    .line 251
    iget v0, p0, Lz/v;->b:I

    iget v1, p0, Lz/v;->c:I

    mul-int/2addr v0, v1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lz/v;->a:Ljava/nio/ByteBuffer;

    .line 253
    iget-object v0, p0, Lz/v;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 255
    if-eqz p2, :cond_dc

    array-length v0, p2

    if-lez v0, :cond_dc

    .line 256
    array-length v0, p2

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Lz/v;->l:Ljava/nio/ShortBuffer;

    .line 258
    iget-object v0, p0, Lz/v;->l:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, p2}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 259
    array-length v0, p2

    iput v0, p0, Lz/v;->m:I

    .line 264
    :goto_db
    return-void

    .line 261
    :cond_dc
    const/4 v0, 0x0

    iput-object v0, p0, Lz/v;->l:Ljava/nio/ShortBuffer;

    .line 262
    iput v2, p0, Lz/v;->m:I

    goto :goto_db
.end method


# virtual methods
.method protected a()V
    .registers 2

    .prologue
    .line 303
    iget-boolean v0, p0, Lz/v;->p:Z

    if-eqz v0, :cond_a

    .line 304
    iget v0, p0, Lz/v;->c:I

    add-int/lit8 v0, v0, 0xc

    iput v0, p0, Lz/v;->c:I

    .line 306
    :cond_a
    iget-boolean v0, p0, Lz/v;->q:Z

    if-eqz v0, :cond_18

    .line 307
    iget v0, p0, Lz/v;->c:I

    iput v0, p0, Lz/v;->f:I

    .line 308
    iget v0, p0, Lz/v;->c:I

    add-int/lit8 v0, v0, 0xc

    iput v0, p0, Lz/v;->c:I

    .line 310
    :cond_18
    iget-boolean v0, p0, Lz/v;->s:Z

    if-eqz v0, :cond_26

    .line 311
    iget v0, p0, Lz/v;->c:I

    iput v0, p0, Lz/v;->g:I

    .line 312
    iget v0, p0, Lz/v;->c:I

    add-int/lit8 v0, v0, 0x10

    iput v0, p0, Lz/v;->c:I

    .line 314
    :cond_26
    iget-boolean v0, p0, Lz/v;->t:Z

    if-eqz v0, :cond_34

    .line 315
    iget v0, p0, Lz/v;->c:I

    iput v0, p0, Lz/v;->g:I

    .line 316
    iget v0, p0, Lz/v;->c:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lz/v;->c:I

    .line 318
    :cond_34
    iget-boolean v0, p0, Lz/v;->r:Z

    if-eqz v0, :cond_42

    .line 319
    iget v0, p0, Lz/v;->c:I

    iput v0, p0, Lz/v;->e:I

    .line 320
    iget v0, p0, Lz/v;->c:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lz/v;->c:I

    .line 322
    :cond_42
    return-void
.end method

.method public a(Lz/k;Lz/j;)Z
    .registers 11
    .parameter
    .parameter

    .prologue
    const v7, 0x88e4

    const v6, 0x8893

    const v5, 0x8892

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 327
    invoke-super {p0, p1, p2}, Lz/M;->a(Lz/k;Lz/j;)Z

    move-result v0

    .line 328
    if-eqz v0, :cond_62

    .line 330
    iget-boolean v1, p2, Lz/j;->e:Z

    if-eqz v1, :cond_63

    .line 331
    iget-object v1, p0, Lz/v;->n:[I

    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 332
    iget-object v1, p0, Lz/v;->n:[I

    aget v1, v1, v3

    invoke-static {v5, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 333
    iget-object v1, p0, Lz/v;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 334
    iget v1, p0, Lz/v;->b:I

    iget v2, p0, Lz/v;->c:I

    mul-int/2addr v1, v2

    iget-object v2, p0, Lz/v;->a:Ljava/nio/ByteBuffer;

    invoke-static {v5, v1, v2, v7}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 336
    invoke-static {v5, v3}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 337
    const-string v1, "InterleavedVertexData"

    const-string v2, "glBindBuffers"

    invoke-static {v1, v2}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    iget-object v1, p0, Lz/v;->l:Ljava/nio/ShortBuffer;

    if-eqz v1, :cond_62

    .line 340
    iget-object v1, p0, Lz/v;->o:[I

    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 341
    iget-object v1, p0, Lz/v;->o:[I

    aget v1, v1, v3

    invoke-static {v6, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 342
    iget-object v1, p0, Lz/v;->l:Ljava/nio/ShortBuffer;

    invoke-virtual {v1, v3}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 343
    iget v1, p0, Lz/v;->m:I

    mul-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lz/v;->l:Ljava/nio/ShortBuffer;

    invoke-static {v6, v1, v2, v7}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 345
    invoke-static {v6, v3}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 346
    const-string v1, "InterleavedVertexData"

    const-string v2, "glBindBuffers"

    invoke-static {v1, v2}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    :cond_62
    :goto_62
    return v0

    .line 349
    :cond_63
    iget-object v1, p0, Lz/v;->n:[I

    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    .line 350
    iget-object v1, p0, Lz/v;->n:[I

    aput v3, v1, v3

    .line 351
    iget-object v1, p0, Lz/v;->l:Ljava/nio/ShortBuffer;

    if-eqz v1, :cond_62

    .line 352
    iget-object v1, p0, Lz/v;->o:[I

    invoke-static {v4, v1, v3}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    .line 353
    iget-object v1, p0, Lz/v;->o:[I

    aput v3, v1, v3

    goto :goto_62
.end method
