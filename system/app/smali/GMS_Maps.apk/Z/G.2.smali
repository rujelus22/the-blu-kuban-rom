.class Lz/G;
.super Lz/F;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 3

    .prologue
    .line 221
    const-string v0, "uniform mat4 uMVPMatrix;\nuniform mat3 uTextureMatrix;\nuniform highp int uTextureMode;\nattribute vec4 aPosition;\nattribute vec2 aTextureCoord;\nattribute vec4 aColor;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  vTextureCoord = (uTextureMatrix * vec3(aTextureCoord, 1.0)).xy;\n  vColor = aColor;\n}\n"

    const-string v1, "precision mediump float;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nuniform sampler2D sTexture0;\nvoid main() {\n  gl_FragColor = vColor * texture2D(sTexture0, vTextureCoord);\n}\n"

    invoke-direct {p0, v0, v1}, Lz/F;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    return-void
.end method
