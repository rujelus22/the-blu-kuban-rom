.class public abstract Lz/D;
.super Lz/o;
.source "SourceFile"


# instance fields
.field protected g:Lz/C;

.field protected final h:Ljava/lang/Class;


# direct methods
.method protected constructor <init>(Ljava/lang/Class;)V
    .registers 3
    .parameter

    .prologue
    .line 66
    sget-object v0, Lz/p;->d:Lz/p;

    invoke-direct {p0, v0}, Lz/o;-><init>(Lz/p;)V

    .line 67
    iput-object p1, p0, Lz/D;->h:Ljava/lang/Class;

    .line 68
    return-void
.end method


# virtual methods
.method public a(Lz/k;Lz/o;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 104
    if-nez p2, :cond_d

    .line 105
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 107
    const-string v0, "ShaderState"

    const-string v1, "glUseProgram"

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_d
    return-void
.end method

.method public a(Lz/k;Lz/j;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-super {p0, p1, p2}, Lz/o;->a(Lz/k;Lz/j;)Z

    move-result v0

    .line 75
    iget-object v1, p0, Lz/D;->g:Lz/C;

    if-nez v1, :cond_14

    .line 76
    invoke-virtual {p1}, Lz/k;->d()Lz/B;

    move-result-object v1

    iget-object v2, p0, Lz/D;->h:Ljava/lang/Class;

    invoke-interface {v1, v2}, Lz/B;->a(Ljava/lang/Class;)Lz/C;

    move-result-object v1

    iput-object v1, p0, Lz/D;->g:Lz/C;

    .line 80
    :cond_14
    if-eqz v0, :cond_1b

    .line 81
    iget-object v1, p0, Lz/D;->g:Lz/C;

    invoke-virtual {v1, p1, p2}, Lz/C;->a(Lz/k;Lz/j;)Z

    .line 84
    :cond_1b
    return v0
.end method
