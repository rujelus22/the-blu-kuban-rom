.class public Lz/K;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:[F


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lz/K;->a:[F

    .line 21
    invoke-virtual {p0}, Lz/K;->a()Lz/K;

    .line 22
    return-void
.end method

.method public static a([F)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 222
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 223
    const/4 v0, 0x0

    :goto_6
    const/16 v2, 0x10

    if-ge v0, v2, :cond_2f

    .line 224
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget v3, p0, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 225
    rem-int/lit8 v2, v0, 0x4

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2c

    .line 226
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 223
    :cond_2c
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 230
    :cond_2f
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lz/K;
    .registers 3

    .prologue
    .line 44
    iget-object v0, p0, Lz/K;->a:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 46
    return-object p0
.end method

.method public a(Lz/K;)Lz/K;
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 131
    iget-object v0, p1, Lz/K;->a:[F

    iget-object v1, p0, Lz/K;->a:[F

    const/16 v2, 0x10

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 132
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 218
    iget-object v0, p0, Lz/K;->a:[F

    invoke-static {v0}, Lz/K;->a([F)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
