.class final enum LaR/aB;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LaR/aB;

.field public static final enum b:LaR/aB;

.field public static final enum c:LaR/aB;

.field private static final synthetic d:[LaR/aB;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    new-instance v0, LaR/aB;

    const-string v1, "EndofQuestion"

    invoke-direct {v0, v1, v2}, LaR/aB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaR/aB;->a:LaR/aB;

    .line 65
    new-instance v0, LaR/aB;

    const-string v1, "FollowupQuestion"

    invoke-direct {v0, v1, v3}, LaR/aB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaR/aB;->b:LaR/aB;

    .line 66
    new-instance v0, LaR/aB;

    const-string v1, "AltPhonesDialog"

    invoke-direct {v0, v1, v4}, LaR/aB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaR/aB;->c:LaR/aB;

    .line 63
    const/4 v0, 0x3

    new-array v0, v0, [LaR/aB;

    sget-object v1, LaR/aB;->a:LaR/aB;

    aput-object v1, v0, v2

    sget-object v1, LaR/aB;->b:LaR/aB;

    aput-object v1, v0, v3

    sget-object v1, LaR/aB;->c:LaR/aB;

    aput-object v1, v0, v4

    sput-object v0, LaR/aB;->d:[LaR/aB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaR/aB;
    .registers 2
    .parameter

    .prologue
    .line 63
    const-class v0, LaR/aB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaR/aB;

    return-object v0
.end method

.method public static values()[LaR/aB;
    .registers 1

    .prologue
    .line 63
    sget-object v0, LaR/aB;->d:[LaR/aB;

    invoke-virtual {v0}, [LaR/aB;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaR/aB;

    return-object v0
.end method
