.class public LaR/D;
.super LaR/t;
.source "SourceFile"


# static fields
.field private static final d:Ljava/util/regex/Pattern;


# instance fields
.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:LaN/B;

.field private h:Ljava/util/List;

.field private i:I

.field private j:Lo/o;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/Boolean;

.field private n:Ljava/lang/Boolean;

.field private o:Ljava/lang/Boolean;

.field private p:Ljava/lang/Boolean;

.field private q:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 38
    const-string v0, "(cid|ftid)=([:x0-9a-fA-F]+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LaR/D;->d:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 107
    invoke-direct {p0}, LaR/t;-><init>()V

    .line 45
    const/4 v0, -0x1

    iput v0, p0, LaR/D;->i:I

    .line 75
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LaR/D;->q:J

    .line 108
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 160
    invoke-direct {p0}, LaR/t;-><init>()V

    .line 45
    const/4 v0, -0x1

    iput v0, p0, LaR/D;->i:I

    .line 75
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LaR/D;->q:J

    .line 161
    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getType()Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-result-object v0

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gq;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    if-ne v0, v1, :cond_19

    .line 162
    invoke-direct {p0, p1}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 167
    :goto_18
    return-void

    .line 165
    :cond_19
    invoke-direct {p0, p1}, LaR/D;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_18
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 110
    invoke-direct {p0}, LaR/t;-><init>()V

    .line 45
    const/4 v0, -0x1

    iput v0, p0, LaR/D;->i:I

    .line 75
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LaR/D;->q:J

    .line 111
    iput-object p1, p0, LaR/D;->a:Ljava/lang/String;

    .line 112
    iget-object v0, p0, LaR/D;->a:Ljava/lang/String;

    invoke-static {v0}, LaR/D;->c(Ljava/lang/String;)Lo/o;

    move-result-object v0

    iput-object v0, p0, LaR/D;->j:Lo/o;

    .line 113
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->k:Ljava/lang/Boolean;

    .line 114
    iput-object p2, p0, LaR/D;->e:Ljava/lang/String;

    .line 115
    iput-object p3, p0, LaR/D;->f:Ljava/lang/String;

    .line 116
    invoke-static {p4}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    iput-object v0, p0, LaR/D;->g:LaN/B;

    .line 117
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, LaR/D;->h:Ljava/util/List;

    .line 118
    return-void
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 78
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 79
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 81
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x5

    .line 121
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaR/D;->a:Ljava/lang/String;

    .line 122
    iget-object v0, p0, LaR/D;->a:Ljava/lang/String;

    invoke-static {v0}, LaR/D;->c(Ljava/lang/String;)Lo/o;

    move-result-object v0

    iput-object v0, p0, LaR/D;->j:Lo/o;

    .line 123
    const/4 v0, 0x2

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->k:Ljava/lang/Boolean;

    .line 124
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaR/D;->e:Ljava/lang/String;

    .line 125
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaR/D;->f:Ljava/lang/String;

    .line 126
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 127
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    iput-object v0, p0, LaR/D;->g:LaN/B;

    .line 129
    :cond_35
    const/16 v0, 0x8

    const-wide/16 v1, -0x1

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v0

    iput-wide v0, p0, LaR/D;->b:J

    .line 131
    const/4 v0, 0x7

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 132
    invoke-static {v0}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lo/D;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LaR/D;->h:Ljava/util/List;

    .line 133
    const/16 v0, 0x9

    const-wide/16 v1, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v0

    iput-wide v0, p0, LaR/D;->c:J

    .line 135
    return-void
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 85
    if-eqz p2, :cond_9

    .line 86
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 88
    :cond_9
    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x3

    .line 138
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaR/D;->a:Ljava/lang/String;

    .line 139
    iget-object v0, p0, LaR/D;->a:Ljava/lang/String;

    invoke-static {v0}, LaR/D;->c(Ljava/lang/String;)Lo/o;

    move-result-object v0

    iput-object v0, p0, LaR/D;->j:Lo/o;

    .line 140
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaR/D;->e:Ljava/lang/String;

    .line 141
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 142
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    iput-object v0, p0, LaR/D;->g:LaN/B;

    .line 144
    :cond_27
    const/4 v0, 0x4

    const-wide/16 v1, -0x1

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v0

    iput-wide v0, p0, LaR/D;->b:J

    .line 146
    const/4 v0, 0x5

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, LaR/D;->i:I

    .line 148
    const/4 v0, 0x6

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->k:Ljava/lang/Boolean;

    .line 149
    const/4 v0, 0x7

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->l:Ljava/lang/Boolean;

    .line 150
    const/16 v0, 0x9

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->m:Ljava/lang/Boolean;

    .line 151
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 152
    invoke-static {v0}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lo/D;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LaR/D;->h:Ljava/util/List;

    .line 153
    const/16 v0, 0xd

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->n:Ljava/lang/Boolean;

    .line 154
    const/16 v0, 0x10

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->o:Ljava/lang/Boolean;

    .line 155
    const/16 v0, 0xb

    invoke-static {p1, v0}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->p:Ljava/lang/Boolean;

    .line 156
    const/16 v0, 0xc

    const-wide/16 v1, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v0

    iput-wide v0, p0, LaR/D;->c:J

    .line 158
    return-void
.end method

.method private static c(Ljava/lang/String;)Lo/o;
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 94
    sget-object v1, LaR/D;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 95
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 98
    const/4 v2, 0x2

    :try_start_e
    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lo/o;->a(Ljava/lang/String;)Lo/o;
    :try_end_15
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_15} :catch_17

    move-result-object v0

    .line 104
    :cond_16
    :goto_16
    return-object v0

    .line 99
    :catch_17
    move-exception v1

    goto :goto_16
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 175
    iget-object v0, p0, LaR/D;->e:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 239
    iput p1, p0, LaR/D;->i:I

    .line 240
    return-void
.end method

.method public a(LaN/B;)V
    .registers 2
    .parameter

    .prologue
    .line 231
    iput-object p1, p0, LaR/D;->g:LaN/B;

    .line 232
    return-void
.end method

.method public a(Ljava/util/List;)V
    .registers 2
    .parameter

    .prologue
    .line 235
    iput-object p1, p0, LaR/D;->h:Ljava/util/List;

    .line 236
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 243
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LaR/D;->k:Ljava/lang/Boolean;

    .line 244
    return-void
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 171
    iget-object v0, p0, LaR/D;->e:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 223
    iput-object p1, p0, LaR/D;->e:Ljava/lang/String;

    .line 224
    return-void
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 183
    iget-object v0, p0, LaR/D;->f:Ljava/lang/String;

    return-object v0
.end method

.method public d()LaN/B;
    .registers 2

    .prologue
    .line 187
    iget-object v0, p0, LaR/D;->g:LaN/B;

    return-object v0
.end method

.method public e()Ljava/util/List;
    .registers 2

    .prologue
    .line 191
    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    return-object v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 195
    iget v0, p0, LaR/D;->i:I

    return v0
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 199
    iget-object v0, p0, LaR/D;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    iget-object v0, p0, LaR/D;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public l()Z
    .registers 2

    .prologue
    .line 203
    iget-object v0, p0, LaR/D;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    iget-object v0, p0, LaR/D;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public m()Z
    .registers 2

    .prologue
    .line 207
    iget-object v0, p0, LaR/D;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    iget-object v0, p0, LaR/D;->n:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public n()Z
    .registers 2

    .prologue
    .line 215
    iget-object v0, p0, LaR/D;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    iget-object v0, p0, LaR/D;->o:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public o()Z
    .registers 2

    .prologue
    .line 219
    iget-object v0, p0, LaR/D;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    iget-object v0, p0, LaR/D;->p:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 7

    .prologue
    .line 267
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gq;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 268
    const/4 v0, 0x1

    iget-object v2, p0, LaR/D;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 269
    const/4 v0, 0x2

    iget-object v2, p0, LaR/D;->k:Ljava/lang/Boolean;

    invoke-static {v1, v0, v2}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V

    .line 270
    iget-object v0, p0, LaR/D;->e:Ljava/lang/String;

    if-eqz v0, :cond_1d

    .line 271
    const/4 v0, 0x3

    iget-object v2, p0, LaR/D;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 273
    :cond_1d
    iget-object v0, p0, LaR/D;->f:Ljava/lang/String;

    if-eqz v0, :cond_27

    .line 274
    const/4 v0, 0x4

    iget-object v2, p0, LaR/D;->f:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 276
    :cond_27
    iget-object v0, p0, LaR/D;->g:LaN/B;

    if-eqz v0, :cond_35

    .line 277
    const/4 v0, 0x5

    iget-object v2, p0, LaR/D;->g:LaN/B;

    invoke-static {v2}, LaN/C;->d(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 280
    :cond_35
    iget-wide v2, p0, LaR/D;->b:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_44

    .line 281
    const/16 v0, 0x8

    iget-wide v2, p0, LaR/D;->b:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 283
    :cond_44
    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    if-eqz v0, :cond_6b

    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6b

    .line 284
    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_56
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    .line 285
    const/4 v3, 0x7

    invoke-virtual {v0}, Lo/D;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_56

    .line 288
    :cond_6b
    iget-wide v2, p0, LaR/D;->c:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_7a

    .line 289
    const/16 v0, 0x9

    iget-wide v2, p0, LaR/D;->c:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 291
    :cond_7a
    return-object v1
.end method

.method public q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 296
    invoke-super {p0}, LaR/t;->k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 298
    iget-object v0, p0, LaR/D;->e:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 299
    const/4 v0, 0x2

    iget-object v2, p0, LaR/D;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 301
    :cond_e
    iget-object v0, p0, LaR/D;->g:LaN/B;

    if-eqz v0, :cond_1c

    .line 302
    const/4 v0, 0x3

    iget-object v2, p0, LaR/D;->g:LaN/B;

    invoke-static {v2}, LaN/C;->d(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 305
    :cond_1c
    iget v0, p0, LaR/D;->i:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_27

    .line 306
    const/4 v0, 0x5

    iget v2, p0, LaR/D;->i:I

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 308
    :cond_27
    const/4 v0, 0x6

    iget-object v2, p0, LaR/D;->k:Ljava/lang/Boolean;

    invoke-static {v1, v0, v2}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V

    .line 309
    const/4 v0, 0x7

    iget-object v2, p0, LaR/D;->l:Ljava/lang/Boolean;

    invoke-static {v1, v0, v2}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V

    .line 311
    iget-object v0, p0, LaR/D;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_42

    .line 312
    const/16 v0, 0xd

    iget-object v2, p0, LaR/D;->n:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 314
    :cond_42
    const/16 v0, 0x9

    iget-object v2, p0, LaR/D;->m:Ljava/lang/Boolean;

    invoke-static {v1, v0, v2}, LaR/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V

    .line 315
    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    if-eqz v0, :cond_71

    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_71

    .line 316
    iget-object v0, p0, LaR/D;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_71

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    .line 317
    const/16 v3, 0xa

    invoke-virtual {v0}, Lo/D;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_5b

    .line 320
    :cond_71
    iget-object v0, p0, LaR/D;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_80

    .line 321
    const/16 v0, 0x10

    iget-object v2, p0, LaR/D;->o:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 323
    :cond_80
    iget-object v0, p0, LaR/D;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_8f

    .line 324
    const/16 v0, 0xb

    iget-object v2, p0, LaR/D;->p:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 328
    :cond_8f
    return-object v1
.end method

.method public r()J
    .registers 3

    .prologue
    .line 337
    iget-wide v0, p0, LaR/D;->q:J

    return-wide v0
.end method
