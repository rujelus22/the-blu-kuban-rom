.class public LaR/L;
.super LaR/t;
.source "SourceFile"


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;J)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1, p5, p6}, LaR/t;-><init>(Ljava/lang/String;J)V

    .line 32
    iput-wide p2, p0, LaR/L;->c:J

    .line 33
    iput-object p4, p0, LaR/L;->d:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/L;
    .registers 8
    .parameter

    .prologue
    .line 39
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 40
    const/16 v0, 0xc

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v2

    .line 42
    const/4 v0, 0x4

    const-wide/16 v4, -0x1

    invoke-static {p0, v0, v4, v5}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v5

    .line 44
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v4

    .line 46
    new-instance v0, LaR/L;

    invoke-direct/range {v0 .. v6}, LaR/L;-><init>(Ljava/lang/String;JLjava/lang/String;J)V

    .line 48
    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4

    .prologue
    .line 53
    invoke-virtual {p0}, LaR/L;->k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 54
    iget-object v1, p0, LaR/L;->d:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 55
    const/4 v1, 0x2

    iget-object v2, p0, LaR/L;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 57
    :cond_e
    return-object v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 66
    iget-object v0, p0, LaR/L;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 61
    iget-object v0, p0, LaR/L;->d:Ljava/lang/String;

    return-object v0
.end method
