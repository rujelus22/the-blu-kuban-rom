.class public LV/h;
.super LU/b;
.source "SourceFile"


# instance fields
.field private final a:LX/a;

.field private final b:Landroid/content/Context;

.field private final c:LX/b;

.field private final d:Lcom/google/common/collect/di;

.field private final e:LU/T;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LU/T;Landroid/content/Context;Lcom/google/common/collect/di;LX/a;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    const-string v0, "playback_signal_provider"

    invoke-direct {p0, v0, p1}, LU/b;-><init>(Ljava/lang/String;LU/T;)V

    .line 89
    new-instance v0, LV/i;

    invoke-direct {v0, p0}, LV/i;-><init>(LV/h;)V

    iput-object v0, p0, LV/h;->f:Ljava/lang/Runnable;

    .line 56
    iput-object p2, p0, LV/h;->b:Landroid/content/Context;

    .line 57
    iput-object p1, p0, LV/h;->e:LU/T;

    .line 58
    iput-object p3, p0, LV/h;->d:Lcom/google/common/collect/di;

    .line 59
    iput-object p4, p0, LV/h;->a:LX/a;

    .line 60
    new-instance v0, LX/b;

    invoke-direct {v0, p4}, LX/b;-><init>(LX/a;)V

    iput-object v0, p0, LV/h;->c:LX/b;

    .line 61
    invoke-virtual {p4}, LX/a;->b()J

    move-result-wide v0

    .line 69
    invoke-direct {p0, v0, v1}, LV/h;->a(J)V

    .line 70
    return-void
.end method

.method static synthetic a(LV/h;)Lcom/google/common/collect/di;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LV/h;->d:Lcom/google/common/collect/di;

    return-object v0
.end method

.method private a(J)V
    .registers 5
    .parameter

    .prologue
    .line 77
    :cond_0
    iget-object v0, p0, LV/h;->d:Lcom/google/common/collect/di;

    invoke-interface {v0}, Lcom/google/common/collect/di;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, LV/h;->d:Lcom/google/common/collect/di;

    invoke-interface {v0}, Lcom/google/common/collect/di;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LV/g;

    iget-wide v0, v0, LV/g;->c:J

    cmp-long v0, v0, p1

    if-ltz v0, :cond_0

    .line 80
    :cond_16
    return-void
.end method

.method static synthetic b(LV/h;)Ljava/lang/Runnable;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LV/h;->f:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic c(LV/h;)LX/a;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LV/h;->a:LX/a;

    return-object v0
.end method

.method static synthetic d(LV/h;)LU/T;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LV/h;->e:LU/T;

    return-object v0
.end method

.method static synthetic e(LV/h;)LX/b;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LV/h;->c:LX/b;

    return-object v0
.end method

.method static synthetic f(LV/h;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LV/h;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public b()V
    .registers 3

    .prologue
    .line 155
    invoke-virtual {p0}, LV/h;->e()Z

    move-result v0

    if-nez v0, :cond_10

    .line 156
    invoke-super {p0}, LU/b;->b()V

    .line 157
    iget-object v0, p0, LV/h;->c:LX/b;

    iget-object v1, p0, LV/h;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LX/b;->b(Ljava/lang/Runnable;)Z

    .line 159
    :cond_10
    return-void
.end method

.method public d()V
    .registers 4

    .prologue
    .line 163
    iget-object v1, p0, LV/h;->f:Ljava/lang/Runnable;

    monitor-enter v1

    .line 164
    :try_start_3
    iget-object v0, p0, LV/h;->c:LX/b;

    iget-object v2, p0, LV/h;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, LX/b;->a(Ljava/lang/Runnable;)V

    .line 169
    invoke-super {p0}, LU/b;->d()V

    .line 170
    monitor-exit v1

    .line 171
    return-void

    .line 170
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method
