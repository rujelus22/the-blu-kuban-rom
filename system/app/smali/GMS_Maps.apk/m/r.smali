.class LM/r;
.super LM/e;
.source "SourceFile"


# instance fields
.field final synthetic c:LM/n;

.field private d:Z

.field private e:I

.field private f:Z

.field private g:LaH/h;


# direct methods
.method constructor <init>(LM/n;LM/b;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 574
    iput-object p1, p0, LM/r;->c:LM/n;

    .line 575
    const-string v0, "driveabout_base_location"

    invoke-direct {p0, v0, p2}, LM/e;-><init>(Ljava/lang/String;LM/b;)V

    .line 576
    invoke-static {p1}, LM/n;->a(LM/n;)LM/v;

    move-result-object v0

    invoke-interface {v0}, LM/v;->a()Ljava/util/List;

    move-result-object v0

    const-string v2, "gps"

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-static {p1}, LM/n;->a(LM/n;)LM/v;

    move-result-object v0

    const-string v2, "gps"

    invoke-interface {v0, v2}, LM/v;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    move v0, v1

    :goto_25
    iput-boolean v0, p0, LM/r;->d:Z

    .line 581
    iput v1, p0, LM/r;->e:I

    .line 582
    return-void

    .line 576
    :cond_2a
    const/4 v0, 0x0

    goto :goto_25
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 587
    iget-object v0, p0, LM/r;->c:LM/n;

    invoke-static {v0}, LM/n;->a(LM/n;)LM/v;

    move-result-object v0

    invoke-interface {v0}, LM/v;->a()Ljava/util/List;

    move-result-object v0

    const-string v1, "gps"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, LM/r;->c:LM/n;

    invoke-static {v0}, LM/n;->a(LM/n;)LM/v;

    move-result-object v0

    const-string v1, "gps"

    invoke-interface {v0, v1}, LM/v;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    const/4 v0, 0x1

    :goto_21
    iput-boolean v0, p0, LM/r;->d:Z

    .line 590
    return-void

    .line 587
    :cond_24
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 593
    iget-boolean v0, p0, LM/r;->f:Z

    return v0
.end method

.method public c()Z
    .registers 3

    .prologue
    .line 597
    iget-boolean v0, p0, LM/r;->d:Z

    if-eqz v0, :cond_b

    iget v0, p0, LM/r;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 603
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    .line 604
    const-string v2, "network"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 606
    invoke-virtual {p0}, LM/r;->c()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 639
    :cond_13
    :goto_13
    return-void

    .line 610
    :cond_14
    const-string v2, "driveabout_gps_fixup"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 613
    iput-boolean v1, p0, LM/r;->d:Z

    .line 614
    const/4 v0, 0x2

    iput v0, p0, LM/r;->e:I

    .line 617
    iget-boolean v0, p0, LM/r;->f:Z

    if-eqz v0, :cond_5f

    invoke-static {}, LM/n;->b()F

    move-result v0

    .line 619
    :goto_29
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v2

    if-eqz v2, :cond_64

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_64

    move v0, v1

    .line 621
    :goto_38
    if-nez v0, :cond_3e

    iget-boolean v2, p0, LM/r;->f:Z

    if-nez v2, :cond_13

    .line 628
    :cond_3e
    iput-boolean v0, p0, LM/r;->f:Z

    .line 631
    new-instance v0, LaH/j;

    invoke-direct {v0}, LaH/j;-><init>()V

    invoke-virtual {v0, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    invoke-virtual {v0, v1}, LaH/j;->a(Z)LaH/j;

    move-result-object v0

    iget-boolean v1, p0, LM/r;->f:Z

    invoke-virtual {v0, v1}, LaH/j;->b(Z)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    iput-object v0, p0, LM/r;->g:LaH/h;

    .line 636
    iget-object p1, p0, LM/r;->g:LaH/h;

    .line 638
    :cond_5b
    invoke-super {p0, p1}, LM/e;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_13

    .line 617
    :cond_5f
    invoke-static {}, LM/n;->a()F

    move-result v0

    goto :goto_29

    .line 619
    :cond_64
    const/4 v0, 0x0

    goto :goto_38
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 643
    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 644
    iput-boolean v1, p0, LM/r;->d:Z

    .line 645
    iput-boolean v1, p0, LM/r;->f:Z

    .line 647
    :cond_d
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 651
    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 652
    const/4 v0, 0x1

    iput-boolean v0, p0, LM/r;->d:Z

    .line 654
    :cond_b
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 662
    const-string v0, "gps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 663
    iput p2, p0, LM/r;->e:I

    .line 664
    const/4 v0, 0x2

    if-eq p2, v0, :cond_10

    .line 665
    const/4 v0, 0x0

    iput-boolean v0, p0, LM/r;->f:Z

    .line 668
    :cond_10
    return-void
.end method
