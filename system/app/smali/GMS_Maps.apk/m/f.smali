.class public LM/f;
.super LM/e;
.source "SourceFile"


# instance fields
.field private c:J

.field private d:J

.field private volatile e:LM/t;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:LR/m;

.field private k:LM/v;

.field private final l:Lcom/google/googlenav/common/a;


# direct methods
.method public constructor <init>(LM/b;LM/v;Lcom/google/googlenav/common/a;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    const-string v0, "driveabout_gps_fixup"

    invoke-direct {p0, v0, p1}, LM/e;-><init>(Ljava/lang/String;LM/b;)V

    .line 37
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LM/f;->c:J

    .line 40
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LM/f;->d:J

    .line 66
    iput-object p2, p0, LM/f;->k:LM/v;

    .line 67
    iput-object p3, p0, LM/f;->l:Lcom/google/googlenav/common/a;

    .line 68
    return-void
.end method

.method private a(LaH/j;)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 134
    iget-boolean v1, p0, LM/f;->h:Z

    if-nez v1, :cond_16

    invoke-virtual {p1}, LaH/j;->e()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-virtual {p1}, LaH/j;->f()F

    move-result v1

    cmpl-float v1, v1, v0

    if-lez v1, :cond_16

    .line 135
    const/4 v1, 0x1

    iput-boolean v1, p0, LM/f;->h:Z

    .line 137
    :cond_16
    iget-boolean v1, p0, LM/f;->h:Z

    if-nez v1, :cond_41

    invoke-virtual {p1}, LaH/j;->g()Z

    move-result v1

    if-eqz v1, :cond_41

    .line 139
    invoke-virtual {p1}, LaH/j;->h()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    .line 140
    iget-object v2, p0, LM/f;->j:LR/m;

    invoke-virtual {v2}, LR/m;->n()I

    move-result v2

    int-to-float v2, v2

    .line 141
    const/high16 v3, 0x4100

    .line 142
    if-ltz v1, :cond_41

    cmpl-float v4, v2, v3

    if-lez v4, :cond_41

    .line 144
    packed-switch v1, :pswitch_data_52

    .line 152
    :goto_38
    sub-float v1, v2, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, LaH/j;->a(F)LaH/j;

    .line 155
    :cond_41
    return-void

    .line 145
    :pswitch_42
    const/high16 v0, 0x3f80

    goto :goto_38

    .line 146
    :pswitch_45
    const/high16 v0, 0x3f40

    goto :goto_38

    .line 147
    :pswitch_48
    const/high16 v0, 0x3f00

    goto :goto_38

    .line 148
    :pswitch_4b
    const/high16 v0, 0x3e80

    goto :goto_38

    .line 149
    :pswitch_4e
    const/high16 v0, 0x3e00

    goto :goto_38

    .line 144
    nop

    :pswitch_data_52
    .packed-switch 0x0
        :pswitch_42
        :pswitch_45
        :pswitch_48
        :pswitch_4b
        :pswitch_4e
    .end packed-switch
.end method

.method private b(LaH/j;)V
    .registers 5
    .parameter

    .prologue
    .line 162
    invoke-virtual {p1}, LaH/j;->e()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 163
    const/high16 v0, 0x4080

    invoke-virtual {p1}, LaH/j;->f()F

    move-result v1

    iget-object v2, p0, LM/f;->j:LR/m;

    invoke-virtual {v2}, LR/m;->o()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {p1, v0}, LaH/j;->a(F)LaH/j;

    .line 166
    :cond_1a
    return-void
.end method

.method private c(LaH/j;)V
    .registers 4
    .parameter

    .prologue
    .line 173
    iget-boolean v0, p0, LM/f;->g:Z

    if-nez v0, :cond_16

    invoke-virtual {p1}, LaH/j;->i()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p1}, LaH/j;->j()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_16

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, LM/f;->g:Z

    .line 176
    :cond_16
    iget-boolean v0, p0, LM/f;->g:Z

    if-nez v0, :cond_1d

    .line 177
    invoke-virtual {p1}, LaH/j;->a()LaH/j;

    .line 179
    :cond_1d
    return-void
.end method

.method private d(LaH/j;)V
    .registers 4
    .parameter

    .prologue
    .line 186
    iget-boolean v0, p0, LM/f;->g:Z

    if-nez v0, :cond_12

    .line 187
    iget-object v0, p0, LM/f;->k:LM/v;

    invoke-interface {v0}, LM/v;->d()F

    move-result v0

    .line 188
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_12

    .line 189
    invoke-virtual {p1, v0}, LaH/j;->b(F)LaH/j;

    .line 192
    :cond_12
    return-void
.end method

.method private e(LaH/j;)V
    .registers 3
    .parameter

    .prologue
    .line 196
    iget-object v0, p0, LM/f;->e:LM/t;

    if-eqz v0, :cond_d

    .line 197
    iget-object v0, p0, LM/f;->e:LM/t;

    invoke-virtual {v0}, LM/t;->b()I

    move-result v0

    invoke-virtual {p1, v0}, LaH/j;->a(I)LaH/j;

    .line 199
    :cond_d
    return-void
.end method

.method private f(LaH/j;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 205
    invoke-virtual {p1}, LaH/j;->g()Z

    move-result v2

    if-nez v2, :cond_a

    .line 212
    :goto_9
    return v1

    .line 208
    :cond_a
    invoke-virtual {p1}, LaH/j;->h()I

    move-result v2

    .line 209
    if-lt v2, v4, :cond_12

    .line 210
    iput-boolean v0, p0, LM/f;->f:Z

    .line 212
    :cond_12
    iget-boolean v3, p0, LM/f;->f:Z

    if-eqz v3, :cond_1a

    if-ge v2, v4, :cond_1a

    :goto_18
    move v1, v0

    goto :goto_9

    :cond_1a
    move v0, v1

    goto :goto_18
.end method

.method private g(LaH/j;)V
    .registers 4
    .parameter

    .prologue
    .line 221
    iget-boolean v0, p0, LM/f;->f:Z

    if-eqz v0, :cond_2f

    invoke-virtual {p1}, LaH/j;->g()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-virtual {p1}, LaH/j;->h()I

    move-result v0

    iget-object v1, p0, LM/f;->j:LR/m;

    invoke-virtual {v1}, LR/m;->m()I

    move-result v1

    if-ge v0, v1, :cond_2f

    invoke-virtual {p1}, LaH/j;->f()F

    move-result v0

    iget-object v1, p0, LM/f;->j:LR/m;

    invoke-virtual {v1}, LR/m;->n()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2f

    .line 226
    iget-object v0, p0, LM/f;->j:LR/m;

    invoke-virtual {v0}, LR/m;->n()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, LaH/j;->a(F)LaH/j;

    .line 228
    :cond_2f
    return-void
.end method

.method private h(LaH/j;)V
    .registers 4
    .parameter

    .prologue
    .line 236
    iget-boolean v0, p0, LM/f;->i:Z

    if-eqz v0, :cond_21

    invoke-virtual {p1}, LaH/j;->d()LaH/h;

    move-result-object v0

    invoke-virtual {v0}, LaH/h;->getAccuracy()F

    move-result v0

    iget-object v1, p0, LM/f;->j:LR/m;

    invoke-virtual {v1}, LR/m;->n()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_21

    .line 237
    iget-object v0, p0, LM/f;->j:LR/m;

    invoke-virtual {v0}, LR/m;->n()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, LaH/j;->a(F)LaH/j;

    .line 239
    :cond_21
    return-void
.end method

.method private i(LaH/j;)V
    .registers 4
    .parameter

    .prologue
    .line 247
    invoke-virtual {p1}, LaH/j;->k()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-virtual {p1}, LaH/j;->l()F

    move-result v0

    const/high16 v1, 0x42c8

    cmpl-float v0, v0, v1

    if-lez v0, :cond_13

    .line 248
    invoke-virtual {p1}, LaH/j;->b()LaH/j;

    .line 250
    :cond_13
    return-void
.end method

.method private j(LaH/j;)V
    .registers 8
    .parameter

    .prologue
    .line 261
    invoke-virtual {p1}, LaH/j;->f()F

    move-result v0

    iget-object v1, p0, LM/f;->j:LR/m;

    invoke-virtual {v1}, LR/m;->n()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_4b

    .line 264
    iget-object v0, p0, LM/f;->l:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->d()J

    move-result-wide v0

    .line 265
    iget-wide v2, p0, LM/f;->c:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_37

    .line 266
    iget-wide v2, p0, LM/f;->c:J

    sub-long v2, v0, v2

    .line 267
    iget-object v4, p0, LM/f;->j:LR/m;

    invoke-virtual {v4}, LR/m;->k()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_37

    .line 268
    const-wide/16 v2, 0x1388

    add-long/2addr v2, v0

    iget-wide v4, p0, LM/f;->d:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, LM/f;->d:J

    .line 273
    :cond_37
    iput-wide v0, p0, LM/f;->c:J

    .line 276
    iget-wide v2, p0, LM/f;->d:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_4b

    .line 277
    iget-object v0, p0, LM/f;->j:LR/m;

    invoke-virtual {v0}, LR/m;->n()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    int-to-float v0, v0

    invoke-virtual {p1, v0}, LaH/j;->a(F)LaH/j;

    .line 280
    :cond_4b
    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 90
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LM/f;->c:J

    .line 91
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LM/f;->d:J

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, LM/f;->e:LM/t;

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, LM/f;->i:Z

    .line 94
    return-void
.end method

.method public a(LM/t;)V
    .registers 2
    .parameter

    .prologue
    .line 84
    iput-object p1, p0, LM/f;->e:LM/t;

    .line 85
    return-void
.end method

.method public a(LM/v;)V
    .registers 2
    .parameter

    .prologue
    .line 76
    iput-object p1, p0, LM/f;->k:LM/v;

    .line 77
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .registers 4
    .parameter

    .prologue
    .line 104
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v1, "driveabout_hmm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 105
    invoke-static {p1}, LM/E;->a(Landroid/location/Location;)Z

    move-result v0

    iput-boolean v0, p0, LM/f;->i:Z

    .line 126
    :cond_12
    :goto_12
    return-void

    .line 108
    :cond_13
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    iput-object v0, p0, LM/f;->j:LR/m;

    .line 109
    new-instance v0, LaH/j;

    invoke-direct {v0}, LaH/j;-><init>()V

    invoke-virtual {v0, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaH/j;->a(Z)LaH/j;

    move-result-object v0

    .line 112
    invoke-direct {p0, v0}, LM/f;->e(LaH/j;)V

    .line 113
    invoke-direct {p0, v0}, LM/f;->a(LaH/j;)V

    .line 114
    invoke-direct {p0, v0}, LM/f;->b(LaH/j;)V

    .line 115
    invoke-direct {p0, v0}, LM/f;->c(LaH/j;)V

    .line 116
    invoke-direct {p0, v0}, LM/f;->d(LaH/j;)V

    .line 117
    invoke-direct {p0, v0}, LM/f;->f(LaH/j;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 120
    invoke-direct {p0, v0}, LM/f;->g(LaH/j;)V

    .line 121
    invoke-direct {p0, v0}, LM/f;->h(LaH/j;)V

    .line 122
    invoke-direct {p0, v0}, LM/f;->i(LaH/j;)V

    .line 123
    invoke-direct {p0, v0}, LM/f;->j(LaH/j;)V

    .line 125
    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    invoke-super {p0, v0}, LM/e;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_12
.end method
