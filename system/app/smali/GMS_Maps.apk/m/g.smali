.class public LM/g;
.super LM/e;
.source "SourceFile"

# interfaces
.implements LM/c;


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field c:Ljava/util/ArrayList;

.field private final e:Lq/e;

.field private final f:Ljava/util/ArrayList;

.field private g:LM/i;

.field private h:LO/z;

.field private i:I

.field private j:D


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 69
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "  "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "    "

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "      "

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "        "

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "          "

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "            "

    aput-object v2, v0, v1

    sput-object v0, LM/g;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LM/b;)V
    .registers 4
    .parameter

    .prologue
    .line 139
    new-instance v0, Lq/e;

    sget-object v1, LA/c;->b:LA/c;

    invoke-static {v1}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v1

    invoke-direct {v0, v1}, Lq/e;-><init>(Lr/z;)V

    invoke-direct {p0, p1, v0}, LM/g;-><init>(LM/b;Lq/e;)V

    .line 141
    return-void
.end method

.method constructor <init>(LM/b;Lq/e;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 148
    const-string v0, "driveabout_hmm"

    invoke-direct {p0, v0, p1}, LM/e;-><init>(Ljava/lang/String;LM/b;)V

    .line 127
    const/4 v0, 0x0

    iput v0, p0, LM/g;->i:I

    .line 131
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    invoke-virtual {v0}, LR/m;->b()D

    move-result-wide v0

    iput-wide v0, p0, LM/g;->j:D

    .line 149
    iput-object p2, p0, LM/g;->e:Lq/e;

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LM/g;->c:Ljava/util/ArrayList;

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LM/g;->f:Ljava/util/ArrayList;

    .line 155
    return-void
.end method

.method static a(D)D
    .registers 4
    .parameter

    .prologue
    .line 1021
    const-wide v0, 0x3fb015bf9217271aL

    mul-double/2addr v0, p0

    return-wide v0
.end method

.method private a(LM/h;Lq/a;LM/i;)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    .line 404
    iget-object v0, p1, LM/h;->f:Lq/c;

    invoke-virtual {p2, v0}, Lq/a;->a(Lq/c;)Lq/c;

    move-result-object v1

    .line 406
    if-nez v1, :cond_a

    .line 429
    :cond_9
    return-void

    .line 414
    :cond_a
    invoke-virtual {v1}, Lq/c;->c()Lo/T;

    move-result-object v0

    invoke-virtual {v1}, Lq/c;->d()Lo/T;

    move-result-object v2

    iget-object v3, p1, LM/h;->j:Lo/T;

    invoke-static {v0, v2, v3}, Lo/T;->d(Lo/T;Lo/T;Lo/T;)F

    move-result v0

    float-to-double v2, v0

    .line 419
    const/4 v0, 0x7

    new-array v10, v0, [Lq/c;

    .line 420
    const-wide/16 v4, 0x0

    invoke-virtual {p1}, LM/h;->a()D

    move-result-wide v6

    move-object v0, p0

    move-object v8, p3

    invoke-direct/range {v0 .. v10}, LM/g;->a(Lq/c;DDDLM/i;I[Lq/c;)V

    .line 425
    iget-object v0, p0, LM/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 426
    :goto_2d
    if-ge v9, v1, :cond_9

    .line 427
    iget-object v0, p0, LM/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/h;

    invoke-virtual {v0}, LM/h;->b()V

    .line 426
    add-int/lit8 v9, v9, 0x1

    goto :goto_2d
.end method

.method private a(LaH/j;LM/i;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 212
    iget-object v0, p0, LM/g;->h:LO/z;

    if-eqz v0, :cond_23

    .line 213
    invoke-virtual {p1}, LaH/j;->m()D

    move-result-wide v0

    invoke-virtual {p1}, LaH/j;->n()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lo/T;->a(DD)Lo/T;

    move-result-object v0

    .line 214
    const-wide v1, 0x40f86a0000000000L

    invoke-virtual {p2}, LM/i;->a()D

    move-result-wide v3

    mul-double/2addr v1, v3

    .line 215
    iget-object v3, p0, LM/g;->h:LO/z;

    invoke-virtual {v3, v0, v1, v2}, LO/z;->a(Lo/T;D)LO/H;

    move-result-object v0

    invoke-virtual {p1, v0}, LaH/j;->a(LO/H;)LaH/j;

    .line 217
    :cond_23
    return-void
.end method

.method private a(LaH/j;LM/i;LR/m;)V
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    .line 240
    invoke-virtual {p1}, LaH/j;->p()Z

    move-result v0

    if-nez v0, :cond_7

    .line 384
    :cond_6
    :goto_6
    return-void

    .line 243
    :cond_7
    iput-object p2, p0, LM/g;->g:LM/i;

    .line 246
    iget v0, p0, LM/g;->i:I

    invoke-virtual {p1, v0}, LaH/j;->b(I)LaH/j;

    .line 249
    iget-wide v0, p2, LM/i;->a:D

    invoke-virtual {p0, p1, p3, v0, v1}, LM/g;->a(LaH/j;LR/m;D)Lq/d;

    move-result-object v0

    .line 250
    if-nez v0, :cond_1a

    .line 251
    invoke-direct {p0, p1, p2}, LM/g;->b(LaH/j;LM/i;)V

    goto :goto_6

    .line 254
    :cond_1a
    new-instance v2, Lq/a;

    invoke-virtual {p3}, LR/m;->e()I

    move-result v1

    iget v3, p0, LM/g;->i:I

    invoke-direct {v2, v0, v1, v3}, Lq/a;-><init>(Lq/d;II)V

    .line 257
    iget-object v0, p0, LM/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 258
    invoke-virtual {v2}, Lq/a;->a()I

    move-result v7

    .line 259
    if-nez v7, :cond_34

    .line 260
    invoke-direct {p0, p1, p2}, LM/g;->b(LaH/j;LM/i;)V

    goto :goto_6

    .line 263
    :cond_34
    const/4 v0, 0x0

    :goto_35
    if-ge v0, v7, :cond_4f

    .line 264
    new-instance v1, LM/h;

    invoke-virtual {v2, v0}, Lq/a;->a(I)Lq/c;

    move-result-object v3

    invoke-direct {v1, v0, v3, p2, p3}, LM/h;-><init>(ILq/c;LM/i;LR/m;)V

    .line 265
    invoke-virtual {v2, v0}, Lq/a;->a(I)Lq/c;

    move-result-object v3

    invoke-virtual {v3, v1}, Lq/c;->a(Ljava/lang/Object;)V

    .line 266
    iget-object v3, p0, LM/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    add-int/lit8 v0, v0, 0x1

    goto :goto_35

    .line 274
    :cond_4f
    iget-boolean v0, p2, LM/i;->i:Z

    if-eqz v0, :cond_81

    iget-wide v0, p2, LM/i;->c:D

    const-wide/high16 v3, 0x4024

    cmpg-double v0, v0, v3

    if-gez v0, :cond_81

    iget-object v0, p0, LM/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_81

    iget v0, p0, LM/g;->i:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_81

    .line 276
    const/4 v0, 0x0

    move v1, v0

    :goto_6a
    iget-object v0, p0, LM/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_94

    .line 277
    iget-object v0, p0, LM/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/h;

    invoke-direct {p0, v0, v2, p2}, LM/g;->a(LM/h;Lq/a;LM/i;)V

    .line 276
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6a

    .line 281
    :cond_81
    const/4 v0, 0x0

    move v1, v0

    :goto_83
    if-ge v1, v7, :cond_94

    .line 282
    iget-object v0, p0, LM/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/h;

    invoke-virtual {v0}, LM/h;->c()V

    .line 281
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_83

    .line 287
    :cond_94
    const/4 v0, 0x0

    move v1, v0

    :goto_96
    if-ge v1, v7, :cond_a8

    .line 288
    iget-object v0, p0, LM/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/h;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LM/h;->a(Z)D

    .line 287
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_96

    .line 290
    :cond_a8
    iget-object v0, p0, LM/g;->c:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 293
    const-wide/16 v3, 0x0

    .line 294
    const-wide/16 v1, 0x0

    .line 295
    const/4 v0, 0x0

    move-wide v5, v3

    move-wide v3, v1

    move v2, v0

    :goto_b5
    if-ge v2, v7, :cond_d7

    .line 296
    iget-object v0, p0, LM/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/h;

    .line 300
    invoke-virtual {p3}, LR/m;->c()I

    move-result v1

    sub-int v1, v7, v1

    if-lt v2, v1, :cond_d5

    const/4 v1, 0x1

    .line 302
    :goto_c8
    invoke-virtual {v0, v1}, LM/h;->a(Z)D

    move-result-wide v8

    .line 303
    add-double/2addr v5, v8

    .line 304
    iget-wide v0, v0, LM/h;->b:D

    mul-double/2addr v0, v8

    add-double/2addr v3, v0

    .line 295
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_b5

    .line 300
    :cond_d5
    const/4 v1, 0x0

    goto :goto_c8

    .line 309
    :cond_d7
    const-wide/16 v0, 0x0

    const-wide/high16 v8, 0x3ff0

    sub-double/2addr v8, v5

    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 310
    const-wide v8, 0x3fe999999999999aL

    iget-wide v10, p0, LM/g;->j:D

    mul-double/2addr v8, v10

    const-wide v10, 0x3fc999999999999aL

    mul-double/2addr v0, v10

    add-double/2addr v0, v8

    iput-wide v0, p0, LM/g;->j:D

    .line 316
    iget-object v0, p0, LM/g;->c:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 317
    iget-object v0, p0, LM/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 318
    invoke-virtual {p3}, LR/m;->c()I

    move-result v0

    invoke-static {v7, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 320
    const/4 v0, 0x0

    move v1, v0

    :goto_105
    if-ge v1, v2, :cond_122

    .line 321
    iget-object v0, p0, LM/g;->c:Ljava/util/ArrayList;

    sub-int v8, v7, v1

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/h;

    .line 322
    invoke-virtual {v0, v5, v6}, LM/h;->a(D)V

    .line 327
    invoke-virtual {v0}, LM/h;->a()D

    move-result-wide v8

    invoke-virtual {p3}, LR/m;->a()D

    move-result-wide v10

    cmpg-double v8, v8, v10

    if-gtz v8, :cond_16d

    .line 341
    :cond_122
    iget-object v0, p0, LM/g;->c:Ljava/util/ArrayList;

    add-int/lit8 v1, v7, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/h;

    .line 343
    iget-wide v1, p0, LM/g;->j:D

    invoke-virtual {p3}, LR/m;->b()D

    move-result-wide v7

    cmpl-double v1, v1, v7

    if-lez v1, :cond_176

    .line 344
    iget-object v1, v0, LM/h;->f:Lq/c;

    invoke-virtual {v1}, Lq/c;->b()Lo/af;

    move-result-object v1

    iget-object v2, v0, LM/h;->j:Lo/T;

    invoke-virtual {p1, v1, v2}, LaH/j;->a(Lo/af;Lo/T;)LaH/j;

    .line 345
    invoke-virtual {p1}, LaH/j;->i()Z

    move-result v1

    if-nez v1, :cond_151

    iget-boolean v1, p2, LM/i;->f:Z

    if-eqz v1, :cond_151

    .line 346
    iget-wide v1, p2, LM/i;->g:D

    double-to-float v1, v1

    invoke-virtual {p1, v1}, LaH/j;->b(F)LaH/j;

    .line 348
    :cond_151
    iget-object v1, v0, LM/h;->h:LO/D;

    if-eqz v1, :cond_6

    .line 351
    iget-object v1, p2, LM/i;->d:Lo/T;

    iget-object v2, v0, LM/h;->h:LO/D;

    invoke-virtual {v2}, LO/D;->b()Lo/T;

    move-result-object v2

    invoke-virtual {v1, v2}, Lo/T;->c(Lo/T;)F

    move-result v1

    float-to-double v1, v1

    .line 353
    iget-object v0, v0, LM/h;->h:LO/D;

    invoke-virtual {v0, v1, v2}, LO/D;->a(D)LO/D;

    move-result-object v0

    invoke-virtual {p1, v0}, LaH/j;->a(LO/H;)LaH/j;

    goto/16 :goto_6

    .line 331
    :cond_16d
    iget-object v8, p0, LM/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_105

    .line 361
    :cond_176
    iget v1, p0, LM/g;->i:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_18a

    .line 362
    iget-object v1, v0, LM/h;->j:Lo/T;

    invoke-virtual {v1}, Lo/T;->b()D

    move-result-wide v1

    iget-object v7, v0, LM/h;->j:Lo/T;

    invoke-virtual {v7}, Lo/T;->d()D

    move-result-wide v7

    invoke-virtual {p1, v1, v2, v7, v8}, LaH/j;->a(DD)LaH/j;

    .line 364
    :cond_18a
    iget-boolean v1, p2, LM/i;->f:Z

    if-eqz v1, :cond_197

    .line 365
    iget-object v1, v0, LM/h;->f:Lq/c;

    invoke-virtual {v1}, Lq/c;->e()F

    move-result v1

    invoke-virtual {p1, v1}, LaH/j;->b(F)LaH/j;

    .line 367
    :cond_197
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LaH/j;->c(Z)LaH/j;

    move-result-object v1

    iget-object v2, v0, LM/h;->f:Lq/c;

    invoke-virtual {v2}, Lq/c;->b()Lo/af;

    move-result-object v2

    iget-object v7, v0, LM/h;->j:Lo/T;

    invoke-virtual {v1, v2, v7}, LaH/j;->a(Lo/af;Lo/T;)LaH/j;

    move-result-object v1

    iget-object v0, v0, LM/h;->h:LO/D;

    invoke-virtual {v1, v0}, LaH/j;->a(LO/H;)LaH/j;

    .line 371
    iget-object v0, p0, LM/g;->h:LO/z;

    if-eqz v0, :cond_6

    const-wide/16 v0, 0x0

    cmpl-double v0, v5, v0

    if-lez v0, :cond_6

    .line 372
    div-double v0, v3, v5

    invoke-virtual {p1, v0, v1}, LaH/j;->b(D)LaH/j;

    goto/16 :goto_6
.end method

.method private a(Lq/c;DDDLM/i;I[Lq/c;)V
    .registers 30
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 452
    aput-object p1, p10, p9

    .line 456
    invoke-virtual/range {p1 .. p1}, Lq/c;->c()Lo/T;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lq/c;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/T;->c(Lo/T;)F

    move-result v2

    float-to-double v0, v2

    move-wide/from16 v16, v0

    .line 462
    move-object/from16 v0, p8

    iget-wide v2, v0, LM/i;->e:D

    move-object/from16 v0, p8

    iget-wide v4, v0, LM/i;->a:D

    mul-double/2addr v2, v4

    div-double v2, v2, v16

    .line 464
    sub-double v3, p2, v2

    .line 466
    invoke-virtual/range {p1 .. p1}, Lq/c;->f()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LM/h;

    .line 474
    iget-wide v5, v2, LM/h;->i:D

    cmpl-double v5, v5, v3

    if-ltz v5, :cond_46

    .line 475
    iget-wide v5, v2, LM/h;->i:D

    sub-double v5, v5, p2

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    move-result-wide v5

    mul-double v5, v5, v16

    move-object/from16 v0, p8

    iget-wide v7, v0, LM/i;->a:D

    div-double/2addr v5, v7

    .line 478
    add-double v5, v5, p4

    .line 479
    move-object/from16 v0, p8

    invoke-virtual {v0, v5, v6}, LM/i;->a(D)D

    move-result-wide v5

    mul-double v5, v5, p6

    .line 481
    invoke-virtual {v2, v5, v6}, LM/h;->b(D)V

    .line 495
    :cond_46
    if-nez p9, :cond_bc

    move-wide v13, v3

    .line 499
    :goto_49
    invoke-virtual/range {p1 .. p1}, Lq/c;->a()Ljava/util/ArrayList;

    move-result-object v18

    .line 500
    const/4 v2, 0x0

    move v15, v2

    :goto_4f
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v15, v2, :cond_bf

    .line 501
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lq/b;

    .line 502
    invoke-virtual {v2}, Lq/b;->b()F

    move-result v3

    float-to-double v4, v3

    .line 504
    cmpl-double v3, v4, v13

    if-lez v3, :cond_b8

    .line 505
    invoke-virtual {v2}, Lq/b;->a()Lq/c;

    move-result-object v3

    .line 508
    invoke-virtual/range {p1 .. p1}, Lq/c;->e()F

    move-result v6

    invoke-virtual {v3}, Lq/c;->e()F

    move-result v7

    invoke-static {v6, v7}, Lo/V;->a(FF)F

    move-result v6

    float-to-double v6, v6

    .line 510
    invoke-static {v6, v7}, LM/g;->a(D)D

    move-result-wide v6

    .line 511
    sub-double v4, v4, p2

    mul-double v4, v4, v16

    move-object/from16 v0, p8

    iget-wide v8, v0, LM/i;->a:D

    div-double/2addr v4, v8

    .line 514
    add-double v4, v4, p4

    add-double/2addr v6, v4

    .line 520
    move-object/from16 v0, p8

    iget-wide v4, v0, LM/i;->j:D

    const-wide/high16 v8, 0x4010

    move-object/from16 v0, p8

    iget-wide v10, v0, LM/i;->k:D

    mul-double/2addr v8, v10

    add-double/2addr v4, v8

    .line 522
    const/4 v8, 0x6

    move/from16 v0, p9

    if-ge v0, v8, :cond_b8

    cmpg-double v4, v6, v4

    if-gez v4, :cond_b8

    .line 524
    move-object/from16 v0, p10

    move/from16 v1, p9

    invoke-static {v3, v0, v1}, LM/g;->a(Lq/c;[Lq/c;I)Z

    move-result v4

    if-nez v4, :cond_b8

    .line 525
    invoke-virtual {v2}, Lq/b;->c()F

    move-result v2

    float-to-double v4, v2

    add-int/lit8 v11, p9, 0x1

    move-object/from16 v2, p0

    move-wide/from16 v8, p6

    move-object/from16 v10, p8

    move-object/from16 v12, p10

    invoke-direct/range {v2 .. v12}, LM/g;->a(Lq/c;DDDLM/i;I[Lq/c;)V

    .line 500
    :cond_b8
    add-int/lit8 v2, v15, 0x1

    move v15, v2

    goto :goto_4f

    :cond_bc
    move-wide/from16 v13, p2

    .line 495
    goto :goto_49

    .line 537
    :cond_bf
    return-void
.end method

.method private static a(Lq/c;[Lq/c;I)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1030
    move v1, v0

    :goto_2
    if-ge v1, p2, :cond_9

    .line 1031
    aget-object v2, p1, v1

    if-ne v2, p0, :cond_a

    .line 1032
    const/4 v0, 0x1

    .line 1035
    :cond_9
    return v0

    .line 1030
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private b(LaH/j;LM/i;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 220
    invoke-virtual {p1}, LaH/j;->i()Z

    move-result v0

    if-nez v0, :cond_10

    iget-boolean v0, p2, LM/i;->f:Z

    if-eqz v0, :cond_10

    .line 221
    iget-wide v0, p2, LM/i;->g:D

    double-to-float v0, v0

    invoke-virtual {p1, v0}, LaH/j;->b(F)LaH/j;

    .line 223
    :cond_10
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1163
    const-string v0, "driveabout_hmm"

    return-object v0
.end method

.method a(LaH/j;LR/m;D)Lq/d;
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1050
    invoke-virtual {p1}, LaH/j;->m()D

    move-result-wide v0

    invoke-virtual {p1}, LaH/j;->n()D

    move-result-wide v3

    invoke-static {v0, v1, v3, v4}, Lo/T;->a(DD)Lo/T;

    move-result-object v4

    .line 1051
    const-wide/high16 v0, 0x4049

    mul-double/2addr v0, p3

    double-to-int v0, v0

    invoke-static {v4, v0}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v1

    .line 1054
    iget-object v0, p0, LM/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1055
    if-lez v5, :cond_6c

    .line 1056
    new-array v6, v5, [Lo/T;

    move v3, v2

    .line 1057
    :goto_20
    if-ge v3, v5, :cond_32

    .line 1058
    iget-object v0, p0, LM/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/h;

    iget-object v0, v0, LM/h;->j:Lo/T;

    aput-object v0, v6, v3

    .line 1057
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_20

    .line 1060
    :cond_32
    invoke-static {v6}, Lo/ad;->a([Lo/T;)Lo/ad;

    move-result-object v0

    const-wide/high16 v5, 0x4014

    mul-double/2addr v5, p3

    double-to-int v3, v5

    invoke-virtual {v0, v3}, Lo/ad;->b(I)Lo/ad;

    move-result-object v0

    invoke-virtual {v1, v0}, Lo/ad;->a(Lo/ad;)Lo/ad;

    move-result-object v0

    .line 1063
    :goto_42
    invoke-virtual {p2}, LR/m;->d()I

    move-result v1

    int-to-double v5, v1

    mul-double/2addr v5, p3

    double-to-int v1, v5

    .line 1065
    invoke-static {v4, v1}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v3

    .line 1068
    :try_start_4d
    iget v1, p0, LM/g;->i:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_60

    const/4 v1, 0x1

    .line 1069
    :goto_53
    iget-object v2, p0, LM/g;->e:Lq/e;

    invoke-virtual {v3, v0}, Lo/ad;->c(Lo/ad;)Lo/ad;

    move-result-object v0

    const-wide/16 v3, 0x5dc

    invoke-virtual {v2, v0, v1, v3, v4}, Lq/e;->a(Lo/ad;ZJ)Lq/d;
    :try_end_5e
    .catch Ljava/lang/InterruptedException; {:try_start_4d .. :try_end_5e} :catch_62

    move-result-object v0

    .line 1073
    :goto_5f
    return-object v0

    :cond_60
    move v1, v2

    .line 1068
    goto :goto_53

    .line 1071
    :catch_62
    move-exception v0

    .line 1072
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 1073
    const/4 v0, 0x0

    goto :goto_5f

    :cond_6c
    move-object v0, v1

    goto :goto_42
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 172
    iput p1, p0, LM/g;->i:I

    .line 173
    return-void
.end method

.method public a(LO/z;)V
    .registers 2
    .parameter

    .prologue
    .line 163
    iput-object p1, p0, LM/g;->h:LO/z;

    .line 164
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .registers 10
    .parameter

    .prologue
    .line 183
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 184
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    .line 189
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v4

    .line 190
    new-instance v5, LM/i;

    iget-object v6, p0, LM/g;->g:LM/i;

    iget-object v7, p0, LM/g;->h:LO/z;

    invoke-direct {v5, p1, v6, v4, v7}, LM/i;-><init>(Landroid/location/Location;LM/i;LR/m;LO/z;)V

    .line 196
    new-instance v6, LaH/j;

    invoke-direct {v6}, LaH/j;-><init>()V

    invoke-virtual {v6, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v6

    .line 197
    invoke-direct {p0, v6, v5, v4}, LM/g;->a(LaH/j;LM/i;LR/m;)V

    .line 199
    invoke-virtual {v6}, LaH/j;->o()LO/H;

    move-result-object v4

    if-nez v4, :cond_2a

    .line 200
    invoke-direct {p0, v6, v5}, LM/g;->a(LaH/j;LM/i;)V

    .line 202
    :cond_2a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long v0, v4, v0

    .line 203
    const-wide/16 v4, 0xc8

    cmp-long v0, v0, v4

    if-lez v0, :cond_3b

    .line 204
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v2

    .line 208
    :cond_3b
    invoke-virtual {v6}, LaH/j;->d()LaH/h;

    move-result-object v0

    invoke-super {p0, v0}, LM/e;->onLocationChanged(Landroid/location/Location;)V

    .line 209
    return-void
.end method
