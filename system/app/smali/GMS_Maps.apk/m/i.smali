.class LM/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:D

.field b:D

.field c:D

.field d:Lo/T;

.field e:D

.field f:Z

.field g:D

.field h:D

.field i:Z

.field j:D

.field k:D

.field final l:LO/z;

.field final m:LR/m;


# direct methods
.method constructor <init>(Landroid/location/Location;LM/i;LR/m;LO/z;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 618
    iput-object p3, p0, LM/i;->m:LR/m;

    .line 619
    iput-object p4, p0, LM/i;->l:LO/z;

    .line 620
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-static {v0, v1}, Lo/T;->a(D)D

    move-result-wide v0

    iput-wide v0, p0, LM/i;->a:D

    .line 621
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    long-to-double v0, v0

    const-wide v2, 0x408f400000000000L

    div-double/2addr v0, v2

    iput-wide v0, p0, LM/i;->b:D

    .line 624
    if-nez p2, :cond_ae

    const-wide/high16 v0, 0x4000

    :goto_22
    iput-wide v0, p0, LM/i;->c:D

    .line 628
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_b5

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v3, v0

    .line 633
    :goto_2f
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    invoke-static {v0, v1, v5, v6}, Lo/T;->a(DD)Lo/T;

    move-result-object v0

    iput-object v0, p0, LM/i;->d:Lo/T;

    .line 635
    iget-object v0, p0, LM/i;->m:LR/m;

    invoke-virtual {v0}, LR/m;->f()I

    move-result v0

    int-to-double v0, v0

    add-double/2addr v0, v3

    iput-wide v0, p0, LM/i;->e:D

    .line 639
    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_c3

    .line 640
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, LM/i;->g:D

    .line 641
    if-nez p2, :cond_bc

    iget-wide v0, p0, LM/i;->g:D

    move-wide v5, v0

    .line 643
    :goto_59
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v1

    float-to-double v1, v1

    double-to-float v5, v5

    iget-wide v6, p0, LM/i;->g:D

    double-to-float v6, v6

    invoke-static {v5, v6}, Lo/V;->a(FF)F

    move-result v5

    float-to-double v5, v5

    iget-object v7, p0, LM/i;->m:LR/m;

    invoke-virtual {v7}, LR/m;->g()I

    move-result v7

    int-to-double v7, v7

    invoke-static/range {v0 .. v8}, LM/i;->a(ZDDDD)D

    move-result-wide v0

    iput-wide v0, p0, LM/i;->h:D

    .line 649
    iget-wide v0, p0, LM/i;->h:D

    const-wide/high16 v2, 0x4059

    cmpg-double v0, v0, v2

    if-gez v0, :cond_c0

    iget-wide v0, p0, LM/i;->h:D

    :goto_82
    iput-wide v0, p0, LM/i;->h:D

    .line 651
    const/4 v0, 0x1

    iput-boolean v0, p0, LM/i;->f:Z

    .line 676
    :goto_87
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_f8

    .line 677
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    float-to-double v0, v0

    iget-wide v2, p0, LM/i;->c:D

    mul-double/2addr v0, v2

    iput-wide v0, p0, LM/i;->j:D

    .line 678
    const-wide/high16 v0, 0x4034

    const-wide/high16 v2, 0x4049

    iget-wide v4, p0, LM/i;->j:D

    const-wide/high16 v6, 0x4000

    div-double/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iput-wide v0, p0, LM/i;->k:D

    .line 680
    const/4 v0, 0x1

    iput-boolean v0, p0, LM/i;->i:Z

    .line 692
    :goto_ad
    return-void

    .line 624
    :cond_ae
    iget-wide v0, p0, LM/i;->b:D

    iget-wide v2, p2, LM/i;->b:D

    sub-double/2addr v0, v2

    goto/16 :goto_22

    .line 628
    :cond_b5
    invoke-static {}, LM/n;->a()F

    move-result v0

    float-to-double v3, v0

    goto/16 :goto_2f

    .line 641
    :cond_bc
    iget-wide v0, p2, LM/i;->g:D

    move-wide v5, v0

    goto :goto_59

    .line 649
    :cond_c0
    const-wide/high16 v0, 0x4059

    goto :goto_82

    .line 652
    :cond_c3
    if-eqz p2, :cond_f4

    iget-boolean v0, p2, LM/i;->f:Z

    if-eqz v0, :cond_f4

    .line 657
    iget-wide v0, p2, LM/i;->g:D

    iput-wide v0, p0, LM/i;->g:D

    .line 658
    iget-object v0, p2, LM/i;->d:Lo/T;

    iget-object v1, p0, LM/i;->d:Lo/T;

    invoke-virtual {v0, v1}, Lo/T;->c(Lo/T;)F

    move-result v0

    float-to-double v0, v0

    iget-wide v2, p0, LM/i;->a:D

    div-double/2addr v0, v2

    .line 661
    iget-wide v2, p2, LM/i;->h:D

    const-wide/high16 v4, 0x3fe0

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    iput-wide v0, p0, LM/i;->h:D

    .line 664
    iget-wide v0, p0, LM/i;->h:D

    const-wide/high16 v2, 0x4059

    cmpg-double v0, v0, v2

    if-gez v0, :cond_f1

    iget-wide v0, p0, LM/i;->h:D

    :goto_eb
    iput-wide v0, p0, LM/i;->h:D

    .line 666
    const/4 v0, 0x1

    iput-boolean v0, p0, LM/i;->f:Z

    goto :goto_87

    .line 664
    :cond_f1
    const-wide/high16 v0, 0x4059

    goto :goto_eb

    .line 668
    :cond_f4
    const/4 v0, 0x0

    iput-boolean v0, p0, LM/i;->f:Z

    goto :goto_87

    .line 681
    :cond_f8
    if-eqz p2, :cond_116

    iget-boolean v0, p2, LM/i;->i:Z

    if-eqz v0, :cond_116

    .line 685
    iget-wide v0, p2, LM/i;->j:D

    iput-wide v0, p0, LM/i;->j:D

    .line 686
    const-wide/high16 v0, 0x4049

    iget-wide v2, p2, LM/i;->k:D

    iget-wide v4, p0, LM/i;->j:D

    const-wide/high16 v6, 0x4000

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iput-wide v0, p0, LM/i;->k:D

    .line 688
    const/4 v0, 0x1

    iput-boolean v0, p0, LM/i;->i:Z

    goto :goto_ad

    .line 690
    :cond_116
    const/4 v0, 0x0

    iput-boolean v0, p0, LM/i;->i:Z

    goto :goto_ad
.end method

.method static a(ZDDDD)D
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 802
    const-wide v0, 0x4046800000000000L

    const-wide/high16 v2, 0x3ff0

    neg-double v4, p3

    const-wide/high16 v6, 0x403e

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    sub-double/2addr v2, v4

    mul-double/2addr v2, v0

    .line 807
    const-wide/high16 v0, 0x4010

    div-double v4, p5, v0

    .line 811
    const-wide/16 v0, 0x0

    .line 812
    if-eqz p0, :cond_24

    .line 813
    const-wide/high16 v0, 0x4034

    neg-double v6, p1

    const-wide/high16 v8, 0x4000

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    mul-double/2addr v0, v6

    .line 816
    :cond_24
    add-double/2addr v2, v4

    add-double/2addr v0, v2

    add-double v0, v0, p7

    return-wide v0
.end method


# virtual methods
.method a()D
    .registers 3

    .prologue
    .line 856
    iget-wide v0, p0, LM/i;->a:D

    return-wide v0
.end method

.method a(D)D
    .registers 7
    .parameter

    .prologue
    .line 768
    iget-wide v0, p0, LM/i;->j:D

    sub-double v0, p1, v0

    iget-wide v2, p0, LM/i;->k:D

    div-double/2addr v0, v2

    .line 769
    neg-double v2, v0

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    return-wide v0
.end method

.method a(LO/D;Lo/T;D)D
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 701
    invoke-virtual {p0, p1, p2, p3, p4}, LM/i;->b(LO/D;Lo/T;D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    return-wide v0
.end method

.method a(Lo/T;DLo/af;)D
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide v8, 0x400ccccccccccccdL

    const-wide/high16 v6, 0x4000

    .line 725
    iget-object v0, p0, LM/i;->d:Lo/T;

    invoke-virtual {p1, v0}, Lo/T;->c(Lo/T;)F

    move-result v0

    float-to-double v0, v0

    .line 726
    iget-wide v2, p0, LM/i;->a:D

    div-double v2, v0, v2

    .line 730
    const-wide v0, 0x3ffccccccccccccdL

    .line 731
    invoke-virtual {p4}, Lo/af;->j()Z

    move-result v4

    if-nez v4, :cond_2f

    .line 732
    invoke-virtual {p4}, Lo/af;->f()I

    move-result v4

    const/16 v5, 0x40

    if-lt v4, v5, :cond_26

    .line 733
    add-double/2addr v0, v8

    .line 735
    :cond_26
    invoke-virtual {p4}, Lo/af;->f()I

    move-result v4

    const/16 v5, 0x80

    if-lt v4, v5, :cond_2f

    .line 736
    add-double/2addr v0, v8

    .line 739
    :cond_2f
    const-wide/16 v4, 0x0

    sub-double v0, v2, v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 743
    iget-wide v2, p0, LM/i;->e:D

    div-double/2addr v0, v2

    .line 744
    neg-double v2, v0

    mul-double/2addr v0, v2

    div-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    .line 749
    iget-boolean v0, p0, LM/i;->f:Z

    if-eqz v0, :cond_5a

    .line 750
    double-to-float v0, p2

    iget-wide v4, p0, LM/i;->g:D

    double-to-float v1, v4

    invoke-static {v0, v1}, Lo/V;->a(FF)F

    move-result v0

    .line 752
    float-to-double v0, v0

    iget-wide v4, p0, LM/i;->h:D

    div-double/2addr v0, v4

    .line 753
    neg-double v4, v0

    mul-double/2addr v0, v4

    div-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    .line 758
    :goto_58
    mul-double/2addr v0, v2

    return-wide v0

    .line 755
    :cond_5a
    const-wide/high16 v0, 0x3ff0

    goto :goto_58
.end method

.method a(Lo/T;D)LO/D;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 831
    const/4 v4, 0x0

    .line 832
    iget-object v1, p0, LM/i;->l:LO/z;

    if-eqz v1, :cond_34

    .line 833
    iget-wide v1, p0, LM/i;->a:D

    iget-wide v5, p0, LM/i;->k:D

    const-wide/high16 v7, 0x4008

    mul-double/2addr v5, v7

    iget-object v3, p0, LM/i;->m:LR/m;

    invoke-virtual {v3}, LR/m;->f()I

    move-result v3

    int-to-double v7, v3

    add-double/2addr v5, v7

    mul-double/2addr v1, v5

    .line 837
    iget-object v3, p0, LM/i;->l:LO/z;

    invoke-virtual {v3, p1, v1, v2, v0}, LO/z;->a(Lo/T;DZ)[LO/D;

    move-result-object v6

    .line 842
    const-wide/high16 v2, -0x10

    move v5, v0

    .line 843
    :goto_1f
    array-length v0, v6

    if-ge v5, v0, :cond_34

    .line 844
    aget-object v0, v6, v5

    invoke-virtual {p0, v0, p1, p2, p3}, LM/i;->b(LO/D;Lo/T;D)D

    move-result-wide v0

    .line 846
    cmpl-double v7, v0, v2

    if-lez v7, :cond_35

    .line 848
    aget-object v2, v6, v5

    .line 843
    :goto_2e
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v4, v2

    move-wide v2, v0

    goto :goto_1f

    .line 852
    :cond_34
    return-object v4

    :cond_35
    move-wide v0, v2

    move-object v2, v4

    goto :goto_2e
.end method

.method b(D)D
    .registers 7
    .parameter

    .prologue
    .line 777
    const-wide/high16 v0, 0x3ff0

    iget-object v2, p0, LM/i;->m:LR/m;

    invoke-virtual {v2}, LR/m;->h()D

    move-result-wide v2

    mul-double/2addr v2, p1

    add-double/2addr v0, v2

    return-wide v0
.end method

.method b(LO/D;Lo/T;D)D
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 712
    invoke-virtual {p1}, LO/D;->b()Lo/T;

    move-result-object v0

    invoke-virtual {v0, p2}, Lo/T;->c(Lo/T;)F

    move-result v0

    float-to-double v0, v0

    iget-wide v2, p0, LM/i;->a:D

    iget-object v4, p0, LM/i;->m:LR/m;

    invoke-virtual {v4}, LR/m;->f()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    .line 714
    invoke-virtual {p1}, LO/D;->c()D

    move-result-wide v2

    double-to-float v2, v2

    double-to-float v3, p3

    invoke-static {v2, v3}, Lo/V;->a(FF)F

    move-result v2

    iget-object v3, p0, LM/i;->m:LR/m;

    invoke-virtual {v3}, LR/m;->g()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    .line 717
    const-wide/high16 v4, -0x4020

    mul-double/2addr v0, v0

    mul-double/2addr v2, v2

    add-double/2addr v0, v2

    mul-double/2addr v0, v4

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 861
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LocationModel[mTimeSinceLastUpdate:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, LM/i;->c:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mPositionMean:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LM/i;->d:Lo/T;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mPositionStdMeters:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, LM/i;->e:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mDistanceMeanMeters:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, LM/i;->i:Z

    if-eqz v0, :cond_74

    iget-wide v2, p0, LM/i;->j:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    :goto_39
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mDistanceStdMeters:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, LM/i;->i:Z

    if-eqz v0, :cond_77

    iget-wide v2, p0, LM/i;->k:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    :goto_4d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mBearingMeanDeg:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, LM/i;->g:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mBearingStdDeg:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, LM/i;->h:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_74
    const-string v0, "--"

    goto :goto_39

    :cond_77
    const-string v0, "--"

    goto :goto_4d
.end method
