.class public LM/E;
.super LM/e;
.source "SourceFile"


# instance fields
.field private final c:Landroid/os/Handler;

.field private final d:Lcom/google/googlenav/common/a;

.field private e:LaH/h;

.field private f:J

.field private final g:[F

.field private h:I

.field private i:LR/m;

.field private final j:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LM/b;Landroid/os/Handler;Lcom/google/googlenav/common/a;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 75
    const-string v0, "da_tunnel_heartbeat"

    invoke-direct {p0, v0, p1}, LM/e;-><init>(Ljava/lang/String;LM/b;)V

    .line 54
    const/4 v0, 0x0

    iput v0, p0, LM/E;->h:I

    .line 59
    new-instance v0, LM/F;

    invoke-direct {v0, p0}, LM/F;-><init>(LM/E;)V

    iput-object v0, p0, LM/E;->j:Ljava/lang/Runnable;

    .line 76
    iput-object p2, p0, LM/E;->c:Landroid/os/Handler;

    .line 77
    iput-object p3, p0, LM/E;->d:Lcom/google/googlenav/common/a;

    .line 78
    const/16 v0, 0xa

    new-array v0, v0, [F

    iput-object v0, p0, LM/E;->g:[F

    .line 79
    iget-object v0, p0, LM/E;->g:[F

    const/high16 v1, -0x4080

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 80
    return-void
.end method

.method static a([F)F
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x0

    const/high16 v2, -0x4080

    .line 212
    .line 213
    const/4 v1, 0x0

    .line 214
    array-length v4, p0

    move v3, v0

    move v7, v1

    move v1, v0

    move v0, v7

    :goto_9
    if-ge v3, v4, :cond_17

    aget v5, p0, v3

    .line 215
    cmpl-float v6, v5, v2

    if-eqz v6, :cond_14

    .line 216
    add-float/2addr v0, v5

    .line 217
    add-int/lit8 v1, v1, 0x1

    .line 214
    :cond_14
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    .line 220
    :cond_17
    const/4 v3, 0x2

    if-ge v1, v3, :cond_1c

    move v0, v2

    .line 223
    :goto_1b
    return v0

    :cond_1c
    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_1b
.end method

.method static a(LaH/h;F)LaH/j;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f80

    const/4 v1, 0x0

    .line 168
    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Lo/T;->a(D)D

    move-result-wide v4

    double-to-float v0, v4

    .line 169
    mul-float v4, p1, v0

    .line 172
    invoke-virtual {p0}, LaH/h;->n()LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    .line 174
    if-nez v0, :cond_19

    move-object v0, v3

    .line 202
    :goto_18
    return-object v0

    .line 177
    :cond_19
    invoke-virtual {v0}, LO/D;->a()LO/z;

    move-result-object v5

    .line 178
    invoke-virtual {v5, v0}, LO/z;->a(LO/D;)D

    move-result-wide v6

    double-to-float v0, v6

    add-float/2addr v0, v4

    .line 179
    invoke-virtual {v5}, LO/z;->n()Lo/X;

    move-result-object v4

    .line 180
    float-to-double v6, v0

    invoke-virtual {v5, v6, v7}, LO/z;->a(D)I

    move-result v6

    .line 181
    if-ltz v6, :cond_36

    invoke-virtual {v4}, Lo/X;->b()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-lt v6, v7, :cond_38

    :cond_36
    move-object v0, v3

    .line 182
    goto :goto_18

    .line 186
    :cond_38
    invoke-virtual {v4, v6}, Lo/X;->a(I)Lo/T;

    move-result-object v3

    .line 187
    add-int/lit8 v7, v6, 0x1

    invoke-virtual {v4, v7}, Lo/X;->a(I)Lo/T;

    move-result-object v4

    .line 188
    invoke-virtual {v5, v6}, LO/z;->b(I)D

    move-result-wide v5

    double-to-float v5, v5

    sub-float/2addr v0, v5

    .line 189
    invoke-virtual {v3, v4}, Lo/T;->c(Lo/T;)F

    move-result v5

    div-float/2addr v0, v5

    .line 190
    cmpg-float v5, v0, v1

    if-gez v5, :cond_75

    move v0, v1

    .line 195
    :cond_52
    :goto_52
    invoke-virtual {v3, v4, v0}, Lo/T;->a(Lo/T;F)Lo/T;

    move-result-object v0

    .line 198
    new-instance v1, LaH/j;

    invoke-direct {v1}, LaH/j;-><init>()V

    invoke-virtual {v1, p0}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v1

    invoke-virtual {v0}, Lo/T;->b()D

    move-result-wide v5

    invoke-virtual {v0}, Lo/T;->d()D

    move-result-wide v7

    invoke-virtual {v1, v5, v6, v7, v8}, LaH/j;->a(DD)LaH/j;

    move-result-object v0

    invoke-static {v3, v4}, Lo/T;->a(Lo/T;Lo/T;)D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v0, v1}, LaH/j;->b(F)LaH/j;

    move-result-object v0

    goto :goto_18

    .line 192
    :cond_75
    cmpl-float v1, v0, v2

    if-lez v1, :cond_52

    move v0, v2

    .line 193
    goto :goto_52
.end method

.method public static a(Landroid/location/Location;)Z
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 121
    instance-of v1, p0, LaH/h;

    if-nez v1, :cond_6

    .line 129
    :cond_5
    :goto_5
    return v0

    .line 124
    :cond_6
    check-cast p0, LaH/h;

    .line 125
    invoke-virtual {p0}, LaH/h;->i()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 128
    invoke-virtual {p0}, LaH/h;->l()Lo/af;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lo/af;->g()Z

    move-result v0

    goto :goto_5
.end method

.method private d()J
    .registers 5

    .prologue
    .line 231
    const-wide/16 v0, 0x7d0

    iget-object v2, p0, LM/E;->i:LR/m;

    invoke-virtual {v2}, LR/m;->l()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 84
    iget-object v0, p0, LM/E;->c:Landroid/os/Handler;

    if-eqz v0, :cond_b

    .line 85
    iget-object v0, p0, LM/E;->c:Landroid/os/Handler;

    iget-object v1, p0, LM/E;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 87
    :cond_b
    return-void
.end method

.method a(J)V
    .registers 5
    .parameter

    .prologue
    .line 236
    iget-object v0, p0, LM/E;->c:Landroid/os/Handler;

    if-eqz v0, :cond_12

    .line 237
    iget-object v0, p0, LM/E;->c:Landroid/os/Handler;

    iget-object v1, p0, LM/E;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 238
    iget-object v0, p0, LM/E;->c:Landroid/os/Handler;

    iget-object v1, p0, LM/E;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 240
    :cond_12
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 91
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    iput-object v0, p0, LM/E;->i:LR/m;

    .line 92
    invoke-direct {p0}, LM/E;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LM/E;->a(J)V

    .line 93
    return-void
.end method

.method c()V
    .registers 7

    .prologue
    .line 137
    iget-object v0, p0, LM/E;->e:LaH/h;

    invoke-static {v0}, LM/E;->a(Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 161
    :cond_8
    :goto_8
    return-void

    .line 140
    :cond_9
    iget-object v0, p0, LM/E;->g:[F

    invoke-static {v0}, LM/E;->a([F)F

    move-result v0

    .line 141
    const/high16 v1, -0x4080

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_8

    const/high16 v1, 0x4000

    cmpg-float v1, v0, v1

    if-ltz v1, :cond_8

    .line 147
    iget-object v1, p0, LM/E;->g:[F

    invoke-static {v1, v0}, Ljava/util/Arrays;->fill([FF)V

    .line 149
    iget-object v1, p0, LM/E;->d:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    .line 150
    const/4 v3, 0x0

    iget-wide v4, p0, LM/E;->f:J

    sub-long v4, v1, v4

    long-to-float v4, v4

    const v5, 0x3a83126f

    mul-float/2addr v4, v5

    mul-float/2addr v4, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 151
    iget-object v4, p0, LM/E;->e:LaH/h;

    invoke-static {v4, v3}, LM/E;->a(LaH/h;F)LaH/j;

    move-result-object v3

    .line 152
    if-eqz v3, :cond_8

    .line 153
    invoke-virtual {v3, v1, v2}, LaH/j;->a(J)LaH/j;

    move-result-object v1

    iget-object v2, p0, LM/E;->i:LR/m;

    invoke-virtual {v2}, LR/m;->n()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, LaH/j;->a(F)LaH/j;

    move-result-object v1

    invoke-virtual {v1, v0}, LaH/j;->c(F)LaH/j;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaH/j;->d(Z)LaH/j;

    .line 157
    invoke-virtual {v3}, LaH/j;->d()LaH/h;

    move-result-object v0

    .line 159
    invoke-super {p0, v0}, LM/e;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_8
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .registers 5
    .parameter

    .prologue
    .line 98
    instance-of v0, p1, LaH/h;

    if-nez v0, :cond_5

    .line 117
    :cond_4
    :goto_4
    return-void

    .line 101
    :cond_5
    check-cast p1, LaH/h;

    .line 102
    invoke-virtual {p1}, LaH/h;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, LaH/h;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 107
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    iput-object v0, p0, LM/E;->i:LR/m;

    .line 110
    invoke-direct {p0}, LM/E;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LM/E;->a(J)V

    .line 112
    iput-object p1, p0, LM/E;->e:LaH/h;

    .line 113
    iget-object v0, p0, LM/E;->d:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, LM/E;->f:J

    .line 115
    iget-object v1, p0, LM/E;->g:[F

    iget v2, p0, LM/E;->h:I

    invoke-virtual {p1}, LaH/h;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-virtual {p1}, LaH/h;->getSpeed()F

    move-result v0

    :goto_38
    aput v0, v1, v2

    .line 116
    iget v0, p0, LM/E;->h:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0xa

    iput v0, p0, LM/E;->h:I

    goto :goto_4

    .line 115
    :cond_43
    const/high16 v0, -0x4080

    goto :goto_38
.end method
