.class public LM/B;
.super LM/e;
.source "SourceFile"


# instance fields
.field private final c:LaV/f;

.field private final d:LN/a;

.field private final e:LaV/f;

.field private f:Landroid/location/Location;

.field private g:Z

.field private h:F


# direct methods
.method public constructor <init>(LM/b;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 43
    const-string v0, "driveabout_orientation_filter"

    invoke-direct {p0, v0, p1}, LM/e;-><init>(Ljava/lang/String;LM/b;)V

    .line 31
    new-instance v0, LaV/f;

    invoke-direct {v0, v1}, LaV/f;-><init>(Z)V

    iput-object v0, p0, LM/B;->e:LaV/f;

    .line 35
    iput-boolean v1, p0, LM/B;->g:Z

    .line 37
    const/4 v0, 0x0

    iput v0, p0, LM/B;->h:F

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, LM/B;->d:LN/a;

    .line 50
    new-instance v0, LaV/f;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LaV/f;-><init>(Z)V

    iput-object v0, p0, LM/B;->c:LaV/f;

    .line 53
    return-void
.end method


# virtual methods
.method public a(LM/C;)V
    .registers 7
    .parameter

    .prologue
    .line 70
    new-instance v1, LM/C;

    invoke-direct {v1, p1}, LM/C;-><init>(LM/C;)V

    .line 71
    const-string v0, "driveabout_orientation_filter"

    invoke-virtual {v1, v0}, LM/C;->a(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, LM/B;->c:LaV/f;

    invoke-virtual {p1}, LM/C;->b()J

    move-result-wide v2

    invoke-virtual {v1}, LM/C;->c()F

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, LaV/f;->a(JF)F

    move-result v2

    .line 87
    iget-boolean v0, p0, LM/B;->g:Z

    if-eqz v0, :cond_64

    const/high16 v0, 0x41f0

    .line 89
    :goto_1e
    iget-object v3, p0, LM/B;->f:Landroid/location/Location;

    if-eqz v3, :cond_67

    iget-object v3, p0, LM/B;->f:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->hasBearing()Z

    move-result v3

    if-eqz v3, :cond_67

    iget v3, p0, LM/B;->h:F

    const v4, 0x3e4ccccd

    cmpl-float v3, v3, v4

    if-lez v3, :cond_67

    iget-object v3, p0, LM/B;->f:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getBearing()F

    move-result v3

    invoke-static {v3, v2}, Lo/V;->a(FF)F

    move-result v3

    cmpg-float v0, v3, v0

    if-gez v0, :cond_67

    .line 93
    iget-object v0, p0, LM/B;->f:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-virtual {v1, v0}, LM/C;->a(F)V

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, LM/B;->g:Z

    .line 100
    :goto_4d
    iget-object v0, p0, LM/B;->e:LaV/f;

    invoke-virtual {p1}, LM/C;->b()J

    move-result-wide v2

    invoke-virtual {p1}, LM/C;->d()F

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, LaV/f;->a(JF)F

    move-result v0

    invoke-virtual {v1, v0}, LM/C;->b(F)V

    .line 101
    iget-object v0, p0, LM/B;->b:LM/b;

    invoke-interface {v0, v1}, LM/b;->a(LM/C;)V

    .line 102
    return-void

    .line 87
    :cond_64
    const/high16 v0, 0x41c8

    goto :goto_1e

    .line 96
    :cond_67
    invoke-virtual {v1, v2}, LM/C;->a(F)V

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, LM/B;->g:Z

    goto :goto_4d
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .registers 3
    .parameter

    .prologue
    .line 57
    iput-object p1, p0, LM/B;->f:Landroid/location/Location;

    .line 58
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 59
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    iput v0, p0, LM/B;->h:F

    .line 61
    :cond_e
    return-void
.end method
