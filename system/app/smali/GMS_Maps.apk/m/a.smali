.class public LM/a;
.super LM/e;
.source "SourceFile"


# instance fields
.field private c:Z

.field private d:J

.field private e:F

.field private f:F

.field private g:F


# direct methods
.method public constructor <init>(LM/b;)V
    .registers 3
    .parameter

    .prologue
    .line 33
    const-string v0, "driveabout_bearing_noise_reduction"

    invoke-direct {p0, v0, p1}, LM/e;-><init>(Ljava/lang/String;LM/b;)V

    .line 26
    const/high16 v0, -0x4080

    iput v0, p0, LM/a;->e:F

    .line 34
    return-void
.end method

.method private a()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 133
    iput v0, p0, LM/a;->f:F

    .line 134
    iput v0, p0, LM/a;->g:F

    .line 135
    return-void
.end method

.method private a(Landroid/location/Location;)Z
    .registers 13
    .parameter

    .prologue
    const/4 v3, 0x0

    const v9, 0x3c8efa35

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 68
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v4

    .line 71
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v5

    iget-wide v7, p0, LM/a;->d:J

    sub-long/2addr v5, v7

    .line 72
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v7

    iput-wide v7, p0, LM/a;->d:J

    .line 73
    invoke-virtual {v4}, LR/m;->k()I

    move-result v0

    int-to-long v7, v0

    cmp-long v0, v5, v7

    if-lez v0, :cond_28

    .line 74
    invoke-direct {p0}, LM/a;->a()V

    .line 75
    const/high16 v0, -0x4080

    iput v0, p0, LM/a;->e:F

    .line 129
    :goto_27
    return v2

    .line 80
    :cond_28
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_45

    .line 81
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    .line 82
    iput v0, p0, LM/a;->e:F

    .line 96
    :goto_34
    invoke-virtual {v4}, LR/m;->i()I

    move-result v7

    int-to-float v7, v7

    const v8, 0x3c23d70a

    mul-float/2addr v7, v8

    .line 98
    cmpl-float v8, v0, v7

    if-lez v8, :cond_52

    .line 99
    invoke-direct {p0}, LM/a;->a()V

    goto :goto_27

    .line 87
    :cond_45
    iget v0, p0, LM/a;->e:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_4e

    .line 88
    iget v0, p0, LM/a;->e:F

    goto :goto_34

    .line 90
    :cond_4e
    invoke-direct {p0}, LM/a;->a()V

    goto :goto_27

    .line 103
    :cond_52
    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v8

    if-nez v8, :cond_5d

    .line 104
    invoke-direct {p0}, LM/a;->a()V

    move v2, v3

    .line 105
    goto :goto_27

    .line 110
    :cond_5d
    cmpl-float v8, v0, v1

    if-lez v8, :cond_b0

    .line 111
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v1

    mul-float/2addr v1, v9

    invoke-static {v1}, Landroid/util/FloatMath;->sin(F)F

    move-result v1

    .line 113
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v8

    mul-float/2addr v8, v9

    invoke-static {v8}, Landroid/util/FloatMath;->cos(F)F

    move-result v8

    .line 115
    mul-float/2addr v1, v0

    .line 116
    mul-float/2addr v0, v8

    .line 119
    :goto_75
    invoke-virtual {v4}, LR/m;->j()I

    move-result v4

    int-to-float v4, v4

    .line 120
    iget v8, p0, LM/a;->f:F

    neg-long v9, v5

    long-to-float v9, v9

    div-float/2addr v9, v4

    float-to-double v9, v9

    invoke-static {v9, v10}, Ljava/lang/Math;->exp(D)D

    move-result-wide v9

    double-to-float v9, v9

    mul-float/2addr v8, v9

    add-float/2addr v1, v8

    iput v1, p0, LM/a;->f:F

    .line 122
    iget v1, p0, LM/a;->g:F

    neg-long v5, v5

    long-to-float v5, v5

    div-float v4, v5, v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v1, v4

    add-float/2addr v0, v1

    iput v0, p0, LM/a;->g:F

    .line 127
    iget v0, p0, LM/a;->f:F

    iget v1, p0, LM/a;->f:F

    mul-float/2addr v0, v1

    iget v1, p0, LM/a;->g:F

    iget v4, p0, LM/a;->g:F

    mul-float/2addr v1, v4

    add-float/2addr v0, v1

    .line 128
    mul-float v1, v7, v7

    .line 129
    cmpl-float v0, v0, v1

    if-ltz v0, :cond_ae

    move v0, v2

    :goto_ab
    move v2, v0

    goto/16 :goto_27

    :cond_ae
    move v0, v3

    goto :goto_ab

    :cond_b0
    move v0, v1

    goto :goto_75
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .registers 4
    .parameter

    .prologue
    .line 39
    new-instance v0, LaH/j;

    invoke-direct {v0}, LaH/j;-><init>()V

    invoke-virtual {v0, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    const-string v1, "driveabout_bearing_noise_reduction"

    invoke-virtual {v0, v1}, LaH/j;->a(Ljava/lang/String;)LaH/j;

    move-result-object v1

    .line 45
    instance-of v0, p1, LaH/h;

    if-eqz v0, :cond_25

    move-object v0, p1

    check-cast v0, LaH/h;

    invoke-virtual {v0}, LaH/h;->g()Z

    move-result v0

    if-eqz v0, :cond_25

    move-object v0, p1

    check-cast v0, LaH/h;

    invoke-virtual {v0}, LaH/h;->h()Z

    move-result v0

    if-nez v0, :cond_2f

    .line 48
    :cond_25
    iget-object v0, p0, LM/a;->b:LM/b;

    invoke-virtual {v1}, LaH/j;->d()LaH/h;

    move-result-object v1

    invoke-interface {v0, v1}, LM/b;->onLocationChanged(Landroid/location/Location;)V

    .line 61
    :goto_2e
    return-void

    .line 54
    :cond_2f
    invoke-direct {p0, p1}, LM/a;->a(Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_3c

    iget-boolean v0, p0, LM/a;->c:Z

    if-eqz v0, :cond_3c

    .line 55
    invoke-virtual {v1}, LaH/j;->a()LaH/j;

    .line 57
    :cond_3c
    invoke-virtual {v1}, LaH/j;->i()Z

    move-result v0

    if-eqz v0, :cond_45

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, LM/a;->c:Z

    .line 60
    :cond_45
    iget-object v0, p0, LM/a;->b:LM/b;

    invoke-virtual {v1}, LaH/j;->d()LaH/h;

    move-result-object v1

    invoke-interface {v0, v1}, LM/b;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_2e
.end method
