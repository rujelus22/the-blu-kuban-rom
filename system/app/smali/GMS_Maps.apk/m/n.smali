.class public LM/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;

.field private b:LM/v;

.field private c:LM/j;

.field private d:LM/r;

.field private e:LM/f;

.field private f:LM/a;

.field private g:LM/c;

.field private h:LM/c;

.field private i:LM/c;

.field private j:LM/E;

.field private k:LM/B;

.field private final l:LM/u;

.field private final m:Ljava/util/List;

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    new-instance v0, LM/u;

    invoke-direct {v0, p0, v3}, LM/u;-><init>(LM/n;LM/o;)V

    iput-object v0, p0, LM/n;->l:LM/u;

    .line 145
    invoke-static {}, Lcom/google/common/collect/bx;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LM/n;->m:Ljava/util/List;

    .line 148
    iput v2, p0, LM/n;->n:I

    .line 154
    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_4e

    move v0, v1

    :goto_1c
    iput-boolean v0, p0, LM/n;->o:Z

    .line 160
    iput-boolean v2, p0, LM/n;->p:Z

    .line 166
    iput-boolean v1, p0, LM/n;->q:Z

    .line 181
    iput-object p1, p0, LM/n;->a:Landroid/content/Context;

    .line 182
    new-instance v0, LM/x;

    invoke-direct {v0, p1, v3}, LM/x;-><init>(Landroid/content/Context;LM/o;)V

    iput-object v0, p0, LM/n;->b:LM/v;

    .line 187
    new-instance v0, LM/j;

    invoke-direct {v0}, LM/j;-><init>()V

    iput-object v0, p0, LM/n;->c:LM/j;

    .line 188
    new-instance v0, LM/g;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-direct {v0, v1}, LM/g;-><init>(LM/b;)V

    iput-object v0, p0, LM/n;->g:LM/c;

    .line 189
    new-instance v0, LM/D;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-direct {v0, v1}, LM/D;-><init>(LM/b;)V

    iput-object v0, p0, LM/n;->h:LM/c;

    .line 190
    invoke-direct {p0, v2}, LM/n;->b(I)V

    .line 191
    invoke-direct {p0, p1}, LM/n;->a(Landroid/content/Context;)V

    .line 192
    invoke-direct {p0, p1}, LM/n;->b(Landroid/content/Context;)V

    .line 193
    return-void

    :cond_4e
    move v0, v2

    .line 154
    goto :goto_1c
.end method

.method public static final a()F
    .registers 2

    .prologue
    .line 89
    invoke-static {}, LM/n;->b()F

    move-result v0

    const v1, 0x3f2aacda

    mul-float/2addr v0, v1

    return v0
.end method

.method static synthetic a(LM/n;)LM/v;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LM/n;->b:LM/v;

    return-object v0
.end method

.method static synthetic a(LM/n;Landroid/content/Context;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1}, LM/n;->b(Landroid/content/Context;)V

    return-void
.end method

.method private a(LM/v;)V
    .registers 4
    .parameter

    .prologue
    .line 443
    invoke-virtual {p0}, LM/n;->g()V

    .line 444
    iput-object p1, p0, LM/n;->b:LM/v;

    .line 445
    iget-object v0, p0, LM/n;->e:LM/f;

    iget-object v1, p0, LM/n;->b:LM/v;

    invoke-virtual {v0, v1}, LM/f;->a(LM/v;)V

    .line 446
    invoke-virtual {p0}, LM/n;->e()V

    .line 447
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 196
    new-instance v0, LM/o;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1, p1}, LM/o;-><init>(LM/n;Landroid/os/Handler;Landroid/content/Context;)V

    invoke-static {p1, v0}, LaH/x;->a(Landroid/content/Context;Landroid/database/ContentObserver;)V

    .line 208
    return-void
.end method

.method public static final b()F
    .registers 1

    .prologue
    .line 97
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, LR/m;->n()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method static synthetic b(LM/n;)LM/c;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LM/n;->i:LM/c;

    return-object v0
.end method

.method private b(I)V
    .registers 6
    .parameter

    .prologue
    .line 461
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    .line 462
    new-instance v1, LM/f;

    iget-object v2, p0, LM/n;->c:LM/j;

    iget-object v3, p0, LM/n;->b:LM/v;

    invoke-direct {v1, v2, v3, v0}, LM/f;-><init>(LM/b;LM/v;Lcom/google/googlenav/common/a;)V

    iput-object v1, p0, LM/n;->e:LM/f;

    .line 463
    new-instance v1, LM/r;

    iget-object v2, p0, LM/n;->c:LM/j;

    invoke-direct {v1, p0, v2}, LM/r;-><init>(LM/n;LM/b;)V

    iput-object v1, p0, LM/n;->d:LM/r;

    .line 464
    new-instance v1, LM/E;

    iget-object v2, p0, LM/n;->c:LM/j;

    iget-object v3, p0, LM/n;->c:LM/j;

    invoke-virtual {v3}, LM/j;->c()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, LM/E;-><init>(LM/b;Landroid/os/Handler;Lcom/google/googlenav/common/a;)V

    iput-object v1, p0, LM/n;->j:LM/E;

    .line 466
    new-instance v0, LM/a;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-direct {v0, v1}, LM/a;-><init>(LM/b;)V

    iput-object v0, p0, LM/n;->f:LM/a;

    .line 488
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "gps"

    iget-object v2, p0, LM/n;->e:LM/f;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 489
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_gps_fixup"

    iget-object v2, p0, LM/n;->d:LM/r;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 490
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "network"

    iget-object v2, p0, LM/n;->d:LM/r;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 491
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_base_location"

    iget-object v2, p0, LM/n;->f:LM/a;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 493
    if-nez p1, :cond_8a

    .line 494
    iget-object v0, p0, LM/n;->g:LM/c;

    iput-object v0, p0, LM/n;->i:LM/c;

    .line 495
    invoke-direct {p0}, LM/n;->m()V

    .line 500
    :goto_61
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_bearing_noise_reduction"

    iget-object v2, p0, LM/n;->i:LM/c;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 507
    new-instance v0, LM/B;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-direct {v0, v1}, LM/B;-><init>(LM/b;)V

    iput-object v0, p0, LM/n;->k:LM/B;

    .line 508
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "android_orientation"

    iget-object v2, p0, LM/n;->k:LM/B;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 509
    iget-object v0, p0, LM/n;->c:LM/j;

    iget-object v1, p0, LM/n;->i:LM/c;

    invoke-interface {v1}, LM/c;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LM/n;->k:LM/B;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 510
    return-void

    .line 497
    :cond_8a
    iget-object v0, p0, LM/n;->h:LM/c;

    iput-object v0, p0, LM/n;->i:LM/c;

    goto :goto_61
.end method

.method private b(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 211
    invoke-static {p1}, LaH/x;->c(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, LM/n;->o:Z

    .line 212
    iget-boolean v0, p0, LM/n;->o:Z

    if-eqz v0, :cond_12

    iget-boolean v0, p0, LM/n;->q:Z

    if-nez v0, :cond_12

    .line 213
    invoke-virtual {p0}, LM/n;->g()V

    .line 217
    :cond_11
    :goto_11
    return-void

    .line 214
    :cond_12
    iget-boolean v0, p0, LM/n;->o:Z

    if-nez v0, :cond_11

    iget-boolean v0, p0, LM/n;->q:Z

    if-eqz v0, :cond_11

    .line 215
    invoke-virtual {p0}, LM/n;->e()V

    goto :goto_11
.end method

.method static synthetic c(LM/n;)LM/f;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LM/n;->e:LM/f;

    return-object v0
.end method

.method private c(I)V
    .registers 6
    .parameter

    .prologue
    .line 705
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_bearing_noise_reduction"

    iget-object v2, p0, LM/n;->i:LM/c;

    invoke-virtual {v0, v1, v2}, LM/j;->b(Ljava/lang/String;LM/b;)V

    .line 707
    iget-object v0, p0, LM/n;->c:LM/j;

    iget-object v1, p0, LM/n;->i:LM/c;

    invoke-interface {v1}, LM/c;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LM/n;->k:LM/B;

    invoke-virtual {v0, v1, v2}, LM/j;->b(Ljava/lang/String;LM/b;)V

    .line 712
    iget-object v0, p0, LM/n;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/b;

    .line 713
    iget-object v2, p0, LM/n;->c:LM/j;

    iget-object v3, p0, LM/n;->i:LM/c;

    invoke-interface {v3}, LM/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LM/j;->b(Ljava/lang/String;LM/b;)V

    goto :goto_1c

    .line 718
    :cond_34
    if-nez p1, :cond_71

    .line 719
    iget-object v0, p0, LM/n;->g:LM/c;

    iput-object v0, p0, LM/n;->i:LM/c;

    .line 720
    invoke-direct {p0}, LM/n;->m()V

    .line 727
    :goto_3d
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_bearing_noise_reduction"

    iget-object v2, p0, LM/n;->i:LM/c;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 729
    iget-object v0, p0, LM/n;->c:LM/j;

    iget-object v1, p0, LM/n;->i:LM/c;

    invoke-interface {v1}, LM/c;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LM/n;->k:LM/B;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 734
    iget-object v0, p0, LM/n;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_59
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_79

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/b;

    .line 735
    iget-object v2, p0, LM/n;->c:LM/j;

    iget-object v3, p0, LM/n;->i:LM/c;

    invoke-interface {v3}, LM/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LM/j;->a(Ljava/lang/String;LM/b;)V

    goto :goto_59

    .line 722
    :cond_71
    invoke-direct {p0}, LM/n;->n()V

    .line 723
    iget-object v0, p0, LM/n;->h:LM/c;

    iput-object v0, p0, LM/n;->i:LM/c;

    goto :goto_3d

    .line 737
    :cond_79
    return-void
.end method

.method private m()V
    .registers 4

    .prologue
    .line 514
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_hmm"

    iget-object v2, p0, LM/n;->j:LM/E;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 515
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "da_tunnel_heartbeat"

    iget-object v2, p0, LM/n;->i:LM/c;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 516
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_hmm"

    iget-object v2, p0, LM/n;->e:LM/f;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 517
    return-void
.end method

.method private n()V
    .registers 4

    .prologue
    .line 521
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_hmm"

    iget-object v2, p0, LM/n;->j:LM/E;

    invoke-virtual {v0, v1, v2}, LM/j;->b(Ljava/lang/String;LM/b;)V

    .line 522
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "da_tunnel_heartbeat"

    iget-object v2, p0, LM/n;->i:LM/c;

    invoke-virtual {v0, v1, v2}, LM/j;->b(Ljava/lang/String;LM/b;)V

    .line 523
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_hmm"

    iget-object v2, p0, LM/n;->e:LM/f;

    invoke-virtual {v0, v1, v2}, LM/j;->b(Ljava/lang/String;LM/b;)V

    .line 524
    return-void
.end method

.method private o()V
    .registers 11

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 537
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 539
    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->a()Ljava/util/List;

    move-result-object v0

    const-string v4, "gps"

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_70

    .line 540
    iget-object v0, p0, LM/n;->b:LM/v;

    const-string v4, "gps"

    invoke-interface {v0, v4}, LM/v;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 542
    :goto_1c
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v4

    invoke-virtual {v4}, LR/m;->k()I

    move-result v4

    .line 543
    if-eqz v0, :cond_60

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v5

    sub-long v5, v2, v5

    int-to-long v7, v4

    cmp-long v4, v5, v7

    if-gez v4, :cond_60

    .line 544
    iget-object v4, p0, LM/n;->c:LM/j;

    invoke-virtual {v4, v0}, LM/j;->onLocationChanged(Landroid/location/Location;)V

    .line 551
    :goto_36
    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->a()Ljava/util/List;

    move-result-object v0

    const-string v4, "network"

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 552
    iget-object v0, p0, LM/n;->b:LM/v;

    const-string v4, "network"

    invoke-interface {v0, v4}, LM/v;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 554
    :goto_4c
    if-eqz v0, :cond_66

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xea60

    cmp-long v2, v2, v4

    if-gez v2, :cond_66

    .line 556
    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-virtual {v1, v0}, LM/j;->onLocationChanged(Landroid/location/Location;)V

    .line 561
    :goto_5f
    return-void

    .line 546
    :cond_60
    iget-object v0, p0, LM/n;->c:LM/j;

    invoke-virtual {v0, v9, v1}, LM/j;->a(ILandroid/os/Bundle;)V

    goto :goto_36

    .line 558
    :cond_66
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v2, "network"

    invoke-virtual {v0, v2, v9, v1}, LM/j;->onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V

    goto :goto_5f

    :cond_6e
    move-object v0, v1

    goto :goto_4c

    :cond_70
    move-object v0, v1

    goto :goto_1c
.end method


# virtual methods
.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 685
    iget v0, p0, LM/n;->n:I

    if-eq v0, p1, :cond_9

    .line 686
    invoke-direct {p0, p1}, LM/n;->c(I)V

    .line 687
    iput p1, p0, LM/n;->n:I

    .line 690
    :cond_9
    iget-object v0, p0, LM/n;->c:LM/j;

    new-instance v1, LM/q;

    invoke-direct {v1, p0, p1}, LM/q;-><init>(LM/n;I)V

    invoke-virtual {v0, v1}, LM/j;->a(Ljava/lang/Runnable;)V

    .line 696
    return-void
.end method

.method public a(J)V
    .registers 9
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 360
    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->a()Ljava/util/List;

    move-result-object v0

    const-string v1, "gps"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 361
    iget-object v0, p0, LM/n;->b:LM/v;

    const-string v1, "gps"

    iget-object v5, p0, LM/n;->c:LM/j;

    move-wide v2, p1

    invoke-interface/range {v0 .. v5}, LM/v;->a(Ljava/lang/String;JFLM/b;)V

    .line 366
    :cond_19
    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->a()Ljava/util/List;

    move-result-object v0

    const-string v1, "network"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 367
    iget-object v0, p0, LM/n;->b:LM/v;

    const-string v1, "network"

    iget-object v5, p0, LM/n;->c:LM/j;

    move-wide v2, p1

    invoke-interface/range {v0 .. v5}, LM/v;->a(Ljava/lang/String;JFLM/b;)V

    .line 372
    :cond_31
    return-void
.end method

.method public a(LL/G;)V
    .registers 3
    .parameter

    .prologue
    .line 430
    invoke-virtual {p1}, LL/G;->a()LM/z;

    move-result-object v0

    invoke-direct {p0, v0}, LM/n;->a(LM/v;)V

    .line 431
    return-void
.end method

.method public a(LM/b;)V
    .registers 4
    .parameter

    .prologue
    .line 279
    iget-object v0, p0, LM/n;->c:LM/j;

    iget-object v1, p0, LM/n;->i:LM/c;

    invoke-interface {v1}, LM/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 280
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_orientation_filter"

    invoke-virtual {v0, v1, p1}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 281
    iget-object v0, p0, LM/n;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    return-void
.end method

.method public a(LM/s;)V
    .registers 3
    .parameter

    .prologue
    .line 741
    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0, p1}, LM/v;->a(LM/s;)V

    .line 742
    return-void
.end method

.method public a(LO/z;)V
    .registers 4
    .parameter

    .prologue
    .line 675
    iget-object v0, p0, LM/n;->c:LM/j;

    new-instance v1, LM/p;

    invoke-direct {v1, p0, p1}, LM/p;-><init>(LM/n;LO/z;)V

    invoke-virtual {v0, v1}, LM/j;->a(Ljava/lang/Runnable;)V

    .line 681
    return-void
.end method

.method public a(Ljava/lang/String;LM/b;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 301
    iget-object v0, p0, LM/n;->c:LM/j;

    invoke-virtual {v0, p1, p2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    .line 302
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 1021
    iget-object v0, p0, LM/n;->i:LM/c;

    invoke-interface {v0}, LM/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 271
    iget-object v0, p0, LM/n;->d:LM/r;

    invoke-virtual {v0}, LM/r;->b()Z

    move-result v0

    return v0
.end method

.method public d()V
    .registers 2

    .prologue
    .line 319
    const/4 v0, 0x1

    iput-boolean v0, p0, LM/n;->p:Z

    .line 320
    invoke-virtual {p0}, LM/n;->e()V

    .line 321
    return-void
.end method

.method public e()V
    .registers 3

    .prologue
    .line 327
    iget-boolean v0, p0, LM/n;->o:Z

    if-nez v0, :cond_c

    iget-boolean v0, p0, LM/n;->p:Z

    if-eqz v0, :cond_c

    iget-boolean v0, p0, LM/n;->q:Z

    if-nez v0, :cond_d

    .line 344
    :cond_c
    :goto_c
    return-void

    .line 331
    :cond_d
    iget-object v0, p0, LM/n;->d:LM/r;

    invoke-virtual {v0}, LM/r;->a()V

    .line 332
    iget-object v0, p0, LM/n;->j:LM/E;

    invoke-virtual {v0}, LM/E;->b()V

    .line 336
    invoke-direct {p0}, LM/n;->o()V

    .line 337
    invoke-virtual {p0}, LM/n;->f()V

    .line 339
    iget-object v0, p0, LM/n;->b:LM/v;

    iget-object v1, p0, LM/n;->l:LM/u;

    invoke-interface {v0, v1}, LM/v;->a(Landroid/location/GpsStatus$Listener;)Z

    .line 341
    iget-object v0, p0, LM/n;->b:LM/v;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-interface {v0, v1}, LM/v;->b(LM/b;)V

    .line 343
    const/4 v0, 0x0

    iput-boolean v0, p0, LM/n;->q:Z

    goto :goto_c
.end method

.method public f()V
    .registers 3

    .prologue
    .line 350
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    invoke-virtual {v0}, LR/m;->l()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, LM/n;->a(J)V

    .line 351
    return-void
.end method

.method public g()V
    .registers 3

    .prologue
    .line 380
    iget-boolean v0, p0, LM/n;->q:Z

    if-eqz v0, :cond_5

    .line 390
    :goto_4
    return-void

    .line 383
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, LM/n;->q:Z

    .line 384
    iget-object v0, p0, LM/n;->c:LM/j;

    invoke-virtual {v0}, LM/j;->d()V

    .line 385
    iget-object v0, p0, LM/n;->j:LM/E;

    invoke-virtual {v0}, LM/E;->a()V

    .line 386
    iget-object v0, p0, LM/n;->e:LM/f;

    invoke-virtual {v0}, LM/f;->a()V

    .line 387
    invoke-virtual {p0}, LM/n;->h()V

    .line 388
    iget-object v0, p0, LM/n;->b:LM/v;

    iget-object v1, p0, LM/n;->l:LM/u;

    invoke-interface {v0, v1}, LM/v;->b(Landroid/location/GpsStatus$Listener;)V

    .line 389
    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->b()V

    goto :goto_4
.end method

.method public h()V
    .registers 3

    .prologue
    .line 396
    iget-object v0, p0, LM/n;->b:LM/v;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-interface {v0, v1}, LM/v;->a(LM/b;)V

    .line 397
    return-void
.end method

.method public i()V
    .registers 2

    .prologue
    .line 404
    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->f()V

    .line 405
    return-void
.end method

.method public j()V
    .registers 2

    .prologue
    .line 412
    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->e()V

    .line 413
    return-void
.end method

.method public k()V
    .registers 3

    .prologue
    .line 419
    iget-object v0, p0, LM/n;->b:LM/v;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-interface {v0, v1}, LM/v;->a(LM/b;)V

    .line 420
    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->b()V

    .line 421
    iget-object v0, p0, LM/n;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 422
    iget-object v0, p0, LM/n;->c:LM/j;

    invoke-virtual {v0}, LM/j;->b()V

    .line 423
    return-void
.end method

.method public l()V
    .registers 4

    .prologue
    .line 438
    new-instance v0, LM/x;

    iget-object v1, p0, LM/n;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LM/x;-><init>(Landroid/content/Context;LM/o;)V

    invoke-direct {p0, v0}, LM/n;->a(LM/v;)V

    .line 439
    return-void
.end method
