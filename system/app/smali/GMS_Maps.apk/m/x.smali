.class LM/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LM/v;


# instance fields
.field private final a:Landroid/location/LocationManager;

.field private b:Landroid/location/GpsStatus;

.field private final c:LM/w;

.field private final d:LaV/h;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter

    .prologue
    .line 869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 865
    new-instance v0, LM/w;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LM/w;-><init>(LM/o;)V

    iput-object v0, p0, LM/x;->c:LM/w;

    .line 870
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    .line 873
    new-instance v0, LM/y;

    invoke-direct {v0, p0}, LM/y;-><init>(LM/x;)V

    .line 893
    new-instance v1, LaV/a;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LaV/a;-><init>(LaV/e;Lcom/google/googlenav/common/a;)V

    .line 895
    invoke-virtual {v1, p1}, LaV/a;->a(Landroid/content/Context;)V

    .line 896
    iput-object v1, p0, LM/x;->d:LaV/h;

    .line 897
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;LM/o;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 862
    invoke-direct {p0, p1}, LM/x;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(LM/x;)Landroid/location/LocationManager;
    .registers 2
    .parameter

    .prologue
    .line 862
    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/location/Location;
    .registers 3
    .parameter

    .prologue
    .line 901
    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/List;
    .registers 2

    .prologue
    .line 906
    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(LM/b;)V
    .registers 3
    .parameter

    .prologue
    .line 917
    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 918
    return-void
.end method

.method public a(LM/s;)V
    .registers 3
    .parameter

    .prologue
    .line 987
    iget-object v0, p0, LM/x;->c:LM/w;

    invoke-virtual {v0, p1}, LM/w;->a(LM/s;)V

    .line 988
    return-void
.end method

.method public a(Ljava/lang/String;JFLM/b;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 928
    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 929
    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 932
    :cond_15
    return-void
.end method

.method public a(Landroid/location/GpsStatus$Listener;)Z
    .registers 3
    .parameter

    .prologue
    .line 942
    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .registers 3

    .prologue
    .line 922
    iget-object v0, p0, LM/x;->d:LaV/h;

    iget-object v1, p0, LM/x;->c:LM/w;

    invoke-virtual {v0, v1}, LaV/h;->b(LaV/i;)V

    .line 923
    return-void
.end method

.method public b(LM/b;)V
    .registers 4
    .parameter

    .prologue
    .line 936
    iget-object v0, p0, LM/x;->c:LM/w;

    invoke-virtual {v0, p1}, LM/w;->a(LM/b;)V

    .line 937
    iget-object v0, p0, LM/x;->d:LaV/h;

    iget-object v1, p0, LM/x;->c:LM/w;

    invoke-virtual {v0, v1}, LaV/h;->a(LaV/i;)V

    .line 938
    return-void
.end method

.method public b(Landroid/location/GpsStatus$Listener;)V
    .registers 3
    .parameter

    .prologue
    .line 947
    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    .line 948
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 911
    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, LM/x;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public c()LM/t;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 953
    iget-object v1, p0, LM/x;->a:Landroid/location/LocationManager;

    iget-object v2, p0, LM/x;->b:Landroid/location/GpsStatus;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getGpsStatus(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v1

    iput-object v1, p0, LM/x;->b:Landroid/location/GpsStatus;

    .line 956
    iget-object v1, p0, LM/x;->b:Landroid/location/GpsStatus;

    invoke-virtual {v1}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    move v2, v0

    :goto_17
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/GpsSatellite;

    .line 957
    add-int/lit8 v2, v2, 0x1

    .line 958
    invoke-virtual {v0}, Landroid/location/GpsSatellite;->usedInFix()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 959
    add-int/lit8 v0, v1, 0x1

    :goto_2d
    move v1, v0

    goto :goto_17

    .line 962
    :cond_2f
    new-instance v0, LM/t;

    invoke-direct {v0, v2, v1}, LM/t;-><init>(II)V

    return-object v0

    :cond_35
    move v0, v1

    goto :goto_2d
.end method

.method public d()F
    .registers 2

    .prologue
    .line 967
    iget-object v0, p0, LM/x;->d:LaV/h;

    invoke-virtual {v0}, LaV/h;->c()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, LM/x;->d:LaV/h;

    invoke-virtual {v0}, LaV/h;->d()F

    move-result v0

    :goto_e
    return v0

    :cond_f
    const/high16 v0, -0x4080

    goto :goto_e
.end method

.method public e()V
    .registers 2

    .prologue
    .line 973
    iget-object v0, p0, LM/x;->d:LaV/h;

    invoke-virtual {v0}, LaV/h;->h()V

    .line 974
    return-void
.end method

.method public f()V
    .registers 3

    .prologue
    .line 978
    iget-object v0, p0, LM/x;->d:LaV/h;

    invoke-virtual {v0}, LaV/h;->i()V

    .line 981
    iget-object v0, p0, LM/x;->d:LaV/h;

    sget-object v1, LaV/j;->c:LaV/j;

    invoke-virtual {v0, v1}, LaV/h;->a(LaV/j;)V

    .line 983
    return-void
.end method
