.class public LaG/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LaB/s;

.field private final b:Ljava/util/Set;

.field private final c:Ljava/util/List;


# direct methods
.method private constructor <init>(LaB/s;Ljava/util/Set;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    iput-object p1, p0, LaG/j;->a:LaB/s;

    .line 177
    iput-object p2, p0, LaG/j;->b:Ljava/util/Set;

    .line 178
    invoke-static {}, Lcom/google/common/collect/bx;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LaG/j;->c:Ljava/util/List;

    .line 179
    return-void
.end method

.method synthetic constructor <init>(LaB/s;Ljava/util/Set;LaG/i;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 162
    invoke-direct {p0, p1, p2}, LaG/j;-><init>(LaB/s;Ljava/util/Set;)V

    return-void
.end method

.method static synthetic a(LaG/j;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 162
    iget-object v0, p0, LaG/j;->c:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(LaB/p;)I
    .registers 11
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 198
    iget-object v0, p0, LaG/j;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    .line 235
    :goto_a
    return v0

    .line 201
    :cond_b
    iget-object v0, p0, LaG/j;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v2, 0x14

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 202
    invoke-static {v2}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 204
    iget-object v0, p0, LaG/j;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 205
    :cond_21
    :goto_21
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 206
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v2, :cond_35

    .line 221
    :cond_2d
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_58

    move v0, v1

    .line 222
    goto :goto_a

    .line 209
    :cond_35
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/m;

    .line 210
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 212
    invoke-static {v0}, LaG/h;->c(LaG/m;)Lcom/google/googlenav/ui/bs;

    move-result-object v5

    .line 213
    if-eqz v5, :cond_21

    .line 216
    iget-object v6, p0, LaG/j;->b:Ljava/util/Set;

    invoke-virtual {v0}, LaG/m;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 219
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_21

    .line 224
    :cond_58
    iget-object v0, p0, LaG/j;->a:LaB/s;

    new-instance v1, LaG/k;

    invoke-direct {v1, p0, p1}, LaG/k;-><init>(LaG/j;LaB/p;)V

    invoke-virtual {v0, v3, v1}, LaB/s;->a(Ljava/lang/Iterable;LaB/p;)V

    .line 235
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_a
.end method

.method public a()V
    .registers 5

    .prologue
    .line 245
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 246
    monitor-enter v1

    .line 247
    :try_start_6
    new-instance v0, LaG/l;

    invoke-direct {v0, p0, v1}, LaG/l;-><init>(LaG/j;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, LaG/j;->a(LaB/p;)I
    :try_end_e
    .catchall {:try_start_6 .. :try_end_e} :catchall_18

    move-result v0

    .line 255
    if-lez v0, :cond_16

    .line 257
    const-wide/16 v2, 0x2710

    :try_start_13
    invoke-virtual {v1, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_16
    .catchall {:try_start_13 .. :try_end_16} :catchall_18
    .catch Ljava/lang/InterruptedException; {:try_start_13 .. :try_end_16} :catch_1b

    .line 262
    :cond_16
    :goto_16
    :try_start_16
    monitor-exit v1

    .line 263
    return-void

    .line 262
    :catchall_18
    move-exception v0

    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_16 .. :try_end_1a} :catchall_18

    throw v0

    .line 258
    :catch_1b
    move-exception v0

    goto :goto_16
.end method

.method public a(LaG/m;)V
    .registers 3
    .parameter

    .prologue
    .line 187
    iget-object v0, p0, LaG/j;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    return-void
.end method
