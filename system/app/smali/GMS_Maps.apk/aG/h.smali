.class public LaG/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lam/f;


# instance fields
.field final b:Ljava/util/EnumMap;

.field final c:Ljava/util/concurrent/ConcurrentMap;

.field private final d:Ljava/util/Set;

.field private final e:LaB/s;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/ui/bi;->a(ZI)Lam/f;

    move-result-object v0

    .line 46
    sget v5, Lcom/google/googlenav/ui/bi;->bF:I

    .line 47
    invoke-interface {v0}, Lam/f;->a()I

    move-result v3

    invoke-interface {v0}, Lam/f;->b()I

    move-result v4

    move v2, v1

    move v6, v5

    move v7, v1

    invoke-static/range {v0 .. v7}, Lam/j;->a(Lam/f;IIIIIII)Lam/f;

    move-result-object v0

    sput-object v0, LaG/h;->a:Lam/f;

    .line 50
    return-void
.end method

.method public constructor <init>(LaB/s;)V
    .registers 8
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, LaG/h;->e:LaB/s;

    .line 70
    invoke-static {}, Lcom/google/common/collect/Maps;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LaG/h;->c:Ljava/util/concurrent/ConcurrentMap;

    .line 71
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LaG/h;->d:Ljava/util/Set;

    .line 73
    const-class v0, LaG/g;

    invoke-static {v0}, Lcom/google/common/collect/Maps;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, LaG/h;->b:Ljava/util/EnumMap;

    .line 74
    invoke-static {}, LaG/g;->values()[LaG/g;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_23
    if-ge v0, v2, :cond_33

    aget-object v3, v1, v0

    .line 75
    iget-object v4, p0, LaG/h;->b:Ljava/util/EnumMap;

    invoke-static {v3}, LaG/d;->a(LaG/g;)LaG/d;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_23

    .line 77
    :cond_33
    return-void
.end method

.method static synthetic c(LaG/m;)Lcom/google/googlenav/ui/bs;
    .registers 2
    .parameter

    .prologue
    .line 34
    invoke-static {p0}, LaG/h;->d(LaG/m;)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    return-object v0
.end method

.method private static d(LaG/m;)Lcom/google/googlenav/ui/bs;
    .registers 4
    .parameter

    .prologue
    .line 150
    if-eqz p0, :cond_c

    invoke-virtual {p0}, LaG/m;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 151
    :cond_c
    const/4 v0, 0x0

    .line 153
    :goto_d
    return-object v0

    :cond_e
    new-instance v0, Lcom/google/googlenav/ui/bs;

    invoke-virtual {p0}, LaG/m;->c()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/googlenav/ui/bi;->bF:I

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    goto :goto_d
.end method


# virtual methods
.method public a(LaG/g;)LaG/d;
    .registers 3
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, LaG/h;->b:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/d;

    return-object v0
.end method

.method public a()LaG/j;
    .registers 5

    .prologue
    .line 114
    new-instance v0, LaG/j;

    iget-object v1, p0, LaG/h;->e:LaB/s;

    iget-object v2, p0, LaG/h;->d:Ljava/util/Set;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LaG/j;-><init>(LaB/s;Ljava/util/Set;LaG/i;)V

    return-object v0
.end method

.method public a(J)LaG/m;
    .registers 5
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, LaG/h;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/m;

    return-object v0
.end method

.method public a(LaG/m;)V
    .registers 5
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, LaG/h;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {p1}, LaG/m;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    return-void
.end method

.method public a(Ljava/util/List;)V
    .registers 4
    .parameter

    .prologue
    .line 104
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/m;

    .line 105
    invoke-virtual {p0, v0}, LaG/h;->a(LaG/m;)V

    goto :goto_4

    .line 107
    :cond_14
    return-void
.end method

.method public b(LaG/m;)Lam/f;
    .registers 4
    .parameter

    .prologue
    .line 142
    invoke-static {p1}, LaG/h;->d(LaG/m;)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    .line 143
    if-nez v0, :cond_8

    .line 144
    const/4 v0, 0x0

    .line 146
    :goto_7
    return-object v0

    :cond_8
    iget-object v1, p0, LaG/h;->e:LaB/s;

    invoke-virtual {v1, v0}, LaB/s;->a(Lcom/google/googlenav/ui/bs;)Lam/f;

    move-result-object v0

    goto :goto_7
.end method

.method public b(Ljava/util/List;)V
    .registers 6
    .parameter

    .prologue
    .line 125
    invoke-virtual {p0}, LaG/h;->a()LaG/j;

    move-result-object v1

    .line 126
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/m;

    .line 127
    invoke-virtual {p0, v0}, LaG/h;->b(LaG/m;)Lam/f;

    move-result-object v3

    if-nez v3, :cond_8

    .line 128
    invoke-virtual {v1, v0}, LaG/j;->a(LaG/m;)V

    goto :goto_8

    .line 131
    :cond_1e
    invoke-virtual {v1}, LaG/j;->a()V

    .line 132
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_25
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/m;

    .line 133
    invoke-virtual {p0, v0}, LaG/h;->b(LaG/m;)Lam/f;

    move-result-object v2

    invoke-virtual {v0, v2}, LaG/m;->a(Lam/f;)V

    goto :goto_25

    .line 135
    :cond_39
    return-void
.end method
