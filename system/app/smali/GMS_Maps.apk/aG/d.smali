.class public LaG/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LaG/g;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/concurrent/ConcurrentMap;

.field private final d:Ljava/util/concurrent/ConcurrentMap;


# direct methods
.method private constructor <init>(LaG/g;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, LaG/d;->a:LaG/g;

    .line 48
    iput-object p2, p0, LaG/d;->b:Ljava/lang/String;

    .line 49
    invoke-static {}, Lcom/google/common/collect/Maps;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LaG/d;->c:Ljava/util/concurrent/ConcurrentMap;

    .line 50
    invoke-static {}, Lcom/google/common/collect/Maps;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LaG/d;->d:Ljava/util/concurrent/ConcurrentMap;

    .line 51
    return-void
.end method

.method public static a(LaG/g;)LaG/d;
    .registers 4
    .parameter

    .prologue
    .line 54
    sget-object v0, LaG/e;->a:[I

    invoke-virtual {p0}, LaG/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_48

    .line 62
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown leaderboard type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :pswitch_24
    new-instance v0, LaG/d;

    const/16 v1, 0x250

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LaG/d;-><init>(LaG/g;Ljava/lang/String;)V

    .line 60
    :goto_2f
    return-object v0

    .line 58
    :pswitch_30
    new-instance v0, LaG/d;

    const/16 v1, 0x24f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LaG/d;-><init>(LaG/g;Ljava/lang/String;)V

    goto :goto_2f

    .line 60
    :pswitch_3c
    new-instance v0, LaG/d;

    const/16 v1, 0x24e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LaG/d;-><init>(LaG/g;Ljava/lang/String;)V

    goto :goto_2f

    .line 54
    :pswitch_data_48
    .packed-switch 0x1
        :pswitch_24
        :pswitch_30
        :pswitch_3c
    .end packed-switch
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 71
    iget-object v0, p0, LaG/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method declared-synchronized a(LaG/f;)V
    .registers 7
    .parameter

    .prologue
    .line 101
    monitor-enter p0

    :try_start_1
    invoke-static {p1}, LaG/f;->b(LaG/f;)I

    move-result v1

    .line 102
    iget-object v0, p0, LaG/d;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/f;

    .line 103
    if-eqz v0, :cond_20

    .line 104
    iget-object v2, p0, LaG/d;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {v0}, LaG/f;->i()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    :cond_20
    invoke-virtual {p1}, LaG/f;->i()J

    move-result-wide v2

    .line 108
    iget-object v0, p0, LaG/d;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 109
    if-eqz v0, :cond_37

    .line 110
    iget-object v4, p0, LaG/d;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    :cond_37
    iget-object v0, p0, LaG/d;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4, p1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    iget-object v0, p0, LaG/d;->d:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4d
    .catchall {:try_start_1 .. :try_end_4d} :catchall_4f

    .line 115
    monitor-exit p0

    return-void

    .line 101
    :catchall_4f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/List;)V
    .registers 4
    .parameter

    .prologue
    .line 118
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaG/f;

    .line 119
    invoke-virtual {p0, v0}, LaG/d;->a(LaG/f;)V

    goto :goto_4

    .line 121
    :cond_14
    return-void
.end method

.method public b()Ljava/util/List;
    .registers 3

    .prologue
    .line 76
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LaG/d;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 78
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 79
    return-object v0
.end method

.method public declared-synchronized c()I
    .registers 2

    .prologue
    .line 84
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaG/d;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->size()I
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    move-result v0

    monitor-exit p0

    return v0

    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method
