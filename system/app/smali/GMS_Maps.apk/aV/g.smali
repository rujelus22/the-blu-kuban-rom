.class public LaV/g;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(LaH/m;LaN/B;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 67
    if-eqz p1, :cond_5

    if-nez p0, :cond_6

    .line 75
    :cond_5
    :goto_5
    return v0

    .line 70
    :cond_6
    invoke-static {p0}, LaV/g;->a(LaH/m;)LaN/B;

    move-result-object v1

    .line 71
    if-eqz v1, :cond_5

    .line 75
    invoke-static {v1, p1}, Lcom/google/googlenav/ui/l;->a(LaN/B;LaN/B;)I

    move-result v0

    goto :goto_5
.end method

.method private static a(LaH/m;)LaN/B;
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-interface {p0}, LaH/m;->g()Z

    move-result v1

    if-nez v1, :cond_8

    .line 56
    :cond_7
    :goto_7
    return-object v0

    .line 52
    :cond_8
    invoke-interface {p0}, LaH/m;->s()LaH/h;

    move-result-object v1

    .line 53
    if-eqz v1, :cond_7

    .line 54
    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v0

    goto :goto_7
.end method

.method public static a(Lcom/google/googlenav/ui/view/android/DistanceView;Lcom/google/googlenav/ui/view/android/HeadingView;LaN/B;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 87
    .line 89
    if-eqz p2, :cond_25

    .line 90
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v1

    .line 91
    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    .line 94
    :goto_b
    if-eqz p0, :cond_16

    .line 95
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/DistanceView;->setLocationProvider(LaH/m;)V

    .line 96
    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/view/android/DistanceView;->setInitialVisibility(LaN/B;)V

    .line 97
    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/view/android/DistanceView;->setDestination(LaN/B;)V

    .line 100
    :cond_16
    if-eqz p1, :cond_24

    .line 101
    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->setOrientationProvider(LaV/h;)V

    .line 102
    invoke-virtual {p1, v1}, Lcom/google/googlenav/ui/view/android/HeadingView;->setLocationProvider(LaH/m;)V

    .line 103
    invoke-virtual {p1, p2}, Lcom/google/googlenav/ui/view/android/HeadingView;->setInitialVisibility(LaN/B;)V

    .line 104
    invoke-virtual {p1, p2}, Lcom/google/googlenav/ui/view/android/HeadingView;->setDestination(LaN/B;)V

    .line 106
    :cond_24
    return-void

    :cond_25
    move-object v1, v0

    goto :goto_b
.end method
