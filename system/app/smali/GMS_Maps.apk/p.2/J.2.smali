.class public Lp/J;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lo/Q;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:F

.field private final j:F

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/String;

.field private final n:Landroid/text/Spanned;

.field private final o:Lp/L;

.field private final p:Ljava/util/Map;

.field private final q:Ljava/util/List;

.field private final r:Ljava/util/List;

.field private final s:Ljava/util/List;

.field private final t:Ljava/util/List;

.field private final u:Ljava/util/List;

.field private final v:Ljava/util/List;

.field private final w:Ljava/util/List;

.field private x:Lp/J;

.field private y:Lp/J;


# direct methods
.method protected constructor <init>(IIILo/Q;IILjava/lang/String;IIFFLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .registers 24
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387
    iput p1, p0, Lp/J;->b:I

    .line 388
    iput p2, p0, Lp/J;->c:I

    .line 389
    iput p3, p0, Lp/J;->d:I

    .line 390
    iput-object p4, p0, Lp/J;->a:Lo/Q;

    .line 391
    iput p5, p0, Lp/J;->e:I

    .line 392
    iput p6, p0, Lp/J;->f:I

    .line 393
    iput p8, p0, Lp/J;->g:I

    .line 394
    iput p9, p0, Lp/J;->h:I

    .line 395
    iput p10, p0, Lp/J;->i:F

    .line 396
    move/from16 v0, p11

    iput v0, p0, Lp/J;->j:F

    .line 398
    move-object/from16 v0, p12

    iput-object v0, p0, Lp/J;->k:Ljava/lang/String;

    .line 399
    move-object/from16 v0, p13

    iput-object v0, p0, Lp/J;->l:Ljava/lang/String;

    .line 400
    move-object/from16 v0, p14

    iput-object v0, p0, Lp/J;->m:Ljava/lang/String;

    .line 401
    move-object/from16 v0, p16

    iput-object v0, p0, Lp/J;->u:Ljava/util/List;

    .line 402
    move-object/from16 v0, p17

    iput-object v0, p0, Lp/J;->v:Ljava/util/List;

    .line 403
    move-object/from16 v0, p18

    iput-object v0, p0, Lp/J;->w:Ljava/util/List;

    .line 405
    iget-object v1, p0, Lp/J;->v:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_37
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_47

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lp/j;

    .line 406
    invoke-virtual {v1, p0}, Lp/j;->a(Lp/J;)V

    goto :goto_37

    .line 409
    :cond_47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lp/J;->q:Ljava/util/List;

    .line 410
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lp/J;->r:Ljava/util/List;

    .line 411
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lp/J;->s:Ljava/util/List;

    .line 412
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lp/J;->t:Ljava/util/List;

    .line 414
    move-object/from16 v0, p15

    invoke-static {p0, v0}, Lp/J;->a(Lp/J;Ljava/util/List;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lp/J;->p:Ljava/util/Map;

    .line 415
    iget-object v1, p0, Lp/J;->p:Ljava/util/Map;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lp/J;->q:Ljava/util/List;

    invoke-static {v1, v2}, Lp/J;->a(Ljava/util/List;Ljava/util/List;)V

    .line 416
    iget-object v1, p0, Lp/J;->p:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lp/J;->r:Ljava/util/List;

    invoke-static {v1, v2}, Lp/J;->a(Ljava/util/List;Ljava/util/List;)V

    .line 417
    iget-object v1, p0, Lp/J;->p:Ljava/util/Map;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lp/J;->r:Ljava/util/List;

    invoke-static {v1, v2}, Lp/J;->a(Ljava/util/List;Ljava/util/List;)V

    .line 418
    iget-object v1, p0, Lp/J;->p:Ljava/util/Map;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lp/J;->s:Ljava/util/List;

    invoke-static {v1, v2}, Lp/J;->a(Ljava/util/List;Ljava/util/List;)V

    .line 419
    iget-object v1, p0, Lp/J;->p:Ljava/util/Map;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lp/J;->t:Ljava/util/List;

    invoke-static {v1, v2}, Lp/J;->a(Ljava/util/List;Ljava/util/List;)V

    .line 420
    iget v1, p0, Lp/J;->b:I

    const/16 v2, 0x10

    if-ne v1, v2, :cond_10d

    .line 421
    iget-object v1, p0, Lp/J;->p:Ljava/util/Map;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 422
    if-eqz v1, :cond_eb

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_eb

    .line 423
    iget-object v2, p0, Lp/J;->q:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 425
    :cond_eb
    iget-object v1, p0, Lp/J;->p:Ljava/util/Map;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 426
    if-eqz v1, :cond_10d

    .line 427
    const/4 v2, 0x0

    :goto_fb
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_10d

    .line 428
    iget-object v3, p0, Lp/J;->r:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 427
    add-int/lit8 v2, v2, 0x1

    goto :goto_fb

    .line 433
    :cond_10d
    iget-object v1, p0, Lp/J;->p:Ljava/util/Map;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 434
    if-eqz v1, :cond_134

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_134

    .line 435
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lp/L;

    iput-object v1, p0, Lp/J;->o:Lp/L;

    .line 439
    :goto_12b
    iget-object v1, p0, Lp/J;->p:Ljava/util/Map;

    invoke-static {p7, v1}, Lp/J;->a(Ljava/lang/String;Ljava/util/Map;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p0, Lp/J;->n:Landroid/text/Spanned;

    .line 440
    return-void

    .line 437
    :cond_134
    const/4 v1, 0x0

    iput-object v1, p0, Lp/J;->o:Lp/L;

    goto :goto_12b
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 914
    .line 915
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 917
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 919
    if-ltz v0, :cond_12

    const/16 v2, 0x14

    if-lt v0, v2, :cond_13

    :cond_12
    move v0, v1

    .line 923
    :cond_13
    :goto_13
    return v0

    :cond_14
    move v0, v1

    goto :goto_13
.end method

.method protected static a(Ljava/lang/String;Ljava/util/Map;)Landroid/text/Spanned;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 581
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 586
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v0, v1, v3}, Ljava/text/Bidi;->requiresBidi([CII)Z

    move-result v0

    if-eqz v0, :cond_16

    move-object v0, v2

    .line 622
    :goto_15
    return-object v0

    .line 590
    :cond_16
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 591
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_23
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 592
    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_23

    .line 596
    :cond_33
    new-instance v0, Lp/K;

    invoke-direct {v0}, Lp/K;-><init>()V

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 604
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v3, v1

    .line 605
    :goto_40
    if-ge v3, v5, :cond_7e

    .line 606
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp/L;

    .line 607
    invoke-virtual {v0}, Lp/L;->b()Ljava/lang/String;

    move-result-object v6

    .line 608
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_56

    .line 605
    :cond_52
    :goto_52
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_40

    .line 611
    :cond_56
    const/4 v1, -0x1

    .line 613
    :cond_57
    invoke-virtual {v0}, Lp/L;->b()Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v7, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 616
    if-ltz v1, :cond_71

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v7, v1

    const-class v8, Ljava/lang/Object;

    invoke-virtual {v2, v1, v7, v8}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v7

    array-length v7, v7

    if-nez v7, :cond_57

    .line 617
    :cond_71
    if-ltz v1, :cond_52

    .line 618
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v1

    const/16 v7, 0x21

    invoke-virtual {v2, v0, v1, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_52

    :cond_7e
    move-object v0, v2

    .line 622
    goto :goto_15
.end method

.method private static a(Lp/Q;Ljava/util/List;)Ljava/lang/String;
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 557
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 560
    invoke-virtual {p0}, Lp/Q;->e()Ljava/lang/String;

    move-result-object v2

    .line 561
    if-eqz v2, :cond_1b

    .line 562
    new-instance v0, Lp/L;

    const/4 v1, 0x6

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Lp/L;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 563
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    :cond_1b
    invoke-virtual {p0}, Lp/Q;->d()Lp/R;

    move-result-object v9

    .line 567
    if-eqz v9, :cond_4d

    move v7, v6

    .line 568
    :goto_22
    invoke-virtual {v9}, Lp/R;->a()I

    move-result v0

    if-ge v7, v0, :cond_4d

    .line 569
    new-instance v0, Lp/L;

    const/4 v1, 0x7

    invoke-virtual {v9, v7}, Lp/R;->a(I)Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Lp/L;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 570
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_42

    .line 571
    const/16 v0, 0xa

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 573
    :cond_42
    invoke-virtual {v9, v7}, Lp/R;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 568
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_22

    .line 576
    :cond_4d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/Q;)Ljava/util/List;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x6

    .line 961
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 962
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 964
    invoke-virtual {p1}, Lo/Q;->e()D

    move-result-wide v3

    .line 965
    const/4 v0, 0x0

    :goto_f
    if-ge v0, v2, :cond_21

    .line 966
    invoke-virtual {p0, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    .line 968
    invoke-static {v5, v3, v4}, Lp/j;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;D)Lp/j;

    move-result-object v5

    .line 969
    if-eqz v5, :cond_1e

    .line 970
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 965
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 973
    :cond_21
    return-object v1
.end method

.method private static a(Lp/J;Ljava/util/List;)Ljava/util/Map;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 632
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 633
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp/L;

    .line 634
    invoke-virtual {v0}, Lp/L;->a()I

    move-result v4

    .line 635
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 636
    if-nez v1, :cond_31

    .line 637
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 638
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 640
    :cond_31
    invoke-static {v0, p0}, Lp/L;->a(Lp/L;Lp/J;)V

    .line 641
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 643
    :cond_38
    return-object v2
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/Q;IIIFFLp/Q;)Lp/J;
    .registers 29
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 464
    const/16 v3, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 465
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 466
    const/16 v3, 0x14

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 467
    const/4 v3, 0x5

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v8

    .line 468
    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v5

    .line 469
    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v9

    .line 470
    const/4 v3, 0x4

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v10

    .line 478
    const/16 v3, 0x13

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_a2

    .line 479
    const/16 v3, 0x13

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 481
    invoke-static {v4}, Lp/J;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v11

    .line 482
    invoke-static {v4}, Lp/J;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v7

    .line 483
    const/16 v3, 0xc

    if-ne v11, v3, :cond_9b

    .line 484
    const/4 v3, 0x7

    const/4 v6, -0x1

    invoke-static {v4, v3, v6}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v3

    .line 492
    :goto_58
    invoke-static {v4}, Lp/J;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v18

    .line 493
    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lp/J;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/Q;)Ljava/util/List;

    move-result-object v19

    .line 494
    invoke-static {v4}, Lp/J;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v20

    .line 496
    const/16 v6, 0x10

    if-ne v11, v6, :cond_9d

    .line 497
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 498
    if-eqz p7, :cond_be

    .line 499
    move-object/from16 v0, p7

    invoke-static {v0, v6}, Lp/J;->a(Lp/Q;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    .line 500
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v12

    if-nez v12, :cond_bc

    :goto_7d
    move-object v5, v4

    move-object v4, v6

    :goto_7f
    move-object/from16 v17, v4

    move v6, v7

    move-object v13, v5

    move v5, v11

    move v7, v3

    .line 518
    :goto_85
    new-instance v3, Lp/N;

    move-object/from16 v4, p1

    move/from16 v11, p5

    move/from16 v12, p6

    invoke-direct/range {v3 .. v20}, Lp/N;-><init>(Lo/Q;IIIIIIFFLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 522
    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-static {v3, v0, v1, v2}, Lp/J;->a(Lp/N;III)Lp/J;

    move-result-object v3

    return-object v3

    .line 489
    :cond_9b
    const/4 v3, -0x1

    goto :goto_58

    .line 505
    :cond_9d
    invoke-static {v4}, Lp/J;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/ArrayList;

    move-result-object v4

    goto :goto_7f

    .line 508
    :cond_a2
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 509
    const/4 v3, 0x0

    .line 510
    const/4 v6, 0x0

    .line 511
    const/4 v7, -0x1

    .line 512
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 513
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 514
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object v13, v5

    move v5, v3

    goto :goto_85

    :cond_bc
    move-object v4, v5

    goto :goto_7d

    :cond_be
    move-object v4, v6

    goto :goto_7f
.end method

.method public static a(Lp/N;III)Lp/J;
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 539
    new-instance v0, Lp/J;

    invoke-virtual/range {p0 .. p0}, Lp/N;->b()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lp/N;->c()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lp/N;->d()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lp/N;->a()Lo/Q;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lp/N;->e()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lp/N;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lp/N;->h()F

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lp/N;->i()F

    move-result v11

    invoke-virtual/range {p0 .. p0}, Lp/N;->k()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lp/N;->l()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lp/N;->m()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lp/N;->n()Ljava/util/List;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lp/N;->o()Ljava/util/List;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, Lp/N;->p()Ljava/util/List;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lp/N;->q()Ljava/util/List;

    move-result-object v18

    move/from16 v5, p1

    move/from16 v8, p2

    move/from16 v9, p3

    invoke-direct/range {v0 .. v18}, Lp/J;-><init>(IIILo/Q;IILjava/lang/String;IIFFLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method private static a(Ljava/util/List;Ljava/util/List;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 626
    if-eqz p0, :cond_5

    .line 627
    invoke-interface {p1, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 629
    :cond_5
    return-void
.end method

.method public static a(I)Z
    .registers 2
    .parameter

    .prologue
    .line 999
    const/4 v0, 0x5

    if-eq p0, v0, :cond_10

    const/4 v0, 0x3

    if-eq p0, v0, :cond_10

    const/16 v0, 0xf

    if-eq p0, v0, :cond_10

    const/4 v0, 0x6

    if-eq p0, v0, :cond_10

    const/4 v0, 0x4

    if-ne p0, v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 930
    .line 931
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 933
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 935
    if-ltz v0, :cond_11

    const/4 v2, 0x3

    if-lt v0, v2, :cond_12

    :cond_11
    move v0, v1

    .line 939
    :cond_12
    :goto_12
    return v0

    :cond_13
    move v0, v1

    goto :goto_12
.end method

.method static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .registers 6
    .parameter

    .prologue
    const/4 v4, 0x5

    .line 946
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 947
    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 949
    const/4 v0, 0x0

    :goto_b
    if-ge v0, v2, :cond_1b

    .line 950
    invoke-virtual {p0, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3}, Lp/M;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lp/M;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 949
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 953
    :cond_1b
    return-object v1
.end method

.method static d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .registers 6
    .parameter

    .prologue
    const/16 v4, 0x8

    .line 980
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 981
    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 983
    const/4 v0, 0x0

    :goto_c
    if-ge v0, v2, :cond_1e

    .line 984
    invoke-virtual {p0, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 986
    invoke-static {v3}, Lp/k;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lp/k;

    move-result-object v3

    .line 987
    if-eqz v3, :cond_1b

    .line 988
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 983
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 991
    :cond_1e
    return-object v1
.end method

.method private static e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/ArrayList;
    .registers 6
    .parameter

    .prologue
    const/4 v4, 0x4

    .line 647
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 648
    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 649
    const/4 v0, 0x0

    :goto_b
    if-ge v0, v2, :cond_1b

    .line 650
    invoke-virtual {p0, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 652
    invoke-static {v3}, Lp/L;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lp/L;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 649
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 654
    :cond_1b
    return-object v1
.end method


# virtual methods
.method public a()Lo/Q;
    .registers 2

    .prologue
    .line 661
    iget-object v0, p0, Lp/J;->a:Lo/Q;

    return-object v0
.end method

.method a(Lp/J;)V
    .registers 2
    .parameter

    .prologue
    .line 886
    iput-object p1, p0, Lp/J;->x:Lp/J;

    .line 887
    return-void
.end method

.method public a(Lp/j;)V
    .registers 3
    .parameter

    .prologue
    .line 877
    iget-object v0, p0, Lp/J;->v:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 878
    invoke-virtual {p1, p0}, Lp/j;->a(Lp/J;)V

    .line 879
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 669
    iget v0, p0, Lp/J;->b:I

    return v0
.end method

.method b(Lp/J;)V
    .registers 2
    .parameter

    .prologue
    .line 894
    iput-object p1, p0, Lp/J;->y:Lp/J;

    .line 895
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 677
    iget v0, p0, Lp/J;->c:I

    return v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 685
    iget v0, p0, Lp/J;->d:I

    return v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 693
    iget v0, p0, Lp/J;->g:I

    return v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 701
    iget v0, p0, Lp/J;->h:I

    return v0
.end method

.method public g()F
    .registers 2

    .prologue
    .line 708
    iget v0, p0, Lp/J;->i:F

    return v0
.end method

.method public h()F
    .registers 2

    .prologue
    .line 715
    iget v0, p0, Lp/J;->j:F

    return v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 722
    iget v0, p0, Lp/J;->e:I

    return v0
.end method

.method public j()Lp/J;
    .registers 2

    .prologue
    .line 729
    iget-object v0, p0, Lp/J;->x:Lp/J;

    return-object v0
.end method

.method public k()Lp/J;
    .registers 2

    .prologue
    .line 736
    iget-object v0, p0, Lp/J;->y:Lp/J;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .registers 2

    .prologue
    .line 743
    iget-object v0, p0, Lp/J;->k:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .registers 2

    .prologue
    .line 750
    iget-object v0, p0, Lp/J;->l:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .registers 2

    .prologue
    .line 758
    iget-object v0, p0, Lp/J;->m:Ljava/lang/String;

    return-object v0
.end method

.method public o()Landroid/text/Spanned;
    .registers 2

    .prologue
    .line 766
    iget-object v0, p0, Lp/J;->n:Landroid/text/Spanned;

    return-object v0
.end method

.method public p()Ljava/util/List;
    .registers 2

    .prologue
    .line 775
    iget-object v0, p0, Lp/J;->q:Ljava/util/List;

    return-object v0
.end method

.method public q()Ljava/util/List;
    .registers 2

    .prologue
    .line 784
    iget-object v0, p0, Lp/J;->r:Ljava/util/List;

    return-object v0
.end method

.method public r()Ljava/util/List;
    .registers 2

    .prologue
    .line 796
    iget-object v0, p0, Lp/J;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 797
    iget-object v0, p0, Lp/J;->q:Ljava/util/List;

    .line 799
    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, Lp/J;->s:Ljava/util/List;

    goto :goto_a
.end method

.method public s()Ljava/util/List;
    .registers 2

    .prologue
    .line 808
    iget-object v0, p0, Lp/J;->t:Ljava/util/List;

    return-object v0
.end method

.method public t()Ljava/util/Map;
    .registers 2

    .prologue
    .line 816
    iget-object v0, p0, Lp/J;->p:Ljava/util/Map;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 867
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[idx:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lp/J;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " loc:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lp/J;->a:Lo/Q;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " point:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lp/J;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lp/J;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " side:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lp/J;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " text:\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lp/J;->o()Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\']"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Ljava/util/List;
    .registers 2

    .prologue
    .line 824
    iget-object v0, p0, Lp/J;->u:Ljava/util/List;

    return-object v0
.end method

.method public v()Ljava/util/List;
    .registers 2

    .prologue
    .line 832
    iget-object v0, p0, Lp/J;->v:Ljava/util/List;

    return-object v0
.end method

.method public w()Ljava/util/List;
    .registers 2

    .prologue
    .line 840
    iget-object v0, p0, Lp/J;->w:Ljava/util/List;

    return-object v0
.end method

.method public final x()Lp/L;
    .registers 3

    .prologue
    .line 848
    invoke-virtual {p0}, Lp/J;->r()Ljava/util/List;

    move-result-object v0

    .line 849
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 850
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_15

    .line 851
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp/L;

    .line 853
    :goto_14
    return-object v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public y()Lp/L;
    .registers 2

    .prologue
    .line 862
    iget-object v0, p0, Lp/J;->o:Lp/L;

    return-object v0
.end method

.method public z()I
    .registers 2

    .prologue
    .line 899
    iget v0, p0, Lp/J;->f:I

    return v0
.end method
