.class public Lp/S;
.super Lp/Q;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:D

.field private c:D


# direct methods
.method public constructor <init>(Lp/Q;Lp/y;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lp/Q;-><init>(Lp/Q;)V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lp/S;->a:Z

    .line 25
    invoke-direct {p0, p2}, Lp/S;->a(Lp/y;)V

    .line 26
    return-void
.end method

.method private a(Lp/y;)V
    .registers 7
    .parameter

    .prologue
    const-wide/high16 v3, -0x4010

    .line 35
    invoke-virtual {p0}, Lp/S;->c()Lo/s;

    move-result-object v0

    .line 36
    if-nez v0, :cond_d

    .line 37
    iput-wide v3, p0, Lp/S;->b:D

    .line 38
    iput-wide v3, p0, Lp/S;->c:D

    .line 53
    :goto_c
    return-void

    .line 41
    :cond_d
    invoke-virtual {v0}, Lo/s;->a()I

    move-result v1

    invoke-virtual {v0}, Lo/s;->b()I

    move-result v0

    invoke-static {v1, v0}, Lo/Q;->b(II)Lo/Q;

    move-result-object v0

    .line 43
    const-wide/high16 v1, 0x4059

    invoke-virtual {p1, v0, v1, v2}, Lp/y;->a(Lo/Q;D)Lp/C;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_2e

    .line 47
    invoke-virtual {p1, v0}, Lp/y;->a(Lp/C;)D

    move-result-wide v1

    iput-wide v1, p0, Lp/S;->b:D

    .line 48
    invoke-virtual {p1, v0}, Lp/y;->b(Lp/C;)D

    move-result-wide v0

    iput-wide v0, p0, Lp/S;->c:D

    goto :goto_c

    .line 50
    :cond_2e
    iput-wide v3, p0, Lp/S;->b:D

    .line 51
    iput-wide v3, p0, Lp/S;->c:D

    goto :goto_c
.end method


# virtual methods
.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 68
    iput-boolean p1, p0, Lp/S;->a:Z

    .line 69
    return-void
.end method

.method public m()Z
    .registers 2

    .prologue
    .line 56
    iget-boolean v0, p0, Lp/S;->a:Z

    return v0
.end method

.method public n()D
    .registers 3

    .prologue
    .line 60
    iget-wide v0, p0, Lp/S;->b:D

    return-wide v0
.end method
