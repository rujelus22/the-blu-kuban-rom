.class public Lp/Q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lp/R;

.field private final b:Lo/s;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:LaT/h;

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 160
    invoke-static {p1}, Lp/Q;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lp/R;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lu/e;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/s;

    move-result-object v1

    if-eqz p2, :cond_2b

    :goto_f
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, p2, v2}, Lp/Q;-><init>(Lp/R;Lo/s;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-static {p1}, Lp/Q;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lp/Q;->g:Ljava/lang/String;

    .line 166
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lp/Q;->f:I

    .line 167
    invoke-static {p1}, Lp/Q;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaT/h;

    move-result-object v0

    iput-object v0, p0, Lp/Q;->h:LaT/h;

    .line 168
    return-void

    .line 160
    :cond_2b
    invoke-static {p1}, Lp/Q;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object p2

    goto :goto_f
.end method

.method public constructor <init>(Ljava/lang/String;Lo/s;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 139
    if-nez p1, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-direct {p0, v0, p2, p3, p4}, Lp/Q;-><init>(Lp/R;Lo/s;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    return-void

    .line 139
    :cond_7
    new-instance v0, Lp/R;

    invoke-direct {v0, p1}, Lp/R;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public constructor <init>(Lo/s;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 171
    move-object v0, v1

    check-cast v0, Lp/R;

    invoke-direct {p0, v0, p1, v1, v1}, Lp/Q;-><init>(Lp/R;Lo/s;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    return-void
.end method

.method public constructor <init>(Lp/Q;)V
    .registers 3
    .parameter

    .prologue
    .line 176
    iget-object v0, p1, Lp/Q;->c:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lp/Q;-><init>(Lp/Q;Ljava/lang/String;)V

    .line 177
    return-void
.end method

.method public constructor <init>(Lp/Q;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lp/Q;->i:Z

    .line 184
    iget-object v0, p1, Lp/Q;->a:Lp/R;

    iput-object v0, p0, Lp/Q;->a:Lp/R;

    .line 185
    iget-object v0, p1, Lp/Q;->b:Lo/s;

    iput-object v0, p0, Lp/Q;->b:Lo/s;

    .line 186
    iput-object p2, p0, Lp/Q;->c:Ljava/lang/String;

    .line 187
    iget-object v0, p1, Lp/Q;->d:Ljava/lang/String;

    iput-object v0, p0, Lp/Q;->d:Ljava/lang/String;

    .line 188
    iget-object v0, p1, Lp/Q;->e:Ljava/lang/String;

    iput-object v0, p0, Lp/Q;->e:Ljava/lang/String;

    .line 189
    iget v0, p1, Lp/Q;->f:I

    iput v0, p0, Lp/Q;->f:I

    .line 190
    iget-object v0, p1, Lp/Q;->g:Ljava/lang/String;

    iput-object v0, p0, Lp/Q;->g:Ljava/lang/String;

    .line 191
    iget-object v0, p1, Lp/Q;->h:LaT/h;

    iput-object v0, p0, Lp/Q;->h:LaT/h;

    .line 192
    iget-boolean v0, p1, Lp/Q;->i:Z

    iput-boolean v0, p0, Lp/Q;->i:Z

    .line 193
    return-void
.end method

.method public constructor <init>(Lp/R;Lo/s;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v1, 0x1

    iput-boolean v1, p0, Lp/Q;->i:Z

    .line 145
    if-eqz p1, :cond_1d

    if-eqz p3, :cond_1d

    invoke-virtual {p1}, Lp/R;->a()I

    move-result v1

    if-lez v1, :cond_1d

    invoke-virtual {p1, v2}, Lp/R;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    move-object p3, v0

    .line 149
    :cond_1d
    if-eqz p1, :cond_30

    invoke-virtual {p1}, Lp/R;->a()I

    move-result v1

    if-eqz v1, :cond_30

    :goto_25
    iput-object p1, p0, Lp/Q;->a:Lp/R;

    .line 150
    iput-object p2, p0, Lp/Q;->b:Lo/s;

    .line 151
    iput-object p3, p0, Lp/Q;->c:Ljava/lang/String;

    .line 152
    iput-object p4, p0, Lp/Q;->d:Ljava/lang/String;

    .line 153
    iput v2, p0, Lp/Q;->f:I

    .line 154
    return-void

    :cond_30
    move-object p1, v0

    .line 149
    goto :goto_25
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lp/R;
    .registers 8
    .parameter

    .prologue
    const/4 v6, 0x4

    .line 380
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 381
    if-eqz v1, :cond_3d

    .line 382
    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 383
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 384
    const/4 v0, 0x0

    :goto_12
    if-ge v0, v2, :cond_24

    .line 385
    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v4

    .line 386
    invoke-static {v4}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_21

    .line 387
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    :cond_21
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 390
    :cond_24
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3d

    .line 391
    new-instance v1, Lp/R;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {v1, v0}, Lp/R;-><init>([Ljava/lang/String;)V

    move-object v0, v1

    .line 398
    :goto_3c
    return-object v0

    .line 394
    :cond_3d
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 395
    invoke-static {v1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4e

    .line 396
    new-instance v0, Lp/R;

    invoke-direct {v0, v1}, Lp/R;-><init>(Ljava/lang/String;)V

    goto :goto_3c

    .line 398
    :cond_4e
    const/4 v0, 0x0

    goto :goto_3c
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 403
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 404
    if-eqz v0, :cond_13

    .line 405
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 406
    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 410
    :goto_12
    return-object v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    const/16 v3, 0x77

    .line 418
    const/4 v0, 0x0

    .line 419
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 420
    if-eqz v1, :cond_18

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 421
    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/googlenav/az;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 423
    :cond_18
    return-object v0
.end method

.method private static d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 9
    .parameter

    .prologue
    const/16 v7, 0x9e

    const/4 v0, 0x0

    .line 431
    if-nez p0, :cond_6

    .line 453
    :cond_5
    :goto_5
    return-object v0

    .line 434
    :cond_6
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 436
    if-eqz v2, :cond_5

    .line 438
    invoke-virtual {v2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    .line 440
    const/4 v1, 0x0

    :goto_12
    if-ge v1, v3, :cond_31

    .line 441
    invoke-virtual {v2, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 442
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    const/16 v6, 0x4c

    if-ne v5, v6, :cond_2e

    .line 444
    invoke-static {v4}, Lcom/google/googlenav/au;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/au;

    move-result-object v1

    .line 448
    :goto_25
    if-eqz v1, :cond_5

    .line 449
    sget-object v0, Lbq/bm;->e:Lcom/google/googlenav/common/io/protocol/Extension;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/au;->a(Lcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_5

    .line 440
    :cond_2e
    add-int/lit8 v1, v1, 0x1

    goto :goto_12

    :cond_31
    move-object v1, v0

    goto :goto_25
.end method

.method private static e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaT/h;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 458
    invoke-static {p0}, Lp/Q;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 459
    if-nez v1, :cond_9

    .line 469
    :cond_8
    :goto_8
    return-object v0

    .line 463
    :cond_9
    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-lez v2, :cond_8

    .line 465
    const/4 v0, 0x0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 467
    invoke-static {v0}, LaT/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaT/h;

    move-result-object v0

    goto :goto_8
.end method


# virtual methods
.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 204
    iput p1, p0, Lp/Q;->f:I

    .line 205
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 232
    iput-object p1, p0, Lp/Q;->e:Ljava/lang/String;

    .line 233
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 272
    iput-boolean p1, p0, Lp/Q;->i:Z

    .line 273
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 196
    iget v0, p0, Lp/Q;->f:I

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public b()I
    .registers 2

    .prologue
    .line 200
    iget v0, p0, Lp/Q;->f:I

    return v0
.end method

.method public c()Lo/s;
    .registers 2

    .prologue
    .line 208
    iget-object v0, p0, Lp/Q;->b:Lo/s;

    return-object v0
.end method

.method public d()Lp/R;
    .registers 2

    .prologue
    .line 212
    iget-object v0, p0, Lp/Q;->a:Lp/R;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 216
    iget-object v0, p0, Lp/Q;->c:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 313
    if-ne p0, p1, :cond_5

    .line 368
    :cond_4
    :goto_4
    return v0

    .line 316
    :cond_5
    if-nez p1, :cond_9

    move v0, v1

    .line 317
    goto :goto_4

    .line 319
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 320
    goto :goto_4

    .line 322
    :cond_15
    check-cast p1, Lp/Q;

    .line 323
    iget-object v2, p0, Lp/Q;->a:Lp/R;

    if-nez v2, :cond_21

    .line 324
    iget-object v2, p1, Lp/Q;->a:Lp/R;

    if-eqz v2, :cond_2d

    move v0, v1

    .line 325
    goto :goto_4

    .line 327
    :cond_21
    iget-object v2, p0, Lp/Q;->a:Lp/R;

    iget-object v3, p1, Lp/Q;->a:Lp/R;

    invoke-virtual {v2, v3}, Lp/R;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2d

    move v0, v1

    .line 328
    goto :goto_4

    .line 330
    :cond_2d
    iget v2, p0, Lp/Q;->f:I

    iget v3, p1, Lp/Q;->f:I

    if-eq v2, v3, :cond_35

    move v0, v1

    .line 331
    goto :goto_4

    .line 333
    :cond_35
    iget-object v2, p0, Lp/Q;->b:Lo/s;

    if-nez v2, :cond_3f

    .line 334
    iget-object v2, p1, Lp/Q;->b:Lo/s;

    if-eqz v2, :cond_4b

    move v0, v1

    .line 335
    goto :goto_4

    .line 337
    :cond_3f
    iget-object v2, p0, Lp/Q;->b:Lo/s;

    iget-object v3, p1, Lp/Q;->b:Lo/s;

    invoke-virtual {v2, v3}, Lo/s;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4b

    move v0, v1

    .line 338
    goto :goto_4

    .line 340
    :cond_4b
    iget-object v2, p0, Lp/Q;->c:Ljava/lang/String;

    if-nez v2, :cond_55

    .line 341
    iget-object v2, p1, Lp/Q;->c:Ljava/lang/String;

    if-eqz v2, :cond_61

    move v0, v1

    .line 342
    goto :goto_4

    .line 344
    :cond_55
    iget-object v2, p0, Lp/Q;->c:Ljava/lang/String;

    iget-object v3, p1, Lp/Q;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_61

    move v0, v1

    .line 345
    goto :goto_4

    .line 347
    :cond_61
    iget-object v2, p0, Lp/Q;->d:Ljava/lang/String;

    if-nez v2, :cond_6b

    .line 348
    iget-object v2, p1, Lp/Q;->d:Ljava/lang/String;

    if-eqz v2, :cond_77

    move v0, v1

    .line 349
    goto :goto_4

    .line 351
    :cond_6b
    iget-object v2, p0, Lp/Q;->d:Ljava/lang/String;

    iget-object v3, p1, Lp/Q;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_77

    move v0, v1

    .line 352
    goto :goto_4

    .line 354
    :cond_77
    iget-object v2, p0, Lp/Q;->g:Ljava/lang/String;

    if-nez v2, :cond_81

    .line 355
    iget-object v2, p1, Lp/Q;->g:Ljava/lang/String;

    if-eqz v2, :cond_8e

    move v0, v1

    .line 356
    goto :goto_4

    .line 358
    :cond_81
    iget-object v2, p0, Lp/Q;->g:Ljava/lang/String;

    iget-object v3, p1, Lp/Q;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8e

    move v0, v1

    .line 359
    goto/16 :goto_4

    .line 361
    :cond_8e
    iget-object v2, p0, Lp/Q;->e:Ljava/lang/String;

    if-nez v2, :cond_99

    .line 362
    iget-object v2, p1, Lp/Q;->e:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 363
    goto/16 :goto_4

    .line 365
    :cond_99
    iget-object v2, p0, Lp/Q;->e:Ljava/lang/String;

    iget-object v3, p1, Lp/Q;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 366
    goto/16 :goto_4
.end method

.method public f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 220
    iget-object v0, p0, Lp/Q;->d:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 224
    iget-object v0, p0, Lp/Q;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()LaT/h;
    .registers 2

    .prologue
    .line 236
    iget-object v0, p0, Lp/Q;->h:LaT/h;

    return-object v0
.end method

.method public hashCode()I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 297
    .line 299
    iget-object v0, p0, Lp/Q;->a:Lp/R;

    if-nez v0, :cond_35

    move v0, v1

    :goto_6
    add-int/lit8 v0, v0, 0x1f

    .line 300
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lp/Q;->f:I

    add-int/2addr v0, v2

    .line 301
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lp/Q;->b:Lo/s;

    if-nez v0, :cond_3c

    move v0, v1

    :goto_14
    add-int/2addr v0, v2

    .line 302
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lp/Q;->c:Ljava/lang/String;

    if-nez v0, :cond_43

    move v0, v1

    :goto_1c
    add-int/2addr v0, v2

    .line 303
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lp/Q;->d:Ljava/lang/String;

    if-nez v0, :cond_4a

    move v0, v1

    :goto_24
    add-int/2addr v0, v2

    .line 305
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lp/Q;->g:Ljava/lang/String;

    if-nez v0, :cond_51

    move v0, v1

    :goto_2c
    add-int/2addr v0, v2

    .line 306
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lp/Q;->e:Ljava/lang/String;

    if-nez v2, :cond_58

    :goto_33
    add-int/2addr v0, v1

    .line 308
    return v0

    .line 299
    :cond_35
    iget-object v0, p0, Lp/Q;->a:Lp/R;

    invoke-virtual {v0}, Lp/R;->hashCode()I

    move-result v0

    goto :goto_6

    .line 301
    :cond_3c
    iget-object v0, p0, Lp/Q;->b:Lo/s;

    invoke-virtual {v0}, Lo/s;->hashCode()I

    move-result v0

    goto :goto_14

    .line 302
    :cond_43
    iget-object v0, p0, Lp/Q;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1c

    .line 303
    :cond_4a
    iget-object v0, p0, Lp/Q;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_24

    .line 305
    :cond_51
    iget-object v0, p0, Lp/Q;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2c

    .line 306
    :cond_58
    iget-object v1, p0, Lp/Q;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_33
.end method

.method public i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 244
    invoke-virtual {p0}, Lp/Q;->j()LaT/c;

    move-result-object v0

    .line 245
    if-eqz v0, :cond_b

    .line 246
    invoke-virtual {v0}, LaT/c;->a()Ljava/lang/String;

    move-result-object v0

    .line 248
    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public j()LaT/c;
    .registers 5

    .prologue
    .line 257
    iget-object v0, p0, Lp/Q;->h:LaT/h;

    if-eqz v0, :cond_27

    .line 258
    iget-object v0, p0, Lp/Q;->h:LaT/h;

    invoke-virtual {v0}, LaT/h;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaT/c;

    .line 259
    invoke-virtual {v0}, LaT/c;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "info"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 264
    :goto_26
    return-object v0

    :cond_27
    const/4 v0, 0x0

    goto :goto_26
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 268
    iget-boolean v0, p0, Lp/Q;->i:Z

    return v0
.end method

.method public l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4

    .prologue
    .line 279
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/aY;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 280
    iget-object v1, p0, Lp/Q;->a:Lp/R;

    if-eqz v1, :cond_15

    .line 281
    const/4 v1, 0x1

    iget-object v2, p0, Lp/Q;->a:Lp/R;

    invoke-virtual {v2}, Lp/R;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 283
    :cond_15
    iget-object v1, p0, Lp/Q;->b:Lo/s;

    if-eqz v1, :cond_29

    .line 284
    const/4 v1, 0x2

    iget-object v2, p0, Lp/Q;->b:Lo/s;

    invoke-static {v2}, Lu/e;->a(Lo/s;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 286
    const/4 v1, 0x5

    iget v2, p0, Lp/Q;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 288
    :cond_29
    iget-object v1, p0, Lp/Q;->d:Ljava/lang/String;

    if-eqz v1, :cond_33

    .line 289
    const/4 v1, 0x3

    iget-object v2, p0, Lp/Q;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 292
    :cond_33
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[addr:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lp/Q;->a:Lp/R;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " point:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lp/Q;->b:Lo/s;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lp/Q;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " title:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lp/Q;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " token:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lp/Q;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lp/Q;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
