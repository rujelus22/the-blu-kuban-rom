.class public Lp/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/m;


# instance fields
.field private final a:Lo/aq;

.field private final b:I

.field private final c:J

.field private final d:[Lp/e;


# direct methods
.method private constructor <init>(Lo/aq;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lp/d;->a:Lo/aq;

    .line 96
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lp/d;->b:I

    .line 97
    iput-wide p3, p0, Lp/d;->c:J

    .line 98
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    .line 99
    new-array v0, v0, [Lp/e;

    iput-object v0, p0, Lp/d;->d:[Lp/e;

    .line 105
    invoke-direct {p0, p2}, Lp/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lo/T;

    move-result-object v0

    .line 106
    invoke-direct {p0, p2, v0}, Lp/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lo/T;)V

    .line 107
    invoke-direct {p0, p2}, Lp/d;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 108
    return-void
.end method

.method public static a([BI)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 231
    new-instance v0, Laq/a;

    invoke-direct {v0, p0}, Laq/a;-><init>([B)V

    .line 232
    invoke-virtual {v0, p1}, Laq/a;->skipBytes(I)I

    .line 233
    invoke-virtual {v0}, Laq/a;->readInt()I

    move-result v1

    .line 234
    const v2, 0x45504752

    if-eq v1, v2, :cond_2a

    .line 235
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FORMAT_MAGIC expected. Found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_2a
    invoke-virtual {v0}, Laq/a;->readUnsignedShort()I

    move-result v1

    .line 238
    const/4 v2, 0x1

    if-eq v1, v2, :cond_50

    .line 239
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Version mismatch: 1 expected, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :cond_50
    invoke-virtual {v0}, Laq/a;->readInt()I

    move-result v0

    return v0
.end method

.method private static a([BLo/T;Lo/T;)Lo/X;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 440
    .line 441
    const/4 v0, 0x0

    .line 442
    if-eqz p0, :cond_40

    .line 443
    new-instance v0, Ljava/io/DataInputStream;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 444
    invoke-static {v0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v1

    .line 446
    :goto_12
    new-instance v5, Lo/Z;

    add-int/lit8 v3, v1, 0x2

    invoke-direct {v5, v3}, Lo/Z;-><init>(I)V

    .line 447
    if-eqz p1, :cond_1e

    .line 448
    invoke-virtual {v5, p1}, Lo/Z;->a(Lo/T;)Z

    :cond_1e
    move v3, v2

    move v4, v2

    .line 451
    :goto_20
    if-ge v2, v1, :cond_36

    .line 452
    invoke-static {v0}, Lo/aG;->b(Ljava/io/DataInput;)I

    move-result v6

    add-int/2addr v4, v6

    .line 453
    invoke-static {v0}, Lo/aG;->b(Ljava/io/DataInput;)I

    move-result v6

    add-int/2addr v3, v6

    .line 454
    invoke-static {v4, v3}, Lo/T;->c(II)Lo/T;

    move-result-object v6

    invoke-virtual {v5, v6}, Lo/Z;->a(Lo/T;)Z

    .line 451
    add-int/lit8 v2, v2, 0x1

    goto :goto_20

    .line 456
    :cond_36
    if-eqz p2, :cond_3b

    .line 457
    invoke-virtual {v5, p2}, Lo/Z;->a(Lo/T;)Z

    .line 459
    :cond_3b
    invoke-virtual {v5}, Lo/Z;->d()Lo/X;

    move-result-object v0

    return-object v0

    :cond_40
    move v1, v2

    goto :goto_12
.end method

.method public static a(Lo/aq;[BIJ)Lp/d;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 185
    invoke-static {p1, p2}, Lp/d;->a([BI)I

    move-result v0

    .line 186
    add-int/lit8 v1, p2, 0xa

    .line 187
    invoke-static {p0, v0, p1, v1}, Lp/d;->a(Lo/aq;I[BI)V

    .line 188
    array-length v0, p1

    sub-int/2addr v0, v1

    .line 190
    new-instance v2, Ljava/util/zip/Inflater;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/zip/Inflater;-><init>(Z)V

    .line 192
    :try_start_11
    invoke-static {p1, v1, v0}, LR/f;->a([BII)LR/g;

    move-result-object v0

    .line 193
    invoke-virtual {v0}, LR/g;->a()[B

    move-result-object v1

    .line 194
    invoke-virtual {v0}, LR/g;->b()I

    move-result v0

    .line 195
    invoke-static {p0, v1, v0, p3, p4}, Lp/d;->b(Lo/aq;[BIJ)Lp/d;
    :try_end_20
    .catchall {:try_start_11 .. :try_end_20} :catchall_30
    .catch Ljava/util/zip/DataFormatException; {:try_start_11 .. :try_end_20} :catch_25

    move-result-object v0

    .line 199
    invoke-virtual {v2}, Ljava/util/zip/Inflater;->end()V

    return-object v0

    .line 196
    :catch_25
    move-exception v0

    .line 197
    :try_start_26
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/util/zip/DataFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_30
    .catchall {:try_start_26 .. :try_end_30} :catchall_30

    .line 199
    :catchall_30
    move-exception v0

    invoke-virtual {v2}, Ljava/util/zip/Inflater;->end()V

    throw v0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lo/T;)V
    .registers 20
    .parameter
    .parameter

    .prologue
    .line 283
    const/4 v1, 0x0

    move v10, v1

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lp/d;->d:[Lp/e;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    if-ge v10, v1, :cond_10f

    .line 284
    mul-int/lit8 v12, v10, 0x2

    .line 285
    mul-int/lit8 v1, v10, 0x2

    add-int/lit8 v13, v1, 0x1

    .line 286
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    .line 287
    const/4 v1, 0x2

    invoke-static {v6, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v7

    .line 289
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v11

    .line 290
    const/4 v1, 0x2

    invoke-static {v11, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v14

    .line 292
    const/4 v1, 0x0

    .line 293
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 295
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 296
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v1

    .line 298
    :cond_3d
    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v9

    .line 300
    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v8

    .line 303
    move-object/from16 v0, p1

    invoke-static {v6, v8, v0}, Lp/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lp/f;

    move-result-object v4

    .line 305
    const/4 v3, 0x0

    .line 306
    const/4 v2, 0x4

    .line 308
    aget-object v15, p2, v13

    .line 309
    aget-object v16, p2, v12

    .line 310
    move-object/from16 v0, v16

    invoke-static {v1, v15, v0}, Lp/d;->a([BLo/T;Lo/T;)Lo/X;

    move-result-object v5

    .line 312
    if-nez v15, :cond_8c

    if-nez v16, :cond_8c

    .line 313
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Both polyline endpoints are missing for segment: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lp/d;->d:[Lp/e;

    aget-object v3, v3, v12

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in tile: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lp/d;->a:Lo/aq;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 316
    :cond_8c
    if-nez v15, :cond_105

    .line 317
    const/4 v2, 0x2

    .line 318
    const/4 v1, 0x5

    .line 324
    :goto_90
    const/4 v3, 0x4

    const/4 v15, 0x0

    invoke-static {v6, v3, v15}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v3

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_112

    .line 326
    or-int/lit8 v6, v2, 0x8

    .line 328
    :goto_9c
    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {v11, v2, v3}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_110

    .line 330
    or-int/lit8 v1, v1, 0x8

    move v11, v1

    .line 333
    :goto_a9
    move-object/from16 v0, p0

    iget-object v15, v0, Lp/d;->d:[Lp/e;

    new-instance v1, Lp/e;

    move-object/from16 v0, p0

    iget-object v2, v0, Lp/d;->a:Lo/aq;

    invoke-static {v2, v12}, Lp/e;->a(Lo/aq;I)J

    move-result-wide v2

    invoke-direct/range {v1 .. v9}, Lp/e;-><init>(J[Lp/f;Lo/X;IIII)V

    aput-object v1, v15, v12

    .line 335
    move-object/from16 v0, p0

    iget-object v15, v0, Lp/d;->d:[Lp/e;

    new-instance v1, Lp/e;

    move-object/from16 v0, p0

    iget-object v2, v0, Lp/d;->a:Lo/aq;

    invoke-static {v2, v13}, Lp/e;->a(Lo/aq;I)J

    move-result-wide v2

    move v6, v11

    move v7, v14

    invoke-direct/range {v1 .. v9}, Lp/e;-><init>(J[Lp/f;Lo/X;IIII)V

    aput-object v1, v15, v13

    .line 338
    invoke-virtual {v5}, Lo/X;->b()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_10a

    .line 339
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Segment polyline had fewer than two points for segment: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lp/d;->d:[Lp/e;

    aget-object v3, v3, v12

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in tile: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lp/d;->a:Lo/aq;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 319
    :cond_105
    if-nez v16, :cond_114

    .line 320
    const/4 v2, 0x1

    .line 321
    const/4 v1, 0x6

    goto :goto_90

    .line 283
    :cond_10a
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto/16 :goto_2

    .line 343
    :cond_10f
    return-void

    :cond_110
    move v11, v1

    goto :goto_a9

    :cond_112
    move v6, v2

    goto :goto_9c

    :cond_114
    move v1, v2

    move v2, v3

    goto/16 :goto_90
.end method

.method private static a(Lo/aq;I[BI)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 212
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 213
    invoke-virtual {p0}, Lo/aq;->c()I

    move-result v1

    invoke-virtual {p0}, Lo/aq;->d()I

    move-result v2

    invoke-virtual {p0}, Lo/aq;->b()I

    move-result v3

    invoke-static {v1, v2, v3, p1, v0}, Lr/y;->a(IIII[B)V

    .line 215
    new-instance v1, Lr/y;

    invoke-direct {v1}, Lr/y;-><init>()V

    .line 216
    const/16 v2, 0x100

    invoke-virtual {v1, v0, v2}, Lr/y;->b([BI)V

    .line 217
    array-length v0, p2

    sub-int/2addr v0, p3

    invoke-virtual {v1, p2, p3, v0}, Lr/y;->a([BII)V

    .line 218
    return-void
.end method

.method private static a([B)[I
    .registers 6
    .parameter

    .prologue
    .line 415
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 416
    invoke-static {v1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v2

    .line 417
    new-array v3, v2, [I

    .line 418
    const/4 v0, 0x0

    :goto_11
    if-ge v0, v2, :cond_1c

    .line 419
    invoke-static {v1}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    aput v4, v3, v0

    .line 418
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 421
    :cond_1c
    return-object v3
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lo/T;
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x4

    const/4 v1, 0x0

    .line 253
    iget-object v0, p0, Lp/d;->d:[Lp/e;

    array-length v0, v0

    new-array v3, v0, [Lo/T;

    .line 255
    invoke-virtual {p1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v4

    move v2, v1

    .line 256
    :goto_c
    if-ge v2, v4, :cond_38

    .line 257
    invoke-virtual {p1, v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 259
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    .line 260
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    .line 261
    invoke-static {v5, v6}, Lo/T;->a(II)Lo/T;

    move-result-object v5

    .line 262
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v0

    invoke-static {v0}, Lp/d;->a([B)[I

    move-result-object v6

    move v0, v1

    .line 264
    :goto_2a
    array-length v7, v6

    if-ge v0, v7, :cond_34

    .line 265
    aget v7, v6, v0

    aput-object v5, v3, v7

    .line 264
    add-int/lit8 v0, v0, 0x1

    goto :goto_2a

    .line 256
    :cond_34
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_c

    .line 268
    :cond_38
    return-object v3
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lp/f;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 472
    invoke-virtual {p0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    .line 474
    if-ne p1, v4, :cond_3c

    move v3, v4

    .line 475
    :goto_b
    if-lez v5, :cond_40

    .line 476
    new-array v1, v5, [Lp/f;

    .line 477
    :goto_f
    if-ge v2, v5, :cond_3e

    .line 478
    invoke-virtual {p0, v9, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v0

    .line 479
    const/4 v6, 0x5

    invoke-virtual {p2, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    .line 480
    const/4 v0, 0x0

    .line 481
    invoke-virtual {v6, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v7

    if-eqz v7, :cond_29

    .line 482
    invoke-virtual {v6, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    .line 484
    :cond_29
    new-instance v7, Lp/f;

    invoke-virtual {v6, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6, v0, v3}, Lp/f;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v7, v1, v2

    .line 477
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_f

    :cond_3c
    move v3, v2

    .line 474
    goto :goto_b

    :cond_3e
    move-object v0, v1

    .line 491
    :goto_3f
    return-object v0

    .line 490
    :cond_40
    new-array v0, v4, [Lp/f;

    sget-object v1, Lp/e;->a:Lp/f;

    aput-object v1, v0, v2

    goto :goto_3f
.end method

.method private static b(Lo/aq;[BIJ)Lp/d;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 166
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbB/a;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 167
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;I)I

    .line 168
    new-instance v1, Lp/d;

    invoke-direct {v1, p0, v0, p3, p4}, Lp/d;-><init>(Lo/aq;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V

    return-object v1
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 20
    .parameter

    .prologue
    .line 353
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v7

    .line 354
    new-instance v8, Lo/T;

    invoke-direct {v8}, Lo/T;-><init>()V

    .line 355
    new-instance v9, Lo/T;

    invoke-direct {v9}, Lo/T;-><init>()V

    .line 356
    const/4 v1, 0x0

    move v6, v1

    :goto_13
    if-ge v6, v7, :cond_dc

    .line 357
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 358
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v2

    invoke-static {v2}, Lp/d;->a([B)[I

    move-result-object v10

    .line 360
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v1

    invoke-static {v1}, Lp/d;->a([B)[I

    move-result-object v11

    .line 361
    const/4 v2, 0x0

    .line 362
    array-length v1, v10

    new-array v12, v1, [Lp/a;

    .line 363
    const/4 v1, 0x0

    :goto_33
    array-length v3, v10

    if-ge v1, v3, :cond_d7

    .line 364
    move-object/from16 v0, p0

    iget-object v3, v0, Lp/d;->d:[Lp/e;

    aget v4, v10, v1

    aget-object v13, v3, v4

    .line 365
    const/4 v4, 0x0

    .line 366
    const/4 v3, 0x0

    move/from16 v17, v3

    move v3, v4

    move v4, v1

    move/from16 v1, v17

    :goto_46
    array-length v5, v10

    if-ge v1, v5, :cond_c8

    .line 367
    array-length v5, v11

    if-lt v2, v5, :cond_53

    .line 370
    array-length v1, v10

    move v4, v1

    .line 366
    :cond_4e
    :goto_4e
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_46

    .line 373
    :cond_53
    aget v14, v11, v2

    .line 374
    if-eqz v14, :cond_4e

    .line 384
    move-object/from16 v0, p0

    iget-object v5, v0, Lp/d;->d:[Lp/e;

    aget v15, v10, v1

    xor-int/lit8 v15, v15, 0x1

    aget-object v15, v5, v15

    .line 385
    add-int/lit8 v5, v3, 0x1

    new-instance v16, Lp/a;

    move-object/from16 v0, v16

    invoke-direct {v0, v15, v14}, Lp/a;-><init>(Lp/e;I)V

    aput-object v16, v12, v3

    .line 386
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v3

    if-eqz v3, :cond_dd

    .line 388
    invoke-virtual {v13, v8}, Lp/e;->a(Lo/T;)V

    .line 389
    const/4 v3, 0x0

    invoke-virtual {v15, v3, v9}, Lp/e;->a(ILo/T;)V

    .line 390
    invoke-virtual {v8, v9}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_dd

    .line 391
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Polylines did not line up when creating arc:  intersection: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " fromIndex: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " toIndex: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " point1: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Lo/T;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " point2: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Lo/T;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 400
    :cond_c8
    new-array v1, v3, [Lp/a;

    .line 401
    const/4 v3, 0x0

    const/4 v5, 0x0

    array-length v14, v1

    invoke-static {v12, v3, v1, v5, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 402
    invoke-virtual {v13, v1}, Lp/e;->a([Lp/a;)V

    .line 363
    add-int/lit8 v1, v4, 0x1

    goto/16 :goto_33

    .line 356
    :cond_d7
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto/16 :goto_13

    .line 405
    :cond_dc
    return-void

    :cond_dd
    move v3, v5

    goto/16 :goto_4e
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 558
    iget-wide v0, p0, Lp/d;->c:J

    return-wide v0
.end method

.method public a(I)Lp/e;
    .registers 3
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lp/d;->d:[Lp/e;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a(Lp/e;)Lp/e;
    .registers 10
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 129
    move v0, v1

    :goto_2
    iget-object v2, p0, Lp/d;->d:[Lp/e;

    array-length v2, v2

    if-ge v0, v2, :cond_1c

    .line 130
    iget-object v2, p0, Lp/d;->d:[Lp/e;

    aget-object v2, v2, v0

    .line 131
    invoke-virtual {v2}, Lp/e;->b()Z

    move-result v3

    if-eqz v3, :cond_19

    invoke-virtual {v2, p1}, Lp/e;->a(Lp/e;)Z

    move-result v3

    if-eqz v3, :cond_19

    move-object v0, v2

    .line 151
    :goto_18
    return-object v0

    .line 129
    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 135
    :cond_1c
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_b6

    .line 136
    const-string v0, "RoadGraphPiece"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No entering segment found in tile: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lp/d;->a:Lo/aq;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with bound: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lp/d;->a:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->i()Lo/ad;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for leaving segment: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with endpoint: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lp/e;->e()Lo/T;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-virtual {p1, v1}, Lp/e;->b(I)Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->e()D

    move-result-wide v2

    const-wide/high16 v4, 0x4024

    mul-double/2addr v2, v4

    move v0, v1

    .line 142
    :goto_70
    iget-object v4, p0, Lp/d;->d:[Lp/e;

    array-length v4, v4

    if-ge v0, v4, :cond_b6

    .line 143
    iget-object v4, p0, Lp/d;->d:[Lp/e;

    aget-object v4, v4, v0

    .line 144
    invoke-virtual {v4, v1}, Lp/e;->b(I)Lo/T;

    move-result-object v5

    invoke-virtual {p1, v1}, Lp/e;->b(I)Lo/T;

    move-result-object v6

    invoke-virtual {v5, v6}, Lo/T;->c(Lo/T;)F

    move-result v5

    float-to-double v5, v5

    cmpg-double v5, v5, v2

    if-gez v5, :cond_b3

    invoke-virtual {v4}, Lp/e;->e()Lo/T;

    move-result-object v5

    invoke-virtual {p1}, Lp/e;->e()Lo/T;

    move-result-object v6

    invoke-virtual {v5, v6}, Lo/T;->c(Lo/T;)F

    move-result v5

    float-to-double v5, v5

    cmpg-double v5, v5, v2

    if-gez v5, :cond_b3

    .line 147
    const-string v5, "RoadGraphPiece"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Nearby segment: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_b3
    add-int/lit8 v0, v0, 0x1

    goto :goto_70

    .line 151
    :cond_b6
    const/4 v0, 0x0

    goto/16 :goto_18
.end method

.method public a(Lcom/google/googlenav/common/a;)Z
    .registers 6
    .parameter

    .prologue
    .line 497
    iget-wide v0, p0, Lp/d;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_14

    invoke-interface {p1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lp/d;->c:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public b()J
    .registers 3

    .prologue
    .line 552
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public b(Lcom/google/googlenav/common/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 543
    const/4 v0, 0x0

    return v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 112
    iget-object v0, p0, Lp/d;->d:[Lp/e;

    array-length v0, v0

    return v0
.end method

.method public c(Lcom/google/googlenav/common/a;)V
    .registers 2
    .parameter

    .prologue
    .line 548
    return-void
.end method

.method public d()Lo/aq;
    .registers 2

    .prologue
    .line 502
    iget-object v0, p0, Lp/d;->a:Lo/aq;

    return-object v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 507
    iget v0, p0, Lp/d;->b:I

    return v0
.end method

.method public g()LA/c;
    .registers 2

    .prologue
    .line 533
    sget-object v0, LA/c;->i:LA/c;

    return-object v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 538
    const/4 v0, -0x1

    return v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 563
    const/4 v0, 0x0

    return v0
.end method
