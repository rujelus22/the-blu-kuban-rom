.class public Lp/L;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private b:Lp/J;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput p1, p0, Lp/L;->a:I

    .line 80
    iput-object p2, p0, Lp/L;->c:Ljava/lang/String;

    .line 81
    iput-object p3, p0, Lp/L;->d:Ljava/lang/String;

    .line 82
    iput-object p4, p0, Lp/L;->e:Ljava/lang/String;

    .line 83
    iput-object p5, p0, Lp/L;->f:Ljava/lang/String;

    .line 84
    iput-boolean p6, p0, Lp/L;->g:Z

    .line 85
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lp/L;
    .registers 2
    .parameter

    .prologue
    .line 33
    invoke-static {p0}, Lp/L;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lp/L;

    move-result-object v0

    return-object v0
.end method

.method private a(Lp/J;)V
    .registers 2
    .parameter

    .prologue
    .line 107
    iput-object p1, p0, Lp/L;->b:Lp/J;

    .line 108
    return-void
.end method

.method static synthetic a(Lp/L;Lp/J;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lp/L;->a(Lp/J;)V

    return-void
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lp/L;
    .registers 9
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x1

    .line 88
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 89
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 90
    const/4 v0, 0x5

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v3

    .line 93
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 94
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 95
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 97
    :goto_20
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-ne v0, v7, :cond_2c

    .line 99
    :goto_26
    new-instance v0, Lp/L;

    invoke-direct/range {v0 .. v6}, Lp/L;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0

    .line 97
    :cond_2c
    const/4 v6, 0x0

    goto :goto_26

    :cond_2e
    move-object v4, v5

    goto :goto_20
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 111
    iget v0, p0, Lp/L;->a:I

    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 115
    iget-object v0, p0, Lp/L;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 119
    iget-object v0, p0, Lp/L;->d:Ljava/lang/String;

    if-nez v0, :cond_7

    iget-object v0, p0, Lp/L;->c:Ljava/lang/String;

    :goto_6
    return-object v0

    :cond_7
    iget-object v0, p0, Lp/L;->d:Ljava/lang/String;

    goto :goto_6
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 123
    iget-object v0, p0, Lp/L;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 127
    iget-object v0, p0, Lp/L;->f:Ljava/lang/String;

    return-object v0
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 131
    iget-boolean v0, p0, Lp/L;->g:Z

    return v0
.end method

.method public g()Lp/J;
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lp/L;->b:Lp/J;

    return-object v0
.end method
