.class public Lp/g;
.super Lad/a;
.source "SourceFile"


# static fields
.field private static k:Ljava/lang/String;


# instance fields
.field private final a:I

.field private final b:[Lp/Q;

.field private final c:[Lp/b;

.field private final d:Z

.field private final e:I

.field private final f:I

.field private final g:F

.field private final h:F

.field private final i:Lp/Q;

.field private final j:I

.field private l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private m:I

.field private n:[Lp/y;

.field private o:[Lp/Q;

.field private p:[Lp/b;


# direct methods
.method private constructor <init>([Lp/Q;IFFZLp/Q;III[Lp/b;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 140
    invoke-direct {p0}, Lad/a;-><init>()V

    .line 96
    const/4 v0, -0x1

    iput v0, p0, Lp/g;->m:I

    .line 141
    iput-object p1, p0, Lp/g;->b:[Lp/Q;

    .line 142
    iput p2, p0, Lp/g;->a:I

    .line 143
    iput p9, p0, Lp/g;->j:I

    .line 144
    iput p3, p0, Lp/g;->g:F

    .line 145
    iput p4, p0, Lp/g;->h:F

    .line 146
    iput-boolean p5, p0, Lp/g;->d:Z

    .line 147
    iput-object p6, p0, Lp/g;->i:Lp/Q;

    .line 148
    iput p8, p0, Lp/g;->f:I

    .line 149
    iput-object p10, p0, Lp/g;->c:[Lp/b;

    .line 150
    iput p7, p0, Lp/g;->e:I

    .line 152
    sget-object v0, Lp/g;->k:Ljava/lang/String;

    if-nez v0, :cond_21

    .line 153
    invoke-direct {p0}, Lp/g;->t()V

    .line 155
    :cond_21
    return-void
.end method

.method synthetic constructor <init>([Lp/Q;IFFZLp/Q;III[Lp/b;Lp/h;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-direct/range {p0 .. p10}, Lp/g;-><init>([Lp/Q;IFFZLp/Q;III[Lp/b;)V

    return-void
.end method

.method static synthetic a(Lp/g;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 39
    iput p1, p0, Lp/g;->m:I

    return p1
.end method

.method static synthetic a([Lp/Q;)Z
    .registers 2
    .parameter

    .prologue
    .line 39
    invoke-static {p0}, Lp/g;->b([Lp/Q;)Z

    move-result v0

    return v0
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)[Lp/Q;
    .registers 12
    .parameter
    .parameter

    .prologue
    const/16 v9, 0xf

    const/4 v8, 0x5

    const/4 v1, 0x0

    .line 564
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 565
    new-array v3, v2, [Lp/Q;

    move v0, v1

    .line 566
    :goto_b
    if-ge v0, v2, :cond_3c

    .line 567
    invoke-virtual {p0, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 569
    const/4 v5, 0x7

    invoke-virtual {v4, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    .line 571
    add-int/lit8 v6, v2, -0x1

    if-ne v0, v6, :cond_33

    .line 572
    new-instance v6, Lp/Q;

    invoke-direct {v6, v5, p1}, Lp/Q;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v6, v3, v0

    .line 577
    :goto_21
    invoke-virtual {v4, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_30

    .line 578
    aget-object v5, v3, v0

    invoke-virtual {v4, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lp/Q;->a(Ljava/lang/String;)V

    .line 566
    :cond_30
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 574
    :cond_33
    new-instance v6, Lp/Q;

    const/4 v7, 0x0

    invoke-direct {v6, v5, v7}, Lp/Q;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v6, v3, v0

    goto :goto_21

    .line 582
    :cond_3c
    return-object v3
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lp/Q;)[Lp/y;
    .registers 16
    .parameter
    .parameter

    .prologue
    const/16 v12, 0x8

    const/4 v2, 0x2

    const/4 v0, 0x0

    .line 509
    invoke-virtual {p1, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v10

    .line 510
    new-array v11, v10, [Lp/y;

    .line 511
    const/16 v1, 0xd

    invoke-static {p1, v1, v0}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v7

    .line 516
    if-le v7, v2, :cond_13

    move v7, v0

    .line 521
    :cond_13
    if-ne v7, v2, :cond_1e

    const/16 v1, 0x17

    invoke-static {p1, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 524
    const/4 v7, 0x3

    :cond_1e
    move v9, v0

    .line 527
    :goto_1f
    if-ge v9, v10, :cond_3c

    .line 528
    invoke-virtual {p1, v12, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 529
    iget v1, p0, Lp/g;->j:I

    iget v3, p0, Lp/g;->g:F

    iget v4, p0, Lp/g;->h:F

    iget-boolean v5, p0, Lp/g;->d:Z

    iget-object v6, p0, Lp/g;->i:Lp/Q;

    iget-object v8, p0, Lp/g;->p:[Lp/b;

    move-object v2, p2

    invoke-static/range {v0 .. v8}, Lp/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I[Lp/Q;FFZLp/Q;I[Lp/b;)Lp/y;

    move-result-object v0

    aput-object v0, v11, v9

    .line 527
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1f

    .line 533
    :cond_3c
    return-object v11
.end method

.method private b(I)I
    .registers 4
    .parameter

    .prologue
    .line 732
    iget-object v0, p0, Lp/g;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method private static b([Lp/Q;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 270
    array-length v2, p0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_40

    .line 274
    aget-object v2, p0, v0

    invoke-virtual {v2}, Lp/Q;->c()Lo/s;

    move-result-object v2

    .line 275
    aget-object v3, p0, v1

    invoke-virtual {v3}, Lp/Q;->c()Lo/s;

    move-result-object v3

    .line 276
    if-eqz v2, :cond_40

    if-eqz v3, :cond_40

    .line 277
    invoke-virtual {v2}, Lo/s;->a()I

    move-result v4

    invoke-virtual {v2}, Lo/s;->b()I

    move-result v2

    invoke-static {v4, v2}, Lo/Q;->b(II)Lo/Q;

    move-result-object v2

    .line 278
    invoke-virtual {v3}, Lo/s;->a()I

    move-result v4

    invoke-virtual {v3}, Lo/s;->b()I

    move-result v3

    invoke-static {v4, v3}, Lo/Q;->b(II)Lo/Q;

    move-result-object v3

    .line 279
    invoke-virtual {v2, v3}, Lo/Q;->c(Lo/Q;)F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {v2}, Lo/Q;->e()D

    move-result-wide v5

    div-double v2, v3, v5

    .line 280
    const-wide/high16 v4, 0x4000

    cmpg-double v2, v2, v4

    if-gez v2, :cond_40

    .line 285
    :goto_3f
    return v0

    :cond_40
    move v0, v1

    goto :goto_3f
.end method

.method static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lp/b;
    .registers 8
    .parameter

    .prologue
    const/16 v6, 0xe

    const/4 v2, 0x0

    .line 590
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    .line 591
    new-array v0, v3, [Lp/b;

    move v1, v2

    .line 592
    :goto_a
    if-ge v1, v3, :cond_39

    .line 593
    invoke-virtual {p0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 594
    invoke-static {v4}, Lp/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lp/b;

    move-result-object v5

    aput-object v5, v0, v1

    .line 595
    aget-object v5, v0, v1

    if-nez v5, :cond_3a

    .line 598
    const-string v0, "DirectionsRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Option with no value: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Li/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    new-array v0, v2, [Lp/b;

    .line 603
    :cond_39
    return-object v0

    .line 592
    :cond_3a
    add-int/lit8 v1, v1, 0x1

    goto :goto_a
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lp/Q;
    .registers 9
    .parameter

    .prologue
    const/4 v7, 0x7

    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 542
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    .line 543
    if-nez v0, :cond_c

    .line 544
    new-array v0, v1, [Lp/Q;

    .line 556
    :cond_b
    return-object v0

    .line 546
    :cond_c
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 548
    invoke-virtual {v2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    .line 550
    new-array v0, v3, [Lp/Q;

    .line 551
    :goto_18
    if-ge v1, v3, :cond_b

    .line 552
    invoke-virtual {v2, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 554
    new-instance v5, Lp/Q;

    const/4 v6, 0x0

    invoke-direct {v5, v4, v6}, Lp/Q;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v5, v0, v1

    .line 551
    add-int/lit8 v1, v1, 0x1

    goto :goto_18
.end method

.method private s()V
    .registers 4

    .prologue
    const/4 v2, 0x2

    .line 478
    iget-object v0, p0, Lp/g;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lp/g;->m:I

    .line 480
    iget-object v0, p0, Lp/g;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lp/g;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lp/b;

    move-result-object v0

    iput-object v0, p0, Lp/g;->p:[Lp/b;

    .line 483
    invoke-direct {p0}, Lp/g;->u()V

    .line 485
    invoke-virtual {p0}, Lp/g;->f_()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 486
    iget-object v0, p0, Lp/g;->b:[Lp/Q;

    iget-object v1, p0, Lp/g;->b:[Lp/Q;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lp/Q;->e()Ljava/lang/String;

    move-result-object v0

    .line 487
    iget-object v1, p0, Lp/g;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v1, v0}, Lp/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)[Lp/Q;

    move-result-object v0

    .line 488
    iget-object v1, p0, Lp/g;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v1, v0}, Lp/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lp/Q;)[Lp/y;

    move-result-object v0

    iput-object v0, p0, Lp/g;->n:[Lp/y;

    .line 489
    iget v0, p0, Lp/g;->j:I

    if-ne v0, v2, :cond_42

    iget-object v0, p0, Lp/g;->n:[Lp/y;

    array-length v0, v0

    if-ge v0, v2, :cond_42

    .line 492
    const/4 v0, 0x4

    iput v0, p0, Lp/g;->m:I

    .line 506
    :cond_42
    :goto_42
    return-void

    .line 494
    :cond_43
    invoke-virtual {p0}, Lp/g;->m()Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 495
    iget-object v0, p0, Lp/g;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lp/g;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lp/Q;

    move-result-object v0

    iput-object v0, p0, Lp/g;->o:[Lp/Q;

    .line 499
    iget-object v0, p0, Lp/g;->o:[Lp/Q;

    array-length v0, v0

    if-nez v0, :cond_42

    .line 500
    const/4 v0, 0x3

    iput v0, p0, Lp/g;->m:I

    .line 501
    const/4 v0, 0x0

    iput-object v0, p0, Lp/g;->o:[Lp/Q;

    goto :goto_42

    .line 504
    :cond_5d
    const-string v0, "DirectionsRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lp/g;->m:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Li/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_42
.end method

.method private t()V
    .registers 2

    .prologue
    .line 688
    invoke-static {}, Lcom/google/android/maps/driveabout/app/cf;->a()Lcom/google/android/maps/driveabout/app/cf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cf;->g()Lcom/google/android/maps/driveabout/app/NavigationService;

    move-result-object v0

    .line 689
    if-eqz v0, :cond_10

    .line 690
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->i()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lp/g;->k:Ljava/lang/String;

    .line 692
    :cond_10
    return-void
.end method

.method private u()V
    .registers 5

    .prologue
    const/4 v3, 0x2

    .line 704
    iget v0, p0, Lp/g;->m:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_7

    .line 728
    :cond_6
    :goto_6
    return-void

    .line 707
    :cond_7
    iget-object v0, p0, Lp/g;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 708
    if-lt v1, v3, :cond_6

    .line 714
    const/4 v0, 0x0

    :goto_11
    add-int/lit8 v2, v1, -0x1

    if-ge v0, v2, :cond_1e

    .line 715
    invoke-direct {p0, v0}, Lp/g;->b(I)I

    move-result v2

    if-nez v2, :cond_6

    .line 714
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 723
    :cond_1e
    add-int/lit8 v0, v1, -0x1

    invoke-direct {p0, v0}, Lp/g;->b(I)I

    move-result v0

    .line 724
    if-ne v0, v3, :cond_6

    .line 726
    iput v3, p0, Lp/g;->m:I

    goto :goto_6
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 426
    const/16 v0, 0x1c

    return v0
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 773
    iput p1, p0, Lp/g;->m:I

    .line 774
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 435
    iput-object p1, p0, Lp/g;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 436
    invoke-direct {p0}, Lp/g;->s()V

    .line 437
    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .registers 3
    .parameter

    .prologue
    .line 469
    invoke-virtual {p0}, Lp/g;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 470
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 5
    .parameter

    .prologue
    .line 445
    :try_start_0
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/aY;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lp/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_9} :catch_b
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_9} :catch_17
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_9} :catch_22

    .line 461
    const/4 v0, 0x1

    return v0

    .line 447
    :catch_b
    move-exception v0

    .line 448
    const-string v1, "DA:DirectionsRequest"

    invoke-static {v1, v0}, LaU/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 449
    const-string v1, "DirectionsRequest: Handling request failed"

    invoke-static {v1, v0}, Li/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 450
    throw v0

    .line 451
    :catch_17
    move-exception v0

    .line 454
    const-string v1, "DirectionsRequest"

    invoke-virtual {v0}, Ljava/io/InterruptedIOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Li/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    throw v0

    .line 456
    :catch_22
    move-exception v0

    .line 457
    const-string v1, "DA:DirectionsRequest"

    invoke-static {v1, v0}, LaU/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 458
    const-string v1, "DirectionsRequest: Parse failed"

    invoke-static {v1, v0}, Li/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 459
    throw v0
.end method

.method public b()Z
    .registers 3

    .prologue
    .line 262
    iget v0, p0, Lp/g;->m:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public c()[Lp/y;
    .registers 2

    .prologue
    .line 293
    iget-object v0, p0, Lp/g;->n:[Lp/y;

    return-object v0
.end method

.method public d()[Lp/b;
    .registers 2

    .prologue
    .line 308
    iget-object v0, p0, Lp/g;->p:[Lp/b;

    return-object v0
.end method

.method public e()[Lp/Q;
    .registers 2

    .prologue
    .line 330
    iget-object v0, p0, Lp/g;->o:[Lp/Q;

    return-object v0
.end method

.method public e_()Z
    .registers 3

    .prologue
    .line 358
    iget v0, p0, Lp/g;->j:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public f()Z
    .registers 3

    .prologue
    .line 351
    iget v0, p0, Lp/g;->j:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public f_()Z
    .registers 2

    .prologue
    .line 372
    iget v0, p0, Lp/g;->m:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public g_()Z
    .registers 3

    .prologue
    .line 379
    iget v0, p0, Lp/g;->m:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public h()I
    .registers 2

    .prologue
    .line 365
    iget v0, p0, Lp/g;->e:I

    return v0
.end method

.method public k()Z
    .registers 3

    .prologue
    .line 387
    iget v0, p0, Lp/g;->m:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public l()Z
    .registers 3

    .prologue
    .line 396
    iget v0, p0, Lp/g;->m:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public m()Z
    .registers 3

    .prologue
    .line 405
    iget v0, p0, Lp/g;->m:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public n()Z
    .registers 3

    .prologue
    .line 412
    iget v0, p0, Lp/g;->m:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public o()I
    .registers 2

    .prologue
    .line 416
    iget v0, p0, Lp/g;->j:I

    return v0
.end method

.method public p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 420
    iget-object v0, p0, Lp/g;->l:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public q()Z
    .registers 2

    .prologue
    .line 431
    const/4 v0, 0x1

    return v0
.end method

.method r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 10

    .prologue
    const v4, 0x186a0

    const/4 v8, 0x2

    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 611
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/aY;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 612
    iget v0, p0, Lp/g;->a:I

    invoke-virtual {v2, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    move v0, v1

    .line 613
    :goto_14
    iget-object v3, p0, Lp/g;->b:[Lp/Q;

    array-length v3, v3

    if-ge v0, v3, :cond_27

    .line 614
    iget-object v3, p0, Lp/g;->b:[Lp/Q;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lp/Q;->l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 613
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 617
    :cond_27
    const/4 v0, 0x5

    iget v3, p0, Lp/g;->f:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 619
    iget v0, p0, Lp/g;->f:I

    if-le v0, v6, :cond_4b

    .line 622
    const/16 v0, 0x16

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 624
    invoke-static {}, Lcom/google/android/maps/driveabout/app/cf;->a()Lcom/google/android/maps/driveabout/app/cf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cf;->g()Lcom/google/android/maps/driveabout/app/NavigationService;

    move-result-object v0

    .line 625
    if-eqz v0, :cond_4b

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->j()Z

    move-result v0

    if-nez v0, :cond_4b

    .line 628
    const/16 v0, 0x20

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 631
    :cond_4b
    const/4 v0, 0x7

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 633
    const/16 v0, 0xd

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 634
    const/16 v0, 0xf

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 636
    const/16 v0, 0x14

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 637
    const/16 v0, 0xb

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 638
    const/16 v0, 0x26

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 639
    const/16 v0, 0x19

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 640
    const/16 v0, 0x15

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 644
    const/16 v0, 0x10

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 645
    const/16 v0, 0x1d

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 646
    const/16 v0, 0x1f

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 647
    const/16 v0, 0x17

    const/16 v3, 0x32

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 648
    const/16 v0, 0x18

    iget v3, p0, Lp/g;->j:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 650
    sget-object v0, Lp/g;->k:Ljava/lang/String;

    if-eqz v0, :cond_9a

    .line 651
    const/16 v0, 0x23

    sget-object v3, Lp/g;->k:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 657
    :cond_9a
    iget v0, p0, Lp/g;->j:I

    if-nez v0, :cond_c0

    .line 658
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/wireless/googlenav/proto/j2me/do;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 659
    iget-object v3, p0, Lp/g;->b:[Lp/Q;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lp/Q;->c()Lo/s;

    move-result-object v3

    invoke-static {v3}, Lu/e;->b(Lo/s;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v0, v6, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 661
    invoke-virtual {v0, v8, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 662
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 663
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 666
    :cond_c0
    iget-object v0, p0, Lp/g;->c:[Lp/b;

    if-eqz v0, :cond_d7

    .line 667
    iget-object v0, p0, Lp/g;->c:[Lp/b;

    array-length v3, v0

    :goto_c7
    if-ge v1, v3, :cond_d7

    aget-object v4, v0, v1

    .line 668
    const/16 v5, 0xa

    invoke-virtual {v4}, Lp/b;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 667
    add-int/lit8 v1, v1, 0x1

    goto :goto_c7

    .line 673
    :cond_d7
    iget v0, p0, Lp/g;->g:F

    cmpl-float v0, v0, v7

    if-gez v0, :cond_e3

    iget v0, p0, Lp/g;->h:F

    cmpl-float v0, v0, v7

    if-ltz v0, :cond_107

    .line 674
    :cond_e3
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/aY;->z:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 675
    iget v1, p0, Lp/g;->g:F

    cmpl-float v1, v1, v7

    if-ltz v1, :cond_f6

    .line 676
    iget v1, p0, Lp/g;->g:F

    float-to-int v1, v1

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 678
    :cond_f6
    iget v1, p0, Lp/g;->h:F

    cmpl-float v1, v1, v7

    if-ltz v1, :cond_102

    .line 679
    iget v1, p0, Lp/g;->h:F

    float-to-int v1, v1

    invoke-virtual {v0, v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 681
    :cond_102
    const/16 v1, 0x11

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 684
    :cond_107
    return-object v2
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 738
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "["

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 739
    const-string v0, " mode:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lp/g;->a:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 740
    const-string v0, " action:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lp/g;->j:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 741
    const-string v0, " startBearing:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lp/g;->g:F

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 742
    const-string v0, " startSpeed:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lp/g;->h:F

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 743
    const-string v0, " waypoints: ["

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 744
    :goto_3a
    iget-object v3, p0, Lp/g;->b:[Lp/Q;

    array-length v3, v3

    if-ge v0, v3, :cond_60

    .line 745
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lp/g;->b:[Lp/Q;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 746
    iget-object v3, p0, Lp/g;->b:[Lp/Q;

    aget-object v3, v3, v0

    iget-object v4, p0, Lp/g;->i:Lp/Q;

    invoke-virtual {v3, v4}, Lp/Q;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5d

    .line 747
    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 744
    :cond_5d
    add-int/lit8 v0, v0, 0x1

    goto :goto_3a

    .line 750
    :cond_60
    iget-boolean v0, p0, Lp/g;->d:Z

    if-eqz v0, :cond_69

    .line 751
    const-string v0, " hasUncertainFromPoint:true"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 753
    :cond_69
    const-string v0, " maxTripCount:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lp/g;->f:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 754
    iget-object v0, p0, Lp/g;->c:[Lp/b;

    if-eqz v0, :cond_8b

    .line 755
    iget-object v0, p0, Lp/g;->c:[Lp/b;

    array-length v3, v0

    :goto_7b
    if-ge v1, v3, :cond_8b

    aget-object v4, v0, v1

    .line 756
    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 755
    add-int/lit8 v1, v1, 0x1

    goto :goto_7b

    .line 759
    :cond_8b
    iget v0, p0, Lp/g;->e:I

    if-eqz v0, :cond_9a

    .line 760
    const-string v0, " previousAlternateExtraMeters:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lp/g;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 762
    :cond_9a
    const-string v0, " ] ]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
