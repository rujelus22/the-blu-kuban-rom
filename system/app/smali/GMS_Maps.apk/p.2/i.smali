.class public Lp/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:[Lp/Q;

.field private final b:I

.field private final c:I

.field private d:F

.field private e:F

.field private f:Z

.field private g:Lp/Q;

.field private h:I

.field private i:I

.field private j:[Lp/b;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 12
    .parameter

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x1

    const/4 v0, 0x0

    const/high16 v1, -0x4080

    const/4 v7, 0x2

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    iput v1, p0, Lp/i;->d:F

    .line 168
    iput v1, p0, Lp/i;->e:F

    .line 174
    const/16 v1, 0x2710

    iput v1, p0, Lp/i;->h:I

    .line 175
    const/4 v1, 0x3

    iput v1, p0, Lp/i;->i:I

    .line 185
    invoke-virtual {p1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    iput v1, p0, Lp/i;->b:I

    .line 186
    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 187
    new-array v1, v2, [Lp/Q;

    iput-object v1, p0, Lp/i;->a:[Lp/Q;

    move v1, v0

    .line 188
    :goto_24
    if-ge v1, v2, :cond_37

    .line 189
    invoke-virtual {p1, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 190
    iget-object v4, p0, Lp/i;->a:[Lp/Q;

    new-instance v5, Lp/Q;

    const/4 v6, 0x0

    invoke-direct {v5, v3, v6}, Lp/Q;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v5, v4, v1

    .line 188
    add-int/lit8 v1, v1, 0x1

    goto :goto_24

    .line 192
    :cond_37
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    iput v1, p0, Lp/i;->i:I

    .line 193
    const/16 v1, 0x18

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    iput v1, p0, Lp/i;->c:I

    .line 194
    invoke-virtual {p1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 195
    new-array v2, v1, [Lp/b;

    iput-object v2, p0, Lp/i;->j:[Lp/b;

    .line 196
    :goto_4e
    if-ge v0, v1, :cond_5f

    .line 197
    invoke-virtual {p1, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 198
    iget-object v3, p0, Lp/i;->j:[Lp/b;

    invoke-static {v2}, Lp/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lp/b;

    move-result-object v2

    aput-object v2, v3, v0

    .line 196
    add-int/lit8 v0, v0, 0x1

    goto :goto_4e

    .line 200
    :cond_5f
    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_75

    .line 202
    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lp/i;->d:F

    .line 203
    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lp/i;->e:F

    .line 213
    :cond_75
    return-void
.end method

.method public constructor <init>([Lp/Q;II)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v0, -0x4080

    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    iput v0, p0, Lp/i;->d:F

    .line 168
    iput v0, p0, Lp/i;->e:F

    .line 174
    const/16 v0, 0x2710

    iput v0, p0, Lp/i;->h:I

    .line 175
    const/4 v0, 0x3

    iput v0, p0, Lp/i;->i:I

    .line 179
    iput-object p1, p0, Lp/i;->a:[Lp/Q;

    .line 180
    iput p2, p0, Lp/i;->b:I

    .line 181
    iput p3, p0, Lp/i;->c:I

    .line 182
    return-void
.end method


# virtual methods
.method public a()Lp/g;
    .registers 13

    .prologue
    .line 247
    new-instance v0, Lp/g;

    iget-object v1, p0, Lp/i;->a:[Lp/Q;

    iget v2, p0, Lp/i;->b:I

    iget v3, p0, Lp/i;->d:F

    iget v4, p0, Lp/i;->e:F

    iget-boolean v5, p0, Lp/i;->f:Z

    iget-object v6, p0, Lp/i;->g:Lp/Q;

    iget v7, p0, Lp/i;->h:I

    iget v8, p0, Lp/i;->i:I

    iget v9, p0, Lp/i;->c:I

    iget-object v10, p0, Lp/i;->j:[Lp/b;

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Lp/g;-><init>([Lp/Q;IFFZLp/Q;III[Lp/b;Lp/h;)V

    .line 250
    iget-object v1, p0, Lp/i;->a:[Lp/Q;

    invoke-static {v1}, Lp/g;->a([Lp/Q;)Z

    move-result v1

    if-nez v1, :cond_26

    .line 251
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lp/g;->a(Lp/g;I)I

    .line 253
    :cond_26
    return-object v0
.end method

.method public a(FF)Lp/i;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 216
    iput p1, p0, Lp/i;->d:F

    .line 217
    iput p2, p0, Lp/i;->e:F

    .line 218
    return-object p0
.end method

.method public a(I)Lp/i;
    .registers 2
    .parameter

    .prologue
    .line 232
    iput p1, p0, Lp/i;->h:I

    .line 233
    return-object p0
.end method

.method public a(Lp/Q;)Lp/i;
    .registers 2
    .parameter

    .prologue
    .line 227
    iput-object p1, p0, Lp/i;->g:Lp/Q;

    .line 228
    return-object p0
.end method

.method public a(Z)Lp/i;
    .registers 2
    .parameter

    .prologue
    .line 222
    iput-boolean p1, p0, Lp/i;->f:Z

    .line 223
    return-object p0
.end method

.method public a([Lp/b;)Lp/i;
    .registers 2
    .parameter

    .prologue
    .line 242
    iput-object p1, p0, Lp/i;->j:[Lp/b;

    .line 243
    return-object p0
.end method

.method public b(I)Lp/i;
    .registers 2
    .parameter

    .prologue
    .line 237
    iput p1, p0, Lp/i;->i:I

    .line 238
    return-object p0
.end method
