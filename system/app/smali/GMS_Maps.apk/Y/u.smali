.class abstract enum LY/u;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LY/u;

.field public static final enum b:LY/u;

.field public static final enum c:LY/u;

.field public static final enum d:LY/u;

.field public static final enum e:LY/u;

.field public static final enum f:LY/u;

.field public static final enum g:LY/u;

.field public static final enum h:LY/u;

.field static final i:[LY/u;

.field private static final synthetic j:[LY/u;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 438
    new-instance v0, LY/v;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, LY/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/u;->a:LY/u;

    .line 445
    new-instance v0, LY/w;

    const-string v1, "STRONG_ACCESS"

    invoke-direct {v0, v1, v4}, LY/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/u;->b:LY/u;

    .line 460
    new-instance v0, LY/x;

    const-string v1, "STRONG_WRITE"

    invoke-direct {v0, v1, v5}, LY/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/u;->c:LY/u;

    .line 475
    new-instance v0, LY/y;

    const-string v1, "STRONG_ACCESS_WRITE"

    invoke-direct {v0, v1, v6}, LY/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/u;->d:LY/u;

    .line 492
    new-instance v0, LY/z;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v7}, LY/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/u;->e:LY/u;

    .line 499
    new-instance v0, LY/A;

    const-string v1, "WEAK_ACCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LY/A;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/u;->f:LY/u;

    .line 514
    new-instance v0, LY/B;

    const-string v1, "WEAK_WRITE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LY/B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/u;->g:LY/u;

    .line 529
    new-instance v0, LY/C;

    const-string v1, "WEAK_ACCESS_WRITE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LY/C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/u;->h:LY/u;

    .line 437
    const/16 v0, 0x8

    new-array v0, v0, [LY/u;

    sget-object v1, LY/u;->a:LY/u;

    aput-object v1, v0, v3

    sget-object v1, LY/u;->b:LY/u;

    aput-object v1, v0, v4

    sget-object v1, LY/u;->c:LY/u;

    aput-object v1, v0, v5

    sget-object v1, LY/u;->d:LY/u;

    aput-object v1, v0, v6

    sget-object v1, LY/u;->e:LY/u;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LY/u;->f:LY/u;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LY/u;->g:LY/u;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LY/u;->h:LY/u;

    aput-object v2, v0, v1

    sput-object v0, LY/u;->j:[LY/u;

    .line 556
    const/16 v0, 0x8

    new-array v0, v0, [LY/u;

    sget-object v1, LY/u;->a:LY/u;

    aput-object v1, v0, v3

    sget-object v1, LY/u;->b:LY/u;

    aput-object v1, v0, v4

    sget-object v1, LY/u;->c:LY/u;

    aput-object v1, v0, v5

    sget-object v1, LY/u;->d:LY/u;

    aput-object v1, v0, v6

    sget-object v1, LY/u;->e:LY/u;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LY/u;->f:LY/u;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LY/u;->g:LY/u;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LY/u;->h:LY/u;

    aput-object v2, v0, v1

    sput-object v0, LY/u;->i:[LY/u;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 437
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILY/o;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 437
    invoke-direct {p0, p1, p2}, LY/u;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(LY/P;ZZ)LY/u;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 563
    sget-object v1, LY/P;->c:LY/P;

    if-ne p0, v1, :cond_14

    const/4 v1, 0x4

    move v2, v1

    :goto_7
    if-eqz p1, :cond_16

    const/4 v1, 0x1

    :goto_a
    or-int/2addr v1, v2

    if-eqz p2, :cond_e

    const/4 v0, 0x2

    :cond_e
    or-int/2addr v0, v1

    .line 566
    sget-object v1, LY/u;->i:[LY/u;

    aget-object v0, v1, v0

    return-object v0

    :cond_14
    move v2, v0

    .line 563
    goto :goto_7

    :cond_16
    move v1, v0

    goto :goto_a
.end method

.method public static valueOf(Ljava/lang/String;)LY/u;
    .registers 2
    .parameter

    .prologue
    .line 437
    const-class v0, LY/u;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LY/u;

    return-object v0
.end method

.method public static values()[LY/u;
    .registers 1

    .prologue
    .line 437
    sget-object v0, LY/u;->j:[LY/u;

    invoke-virtual {v0}, [LY/u;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LY/u;

    return-object v0
.end method


# virtual methods
.method a(LY/M;LY/L;LY/L;)LY/L;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 589
    invoke-interface {p2}, LY/L;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, LY/L;->c()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, LY/u;->a(LY/M;Ljava/lang/Object;ILY/L;)LY/L;

    move-result-object v0

    return-object v0
.end method

.method abstract a(LY/M;Ljava/lang/Object;ILY/L;)LY/L;
.end method

.method a(LY/L;LY/L;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 596
    invoke-interface {p1}, LY/L;->e()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, LY/L;->a(J)V

    .line 598
    invoke-interface {p1}, LY/L;->g()LY/L;

    move-result-object v0

    invoke-static {v0, p2}, LY/n;->a(LY/L;LY/L;)V

    .line 599
    invoke-interface {p1}, LY/L;->f()LY/L;

    move-result-object v0

    invoke-static {p2, v0}, LY/n;->a(LY/L;LY/L;)V

    .line 601
    invoke-static {p1}, LY/n;->b(LY/L;)V

    .line 602
    return-void
.end method

.method b(LY/L;LY/L;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 608
    invoke-interface {p1}, LY/L;->h()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, LY/L;->b(J)V

    .line 610
    invoke-interface {p1}, LY/L;->j()LY/L;

    move-result-object v0

    invoke-static {v0, p2}, LY/n;->b(LY/L;LY/L;)V

    .line 611
    invoke-interface {p1}, LY/L;->i()LY/L;

    move-result-object v0

    invoke-static {p2, v0}, LY/n;->b(LY/L;LY/L;)V

    .line 613
    invoke-static {p1}, LY/n;->c(LY/L;)V

    .line 614
    return-void
.end method
