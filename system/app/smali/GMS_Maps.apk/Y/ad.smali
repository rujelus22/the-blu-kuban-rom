.class LY/ad;
.super Ljava/lang/ref/WeakReference;
.source "SourceFile"

# interfaces
.implements LY/L;


# instance fields
.field final g:I

.field final h:LY/L;

.field volatile i:LY/Z;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILY/L;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1347
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 1425
    invoke-static {}, LY/n;->o()LY/Z;

    move-result-object v0

    iput-object v0, p0, LY/ad;->i:LY/Z;

    .line 1348
    iput p3, p0, LY/ad;->g:I

    .line 1349
    iput-object p4, p0, LY/ad;->h:LY/L;

    .line 1350
    return-void
.end method


# virtual methods
.method public a()LY/Z;
    .registers 2

    .prologue
    .line 1429
    iget-object v0, p0, LY/ad;->i:LY/Z;

    return-object v0
.end method

.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 1366
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(LY/L;)V
    .registers 3
    .parameter

    .prologue
    .line 1376
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(LY/Z;)V
    .registers 2
    .parameter

    .prologue
    .line 1434
    iput-object p1, p0, LY/ad;->i:LY/Z;

    .line 1435
    return-void
.end method

.method public b()LY/L;
    .registers 2

    .prologue
    .line 1444
    iget-object v0, p0, LY/ad;->h:LY/L;

    return-object v0
.end method

.method public b(J)V
    .registers 4
    .parameter

    .prologue
    .line 1398
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(LY/L;)V
    .registers 3
    .parameter

    .prologue
    .line 1386
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 1439
    iget v0, p0, LY/ad;->g:I

    return v0
.end method

.method public c(LY/L;)V
    .registers 3
    .parameter

    .prologue
    .line 1408
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1354
    invoke-virtual {p0}, LY/ad;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d(LY/L;)V
    .registers 3
    .parameter

    .prologue
    .line 1418
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .registers 2

    .prologue
    .line 1361
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()LY/L;
    .registers 2

    .prologue
    .line 1371
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()LY/L;
    .registers 2

    .prologue
    .line 1381
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()J
    .registers 2

    .prologue
    .line 1393
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()LY/L;
    .registers 2

    .prologue
    .line 1403
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public j()LY/L;
    .registers 2

    .prologue
    .line 1413
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
