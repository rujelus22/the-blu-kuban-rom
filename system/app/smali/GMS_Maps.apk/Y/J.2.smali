.class LY/J;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LY/d;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field final a:LY/n;


# direct methods
.method constructor <init>(LY/e;)V
    .registers 3
    .parameter

    .prologue
    .line 4745
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LY/J;-><init>(LY/e;LY/k;)V

    .line 4746
    return-void
.end method

.method protected constructor <init>(LY/e;LY/k;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4750
    new-instance v0, LY/n;

    invoke-direct {v0, p1, p2}, LY/n;-><init>(LY/e;LY/k;)V

    iput-object v0, p0, LY/J;->a:LY/n;

    .line 4751
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 4841
    invoke-virtual {p0, p1}, LY/J;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/concurrent/ConcurrentMap;
    .registers 2

    .prologue
    .line 4805
    iget-object v0, p0, LY/J;->a:LY/n;

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 4829
    iget-object v0, p0, LY/J;->a:LY/n;

    invoke-virtual {v0, p1}, LY/n;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    .line 4834
    :try_start_0
    invoke-virtual {p0, p1}, LY/J;->b(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    return-object v0

    .line 4835
    :catch_5
    move-exception v0

    .line 4836
    new-instance v1, Lae/p;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v0}, Lae/p;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
