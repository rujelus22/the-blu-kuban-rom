.class public abstract enum LY/an;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LY/an;

.field public static final enum b:LY/an;

.field public static final enum c:LY/an;

.field public static final enum d:LY/an;

.field public static final enum e:LY/an;

.field private static final synthetic f:[LY/an;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, LY/ao;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v2}, LY/ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/an;->a:LY/an;

    .line 51
    new-instance v0, LY/ap;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v3}, LY/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/an;->b:LY/an;

    .line 63
    new-instance v0, LY/aq;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1, v4}, LY/aq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/an;->c:LY/an;

    .line 74
    new-instance v0, LY/ar;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5}, LY/ar;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/an;->d:LY/an;

    .line 85
    new-instance v0, LY/as;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v6}, LY/as;-><init>(Ljava/lang/String;I)V

    sput-object v0, LY/an;->e:LY/an;

    .line 31
    const/4 v0, 0x5

    new-array v0, v0, [LY/an;

    sget-object v1, LY/an;->a:LY/an;

    aput-object v1, v0, v2

    sget-object v1, LY/an;->b:LY/an;

    aput-object v1, v0, v3

    sget-object v1, LY/an;->c:LY/an;

    aput-object v1, v0, v4

    sget-object v1, LY/an;->d:LY/an;

    aput-object v1, v0, v5

    sget-object v1, LY/an;->e:LY/an;

    aput-object v1, v0, v6

    sput-object v0, LY/an;->f:[LY/an;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILY/ao;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, LY/an;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LY/an;
    .registers 2
    .parameter

    .prologue
    .line 31
    const-class v0, LY/an;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LY/an;

    return-object v0
.end method

.method public static values()[LY/an;
    .registers 1

    .prologue
    .line 31
    sget-object v0, LY/an;->f:[LY/an;

    invoke-virtual {v0}, [LY/an;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LY/an;

    return-object v0
.end method


# virtual methods
.method abstract a()Z
.end method
