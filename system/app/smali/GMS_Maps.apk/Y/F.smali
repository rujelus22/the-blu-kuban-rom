.class abstract LY/F;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field b:I

.field c:I

.field d:LY/M;

.field e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field f:LY/L;

.field g:LY/am;

.field h:LY/am;

.field final synthetic i:LY/n;


# direct methods
.method constructor <init>(LY/n;)V
    .registers 3
    .parameter

    .prologue
    .line 4290
    iput-object p1, p0, LY/F;->i:LY/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4291
    iget-object v0, p1, LY/n;->e:[LY/M;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LY/F;->b:I

    .line 4292
    const/4 v0, -0x1

    iput v0, p0, LY/F;->c:I

    .line 4293
    invoke-virtual {p0}, LY/F;->b()V

    .line 4294
    return-void
.end method


# virtual methods
.method a(LY/L;)Z
    .registers 6
    .parameter

    .prologue
    .line 4353
    :try_start_0
    iget-object v0, p0, LY/F;->i:LY/n;

    iget-object v0, v0, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v0}, Lcom/google/common/base/ae;->a()J

    move-result-wide v0

    .line 4354
    invoke-interface {p1}, LY/L;->d()Ljava/lang/Object;

    move-result-object v2

    .line 4355
    iget-object v3, p0, LY/F;->i:LY/n;

    invoke-virtual {v3, p1, v0, v1}, LY/n;->a(LY/L;J)Ljava/lang/Object;

    move-result-object v0

    .line 4356
    if-eqz v0, :cond_24

    .line 4357
    new-instance v1, LY/am;

    iget-object v3, p0, LY/F;->i:LY/n;

    invoke-direct {v1, v3, v2, v0}, LY/am;-><init>(LY/n;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, LY/F;->g:LY/am;
    :try_end_1d
    .catchall {:try_start_0 .. :try_end_1d} :catchall_2b

    .line 4358
    const/4 v0, 0x1

    .line 4364
    iget-object v1, p0, LY/F;->d:LY/M;

    invoke-virtual {v1}, LY/M;->m()V

    :goto_23
    return v0

    .line 4361
    :cond_24
    const/4 v0, 0x0

    .line 4364
    iget-object v1, p0, LY/F;->d:LY/M;

    invoke-virtual {v1}, LY/M;->m()V

    goto :goto_23

    :catchall_2b
    move-exception v0

    iget-object v1, p0, LY/F;->d:LY/M;

    invoke-virtual {v1}, LY/M;->m()V

    throw v0
.end method

.method final b()V
    .registers 4

    .prologue
    .line 4297
    const/4 v0, 0x0

    iput-object v0, p0, LY/F;->g:LY/am;

    .line 4299
    invoke-virtual {p0}, LY/F;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 4317
    :cond_9
    :goto_9
    return-void

    .line 4303
    :cond_a
    invoke-virtual {p0}, LY/F;->d()Z

    move-result v0

    if-nez v0, :cond_9

    .line 4307
    :cond_10
    iget v0, p0, LY/F;->b:I

    if-ltz v0, :cond_9

    .line 4308
    iget-object v0, p0, LY/F;->i:LY/n;

    iget-object v0, v0, LY/n;->e:[LY/M;

    iget v1, p0, LY/F;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, LY/F;->b:I

    aget-object v0, v0, v1

    iput-object v0, p0, LY/F;->d:LY/M;

    .line 4309
    iget-object v0, p0, LY/F;->d:LY/M;

    iget v0, v0, LY/M;->b:I

    if-eqz v0, :cond_10

    .line 4310
    iget-object v0, p0, LY/F;->d:LY/M;

    iget-object v0, v0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, LY/F;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 4311
    iget-object v0, p0, LY/F;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LY/F;->c:I

    .line 4312
    invoke-virtual {p0}, LY/F;->d()Z

    move-result v0

    if-eqz v0, :cond_10

    goto :goto_9
.end method

.method c()Z
    .registers 2

    .prologue
    .line 4323
    iget-object v0, p0, LY/F;->f:LY/L;

    if-eqz v0, :cond_23

    .line 4324
    iget-object v0, p0, LY/F;->f:LY/L;

    invoke-interface {v0}, LY/L;->b()LY/L;

    move-result-object v0

    iput-object v0, p0, LY/F;->f:LY/L;

    :goto_c
    iget-object v0, p0, LY/F;->f:LY/L;

    if-eqz v0, :cond_23

    .line 4325
    iget-object v0, p0, LY/F;->f:LY/L;

    invoke-virtual {p0, v0}, LY/F;->a(LY/L;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 4326
    const/4 v0, 0x1

    .line 4330
    :goto_19
    return v0

    .line 4324
    :cond_1a
    iget-object v0, p0, LY/F;->f:LY/L;

    invoke-interface {v0}, LY/L;->b()LY/L;

    move-result-object v0

    iput-object v0, p0, LY/F;->f:LY/L;

    goto :goto_c

    .line 4330
    :cond_23
    const/4 v0, 0x0

    goto :goto_19
.end method

.method d()Z
    .registers 4

    .prologue
    .line 4337
    :cond_0
    iget v0, p0, LY/F;->c:I

    if-ltz v0, :cond_26

    .line 4338
    iget-object v0, p0, LY/F;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, LY/F;->c:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, LY/F;->c:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    iput-object v0, p0, LY/F;->f:LY/L;

    if-eqz v0, :cond_0

    .line 4339
    iget-object v0, p0, LY/F;->f:LY/L;

    invoke-virtual {p0, v0}, LY/F;->a(LY/L;)Z

    move-result v0

    if-nez v0, :cond_24

    invoke-virtual {p0}, LY/F;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4340
    :cond_24
    const/4 v0, 0x1

    .line 4344
    :goto_25
    return v0

    :cond_26
    const/4 v0, 0x0

    goto :goto_25
.end method

.method e()LY/am;
    .registers 2

    .prologue
    .line 4373
    iget-object v0, p0, LY/F;->g:LY/am;

    if-nez v0, :cond_a

    .line 4374
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 4376
    :cond_a
    iget-object v0, p0, LY/F;->g:LY/am;

    iput-object v0, p0, LY/F;->h:LY/am;

    .line 4377
    invoke-virtual {p0}, LY/F;->b()V

    .line 4378
    iget-object v0, p0, LY/F;->h:LY/am;

    return-object v0
.end method

.method public hasNext()Z
    .registers 2

    .prologue
    .line 4369
    iget-object v0, p0, LY/F;->g:LY/am;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public remove()V
    .registers 3

    .prologue
    .line 4382
    iget-object v0, p0, LY/F;->h:LY/am;

    if-eqz v0, :cond_17

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    .line 4383
    iget-object v0, p0, LY/F;->i:LY/n;

    iget-object v1, p0, LY/F;->h:LY/am;

    invoke-virtual {v1}, LY/am;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LY/n;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4384
    const/4 v0, 0x0

    iput-object v0, p0, LY/F;->h:LY/am;

    .line 4385
    return-void

    .line 4382
    :cond_17
    const/4 v0, 0x0

    goto :goto_5
.end method
