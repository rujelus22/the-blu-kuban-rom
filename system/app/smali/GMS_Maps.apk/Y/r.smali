.class final LY/r;
.super Ljava/util/AbstractQueue;
.source "SourceFile"


# instance fields
.field final a:LY/L;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 3759
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 3760
    new-instance v0, LY/s;

    invoke-direct {v0, p0}, LY/s;-><init>(LY/r;)V

    iput-object v0, p0, LY/r;->a:LY/L;

    return-void
.end method


# virtual methods
.method public a()LY/L;
    .registers 3

    .prologue
    .line 3811
    iget-object v0, p0, LY/r;->a:LY/L;

    invoke-interface {v0}, LY/L;->f()LY/L;

    move-result-object v0

    .line 3812
    iget-object v1, p0, LY/r;->a:LY/L;

    if-ne v0, v1, :cond_b

    const/4 v0, 0x0

    :cond_b
    return-object v0
.end method

.method public a(LY/L;)Z
    .registers 4
    .parameter

    .prologue
    .line 3800
    invoke-interface {p1}, LY/L;->g()LY/L;

    move-result-object v0

    invoke-interface {p1}, LY/L;->f()LY/L;

    move-result-object v1

    invoke-static {v0, v1}, LY/n;->a(LY/L;LY/L;)V

    .line 3803
    iget-object v0, p0, LY/r;->a:LY/L;

    invoke-interface {v0}, LY/L;->g()LY/L;

    move-result-object v0

    invoke-static {v0, p1}, LY/n;->a(LY/L;LY/L;)V

    .line 3804
    iget-object v0, p0, LY/r;->a:LY/L;

    invoke-static {p1, v0}, LY/n;->a(LY/L;LY/L;)V

    .line 3806
    const/4 v0, 0x1

    return v0
.end method

.method public b()LY/L;
    .registers 3

    .prologue
    .line 3817
    iget-object v0, p0, LY/r;->a:LY/L;

    invoke-interface {v0}, LY/L;->f()LY/L;

    move-result-object v0

    .line 3818
    iget-object v1, p0, LY/r;->a:LY/L;

    if-ne v0, v1, :cond_c

    .line 3819
    const/4 v0, 0x0

    .line 3823
    :goto_b
    return-object v0

    .line 3822
    :cond_c
    invoke-virtual {p0, v0}, LY/r;->remove(Ljava/lang/Object;)Z

    goto :goto_b
.end method

.method public clear()V
    .registers 3

    .prologue
    .line 3862
    iget-object v0, p0, LY/r;->a:LY/L;

    invoke-interface {v0}, LY/L;->f()LY/L;

    move-result-object v0

    .line 3863
    :goto_6
    iget-object v1, p0, LY/r;->a:LY/L;

    if-eq v0, v1, :cond_13

    .line 3864
    invoke-interface {v0}, LY/L;->f()LY/L;

    move-result-object v1

    .line 3865
    invoke-static {v0}, LY/n;->b(LY/L;)V

    move-object v0, v1

    .line 3867
    goto :goto_6

    .line 3869
    :cond_13
    iget-object v0, p0, LY/r;->a:LY/L;

    iget-object v1, p0, LY/r;->a:LY/L;

    invoke-interface {v0, v1}, LY/L;->a(LY/L;)V

    .line 3870
    iget-object v0, p0, LY/r;->a:LY/L;

    iget-object v1, p0, LY/r;->a:LY/L;

    invoke-interface {v0, v1}, LY/L;->b(LY/L;)V

    .line 3871
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 3841
    check-cast p1, LY/L;

    .line 3842
    invoke-interface {p1}, LY/L;->f()LY/L;

    move-result-object v0

    sget-object v1, LY/K;->a:LY/K;

    if-eq v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public isEmpty()Z
    .registers 3

    .prologue
    .line 3847
    iget-object v0, p0, LY/r;->a:LY/L;

    invoke-interface {v0}, LY/L;->f()LY/L;

    move-result-object v0

    iget-object v1, p0, LY/r;->a:LY/L;

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 3875
    new-instance v0, LY/t;

    invoke-virtual {p0}, LY/r;->a()LY/L;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LY/t;-><init>(LY/r;LY/L;)V

    return-object v0
.end method

.method public synthetic offer(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 3759
    check-cast p1, LY/L;

    invoke-virtual {p0, p1}, LY/r;->a(LY/L;)Z

    move-result v0

    return v0
.end method

.method public synthetic peek()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3759
    invoke-virtual {p0}, LY/r;->a()LY/L;

    move-result-object v0

    return-object v0
.end method

.method public synthetic poll()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3759
    invoke-virtual {p0}, LY/r;->b()LY/L;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 3829
    check-cast p1, LY/L;

    .line 3830
    invoke-interface {p1}, LY/L;->g()LY/L;

    move-result-object v0

    .line 3831
    invoke-interface {p1}, LY/L;->f()LY/L;

    move-result-object v1

    .line 3832
    invoke-static {v0, v1}, LY/n;->a(LY/L;LY/L;)V

    .line 3833
    invoke-static {p1}, LY/n;->b(LY/L;)V

    .line 3835
    sget-object v0, LY/K;->a:LY/K;

    if-eq v1, v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public size()I
    .registers 4

    .prologue
    .line 3852
    const/4 v1, 0x0

    .line 3853
    iget-object v0, p0, LY/r;->a:LY/L;

    invoke-interface {v0}, LY/L;->f()LY/L;

    move-result-object v0

    :goto_7
    iget-object v2, p0, LY/r;->a:LY/L;

    if-eq v0, v2, :cond_12

    .line 3855
    add-int/lit8 v1, v1, 0x1

    .line 3854
    invoke-interface {v0}, LY/L;->f()LY/L;

    move-result-object v0

    goto :goto_7

    .line 3857
    :cond_12
    return v1
.end method
