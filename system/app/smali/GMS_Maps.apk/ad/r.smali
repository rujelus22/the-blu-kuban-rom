.class public LaD/r;
.super LaD/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(LaD/n;)V
    .registers 2
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, LaD/e;-><init>(LaD/n;)V

    .line 63
    return-void
.end method


# virtual methods
.method public a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)LaD/f;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const v8, 0x3e32b8c2

    .line 78
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_d

    .line 82
    sget-object v0, LaD/f;->b:LaD/f;

    .line 138
    :goto_c
    return-object v0

    .line 87
    :cond_d
    invoke-virtual {p3}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/j;

    .line 88
    invoke-virtual {v0}, LaD/j;->f()F

    move-result v3

    .line 90
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-virtual {p3, v1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v4

    .line 91
    const/4 v5, 0x0

    move-object v2, v0

    .line 92
    :goto_21
    invoke-interface {v4}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_37

    .line 93
    invoke-interface {v4}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaD/j;

    .line 94
    invoke-virtual {v1}, LaD/j;->b()I

    move-result v6

    invoke-virtual {v0}, LaD/j;->b()I

    move-result v7

    if-eq v6, v7, :cond_3e

    .line 109
    :cond_37
    cmpl-float v1, v5, v8

    if-lez v1, :cond_53

    .line 114
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_c

    .line 97
    :cond_3e
    invoke-virtual {v1}, LaD/j;->f()F

    move-result v2

    invoke-static {v3, v2}, LaD/r;->a(FF)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 103
    cmpl-float v2, v2, v8

    if-lez v2, :cond_51

    .line 104
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_c

    :cond_51
    move-object v2, v1

    .line 108
    goto :goto_21

    .line 118
    :cond_53
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7e

    const v1, 0x3dcccccd

    .line 122
    :goto_5c
    invoke-virtual {v2}, LaD/j;->g()F

    move-result v2

    .line 123
    invoke-virtual {v0}, LaD/j;->g()F

    move-result v3

    .line 124
    invoke-virtual {v0}, LaD/j;->c()F

    move-result v4

    invoke-virtual {v0}, LaD/j;->d()F

    move-result v0

    add-float/2addr v0, v4

    const/high16 v4, 0x3f00

    mul-float/2addr v0, v4

    .line 125
    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float v0, v2, v0

    .line 126
    cmpg-float v0, v0, v1

    if-gez v0, :cond_82

    .line 130
    sget-object v0, LaD/f;->b:LaD/f;

    goto :goto_c

    .line 118
    :cond_7e
    const v1, 0x3e4ccccd

    goto :goto_5c

    .line 138
    :cond_82
    sget-object v0, LaD/f;->c:LaD/f;

    goto :goto_c
.end method

.method protected b(LaD/k;)Z
    .registers 4
    .parameter

    .prologue
    .line 143
    iget-object v0, p0, LaD/r;->a:LaD/n;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LaD/n;->b(LaD/k;Z)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method protected d(LaD/k;)V
    .registers 4
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, LaD/r;->a:LaD/n;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LaD/n;->c(LaD/k;Z)V

    .line 149
    return-void
.end method

.method protected f(LaD/k;)Z
    .registers 4
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, LaD/r;->a:LaD/n;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LaD/n;->a(LaD/k;Z)Z

    move-result v0

    return v0
.end method
