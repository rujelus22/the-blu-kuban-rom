.class public LaO/e;
.super LaN/u;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/bm;


# instance fields
.field private final c:Lcom/google/android/maps/driveabout/vector/bk;

.field private final d:LaO/a;

.field private final e:LC/a;

.field private final f:[F

.field private final g:Lo/T;

.field private final h:Lo/T;

.field private final i:Lcom/google/googlenav/android/aa;

.field private j:F

.field private k:LaO/i;

.field private final l:Lcom/google/android/maps/driveabout/vector/aE;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/bk;LaO/a;LaN/H;Lcom/google/googlenav/android/aa;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    .line 123
    invoke-direct {p0}, LaN/u;-><init>()V

    .line 72
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, LaO/e;->f:[F

    .line 78
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LaO/e;->g:Lo/T;

    .line 85
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LaO/e;->h:Lo/T;

    .line 92
    iput v4, p0, LaO/e;->j:F

    .line 102
    new-instance v0, LaO/f;

    invoke-direct {v0, p0}, LaO/f;-><init>(LaO/e;)V

    iput-object v0, p0, LaO/e;->l:Lcom/google/android/maps/driveabout/vector/aE;

    .line 124
    iput-object p1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    .line 125
    invoke-virtual {p1, p0}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lcom/google/android/maps/driveabout/vector/bm;)V

    .line 126
    iput-object p2, p0, LaO/e;->d:LaO/a;

    .line 127
    iput-object p4, p0, LaO/e;->i:Lcom/google/googlenav/android/aa;

    .line 128
    invoke-static {p3, v4}, LaO/e;->a(LaN/H;F)LC/b;

    move-result-object v0

    .line 130
    new-instance v1, LC/a;

    invoke-virtual {p2}, LaO/a;->t()I

    move-result v2

    invoke-virtual {p2}, LaO/a;->s()I

    move-result v3

    invoke-direct {v1, v0, v2, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    iput-object v1, p0, LaO/e;->e:LC/a;

    .line 131
    iget-object v1, p0, LaO/e;->l:Lcom/google/android/maps/driveabout/vector/aE;

    invoke-virtual {p1, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lcom/google/android/maps/driveabout/vector/aE;)V

    .line 132
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V

    .line 133
    invoke-virtual {p2}, LaO/a;->t()I

    move-result v0

    invoke-virtual {p2}, LaO/a;->s()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LaO/e;->c(II)V

    .line 135
    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lcom/google/android/maps/driveabout/vector/bq;)V

    .line 136
    return-void
.end method

.method public static a(LaN/Y;F)F
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 176
    invoke-virtual {p0}, LaN/Y;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    return v0
.end method

.method static a(LaN/H;F)LC/b;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 161
    invoke-virtual {p0}, LaN/H;->a()LaN/B;

    move-result-object v1

    .line 162
    new-instance v0, LC/b;

    invoke-static {v1}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v1

    invoke-virtual {p0}, LaN/H;->b()LaN/Y;

    move-result-object v2

    invoke-static {v2, p1}, LaO/e;->a(LaN/Y;F)F

    move-result v2

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    .line 168
    return-object v0
.end method

.method private declared-synchronized a(IILC/b;)LaN/B;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 613
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0, p3}, LC/a;->a(LC/b;)V

    .line 614
    iget-object v0, p0, LaO/e;->e:LC/a;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    invoke-static {v0}, LR/e;->b(Lo/T;)LaN/B;
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_14

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 613
    :catchall_14
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(LC/b;)LaN/B;
    .registers 3
    .parameter

    .prologue
    .line 260
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0, p1}, LC/a;->a(LC/b;)V

    .line 261
    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0}, LC/a;->h()Lo/T;

    move-result-object v0

    invoke-static {v0}, LR/e;->b(Lo/T;)LaN/B;
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_12

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 260
    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(LaO/e;)LaO/a;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, LaO/e;->d:LaO/a;

    return-object v0
.end method

.method static synthetic a(LaO/e;LaO/i;)LaO/i;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, LaO/e;->k:LaO/i;

    return-object p1
.end method

.method private declared-synchronized a(LaN/B;LaN/Y;Z)V
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    .line 336
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v4

    .line 337
    invoke-virtual {v4}, LC/b;->a()F

    move-result v0

    .line 341
    if-eqz p2, :cond_10b

    .line 342
    iget v1, p0, LaO/e;->j:F

    invoke-static {p2, v1}, LaO/e;->a(LaN/Y;F)F

    move-result v2

    .line 343
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v3

    if-eq v1, v3, :cond_10b

    .line 348
    :goto_1d
    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0, v4}, LC/a;->a(LC/b;)V

    .line 349
    iget v0, p0, LaO/e;->a:I

    div-int/lit8 v0, v0, 0x2

    .line 350
    iget v1, p0, LaO/e;->b:I

    div-int/lit8 v1, v1, 0x2

    .line 351
    iget-object v3, p0, LaO/e;->g:Lo/T;

    invoke-static {p1, v3}, LR/e;->a(LaN/B;Lo/T;)V

    .line 352
    iget-object v3, p0, LaO/e;->e:LC/a;

    iget-object v5, p0, LaO/e;->g:Lo/T;

    invoke-virtual {v3, v5}, LC/a;->b(Lo/T;)[I

    move-result-object v3

    .line 353
    const/4 v5, 0x0

    aget v5, v3, v5

    sub-int v7, v5, v0

    .line 354
    const/4 v0, 0x1

    aget v0, v3, v0

    sub-int v6, v0, v1

    .line 355
    int-to-double v0, v7

    int-to-double v8, v7

    mul-double/2addr v0, v8

    int-to-double v8, v6

    int-to-double v10, v6

    mul-double/2addr v8, v10

    add-double/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v8, v0

    .line 357
    new-instance v0, LC/b;

    iget-object v1, p0, LaO/e;->g:Lo/T;

    invoke-virtual {v4}, LC/b;->d()F

    move-result v3

    invoke-virtual {v4}, LC/b;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    .line 363
    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LC/b;->equals(Ljava/lang/Object;)Z
    :try_end_66
    .catchall {:try_start_1 .. :try_end_66} :catchall_cd

    move-result v1

    if-eqz v1, :cond_6b

    .line 395
    :cond_69
    :goto_69
    monitor-exit p0

    return-void

    .line 370
    :cond_6b
    const/4 v9, -0x1

    .line 371
    if-eqz p3, :cond_dc

    .line 376
    :try_start_6e
    iget-object v1, p0, LaO/e;->k:LaO/i;

    if-eqz v1, :cond_7a

    iget-object v1, p0, LaO/e;->k:LaO/i;

    invoke-virtual {v1, v0}, LaO/i;->a(LC/b;)Z

    move-result v1

    if-nez v1, :cond_69

    :cond_7a
    invoke-direct {p0, v0, v8}, LaO/e;->a(LC/b;I)Z

    move-result v1

    if-nez v1, :cond_69

    .line 379
    iget-object v1, p0, LaO/e;->g:Lo/T;

    iget-object v3, p0, LaO/e;->e:LC/a;

    invoke-virtual {v3}, LC/a;->h()Lo/T;

    move-result-object v3

    invoke-virtual {v1, v3}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d0

    const/4 v1, 0x1

    move v5, v1

    :goto_90
    iget-object v1, p0, LaO/e;->e:LC/a;

    invoke-virtual {v1}, LC/a;->r()F

    move-result v1

    cmpl-float v1, v2, v1

    if-eqz v1, :cond_d3

    const/4 v3, 0x1

    :goto_9b
    const/4 v4, 0x1

    move-object v1, p0

    move v2, v5

    move v5, v7

    invoke-virtual/range {v1 .. v6}, LaO/e;->a(ZZZII)V

    .line 381
    new-instance v1, LaO/i;

    invoke-direct {v1, v0}, LaO/i;-><init>(LC/b;)V

    iput-object v1, p0, LaO/e;->k:LaO/i;

    .line 382
    iget-object v0, p0, LaO/e;->k:LaO/i;

    invoke-virtual {v0}, LaO/i;->b()LC/b;

    move-result-object v0

    invoke-virtual {v0}, LC/b;->a()F

    move-result v0

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {v1}, LC/b;->a()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_d5

    const/4 v0, 0x1

    .line 384
    :goto_c2
    if-eqz v0, :cond_d7

    const/4 v0, -0x1

    .line 386
    :goto_c5
    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    iget-object v2, p0, LaO/e;->k:LaO/i;

    invoke-virtual {v1, v2, v0, v9}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;II)V
    :try_end_cc
    .catchall {:try_start_6e .. :try_end_cc} :catchall_cd

    goto :goto_69

    .line 336
    :catchall_cd
    move-exception v0

    monitor-exit p0

    throw v0

    .line 379
    :cond_d0
    const/4 v1, 0x0

    move v5, v1

    goto :goto_90

    :cond_d3
    const/4 v3, 0x0

    goto :goto_9b

    .line 382
    :cond_d5
    const/4 v0, 0x0

    goto :goto_c2

    .line 384
    :cond_d7
    :try_start_d7
    invoke-static {v8}, LaO/e;->b(I)I

    move-result v0

    goto :goto_c5

    .line 389
    :cond_dc
    iget-object v1, p0, LaO/e;->g:Lo/T;

    iget-object v3, p0, LaO/e;->e:LC/a;

    invoke-virtual {v3}, LC/a;->h()Lo/T;

    move-result-object v3

    invoke-virtual {v1, v3}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_106

    const/4 v1, 0x1

    move v5, v1

    :goto_ec
    iget-object v1, p0, LaO/e;->e:LC/a;

    invoke-virtual {v1}, LC/a;->r()F

    move-result v1

    cmpl-float v1, v2, v1

    if-eqz v1, :cond_109

    const/4 v3, 0x1

    :goto_f7
    const/4 v4, 0x1

    move-object v1, p0

    move v2, v5

    move v5, v7

    invoke-virtual/range {v1 .. v6}, LaO/e;->a(ZZZII)V

    .line 391
    const/4 v1, -0x1

    .line 393
    iget-object v2, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v2, v0, v1, v9}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;II)V
    :try_end_104
    .catchall {:try_start_d7 .. :try_end_104} :catchall_cd

    goto/16 :goto_69

    .line 389
    :cond_106
    const/4 v1, 0x0

    move v5, v1

    goto :goto_ec

    :cond_109
    const/4 v3, 0x0

    goto :goto_f7

    :cond_10b
    move v2, v0

    goto/16 :goto_1d
.end method

.method private declared-synchronized a(Lo/T;LC/b;Landroid/graphics/Point;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 593
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0, p2}, LC/a;->a(LC/b;)V

    .line 594
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget-object v1, p0, LaO/e;->f:[F

    invoke-virtual {v0, p1, v1}, LC/a;->a(Lo/T;[F)V

    .line 595
    iget-object v0, p0, LaO/e;->f:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, LaO/e;->f:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Point;->set(II)V
    :try_end_22
    .catchall {:try_start_1 .. :try_end_22} :catchall_24

    .line 596
    monitor-exit p0

    return-void

    .line 593
    :catchall_24
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(LC/b;I)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 402
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {p1, v0}, LC/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1e

    invoke-virtual {p1}, LC/b;->a()F

    move-result v0

    iget-object v1, p0, LaO/e;->e:LC/a;

    invoke-virtual {v1}, LC/a;->r()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_20

    const/16 v0, 0xf

    if-ge p2, v0, :cond_20

    :cond_1e
    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method private b(LC/b;)LaN/Y;
    .registers 4
    .parameter

    .prologue
    .line 297
    invoke-virtual {p1}, LC/b;->a()F

    move-result v0

    sget-object v1, LaO/j;->a:LaO/j;

    invoke-virtual {p0, v0, v1}, LaO/e;->a(FLaO/j;)LaN/Y;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(LaO/e;)Lcom/google/googlenav/android/aa;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, LaO/e;->i:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method private g()I
    .registers 6

    .prologue
    .line 226
    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0}, LC/a;->C()Lo/aQ;

    move-result-object v0

    .line 227
    invoke-virtual {v0}, Lo/aQ;->g()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->a()I

    move-result v1

    .line 228
    invoke-virtual {v0}, Lo/aQ;->f()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->a()I

    move-result v2

    .line 229
    invoke-virtual {v0}, Lo/aQ;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3}, Lo/T;->a()I

    move-result v3

    .line 230
    invoke-virtual {v0}, Lo/aQ;->e()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->a()I

    move-result v0

    .line 233
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 234
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 235
    sub-int v0, v4, v0

    return v0
.end method

.method private h()I
    .registers 4

    .prologue
    .line 239
    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0}, LC/a;->C()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->a()Lo/aR;

    move-result-object v0

    invoke-virtual {v0}, Lo/aR;->e()I

    move-result v0

    .line 240
    iget-object v1, p0, LaO/e;->g:Lo/T;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lo/T;->d(II)V

    .line 241
    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-virtual {v0}, Lo/T;->c()I

    move-result v0

    return v0
.end method


# virtual methods
.method public declared-synchronized a(ILaN/B;)F
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 192
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LC/a;->a(LC/b;)V

    .line 193
    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-static {p2, v0}, LR/e;->a(LaN/B;Lo/T;)V

    .line 194
    const/4 v0, 0x1

    .line 195
    iget-object v1, p0, LaO/e;->e:LC/a;

    iget-object v2, p0, LaO/e;->g:Lo/T;

    invoke-virtual {v1, v2, v0}, LC/a;->a(Lo/T;Z)F

    move-result v0

    .line 196
    int-to-float v1, p1

    iget-object v2, p0, LaO/e;->e:LC/a;

    iget-object v3, p0, LaO/e;->g:Lo/T;

    invoke-virtual {v3}, Lo/T;->e()D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v2, v3, v0}, LC/a;->b(FF)F
    :try_end_27
    .catchall {:try_start_1 .. :try_end_27} :catchall_2b

    move-result v0

    mul-float/2addr v0, v1

    monitor-exit p0

    return v0

    .line 192
    :catchall_2b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()I
    .registers 3

    .prologue
    .line 202
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LC/a;->a(LC/b;)V

    .line 203
    invoke-direct {p0}, LaO/e;->g()I
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_12

    move-result v0

    monitor-exit p0

    return v0

    .line 202
    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LaN/B;)I
    .registers 4
    .parameter

    .prologue
    .line 525
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-static {p1, v0}, LR/e;->a(LaN/B;Lo/T;)V

    .line 526
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    iget-object v1, p0, LaO/e;->g:Lo/T;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lo/T;)F

    move-result v0

    float-to-int v0, v0

    .line 531
    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0x16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_19

    move-result v0

    monitor-exit p0

    return v0

    .line 525
    :catchall_19
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LaN/H;)I
    .registers 4
    .parameter

    .prologue
    .line 214
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget v1, p0, LaO/e;->j:F

    invoke-static {p1, v1}, LaO/e;->a(LaN/H;F)LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LC/a;->a(LC/b;)V

    .line 215
    invoke-direct {p0}, LaO/e;->g()I
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_12

    move-result v0

    monitor-exit p0

    return v0

    .line 214
    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(LaN/B;LaN/Y;II)LaN/B;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 600
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v4

    .line 601
    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-static {p1, v0}, LR/e;->a(LaN/B;Lo/T;)V

    .line 602
    const/4 v5, 0x0

    .line 603
    new-instance v0, LC/b;

    iget-object v1, p0, LaO/e;->g:Lo/T;

    iget v2, p0, LaO/e;->j:F

    invoke-static {p2, v2}, LaO/e;->a(LaN/Y;F)F

    move-result v2

    invoke-virtual {v4}, LC/b;->d()F

    move-result v3

    invoke-virtual {v4}, LC/b;->e()F

    move-result v4

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    .line 609
    invoke-direct {p0, p3, p4, v0}, LaO/e;->a(IILC/b;)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method protected a(FLaO/j;)LaN/Y;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 279
    const/high16 v0, 0x3f80

    add-float/2addr v0, p1

    .line 281
    sget-object v1, LaO/h;->a:[I

    invoke-virtual {p2}, LaO/j;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2c

    .line 290
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 293
    :goto_12
    const/16 v1, 0x16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    return-object v0

    .line 283
    :pswitch_1d
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 284
    goto :goto_12

    .line 286
    :pswitch_24
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 287
    goto :goto_12

    .line 281
    nop

    :pswitch_data_2c
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_24
    .end packed-switch
.end method

.method public declared-synchronized a(F)V
    .registers 5
    .parameter

    .prologue
    .line 186
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget v1, p0, LaO/e;->a:I

    iget v2, p0, LaO/e;->b:I

    invoke-virtual {v0, v1, v2, p1}, LC/a;->a(IIF)V

    .line 187
    iput p1, p0, LaO/e;->j:F
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_e

    .line 188
    monitor-exit p0

    return-void

    .line 186
    :catchall_e
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 311
    iget-object v0, p0, LaO/e;->d:LaO/a;

    invoke-virtual {v0, p1}, LaO/a;->b(I)V

    .line 312
    return-void
.end method

.method public declared-synchronized a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 476
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bk;->b(FF)V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    .line 477
    monitor-exit p0

    return-void

    .line 476
    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LaN/B;LaN/Y;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 322
    monitor-enter p0

    const/4 v0, 0x1

    .line 323
    :try_start_2
    invoke-direct {p0, p1, p2, v0}, LaO/e;->a(LaN/B;LaN/Y;Z)V
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_7

    .line 324
    monitor-exit p0

    return-void

    .line 322
    :catchall_7
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(LaN/B;LaN/Y;LaN/B;Landroid/graphics/Point;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 570
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v4

    .line 571
    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-static {p1, v0}, LR/e;->a(LaN/B;Lo/T;)V

    .line 572
    iget-object v0, p0, LaO/e;->h:Lo/T;

    invoke-static {p3, v0}, LR/e;->a(LaN/B;Lo/T;)V

    .line 573
    const/4 v5, 0x0

    .line 574
    new-instance v0, LC/b;

    iget-object v1, p0, LaO/e;->g:Lo/T;

    iget v2, p0, LaO/e;->j:F

    invoke-static {p2, v2}, LaO/e;->a(LaN/Y;F)F

    move-result v2

    invoke-virtual {v4}, LC/b;->d()F

    move-result v3

    invoke-virtual {v4}, LC/b;->e()F

    move-result v4

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    .line 580
    iget-object v1, p0, LaO/e;->h:Lo/T;

    invoke-direct {p0, v1, v0, p4}, LaO/e;->a(Lo/T;LC/b;Landroid/graphics/Point;)V
    :try_end_2c
    .catchall {:try_start_1 .. :try_end_2c} :catchall_2e

    .line 581
    monitor-exit p0

    return-void

    .line 570
    :catchall_2e
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(LaN/B;Landroid/graphics/Point;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 586
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-static {p1, v0}, LR/e;->a(LaN/B;Lo/T;)V

    .line 587
    iget-object v0, p0, LaO/e;->g:Lo/T;

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, LaO/e;->a(Lo/T;LC/b;Landroid/graphics/Point;)V
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    .line 588
    monitor-exit p0

    return-void

    .line 586
    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(LaN/Y;II)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 495
    monitor-enter p0

    :try_start_1
    iget v0, p0, LaO/e;->j:F

    invoke-static {p1, v0}, LaO/e;->a(LaN/Y;F)F

    move-result v0

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {v1}, LC/b;->a()F

    move-result v1

    sub-float/2addr v0, v1

    .line 500
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, LaO/e;->a(ZZZ)V

    .line 501
    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    int-to-float v2, p2

    int-to-float v3, p3

    const/16 v4, 0x14a

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/maps/driveabout/vector/bk;->a(FFFI)F
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_23

    .line 502
    monitor-exit p0

    return-void

    .line 495
    :catchall_23
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lo/D;)V
    .registers 3
    .parameter

    .prologue
    .line 641
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lo/D;)V

    .line 642
    return-void
.end method

.method public a(ZZZII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 146
    invoke-super/range {p0 .. p5}, LaN/u;->a(ZZZII)V

    .line 147
    return-void
.end method

.method public a([LaN/B;IIILaN/Y;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 468
    if-nez p1, :cond_3

    .line 472
    :goto_2
    return-void

    .line 471
    :cond_3
    aget-object v0, p1, p3

    invoke-virtual {p0, v0, p5}, LaO/e;->d(LaN/B;LaN/Y;)V

    goto :goto_2
.end method

.method public declared-synchronized b()I
    .registers 3

    .prologue
    .line 208
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LC/a;->a(LC/b;)V

    .line 209
    invoke-direct {p0}, LaO/e;->h()I
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_12

    move-result v0

    monitor-exit p0

    return v0

    .line 208
    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(LaN/H;)I
    .registers 4
    .parameter

    .prologue
    .line 220
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget v1, p0, LaO/e;->j:F

    invoke-static {p1, v1}, LaO/e;->a(LaN/H;F)LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LC/a;->a(LC/b;)V

    .line 221
    invoke-direct {p0}, LaO/e;->h()I
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_12

    move-result v0

    monitor-exit p0

    return v0

    .line 220
    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(F)V
    .registers 3
    .parameter

    .prologue
    .line 555
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bk;->c(F)V

    .line 556
    return-void
.end method

.method protected declared-synchronized b(LaN/B;LaN/Y;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 316
    monitor-enter p0

    const/4 v0, 0x0

    .line 317
    :try_start_2
    invoke-direct {p0, p1, p2, v0}, LaO/e;->a(LaN/B;LaN/Y;Z)V
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_7

    .line 318
    monitor-exit p0

    return-void

    .line 316
    :catchall_7
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()LaN/B;
    .registers 2

    .prologue
    .line 256
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-direct {p0, v0}, LaO/e;->a(LC/b;)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized c(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 181
    monitor-enter p0

    :try_start_1
    invoke-super {p0, p1, p2}, LaN/u;->c(II)V

    .line 182
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget v1, p0, LaO/e;->j:F

    invoke-virtual {v0, p1, p2, v1}, LC/a;->a(IIF)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_d

    .line 183
    monitor-exit p0

    return-void

    .line 181
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized c(LaN/B;LaN/Y;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 506
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v4

    .line 508
    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-static {p1, v0}, LR/e;->a(LaN/B;Lo/T;)V

    .line 509
    new-instance v0, LC/b;

    iget-object v1, p0, LaO/e;->g:Lo/T;

    iget v2, p0, LaO/e;->j:F

    invoke-static {p2, v2}, LaO/e;->a(LaN/Y;F)F

    move-result v2

    invoke-virtual {v4}, LC/b;->d()F

    move-result v3

    invoke-virtual {v4}, LC/b;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    .line 515
    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V
    :try_end_28
    .catchall {:try_start_1 .. :try_end_28} :catchall_2a

    .line 516
    monitor-exit p0

    return-void

    .line 506
    :catchall_2a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()LaN/Y;
    .registers 2

    .prologue
    .line 266
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-direct {p0, v0}, LaO/e;->b(LC/b;)LaN/Y;

    move-result-object v0

    return-object v0
.end method

.method public e()F
    .registers 2

    .prologue
    .line 271
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {v0}, LC/b;->e()F

    move-result v0

    return v0
.end method

.method public e(II)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 622
    .line 623
    const/4 v2, 0x0

    move-object v0, p0

    move v3, v1

    move v4, p1

    move v5, p2

    .line 625
    invoke-virtual/range {v0 .. v5}, LaO/e;->a(ZZZII)V

    .line 626
    return-void
.end method

.method public f()LaN/H;
    .registers 7

    .prologue
    .line 302
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v2

    .line 303
    iget-object v0, p0, LaO/e;->d:LaO/a;

    invoke-virtual {v0}, LaO/a;->b()LaN/H;

    move-result-object v5

    .line 304
    new-instance v0, LaN/H;

    invoke-direct {p0, v2}, LaO/e;->a(LC/b;)LaN/B;

    move-result-object v1

    invoke-direct {p0, v2}, LaO/e;->b(LC/b;)LaN/Y;

    move-result-object v2

    invoke-virtual {v5}, LaN/H;->c()I

    move-result v3

    invoke-virtual {v5}, LaN/H;->g()Z

    move-result v4

    invoke-virtual {v5}, LaN/H;->h()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LaN/H;-><init>(LaN/B;LaN/Y;IZZ)V

    return-object v0
.end method

.method public f(II)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 633
    const/4 v1, 0x0

    move-object v0, p0

    move v3, v2

    move v4, p1

    move v5, p2

    .line 636
    invoke-virtual/range {v0 .. v5}, LaO/e;->a(ZZZII)V

    .line 637
    return-void
.end method

.method public i()V
    .registers 2

    .prologue
    .line 486
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->h()V

    .line 487
    return-void
.end method

.method public j()Z
    .registers 4

    .prologue
    .line 536
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {v0}, LC/b;->a()F

    move-result v0

    .line 537
    sget-object v1, LaO/j;->b:LaO/j;

    invoke-virtual {p0, v0, v1}, LaO/e;->a(FLaO/j;)LaN/Y;

    move-result-object v1

    .line 538
    invoke-virtual {v1}, LaN/Y;->c()LaN/Y;

    move-result-object v2

    if-eqz v2, :cond_2c

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    invoke-virtual {p0}, LaO/e;->c()LaN/B;

    move-result-object v2

    invoke-virtual {p0, v2}, LaO/e;->a(LaN/B;)I

    move-result v2

    if-ge v1, v2, :cond_2c

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bk;->d()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2e

    :cond_2c
    const/4 v0, 0x1

    :goto_2d
    return v0

    :cond_2e
    const/4 v0, 0x0

    goto :goto_2d
.end method

.method public k()Z
    .registers 3

    .prologue
    .line 545
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {v0}, LC/b;->a()F

    move-result v0

    .line 546
    sget-object v1, LaO/j;->c:LaO/j;

    invoke-virtual {p0, v0, v1}, LaO/e;->a(FLaO/j;)LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->d()LaN/Y;

    move-result-object v1

    if-eqz v1, :cond_20

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->e()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_22

    :cond_20
    const/4 v0, 0x1

    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public l()Z
    .registers 2

    .prologue
    .line 564
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->i()Z

    move-result v0

    return v0
.end method

.method public m()V
    .registers 1

    .prologue
    .line 156
    invoke-super {p0}, LaN/u;->m()V

    .line 157
    return-void
.end method
