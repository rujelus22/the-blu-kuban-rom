.class Lao/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lao/n;


# instance fields
.field private final a:Ljava/util/Map;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 740
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 745
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lao/o;->a:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lao/m;)V
    .registers 2
    .parameter

    .prologue
    .line 740
    invoke-direct {p0}, Lao/o;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILao/l;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 787
    iget-object v1, p0, Lao/o;->a:Ljava/util/Map;

    monitor-enter v1

    .line 788
    :try_start_3
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lao/o;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 789
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_25

    .line 791
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_13
    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/y;

    .line 792
    if-eqz v0, :cond_13

    .line 793
    invoke-interface {v0, p1, p2}, Lao/y;->a(ILao/h;)V

    goto :goto_13

    .line 789
    :catchall_25
    move-exception v0

    :try_start_26
    monitor-exit v1
    :try_end_27
    .catchall {:try_start_26 .. :try_end_27} :catchall_25

    throw v0

    .line 796
    :cond_28
    return-void
.end method

.method public a(Lao/y;)V
    .registers 4
    .parameter

    .prologue
    .line 750
    iget-object v0, p0, Lao/o;->a:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 751
    return-void
.end method

.method public a(Lau/B;Lao/l;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 767
    iget-object v1, p0, Lao/o;->a:Ljava/util/Map;

    monitor-enter v1

    .line 768
    :try_start_3
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lao/o;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 769
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_25

    .line 771
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_13
    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/y;

    .line 772
    if-eqz v0, :cond_13

    .line 773
    invoke-interface {v0, p1, p2}, Lao/y;->a(Lau/B;Lao/h;)V

    goto :goto_13

    .line 769
    :catchall_25
    move-exception v0

    :try_start_26
    monitor-exit v1
    :try_end_27
    .catchall {:try_start_26 .. :try_end_27} :catchall_25

    throw v0

    .line 776
    :cond_28
    return-void
.end method

.method public b(Lao/y;)V
    .registers 3
    .parameter

    .prologue
    .line 755
    iget-object v0, p0, Lao/o;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 756
    return-void
.end method
