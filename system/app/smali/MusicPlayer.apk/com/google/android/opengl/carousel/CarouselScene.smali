.class Lcom/google/android/opengl/carousel/CarouselScene;
.super Ljava/lang/Object;
.source "CarouselScene.java"


# static fields
.field private static final SELECTED_SCALE_FACTOR:Lcom/google/android/opengl/common/Float3;

.field private static sProjection:[F

.field private static sRotationTemp:[F


# instance fields
.field mAnimatedSelection:I

.field private mAnimating:Z

.field private mAutoscrollDuration:D

.field private mAutoscrollInterpolationMode:I

.field private mAutoscrollStartAngle:F

.field private mAutoscrollStartTime:D

.field private mAutoscrollStopAngle:F

.field public mBackground:Lcom/google/android/opengl/carousel/Background;

.field private mBias:F

.field public mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

.field mCards:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/opengl/carousel/Card;",
            ">;"
        }
    .end annotation
.end field

.field mCarouselCylinder:Lcom/google/android/opengl/carousel/Cylinder;

.field mCarouselPlane:Lcom/google/android/opengl/carousel/Plane;

.field public mCarouselRotationAngle:F

.field mDetailFadeRate:F

.field mEnableSelection:Z

.field mEndAngle:F

.field mFade0:F

.field mFade1:F

.field private mFocusedItem:I

.field private mHitAngle:F

.field private mHoverCard:I

.field private mHoverDetail:I

.field mInScaleAnimation:Z

.field private mIsAutoScrolling:Z

.field private mIsDragging:Z

.field private mLastAngle:F

.field private mLastPosition:Landroid/graphics/PointF;

.field private mLastStopTime:J

.field private mLastTime:J

.field private mMass:F

.field private mMinVelocity:F

.field mMix0:F

.field mMix1:F

.field mMixAndFadeEnabled:Z

.field mOnLongPress:Z

.field private mOverscroll:Z

.field private mOverscrollBias:F

.field private mReleaseTime:J

.field public mRenderMode:I

.field public mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

.field public mRotationAngle:F

.field mSelectedDetail:I

.field private mSelectionRadius:F

.field private mSelectionVelocityThreshold:F

.field public mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

.field private mStopTimeThreshold:J

.field mTexCoord:Ljava/nio/FloatBuffer;

.field private mTexCoordData:[F

.field private mTiltAngle:F

.field private mTouchBias:F

.field private mTouchPosition:Landroid/graphics/PointF;

.field private mTouchTime:J

.field private mVelocity:F

.field private mVelocityHistory:[F

.field private mVelocityHistoryCount:I

.field private mVelocityThreshold:F

.field private mVertices:Ljava/nio/FloatBuffer;

.field mVerticesData:[F

.field mWedgeAngle:F


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const v1, 0x3e4ccccd

    .line 50
    new-instance v0, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v0, v1, v1, v1}, Lcom/google/android/opengl/common/Float3;-><init>(FFF)V

    sput-object v0, Lcom/google/android/opengl/carousel/CarouselScene;->SELECTED_SCALE_FACTOR:Lcom/google/android/opengl/common/Float3;

    .line 55
    const/16 v0, 0x10

    new-array v0, v0, [F

    sput-object v0, Lcom/google/android/opengl/carousel/CarouselScene;->sProjection:[F

    .line 156
    const/16 v0, 0x20

    new-array v0, v0, [F

    sput-object v0, Lcom/google/android/opengl/carousel/CarouselScene;->sRotationTemp:[F

    return-void
.end method

.method public constructor <init>(Lcom/google/android/opengl/carousel/CarouselRenderer;Lcom/google/android/opengl/carousel/CarouselSetting;)V
    .registers 13
    .parameter "renderer"
    .parameter "setting"

    .prologue
    const-wide/16 v8, 0x0

    const-wide/16 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    .line 72
    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderMode:I

    .line 84
    const/16 v1, 0xc

    new-array v1, v1, [F

    fill-array-data v1, :array_120

    iput-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVerticesData:[F

    .line 88
    const/16 v1, 0x8

    new-array v1, v1, [F

    fill-array-data v1, :array_13c

    iput-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTexCoordData:[F

    .line 91
    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsDragging:Z

    .line 92
    iput-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    .line 93
    iput-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastStopTime:J

    .line 95
    const-wide/16 v1, 0x4b

    iput-wide v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mStopTimeThreshold:J

    .line 96
    iput-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchTime:J

    .line 97
    iput-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mReleaseTime:J

    .line 99
    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mInScaleAnimation:Z

    .line 100
    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchBias:F

    .line 101
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    iput-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchPosition:Landroid/graphics/PointF;

    .line 102
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    iput-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    .line 104
    const/high16 v1, 0x4248

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSelectionRadius:F

    .line 106
    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    .line 108
    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOnLongPress:Z

    .line 109
    iput v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimatedSelection:I

    .line 110
    iput v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSelectedDetail:I

    .line 113
    iput v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mFocusedItem:I

    .line 115
    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    .line 116
    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 118
    const/4 v1, 0x5

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistory:[F

    .line 120
    const v1, 0x3ae4c389

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityThreshold:F

    .line 122
    const v1, 0x3db2b8c3

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSelectionVelocityThreshold:F

    .line 127
    const/high16 v1, 0x40a0

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMass:F

    .line 131
    const/high16 v1, 0x3f80

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mDetailFadeRate:F

    .line 133
    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRotationAngle:F

    .line 135
    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselRotationAngle:F

    .line 136
    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    .line 137
    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsAutoScrolling:Z

    .line 146
    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMixAndFadeEnabled:Z

    .line 149
    new-instance v1, Lcom/google/android/opengl/carousel/Plane;

    invoke-direct {v1}, Lcom/google/android/opengl/carousel/Plane;-><init>()V

    iput-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselPlane:Lcom/google/android/opengl/carousel/Plane;

    .line 151
    new-instance v1, Lcom/google/android/opengl/carousel/Cylinder;

    const/high16 v2, 0x41a0

    invoke-direct {v1, v2}, Lcom/google/android/opengl/carousel/Cylinder;-><init>(F)V

    iput-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselCylinder:Lcom/google/android/opengl/carousel/Cylinder;

    .line 152
    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastAngle:F

    .line 158
    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    .line 162
    iput v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverCard:I

    .line 163
    iput v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverDetail:I

    .line 1553
    iput-wide v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartTime:D

    .line 1554
    iput-wide v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollDuration:D

    .line 1555
    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollInterpolationMode:I

    .line 1557
    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStopAngle:F

    .line 1558
    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartAngle:F

    .line 170
    iput-object p1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    .line 171
    iput-object p2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    .line 173
    new-instance v1, Lcom/google/android/opengl/carousel/Background;

    invoke-direct {v1, p0}, Lcom/google/android/opengl/carousel/Background;-><init>(Lcom/google/android/opengl/carousel/CarouselScene;)V

    iput-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBackground:Lcom/google/android/opengl/carousel/Background;

    .line 175
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mDefaultCardMatrix:[F

    invoke-static {v1, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 177
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVerticesData:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVertices:Ljava/nio/FloatBuffer;

    .line 179
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVertices:Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVerticesData:[F

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 181
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTexCoordData:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTexCoord:Ljava/nio/FloatBuffer;

    .line 183
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTexCoord:Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTexCoordData:[F

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 187
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    invoke-virtual {v1}, Lcom/google/android/opengl/carousel/CarouselRenderer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 188
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x43fa

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMinVelocity:F

    .line 190
    const-string v1, "CarouselScene"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "System Velocity limit, min: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMinVelocity:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    return-void

    .line 84
    nop

    :array_120
    .array-data 0x4
        0x0t 0x0t 0x80t 0xbft
        0x0t 0x0t 0x80t 0xbft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0xbft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0xbft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 88
    :array_13c
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private carouselRotationAngleToRadians(F)F
    .registers 4
    .parameter "carouselRotationAngle"

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mFillDirection:I

    neg-int v0, v0

    int-to-float v0, v0

    invoke-direct {p0, p1}, Lcom/google/android/opengl/carousel/CarouselScene;->wedgeAngle(F)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private computeAverageVelocityFromHistory()F
    .registers 6

    .prologue
    .line 997
    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    if-lez v3, :cond_1b

    .line 998
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 999
    .local v0, count:I
    const/4 v2, 0x0

    .line 1000
    .local v2, vsum:F
    const/4 v1, 0x0

    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_17

    .line 1001
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistory:[F

    aget v3, v3, v1

    add-float/2addr v2, v3

    .line 1000
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 1003
    :cond_17
    int-to-float v3, v0

    div-float v3, v2, v3

    .line 1005
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v2           #vsum:F
    :goto_1a
    return v3

    :cond_1b
    const/4 v3, 0x0

    goto :goto_1a
.end method

.method private cullCards()I
    .registers 15

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 387
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v10, v10, Lcom/google/android/opengl/carousel/CarouselSetting;->mPrefetchCardCount:I

    div-int/lit8 v5, v10, 0x2

    .line 388
    .local v5, prefetchCardCountPerSide:I
    const/4 v4, 0x0

    .line 389
    .local v4, portraitCullOffset:F
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-boolean v10, v10, Lcom/google/android/opengl/carousel/CarouselRenderer;->mIsPortrait:Z

    if-eqz v10, :cond_13

    .line 390
    const v10, 0x3e99999a

    add-float/2addr v4, v10

    .line 394
    :cond_13
    const v10, -0x4019999a

    sub-float/2addr v10, v4

    invoke-direct {p0, v10}, Lcom/google/android/opengl/carousel/CarouselScene;->slotAngle(F)F

    move-result v8

    .line 395
    .local v8, thetaFirst:F
    neg-int v10, v5

    int-to-float v10, v10

    sub-float/2addr v10, v4

    invoke-direct {p0, v10}, Lcom/google/android/opengl/carousel/CarouselScene;->slotAngle(F)F

    move-result v6

    .line 396
    .local v6, textureFirst:F
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v10, v10, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleSlotCount:I

    int-to-float v10, v10

    const/high16 v11, 0x3f80

    add-float/2addr v10, v11

    invoke-direct {p0, v10}, Lcom/google/android/opengl/carousel/CarouselScene;->slotAngle(F)F

    move-result v9

    .line 397
    .local v9, thetaLast:F
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v10, v10, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleSlotCount:I

    add-int/lit8 v10, v10, -0x1

    add-int/2addr v10, v5

    int-to-float v10, v10

    invoke-direct {p0, v10}, Lcom/google/android/opengl/carousel/CarouselScene;->slotAngle(F)F

    move-result v7

    .line 399
    .local v7, textureLast:F
    const/4 v1, 0x0

    .line 401
    .local v1, count:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_3c
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v2, v10, :cond_8b

    .line 403
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    .line 404
    .local v0, card:Lcom/google/android/opengl/carousel/Card;
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v10, v10, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleSlotCount:I

    if-lez v10, :cond_86

    .line 407
    int-to-float v10, v2

    invoke-virtual {p0, v10}, Lcom/google/android/opengl/carousel/CarouselScene;->cardAngle(F)F

    move-result v3

    .line 408
    .local v3, p:F
    cmpl-float v10, v3, v8

    if-ltz v10, :cond_5f

    cmpg-float v10, v3, v9

    if-ltz v10, :cond_67

    :cond_5f
    cmpg-float v10, v3, v8

    if-gtz v10, :cond_80

    cmpl-float v10, v3, v9

    if-lez v10, :cond_80

    .line 409
    :cond_67
    iput-boolean v12, v0, Lcom/google/android/opengl/carousel/Card;->mVisible:Z

    .line 410
    add-int/lit8 v1, v1, 0x1

    .line 414
    :goto_6b
    cmpl-float v10, v3, v6

    if-ltz v10, :cond_73

    cmpg-float v10, v3, v7

    if-ltz v10, :cond_7b

    :cond_73
    cmpg-float v10, v3, v6

    if-gtz v10, :cond_83

    cmpl-float v10, v3, v7

    if-lez v10, :cond_83

    .line 415
    :cond_7b
    iput-boolean v12, v0, Lcom/google/android/opengl/carousel/Card;->mPrefetchTexture:Z

    .line 401
    .end local v3           #p:F
    :goto_7d
    add-int/lit8 v2, v2, 0x1

    goto :goto_3c

    .line 412
    .restart local v3       #p:F
    :cond_80
    iput-boolean v13, v0, Lcom/google/android/opengl/carousel/Card;->mVisible:Z

    goto :goto_6b

    .line 417
    :cond_83
    iput-boolean v13, v0, Lcom/google/android/opengl/carousel/Card;->mPrefetchTexture:Z

    goto :goto_7d

    .line 421
    .end local v3           #p:F
    :cond_86
    iput-boolean v12, v0, Lcom/google/android/opengl/carousel/Card;->mVisible:Z

    .line 422
    add-int/lit8 v1, v1, 0x1

    goto :goto_7d

    .line 426
    .end local v0           #card:Lcom/google/android/opengl/carousel/Card;
    :cond_8b
    return v1
.end method

.method private deltaTimeInSeconds(J)F
    .registers 7
    .parameter

    .prologue
    .line 1066
    iget-wide v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_11

    .line 1067
    iget-wide v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    const/high16 v1, 0x447a

    div-float/2addr v0, v1

    .line 1069
    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method private describeShuffle(Ljava/lang/String;[I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1479
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1480
    const/16 v0, 0x5b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1481
    const/4 v0, 0x0

    :goto_b
    array-length v2, p2

    if-ge v0, v2, :cond_1d

    .line 1482
    if-lez v0, :cond_15

    .line 1483
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1485
    :cond_15
    aget v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1481
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 1487
    :cond_1d
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1488
    const-string v0, "CarouselScene"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1490
    return-void
.end method

.method private doAutoscroll(F)Z
    .registers 12
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const-wide/16 v6, 0x0

    const/high16 v1, 0x3f80

    .line 1616
    iget-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollDuration:D

    cmpl-double v0, v4, v6

    if-nez v0, :cond_e

    move v0, v2

    .line 1646
    :goto_d
    return v0

    .line 1620
    :cond_e
    iget-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartTime:D

    cmpl-double v0, v4, v6

    if-nez v0, :cond_17

    .line 1621
    float-to-double v4, p1

    iput-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartTime:D

    .line 1624
    :cond_17
    iget-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartTime:D

    iget-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollDuration:D

    add-double/2addr v4, v6

    .line 1626
    float-to-double v6, p1

    iget-wide v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartTime:D

    sub-double/2addr v6, v8

    iget-wide v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollDuration:D

    div-double/2addr v6, v8

    double-to-float v0, v6

    .line 1627
    cmpl-float v6, v0, v1

    if-lez v6, :cond_29

    move v0, v1

    .line 1632
    :cond_29
    iget v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollInterpolationMode:I

    if-ne v6, v3, :cond_4f

    .line 1633
    sub-float v6, v1, v0

    sub-float v0, v1, v0

    mul-float/2addr v0, v6

    sub-float v0, v1, v0

    .line 1639
    :cond_34
    :goto_34
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStopAngle:F

    mul-float/2addr v1, v0

    float-to-double v6, v1

    const-wide/high16 v8, 0x3ff0

    float-to-double v0, v0

    sub-double v0, v8, v0

    iget v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartAngle:F

    float-to-double v8, v8

    mul-double/2addr v0, v8

    add-double/2addr v0, v6

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    .line 1641
    float-to-double v0, p1

    cmpl-double v0, v0, v4

    if-lez v0, :cond_5f

    .line 1642
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->stopAutoscroll()V

    move v0, v2

    .line 1643
    goto :goto_d

    .line 1635
    :cond_4f
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollInterpolationMode:I

    const/4 v6, 0x2

    if-ne v1, v6, :cond_34

    .line 1636
    mul-float v1, v0, v0

    const/high16 v6, 0x4040

    const/high16 v7, 0x4000

    mul-float/2addr v0, v7

    sub-float v0, v6, v0

    mul-float/2addr v0, v1

    goto :goto_34

    :cond_5f
    move v0, v3

    .line 1646
    goto :goto_d
.end method

.method private doPhysics(F)Z
    .registers 10
    .parameter

    .prologue
    .line 1140
    const v0, 0x3c23d70a

    .line 1141
    cmpl-float v1, p1, v0

    if-lez v1, :cond_64

    div-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1143
    :goto_f
    const/4 v1, 0x5

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1144
    int-to-float v0, v2

    div-float v3, p1, v0

    .line 1146
    iget v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    .line 1147
    const/4 v0, 0x0

    move v1, v0

    :goto_1b
    if-ge v1, v2, :cond_66

    .line 1149
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mFrictionCoeff:F

    neg-float v0, v0

    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    mul-float/2addr v5, v0

    .line 1152
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    iget v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    add-float/2addr v0, v6

    .line 1153
    const v6, 0x40c90fdb

    iget-object v7, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v7, v7, Lcom/google/android/opengl/carousel/CarouselSetting;->mSlotCount:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    .line 1154
    div-float/2addr v0, v6

    .line 1157
    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-float v6, v6

    sub-float/2addr v0, v6

    .line 1159
    const/high16 v6, 0x3f00

    cmpl-float v6, v0, v6

    if-lez v6, :cond_48

    .line 1160
    const/high16 v6, 0x3f80

    sub-float v0, v6, v0

    neg-float v0, v0

    .line 1164
    :cond_48
    const/high16 v6, -0x8000

    mul-float/2addr v0, v6

    .line 1167
    iget v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMass:F

    iget v7, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    mul-float/2addr v6, v7

    add-float/2addr v0, v5

    mul-float/2addr v0, v3

    add-float/2addr v0, v6

    .line 1168
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMass:F

    div-float/2addr v0, v5

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 1170
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    mul-float/2addr v5, v3

    add-float/2addr v0, v5

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    .line 1147
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1b

    .line 1141
    :cond_64
    const/4 v0, 0x1

    goto :goto_f

    .line 1173
    :cond_66
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    .line 1185
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityThreshold:F

    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->isInMotion(F)Z

    move-result v0

    return v0
.end method

.method private doSelection(FF)I
    .registers 6
    .parameter "x"
    .parameter "y"

    .prologue
    .line 712
    new-instance v0, Lcom/google/android/opengl/carousel/Ray;

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    invoke-direct {v0, v2}, Lcom/google/android/opengl/carousel/Ray;-><init>(Lcom/google/android/opengl/carousel/CarouselRenderer;)V

    .line 713
    .local v0, ray:Lcom/google/android/opengl/carousel/Ray;
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v2, v2, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCamera:Lcom/google/android/opengl/carousel/GLCamera;

    invoke-virtual {v0, v2, p1, p2}, Lcom/google/android/opengl/carousel/Ray;->makeRayForPixelAt(Lcom/google/android/opengl/carousel/GLCamera;FF)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 714
    const v2, 0x7cf0bdc2

    iput v2, v0, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    .line 715
    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->intersectGeometry(Lcom/google/android/opengl/carousel/Ray;)I

    move-result v1

    .line 719
    :goto_1a
    return v1

    :cond_1b
    const/4 v1, -0x1

    goto :goto_1a
.end method

.method private dragFunction(FF)F
    .registers 10
    .parameter
    .parameter

    .prologue
    const-wide v5, 0x401921fb54442d18L

    .line 1074
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->hitAngle(FF)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 1075
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHitAngle:F

    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastAngle:F

    sub-float/2addr v0, v1

    .line 1077
    float-to-double v1, v0

    const-wide v3, -0x3ff6de04abbbd2e8L

    cmpg-double v1, v1, v3

    if-gez v1, :cond_22

    .line 1078
    float-to-double v0, v0

    add-double/2addr v0, v5

    double-to-float v0, v0

    .line 1082
    :cond_1d
    :goto_1d
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHitAngle:F

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastAngle:F

    .line 1098
    :goto_21
    return v0

    .line 1079
    :cond_22
    float-to-double v1, v0

    const-wide v3, 0x400921fb54442d18L

    cmpl-double v1, v1, v3

    if-lez v1, :cond_1d

    .line 1080
    float-to-double v0, v0

    sub-double/2addr v0, v5

    double-to-float v0, v0

    goto :goto_1d

    .line 1085
    :cond_30
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mDragFactor:F

    .line 1086
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mDragModel:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_45

    .line 1087
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->projectedDelta(FF)F

    move-result v1

    mul-float/2addr v0, v1

    .line 1096
    :goto_40
    const v1, 0x40490fdb

    mul-float/2addr v0, v1

    goto :goto_21

    .line 1089
    :cond_45
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-boolean v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mIsPortrait:Z

    if-eqz v1, :cond_5b

    .line 1090
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, p2

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselRenderer;->mHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x4000

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    goto :goto_40

    .line 1092
    :cond_5b
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float v1, p1, v1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselRenderer;->mWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    goto :goto_40
.end method

.method private drawCards(J)Z
    .registers 16
    .parameter "currentTime"

    .prologue
    .line 597
    const-wide v0, 0x401921fb54442d18L

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mSlotCount:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mWedgeAngle:F

    .line 598
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleSlotCount:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mWedgeAngle:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEndAngle:F

    .line 599
    const/4 v12, 0x0

    .line 601
    .local v12, stillAnimating:Z
    const/4 v0, 0x1

    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTexCoord:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 603
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->setCardPosition()V

    .line 604
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCardRenderer:Lcom/google/android/opengl/carousel/ICardRenderer;

    if-eqz v0, :cond_39

    .line 605
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCardRenderer:Lcom/google/android/opengl/carousel/ICardRenderer;

    invoke-interface {v0}, Lcom/google/android/opengl/carousel/ICardRenderer;->beforeDrawAllCards()V

    .line 608
    :cond_39
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    .local v10, i:I
    :goto_41
    if-ltz v10, :cond_c5

    .line 609
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/opengl/carousel/Card;

    .line 610
    .local v7, card:Lcom/google/android/opengl/carousel/Card;
    iget-boolean v0, v7, Lcom/google/android/opengl/carousel/Card;->mVisible:Z

    if-nez v0, :cond_52

    .line 608
    :cond_4f
    :goto_4f
    add-int/lit8 v10, v10, -0x1

    goto :goto_41

    .line 613
    :cond_52
    iget-object v0, v7, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/RequestableTexture;->getChangeTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->getAnimatedAlpha(JJ)F

    move-result v6

    .line 615
    .local v6, animatedAlpha:F
    const/high16 v0, 0x3f80

    cmpg-float v0, v6, v0

    if-gez v0, :cond_66

    .line 616
    const/4 v12, 0x1

    .line 621
    :cond_66
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mRezInCardCount:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_b8

    .line 622
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEndAngle:F

    int-to-float v1, v10

    invoke-virtual {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->cardAngle(F)F

    move-result v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mWedgeAngle:F

    div-float v11, v0, v1

    .line 623
    .local v11, positionAlpha:F
    const/high16 v0, 0x3f80

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mRezInCardCount:F

    div-float v1, v11, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v11

    .line 629
    :goto_87
    const/high16 v0, 0x3f80

    mul-float v1, v6, v11

    invoke-virtual {p0, v10}, Lcom/google/android/opengl/carousel/CarouselScene;->getFadeOutLeftAlpha(I)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v8

    .line 632
    .local v8, fadeAmount:F
    iget-object v0, v7, Lcom/google/android/opengl/carousel/Card;->mMMatrix:[F

    iget v1, v7, Lcom/google/android/opengl/carousel/Card;->mId:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/opengl/carousel/CarouselScene;->getMatrixForCard([FIZZ)Z

    move-result v0

    or-int/2addr v12, v0

    .line 633
    invoke-virtual {v7, v8}, Lcom/google/android/opengl/carousel/Card;->draw(F)V

    .line 636
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimatedSelection:I

    if-ne v10, v0, :cond_bb

    iget-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    if-eqz v0, :cond_bb

    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mScaleSelectedCard:Z

    if-nez v0, :cond_bb

    .line 637
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getAnimatedGlowingForSelected()F

    move-result v9

    .line 638
    .local v9, glowAlpha:F
    invoke-virtual {v7, v9}, Lcom/google/android/opengl/carousel/Card;->drawGlowing(F)V

    goto :goto_4f

    .line 625
    .end local v8           #fadeAmount:F
    .end local v9           #glowAlpha:F
    .end local v11           #positionAlpha:F
    :cond_b8
    const/high16 v11, 0x3f80

    .restart local v11       #positionAlpha:F
    goto :goto_87

    .line 639
    .restart local v8       #fadeAmount:F
    :cond_bb
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mFocusedItem:I

    if-ne v10, v0, :cond_4f

    .line 640
    const/high16 v0, 0x3f80

    invoke-virtual {v7, v0}, Lcom/google/android/opengl/carousel/Card;->drawGlowing(F)V

    goto :goto_4f

    .line 644
    .end local v6           #animatedAlpha:F
    .end local v7           #card:Lcom/google/android/opengl/carousel/Card;
    .end local v8           #fadeAmount:F
    .end local v11           #positionAlpha:F
    :cond_c5
    const-string v0, "drawCards"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 645
    return v12
.end method

.method private drawDetails(J)Z
    .registers 14
    .parameter "currentTime"

    .prologue
    .line 655
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mShowDetails:Z

    if-nez v0, :cond_8

    .line 656
    const/4 v10, 0x0

    .line 681
    :goto_7
    return v10

    .line 658
    :cond_8
    const/4 v0, 0x1

    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTexCoord:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 660
    const/4 v10, 0x0

    .line 663
    .local v10, stillAnimating:Z
    sget-object v0, Lcom/google/android/opengl/carousel/CarouselScene;->sProjection:[F

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mWidth:I

    int-to-float v3, v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselRenderer;->mHeight:I

    int-to-float v5, v5

    const/4 v6, 0x0

    const/high16 v7, 0x41a0

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->orthoM([FIFFFFFF)V

    .line 666
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {v0, v1}, Lcom/google/android/opengl/carousel/CarouselRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 667
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 668
    const/16 v0, 0xde1

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselTexture;->mDetailLoadingId:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 670
    const/4 v8, 0x0

    .line 671
    .local v8, card:Lcom/google/android/opengl/carousel/Card;
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v9, v0, -0x1

    .local v9, i:I
    :goto_4c
    if-ltz v9, :cond_6d

    .line 672
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    iget-boolean v0, v0, Lcom/google/android/opengl/carousel/Card;->mVisible:Z

    if-nez v0, :cond_5d

    .line 671
    :goto_5a
    add-int/lit8 v9, v9, -0x1

    goto :goto_4c

    .line 675
    :cond_5d
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8           #card:Lcom/google/android/opengl/carousel/Card;
    check-cast v8, Lcom/google/android/opengl/carousel/Card;

    .line 676
    .restart local v8       #card:Lcom/google/android/opengl/carousel/Card;
    sget-object v0, Lcom/google/android/opengl/carousel/CarouselScene;->sProjection:[F

    invoke-virtual {v8, p1, p2, v0}, Lcom/google/android/opengl/carousel/Card;->drawDetails(J[F)Z

    move-result v0

    or-int/2addr v10, v0

    goto :goto_5a

    .line 679
    :cond_6d
    const-string v0, "drawDetails"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    goto :goto_7
.end method

.method private getAnimatedGlowingForSelected()F
    .registers 5

    .prologue
    .line 1044
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchTime:J

    sub-long/2addr v0, v2

    .line 1046
    const-wide/16 v2, 0xc8

    cmp-long v2, v0, v2

    if-gez v2, :cond_12

    .line 1047
    long-to-float v0, v0

    const/high16 v1, 0x4348

    div-float/2addr v0, v1

    .line 1051
    :goto_11
    return v0

    .line 1049
    :cond_12
    const/high16 v0, 0x3f80

    goto :goto_11
.end method

.method private getAnimatedScaleForSelected(Lcom/google/android/opengl/common/Float3;)Z
    .registers 12
    .parameter

    .prologue
    const/high16 v9, 0x4348

    const/high16 v8, 0x3f80

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1011
    invoke-static {}, Lcom/google/android/opengl/common/Float3;->getUnit()Lcom/google/android/opengl/common/Float3;

    move-result-object v3

    .line 1015
    iget-boolean v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsDragging:Z

    if-eqz v4, :cond_45

    .line 1017
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchTime:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x7d

    sub-long/2addr v4, v6

    .line 1018
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-lez v6, :cond_23

    iget-boolean v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    if-nez v6, :cond_27

    :cond_23
    iget-boolean v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mInScaleAnimation:Z

    if-eqz v6, :cond_2f

    .line 1019
    :cond_27
    long-to-float v6, v4

    div-float/2addr v6, v9

    .line 1020
    invoke-static {v6, v2, v8}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v2

    .line 1021
    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mInScaleAnimation:Z

    .line 1023
    :cond_2f
    const-wide/16 v6, 0xc8

    cmp-long v4, v4, v6

    if-gez v4, :cond_43

    :goto_35
    move v1, v2

    .line 1036
    :goto_36
    sget-object v2, Lcom/google/android/opengl/carousel/CarouselScene;->SELECTED_SCALE_FACTOR:Lcom/google/android/opengl/common/Float3;

    invoke-static {v2, v1}, Lcom/google/android/opengl/common/Float3;->mupltiple(Lcom/google/android/opengl/common/Float3;F)Lcom/google/android/opengl/common/Float3;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/google/android/opengl/common/Float3;->add(Lcom/google/android/opengl/common/Float3;)V

    .line 1037
    invoke-virtual {p1, v3}, Lcom/google/android/opengl/common/Float3;->set(Lcom/google/android/opengl/common/Float3;)V

    .line 1039
    return v0

    :cond_43
    move v0, v1

    .line 1023
    goto :goto_35

    .line 1024
    :cond_45
    iget-boolean v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mInScaleAnimation:Z

    if-eqz v4, :cond_61

    .line 1026
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mReleaseTime:J

    sub-long/2addr v4, v6

    .line 1027
    const-wide/16 v6, 0xc8

    cmp-long v6, v4, v6

    if-gez v6, :cond_5f

    .line 1028
    long-to-float v1, v4

    div-float/2addr v1, v9

    sub-float v1, v8, v1

    .line 1029
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto :goto_36

    .line 1033
    :cond_5f
    iput-boolean v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mInScaleAnimation:Z

    :cond_61
    move v0, v1

    move v1, v2

    goto :goto_36
.end method

.method private getCardTiltAngle(I)F
    .registers 9
    .parameter "i"

    .prologue
    const/4 v6, 0x0

    .line 1334
    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    .line 1335
    .local v1, rowCount:I
    div-int/2addr p1, v1

    .line 1336
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v5

    add-int/2addr v5, v1

    add-int/lit8 v5, v5, -0x1

    div-int v4, v5, v1

    .line 1337
    .local v4, totalSlots:I
    const/high16 v3, 0x40a0

    .line 1338
    .local v3, tiltSlotNumber:F
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    div-float v0, v5, v3

    .line 1339
    .local v0, deltaTilt:F
    const/4 v2, 0x0

    .line 1340
    .local v2, tiltAngle:F
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_27

    int-to-float v5, p1

    cmpg-float v5, v5, v3

    if-gez v5, :cond_27

    .line 1342
    int-to-float v5, p1

    sub-float v5, v3, v5

    mul-float v2, v0, v5

    .line 1346
    :cond_26
    :goto_26
    return v2

    .line 1343
    :cond_27
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_26

    int-to-float v5, p1

    int-to-float v6, v4

    sub-float/2addr v6, v3

    cmpl-float v5, v5, v6

    if-lez v5, :cond_26

    .line 1344
    sub-int v5, p1, v4

    int-to-float v5, v5

    add-float/2addr v5, v3

    const/high16 v6, 0x3f80

    add-float/2addr v5, v6

    mul-float v2, v0, v5

    goto :goto_26
.end method

.method private getSwayAngleForVelocity(FZ)F
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1660
    const/4 v0, 0x0

    .line 1662
    if-eqz p2, :cond_14

    .line 1664
    const v0, 0x40060a92

    .line 1665
    neg-float v1, p1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mSwaySensitivity:F

    mul-float/2addr v1, v2

    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->logistic(F)F

    move-result v1

    const/high16 v2, 0x3f00

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    .line 1668
    :cond_14
    return v0
.end method

.method private getVerticalOffsetForCard(I)F
    .registers 10
    .parameter "i"

    .prologue
    const/4 v7, 0x1

    .line 1354
    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    .line 1355
    .local v1, rowCount:I
    if-ne v1, v7, :cond_9

    .line 1357
    const/4 v5, 0x0

    .line 1371
    :goto_8
    return v5

    .line 1359
    :cond_9
    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowSpacing:F

    .line 1360
    .local v3, rowSpacing:F
    sget-object v5, Lcom/google/android/opengl/carousel/Card;->mVerticesData:[F

    const/16 v6, 0xa

    aget v5, v5, v6

    sget-object v6, Lcom/google/android/opengl/carousel/Card;->mVerticesData:[F

    aget v6, v6, v7

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v6, v6, Lcom/google/android/opengl/carousel/CarouselSetting;->mDefaultCardMatrix:[F

    const/4 v7, 0x5

    aget v6, v6, v7

    mul-float v0, v5, v6

    .line 1362
    .local v0, cardHeight:F
    int-to-float v5, v1

    add-float v6, v0, v3

    mul-float/2addr v5, v6

    sub-float v4, v5, v3

    .line 1363
    .local v4, totalHeight:F
    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v5, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mFirstCardTop:Z

    if-eqz v5, :cond_3f

    .line 1364
    rem-int v5, p1, v1

    sub-int v5, v1, v5

    add-int/lit8 p1, v5, -0x1

    .line 1369
    :goto_33
    int-to-float v5, p1

    add-float v6, v0, v3

    mul-float v2, v5, v6

    .line 1371
    .local v2, rowOffset:F
    sub-float v5, v0, v4

    const/high16 v6, 0x4000

    div-float/2addr v5, v6

    add-float/2addr v5, v2

    goto :goto_8

    .line 1366
    .end local v2           #rowOffset:F
    :cond_3f
    rem-int/2addr p1, v1

    goto :goto_33
.end method

.method private haveEGLContext()Z
    .registers 3

    .prologue
    .line 501
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentContext()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private hitAngle(FF)Z
    .registers 11
    .parameter "x"
    .parameter "y"

    .prologue
    const/4 v3, 0x1

    .line 1397
    new-instance v2, Lcom/google/android/opengl/carousel/Ray;

    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    invoke-direct {v2, v4}, Lcom/google/android/opengl/carousel/Ray;-><init>(Lcom/google/android/opengl/carousel/CarouselRenderer;)V

    .line 1398
    .local v2, ray:Lcom/google/android/opengl/carousel/Ray;
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v4, v4, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCamera:Lcom/google/android/opengl/carousel/GLCamera;

    invoke-virtual {v2, v4, p1, p2}, Lcom/google/android/opengl/carousel/Ray;->makeRayForPixelAt(Lcom/google/android/opengl/carousel/GLCamera;FF)Z

    .line 1400
    const v4, 0x7cf0bdc2

    iput v4, v2, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    .line 1401
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v4, v4, Lcom/google/android/opengl/carousel/CarouselSetting;->mDragModel:I

    if-ne v4, v3, :cond_4e

    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselPlane:Lcom/google/android/opengl/carousel/Plane;

    invoke-virtual {v2, v4}, Lcom/google/android/opengl/carousel/Ray;->rayPlaneIntersect(Lcom/google/android/opengl/carousel/Plane;)Z

    move-result v4

    if-eqz v4, :cond_4e

    .line 1403
    new-instance v1, Lcom/google/android/opengl/common/Float3;

    iget-object v4, v2, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    invoke-direct {v1, v4}, Lcom/google/android/opengl/common/Float3;-><init>(Lcom/google/android/opengl/common/Float3;)V

    .line 1404
    .local v1, point:Lcom/google/android/opengl/common/Float3;
    iget-object v4, v2, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    iget v5, v2, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    invoke-static {v4, v5}, Lcom/google/android/opengl/common/Float3;->mupltiple(Lcom/google/android/opengl/common/Float3;F)Lcom/google/android/opengl/common/Float3;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/opengl/common/Float3;->add(Lcom/google/android/opengl/common/Float3;)V

    .line 1406
    new-instance v0, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v0, v1}, Lcom/google/android/opengl/common/Float3;-><init>(Lcom/google/android/opengl/common/Float3;)V

    .line 1407
    .local v0, direction:Lcom/google/android/opengl/common/Float3;
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselPlane:Lcom/google/android/opengl/carousel/Plane;

    iget-object v4, v4, Lcom/google/android/opengl/carousel/Plane;->mPoint:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v0, v4}, Lcom/google/android/opengl/common/Float3;->minus(Lcom/google/android/opengl/common/Float3;)V

    .line 1409
    iget v4, v0, Lcom/google/android/opengl/common/Float3;->x:F

    float-to-double v4, v4

    iget v6, v0, Lcom/google/android/opengl/common/Float3;->z:F

    float-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHitAngle:F

    .line 1423
    .end local v0           #direction:Lcom/google/android/opengl/common/Float3;
    .end local v1           #point:Lcom/google/android/opengl/common/Float3;
    :goto_4d
    return v3

    .line 1411
    :cond_4e
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v4, v4, Lcom/google/android/opengl/carousel/CarouselSetting;->mDragModel:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_5c

    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v4, v4, Lcom/google/android/opengl/carousel/CarouselSetting;->mDragModel:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_90

    :cond_5c
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselCylinder:Lcom/google/android/opengl/carousel/Cylinder;

    invoke-virtual {v2, v4}, Lcom/google/android/opengl/carousel/Ray;->rayCylinderIntersect(Lcom/google/android/opengl/carousel/Cylinder;)Z

    move-result v4

    if-eqz v4, :cond_90

    .line 1414
    new-instance v1, Lcom/google/android/opengl/common/Float3;

    iget-object v4, v2, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    invoke-direct {v1, v4}, Lcom/google/android/opengl/common/Float3;-><init>(Lcom/google/android/opengl/common/Float3;)V

    .line 1415
    .restart local v1       #point:Lcom/google/android/opengl/common/Float3;
    iget-object v4, v2, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    iget v5, v2, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    invoke-static {v4, v5}, Lcom/google/android/opengl/common/Float3;->mupltiple(Lcom/google/android/opengl/common/Float3;F)Lcom/google/android/opengl/common/Float3;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/opengl/common/Float3;->add(Lcom/google/android/opengl/common/Float3;)V

    .line 1417
    new-instance v0, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v0, v1}, Lcom/google/android/opengl/common/Float3;-><init>(Lcom/google/android/opengl/common/Float3;)V

    .line 1418
    .restart local v0       #direction:Lcom/google/android/opengl/common/Float3;
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselCylinder:Lcom/google/android/opengl/carousel/Cylinder;

    iget-object v4, v4, Lcom/google/android/opengl/carousel/Cylinder;->mCenter:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v0, v4}, Lcom/google/android/opengl/common/Float3;->minus(Lcom/google/android/opengl/common/Float3;)V

    .line 1420
    iget v4, v0, Lcom/google/android/opengl/common/Float3;->x:F

    float-to-double v4, v4

    iget v6, v0, Lcom/google/android/opengl/common/Float3;->z:F

    float-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHitAngle:F

    goto :goto_4d

    .line 1423
    .end local v0           #direction:Lcom/google/android/opengl/common/Float3;
    .end local v1           #point:Lcom/google/android/opengl/common/Float3;
    :cond_90
    const/4 v3, 0x0

    goto :goto_4d
.end method

.method private intersectDetailTexture(FFLcom/google/android/opengl/common/Float2;)I
    .registers 13
    .parameter "x"
    .parameter "y"
    .parameter "tapCoordinates"

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 734
    const/4 v1, 0x0

    .local v1, id:I
    :goto_3
    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_4a

    .line 735
    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    .line 736
    .local v0, card:Lcom/google/android/opengl/carousel/Card;
    iget-boolean v6, v0, Lcom/google/android/opengl/carousel/Card;->mDetailVisible:Z

    if-nez v6, :cond_1a

    .line 734
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 739
    :cond_1a
    iget-object v6, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

    aget-object v6, v6, v7

    iget v2, v6, Lcom/google/android/opengl/common/Float2;->x:F

    .line 740
    .local v2, x0:F
    iget-object v6, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

    aget-object v6, v6, v7

    iget v4, v6, Lcom/google/android/opengl/common/Float2;->y:F

    .line 741
    .local v4, y0:F
    iget-object v6, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

    aget-object v6, v6, v8

    iget v3, v6, Lcom/google/android/opengl/common/Float2;->x:F

    .line 742
    .local v3, x1:F
    iget-object v6, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

    aget-object v6, v6, v8

    iget v5, v6, Lcom/google/android/opengl/common/Float2;->y:F

    .line 743
    .local v5, y1:F
    cmpl-float v6, p1, v2

    if-ltz v6, :cond_17

    cmpg-float v6, p1, v3

    if-gtz v6, :cond_17

    cmpl-float v6, p2, v4

    if-ltz v6, :cond_17

    cmpg-float v6, p2, v5

    if-gtz v6, :cond_17

    .line 744
    sub-float v6, p1, v2

    sub-float v7, p2, v4

    invoke-virtual {p3, v6, v7}, Lcom/google/android/opengl/common/Float2;->set(FF)V

    .line 748
    .end local v0           #card:Lcom/google/android/opengl/carousel/Card;
    .end local v1           #id:I
    .end local v2           #x0:F
    .end local v3           #x1:F
    .end local v4           #y0:F
    .end local v5           #y1:F
    :goto_49
    return v1

    .restart local v1       #id:I
    :cond_4a
    const/4 v1, -0x1

    goto :goto_49
.end method

.method private intersectGeometry(Lcom/google/android/opengl/carousel/Ray;)I
    .registers 15
    .parameter "ray"

    .prologue
    .line 752
    const/4 v7, -0x1

    .line 754
    .local v7, hit:I
    const/4 v8, 0x0

    .local v8, id:I
    :goto_2
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v8, v1, :cond_9d

    .line 755
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/opengl/carousel/Card;

    iget-boolean v1, v1, Lcom/google/android/opengl/carousel/Card;->mVisible:Z

    if-eqz v1, :cond_99

    .line 756
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/opengl/carousel/Card;

    .line 757
    .local v6, card:Lcom/google/android/opengl/carousel/Card;
    iget-object v2, v6, Lcom/google/android/opengl/carousel/Card;->mMMatrix:[F

    .line 758
    .local v2, matrix:[F
    const/4 v1, 0x4

    invoke-static {v1}, Lcom/google/android/opengl/common/Float3;->getArray(I)[Lcom/google/android/opengl/common/Float3;

    move-result-object v11

    .line 761
    .local v11, p:[Lcom/google/android/opengl/common/Float3;
    const/4 v12, 0x0

    .local v12, vertex:I
    :goto_26
    const/4 v1, 0x4

    if-ge v12, v1, :cond_7a

    .line 762
    invoke-virtual {v6, v12}, Lcom/google/android/opengl/carousel/Card;->getVertexCoord(I)[F

    move-result-object v4

    .line 763
    .local v4, cardVertices:[F
    const/4 v1, 0x4

    new-array v0, v1, [F

    .line 764
    .local v0, tmp:[F
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 766
    const/4 v1, 0x3

    aget v1, v0, v1

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_61

    .line 767
    aget-object v1, v11, v12

    const/4 v3, 0x0

    aget v3, v0, v3

    iput v3, v1, Lcom/google/android/opengl/common/Float3;->x:F

    .line 768
    aget-object v1, v11, v12

    const/4 v3, 0x1

    aget v3, v0, v3

    iput v3, v1, Lcom/google/android/opengl/common/Float3;->y:F

    .line 769
    aget-object v1, v11, v12

    const/4 v3, 0x2

    aget v3, v0, v3

    iput v3, v1, Lcom/google/android/opengl/common/Float3;->z:F

    .line 770
    aget-object v1, v11, v12

    const/high16 v3, 0x3f80

    const/4 v5, 0x3

    aget v5, v0, v5

    div-float/2addr v3, v5

    invoke-virtual {v1, v3}, Lcom/google/android/opengl/common/Float3;->times(F)V

    .line 761
    :goto_5e
    add-int/lit8 v12, v12, 0x1

    goto :goto_26

    .line 772
    :cond_61
    const-string v1, "CarouselScene"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad w coord: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5e

    .line 777
    .end local v0           #tmp:[F
    .end local v4           #cardVertices:[F
    :cond_7a
    const/4 v1, 0x0

    aget-object v1, v11, v1

    const/4 v3, 0x1

    aget-object v3, v11, v3

    const/4 v5, 0x2

    aget-object v5, v11, v5

    invoke-virtual {p1, v1, v3, v5}, Lcom/google/android/opengl/carousel/Ray;->rayTriangleIntersect(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)Z

    move-result v9

    .line 778
    .local v9, is1:Z
    const/4 v1, 0x2

    aget-object v1, v11, v1

    const/4 v3, 0x3

    aget-object v3, v11, v3

    const/4 v5, 0x0

    aget-object v5, v11, v5

    invoke-virtual {p1, v1, v3, v5}, Lcom/google/android/opengl/carousel/Ray;->rayTriangleIntersect(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)Z

    move-result v10

    .line 779
    .local v10, is2:Z
    if-nez v9, :cond_98

    if-eqz v10, :cond_99

    .line 780
    :cond_98
    move v7, v8

    .line 754
    .end local v2           #matrix:[F
    .end local v6           #card:Lcom/google/android/opengl/carousel/Card;
    .end local v9           #is1:Z
    .end local v10           #is2:Z
    .end local v11           #p:[Lcom/google/android/opengl/common/Float3;
    .end local v12           #vertex:I
    :cond_99
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    .line 785
    :cond_9d
    return v7
.end method

.method private isInMotion(F)Z
    .registers 3
    .parameter "threshold"

    .prologue
    .line 1190
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, p1

    if-lez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private logistic(F)F
    .registers 6
    .parameter "t"

    .prologue
    const-wide/high16 v2, 0x3ff0

    .line 1656
    neg-float v0, p1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    add-double/2addr v0, v2

    div-double v0, v2, v0

    double-to-float v0, v0

    return v0
.end method

.method private maximumBias()F
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 1204
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    div-int v0, v1, v2

    .line 1205
    .local v0, totalSlots:I
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mFillDirection:I

    if-lez v1, :cond_1d

    invoke-direct {p0, v3}, Lcom/google/android/opengl/carousel/CarouselScene;->wedgeAngle(F)F

    move-result v1

    :goto_1c
    return v1

    :cond_1d
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleDetailCount:I

    sub-int v1, v0, v1

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->wedgeAngle(F)F

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto :goto_1c
.end method

.method private minimumBias()F
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 1196
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    div-int v0, v1, v2

    .line 1197
    .local v0, totalSlots:I
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mFillDirection:I

    if-lez v1, :cond_29

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleDetailCount:I

    sub-int v1, v0, v1

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->wedgeAngle(F)F

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    neg-float v1, v1

    :goto_28
    return v1

    :cond_29
    invoke-direct {p0, v3}, Lcom/google/android/opengl/carousel/CarouselScene;->wedgeAngle(F)F

    move-result v1

    goto :goto_28
.end method

.method private projectedDelta(FF)F
    .registers 9
    .parameter
    .parameter

    .prologue
    const/high16 v5, 0x4040

    .line 1108
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, p2

    float-to-double v0, v0

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float v2, p1, v2

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1109
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, p2

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float v2, p1, v2

    invoke-static {v1, v2}, Landroid/graphics/PointF;->length(FF)F

    move-result v1

    .line 1110
    float-to-double v1, v1

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mTrajectoryAngle:F

    sub-float/2addr v0, v3

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    mul-double v0, v1, v3

    double-to-float v0, v0

    .line 1111
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mWidth:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselRenderer;->mHeight:I

    int-to-float v2, v2

    invoke-static {v1, v2}, Landroid/graphics/PointF;->length(FF)F

    move-result v1

    div-float/2addr v0, v1

    .line 1112
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-boolean v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mIsPortrait:Z

    if-eqz v1, :cond_47

    .line 1114
    div-float/2addr v0, v5

    .line 1117
    :cond_47
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mEnableBoostArea:Z

    if-eqz v1, :cond_5f

    .line 1119
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mWidth:I

    add-int/lit8 v1, v1, -0x78

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-lez v1, :cond_5f

    const/high16 v1, 0x4396

    cmpg-float v1, p2, v1

    if-gez v1, :cond_5f

    .line 1120
    mul-float/2addr v0, v5

    .line 1124
    :cond_5f
    return v0
.end method

.method private radiansToCarouselRotationAngle(F)F
    .registers 7
    .parameter "angle"

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mFillDirection:I

    int-to-float v0, v0

    neg-float v1, p1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mSlotCount:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-double v1, v1

    const-wide v3, 0x401921fb54442d18L

    div-double/2addr v1, v3

    double-to-float v1, v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private rotateM([FIFFFF)V
    .registers 16
    .parameter "m"
    .parameter "mOffset"
    .parameter "a"
    .parameter "x"
    .parameter "y"
    .parameter "z"

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x10

    .line 1387
    sget-object v0, Lcom/google/android/opengl/carousel/CarouselScene;->sRotationTemp:[F

    .local v0, temp:[F
    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    .line 1388
    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->setRotateM([FIFFFF)V

    move-object v2, v0

    move v3, v8

    move-object v4, p1

    move v5, p2

    move-object v6, v0

    move v7, v1

    .line 1389
    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1390
    invoke-static {v0, v8, p1, p2, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1391
    return-void
.end method

.method private setCardPosition()V
    .registers 9

    .prologue
    const v3, 0x8892

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 363
    const/4 v6, 0x0

    .line 364
    .local v6, USE_VBO:Z
    if-nez v6, :cond_13

    .line 365
    const/4 v1, 0x3

    const/16 v2, 0x1406

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVertices:Ljava/nio/FloatBuffer;

    move v3, v0

    move v4, v0

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 376
    :goto_12
    return-void

    .line 370
    :cond_13
    new-array v7, v1, [I

    .line 371
    .local v7, vboIds:[I
    invoke-static {v1, v7, v0}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 372
    aget v0, v7, v0

    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 373
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVerticesData:[F

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVertices:Ljava/nio/FloatBuffer;

    const v2, 0x88e4

    invoke-static {v3, v0, v1, v2}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    goto :goto_12
.end method

.method private slotAngle(F)F
    .registers 6
    .parameter "p"

    .prologue
    .line 1270
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    invoke-direct {p0, p1}, Lcom/google/android/opengl/carousel/CarouselScene;->wedgeAngle(F)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mFillDirection:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float v0, v1, v2

    .line 1271
    .local v0, angle:F
    return v0
.end method

.method private stopAutoscroll()V
    .registers 3

    .prologue
    .line 1606
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsAutoScrolling:Z

    .line 1607
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartTime:D

    .line 1608
    return-void
.end method

.method private tiltOverscroll()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 977
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    iget v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_b

    .line 979
    const/4 v0, 0x0

    .line 993
    :goto_a
    return v0

    .line 983
    :cond_b
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    iget v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    sub-float/2addr v1, v2

    .line 985
    const v2, 0x3ec90fdb

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mTiltMaximumAngle:F

    mul-float/2addr v1, v2

    .line 987
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mFillDirection:I

    if-ne v2, v0, :cond_22

    .line 988
    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    goto :goto_a

    .line 990
    :cond_22
    neg-float v1, v1

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    goto :goto_a
.end method

.method private updateCardResources(J)V
    .registers 8
    .parameter "currentTime"

    .prologue
    .line 438
    const/4 v2, 0x1

    .line 439
    .local v2, requestLargeTexture:Z
    iget-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsDragging:Z

    if-nez v3, :cond_14

    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v4, v4, Lcom/google/android/opengl/carousel/CarouselSetting;->mTextureVelocityThreshold:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_14

    .line 441
    const/4 v2, 0x0

    .line 446
    :cond_14
    const/4 v1, 0x0

    .local v1, i:I
    :goto_15
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_33

    .line 447
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    .line 448
    .local v0, card:Lcom/google/android/opengl/carousel/Card;
    iget-boolean v3, v0, Lcom/google/android/opengl/carousel/Card;->mPrefetchTexture:Z

    if-eqz v3, :cond_2f

    .line 449
    invoke-virtual {v0, v2}, Lcom/google/android/opengl/carousel/Card;->requestTexture(Z)V

    .line 446
    :goto_2c
    add-int/lit8 v1, v1, 0x1

    goto :goto_15

    .line 451
    :cond_2f
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/Card;->initCardTexture()V

    goto :goto_2c

    .line 454
    .end local v0           #card:Lcom/google/android/opengl/carousel/Card;
    :cond_33
    return-void
.end method

.method private updateNextPosition(J)Z
    .registers 11
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 1214
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->deltaTimeInSeconds(J)F

    move-result v2

    .line 1215
    cmpg-float v3, v2, v7

    if-gtz v3, :cond_c

    .line 1256
    :goto_b
    return v1

    .line 1218
    :cond_c
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->maximumBias()F

    move-result v3

    .line 1219
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->minimumBias()F

    move-result v4

    .line 1224
    iget-boolean v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    if-eqz v5, :cond_50

    .line 1225
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    const v6, 0x3c23d70a

    cmpl-float v5, v5, v6

    if-lez v5, :cond_34

    .line 1226
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mTiltMaximumAngle:F

    mul-float/2addr v2, v5

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    .line 1249
    :goto_2b
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    invoke-static {v0, v4, v3}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v0

    .line 1251
    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    goto :goto_b

    .line 1228
    :cond_34
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    const v6, -0x43dc28f6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_48

    .line 1229
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mTiltMaximumAngle:F

    mul-float/2addr v2, v5

    add-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    goto :goto_2b

    .line 1232
    :cond_48
    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    .line 1233
    iput v7, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    .line 1234
    iput v7, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    move v1, v0

    goto :goto_2b

    .line 1236
    :cond_50
    iget-boolean v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsAutoScrolling:Z

    if-eqz v5, :cond_5a

    .line 1237
    long-to-float v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->doAutoscroll(F)Z

    move-result v1

    goto :goto_2b

    .line 1239
    :cond_5a
    invoke-direct {p0, v2}, Lcom/google/android/opengl/carousel/CarouselScene;->doPhysics(F)Z

    move-result v2

    .line 1240
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    cmpl-float v5, v5, v7

    if-eqz v5, :cond_65

    move v0, v1

    :cond_65
    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    .line 1241
    iget-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    if-eqz v0, :cond_6e

    .line 1242
    iput v7, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    goto :goto_2b

    :cond_6e
    move v1, v2

    goto :goto_2b
.end method

.method private declared-synchronized updateTexture(Lcom/google/android/opengl/carousel/RequestableTexture;Landroid/graphics/Bitmap;)V
    .registers 6
    .parameter "texture"
    .parameter "bitmap"

    .prologue
    .line 486
    monitor-enter p0

    if-nez p2, :cond_5

    .line 498
    :goto_3
    monitor-exit p0

    return-void

    .line 490
    :cond_5
    :try_start_5
    invoke-virtual {p1}, Lcom/google/android/opengl/carousel/RequestableTexture;->isTextureIdAllocated()Z

    move-result v0

    if-nez v0, :cond_15

    .line 491
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/opengl/carousel/RequestableTexture;->setTextureLoadTime(J)V

    .line 492
    invoke-virtual {p1}, Lcom/google/android/opengl/carousel/RequestableTexture;->genTextureId()V

    .line 494
    :cond_15
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/opengl/carousel/RequestableTexture;->setExtent(II)V

    .line 495
    const/16 v0, 0xde1

    invoke-virtual {p1}, Lcom/google/android/opengl/carousel/RequestableTexture;->getTextureId()I

    move-result v1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 496
    invoke-static {}, Lcom/google/android/opengl/carousel/GL2Helper;->setDefaultNPOTTextureState()V

    .line 497
    const/16 v0, 0xde1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, p2, v2}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V
    :try_end_33
    .catchall {:try_start_5 .. :try_end_33} :catchall_34

    goto :goto_3

    .line 486
    :catchall_34
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private wedgeAngle(F)F
    .registers 6
    .parameter "cards"

    .prologue
    .line 1129
    const/high16 v0, 0x4000

    mul-float/2addr v0, p1

    float-to-double v0, v0

    const-wide v2, 0x400921fb54442d18L

    mul-double/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mSlotCount:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method


# virtual methods
.method cancelHover()V
    .registers 2

    .prologue
    const/4 v0, -0x1

    .line 855
    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverCard:I

    .line 856
    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverDetail:I

    .line 857
    return-void
.end method

.method cardAngle(F)F
    .registers 5
    .parameter "p"

    .prologue
    .line 1263
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    int-to-float v2, v2

    div-float v2, p1, v2

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-direct {p0, v2}, Lcom/google/android/opengl/carousel/CarouselScene;->slotAngle(F)F

    move-result v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRotationAngle:F

    add-float v0, v1, v2

    .line 1264
    .local v0, angle:F
    return v0
.end method

.method public disableMixAndFade()V
    .registers 2

    .prologue
    .line 570
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMixAndFadeEnabled:Z

    .line 571
    return-void
.end method

.method doHover(FF)Z
    .registers 9
    .parameter "x"
    .parameter "y"

    .prologue
    const/4 v5, -0x1

    .line 830
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->doSelection(FF)I

    move-result v0

    .line 831
    .local v0, hoverCard:I
    new-instance v3, Lcom/google/android/opengl/common/Float2;

    invoke-direct {v3}, Lcom/google/android/opengl/common/Float2;-><init>()V

    .line 832
    .local v3, point:Lcom/google/android/opengl/common/Float2;
    invoke-direct {p0, p1, p2, v3}, Lcom/google/android/opengl/carousel/CarouselScene;->intersectDetailTexture(FFLcom/google/android/opengl/common/Float2;)I

    move-result v1

    .line 833
    .local v1, hoverDetail:I
    const/4 v2, 0x0

    .line 834
    .local v2, hoverOccur:Z
    iget v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverCard:I

    if-eq v0, v4, :cond_1a

    .line 835
    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverCard:I

    .line 836
    if-eq v0, v5, :cond_1a

    .line 837
    const/4 v2, 0x1

    .line 838
    iput v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverDetail:I

    .line 841
    :cond_1a
    iget v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverDetail:I

    if-eq v1, v4, :cond_25

    .line 842
    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverDetail:I

    .line 843
    if-eq v1, v5, :cond_25

    .line 844
    const/4 v2, 0x1

    .line 845
    iput v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverCard:I

    .line 848
    :cond_25
    return v2
.end method

.method doLongPress()Z
    .registers 11

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 901
    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-nez v6, :cond_8

    move v6, v7

    .line 923
    :goto_7
    return v6

    .line 903
    :cond_8
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 906
    .local v0, currentTime:J
    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    iget-object v9, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v6, v9}, Lcom/google/android/opengl/carousel/CarouselScene;->doSelection(FF)I

    move-result v4

    .line 907
    .local v4, selection:I
    iget-boolean v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    if-eqz v6, :cond_66

    const/4 v6, -0x1

    if-eq v4, v6, :cond_66

    .line 909
    const/4 v6, 0x2

    new-array v5, v6, [I

    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    float-to-int v6, v6

    aput v6, v5, v7

    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    float-to-int v6, v6

    aput v6, v5, v8

    .line 910
    .local v5, touchPosition:[I
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 911
    .local v2, detailCoordinates:Landroid/graphics/Rect;
    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/opengl/carousel/Card;

    iget-object v3, v6, Lcom/google/android/opengl/carousel/Card;->mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

    .line 912
    .local v3, pos:[Lcom/google/android/opengl/common/Float2;
    aget-object v6, v3, v7

    iget v6, v6, Lcom/google/android/opengl/common/Float2;->x:F

    float-to-int v6, v6

    iput v6, v2, Landroid/graphics/Rect;->left:I

    .line 913
    aget-object v6, v3, v7

    iget v6, v6, Lcom/google/android/opengl/common/Float2;->y:F

    float-to-int v6, v6

    iput v6, v2, Landroid/graphics/Rect;->bottom:I

    .line 914
    aget-object v6, v3, v8

    iget v6, v6, Lcom/google/android/opengl/common/Float2;->x:F

    float-to-int v6, v6

    iput v6, v2, Landroid/graphics/Rect;->right:I

    .line 915
    aget-object v6, v3, v8

    iget v6, v6, Lcom/google/android/opengl/common/Float2;->y:F

    float-to-int v6, v6

    iput v6, v2, Landroid/graphics/Rect;->top:I

    .line 916
    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    invoke-interface {v6, v4, v5, v2}, Lcom/google/android/opengl/carousel/CarouselCallback;->onCardLongPress(I[ILandroid/graphics/Rect;)V

    .line 917
    iput-boolean v7, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    .line 918
    iput-boolean v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOnLongPress:Z

    move v6, v8

    .line 919
    goto :goto_7

    .line 921
    .end local v2           #detailCoordinates:Landroid/graphics/Rect;
    .end local v3           #pos:[Lcom/google/android/opengl/common/Float2;
    .end local v5           #touchPosition:[I
    :cond_66
    iput-wide v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    move v6, v7

    .line 923
    goto :goto_7
.end method

.method public doMotion(FF)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const v8, 0x3ec90fdb

    const/4 v2, 0x0

    .line 927
    iget-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOnLongPress:Z

    if-eqz v0, :cond_9

    .line 974
    :goto_8
    return-void

    .line 933
    :cond_9
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->maximumBias()F

    move-result v3

    .line 934
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->minimumBias()F

    move-result v4

    .line 936
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    .line 937
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->dragFunction(FF)F

    move-result v1

    .line 939
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mAntiJitter:Z

    if-eqz v0, :cond_d2

    .line 940
    iget-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    if-nez v0, :cond_d2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v7, 0x3951b717

    cmpl-float v0, v0, v7

    if-lez v0, :cond_d2

    .line 941
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mMaxDeltaBias:F

    neg-float v0, v0

    iget-object v7, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v7, v7, Lcom/google/android/opengl/carousel/CarouselSetting;->mMaxDeltaBias:F

    invoke-static {v1, v0, v7}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v0

    .line 945
    :goto_3b
    iget v7, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    add-float/2addr v0, v7

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    .line 946
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    sub-float v7, v4, v8

    add-float/2addr v8, v3

    invoke-static {v0, v7, v8}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    .line 948
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    invoke-static {v0, v4, v3}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    .line 949
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->tiltOverscroll()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    .line 951
    new-instance v0, Landroid/graphics/PointF;

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchPosition:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float v3, p1, v3

    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchPosition:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float v4, p2, v4

    invoke-direct {v0, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 952
    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v4, v0, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v4

    iget v4, v0, Landroid/graphics/PointF;->y:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    mul-float/2addr v0, v4

    add-float/2addr v0, v3

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    double-to-float v0, v3

    .line 953
    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSelectionRadius:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_d0

    const/4 v0, 0x1

    .line 954
    :goto_82
    iget-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    and-int/2addr v0, v3

    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    .line 955
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 957
    invoke-direct {p0, v5, v6}, Lcom/google/android/opengl/carousel/CarouselScene;->deltaTimeInSeconds(J)F

    move-result v0

    .line 958
    cmpl-float v3, v0, v2

    if-lez v3, :cond_c6

    .line 959
    div-float v0, v1, v0

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mAccelerationFactor:F

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mAccelerationRatio:F

    mul-float/2addr v0, v1

    .line 960
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mVelocityUpLimit:F

    neg-float v1, v1

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mVelocityUpLimit:F

    invoke-static {v0, v1, v3}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v0

    .line 961
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMinVelocity:F

    cmpg-float v1, v1, v3

    if-gez v1, :cond_b8

    move v0, v2

    .line 964
    :cond_b8
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistory:[F

    iget v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    rem-int/lit8 v2, v2, 0x5

    aput v0, v1, v2

    .line 965
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    .line 968
    :cond_c6
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->computeAverageVelocityFromHistory()F

    move-result v0

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 969
    iput-wide v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    goto/16 :goto_8

    .line 953
    :cond_d0
    const/4 v0, 0x0

    goto :goto_82

    :cond_d2
    move v0, v1

    goto/16 :goto_3b
.end method

.method public doStart(FF)V
    .registers 10
    .parameter "x"
    .parameter "y"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 789
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    invoke-virtual {v1, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 790
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchPosition:Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    invoke-virtual {v1, v4}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 792
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->hitAngle(FF)Z

    move-result v1

    if-eqz v1, :cond_5b

    .line 793
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHitAngle:F

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastAngle:F

    .line 799
    :goto_19
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSelectionVelocityThreshold:F

    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->isInMotion(F)Z

    move-result v1

    if-nez v1, :cond_5e

    move v1, v2

    :goto_22
    iput-boolean v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    .line 801
    iput v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 802
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistory:[F

    aput v6, v1, v3

    .line 803
    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    .line 806
    iget-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    iput-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mReleaseTime:J

    .line 807
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchTime:J

    .line 808
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchBias:F

    .line 810
    iput-boolean v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsDragging:Z

    .line 812
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->doSelection(FF)I

    move-result v1

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimatedSelection:I

    .line 813
    new-instance v0, Lcom/google/android/opengl/common/Float2;

    invoke-direct {v0}, Lcom/google/android/opengl/common/Float2;-><init>()V

    .line 814
    .local v0, point:Lcom/google/android/opengl/common/Float2;
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->intersectDetailTexture(FFLcom/google/android/opengl/common/Float2;)I

    move-result v1

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSelectedDetail:I

    .line 816
    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    .line 817
    iput v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    .line 818
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    .line 819
    iput v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 820
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->stopAutoscroll()V

    .line 821
    return-void

    .line 795
    .end local v0           #point:Lcom/google/android/opengl/common/Float2;
    :cond_5b
    iput v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastAngle:F

    goto :goto_19

    :cond_5e
    move v1, v3

    .line 799
    goto :goto_22
.end method

.method public doStop(FF)V
    .registers 13
    .parameter "x"
    .parameter "y"

    .prologue
    const/4 v9, 0x1

    const/4 v5, -0x1

    const/4 v8, 0x0

    .line 861
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-nez v4, :cond_8

    .line 894
    :goto_7
    return-void

    .line 863
    :cond_8
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 865
    .local v0, currentTime:J
    iput-wide v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mReleaseTime:J

    .line 866
    iput-boolean v9, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    .line 867
    iget-boolean v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    if-eqz v4, :cond_45

    .line 868
    const/4 v3, -0x1

    .line 869
    .local v3, selection:I
    new-instance v2, Lcom/google/android/opengl/common/Float2;

    invoke-direct {v2}, Lcom/google/android/opengl/common/Float2;-><init>()V

    .line 870
    .local v2, point:Lcom/google/android/opengl/common/Float2;
    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/opengl/carousel/CarouselScene;->intersectDetailTexture(FFLcom/google/android/opengl/common/Float2;)I

    move-result v3

    if-eq v3, v5, :cond_36

    .line 872
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    iget v5, v2, Lcom/google/android/opengl/common/Float2;->x:F

    float-to-int v5, v5

    iget v6, v2, Lcom/google/android/opengl/common/Float2;->y:F

    float-to-int v6, v6

    invoke-interface {v4, v3, v5, v6}, Lcom/google/android/opengl/carousel/CarouselCallback;->onDetailSelected(III)V

    .line 879
    :cond_2b
    :goto_2b
    iput-boolean v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    .line 890
    .end local v2           #point:Lcom/google/android/opengl/common/Float2;
    .end local v3           #selection:I
    :cond_2d
    :goto_2d
    iput-boolean v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    .line 891
    iput-boolean v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOnLongPress:Z

    .line 892
    iput-wide v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    .line 893
    iput-boolean v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsDragging:Z

    goto :goto_7

    .line 873
    .restart local v2       #point:Lcom/google/android/opengl/common/Float2;
    .restart local v3       #selection:I
    :cond_36
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->doSelection(FF)I

    move-result v3

    if-eq v3, v5, :cond_2b

    .line 876
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->sendAnimationFinished()V

    .line 877
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    invoke-interface {v4, v3}, Lcom/google/android/opengl/carousel/CarouselCallback;->onCardSelected(I)V

    goto :goto_2b

    .line 881
    .end local v2           #point:Lcom/google/android/opengl/common/Float2;
    .end local v3           #selection:I
    :cond_45
    iget-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastStopTime:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_59

    iget-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastStopTime:J

    sub-long v4, v0, v4

    iget-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mStopTimeThreshold:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_59

    .line 882
    iput v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    .line 884
    :cond_59
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->computeAverageVelocityFromHistory()F

    move-result v4

    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 885
    iget v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mVelocityUpLimit:F

    neg-float v5, v5

    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v6, v6, Lcom/google/android/opengl/carousel/CarouselSetting;->mVelocityUpLimit:F

    invoke-static {v4, v5, v6}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v4

    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 886
    iget v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityThreshold:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_2d

    .line 887
    iput-boolean v9, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    goto :goto_2d
.end method

.method public draw()Z
    .registers 10

    .prologue
    const/16 v8, 0xbe2

    const/4 v3, 0x0

    .line 252
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 254
    .local v0, currentTime:J
    iget-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchTime:J

    sub-long v4, v0, v4

    const-wide/16 v6, 0xc8

    cmp-long v4, v4, v6

    if-gtz v4, :cond_22

    const/4 v2, 0x1

    .line 256
    .local v2, stillAnimating:Z
    :goto_12
    invoke-static {v8}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 257
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBackground:Lcom/google/android/opengl/carousel/Background;

    invoke-virtual {v4, v0, v1}, Lcom/google/android/opengl/carousel/Background;->draw(J)Z

    .line 259
    invoke-static {v8}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 261
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-nez v4, :cond_24

    .line 294
    :cond_21
    :goto_21
    return v3

    .end local v2           #stillAnimating:Z
    :cond_22
    move v2, v3

    .line 254
    goto :goto_12

    .line 263
    .restart local v2       #stillAnimating:Z
    :cond_24
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v4

    if-lez v4, :cond_21

    .line 266
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-boolean v3, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mAutoRotation:Z

    if-eqz v3, :cond_3c

    .line 267
    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRotationAngle:F

    float-to-double v3, v3

    const-wide v5, 0x3f60624dd2f1a9fcL

    sub-double/2addr v3, v5

    double-to-float v3, v3

    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRotationAngle:F

    .line 270
    :cond_3c
    iget-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsDragging:Z

    if-nez v3, :cond_44

    .line 271
    invoke-direct {p0, v0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->updateNextPosition(J)Z

    move-result v2

    .line 274
    :cond_44
    iput-wide v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    .line 276
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->cullCards()I

    .line 277
    invoke-direct {p0, v0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->updateCardResources(J)V

    .line 279
    invoke-direct {p0, v0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->drawCards(J)Z

    move-result v3

    or-int/2addr v2, v3

    .line 280
    invoke-direct {p0, v0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->drawDetails(J)Z

    move-result v3

    or-int/2addr v2, v3

    .line 282
    iget-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    if-eq v2, v3, :cond_61

    .line 283
    if-eqz v2, :cond_69

    .line 285
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->sendAnimationStarted()V

    .line 290
    :goto_5f
    iput-boolean v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    .line 293
    :cond_61
    const-string v3, "CarouselScene.draw"

    invoke-static {v3}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 294
    iget-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    goto :goto_21

    .line 288
    :cond_69
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->sendAnimationFinished()V

    goto :goto_5f
.end method

.method getAnimatedAlpha(JJ)F
    .registers 11
    .parameter "startTime"
    .parameter "currentTime"

    .prologue
    .line 691
    sub-long v4, p3, p1

    long-to-double v2, v4

    .line 692
    .local v2, timeElapsed:D
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-wide v4, v4, Lcom/google/android/opengl/carousel/CarouselSetting;->mFadeInDuration:J

    long-to-double v4, v4

    div-double v0, v2, v4

    .line 693
    .local v0, alpha:D
    const/high16 v4, 0x3f80

    double-to-float v5, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    return v4
.end method

.method getCardCount()I
    .registers 2

    .prologue
    .line 1676
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getCarouselRotationPosition()I
    .registers 3

    .prologue
    .line 347
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->radiansToCarouselRotationAngle(F)F

    move-result v0

    .line 348
    .local v0, f:F
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    return v1
.end method

.method public getFade0()F
    .registers 2

    .prologue
    .line 586
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mFade0:F

    return v0
.end method

.method public getFade1()F
    .registers 2

    .prologue
    .line 590
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mFade1:F

    return v0
.end method

.method getFadeOutLeftAlpha(I)F
    .registers 7
    .parameter "i"

    .prologue
    const/high16 v2, 0x3f80

    const/4 v4, 0x0

    .line 700
    int-to-float v1, p1

    invoke-virtual {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->cardAngle(F)F

    move-result v0

    .line 701
    .local v0, angle:F
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mFadeOutLeftAngle:F

    cmpl-float v1, v1, v4

    if-lez v1, :cond_34

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_34

    .line 702
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/opengl/carousel/Card;

    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/google/android/opengl/carousel/Card;->mFadeWithEmptyTexture:Z

    .line 703
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    sub-float v1, v0, v1

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mFadeOutLeftAngle:F

    div-float/2addr v1, v3

    add-float/2addr v1, v2

    invoke-static {v4, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 706
    :goto_33
    return v1

    .line 705
    :cond_34
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/opengl/carousel/Card;

    const/4 v3, 0x0

    iput-boolean v3, v1, Lcom/google/android/opengl/carousel/Card;->mFadeWithEmptyTexture:Z

    move v1, v2

    .line 706
    goto :goto_33
.end method

.method public getFocusedItem()I
    .registers 2

    .prologue
    .line 1493
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mFocusedItem:I

    return v0
.end method

.method public getHoverCard()I
    .registers 2

    .prologue
    .line 1501
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverCard:I

    return v0
.end method

.method public getHoverDetail()I
    .registers 2

    .prologue
    .line 1505
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverDetail:I

    return v0
.end method

.method getMatrixForCard([FIZZ)Z
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1290
    int-to-float v0, p2

    invoke-virtual {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->cardAngle(F)F

    move-result v7

    .line 1291
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    invoke-direct {p0, v0, p3}, Lcom/google/android/opengl/carousel/CarouselScene;->getSwayAngleForVelocity(FZ)F

    move-result v8

    .line 1292
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1293
    const/4 v2, 0x0

    float-to-double v0, v7

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v3, v0

    const/4 v4, 0x0

    const/high16 v5, 0x3f80

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/opengl/carousel/CarouselScene;->rotateM([FIFFFF)V

    .line 1294
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mRadius:F

    invoke-direct {p0, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->getVerticalOffsetForCard(I)F

    move-result v2

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1296
    invoke-direct {p0, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardTiltAngle(I)F

    move-result v0

    .line 1297
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mCardRotation:F

    add-float/2addr v1, v8

    add-float/2addr v0, v1

    .line 1298
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mCardFaceTangent:Z

    if-nez v1, :cond_3d

    .line 1299
    sub-float/2addr v0, v7

    .line 1302
    :cond_3d
    const/4 v2, 0x0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v3, v0

    const/4 v4, 0x0

    const/high16 v5, 0x3f80

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/opengl/carousel/CarouselScene;->rotateM([FIFFFF)V

    .line 1303
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-boolean v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mIsPortrait:Z

    if-eqz v0, :cond_6f

    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mSpecialRotationInPortrait:Z

    if-eqz v0, :cond_6f

    .line 1306
    const/4 v1, 0x0

    const/high16 v2, -0x3eb0

    const/high16 v3, 0x3f80

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 1307
    const/4 v1, 0x0

    const/high16 v2, 0x4000

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v5, 0x3f80

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 1310
    :cond_6f
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mCardXYScale:Lcom/google/android/opengl/common/Float2;

    iget v1, v1, Lcom/google/android/opengl/common/Float2;->x:F

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mCardXYScale:Lcom/google/android/opengl/common/Float2;

    iget v2, v2, Lcom/google/android/opengl/common/Float2;->y:F

    const/high16 v3, 0x3f80

    invoke-static {p1, v0, v1, v2, v3}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 1311
    const/4 v0, 0x0

    .line 1312
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimatedSelection:I

    if-ne p2, v1, :cond_b4

    .line 1313
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mScaleSelectedCard:Z

    if-eqz v1, :cond_b4

    .line 1314
    invoke-static {}, Lcom/google/android/opengl/common/Float3;->getUnit()Lcom/google/android/opengl/common/Float3;

    move-result-object v1

    .line 1315
    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->getAnimatedScaleForSelected(Lcom/google/android/opengl/common/Float3;)Z

    move-result v0

    .line 1316
    const/4 v2, 0x0

    iget v3, v1, Lcom/google/android/opengl/common/Float3;->x:F

    iget v4, v1, Lcom/google/android/opengl/common/Float3;->y:F

    iget v1, v1, Lcom/google/android/opengl/common/Float3;->z:F

    invoke-static {p1, v2, v3, v4, v1}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    move v6, v0

    .line 1320
    :goto_9f
    if-eqz p4, :cond_b3

    .line 1321
    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Card;->mClientMatrix:[F

    const/4 v5, 0x0

    move-object v0, p1

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1324
    :cond_b3
    return v6

    :cond_b4
    move v6, v0

    goto :goto_9f
.end method

.method public getMix0()F
    .registers 2

    .prologue
    .line 578
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMix0:F

    return v0
.end method

.method public getMix1()F
    .registers 2

    .prologue
    .line 582
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMix1:F

    return v0
.end method

.method public getRealtimeCarouselRotationAngle()F
    .registers 2

    .prologue
    .line 339
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->radiansToCarouselRotationAngle(F)F

    move-result v0

    return v0
.end method

.method invalidateDetailTexture(IZ)V
    .registers 5
    .parameter "n"
    .parameter "eraseCurrent"

    .prologue
    .line 1539
    if-ltz p1, :cond_8

    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v0

    if-lt p1, v0, :cond_9

    .line 1542
    :cond_8
    :goto_8
    return-void

    .line 1541
    :cond_9
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/opengl/carousel/Card;->invalidateTexture(I)V

    goto :goto_8
.end method

.method public invalidateDetailTextures(Z)V
    .registers 5
    .parameter "eraseCurrent"

    .prologue
    .line 1545
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_17

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    .line 1546
    .local v0, card:Lcom/google/android/opengl/carousel/Card;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/opengl/carousel/Card;->invalidateTexture(I)V

    goto :goto_6

    .line 1548
    .end local v0           #card:Lcom/google/android/opengl/carousel/Card;
    :cond_17
    return-void
.end method

.method public isMixAndFadeEnabled()Z
    .registers 2

    .prologue
    .line 574
    iget-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMixAndFadeEnabled:Z

    return v0
.end method

.method public sendAnimationFinished()V
    .registers 3

    .prologue
    .line 320
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-nez v1, :cond_5

    .line 330
    :goto_4
    return-void

    .line 326
    :cond_5
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->radiansToCarouselRotationAngle(F)F

    move-result v0

    .line 329
    .local v0, angle:F
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    invoke-interface {v1, v0}, Lcom/google/android/opengl/carousel/CarouselCallback;->onAnimationFinished(F)V

    goto :goto_4
.end method

.method sendAnimationStarted()V
    .registers 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-eqz v0, :cond_9

    .line 312
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    invoke-interface {v0}, Lcom/google/android/opengl/carousel/CarouselCallback;->onAnimationStarted()V

    .line 314
    :cond_9
    return-void
.end method

.method public setCarouselRotationAngle(F)V
    .registers 4
    .parameter "angle"

    .prologue
    .line 1520
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    int-to-float v1, v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselRotationAngle:F

    .line 1521
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->updateCarouselRotationAngle()V

    .line 1522
    return-void
.end method

.method setCarouselRotationAngle(FIIF)V
    .registers 11
    .parameter "endAngle"
    .parameter "milliseconds"
    .parameter "interpolationMode"
    .parameter "maxAnimatedArc"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1569
    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    invoke-direct {p0, v3}, Lcom/google/android/opengl/carousel/CarouselScene;->radiansToCarouselRotationAngle(F)F

    move-result v0

    .line 1571
    .local v0, actualStart:F
    cmpl-float v3, p4, v5

    if-lez v3, :cond_18

    .line 1573
    cmpg-float v3, v0, p1

    if-gtz v3, :cond_4f

    .line 1574
    sub-float v3, p1, p4

    cmpg-float v3, v0, v3

    if-gez v3, :cond_18

    .line 1575
    sub-float v0, p1, p4

    .line 1585
    :cond_18
    :goto_18
    iput-boolean v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    .line 1586
    iput-boolean v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsAutoScrolling:Z

    .line 1587
    int-to-double v3, p2

    iput-wide v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollDuration:D

    .line 1588
    iput p3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollInterpolationMode:I

    .line 1589
    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->carouselRotationAngleToRadians(F)F

    move-result v3

    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartAngle:F

    .line 1590
    invoke-direct {p0, p1}, Lcom/google/android/opengl/carousel/CarouselScene;->carouselRotationAngleToRadians(F)F

    move-result v3

    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStopAngle:F

    .line 1593
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->maximumBias()F

    move-result v1

    .line 1594
    .local v1, highBias:F
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->minimumBias()F

    move-result v2

    .line 1595
    .local v2, lowBias:F
    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartAngle:F

    invoke-static {v3, v2, v1}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v3

    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartAngle:F

    .line 1596
    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStopAngle:F

    invoke-static {v3, v2, v1}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v3

    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStopAngle:F

    .line 1599
    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartTime:D

    .line 1600
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    .line 1601
    iput v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 1602
    return-void

    .line 1579
    .end local v1           #highBias:F
    .end local v2           #lowBias:F
    :cond_4f
    add-float v3, p1, p4

    cmpl-float v3, v0, v3

    if-lez v3, :cond_18

    .line 1580
    add-float v0, p1, p4

    goto :goto_18
.end method

.method setDetailTexture(IFFFFLandroid/graphics/Bitmap;)V
    .registers 11
    .parameter "n"
    .parameter "offx"
    .parameter "offy"
    .parameter "loffx"
    .parameter "loffy"
    .parameter "bitmap"

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 511
    if-ltz p1, :cond_a

    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v1

    if-lt p1, v1, :cond_14

    .line 512
    :cond_a
    if-eqz p6, :cond_13

    .line 513
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    invoke-virtual {v1, v3, p6}, Lcom/google/android/opengl/carousel/CarouselSetting;->recycleIfRequired(ILandroid/graphics/Bitmap;)Z

    .line 535
    :cond_13
    :goto_13
    return-void

    .line 518
    :cond_14
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    .line 519
    .local v0, card:Lcom/google/android/opengl/carousel/Card;
    if-nez p6, :cond_21

    .line 520
    iput v2, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTextureState:I

    goto :goto_13

    .line 524
    :cond_21
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->haveEGLContext()Z

    move-result v1

    if-eqz v1, :cond_49

    .line 525
    iget-object v1, v0, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-direct {p0, v1, p6}, Lcom/google/android/opengl/carousel/CarouselScene;->updateTexture(Lcom/google/android/opengl/carousel/RequestableTexture;Landroid/graphics/Bitmap;)V

    .line 526
    iput v3, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTextureState:I

    .line 527
    iget-object v1, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTextureOffset:Lcom/google/android/opengl/common/Float2;

    iput p2, v1, Lcom/google/android/opengl/common/Float2;->x:F

    .line 528
    iget-object v1, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTextureOffset:Lcom/google/android/opengl/common/Float2;

    iput p3, v1, Lcom/google/android/opengl/common/Float2;->y:F

    .line 529
    iget-object v1, v0, Lcom/google/android/opengl/carousel/Card;->mDetailLineOffset:Lcom/google/android/opengl/common/Float2;

    iput p4, v1, Lcom/google/android/opengl/common/Float2;->x:F

    .line 530
    iget-object v1, v0, Lcom/google/android/opengl/carousel/Card;->mDetailLineOffset:Lcom/google/android/opengl/common/Float2;

    iput p5, v1, Lcom/google/android/opengl/common/Float2;->y:F

    .line 534
    :goto_41
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    invoke-virtual {v1, v3, p6}, Lcom/google/android/opengl/carousel/CarouselSetting;->recycleIfRequired(ILandroid/graphics/Bitmap;)Z

    goto :goto_13

    .line 532
    :cond_49
    iput v2, v0, Lcom/google/android/opengl/carousel/Card;->mTextureState:I

    goto :goto_41
.end method

.method public setFocusedItem(I)V
    .registers 2
    .parameter "focusedItem"

    .prologue
    .line 1497
    iput p1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mFocusedItem:I

    .line 1498
    return-void
.end method

.method public setGeometry(ILcom/google/android/opengl/carousel/Mesh;)V
    .registers 3
    .parameter "n"
    .parameter "geometry"

    .prologue
    .line 544
    return-void
.end method

.method public setMatrixForItem(I[F)V
    .registers 7
    .parameter "n"
    .parameter "matrix"

    .prologue
    const/4 v3, 0x0

    .line 553
    if-ltz p1, :cond_9

    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v0

    if-lt p1, v0, :cond_35

    .line 554
    :cond_9
    const-string v0, "CarouselScene"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The index n is invalid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should be in range [0, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    :cond_35
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/Card;->mClientMatrix:[F

    array-length v1, p2

    invoke-static {p2, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 559
    return-void
.end method

.method public setMixAndFade(FFFF)V
    .registers 6
    .parameter "mix0"
    .parameter "mix1"
    .parameter "fade0"
    .parameter "fade1"

    .prologue
    .line 562
    iput p1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMix0:F

    .line 563
    iput p2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMix1:F

    .line 564
    iput p3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mFade0:F

    .line 565
    iput p4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mFade1:F

    .line 566
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mMixAndFadeEnabled:Z

    .line 567
    return-void
.end method

.method public setTexture(ILandroid/graphics/Bitmap;)V
    .registers 7
    .parameter "n"
    .parameter "bitmap"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 463
    if-ltz p1, :cond_a

    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v1

    if-lt p1, v1, :cond_14

    .line 464
    :cond_a
    if-eqz p2, :cond_13

    .line 465
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    invoke-virtual {v1, v3, p2}, Lcom/google/android/opengl/carousel/CarouselSetting;->recycleIfRequired(ILandroid/graphics/Bitmap;)Z

    .line 483
    :cond_13
    :goto_13
    return-void

    .line 470
    :cond_14
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    .line 471
    .local v0, card:Lcom/google/android/opengl/carousel/Card;
    if-nez p2, :cond_21

    .line 472
    iput v2, v0, Lcom/google/android/opengl/carousel/Card;->mTextureState:I

    goto :goto_13

    .line 476
    :cond_21
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->haveEGLContext()Z

    move-result v1

    if-eqz v1, :cond_39

    .line 477
    iget-object v1, v0, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    aget-object v1, v1, v2

    invoke-direct {p0, v1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->updateTexture(Lcom/google/android/opengl/carousel/RequestableTexture;Landroid/graphics/Bitmap;)V

    .line 478
    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/opengl/carousel/Card;->mTextureState:I

    .line 482
    :goto_31
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    invoke-virtual {v1, v3, p2}, Lcom/google/android/opengl/carousel/CarouselSetting;->recycleIfRequired(ILandroid/graphics/Bitmap;)Z

    goto :goto_13

    .line 480
    :cond_39
    iput v2, v0, Lcom/google/android/opengl/carousel/Card;->mTextureState:I

    goto :goto_31
.end method

.method shuffle([I)V
    .registers 12
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x1

    .line 1435
    array-length v4, p1

    .line 1436
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v5

    .line 1440
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1442
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    move v2, v3

    .line 1444
    :goto_11
    if-ge v2, v4, :cond_5e

    .line 1445
    aget v0, p1, v2

    .line 1447
    if-ge v0, v5, :cond_19

    if-ge v0, v1, :cond_41

    .line 1450
    :cond_19
    const-string v7, "CarouselScene"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "In shuffle, card index maybe wrong, oldIndex: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " oldCount: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1451
    const-string v0, "comeFrom:+ "

    invoke-direct {p0, v0, p1}, Lcom/google/android/opengl/carousel/CarouselScene;->describeShuffle(Ljava/lang/String;[I)V

    move v0, v1

    .line 1456
    :cond_41
    if-ne v0, v1, :cond_4f

    .line 1457
    new-instance v0, Lcom/google/android/opengl/carousel/Card;

    invoke-direct {v0, p0, v2}, Lcom/google/android/opengl/carousel/Card;-><init>(Lcom/google/android/opengl/carousel/CarouselScene;I)V

    .line 1464
    :goto_48
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1444
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_11

    .line 1459
    :cond_4f
    iget-object v7, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    .line 1460
    iget v7, v0, Lcom/google/android/opengl/carousel/Card;->mId:I

    iput v7, v0, Lcom/google/android/opengl/carousel/Card;->mOldId:I

    .line 1461
    iput v2, v0, Lcom/google/android/opengl/carousel/Card;->mId:I

    goto :goto_48

    .line 1468
    :cond_5e
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_64
    :goto_64
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    .line 1469
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_64

    .line 1470
    invoke-virtual {v0, v3}, Lcom/google/android/opengl/carousel/Card;->initCardTexture(Z)V

    goto :goto_64

    .line 1473
    :cond_7a
    iput-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    .line 1474
    return-void
.end method

.method updateCarouselRotationAngle()V
    .registers 2

    .prologue
    .line 302
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselRotationAngle:F

    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->carouselRotationAngleToRadians(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    .line 305
    return-void
.end method
