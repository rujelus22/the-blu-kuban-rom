.class Ljackpal/androidterm/emulatorview/TermKeyListener;
.super Ljava/lang/Object;
.source "TermKeyListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;
    }
.end annotation


# static fields
.field public static final KEYCODE_0:I = 0x7

.field public static final KEYCODE_1:I = 0x8

.field public static final KEYCODE_2:I = 0x9

.field public static final KEYCODE_3:I = 0xa

.field public static final KEYCODE_4:I = 0xb

.field public static final KEYCODE_5:I = 0xc

.field public static final KEYCODE_6:I = 0xd

.field public static final KEYCODE_7:I = 0xe

.field public static final KEYCODE_8:I = 0xf

.field public static final KEYCODE_9:I = 0x10

.field public static final KEYCODE_A:I = 0x1d

.field public static final KEYCODE_ALT_LEFT:I = 0x39

.field public static final KEYCODE_ALT_RIGHT:I = 0x3a

.field public static final KEYCODE_APOSTROPHE:I = 0x4b

.field public static final KEYCODE_AT:I = 0x4d

.field public static final KEYCODE_AVR_INPUT:I = 0xb6

.field public static final KEYCODE_AVR_POWER:I = 0xb5

.field public static final KEYCODE_B:I = 0x1e

.field public static final KEYCODE_BACK:I = 0x4

.field public static final KEYCODE_BACKSLASH:I = 0x49

.field public static final KEYCODE_BOOKMARK:I = 0xae

.field public static final KEYCODE_BREAK:I = 0x79

.field public static final KEYCODE_BUTTON_A:I = 0x60

.field public static final KEYCODE_BUTTON_B:I = 0x61

.field public static final KEYCODE_BUTTON_C:I = 0x62

.field public static final KEYCODE_BUTTON_L1:I = 0x66

.field public static final KEYCODE_BUTTON_L2:I = 0x68

.field public static final KEYCODE_BUTTON_MODE:I = 0x6e

.field public static final KEYCODE_BUTTON_R1:I = 0x67

.field public static final KEYCODE_BUTTON_R2:I = 0x69

.field public static final KEYCODE_BUTTON_SELECT:I = 0x6d

.field public static final KEYCODE_BUTTON_START:I = 0x6c

.field public static final KEYCODE_BUTTON_THUMBL:I = 0x6a

.field public static final KEYCODE_BUTTON_THUMBR:I = 0x6b

.field public static final KEYCODE_BUTTON_X:I = 0x63

.field public static final KEYCODE_BUTTON_Y:I = 0x64

.field public static final KEYCODE_BUTTON_Z:I = 0x65

.field public static final KEYCODE_C:I = 0x1f

.field public static final KEYCODE_CALL:I = 0x5

.field public static final KEYCODE_CAMERA:I = 0x1b

.field public static final KEYCODE_CAPS_LOCK:I = 0x73

.field public static final KEYCODE_CAPTIONS:I = 0xaf

.field public static final KEYCODE_CHANNEL_DOWN:I = 0xa7

.field public static final KEYCODE_CHANNEL_UP:I = 0xa6

.field public static final KEYCODE_CLEAR:I = 0x1c

.field public static final KEYCODE_COMMA:I = 0x37

.field public static final KEYCODE_CTRL_LEFT:I = 0x71

.field public static final KEYCODE_CTRL_RIGHT:I = 0x72

.field public static final KEYCODE_D:I = 0x20

.field public static final KEYCODE_DEL:I = 0x43

.field public static final KEYCODE_DPAD_CENTER:I = 0x17

.field public static final KEYCODE_DPAD_DOWN:I = 0x14

.field public static final KEYCODE_DPAD_LEFT:I = 0x15

.field public static final KEYCODE_DPAD_RIGHT:I = 0x16

.field public static final KEYCODE_DPAD_UP:I = 0x13

.field public static final KEYCODE_DVR:I = 0xad

.field public static final KEYCODE_E:I = 0x21

.field public static final KEYCODE_ENDCALL:I = 0x6

.field public static final KEYCODE_ENTER:I = 0x42

.field public static final KEYCODE_ENVELOPE:I = 0x41

.field public static final KEYCODE_EQUALS:I = 0x46

.field public static final KEYCODE_ESCAPE:I = 0x6f

.field public static final KEYCODE_EXPLORER:I = 0x40

.field public static final KEYCODE_F:I = 0x22

.field public static final KEYCODE_F1:I = 0x83

.field public static final KEYCODE_F10:I = 0x8c

.field public static final KEYCODE_F11:I = 0x8d

.field public static final KEYCODE_F12:I = 0x8e

.field public static final KEYCODE_F2:I = 0x84

.field public static final KEYCODE_F3:I = 0x85

.field public static final KEYCODE_F4:I = 0x86

.field public static final KEYCODE_F5:I = 0x87

.field public static final KEYCODE_F6:I = 0x88

.field public static final KEYCODE_F7:I = 0x89

.field public static final KEYCODE_F8:I = 0x8a

.field public static final KEYCODE_F9:I = 0x8b

.field public static final KEYCODE_FOCUS:I = 0x50

.field public static final KEYCODE_FORWARD:I = 0x7d

.field public static final KEYCODE_FORWARD_DEL:I = 0x70

.field public static final KEYCODE_FUNCTION:I = 0x77

.field public static final KEYCODE_G:I = 0x23

.field public static final KEYCODE_GRAVE:I = 0x44

.field public static final KEYCODE_GUIDE:I = 0xac

.field public static final KEYCODE_H:I = 0x24

.field public static final KEYCODE_HEADSETHOOK:I = 0x4f

.field public static final KEYCODE_HOME:I = 0x3

.field public static final KEYCODE_I:I = 0x25

.field public static final KEYCODE_INFO:I = 0xa5

.field public static final KEYCODE_INSERT:I = 0x7c

.field public static final KEYCODE_J:I = 0x26

.field public static final KEYCODE_K:I = 0x27

.field public static final KEYCODE_L:I = 0x28

.field public static final KEYCODE_LEFT_BRACKET:I = 0x47

.field public static final KEYCODE_M:I = 0x29

.field public static final KEYCODE_MEDIA_CLOSE:I = 0x80

.field public static final KEYCODE_MEDIA_EJECT:I = 0x81

.field public static final KEYCODE_MEDIA_FAST_FORWARD:I = 0x5a

.field public static final KEYCODE_MEDIA_NEXT:I = 0x57

.field public static final KEYCODE_MEDIA_PAUSE:I = 0x7f

.field public static final KEYCODE_MEDIA_PLAY:I = 0x7e

.field public static final KEYCODE_MEDIA_PLAY_PAUSE:I = 0x55

.field public static final KEYCODE_MEDIA_PREVIOUS:I = 0x58

.field public static final KEYCODE_MEDIA_RECORD:I = 0x82

.field public static final KEYCODE_MEDIA_REWIND:I = 0x59

.field public static final KEYCODE_MEDIA_STOP:I = 0x56

.field public static final KEYCODE_MENU:I = 0x52

.field public static final KEYCODE_META_LEFT:I = 0x75

.field public static final KEYCODE_META_RIGHT:I = 0x76

.field public static final KEYCODE_MINUS:I = 0x45

.field public static final KEYCODE_MOVE_END:I = 0x7b

.field public static final KEYCODE_MOVE_HOME:I = 0x7a

.field public static final KEYCODE_MUTE:I = 0x5b

.field public static final KEYCODE_N:I = 0x2a

.field public static final KEYCODE_NOTIFICATION:I = 0x53

.field public static final KEYCODE_NUM:I = 0x4e

.field public static final KEYCODE_NUMPAD_0:I = 0x90

.field public static final KEYCODE_NUMPAD_1:I = 0x91

.field public static final KEYCODE_NUMPAD_2:I = 0x92

.field public static final KEYCODE_NUMPAD_3:I = 0x93

.field public static final KEYCODE_NUMPAD_4:I = 0x94

.field public static final KEYCODE_NUMPAD_5:I = 0x95

.field public static final KEYCODE_NUMPAD_6:I = 0x96

.field public static final KEYCODE_NUMPAD_7:I = 0x97

.field public static final KEYCODE_NUMPAD_8:I = 0x98

.field public static final KEYCODE_NUMPAD_9:I = 0x99

.field public static final KEYCODE_NUMPAD_ADD:I = 0x9d

.field public static final KEYCODE_NUMPAD_COMMA:I = 0x9f

.field public static final KEYCODE_NUMPAD_DIVIDE:I = 0x9a

.field public static final KEYCODE_NUMPAD_DOT:I = 0x9e

.field public static final KEYCODE_NUMPAD_ENTER:I = 0xa0

.field public static final KEYCODE_NUMPAD_EQUALS:I = 0xa1

.field public static final KEYCODE_NUMPAD_LEFT_PAREN:I = 0xa2

.field public static final KEYCODE_NUMPAD_MULTIPLY:I = 0x9b

.field public static final KEYCODE_NUMPAD_RIGHT_PAREN:I = 0xa3

.field public static final KEYCODE_NUMPAD_SUBTRACT:I = 0x9c

.field public static final KEYCODE_NUM_LOCK:I = 0x8f

.field public static final KEYCODE_O:I = 0x2b

.field public static final KEYCODE_OFFSET:I = 0xa00000

.field public static final KEYCODE_P:I = 0x2c

.field public static final KEYCODE_PAGE_DOWN:I = 0x5d

.field public static final KEYCODE_PAGE_UP:I = 0x5c

.field public static final KEYCODE_PERIOD:I = 0x38

.field public static final KEYCODE_PICTSYMBOLS:I = 0x5e

.field public static final KEYCODE_PLUS:I = 0x51

.field public static final KEYCODE_POUND:I = 0x12

.field public static final KEYCODE_POWER:I = 0x1a

.field public static final KEYCODE_PROG_BLUE:I = 0xba

.field public static final KEYCODE_PROG_GREEN:I = 0xb8

.field public static final KEYCODE_PROG_RED:I = 0xb7

.field public static final KEYCODE_PROG_YELLOW:I = 0xb9

.field public static final KEYCODE_Q:I = 0x2d

.field public static final KEYCODE_R:I = 0x2e

.field public static final KEYCODE_RIGHT_BRACKET:I = 0x48

.field public static final KEYCODE_S:I = 0x2f

.field public static final KEYCODE_SCROLL_LOCK:I = 0x74

.field public static final KEYCODE_SEARCH:I = 0x54

.field public static final KEYCODE_SEMICOLON:I = 0x4a

.field public static final KEYCODE_SETTINGS:I = 0xb0

.field public static final KEYCODE_SHIFT_LEFT:I = 0x3b

.field public static final KEYCODE_SHIFT_RIGHT:I = 0x3c

.field public static final KEYCODE_SLASH:I = 0x4c

.field public static final KEYCODE_SOFT_LEFT:I = 0x1

.field public static final KEYCODE_SOFT_RIGHT:I = 0x2

.field public static final KEYCODE_SPACE:I = 0x3e

.field public static final KEYCODE_STAR:I = 0x11

.field public static final KEYCODE_STB_INPUT:I = 0xb4

.field public static final KEYCODE_STB_POWER:I = 0xb3

.field public static final KEYCODE_SWITCH_CHARSET:I = 0x5f

.field public static final KEYCODE_SYM:I = 0x3f

.field public static final KEYCODE_SYSRQ:I = 0x78

.field public static final KEYCODE_T:I = 0x30

.field public static final KEYCODE_TAB:I = 0x3d

.field public static final KEYCODE_TV:I = 0xaa

.field public static final KEYCODE_TV_INPUT:I = 0xb2

.field public static final KEYCODE_TV_POWER:I = 0xb1

.field public static final KEYCODE_U:I = 0x31

.field public static final KEYCODE_UNKNOWN:I = 0x0

.field public static final KEYCODE_V:I = 0x32

.field public static final KEYCODE_VOLUME_DOWN:I = 0x19

.field public static final KEYCODE_VOLUME_MUTE:I = 0xa4

.field public static final KEYCODE_VOLUME_UP:I = 0x18

.field public static final KEYCODE_W:I = 0x33

.field public static final KEYCODE_WINDOW:I = 0xab

.field public static final KEYCODE_X:I = 0x34

.field public static final KEYCODE_Y:I = 0x35

.field public static final KEYCODE_Z:I = 0x36

.field public static final KEYCODE_ZOOM_IN:I = 0xa8

.field public static final KEYCODE_ZOOM_OUT:I = 0xa9

.field private static final LAST_KEYCODE:I = 0xba

.field private static final META_ALT_ON:I = 0x2

.field private static final META_CAPS_LOCK_ON:I = 0x100000

.field private static final META_CTRL_MASK:I = 0x7000

.field private static final META_CTRL_ON:I = 0x1000

.field private static final META_META_MASK:I = 0x70000

.field private static final META_META_ON:I = 0x10000

.field private static final META_SHIFT_ON:I = 0x1


# instance fields
.field private SUPPORT_8_BIT_META:Z

.field private mAltKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

.field private mAltSendsEsc:Z

.field private mAppKeyCodes:[Ljava/lang/String;

.field private mBackKeyCode:I

.field private mCapKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

.field private mControlKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

.field private mFnKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

.field private mKeyCodes:[Ljava/lang/String;

.field private mTermSession:Ljackpal/androidterm/emulatorview/TermSession;


# direct methods
.method public constructor <init>(Ljackpal/androidterm/emulatorview/TermSession;)V
    .registers 4
    .parameter "termSession"

    .prologue
    const/16 v1, 0x100

    .line 680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->SUPPORT_8_BIT_META:Z

    .line 500
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    .line 501
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    .line 660
    new-instance v0, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;-><init>(Ljackpal/androidterm/emulatorview/TermKeyListener;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAltKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    .line 662
    new-instance v0, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;-><init>(Ljackpal/androidterm/emulatorview/TermKeyListener;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mCapKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    .line 664
    new-instance v0, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;-><init>(Ljackpal/androidterm/emulatorview/TermKeyListener;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mControlKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    .line 666
    new-instance v0, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;-><init>(Ljackpal/androidterm/emulatorview/TermKeyListener;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mFnKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    .line 681
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mTermSession:Ljackpal/androidterm/emulatorview/TermSession;

    .line 682
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TermKeyListener;->initKeyCodes()V

    .line 683
    return-void
.end method

.method private initKeyCodes()V
    .registers 9

    .prologue
    const/16 v7, 0x90

    const/16 v6, 0x16

    const/16 v5, 0x15

    const/16 v4, 0x14

    const/16 v3, 0x13

    .line 504
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x17

    const-string v2, "\r"

    aput-object v2, v0, v1

    .line 505
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const-string v1, "\u001b[A"

    aput-object v1, v0, v3

    .line 506
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const-string v1, "\u001b[B"

    aput-object v1, v0, v4

    .line 507
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const-string v1, "\u001b[C"

    aput-object v1, v0, v6

    .line 508
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const-string v1, "\u001b[D"

    aput-object v1, v0, v5

    .line 509
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x83

    const-string v2, "\u001b[OP"

    aput-object v2, v0, v1

    .line 510
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x84

    const-string v2, "\u001b[OQ"

    aput-object v2, v0, v1

    .line 511
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x85

    const-string v2, "\u001b[OR"

    aput-object v2, v0, v1

    .line 512
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x86

    const-string v2, "\u001b[OS"

    aput-object v2, v0, v1

    .line 513
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x87

    const-string v2, "\u001b[15~"

    aput-object v2, v0, v1

    .line 514
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x88

    const-string v2, "\u001b[17~"

    aput-object v2, v0, v1

    .line 515
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x89

    const-string v2, "\u001b[18~"

    aput-object v2, v0, v1

    .line 516
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x8a

    const-string v2, "\u001b[19~"

    aput-object v2, v0, v1

    .line 517
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x8b

    const-string v2, "\u001b[20~"

    aput-object v2, v0, v1

    .line 518
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x8c

    const-string v2, "\u001b[21~"

    aput-object v2, v0, v1

    .line 519
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x8d

    const-string v2, "\u001b[23~"

    aput-object v2, v0, v1

    .line 520
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x8e

    const-string v2, "\u001b[24~"

    aput-object v2, v0, v1

    .line 521
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x78

    const-string v2, "\u001b[32~"

    aput-object v2, v0, v1

    .line 523
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x79

    const-string v2, "\u001b[34~"

    aput-object v2, v0, v1

    .line 525
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x3d

    const-string v2, "\t"

    aput-object v2, v0, v1

    .line 526
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x42

    const-string v2, "\r"

    aput-object v2, v0, v1

    .line 527
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x6f

    const-string v2, "\u001b"

    aput-object v2, v0, v1

    .line 529
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x7c

    const-string v2, "\u001b[2~"

    aput-object v2, v0, v1

    .line 530
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x70

    const-string v2, "\u001b[3~"

    aput-object v2, v0, v1

    .line 531
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x7a

    const-string v2, "\u001b[1~"

    aput-object v2, v0, v1

    .line 532
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x7b

    const-string v2, "\u001b[4~"

    aput-object v2, v0, v1

    .line 533
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x5c

    const-string v2, "\u001b[5~"

    aput-object v2, v0, v1

    .line 534
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x5d

    const-string v2, "\u001b[6~"

    aput-object v2, v0, v1

    .line 535
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x43

    const-string v2, "\u007f"

    aput-object v2, v0, v1

    .line 536
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x8f

    const-string v2, "\u001bOP"

    aput-object v2, v0, v1

    .line 537
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x9a

    const-string v2, "/"

    aput-object v2, v0, v1

    .line 538
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x9b

    const-string v2, "*"

    aput-object v2, v0, v1

    .line 539
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x9c

    const-string v2, "-"

    aput-object v2, v0, v1

    .line 540
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x9d

    const-string v2, "+"

    aput-object v2, v0, v1

    .line 541
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0xa0

    const-string v2, "\r"

    aput-object v2, v0, v1

    .line 542
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0xa1

    const-string v2, "="

    aput-object v2, v0, v1

    .line 543
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x9e

    const-string v2, "."

    aput-object v2, v0, v1

    .line 544
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x9f

    const-string v2, ","

    aput-object v2, v0, v1

    .line 545
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const-string v1, "0"

    aput-object v1, v0, v7

    .line 546
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x91

    const-string v2, "1"

    aput-object v2, v0, v1

    .line 547
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x92

    const-string v2, "2"

    aput-object v2, v0, v1

    .line 548
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x93

    const-string v2, "3"

    aput-object v2, v0, v1

    .line 549
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x94

    const-string v2, "4"

    aput-object v2, v0, v1

    .line 550
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x95

    const-string v2, "5"

    aput-object v2, v0, v1

    .line 551
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x96

    const-string v2, "6"

    aput-object v2, v0, v1

    .line 552
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x97

    const-string v2, "7"

    aput-object v2, v0, v1

    .line 553
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x98

    const-string v2, "8"

    aput-object v2, v0, v1

    .line 554
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x99

    const-string v2, "9"

    aput-object v2, v0, v1

    .line 556
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const-string v1, "\u001bOA"

    aput-object v1, v0, v3

    .line 557
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const-string v1, "\u001bOB"

    aput-object v1, v0, v4

    .line 558
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const-string v1, "\u001bOC"

    aput-object v1, v0, v6

    .line 559
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const-string v1, "\u001bOD"

    aput-object v1, v0, v5

    .line 560
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x9a

    const-string v2, "\u001bOo"

    aput-object v2, v0, v1

    .line 561
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x9b

    const-string v2, "\u001bOj"

    aput-object v2, v0, v1

    .line 562
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x9c

    const-string v2, "\u001bOm"

    aput-object v2, v0, v1

    .line 563
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x9d

    const-string v2, "\u001bOk"

    aput-object v2, v0, v1

    .line 564
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0xa0

    const-string v2, "\u001bOM"

    aput-object v2, v0, v1

    .line 565
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0xa1

    const-string v2, "\u001bOX"

    aput-object v2, v0, v1

    .line 566
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x9e

    const-string v2, "\u001bOn"

    aput-object v2, v0, v1

    .line 567
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x9f

    const-string v2, "\u001bOl"

    aput-object v2, v0, v1

    .line 568
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const-string v1, "\u001bOp"

    aput-object v1, v0, v7

    .line 569
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x91

    const-string v2, "\u001bOq"

    aput-object v2, v0, v1

    .line 570
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x92

    const-string v2, "\u001bOr"

    aput-object v2, v0, v1

    .line 571
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x93

    const-string v2, "\u001bOs"

    aput-object v2, v0, v1

    .line 572
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x94

    const-string v2, "\u001bOt"

    aput-object v2, v0, v1

    .line 573
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x95

    const-string v2, "\u001bOu"

    aput-object v2, v0, v1

    .line 574
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x96

    const-string v2, "\u001bOv"

    aput-object v2, v0, v1

    .line 575
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x97

    const-string v2, "\u001bOw"

    aput-object v2, v0, v1

    .line 576
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x98

    const-string v2, "\u001bOx"

    aput-object v2, v0, v1

    .line 577
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    const/16 v1, 0x99

    const-string v2, "\u001bOy"

    aput-object v2, v0, v1

    .line 578
    return-void
.end method

.method static isEventFromToggleDevice(Landroid/view/KeyEvent;)Z
    .registers 5
    .parameter "event"

    .prologue
    const/4 v1, 0x1

    .line 892
    sget v2, Ljackpal/androidterm/emulatorview/compat/AndroidCompat;->SDK:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_8

    .line 897
    :cond_7
    :goto_7
    return v1

    .line 895
    :cond_8
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v2

    invoke-static {v2}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object v2

    invoke-static {v2}, Ljackpal/androidterm/emulatorview/compat/KeyCharacterMapCompat;->wrap(Ljava/lang/Object;)Ljackpal/androidterm/emulatorview/compat/KeyCharacterMapCompat;

    move-result-object v0

    .line 897
    .local v0, kcm:Ljackpal/androidterm/emulatorview/compat/KeyCharacterMapCompat;
    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/compat/KeyCharacterMapCompat;->getModifierBehaviour()I

    move-result v2

    if-eq v2, v1, :cond_7

    const/4 v1, 0x0

    goto :goto_7
.end method


# virtual methods
.method public getAltSendsEsc()Z
    .registers 2

    .prologue
    .line 951
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAltSendsEsc:Z

    return v0
.end method

.method public handleControlKey(Z)V
    .registers 3
    .parameter "down"

    .prologue
    .line 694
    if-eqz p1, :cond_8

    .line 695
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mControlKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->onPress()V

    .line 699
    :goto_7
    return-void

    .line 697
    :cond_8
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mControlKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->onRelease()V

    goto :goto_7
.end method

.method public handleFnKey(Z)V
    .registers 3
    .parameter "down"

    .prologue
    .line 702
    if-eqz p1, :cond_8

    .line 703
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mFnKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->onPress()V

    .line 707
    :goto_7
    return-void

    .line 705
    :cond_8
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mFnKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->onRelease()V

    goto :goto_7
.end method

.method public handleKeyCode(IZ)Z
    .registers 5
    .parameter "keyCode"
    .parameter "appMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 902
    if-ltz p1, :cond_1d

    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    array-length v1, v1

    if-ge p1, v1, :cond_1d

    .line 903
    const/4 v0, 0x0

    .line 904
    .local v0, code:Ljava/lang/String;
    if-eqz p2, :cond_e

    .line 905
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAppKeyCodes:[Ljava/lang/String;

    aget-object v0, v1, p1

    .line 907
    :cond_e
    if-nez v0, :cond_14

    .line 908
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mKeyCodes:[Ljava/lang/String;

    aget-object v0, v1, p1

    .line 910
    :cond_14
    if-eqz v0, :cond_1d

    .line 911
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mTermSession:Ljackpal/androidterm/emulatorview/TermSession;

    invoke-virtual {v1, v0}, Ljackpal/androidterm/emulatorview/TermSession;->write(Ljava/lang/String;)V

    .line 912
    const/4 v1, 0x1

    .line 915
    .end local v0           #code:Ljava/lang/String;
    :goto_1c
    return v1

    :cond_1d
    const/4 v1, 0x0

    goto :goto_1c
.end method

.method public isAltActive()Z
    .registers 2

    .prologue
    .line 955
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAltKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->isActive()Z

    move-result v0

    return v0
.end method

.method public keyDown(ILandroid/view/KeyEvent;ZZ)V
    .registers 19
    .parameter "keyCode"
    .parameter "event"
    .parameter "appMode"
    .parameter "allowToggle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 797
    move/from16 v0, p3

    invoke-virtual {p0, p1, v0}, Ljackpal/androidterm/emulatorview/TermKeyListener;->handleKeyCode(IZ)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 889
    :cond_8
    :goto_8
    :sswitch_8
    return-void

    .line 800
    :cond_9
    const/4 v8, -0x1

    .line 801
    .local v8, result:I
    const/4 v1, 0x0

    .line 802
    .local v1, chordedCtrl:Z
    const/4 v9, 0x0

    .line 803
    .local v9, setHighBit:Z
    sparse-switch p1, :sswitch_data_e2

    .line 832
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v7

    .line 833
    .local v7, metaState:I
    and-int/lit16 v10, v7, 0x1000

    if-eqz v10, :cond_b2

    const/4 v1, 0x1

    .line 834
    :goto_18
    if-eqz p4, :cond_b5

    iget-object v10, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mCapKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v10}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->isActive()Z

    move-result v10

    if-eqz v10, :cond_b5

    const/4 v3, 0x1

    .line 836
    .local v3, effectiveCaps:Z
    :goto_23
    if-eqz p4, :cond_b8

    iget-object v10, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAltKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v10}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->isActive()Z

    move-result v10

    if-eqz v10, :cond_b8

    const/4 v2, 0x1

    .line 837
    .local v2, effectiveAlt:Z
    :goto_2e
    and-int/lit16 v6, v7, -0x7001

    .line 838
    .local v6, effectiveMetaState:I
    if-eqz v3, :cond_34

    .line 839
    or-int/lit8 v6, v6, 0x1

    .line 841
    :cond_34
    if-nez p4, :cond_3b

    and-int/lit8 v10, v6, 0x2

    if-eqz v10, :cond_3b

    .line 842
    const/4 v2, 0x1

    .line 844
    :cond_3b
    if-eqz v2, :cond_52

    .line 845
    iget-boolean v10, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAltSendsEsc:Z

    if-eqz v10, :cond_bb

    .line 846
    iget-object v10, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mTermSession:Ljackpal/androidterm/emulatorview/TermSession;

    const/4 v11, 0x1

    new-array v11, v11, [B

    const/4 v12, 0x0

    const/16 v13, 0x1b

    aput-byte v13, v11, v12

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-virtual {v10, v11, v12, v13}, Ljackpal/androidterm/emulatorview/TermSession;->write([BII)V

    .line 847
    and-int/lit8 v6, v6, -0x33

    .line 860
    :cond_52
    :goto_52
    const/high16 v10, 0x1

    and-int/2addr v10, v7

    if-eqz v10, :cond_6e

    .line 861
    iget-boolean v10, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAltSendsEsc:Z

    if-eqz v10, :cond_c6

    .line 862
    iget-object v10, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mTermSession:Ljackpal/androidterm/emulatorview/TermSession;

    const/4 v11, 0x1

    new-array v11, v11, [B

    const/4 v12, 0x0

    const/16 v13, 0x1b

    aput-byte v13, v11, v12

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-virtual {v10, v11, v12, v13}, Ljackpal/androidterm/emulatorview/TermSession;->write([BII)V

    .line 863
    const v10, -0x70001

    and-int/2addr v6, v10

    .line 871
    :cond_6e
    :goto_6e
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/KeyEvent;->getUnicodeChar(I)I

    move-result v8

    .line 876
    .end local v2           #effectiveAlt:Z
    .end local v3           #effectiveCaps:Z
    .end local v6           #effectiveMetaState:I
    .end local v7           #metaState:I
    :cond_74
    :goto_74
    if-nez v1, :cond_80

    if-eqz p4, :cond_d0

    iget-object v10, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mControlKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v10}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->isActive()Z

    move-result v10

    if-eqz v10, :cond_d0

    :cond_80
    const/4 v4, 0x1

    .line 877
    .local v4, effectiveControl:Z
    :goto_81
    if-eqz p4, :cond_d2

    iget-object v10, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mFnKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v10}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->isActive()Z

    move-result v10

    if-eqz v10, :cond_d2

    const/4 v5, 0x1

    .line 879
    .local v5, effectiveFn:Z
    :goto_8c
    invoke-virtual {p0, v4, v5, v8}, Ljackpal/androidterm/emulatorview/TermKeyListener;->mapControlChar(ZZI)I

    move-result v8

    .line 881
    const/high16 v10, 0xa0

    if-lt v8, v10, :cond_d4

    .line 882
    const/high16 v10, 0xa0

    sub-int v10, v8, v10

    move/from16 v0, p3

    invoke-virtual {p0, v10, v0}, Ljackpal/androidterm/emulatorview/TermKeyListener;->handleKeyCode(IZ)Z

    goto/16 :goto_8

    .line 806
    .end local v4           #effectiveControl:Z
    .end local v5           #effectiveFn:Z
    :sswitch_9f
    if-eqz p4, :cond_74

    .line 807
    iget-object v10, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAltKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v10}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->onPress()V

    goto :goto_74

    .line 813
    :sswitch_a7
    if-eqz p4, :cond_74

    .line 814
    iget-object v10, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mCapKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v10}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->onPress()V

    goto :goto_74

    .line 828
    :sswitch_af
    iget v8, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mBackKeyCode:I

    .line 829
    goto :goto_74

    .line 833
    .restart local v7       #metaState:I
    :cond_b2
    const/4 v1, 0x0

    goto/16 :goto_18

    .line 834
    :cond_b5
    const/4 v3, 0x0

    goto/16 :goto_23

    .line 836
    .restart local v3       #effectiveCaps:Z
    :cond_b8
    const/4 v2, 0x0

    goto/16 :goto_2e

    .line 848
    .restart local v2       #effectiveAlt:Z
    .restart local v6       #effectiveMetaState:I
    :cond_bb
    iget-boolean v10, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->SUPPORT_8_BIT_META:Z

    if-eqz v10, :cond_c3

    .line 849
    const/4 v9, 0x1

    .line 850
    and-int/lit8 v6, v6, -0x33

    goto :goto_52

    .line 853
    :cond_c3
    or-int/lit8 v6, v6, 0x2

    goto :goto_52

    .line 865
    :cond_c6
    iget-boolean v10, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->SUPPORT_8_BIT_META:Z

    if-eqz v10, :cond_6e

    .line 866
    const/4 v9, 0x1

    .line 867
    const v10, -0x70001

    and-int/2addr v6, v10

    goto :goto_6e

    .line 876
    .end local v2           #effectiveAlt:Z
    .end local v3           #effectiveCaps:Z
    .end local v6           #effectiveMetaState:I
    .end local v7           #metaState:I
    :cond_d0
    const/4 v4, 0x0

    goto :goto_81

    .line 877
    .restart local v4       #effectiveControl:Z
    :cond_d2
    const/4 v5, 0x0

    goto :goto_8c

    .line 883
    .restart local v5       #effectiveFn:Z
    :cond_d4
    if-ltz v8, :cond_8

    .line 884
    if-eqz v9, :cond_da

    .line 885
    or-int/lit16 v8, v8, 0x80

    .line 887
    :cond_da
    iget-object v10, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mTermSession:Ljackpal/androidterm/emulatorview/TermSession;

    invoke-virtual {v10, v8}, Ljackpal/androidterm/emulatorview/TermSession;->write(I)V

    goto/16 :goto_8

    .line 803
    nop

    :sswitch_data_e2
    .sparse-switch
        0x4 -> :sswitch_af
        0x39 -> :sswitch_9f
        0x3a -> :sswitch_9f
        0x3b -> :sswitch_a7
        0x3c -> :sswitch_a7
        0x71 -> :sswitch_8
        0x72 -> :sswitch_8
        0x73 -> :sswitch_8
    .end sparse-switch
.end method

.method public keyUp(ILandroid/view/KeyEvent;)V
    .registers 5
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 924
    invoke-static {p2}, Ljackpal/androidterm/emulatorview/TermKeyListener;->isEventFromToggleDevice(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 925
    .local v0, allowToggle:Z
    sparse-switch p1, :sswitch_data_18

    .line 948
    :cond_7
    :goto_7
    :sswitch_7
    return-void

    .line 928
    :sswitch_8
    if-eqz v0, :cond_7

    .line 929
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAltKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->onRelease()V

    goto :goto_7

    .line 934
    :sswitch_10
    if-eqz v0, :cond_7

    .line 935
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mCapKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->onRelease()V

    goto :goto_7

    .line 925
    :sswitch_data_18
    .sparse-switch
        0x39 -> :sswitch_8
        0x3a -> :sswitch_8
        0x3b -> :sswitch_10
        0x3c -> :sswitch_10
        0x71 -> :sswitch_7
        0x72 -> :sswitch_7
    .end sparse-switch
.end method

.method public mapControlChar(I)I
    .registers 4
    .parameter "ch"

    .prologue
    .line 710
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mControlKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->isActive()Z

    move-result v0

    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mFnKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->isActive()Z

    move-result v1

    invoke-virtual {p0, v0, v1, p1}, Ljackpal/androidterm/emulatorview/TermKeyListener;->mapControlChar(ZZI)I

    move-result v0

    return v0
.end method

.method public mapControlChar(ZZI)I
    .registers 10
    .parameter "control"
    .parameter "fn"
    .parameter "ch"

    .prologue
    const/16 v5, 0x61

    const/16 v4, 0x41

    const/16 v3, 0x39

    const/16 v2, 0x30

    .line 714
    move v0, p3

    .line 715
    .local v0, result:I
    if-eqz p1, :cond_8e

    .line 717
    if-lt v0, v5, :cond_2e

    const/16 v1, 0x7a

    if-gt v0, v1, :cond_2e

    .line 718
    add-int/lit8 v1, v0, -0x61

    add-int/lit8 v1, v1, 0x1

    int-to-char v0, v1

    .line 779
    :cond_16
    :goto_16
    const/4 v1, -0x1

    if-le v0, v1, :cond_2d

    .line 780
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAltKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->adjustAfterKeypress()V

    .line 781
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mCapKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->adjustAfterKeypress()V

    .line 782
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mControlKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->adjustAfterKeypress()V

    .line 783
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mFnKey:Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;

    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/TermKeyListener$ModifierKey;->adjustAfterKeypress()V

    .line 786
    :cond_2d
    return v0

    .line 719
    :cond_2e
    if-lt v0, v4, :cond_3a

    const/16 v1, 0x5a

    if-gt v0, v1, :cond_3a

    .line 720
    add-int/lit8 v1, v0, -0x41

    add-int/lit8 v1, v1, 0x1

    int-to-char v0, v1

    goto :goto_16

    .line 721
    :cond_3a
    const/16 v1, 0x20

    if-eq v0, v1, :cond_42

    const/16 v1, 0x32

    if-ne v0, v1, :cond_44

    .line 722
    :cond_42
    const/4 v0, 0x0

    goto :goto_16

    .line 723
    :cond_44
    const/16 v1, 0x5b

    if-eq v0, v1, :cond_4c

    const/16 v1, 0x33

    if-ne v0, v1, :cond_4f

    .line 724
    :cond_4c
    const/16 v0, 0x1b

    goto :goto_16

    .line 725
    :cond_4f
    const/16 v1, 0x5c

    if-eq v0, v1, :cond_57

    const/16 v1, 0x34

    if-ne v0, v1, :cond_5a

    .line 726
    :cond_57
    const/16 v0, 0x1c

    goto :goto_16

    .line 727
    :cond_5a
    const/16 v1, 0x5d

    if-eq v0, v1, :cond_62

    const/16 v1, 0x35

    if-ne v0, v1, :cond_65

    .line 728
    :cond_62
    const/16 v0, 0x1d

    goto :goto_16

    .line 729
    :cond_65
    const/16 v1, 0x5e

    if-eq v0, v1, :cond_6d

    const/16 v1, 0x36

    if-ne v0, v1, :cond_70

    .line 730
    :cond_6d
    const/16 v0, 0x1e

    goto :goto_16

    .line 731
    :cond_70
    const/16 v1, 0x5f

    if-eq v0, v1, :cond_78

    const/16 v1, 0x37

    if-ne v0, v1, :cond_7b

    .line 732
    :cond_78
    const/16 v0, 0x1f

    goto :goto_16

    .line 733
    :cond_7b
    const/16 v1, 0x38

    if-ne v0, v1, :cond_82

    .line 734
    const/16 v0, 0x7f

    goto :goto_16

    .line 735
    :cond_82
    if-ne v0, v3, :cond_88

    .line 736
    const v0, 0xa0008d

    goto :goto_16

    .line 737
    :cond_88
    if-ne v0, v2, :cond_16

    .line 738
    const v0, 0xa0008e

    goto :goto_16

    .line 740
    :cond_8e
    if-eqz p2, :cond_16

    .line 741
    const/16 v1, 0x77

    if-eq v0, v1, :cond_98

    const/16 v1, 0x57

    if-ne v0, v1, :cond_9d

    .line 742
    :cond_98
    const v0, 0xa00013

    goto/16 :goto_16

    .line 743
    :cond_9d
    if-eq v0, v5, :cond_a1

    if-ne v0, v4, :cond_a6

    .line 744
    :cond_a1
    const v0, 0xa00015

    goto/16 :goto_16

    .line 745
    :cond_a6
    const/16 v1, 0x73

    if-eq v0, v1, :cond_ae

    const/16 v1, 0x53

    if-ne v0, v1, :cond_b3

    .line 746
    :cond_ae
    const v0, 0xa00014

    goto/16 :goto_16

    .line 747
    :cond_b3
    const/16 v1, 0x64

    if-eq v0, v1, :cond_bb

    const/16 v1, 0x44

    if-ne v0, v1, :cond_c0

    .line 748
    :cond_bb
    const v0, 0xa00016

    goto/16 :goto_16

    .line 749
    :cond_c0
    const/16 v1, 0x70

    if-eq v0, v1, :cond_c8

    const/16 v1, 0x50

    if-ne v0, v1, :cond_cd

    .line 750
    :cond_c8
    const v0, 0xa0005c

    goto/16 :goto_16

    .line 751
    :cond_cd
    const/16 v1, 0x6e

    if-eq v0, v1, :cond_d5

    const/16 v1, 0x4e

    if-ne v0, v1, :cond_da

    .line 752
    :cond_d5
    const v0, 0xa0005d

    goto/16 :goto_16

    .line 753
    :cond_da
    const/16 v1, 0x74

    if-eq v0, v1, :cond_e2

    const/16 v1, 0x54

    if-ne v0, v1, :cond_e7

    .line 754
    :cond_e2
    const v0, 0xa0003d

    goto/16 :goto_16

    .line 755
    :cond_e7
    const/16 v1, 0x6c

    if-eq v0, v1, :cond_ef

    const/16 v1, 0x4c

    if-ne v0, v1, :cond_f3

    .line 756
    :cond_ef
    const/16 v0, 0x7c

    goto/16 :goto_16

    .line 757
    :cond_f3
    const/16 v1, 0x75

    if-eq v0, v1, :cond_fb

    const/16 v1, 0x55

    if-ne v0, v1, :cond_ff

    .line 758
    :cond_fb
    const/16 v0, 0x5f

    goto/16 :goto_16

    .line 759
    :cond_ff
    const/16 v1, 0x65

    if-eq v0, v1, :cond_107

    const/16 v1, 0x45

    if-ne v0, v1, :cond_10b

    .line 760
    :cond_107
    const/16 v0, 0x1b

    goto/16 :goto_16

    .line 761
    :cond_10b
    const/16 v1, 0x2e

    if-ne v0, v1, :cond_113

    .line 762
    const/16 v0, 0x1c

    goto/16 :goto_16

    .line 763
    :cond_113
    if-le v0, v2, :cond_121

    if-gt v0, v3, :cond_121

    .line 765
    const/high16 v1, 0xa0

    add-int/2addr v1, v0

    add-int/lit16 v1, v1, 0x83

    add-int/lit8 v1, v1, -0x1

    int-to-char v0, v1

    goto/16 :goto_16

    .line 766
    :cond_121
    if-ne v0, v2, :cond_128

    .line 767
    const v0, 0xa0008c

    goto/16 :goto_16

    .line 768
    :cond_128
    const/16 v1, 0x69

    if-eq v0, v1, :cond_130

    const/16 v1, 0x49

    if-ne v0, v1, :cond_135

    .line 769
    :cond_130
    const v0, 0xa0007c

    goto/16 :goto_16

    .line 770
    :cond_135
    const/16 v1, 0x78

    if-eq v0, v1, :cond_13d

    const/16 v1, 0x58

    if-ne v0, v1, :cond_142

    .line 771
    :cond_13d
    const v0, 0xa00070

    goto/16 :goto_16

    .line 772
    :cond_142
    const/16 v1, 0x68

    if-eq v0, v1, :cond_14a

    const/16 v1, 0x48

    if-ne v0, v1, :cond_14f

    .line 773
    :cond_14a
    const v0, 0xa0007a

    goto/16 :goto_16

    .line 774
    :cond_14f
    const/16 v1, 0x66

    if-eq v0, v1, :cond_157

    const/16 v1, 0x46

    if-ne v0, v1, :cond_16

    .line 775
    :cond_157
    const v0, 0xa0007b

    goto/16 :goto_16
.end method

.method public setAltSendsEsc(Z)V
    .registers 2
    .parameter "flag"

    .prologue
    .line 690
    iput-boolean p1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mAltSendsEsc:Z

    .line 691
    return-void
.end method

.method public setBackKeyCharacter(I)V
    .registers 2
    .parameter "code"

    .prologue
    .line 686
    iput p1, p0, Ljackpal/androidterm/emulatorview/TermKeyListener;->mBackKeyCode:I

    .line 687
    return-void
.end method
