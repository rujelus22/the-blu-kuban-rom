.class public Ljackpal/androidterm/emulatorview/EmulatorView;
.super Landroid/view/View;
.source "EmulatorView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# static fields
.field private static final CURSOR_BLINK_PERIOD:I = 0x3e8

.field private static final SELECT_TEXT_OFFSET_Y:I = -0x28

.field private static final sTrapAltAndMeta:Z


# instance fields
.field private final LOG_IME:Z

.field private final LOG_KEY_EVENTS:Z

.field private final TAG:Ljava/lang/String;

.field private mBackKeySendsCharacter:Z

.field private mBackgroundPaint:Landroid/graphics/Paint;

.field private mBlinkCursor:Ljava/lang/Runnable;

.field private mCharacterHeight:I

.field private mCharacterWidth:F

.field private mColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

.field private mColumns:I

.field private mControlKeyCode:I

.field private mCursorBlink:I

.field private mCursorPaint:Landroid/graphics/Paint;

.field private mCursorStyle:I

.field private mCursorVisible:Z

.field private mDeferInit:Z

.field private mDensity:F

.field private mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

.field private mExtGestureListener:Landroid/view/GestureDetector$OnGestureListener;

.field private mFlingRunner:Ljava/lang/Runnable;

.field private mFnKeyCode:I

.field private mForegroundPaint:Landroid/graphics/Paint;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private final mHandler:Landroid/os/Handler;

.field private mImeBuffer:Ljava/lang/String;

.field private mIsActive:Z

.field private mIsControlKeySent:Z

.field private mIsFnKeySent:Z

.field private mIsSelectingText:Z

.field private mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

.field private mKnownSize:Z

.field private mLeftColumn:I

.field private mRows:I

.field private mScaledDensity:F

.field private mScrollRemainder:F

.field private mScroller:Landroid/widget/Scroller;

.field private mSelX1:I

.field private mSelX2:I

.field private mSelXAnchor:I

.field private mSelY1:I

.field private mSelY2:I

.field private mSelYAnchor:I

.field private mTermSession:Ljackpal/androidterm/emulatorview/TermSession;

.field private mTextRenderer:Ljackpal/androidterm/emulatorview/TextRenderer;

.field private mTextSize:I

.field private mTopOfScreenMargin:I

.field private mTopRow:I

.field private mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

.field private mUpdateNotify:Ljackpal/androidterm/emulatorview/UpdateCallback;

.field private mUseCookedIme:Z

.field private mVisibleColumns:I

.field private mVisibleHeight:I

.field private mVisibleWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 184
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Transformer TF101"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Ljackpal/androidterm/emulatorview/EmulatorView;->sTrapAltAndMeta:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 271
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    const-string v0, "EmulatorView"

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->TAG:Ljava/lang/String;

    .line 57
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->LOG_KEY_EVENTS:Z

    .line 58
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->LOG_IME:Z

    .line 67
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mDeferInit:Z

    .line 102
    const/16 v0, 0xa

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextSize:I

    .line 110
    sget-object v0, Ljackpal/androidterm/emulatorview/BaseTextRenderer;->defaultColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorVisible:Z

    .line 155
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsSelectingText:Z

    .line 157
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBackKeySendsCharacter:Z

    .line 160
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsControlKeySent:Z

    .line 161
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsFnKeySent:Z

    .line 167
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelXAnchor:I

    .line 168
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelYAnchor:I

    .line 169
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX1:I

    .line 170
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY1:I

    .line 171
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX2:I

    .line 172
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY2:I

    .line 174
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsActive:Z

    .line 186
    new-instance v0, Ljackpal/androidterm/emulatorview/EmulatorView$1;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/EmulatorView$1;-><init>(Ljackpal/androidterm/emulatorview/EmulatorView;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBlinkCursor:Ljava/lang/Runnable;

    .line 202
    new-instance v0, Ljackpal/androidterm/emulatorview/EmulatorView$2;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/EmulatorView$2;-><init>(Ljackpal/androidterm/emulatorview/EmulatorView;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mFlingRunner:Ljava/lang/Runnable;

    .line 224
    const-string v0, ""

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mImeBuffer:Ljava/lang/String;

    .line 229
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mHandler:Landroid/os/Handler;

    .line 234
    new-instance v0, Ljackpal/androidterm/emulatorview/EmulatorView$3;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/EmulatorView$3;-><init>(Ljackpal/androidterm/emulatorview/EmulatorView;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mUpdateNotify:Ljackpal/androidterm/emulatorview/UpdateCallback;

    .line 272
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/EmulatorView;->commonConstructor(Landroid/content/Context;)V

    .line 273
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 284
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    const-string v0, "EmulatorView"

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->TAG:Ljava/lang/String;

    .line 57
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->LOG_KEY_EVENTS:Z

    .line 58
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->LOG_IME:Z

    .line 67
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mDeferInit:Z

    .line 102
    const/16 v0, 0xa

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextSize:I

    .line 110
    sget-object v0, Ljackpal/androidterm/emulatorview/BaseTextRenderer;->defaultColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorVisible:Z

    .line 155
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsSelectingText:Z

    .line 157
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBackKeySendsCharacter:Z

    .line 160
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsControlKeySent:Z

    .line 161
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsFnKeySent:Z

    .line 167
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelXAnchor:I

    .line 168
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelYAnchor:I

    .line 169
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX1:I

    .line 170
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY1:I

    .line 171
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX2:I

    .line 172
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY2:I

    .line 174
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsActive:Z

    .line 186
    new-instance v0, Ljackpal/androidterm/emulatorview/EmulatorView$1;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/EmulatorView$1;-><init>(Ljackpal/androidterm/emulatorview/EmulatorView;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBlinkCursor:Ljava/lang/Runnable;

    .line 202
    new-instance v0, Ljackpal/androidterm/emulatorview/EmulatorView$2;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/EmulatorView$2;-><init>(Ljackpal/androidterm/emulatorview/EmulatorView;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mFlingRunner:Ljava/lang/Runnable;

    .line 224
    const-string v0, ""

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mImeBuffer:Ljava/lang/String;

    .line 229
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mHandler:Landroid/os/Handler;

    .line 234
    new-instance v0, Ljackpal/androidterm/emulatorview/EmulatorView$3;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/EmulatorView$3;-><init>(Ljackpal/androidterm/emulatorview/EmulatorView;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mUpdateNotify:Ljackpal/androidterm/emulatorview/UpdateCallback;

    .line 285
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/EmulatorView;->commonConstructor(Landroid/content/Context;)V

    .line 286
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljackpal/androidterm/emulatorview/TermSession;Landroid/util/DisplayMetrics;)V
    .registers 7
    .parameter "context"
    .parameter "session"
    .parameter "metrics"

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 257
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 56
    const-string v0, "EmulatorView"

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->TAG:Ljava/lang/String;

    .line 57
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->LOG_KEY_EVENTS:Z

    .line 58
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->LOG_IME:Z

    .line 67
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mDeferInit:Z

    .line 102
    const/16 v0, 0xa

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextSize:I

    .line 110
    sget-object v0, Ljackpal/androidterm/emulatorview/BaseTextRenderer;->defaultColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorVisible:Z

    .line 155
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsSelectingText:Z

    .line 157
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBackKeySendsCharacter:Z

    .line 160
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsControlKeySent:Z

    .line 161
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsFnKeySent:Z

    .line 167
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelXAnchor:I

    .line 168
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelYAnchor:I

    .line 169
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX1:I

    .line 170
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY1:I

    .line 171
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX2:I

    .line 172
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY2:I

    .line 174
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsActive:Z

    .line 186
    new-instance v0, Ljackpal/androidterm/emulatorview/EmulatorView$1;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/EmulatorView$1;-><init>(Ljackpal/androidterm/emulatorview/EmulatorView;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBlinkCursor:Ljava/lang/Runnable;

    .line 202
    new-instance v0, Ljackpal/androidterm/emulatorview/EmulatorView$2;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/EmulatorView$2;-><init>(Ljackpal/androidterm/emulatorview/EmulatorView;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mFlingRunner:Ljava/lang/Runnable;

    .line 224
    const-string v0, ""

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mImeBuffer:Ljava/lang/String;

    .line 229
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mHandler:Landroid/os/Handler;

    .line 234
    new-instance v0, Ljackpal/androidterm/emulatorview/EmulatorView$3;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/EmulatorView$3;-><init>(Ljackpal/androidterm/emulatorview/EmulatorView;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mUpdateNotify:Ljackpal/androidterm/emulatorview/UpdateCallback;

    .line 258
    invoke-virtual {p0, p2}, Ljackpal/androidterm/emulatorview/EmulatorView;->attachSession(Ljackpal/androidterm/emulatorview/TermSession;)V

    .line 259
    invoke-virtual {p0, p3}, Ljackpal/androidterm/emulatorview/EmulatorView;->setDensity(Landroid/util/DisplayMetrics;)V

    .line 260
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/EmulatorView;->commonConstructor(Landroid/content/Context;)V

    .line 261
    return-void
.end method

.method static synthetic access$000(Ljackpal/androidterm/emulatorview/EmulatorView;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorBlink:I

    return v0
.end method

.method static synthetic access$100(Ljackpal/androidterm/emulatorview/EmulatorView;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorVisible:Z

    return v0
.end method

.method static synthetic access$1000(Ljackpal/androidterm/emulatorview/EmulatorView;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 55
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->ensureCursorVisible()V

    return-void
.end method

.method static synthetic access$102(Ljackpal/androidterm/emulatorview/EmulatorView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 55
    iput-boolean p1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorVisible:Z

    return p1
.end method

.method static synthetic access$1100(Ljackpal/androidterm/emulatorview/EmulatorView;)Ljackpal/androidterm/emulatorview/TermKeyListener;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    return-object v0
.end method

.method static synthetic access$1200(Ljackpal/androidterm/emulatorview/EmulatorView;)Ljackpal/androidterm/emulatorview/TermSession;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTermSession:Ljackpal/androidterm/emulatorview/TermSession;

    return-object v0
.end method

.method static synthetic access$1300(Ljackpal/androidterm/emulatorview/EmulatorView;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 55
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->clearSpecialKeyStatus()V

    return-void
.end method

.method static synthetic access$1400(Ljackpal/androidterm/emulatorview/EmulatorView;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 55
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/EmulatorView;->setImeBuffer(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1500(Ljackpal/androidterm/emulatorview/EmulatorView;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mImeBuffer:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Ljackpal/androidterm/emulatorview/EmulatorView;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Ljackpal/androidterm/emulatorview/EmulatorView;)Landroid/widget/Scroller;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mScroller:Landroid/widget/Scroller;

    return-object v0
.end method

.method static synthetic access$400(Ljackpal/androidterm/emulatorview/EmulatorView;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    return v0
.end method

.method static synthetic access$402(Ljackpal/androidterm/emulatorview/EmulatorView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 55
    iput p1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    return p1
.end method

.method static synthetic access$500(Ljackpal/androidterm/emulatorview/EmulatorView;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsSelectingText:Z

    return v0
.end method

.method static synthetic access$600(Ljackpal/androidterm/emulatorview/EmulatorView;)Ljackpal/androidterm/emulatorview/TerminalEmulator;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    return-object v0
.end method

.method static synthetic access$720(Ljackpal/androidterm/emulatorview/EmulatorView;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 55
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY1:I

    sub-int/2addr v0, p1

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY1:I

    return v0
.end method

.method static synthetic access$820(Ljackpal/androidterm/emulatorview/EmulatorView;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 55
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY2:I

    sub-int/2addr v0, p1

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY2:I

    return v0
.end method

.method static synthetic access$920(Ljackpal/androidterm/emulatorview/EmulatorView;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 55
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelYAnchor:I

    sub-int/2addr v0, p1

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelYAnchor:I

    return v0
.end method

.method private clearSpecialKeyStatus()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1076
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsControlKeySent:Z

    if-eqz v0, :cond_c

    .line 1077
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsControlKeySent:Z

    .line 1078
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    invoke-virtual {v0, v1}, Ljackpal/androidterm/emulatorview/TermKeyListener;->handleControlKey(Z)V

    .line 1080
    :cond_c
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsFnKeySent:Z

    if-eqz v0, :cond_17

    .line 1081
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsFnKeySent:Z

    .line 1082
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    invoke-virtual {v0, v1}, Ljackpal/androidterm/emulatorview/TermKeyListener;->handleFnKey(Z)V

    .line 1084
    :cond_17
    return-void
.end method

.method private commonConstructor(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 290
    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mScroller:Landroid/widget/Scroller;

    .line 291
    return-void
.end method

.method private ensureCursorVisible()V
    .registers 5

    .prologue
    .line 1209
    const/4 v2, 0x0

    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    .line 1210
    iget v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleColumns:I

    if-lez v2, :cond_1b

    .line 1211
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getCursorCol()I

    move-result v0

    .line 1212
    .local v0, cx:I
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getCursorCol()I

    move-result v2

    iget v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mLeftColumn:I

    sub-int v1, v2, v3

    .line 1213
    .local v1, visibleCursorX:I
    if-gez v1, :cond_1c

    .line 1214
    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mLeftColumn:I

    .line 1219
    .end local v0           #cx:I
    .end local v1           #visibleCursorX:I
    :cond_1b
    :goto_1b
    return-void

    .line 1215
    .restart local v0       #cx:I
    .restart local v1       #visibleCursorX:I
    :cond_1c
    iget v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleColumns:I

    if-lt v1, v2, :cond_1b

    .line 1216
    iget v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleColumns:I

    sub-int v2, v0, v2

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mLeftColumn:I

    goto :goto_1b
.end method

.method private handleControlKey(IZ)Z
    .registers 4
    .parameter "keyCode"
    .parameter "down"

    .prologue
    .line 1050
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mControlKeyCode:I

    if-ne p1, v0, :cond_b

    .line 1054
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    invoke-virtual {v0, p2}, Ljackpal/androidterm/emulatorview/TermKeyListener;->handleControlKey(Z)V

    .line 1055
    const/4 v0, 0x1

    .line 1057
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private handleFnKey(IZ)Z
    .registers 4
    .parameter "keyCode"
    .parameter "down"

    .prologue
    .line 1061
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mFnKeyCode:I

    if-ne p1, v0, :cond_b

    .line 1065
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    invoke-virtual {v0, p2}, Ljackpal/androidterm/emulatorview/TermKeyListener;->handleFnKey(Z)V

    .line 1066
    const/4 v0, 0x1

    .line 1068
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private initialize()V
    .registers 3

    .prologue
    .line 729
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTermSession:Ljackpal/androidterm/emulatorview/TermSession;

    .line 731
    .local v0, session:Ljackpal/androidterm/emulatorview/TermSession;
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->updateText()V

    .line 733
    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TermSession;->getTranscriptScreen()Ljackpal/androidterm/emulatorview/TranscriptScreen;

    move-result-object v1

    iput-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    .line 734
    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TermSession;->getEmulator()Ljackpal/androidterm/emulatorview/TerminalEmulator;

    move-result-object v1

    iput-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    .line 735
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mUpdateNotify:Ljackpal/androidterm/emulatorview/UpdateCallback;

    invoke-virtual {v0, v1}, Ljackpal/androidterm/emulatorview/TermSession;->setUpdateCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)V

    .line 737
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->requestFocus()Z

    .line 738
    return-void
.end method

.method private isInterceptedSystemKey(I)Z
    .registers 3
    .parameter "keyCode"

    .prologue
    .line 997
    const/4 v0, 0x4

    if-ne p1, v0, :cond_9

    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBackKeySendsCharacter:Z

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method private isSystemKey(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 1072
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v0

    return v0
.end method

.method private onTouchEventWhileSelectingText(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter "ev"

    .prologue
    const/4 v12, 0x1

    .line 920
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 921
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    iget v9, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCharacterWidth:F

    div-float/2addr v8, v9

    float-to-int v2, v8

    .line 922
    .local v2, cx:I
    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    const/high16 v10, -0x3de0

    iget v11, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mScaledDensity:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCharacterHeight:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    float-to-int v9, v9

    iget v10, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    add-int/2addr v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 925
    .local v3, cy:I
    packed-switch v0, :pswitch_data_84

    .line 954
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->toggleSelectingText()V

    .line 955
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->invalidate()V

    .line 958
    :goto_2d
    return v12

    .line 927
    :pswitch_2e
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelXAnchor:I

    .line 928
    iput v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelYAnchor:I

    .line 929
    iput v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX1:I

    .line 930
    iput v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY1:I

    .line 931
    iget v8, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX1:I

    iput v8, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX2:I

    .line 932
    iget v8, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY1:I

    iput v8, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY2:I

    goto :goto_2d

    .line 936
    :pswitch_3f
    iget v8, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelXAnchor:I

    invoke-static {v8, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 937
    .local v6, minx:I
    iget v8, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelXAnchor:I

    invoke-static {v8, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 938
    .local v4, maxx:I
    iget v8, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelYAnchor:I

    invoke-static {v8, v3}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 939
    .local v7, miny:I
    iget v8, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelYAnchor:I

    invoke-static {v8, v3}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 940
    .local v5, maxy:I
    iput v6, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX1:I

    .line 941
    iput v7, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY1:I

    .line 942
    iput v4, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX2:I

    .line 943
    iput v5, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY2:I

    .line 944
    if-ne v0, v12, :cond_7f

    .line 945
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "clipboard"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/ClipboardManager;

    .line 948
    .local v1, clip:Landroid/text/ClipboardManager;
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->getSelectedText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    .line 949
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->toggleSelectingText()V

    .line 951
    .end local v1           #clip:Landroid/text/ClipboardManager;
    :cond_7f
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->invalidate()V

    goto :goto_2d

    .line 925
    nop

    :pswitch_data_84
    .packed-switch 0x0
        :pswitch_2e
        :pswitch_3f
        :pswitch_3f
    .end packed-switch
.end method

.method private setImeBuffer(Ljava/lang/String;)V
    .registers 3
    .parameter "buffer"

    .prologue
    .line 669
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mImeBuffer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 670
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->invalidate()V

    .line 672
    :cond_b
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mImeBuffer:Ljava/lang/String;

    .line 673
    return-void
.end method

.method private updateSize(II)V
    .registers 7
    .parameter "w"
    .parameter "h"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1125
    int-to-float v0, p1

    iget v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCharacterWidth:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mColumns:I

    .line 1126
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleWidth:I

    int-to-float v0, v0

    iget v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCharacterWidth:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleColumns:I

    .line 1128
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextRenderer:Ljackpal/androidterm/emulatorview/TextRenderer;

    invoke-interface {v0}, Ljackpal/androidterm/emulatorview/TextRenderer;->getTopMargin()I

    move-result v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopOfScreenMargin:I

    .line 1129
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopOfScreenMargin:I

    sub-int v0, p2, v0

    iget v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCharacterHeight:I

    div-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mRows:I

    .line 1130
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTermSession:Ljackpal/androidterm/emulatorview/TermSession;

    iget v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mColumns:I

    iget v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mRows:I

    invoke-virtual {v0, v1, v2}, Ljackpal/androidterm/emulatorview/TermSession;->updateSize(II)V

    .line 1133
    iput v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    .line 1134
    iput v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mLeftColumn:I

    .line 1136
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->invalidate()V

    .line 1137
    return-void
.end method

.method private updateText()V
    .registers 4

    .prologue
    .line 1087
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    .line 1088
    .local v0, scheme:Ljackpal/androidterm/emulatorview/ColorScheme;
    iget v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextSize:I

    if-lez v1, :cond_36

    .line 1089
    new-instance v1, Ljackpal/androidterm/emulatorview/PaintRenderer;

    iget v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextSize:I

    invoke-direct {v1, v2, v0}, Ljackpal/androidterm/emulatorview/PaintRenderer;-><init>(ILjackpal/androidterm/emulatorview/ColorScheme;)V

    iput-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextRenderer:Ljackpal/androidterm/emulatorview/TextRenderer;

    .line 1095
    :goto_f
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mForegroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/ColorScheme;->getForeColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1096
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/ColorScheme;->getBackColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1097
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextRenderer:Ljackpal/androidterm/emulatorview/TextRenderer;

    invoke-interface {v1}, Ljackpal/androidterm/emulatorview/TextRenderer;->getCharacterWidth()F

    move-result v1

    iput v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCharacterWidth:F

    .line 1098
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextRenderer:Ljackpal/androidterm/emulatorview/TextRenderer;

    invoke-interface {v1}, Ljackpal/androidterm/emulatorview/TextRenderer;->getCharacterHeight()I

    move-result v1

    iput v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCharacterHeight:I

    .line 1100
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ljackpal/androidterm/emulatorview/EmulatorView;->updateSize(Z)V

    .line 1101
    return-void

    .line 1092
    :cond_36
    new-instance v1, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;

    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;-><init>(Landroid/content/res/Resources;Ljackpal/androidterm/emulatorview/ColorScheme;)V

    iput-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextRenderer:Ljackpal/androidterm/emulatorview/TextRenderer;

    goto :goto_f
.end method


# virtual methods
.method public attachSession(Ljackpal/androidterm/emulatorview/TermSession;)V
    .registers 7
    .parameter "session"

    .prologue
    const/16 v4, 0x80

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 299
    const/4 v0, 0x0

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextRenderer:Ljackpal/androidterm/emulatorview/TextRenderer;

    .line 300
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorPaint:Landroid/graphics/Paint;

    .line 301
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorPaint:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 302
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mForegroundPaint:Landroid/graphics/Paint;

    .line 303
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBackgroundPaint:Landroid/graphics/Paint;

    .line 304
    iput v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    .line 305
    iput v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mLeftColumn:I

    .line 306
    new-instance v0, Landroid/view/GestureDetector;

    invoke-direct {v0, p0}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 308
    invoke-virtual {p0, v2}, Ljackpal/androidterm/emulatorview/EmulatorView;->setVerticalScrollBarEnabled(Z)V

    .line 309
    invoke-virtual {p0, v2}, Ljackpal/androidterm/emulatorview/EmulatorView;->setFocusable(Z)V

    .line 310
    invoke-virtual {p0, v2}, Ljackpal/androidterm/emulatorview/EmulatorView;->setFocusableInTouchMode(Z)V

    .line 312
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTermSession:Ljackpal/androidterm/emulatorview/TermSession;

    .line 314
    new-instance v0, Ljackpal/androidterm/emulatorview/TermKeyListener;

    invoke-direct {v0, p1}, Ljackpal/androidterm/emulatorview/TermKeyListener;-><init>(Ljackpal/androidterm/emulatorview/TermSession;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    .line 317
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mDeferInit:Z

    if-eqz v0, :cond_4b

    .line 318
    iput-boolean v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mDeferInit:Z

    .line 319
    iput-boolean v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKnownSize:Z

    .line 320
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->initialize()V

    .line 322
    :cond_4b
    return-void
.end method

.method protected computeVerticalScrollExtent()I
    .registers 2

    .prologue
    .line 712
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mRows:I

    return v0
.end method

.method protected computeVerticalScrollOffset()I
    .registers 3

    .prologue
    .line 722
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->getActiveRows()I

    move-result v0

    iget v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    add-int/2addr v0, v1

    iget v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mRows:I

    sub-int/2addr v0, v1

    return v0
.end method

.method protected computeVerticalScrollRange()I
    .registers 2

    .prologue
    .line 702
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->getActiveRows()I

    move-result v0

    return v0
.end method

.method public getKeypadApplicationMode()Z
    .registers 2

    .prologue
    .line 679
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getKeypadApplicationMode()Z

    move-result v0

    return v0
.end method

.method public getSelectedText()Ljava/lang/String;
    .registers 6

    .prologue
    .line 1248
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    iget v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX1:I

    iget v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY1:I

    iget v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX2:I

    iget v4, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY2:I

    invoke-virtual {v0, v1, v2, v3, v4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getSelectedText(IIII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSelectingText()Z
    .registers 2

    .prologue
    .line 1239
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsSelectingText:Z

    return v0
.end method

.method public getTermSession()Ljackpal/androidterm/emulatorview/TermSession;
    .registers 2

    .prologue
    .line 746
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTermSession:Ljackpal/androidterm/emulatorview/TermSession;

    return-object v0
.end method

.method public getVisibleHeight()I
    .registers 2

    .prologue
    .line 764
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleHeight:I

    return v0
.end method

.method public getVisibleWidth()I
    .registers 2

    .prologue
    .line 755
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleWidth:I

    return v0
.end method

.method public onCheckIsTextEditor()Z
    .registers 2

    .prologue
    .line 378
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .registers 4
    .parameter "outAttrs"

    .prologue
    const/4 v1, 0x1

    .line 383
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mUseCookedIme:Z

    if-eqz v0, :cond_e

    move v0, v1

    :goto_6
    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 386
    new-instance v0, Ljackpal/androidterm/emulatorview/EmulatorView$4;

    invoke-direct {v0, p0, p0, v1}, Ljackpal/androidterm/emulatorview/EmulatorView$4;-><init>(Ljackpal/androidterm/emulatorview/EmulatorView;Landroid/view/View;Z)V

    return-object v0

    .line 383
    :cond_e
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "e"

    .prologue
    const/4 v1, 0x1

    .line 900
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mExtGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    if-eqz v0, :cond_e

    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mExtGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 904
    :goto_d
    return v1

    .line 903
    :cond_e
    const/4 v0, 0x0

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mScrollRemainder:F

    goto :goto_d
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 25
    .parameter "canvas"

    .prologue
    .line 1165
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Ljackpal/androidterm/emulatorview/EmulatorView;->updateSize(Z)V

    .line 1167
    move-object/from16 v0, p0

    iget-object v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    if-nez v1, :cond_d

    .line 1206
    :cond_c
    return-void

    .line 1172
    :cond_d
    invoke-virtual/range {p0 .. p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->getWidth()I

    move-result v22

    .line 1173
    .local v22, w:I
    invoke-virtual/range {p0 .. p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->getHeight()I

    move-result v20

    .line 1175
    .local v20, h:I
    move-object/from16 v0, p0

    iget-object v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getReverseVideo()Z

    move-result v21

    .line 1176
    .local v21, reverseVideo:Z
    move-object/from16 v0, p0

    iget-object v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextRenderer:Ljackpal/androidterm/emulatorview/TextRenderer;

    move/from16 v0, v21

    invoke-interface {v1, v0}, Ljackpal/androidterm/emulatorview/TextRenderer;->setReverseVideo(Z)V

    .line 1178
    if-eqz v21, :cond_bc

    move-object/from16 v0, p0

    iget-object v6, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mForegroundPaint:Landroid/graphics/Paint;

    .line 1180
    .local v6, backgroundPaint:Landroid/graphics/Paint;
    :goto_2c
    const/4 v2, 0x0

    const/4 v3, 0x0

    move/from16 v0, v22

    int-to-float v4, v0

    move/from16 v0, v20

    int-to-float v5, v0

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1181
    move-object/from16 v0, p0

    iget v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mLeftColumn:I

    neg-int v1, v1

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCharacterWidth:F

    mul-float v10, v1, v2

    .line 1182
    .local v10, x:F
    move-object/from16 v0, p0

    iget v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCharacterHeight:I

    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopOfScreenMargin:I

    add-int/2addr v1, v2

    int-to-float v11, v1

    .line 1183
    .local v11, y:F
    move-object/from16 v0, p0

    iget v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mRows:I

    add-int v19, v1, v2

    .line 1184
    .local v19, endLine:I
    move-object/from16 v0, p0

    iget-object v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getCursorCol()I

    move-result v17

    .line 1185
    .local v17, cx:I
    move-object/from16 v0, p0

    iget-object v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getCursorRow()I

    move-result v18

    .line 1186
    .local v18, cy:I
    move-object/from16 v0, p0

    iget v8, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    .local v8, i:I
    :goto_6d
    move/from16 v0, v19

    if-ge v8, v0, :cond_c

    .line 1187
    const/4 v13, -0x1

    .line 1188
    .local v13, cursorX:I
    move/from16 v0, v18

    if-ne v8, v0, :cond_7e

    move-object/from16 v0, p0

    iget-boolean v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorVisible:Z

    if-eqz v1, :cond_7e

    .line 1189
    move/from16 v13, v17

    .line 1191
    :cond_7e
    const/4 v14, -0x1

    .line 1192
    .local v14, selx1:I
    const/4 v15, -0x1

    .line 1193
    .local v15, selx2:I
    move-object/from16 v0, p0

    iget v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY1:I

    if-lt v8, v1, :cond_a0

    move-object/from16 v0, p0

    iget v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY2:I

    if-gt v8, v1, :cond_a0

    .line 1194
    move-object/from16 v0, p0

    iget v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY1:I

    if-ne v8, v1, :cond_96

    .line 1195
    move-object/from16 v0, p0

    iget v14, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX1:I

    .line 1197
    :cond_96
    move-object/from16 v0, p0

    iget v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY2:I

    if-ne v8, v1, :cond_c2

    .line 1198
    move-object/from16 v0, p0

    iget v15, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX2:I

    .line 1203
    :cond_a0
    :goto_a0
    move-object/from16 v0, p0

    iget-object v7, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    move-object/from16 v0, p0

    iget-object v12, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextRenderer:Ljackpal/androidterm/emulatorview/TextRenderer;

    move-object/from16 v0, p0

    iget-object v0, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mImeBuffer:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v9, p1

    invoke-virtual/range {v7 .. v16}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->drawText(ILandroid/graphics/Canvas;FFLjackpal/androidterm/emulatorview/TextRenderer;IIILjava/lang/String;)V

    .line 1204
    move-object/from16 v0, p0

    iget v1, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCharacterHeight:I

    int-to-float v1, v1

    add-float/2addr v11, v1

    .line 1186
    add-int/lit8 v8, v8, 0x1

    goto :goto_6d

    .line 1178
    .end local v6           #backgroundPaint:Landroid/graphics/Paint;
    .end local v8           #i:I
    .end local v10           #x:F
    .end local v11           #y:F
    .end local v13           #cursorX:I
    .end local v14           #selx1:I
    .end local v15           #selx2:I
    .end local v17           #cx:I
    .end local v18           #cy:I
    .end local v19           #endLine:I
    :cond_bc
    move-object/from16 v0, p0

    iget-object v6, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBackgroundPaint:Landroid/graphics/Paint;

    goto/16 :goto_2c

    .line 1200
    .restart local v6       #backgroundPaint:Landroid/graphics/Paint;
    .restart local v8       #i:I
    .restart local v10       #x:F
    .restart local v11       #y:F
    .restart local v13       #cursorX:I
    .restart local v14       #selx1:I
    .restart local v15       #selx2:I
    .restart local v17       #cx:I
    .restart local v18       #cy:I
    .restart local v19       #endLine:I
    :cond_c2
    move-object/from16 v0, p0

    iget v15, v0, Ljackpal/androidterm/emulatorview/EmulatorView;->mColumns:I

    goto :goto_a0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 16
    .parameter "e1"
    .parameter "e2"
    .parameter "velocityX"
    .parameter "velocityY"

    .prologue
    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 879
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mExtGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    if-eqz v0, :cond_f

    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mExtGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/view/GestureDetector$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 890
    :goto_e
    return v10

    .line 882
    :cond_f
    const/high16 v9, 0x3e80

    .line 883
    .local v9, SCALE:F
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mScroller:Landroid/widget/Scroller;

    iget v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    mul-float v3, p3, v9

    float-to-int v3, v3

    neg-int v3, v3

    mul-float v4, p4, v9

    float-to-int v4, v4

    neg-int v4, v4

    iget-object v5, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    invoke-virtual {v5}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->getActiveTranscriptRows()I

    move-result v5

    neg-int v7, v5

    move v5, v1

    move v6, v1

    move v8, v1

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 887
    const/4 v0, 0x0

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mScrollRemainder:F

    .line 889
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mFlingRunner:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Ljackpal/androidterm/emulatorview/EmulatorView;->post(Ljava/lang/Runnable;)Z

    goto :goto_e
.end method

.method public onJumpTapDown(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "e1"
    .parameter "e2"

    .prologue
    .line 865
    const/4 v0, 0x0

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    .line 866
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->invalidate()V

    .line 867
    const/4 v0, 0x1

    return v0
.end method

.method public onJumpTapUp(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "e1"
    .parameter "e2"

    .prologue
    .line 872
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->getActiveTranscriptRows()I

    move-result v0

    neg-int v0, v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    .line 873
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->invalidate()V

    .line 874
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v0, 0x1

    .line 973
    invoke-direct {p0, p1, v0}, Ljackpal/androidterm/emulatorview/EmulatorView;->handleControlKey(IZ)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 992
    :cond_7
    :goto_7
    return v0

    .line 975
    :cond_8
    invoke-direct {p0, p1, v0}, Ljackpal/androidterm/emulatorview/EmulatorView;->handleFnKey(IZ)Z

    move-result v1

    if-nez v1, :cond_7

    .line 977
    invoke-direct {p0, p1, p2}, Ljackpal/androidterm/emulatorview/EmulatorView;->isSystemKey(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 978
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/EmulatorView;->isInterceptedSystemKey(I)Z

    move-result v1

    if-nez v1, :cond_1f

    .line 980
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_7

    .line 987
    :cond_1f
    :try_start_1f
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->getKeypadApplicationMode()Z

    move-result v2

    invoke-static {p2}, Ljackpal/androidterm/emulatorview/TermKeyListener;->isEventFromToggleDevice(Landroid/view/KeyEvent;)Z

    move-result v3

    invoke-virtual {v1, p1, p2, v2, v3}, Ljackpal/androidterm/emulatorview/TermKeyListener;->keyDown(ILandroid/view/KeyEvent;ZZ)V
    :try_end_2c
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_2c} :catch_2d

    goto :goto_7

    .line 989
    :catch_2d
    move-exception v1

    goto :goto_7
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .registers 12
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1030
    sget-boolean v7, Ljackpal/androidterm/emulatorview/EmulatorView;->sTrapAltAndMeta:Z

    if-eqz v7, :cond_4e

    .line 1031
    iget-object v7, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    invoke-virtual {v7}, Ljackpal/androidterm/emulatorview/TermKeyListener;->getAltSendsEsc()Z

    move-result v1

    .line 1032
    .local v1, altEsc:Z
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v7

    and-int/lit8 v7, v7, 0x2

    if-eqz v7, :cond_43

    move v2, v5

    .line 1033
    .local v2, altOn:Z
    :goto_15
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v7

    const/high16 v8, 0x1

    and-int/2addr v7, v8

    if-eqz v7, :cond_45

    move v4, v5

    .line 1034
    .local v4, metaOn:Z
    :goto_1f
    const/16 v7, 0x39

    if-eq p1, v7, :cond_27

    const/16 v7, 0x3a

    if-ne p1, v7, :cond_47

    :cond_27
    move v3, v5

    .line 1036
    .local v3, altPressed:Z
    :goto_28
    iget-object v5, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    invoke-virtual {v5}, Ljackpal/androidterm/emulatorview/TermKeyListener;->isAltActive()Z

    move-result v0

    .line 1037
    .local v0, altActive:Z
    if-eqz v1, :cond_4e

    if-nez v2, :cond_38

    if-nez v3, :cond_38

    if-nez v0, :cond_38

    if-eqz v4, :cond_4e

    .line 1038
    :cond_38
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_49

    .line 1039
    invoke-virtual {p0, p1, p2}, Ljackpal/androidterm/emulatorview/EmulatorView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v5

    .line 1046
    .end local v0           #altActive:Z
    .end local v1           #altEsc:Z
    .end local v2           #altOn:Z
    .end local v3           #altPressed:Z
    .end local v4           #metaOn:Z
    :goto_42
    return v5

    .restart local v1       #altEsc:Z
    :cond_43
    move v2, v6

    .line 1032
    goto :goto_15

    .restart local v2       #altOn:Z
    :cond_45
    move v4, v6

    .line 1033
    goto :goto_1f

    .restart local v4       #metaOn:Z
    :cond_47
    move v3, v6

    .line 1034
    goto :goto_28

    .line 1041
    .restart local v0       #altActive:Z
    .restart local v3       #altPressed:Z
    :cond_49
    invoke-virtual {p0, p1, p2}, Ljackpal/androidterm/emulatorview/EmulatorView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v5

    goto :goto_42

    .line 1046
    .end local v0           #altActive:Z
    .end local v1           #altEsc:Z
    .end local v2           #altOn:Z
    .end local v3           #altPressed:Z
    .end local v4           #metaOn:Z
    :cond_4e
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v5

    goto :goto_42
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 1012
    invoke-direct {p0, p1, v2}, Ljackpal/androidterm/emulatorview/EmulatorView;->handleControlKey(IZ)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1025
    :cond_8
    :goto_8
    return v0

    .line 1014
    :cond_9
    invoke-direct {p0, p1, v2}, Ljackpal/androidterm/emulatorview/EmulatorView;->handleFnKey(IZ)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1016
    invoke-direct {p0, p1, p2}, Ljackpal/androidterm/emulatorview/EmulatorView;->isSystemKey(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 1018
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/EmulatorView;->isInterceptedSystemKey(I)Z

    move-result v1

    if-nez v1, :cond_20

    .line 1019
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_8

    .line 1023
    :cond_20
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    invoke-virtual {v1, p1, p2}, Ljackpal/androidterm/emulatorview/TermKeyListener;->keyUp(ILandroid/view/KeyEvent;)V

    .line 1024
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->clearSpecialKeyStatus()V

    goto :goto_8
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .registers 2
    .parameter "e"

    .prologue
    .line 841
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->showContextMenu()Z

    .line 842
    return-void
.end method

.method public onPause()V
    .registers 3

    .prologue
    .line 353
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorBlink:I

    if-eqz v0, :cond_b

    .line 354
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBlinkCursor:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 356
    :cond_b
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsActive:Z

    .line 357
    return-void
.end method

.method public onResume()V
    .registers 5

    .prologue
    .line 342
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsActive:Z

    .line 343
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljackpal/androidterm/emulatorview/EmulatorView;->updateSize(Z)V

    .line 344
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorBlink:I

    if-eqz v0, :cond_14

    .line 345
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBlinkCursor:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 347
    :cond_14
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 10
    .parameter "e1"
    .parameter "e2"
    .parameter "distanceX"
    .parameter "distanceY"

    .prologue
    const/4 v4, 0x1

    .line 846
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mExtGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    if-eqz v1, :cond_e

    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mExtGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 857
    :goto_d
    return v4

    .line 849
    :cond_e
    iget v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mScrollRemainder:F

    add-float/2addr p4, v1

    .line 850
    iget v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCharacterHeight:I

    int-to-float v1, v1

    div-float v1, p4, v1

    float-to-int v0, v1

    .line 851
    .local v0, deltaRows:I
    iget v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCharacterHeight:I

    mul-int/2addr v1, v0

    int-to-float v1, v1

    sub-float v1, p4, v1

    iput v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mScrollRemainder:F

    .line 852
    const/4 v1, 0x0

    iget-object v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    invoke-virtual {v2}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->getActiveTranscriptRows()I

    move-result v2

    neg-int v2, v2

    iget v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    add-int/2addr v3, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    .line 855
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->invalidate()V

    goto :goto_d
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter "e"

    .prologue
    .line 894
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mExtGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    if-eqz v0, :cond_9

    .line 895
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mExtGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onShowPress(Landroid/view/MotionEvent;)V

    .line 897
    :cond_9
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)V
    .registers 2
    .parameter "e"

    .prologue
    .line 861
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "e"

    .prologue
    const/4 v1, 0x1

    .line 832
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mExtGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    if-eqz v0, :cond_e

    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mExtGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 836
    :goto_d
    return v1

    .line 835
    :cond_e
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->requestFocus()Z

    goto :goto_d
.end method

.method protected onSizeChanged(IIII)V
    .registers 7
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    .prologue
    const/4 v1, 0x1

    .line 1110
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTermSession:Ljackpal/androidterm/emulatorview/TermSession;

    if-nez v0, :cond_8

    .line 1112
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mDeferInit:Z

    .line 1122
    :goto_7
    return-void

    .line 1116
    :cond_8
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKnownSize:Z

    if-nez v0, :cond_12

    .line 1117
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKnownSize:Z

    .line 1118
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->initialize()V

    goto :goto_7

    .line 1120
    :cond_12
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljackpal/androidterm/emulatorview/EmulatorView;->updateSize(Z)V

    goto :goto_7
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    .prologue
    .line 911
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsSelectingText:Z

    if-eqz v0, :cond_9

    .line 912
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/EmulatorView;->onTouchEventWhileSelectingText(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 914
    :goto_8
    return v0

    :cond_9
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_8
.end method

.method public page(I)V
    .registers 6
    .parameter "delta"

    .prologue
    .line 775
    const/4 v0, 0x0

    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->getActiveTranscriptRows()I

    move-result v1

    neg-int v1, v1

    iget v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    iget v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mRows:I

    mul-int/2addr v3, p1

    add-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTopRow:I

    .line 778
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->invalidate()V

    .line 779
    return-void
.end method

.method public pageHorizontal(I)V
    .registers 6
    .parameter "deltaColumns"

    .prologue
    .line 788
    const/4 v0, 0x0

    iget v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mLeftColumn:I

    add-int/2addr v1, p1

    iget v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mColumns:I

    iget v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleColumns:I

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mLeftColumn:I

    .line 791
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->invalidate()V

    .line 792
    return-void
.end method

.method public sendControlKey()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 1255
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsControlKeySent:Z

    .line 1256
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    invoke-virtual {v0, v1}, Ljackpal/androidterm/emulatorview/TermKeyListener;->handleControlKey(Z)V

    .line 1257
    return-void
.end method

.method public sendFnKey()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 1264
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsFnKeySent:Z

    .line 1265
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    invoke-virtual {v0, v1}, Ljackpal/androidterm/emulatorview/TermKeyListener;->handleFnKey(Z)V

    .line 1266
    return-void
.end method

.method public setAltSendsEsc(Z)V
    .registers 3
    .parameter "flag"

    .prologue
    .line 1282
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    invoke-virtual {v0, p1}, Ljackpal/androidterm/emulatorview/TermKeyListener;->setAltSendsEsc(Z)V

    .line 1283
    return-void
.end method

.method public setBackKeyCharacter(I)V
    .registers 3
    .parameter "keyCode"

    .prologue
    .line 1272
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKeyListener:Ljackpal/androidterm/emulatorview/TermKeyListener;

    invoke-virtual {v0, p1}, Ljackpal/androidterm/emulatorview/TermKeyListener;->setBackKeyCharacter(I)V

    .line 1273
    if-eqz p1, :cond_b

    const/4 v0, 0x1

    :goto_8
    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBackKeySendsCharacter:Z

    .line 1274
    return-void

    .line 1273
    :cond_b
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public setColorScheme(Ljackpal/androidterm/emulatorview/ColorScheme;)V
    .registers 3
    .parameter "scheme"

    .prologue
    .line 368
    if-nez p1, :cond_a

    .line 369
    sget-object v0, Ljackpal/androidterm/emulatorview/BaseTextRenderer;->defaultColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    .line 373
    :goto_6
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->updateText()V

    .line 374
    return-void

    .line 371
    :cond_a
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    goto :goto_6
.end method

.method public setControlKeyCode(I)V
    .registers 2
    .parameter "keyCode"

    .prologue
    .line 1289
    iput p1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mControlKeyCode:I

    .line 1290
    return-void
.end method

.method public setCursorStyle(II)V
    .registers 7
    .parameter "style"
    .parameter "blink"

    .prologue
    .line 811
    iput p1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorStyle:I

    .line 812
    if-eqz p2, :cond_14

    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorBlink:I

    if-nez v0, :cond_14

    .line 813
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBlinkCursor:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 817
    :cond_11
    :goto_11
    iput p2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorBlink:I

    .line 818
    return-void

    .line 814
    :cond_14
    if-nez p2, :cond_11

    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mCursorBlink:I

    if-eqz v0, :cond_11

    .line 815
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mBlinkCursor:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_11
.end method

.method public setDensity(Landroid/util/DisplayMetrics;)V
    .registers 4
    .parameter "metrics"

    .prologue
    .line 330
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mDensity:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_10

    .line 332
    iget v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextSize:I

    int-to-float v0, v0

    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextSize:I

    .line 334
    :cond_10
    iget v0, p1, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mDensity:F

    .line 335
    iget v0, p1, Landroid/util/DisplayMetrics;->scaledDensity:F

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mScaledDensity:F

    .line 336
    return-void
.end method

.method public setExtGestureListener(Landroid/view/GestureDetector$OnGestureListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 694
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mExtGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 695
    return-void
.end method

.method public setFnKeyCode(I)V
    .registers 2
    .parameter "keyCode"

    .prologue
    .line 1296
    iput p1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mFnKeyCode:I

    .line 1297
    return-void
.end method

.method public setTextSize(I)V
    .registers 4
    .parameter "fontSize"

    .prologue
    .line 800
    int-to-float v0, p1

    iget v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mDensity:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mTextSize:I

    .line 801
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->updateText()V

    .line 802
    return-void
.end method

.method public setUseCookedIME(Z)V
    .registers 2
    .parameter "useCookedIME"

    .prologue
    .line 826
    iput-boolean p1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mUseCookedIme:Z

    .line 827
    return-void
.end method

.method public toggleSelectingText()V
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 1225
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsSelectingText:Z

    if-nez v0, :cond_1e

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsSelectingText:Z

    .line 1226
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsSelectingText:Z

    if-nez v0, :cond_20

    :goto_e
    invoke-virtual {p0, v1}, Ljackpal/androidterm/emulatorview/EmulatorView;->setVerticalScrollBarEnabled(Z)V

    .line 1227
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mIsSelectingText:Z

    if-nez v0, :cond_1d

    .line 1228
    iput v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX1:I

    .line 1229
    iput v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY1:I

    .line 1230
    iput v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelX2:I

    .line 1231
    iput v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mSelY2:I

    .line 1233
    :cond_1d
    return-void

    :cond_1e
    move v0, v2

    .line 1225
    goto :goto_8

    :cond_20
    move v1, v2

    .line 1226
    goto :goto_e
.end method

.method public updateSize(Z)V
    .registers 6
    .parameter "force"

    .prologue
    .line 1146
    iget-boolean v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mKnownSize:Z

    if-eqz v2, :cond_21

    .line 1147
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->getWidth()I

    move-result v1

    .line 1148
    .local v1, w:I
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/EmulatorView;->getHeight()I

    move-result v0

    .line 1150
    .local v0, h:I
    if-nez p1, :cond_16

    iget v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleWidth:I

    if-ne v1, v2, :cond_16

    iget v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleHeight:I

    if-eq v0, v2, :cond_21

    .line 1151
    :cond_16
    iput v1, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleWidth:I

    .line 1152
    iput v0, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleHeight:I

    .line 1153
    iget v2, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleWidth:I

    iget v3, p0, Ljackpal/androidterm/emulatorview/EmulatorView;->mVisibleHeight:I

    invoke-direct {p0, v2, v3}, Ljackpal/androidterm/emulatorview/EmulatorView;->updateSize(II)V

    .line 1156
    .end local v0           #h:I
    .end local v1           #w:I
    :cond_21
    return-void
.end method
