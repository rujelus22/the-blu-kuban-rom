.class Ljackpal/androidterm/emulatorview/UnicodeTranscript;
.super Ljava/lang/Object;
.source "UnicodeTranscript.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UnicodeTranscript"


# instance fields
.field private mActiveTranscriptRows:I

.field private mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

.field private mColumns:I

.field private mDefaultStyle:I

.field private mLineWrap:[Z

.field private mLines:[Ljava/lang/Object;

.field private mScreenFirstRow:I

.field private mScreenRows:I

.field private mTotalRows:I

.field private tmpColor:Ljackpal/androidterm/emulatorview/StyleRow;

.field private tmpLine:[C


# direct methods
.method public constructor <init>(IIII)V
    .registers 7
    .parameter "columns"
    .parameter "totalRows"
    .parameter "screenRows"
    .parameter "defaultStyle"

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    .line 54
    iput v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mDefaultStyle:I

    .line 56
    iput v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    .line 62
    iput p1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    .line 63
    iput p2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mTotalRows:I

    .line 64
    iput p3, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    .line 65
    new-array v0, p2, [Ljava/lang/Object;

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    .line 66
    new-array v0, p2, [Ljackpal/androidterm/emulatorview/StyleRow;

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    .line 67
    new-array v0, p2, [Z

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLineWrap:[Z

    .line 68
    new-instance v0, Ljackpal/androidterm/emulatorview/StyleRow;

    iget v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    invoke-direct {v0, p4, v1}, Ljackpal/androidterm/emulatorview/StyleRow;-><init>(II)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpColor:Ljackpal/androidterm/emulatorview/StyleRow;

    .line 70
    iput p4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mDefaultStyle:I

    .line 71
    return-void
.end method

.method private allocateBasicLine(II)[C
    .registers 8
    .parameter "row"
    .parameter "columns"

    .prologue
    .line 689
    new-array v1, p2, [C

    .line 692
    .local v1, line:[C
    const/4 v0, 0x0

    .local v0, i:I
    :goto_3
    if-ge v0, p2, :cond_c

    .line 693
    const/16 v2, 0x20

    aput-char v2, v1, v0

    .line 692
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 696
    :cond_c
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aput-object v1, v2, p1

    .line 697
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    aget-object v2, v2, p1

    if-nez v2, :cond_20

    .line 698
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    new-instance v3, Ljackpal/androidterm/emulatorview/StyleRow;

    const/4 v4, 0x0

    invoke-direct {v3, v4, p2}, Ljackpal/androidterm/emulatorview/StyleRow;-><init>(II)V

    aput-object v3, v2, p1

    .line 700
    :cond_20
    return-object v1
.end method

.method private allocateFullLine(II)Ljackpal/androidterm/emulatorview/FullUnicodeLine;
    .registers 7
    .parameter "row"
    .parameter "columns"

    .prologue
    .line 704
    new-instance v0, Ljackpal/androidterm/emulatorview/FullUnicodeLine;

    invoke-direct {v0, p2}, Ljackpal/androidterm/emulatorview/FullUnicodeLine;-><init>(I)V

    .line 706
    .local v0, line:Ljackpal/androidterm/emulatorview/FullUnicodeLine;
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aput-object v0, v1, p1

    .line 707
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    aget-object v1, v1, p1

    if-nez v1, :cond_19

    .line 708
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    new-instance v2, Ljackpal/androidterm/emulatorview/StyleRow;

    const/4 v3, 0x0

    invoke-direct {v2, v3, p2}, Ljackpal/androidterm/emulatorview/StyleRow;-><init>(II)V

    aput-object v2, v1, p1

    .line 710
    :cond_19
    return-object v0
.end method

.method private blockCopyLines(III)V
    .registers 11
    .parameter "src"
    .parameter "len"
    .parameter "shift"

    .prologue
    .line 243
    iget v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mTotalRows:I

    .line 246
    .local v2, totalRows:I
    add-int v3, p1, p3

    if-ltz v3, :cond_28

    .line 247
    add-int v3, p1, p3

    rem-int v0, v3, v2

    .line 252
    .local v0, dst:I
    :goto_a
    add-int v3, p1, p2

    if-gt v3, v2, :cond_2d

    add-int v3, v0, p2

    if-gt v3, v2, :cond_2d

    .line 254
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    invoke-static {v3, p1, v4, v0, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 255
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    invoke-static {v3, p1, v4, v0, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 256
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLineWrap:[Z

    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLineWrap:[Z

    invoke-static {v3, p1, v4, v0, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 275
    :cond_27
    return-void

    .line 249
    .end local v0           #dst:I
    :cond_28
    add-int v3, v2, p1

    add-int v0, v3, p3

    .restart local v0       #dst:I
    goto :goto_a

    .line 260
    :cond_2d
    if-gez p3, :cond_5f

    .line 262
    const/4 v1, 0x0

    .local v1, i:I
    :goto_30
    if-ge v1, p2, :cond_27

    .line 263
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    add-int v4, v0, v1

    rem-int/2addr v4, v2

    iget-object v5, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    add-int v6, p1, v1

    rem-int/2addr v6, v2

    aget-object v5, v5, v6

    aput-object v5, v3, v4

    .line 264
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    add-int v4, v0, v1

    rem-int/2addr v4, v2

    iget-object v5, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    add-int v6, p1, v1

    rem-int/2addr v6, v2

    aget-object v5, v5, v6

    aput-object v5, v3, v4

    .line 265
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLineWrap:[Z

    add-int v4, v0, v1

    rem-int/2addr v4, v2

    iget-object v5, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLineWrap:[Z

    add-int v6, p1, v1

    rem-int/2addr v6, v2

    aget-boolean v5, v5, v6

    aput-boolean v5, v3, v4

    .line 262
    add-int/lit8 v1, v1, 0x1

    goto :goto_30

    .line 269
    .end local v1           #i:I
    :cond_5f
    add-int/lit8 v1, p2, -0x1

    .restart local v1       #i:I
    :goto_61
    if-ltz v1, :cond_27

    .line 270
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    add-int v4, v0, v1

    rem-int/2addr v4, v2

    iget-object v5, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    add-int v6, p1, v1

    rem-int/2addr v6, v2

    aget-object v5, v5, v6

    aput-object v5, v3, v4

    .line 271
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    add-int v4, v0, v1

    rem-int/2addr v4, v2

    iget-object v5, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    add-int v6, p1, v1

    rem-int/2addr v6, v2

    aget-object v5, v5, v6

    aput-object v5, v3, v4

    .line 272
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLineWrap:[Z

    add-int v4, v0, v1

    rem-int/2addr v4, v2

    iget-object v5, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLineWrap:[Z

    add-int v6, p1, v1

    rem-int/2addr v6, v2

    aget-boolean v5, v5, v6

    aput-boolean v5, v3, v4

    .line 269
    add-int/lit8 v1, v1, -0x1

    goto :goto_61
.end method

.method public static charWidth(CC)I
    .registers 3
    .parameter "cHigh"
    .parameter "cLow"

    .prologue
    .line 537
    invoke-static {p0, p1}, Ljava/lang/Character;->toCodePoint(CC)I

    move-result v0

    invoke-static {v0}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->charWidth(I)I

    move-result v0

    return v0
.end method

.method public static charWidth(I)I
    .registers 4
    .parameter "codePoint"

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 499
    const/16 v2, 0x1f

    if-le p0, v2, :cond_b

    const/16 v2, 0x7f

    if-ge p0, v2, :cond_b

    .line 533
    :cond_a
    :goto_a
    return v0

    .line 505
    :cond_b
    const/16 v2, 0x1b

    if-eq p0, v2, :cond_a

    .line 509
    invoke-static {p0}, Ljava/lang/Character;->getType(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_34

    .line 517
    invoke-static {p0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    if-ne v2, v0, :cond_29

    .line 519
    int-to-char v2, p0

    invoke-static {v2}, Ljackpal/androidterm/emulatorview/compat/AndroidCharacterCompat;->getEastAsianWidth(C)I

    move-result v2

    packed-switch v2, :pswitch_data_46

    :pswitch_24
    goto :goto_a

    :pswitch_25
    move v0, v1

    .line 522
    goto :goto_a

    .line 514
    :sswitch_27
    const/4 v0, 0x0

    goto :goto_a

    .line 526
    :cond_29
    shr-int/lit8 v2, p0, 0x10

    and-int/lit8 v2, v2, 0xf

    packed-switch v2, :pswitch_data_50

    goto :goto_a

    :pswitch_31
    move v0, v1

    .line 529
    goto :goto_a

    .line 509
    nop

    :sswitch_data_34
    .sparse-switch
        0x6 -> :sswitch_27
        0x7 -> :sswitch_27
        0xf -> :sswitch_27
        0x10 -> :sswitch_27
    .end sparse-switch

    .line 519
    :pswitch_data_46
    .packed-switch 0x3
        :pswitch_25
        :pswitch_24
        :pswitch_25
    .end packed-switch

    .line 526
    :pswitch_data_50
    .packed-switch 0x2
        :pswitch_31
        :pswitch_31
    .end packed-switch
.end method

.method public static charWidth([CI)I
    .registers 4
    .parameter "chars"
    .parameter "index"

    .prologue
    .line 549
    aget-char v0, p0, p1

    .line 550
    .local v0, c:C
    invoke-static {v0}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 551
    add-int/lit8 v1, p1, 0x1

    aget-char v1, p0, v1

    invoke-static {v0, v1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->charWidth(CC)I

    move-result v1

    .line 553
    :goto_10
    return v1

    :cond_11
    invoke-static {v0}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->charWidth(I)I

    move-result v1

    goto :goto_10
.end method

.method private externalToInternalRow(I)I
    .registers 5
    .parameter "extRow"

    .prologue
    .line 105
    iget v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    neg-int v1, v1

    if-lt p1, v1, :cond_9

    iget v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    if-le p1, v1, :cond_3f

    .line 106
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "externalToInternalRow "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, errorMessage:Ljava/lang/String;
    const-string v1, "UnicodeTranscript"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 112
    .end local v0           #errorMessage:Ljava/lang/String;
    :cond_3f
    if-ltz p1, :cond_48

    .line 113
    iget v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    add-int/2addr v1, p1

    iget v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mTotalRows:I

    rem-int/2addr v1, v2

    .line 118
    :goto_47
    return v1

    .line 115
    :cond_48
    neg-int v1, p1

    iget v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    if-le v1, v2, :cond_54

    .line 116
    iget v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mTotalRows:I

    iget v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    add-int/2addr v1, v2

    add-int/2addr v1, p1

    goto :goto_47

    .line 118
    :cond_54
    iget v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    add-int/2addr v1, p1

    goto :goto_47
.end method

.method private isBasicChar(I)Z
    .registers 4
    .parameter "codePoint"

    .prologue
    const/4 v0, 0x1

    .line 685
    invoke-static {p1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->charWidth(I)I

    move-result v1

    if-ne v1, v0, :cond_e

    invoke-static {p1}, Ljava/lang/Character;->charCount(I)I

    move-result v1

    if-ne v1, v0, :cond_e

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method


# virtual methods
.method public blockCopy(IIIIII)V
    .registers 29
    .parameter "sx"
    .parameter "sy"
    .parameter "w"
    .parameter "h"
    .parameter "dx"
    .parameter "dy"

    .prologue
    .line 367
    if-ltz p1, :cond_28

    add-int v3, p1, p3

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    if-gt v3, v4, :cond_28

    if-ltz p2, :cond_28

    add-int v3, p2, p4

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    if-gt v3, v4, :cond_28

    if-ltz p5, :cond_28

    add-int v3, p5, p3

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    if-gt v3, v4, :cond_28

    if-ltz p6, :cond_28

    add-int v3, p6, p4

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    if-le v3, v4, :cond_2e

    .line 370
    :cond_28
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3

    .line 372
    :cond_2e
    move-object/from16 v0, p0

    iget-object v0, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    move-object/from16 v16, v0

    .line 373
    .local v16, lines:[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v12, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    .line 374
    .local v12, color:[Ljackpal/androidterm/emulatorview/StyleRow;
    move/from16 v0, p2

    move/from16 v1, p6

    if-le v0, v1, :cond_f0

    .line 376
    const/16 v20, 0x0

    .local v20, y:I
    :goto_40
    move/from16 v0, v20

    move/from16 v1, p4

    if-ge v0, v1, :cond_1a6

    .line 377
    add-int v3, p2, v20

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result v17

    .line 378
    .local v17, srcRow:I
    add-int v3, p6, v20

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result v14

    .line 379
    .local v14, dstRow:I
    aget-object v3, v16, v17

    instance-of v3, v3, [C

    if-eqz v3, :cond_7f

    aget-object v3, v16, v14

    instance-of v3, v3, [C

    if-eqz v3, :cond_7f

    .line 380
    aget-object v3, v16, v17

    aget-object v4, v16, v14

    move/from16 v0, p1

    move/from16 v1, p5

    move/from16 v2, p3

    invoke-static {v3, v0, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 410
    :cond_6f
    aget-object v3, v12, v17

    aget-object v4, v12, v14

    move/from16 v0, p1

    move/from16 v1, p5

    move/from16 v2, p3

    invoke-virtual {v3, v0, v4, v1, v2}, Ljackpal/androidterm/emulatorview/StyleRow;->copy(ILjackpal/androidterm/emulatorview/StyleRow;II)V

    .line 376
    :goto_7c
    add-int/lit8 v20, v20, 0x1

    goto :goto_40

    .line 383
    :cond_7f
    add-int v5, p6, v20

    .line 384
    .local v5, extDstRow:I
    add-int v3, p2, v20

    add-int v4, p1, p3

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v3, v1, v4}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getLine(III)[C

    move-result-object v18

    .line 385
    .local v18, tmp:[C
    if-nez v18, :cond_a0

    .line 387
    const/4 v7, 0x1

    const/16 v8, 0x20

    move-object/from16 v0, p0

    iget v9, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mDefaultStyle:I

    move-object/from16 v3, p0

    move/from16 v4, p5

    move/from16 v6, p3

    invoke-virtual/range {v3 .. v9}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->blockSet(IIIIII)V

    goto :goto_7c

    .line 390
    :cond_a0
    const/4 v10, 0x0

    .line 391
    .local v10, cHigh:C
    const/16 v19, 0x0

    .line 392
    .local v19, x:I
    move-object/from16 v0, p0

    iget v13, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    .line 393
    .local v13, columns:I
    const/4 v15, 0x0

    .local v15, i:I
    :goto_a8
    move-object/from16 v0, v18

    array-length v3, v0

    if-ge v15, v3, :cond_6f

    .line 394
    aget-char v3, v18, v15

    if-eqz v3, :cond_6f

    add-int v3, p5, v19

    if-ge v3, v13, :cond_6f

    .line 397
    aget-char v3, v18, v15

    invoke-static {v3}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_c2

    .line 398
    aget-char v10, v18, v15

    .line 393
    :goto_bf
    add-int/lit8 v15, v15, 0x1

    goto :goto_a8

    .line 400
    :cond_c2
    aget-char v3, v18, v15

    invoke-static {v3}, Ljava/lang/Character;->isLowSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_de

    .line 401
    aget-char v3, v18, v15

    invoke-static {v10, v3}, Ljava/lang/Character;->toCodePoint(CC)I

    move-result v11

    .line 402
    .local v11, codePoint:I
    add-int v3, p5, v19

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5, v11}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->setChar(III)Z

    .line 403
    invoke-static {v11}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->charWidth(I)I

    move-result v3

    add-int v19, v19, v3

    .line 404
    goto :goto_bf

    .line 405
    .end local v11           #codePoint:I
    :cond_de
    add-int v3, p5, v19

    aget-char v4, v18, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5, v4}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->setChar(III)Z

    .line 406
    aget-char v3, v18, v15

    invoke-static {v3}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->charWidth(I)I

    move-result v3

    add-int v19, v19, v3

    goto :goto_bf

    .line 414
    .end local v5           #extDstRow:I
    .end local v10           #cHigh:C
    .end local v13           #columns:I
    .end local v14           #dstRow:I
    .end local v15           #i:I
    .end local v17           #srcRow:I
    .end local v18           #tmp:[C
    .end local v19           #x:I
    .end local v20           #y:I
    :cond_f0
    const/16 v20, 0x0

    .restart local v20       #y:I
    :goto_f2
    move/from16 v0, v20

    move/from16 v1, p4

    if-ge v0, v1, :cond_1a6

    .line 415
    add-int/lit8 v3, v20, 0x1

    sub-int v21, p4, v3

    .line 416
    .local v21, y2:I
    add-int v3, p2, v21

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result v17

    .line 417
    .restart local v17       #srcRow:I
    add-int v3, p6, v21

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result v14

    .line 418
    .restart local v14       #dstRow:I
    aget-object v3, v16, v17

    instance-of v3, v3, [C

    if-eqz v3, :cond_135

    aget-object v3, v16, v14

    instance-of v3, v3, [C

    if-eqz v3, :cond_135

    .line 419
    aget-object v3, v16, v17

    aget-object v4, v16, v14

    move/from16 v0, p1

    move/from16 v1, p5

    move/from16 v2, p3

    invoke-static {v3, v0, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 448
    :cond_125
    aget-object v3, v12, v17

    aget-object v4, v12, v14

    move/from16 v0, p1

    move/from16 v1, p5

    move/from16 v2, p3

    invoke-virtual {v3, v0, v4, v1, v2}, Ljackpal/androidterm/emulatorview/StyleRow;->copy(ILjackpal/androidterm/emulatorview/StyleRow;II)V

    .line 414
    :goto_132
    add-int/lit8 v20, v20, 0x1

    goto :goto_f2

    .line 421
    :cond_135
    add-int v5, p6, v21

    .line 422
    .restart local v5       #extDstRow:I
    add-int v3, p2, v21

    add-int v4, p1, p3

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v3, v1, v4}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getLine(III)[C

    move-result-object v18

    .line 423
    .restart local v18       #tmp:[C
    if-nez v18, :cond_156

    .line 425
    const/4 v7, 0x1

    const/16 v8, 0x20

    move-object/from16 v0, p0

    iget v9, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mDefaultStyle:I

    move-object/from16 v3, p0

    move/from16 v4, p5

    move/from16 v6, p3

    invoke-virtual/range {v3 .. v9}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->blockSet(IIIIII)V

    goto :goto_132

    .line 428
    :cond_156
    const/4 v10, 0x0

    .line 429
    .restart local v10       #cHigh:C
    const/16 v19, 0x0

    .line 430
    .restart local v19       #x:I
    move-object/from16 v0, p0

    iget v13, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    .line 431
    .restart local v13       #columns:I
    const/4 v15, 0x0

    .restart local v15       #i:I
    :goto_15e
    move-object/from16 v0, v18

    array-length v3, v0

    if-ge v15, v3, :cond_125

    .line 432
    aget-char v3, v18, v15

    if-eqz v3, :cond_125

    add-int v3, p5, v19

    if-ge v3, v13, :cond_125

    .line 435
    aget-char v3, v18, v15

    invoke-static {v3}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_178

    .line 436
    aget-char v10, v18, v15

    .line 431
    :goto_175
    add-int/lit8 v15, v15, 0x1

    goto :goto_15e

    .line 438
    :cond_178
    aget-char v3, v18, v15

    invoke-static {v3}, Ljava/lang/Character;->isLowSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_194

    .line 439
    aget-char v3, v18, v15

    invoke-static {v10, v3}, Ljava/lang/Character;->toCodePoint(CC)I

    move-result v11

    .line 440
    .restart local v11       #codePoint:I
    add-int v3, p5, v19

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5, v11}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->setChar(III)Z

    .line 441
    invoke-static {v11}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->charWidth(I)I

    move-result v3

    add-int v19, v19, v3

    .line 442
    goto :goto_175

    .line 443
    .end local v11           #codePoint:I
    :cond_194
    add-int v3, p5, v19

    aget-char v4, v18, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5, v4}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->setChar(III)Z

    .line 444
    aget-char v3, v18, v15

    invoke-static {v3}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->charWidth(I)I

    move-result v3

    add-int v19, v19, v3

    goto :goto_175

    .line 451
    .end local v5           #extDstRow:I
    .end local v10           #cHigh:C
    .end local v13           #columns:I
    .end local v14           #dstRow:I
    .end local v15           #i:I
    .end local v17           #srcRow:I
    .end local v18           #tmp:[C
    .end local v19           #x:I
    .end local v21           #y2:I
    :cond_1a6
    return-void
.end method

.method public blockSet(IIIIII)V
    .registers 12
    .parameter "sx"
    .parameter "sy"
    .parameter "w"
    .parameter "h"
    .parameter "val"
    .parameter "style"

    .prologue
    .line 466
    if-ltz p1, :cond_10

    add-int v2, p1, p3

    iget v3, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    if-gt v2, v3, :cond_10

    if-ltz p2, :cond_10

    add-int v2, p2, p4

    iget v3, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    if-le v2, v3, :cond_6e

    .line 467
    :cond_10
    const-string v2, "UnicodeTranscript"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "illegal arguments! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 471
    :cond_6e
    const/4 v1, 0x0

    .local v1, y:I
    :goto_6f
    if-ge v1, p4, :cond_81

    .line 472
    const/4 v0, 0x0

    .local v0, x:I
    :goto_72
    if-ge v0, p3, :cond_7e

    .line 473
    add-int v2, p1, v0

    add-int v3, p2, v1

    invoke-virtual {p0, v2, v3, p5, p6}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->setChar(IIII)Z

    .line 472
    add-int/lit8 v0, v0, 0x1

    goto :goto_72

    .line 471
    :cond_7e
    add-int/lit8 v1, v1, 0x1

    goto :goto_6f

    .line 476
    .end local v0           #x:I
    :cond_81
    return-void
.end method

.method public getActiveRows()I
    .registers 3

    .prologue
    .line 86
    iget v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    iget v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getActiveTranscriptRows()I
    .registers 2

    .prologue
    .line 82
    iget v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    return v0
.end method

.method public getChar(II)Z
    .registers 4
    .parameter "row"
    .parameter "column"

    .prologue
    .line 649
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getChar(III)Z

    move-result v0

    return v0
.end method

.method public getChar(III)Z
    .registers 10
    .parameter "row"
    .parameter "column"
    .parameter "charIndex"

    .prologue
    .line 653
    const/4 v0, 0x1

    new-array v4, v0, [C

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getChar(III[CI)Z

    move-result v0

    return v0
.end method

.method public getChar(III[CI)Z
    .registers 8
    .parameter "row"
    .parameter "column"
    .parameter "charIndex"
    .parameter "out"
    .parameter "offset"

    .prologue
    .line 668
    iget v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    neg-int v1, v1

    if-lt p1, v1, :cond_b

    iget v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    add-int/lit8 v1, v1, -0x1

    if-le p1, v1, :cond_11

    .line 669
    :cond_b
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 671
    :cond_11
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result p1

    .line 673
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aget-object v1, v1, p1

    instance-of v1, v1, [C

    if-eqz v1, :cond_2c

    .line 675
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aget-object v1, v1, p1

    check-cast v1, [C

    move-object v0, v1

    check-cast v0, [C

    .line 676
    .local v0, line:[C
    aget-char v1, v0, p2

    aput-char v1, p4, p5

    .line 677
    const/4 v1, 0x0

    .line 681
    .end local v0           #line:[C
    :goto_2b
    return v1

    .line 680
    :cond_2c
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aget-object v0, v1, p1

    check-cast v0, Ljackpal/androidterm/emulatorview/FullUnicodeLine;

    .line 681
    .local v0, line:Ljackpal/androidterm/emulatorview/FullUnicodeLine;
    invoke-virtual {v0, p2, p3, p4, p5}, Ljackpal/androidterm/emulatorview/FullUnicodeLine;->getChar(II[CI)Z

    move-result v1

    goto :goto_2b
.end method

.method public getDefaultStyle()I
    .registers 2

    .prologue
    .line 78
    iget v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mDefaultStyle:I

    return v0
.end method

.method public getLine(I)[C
    .registers 4
    .parameter "row"

    .prologue
    .line 617
    const/4 v0, 0x0

    iget v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    invoke-virtual {p0, p1, v0, v1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getLine(III)[C

    move-result-object v0

    return-object v0
.end method

.method public getLine(III)[C
    .registers 11
    .parameter "row"
    .parameter "x1"
    .parameter "x2"

    .prologue
    const/4 v6, 0x0

    .line 571
    iget v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    neg-int v4, v4

    if-lt p1, v4, :cond_c

    iget v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    add-int/lit8 v4, v4, -0x1

    if-le p1, v4, :cond_12

    .line 572
    :cond_c
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    .line 575
    :cond_12
    iget v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    .line 576
    .local v0, columns:I
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result p1

    .line 577
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aget-object v4, v4, p1

    if-nez v4, :cond_20

    .line 579
    const/4 v4, 0x0

    .line 613
    :goto_1f
    return-object v4

    .line 581
    :cond_20
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aget-object v4, v4, p1

    instance-of v4, v4, [C

    if-eqz v4, :cond_58

    .line 583
    if-nez p2, :cond_35

    if-ne p3, v0, :cond_35

    .line 585
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aget-object v4, v4, p1

    check-cast v4, [C

    check-cast v4, [C

    goto :goto_1f

    .line 587
    :cond_35
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpLine:[C

    if-eqz v4, :cond_40

    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpLine:[C

    array-length v4, v4

    add-int/lit8 v5, v0, 0x1

    if-ge v4, v5, :cond_46

    .line 588
    :cond_40
    add-int/lit8 v4, v0, 0x1

    new-array v4, v4, [C

    iput-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpLine:[C

    .line 590
    :cond_46
    sub-int v1, p3, p2

    .line 591
    .local v1, length:I
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aget-object v4, v4, p1

    iget-object v5, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpLine:[C

    invoke-static {v4, p2, v5, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 592
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpLine:[C

    aput-char v6, v4, v1

    .line 593
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpLine:[C

    goto :goto_1f

    .line 598
    .end local v1           #length:I
    :cond_58
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aget-object v2, v4, p1

    check-cast v2, Ljackpal/androidterm/emulatorview/FullUnicodeLine;

    .line 599
    .local v2, line:Ljackpal/androidterm/emulatorview/FullUnicodeLine;
    invoke-virtual {v2}, Ljackpal/androidterm/emulatorview/FullUnicodeLine;->getLine()[C

    move-result-object v3

    .line 600
    .local v3, rawLine:[C
    invoke-virtual {v2, p2}, Ljackpal/androidterm/emulatorview/FullUnicodeLine;->findStartOfColumn(I)I

    move-result p2

    .line 601
    if-ge p3, v0, :cond_8b

    .line 602
    invoke-virtual {v2, p3}, Ljackpal/androidterm/emulatorview/FullUnicodeLine;->findStartOfColumn(I)I

    move-result p3

    .line 606
    :goto_6c
    sub-int v1, p3, p2

    .line 608
    .restart local v1       #length:I
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpLine:[C

    if-eqz v4, :cond_79

    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpLine:[C

    array-length v4, v4

    add-int/lit8 v5, v1, 0x1

    if-ge v4, v5, :cond_7f

    .line 609
    :cond_79
    add-int/lit8 v4, v1, 0x1

    new-array v4, v4, [C

    iput-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpLine:[C

    .line 611
    :cond_7f
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpLine:[C

    invoke-static {v3, p2, v4, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 612
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpLine:[C

    aput-char v6, v4, v1

    .line 613
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpLine:[C

    goto :goto_1f

    .line 604
    .end local v1           #length:I
    :cond_8b
    invoke-virtual {v2}, Ljackpal/androidterm/emulatorview/FullUnicodeLine;->getSpaceUsed()I

    move-result p3

    goto :goto_6c
.end method

.method public getLineColor(I)Ljackpal/androidterm/emulatorview/StyleRow;
    .registers 4
    .parameter "row"

    .prologue
    .line 645
    const/4 v0, 0x0

    iget v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    invoke-virtual {p0, p1, v0, v1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getLineColor(III)Ljackpal/androidterm/emulatorview/StyleRow;

    move-result-object v0

    return-object v0
.end method

.method public getLineColor(III)Ljackpal/androidterm/emulatorview/StyleRow;
    .registers 8
    .parameter "row"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 626
    iget v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    neg-int v2, v2

    if-lt p1, v2, :cond_b

    iget v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    add-int/lit8 v2, v2, -0x1

    if-le p1, v2, :cond_11

    .line 627
    :cond_b
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 630
    :cond_11
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result p1

    .line 631
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    aget-object v0, v2, p1

    .line 632
    .local v0, color:Ljackpal/androidterm/emulatorview/StyleRow;
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->tmpColor:Ljackpal/androidterm/emulatorview/StyleRow;

    .line 633
    .local v1, tmp:Ljackpal/androidterm/emulatorview/StyleRow;
    if-eqz v0, :cond_2c

    .line 634
    if-nez p2, :cond_24

    iget v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    if-ne p3, v2, :cond_24

    .line 640
    .end local v0           #color:Ljackpal/androidterm/emulatorview/StyleRow;
    :goto_23
    return-object v0

    .line 637
    .restart local v0       #color:Ljackpal/androidterm/emulatorview/StyleRow;
    :cond_24
    const/4 v2, 0x0

    sub-int v3, p3, p2

    invoke-virtual {v0, p2, v1, v2, v3}, Ljackpal/androidterm/emulatorview/StyleRow;->copy(ILjackpal/androidterm/emulatorview/StyleRow;II)V

    move-object v0, v1

    .line 638
    goto :goto_23

    .line 640
    :cond_2c
    const/4 v0, 0x0

    goto :goto_23
.end method

.method public getLineWrap(I)Z
    .registers 4
    .parameter "row"

    .prologue
    .line 128
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLineWrap:[Z

    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result v1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public resize(II[I)Z
    .registers 21
    .parameter "newColumns"
    .parameter "newRows"
    .parameter "cursor"

    .prologue
    .line 143
    move-object/from16 v0, p0

    iget v15, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    move/from16 v0, p1

    if-ne v0, v15, :cond_10

    move-object/from16 v0, p0

    iget v15, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mTotalRows:I

    move/from16 v0, p2

    if-le v0, v15, :cond_12

    .line 144
    :cond_10
    const/4 v15, 0x0

    .line 231
    :goto_11
    return v15

    .line 147
    :cond_12
    move-object/from16 v0, p0

    iget v12, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    .line 148
    .local v12, screenRows:I
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    .line 149
    .local v2, activeTranscriptRows:I
    sub-int v13, v12, p2

    .line 150
    .local v13, shift:I
    neg-int v15, v2

    if-ge v13, v15, :cond_85

    .line 152
    move-object/from16 v0, p0

    iget-object v10, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    .line 153
    .local v10, lines:[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v3, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    .line 154
    .local v3, color:[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v9, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLineWrap:[Z

    .line 155
    .local v9, lineWrap:[Z
    move-object/from16 v0, p0

    iget v11, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    .line 156
    .local v11, screenFirstRow:I
    move-object/from16 v0, p0

    iget v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mTotalRows:I

    .line 157
    .local v14, totalRows:I
    const/4 v4, 0x0

    .local v4, i:I
    :goto_34
    sub-int v15, v2, v13

    if-ge v4, v15, :cond_49

    .line 158
    add-int v15, v11, v12

    add-int/2addr v15, v4

    rem-int v5, v15, v14

    .line 159
    .local v5, index:I
    const/4 v15, 0x0

    aput-object v15, v10, v5

    .line 160
    const/4 v15, 0x0

    aput-object v15, v3, v5

    .line 161
    const/4 v15, 0x0

    aput-boolean v15, v9, v5

    .line 157
    add-int/lit8 v4, v4, 0x1

    goto :goto_34

    .line 163
    .end local v5           #index:I
    :cond_49
    neg-int v13, v2

    .line 215
    .end local v3           #color:[Ljava/lang/Object;
    .end local v4           #i:I
    .end local v9           #lineWrap:[Z
    .end local v10           #lines:[Ljava/lang/Object;
    .end local v11           #screenFirstRow:I
    .end local v14           #totalRows:I
    :cond_4a
    :goto_4a
    if-gtz v13, :cond_59

    if-gez v13, :cond_dd

    move-object/from16 v0, p0

    iget v15, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    neg-int v0, v13

    move/from16 v16, v0

    move/from16 v0, v16

    if-lt v15, v0, :cond_dd

    .line 217
    :cond_59
    move-object/from16 v0, p0

    iget v15, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    add-int/2addr v15, v13

    move-object/from16 v0, p0

    iget v0, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mTotalRows:I

    move/from16 v16, v0

    rem-int v15, v15, v16

    move-object/from16 v0, p0

    iput v15, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    .line 223
    :cond_6a
    :goto_6a
    move-object/from16 v0, p0

    iget v15, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    add-int/2addr v15, v13

    if-gez v15, :cond_f2

    .line 224
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    .line 228
    :goto_76
    const/4 v15, 0x1

    aget v16, p3, v15

    sub-int v16, v16, v13

    aput v16, p3, v15

    .line 229
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    .line 231
    const/4 v15, 0x1

    goto :goto_11

    .line 164
    :cond_85
    if-lez v13, :cond_4a

    const/4 v15, 0x1

    aget v15, p3, v15

    add-int/lit8 v16, v12, -0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_4a

    .line 167
    move-object/from16 v0, p0

    iget-object v10, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    .line 168
    .restart local v10       #lines:[Ljava/lang/Object;
    add-int/lit8 v4, v12, -0x1

    .restart local v4       #i:I
    :goto_96
    const/4 v15, 0x1

    aget v15, p3, v15

    if-le v4, v15, :cond_4a

    .line 169
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result v5

    .line 170
    .restart local v5       #index:I
    aget-object v15, v10, v5

    if-nez v15, :cond_ac

    .line 172
    add-int/lit8 v13, v13, -0x1

    .line 173
    if-eqz v13, :cond_4a

    .line 168
    :cond_a9
    add-int/lit8 v4, v4, -0x1

    goto :goto_96

    .line 181
    :cond_ac
    aget-object v15, v10, v5

    instance-of v15, v15, [C

    if-eqz v15, :cond_c9

    .line 182
    aget-object v15, v10, v5

    check-cast v15, [C

    move-object v8, v15

    check-cast v8, [C

    .line 187
    .local v8, line:[C
    :goto_b9
    array-length v7, v8

    .line 189
    .local v7, len:I
    const/4 v6, 0x0

    .local v6, j:I
    :goto_bb
    if-ge v6, v7, :cond_c2

    .line 190
    aget-char v15, v8, v6

    if-nez v15, :cond_d2

    .line 192
    move v6, v7

    .line 200
    :cond_c2
    if-ne v6, v7, :cond_4a

    .line 202
    add-int/lit8 v13, v13, -0x1

    .line 203
    if-nez v13, :cond_a9

    goto :goto_4a

    .line 184
    .end local v6           #j:I
    .end local v7           #len:I
    .end local v8           #line:[C
    :cond_c9
    aget-object v15, v10, v5

    check-cast v15, Ljackpal/androidterm/emulatorview/FullUnicodeLine;

    invoke-virtual {v15}, Ljackpal/androidterm/emulatorview/FullUnicodeLine;->getLine()[C

    move-result-object v8

    .restart local v8       #line:[C
    goto :goto_b9

    .line 194
    .restart local v6       #j:I
    .restart local v7       #len:I
    :cond_d2
    aget-char v15, v8, v6

    const/16 v16, 0x20

    move/from16 v0, v16

    if-ne v15, v0, :cond_c2

    .line 189
    add-int/lit8 v6, v6, 0x1

    goto :goto_bb

    .line 218
    .end local v4           #i:I
    .end local v5           #index:I
    .end local v6           #j:I
    .end local v7           #len:I
    .end local v8           #line:[C
    .end local v10           #lines:[Ljava/lang/Object;
    :cond_dd
    if-gez v13, :cond_6a

    .line 220
    move-object/from16 v0, p0

    iget v15, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mTotalRows:I

    move-object/from16 v0, p0

    iget v0, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    move/from16 v16, v0

    add-int v15, v15, v16

    add-int/2addr v15, v13

    move-object/from16 v0, p0

    iput v15, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    goto/16 :goto_6a

    .line 226
    :cond_f2
    move-object/from16 v0, p0

    iget v15, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    add-int/2addr v15, v13

    move-object/from16 v0, p0

    iput v15, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    goto/16 :goto_76
.end method

.method public scroll(III)V
    .registers 21
    .parameter "topMargin"
    .parameter "bottomMargin"
    .parameter "style"

    .prologue
    .line 288
    add-int/lit8 v14, p2, -0x1

    move/from16 v0, p1

    if-le v0, v14, :cond_c

    .line 289
    new-instance v14, Ljava/lang/IllegalArgumentException;

    invoke-direct {v14}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v14

    .line 292
    :cond_c
    if-gez p1, :cond_14

    .line 293
    new-instance v14, Ljava/lang/IllegalArgumentException;

    invoke-direct {v14}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v14

    .line 296
    :cond_14
    move-object/from16 v0, p0

    iget v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    move/from16 v0, p2

    if-le v0, v14, :cond_22

    .line 297
    new-instance v14, Ljava/lang/IllegalArgumentException;

    invoke-direct {v14}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v14

    .line 300
    :cond_22
    move-object/from16 v0, p0

    iget v8, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    .line 301
    .local v8, screenRows:I
    move-object/from16 v0, p0

    iget v13, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mTotalRows:I

    .line 303
    .local v13, totalRows:I
    if-nez p1, :cond_79

    move/from16 v0, p2

    if-ne v0, v8, :cond_79

    .line 305
    move-object/from16 v0, p0

    iget v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    add-int/lit8 v14, v14, 0x1

    rem-int/2addr v14, v13

    move-object/from16 v0, p0

    iput v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    .line 306
    move-object/from16 v0, p0

    iget v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    sub-int v15, v13, v8

    if-ge v14, v15, :cond_4d

    .line 307
    move-object/from16 v0, p0

    iget v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    .line 311
    :cond_4d
    add-int/lit8 v14, p2, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result v2

    .line 312
    .local v2, blankRow:I
    move-object/from16 v0, p0

    iget-object v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v15, v14, v2

    .line 313
    move-object/from16 v0, p0

    iget-object v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    new-instance v15, Ljackpal/androidterm/emulatorview/StyleRow;

    move-object/from16 v0, p0

    iget v0, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    move/from16 v16, v0

    move/from16 v0, p3

    move/from16 v1, v16

    invoke-direct {v15, v0, v1}, Ljackpal/androidterm/emulatorview/StyleRow;-><init>(II)V

    aput-object v15, v14, v2

    .line 314
    move-object/from16 v0, p0

    iget-object v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLineWrap:[Z

    const/4 v15, 0x0

    aput-boolean v15, v14, v2

    .line 350
    :goto_78
    return-void

    .line 319
    .end local v2           #blankRow:I
    :cond_79
    move-object/from16 v0, p0

    iget v7, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    .line 320
    .local v7, screenFirstRow:I
    invoke-direct/range {p0 .. p1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result v12

    .line 321
    .local v12, topMarginInt:I
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result v3

    .line 326
    .local v3, bottomMarginInt:I
    move-object/from16 v0, p0

    iget-object v6, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    .line 327
    .local v6, lines:[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v4, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    .line 328
    .local v4, color:[Ljackpal/androidterm/emulatorview/StyleRow;
    move-object/from16 v0, p0

    iget-object v5, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLineWrap:[Z

    .line 329
    .local v5, lineWrap:[Z
    aget-object v10, v6, v12

    .line 330
    .local v10, scrollLine:Ljava/lang/Object;
    aget-object v9, v4, v12

    .line 331
    .local v9, scrollColor:Ljackpal/androidterm/emulatorview/StyleRow;
    aget-boolean v11, v5, v12

    .line 332
    .local v11, scrollLineWrap:Z
    const/4 v14, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v7, v1, v14}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->blockCopyLines(III)V

    .line 333
    sub-int v14, v8, p2

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v14, v15}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->blockCopyLines(III)V

    .line 334
    aput-object v10, v6, v7

    .line 335
    aput-object v9, v4, v7

    .line 336
    aput-boolean v11, v5, v7

    .line 339
    add-int/lit8 v14, v7, 0x1

    rem-int/2addr v14, v13

    move-object/from16 v0, p0

    iput v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenFirstRow:I

    .line 340
    move-object/from16 v0, p0

    iget v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    sub-int v15, v13, v8

    if-ge v14, v15, :cond_ca

    .line 341
    move-object/from16 v0, p0

    iget v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mActiveTranscriptRows:I

    .line 345
    :cond_ca
    add-int/lit8 v14, p2, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result v2

    .line 346
    .restart local v2       #blankRow:I
    const/4 v14, 0x0

    aput-object v14, v6, v2

    .line 347
    new-instance v14, Ljackpal/androidterm/emulatorview/StyleRow;

    move-object/from16 v0, p0

    iget v15, v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    move/from16 v0, p3

    invoke-direct {v14, v0, v15}, Ljackpal/androidterm/emulatorview/StyleRow;-><init>(II)V

    aput-object v14, v4, v2

    .line 348
    const/4 v14, 0x0

    aput-boolean v14, v5, v2

    goto :goto_78
.end method

.method public setChar(III)Z
    .registers 9
    .parameter "column"
    .parameter "row"
    .parameter "codePoint"

    .prologue
    const/4 v4, 0x1

    .line 725
    iget v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    if-ge p2, v2, :cond_9

    iget v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    if-lt p1, v2, :cond_49

    .line 726
    :cond_9
    const-string v2, "UnicodeTranscript"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "illegal arguments! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mScreenRows:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 729
    :cond_49
    invoke-direct {p0, p2}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result p2

    .line 735
    const/4 v0, -0x1

    .line 738
    .local v0, basicMode:I
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aget-object v2, v2, p2

    if-nez v2, :cond_60

    .line 739
    invoke-direct {p0, p3}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->isBasicChar(I)Z

    move-result v2

    if-eqz v2, :cond_81

    .line 740
    iget v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    invoke-direct {p0, p2, v2}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->allocateBasicLine(II)[C

    .line 741
    const/4 v0, 0x1

    .line 748
    :cond_60
    :goto_60
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aget-object v2, v2, p2

    instance-of v2, v2, [C

    if-eqz v2, :cond_93

    .line 749
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aget-object v2, v2, p2

    check-cast v2, [C

    move-object v1, v2

    check-cast v1, [C

    .line 751
    .local v1, line:[C
    const/4 v2, -0x1

    if-ne v0, v2, :cond_7b

    .line 752
    invoke-direct {p0, p3}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->isBasicChar(I)Z

    move-result v2

    if-eqz v2, :cond_88

    .line 753
    const/4 v0, 0x1

    .line 759
    :cond_7b
    :goto_7b
    if-ne v0, v4, :cond_8a

    .line 761
    int-to-char v2, p3

    aput-char v2, v1, p1

    .line 771
    .end local v1           #line:[C
    :goto_80
    return v4

    .line 743
    :cond_81
    iget v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColumns:I

    invoke-direct {p0, p2, v2}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->allocateFullLine(II)Ljackpal/androidterm/emulatorview/FullUnicodeLine;

    .line 744
    const/4 v0, 0x0

    goto :goto_60

    .line 755
    .restart local v1       #line:[C
    :cond_88
    const/4 v0, 0x0

    goto :goto_7b

    .line 766
    :cond_8a
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    new-instance v3, Ljackpal/androidterm/emulatorview/FullUnicodeLine;

    invoke-direct {v3, v1}, Ljackpal/androidterm/emulatorview/FullUnicodeLine;-><init>([C)V

    aput-object v3, v2, p2

    .line 769
    .end local v1           #line:[C
    :cond_93
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLines:[Ljava/lang/Object;

    aget-object v1, v2, p2

    check-cast v1, Ljackpal/androidterm/emulatorview/FullUnicodeLine;

    .line 770
    .local v1, line:Ljackpal/androidterm/emulatorview/FullUnicodeLine;
    invoke-virtual {v1, p1, p3}, Ljackpal/androidterm/emulatorview/FullUnicodeLine;->setChar(II)V

    goto :goto_80
.end method

.method public setChar(IIII)Z
    .registers 6
    .parameter "column"
    .parameter "row"
    .parameter "codePoint"
    .parameter "style"

    .prologue
    .line 714
    invoke-virtual {p0, p1, p2, p3}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->setChar(III)Z

    move-result v0

    if-nez v0, :cond_8

    .line 715
    const/4 v0, 0x0

    .line 721
    :goto_7
    return v0

    .line 718
    :cond_8
    invoke-direct {p0, p2}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result p2

    .line 719
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mColor:[Ljackpal/androidterm/emulatorview/StyleRow;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1, p4}, Ljackpal/androidterm/emulatorview/StyleRow;->set(II)V

    .line 721
    const/4 v0, 0x1

    goto :goto_7
.end method

.method public setDefaultStyle(I)V
    .registers 2
    .parameter "defaultStyle"

    .prologue
    .line 74
    iput p1, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mDefaultStyle:I

    .line 75
    return-void
.end method

.method public setLineWrap(I)V
    .registers 5
    .parameter "row"

    .prologue
    .line 124
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->mLineWrap:[Z

    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->externalToInternalRow(I)I

    move-result v1

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    .line 125
    return-void
.end method
