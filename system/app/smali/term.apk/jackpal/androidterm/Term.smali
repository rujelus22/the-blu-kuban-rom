.class public Ljackpal/androidterm/Term;
.super Landroid/app/Activity;
.source "Term.java"

# interfaces
.implements Ljackpal/androidterm/emulatorview/UpdateCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljackpal/androidterm/Term$EmulatorViewGestureListener;,
        Ljackpal/androidterm/Term$WindowListActionBarAdapter;
    }
.end annotation


# static fields
.field private static final ACTION_PATH_BROADCAST:Ljava/lang/String; = "jackpal.androidterm.broadcast.APPEND_TO_PATH"

.field private static final ACTION_PATH_PREPEND_BROADCAST:Ljava/lang/String; = "jackpal.androidterm.broadcast.PREPEND_TO_PATH"

.field private static final COPY_ALL_ID:I = 0x1

.field public static final EXTRA_WINDOW_ID:Ljava/lang/String; = "jackpal.androidterm.window_id"

.field private static final FLAG_INCLUDE_STOPPED_PACKAGES:I = 0x20

.field private static final PASTE_ID:I = 0x2

.field private static final PERMISSION_PATH_BROADCAST:Ljava/lang/String; = "jackpal.androidterm.permission.APPEND_TO_PATH"

.field private static final PERMISSION_PATH_PREPEND_BROADCAST:Ljava/lang/String; = "jackpal.androidterm.permission.PREPEND_TO_PATH"

.field public static final REQUEST_CHOOSE_WINDOW:I = 0x1

.field private static final SELECT_TEXT_ID:I = 0x0

.field private static final SEND_CONTROL_KEY_ID:I = 0x3

.field private static final SEND_FN_KEY_ID:I = 0x4

.field private static final VIEW_FLIPPER:I = 0x7f0b0000

.field private static final WIFI_MODE_FULL_HIGH_PERF:I = 0x3


# instance fields
.field private TSIntent:Landroid/content/Intent;

.field private mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

.field private mActionBarMode:I

.field private mAlreadyStarted:Z

.field private mBackKeyListener:Landroid/view/View$OnKeyListener;

.field private mBackKeyPressed:Z

.field private mHandler:Landroid/os/Handler;

.field private mHaveFullHwKeyboard:Z

.field private mLastNewIntent:Landroid/content/Intent;

.field private mPathReceiver:Landroid/content/BroadcastReceiver;

.field private mPendingPathBroadcasts:I

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mSettings:Ljackpal/androidterm/util/TermSettings;

.field private mStopServiceOnFinish:Z

.field private mTSConnection:Landroid/content/ServiceConnection;

.field private mTermService:Ljackpal/androidterm/TermService;

.field private mTermSessions:Ljackpal/androidterm/util/SessionList;

.field private mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

.field private mWinListAdapter:Ljackpal/androidterm/WindowListAdapter;

.field private mWinListItemSelected:Ljackpal/androidterm/compat/ActionBarCompat$OnNavigationListener;

.field private onResumeSelectWindow:I


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 100
    iput-boolean v1, p0, Ljackpal/androidterm/Term;->mAlreadyStarted:Z

    .line 101
    iput-boolean v1, p0, Ljackpal/androidterm/Term;->mStopServiceOnFinish:Z

    .line 108
    const/4 v0, -0x1

    iput v0, p0, Ljackpal/androidterm/Term;->onResumeSelectWindow:I

    .line 121
    iput v1, p0, Ljackpal/androidterm/Term;->mPendingPathBroadcasts:I

    .line 122
    new-instance v0, Ljackpal/androidterm/Term$1;

    invoke-direct {v0, p0}, Ljackpal/androidterm/Term$1;-><init>(Ljackpal/androidterm/Term;)V

    iput-object v0, p0, Ljackpal/androidterm/Term;->mPathReceiver:Landroid/content/BroadcastReceiver;

    .line 142
    new-instance v0, Ljackpal/androidterm/Term$2;

    invoke-direct {v0, p0}, Ljackpal/androidterm/Term$2;-><init>(Ljackpal/androidterm/Term;)V

    iput-object v0, p0, Ljackpal/androidterm/Term;->mTSConnection:Landroid/content/ServiceConnection;

    .line 159
    iput v1, p0, Ljackpal/androidterm/Term;->mActionBarMode:I

    .line 194
    new-instance v0, Ljackpal/androidterm/Term$3;

    invoke-direct {v0, p0}, Ljackpal/androidterm/Term$3;-><init>(Ljackpal/androidterm/Term;)V

    iput-object v0, p0, Ljackpal/androidterm/Term;->mWinListItemSelected:Ljackpal/androidterm/compat/ActionBarCompat$OnNavigationListener;

    .line 207
    iput-boolean v1, p0, Ljackpal/androidterm/Term;->mHaveFullHwKeyboard:Z

    .line 242
    new-instance v0, Ljackpal/androidterm/Term$4;

    invoke-direct {v0, p0}, Ljackpal/androidterm/Term$4;-><init>(Ljackpal/androidterm/Term;)V

    iput-object v0, p0, Ljackpal/androidterm/Term;->mBackKeyListener:Landroid/view/View$OnKeyListener;

    .line 255
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Ljackpal/androidterm/Term;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Ljackpal/androidterm/Term;Landroid/os/Bundle;)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 78
    invoke-direct {p0, p1}, Ljackpal/androidterm/Term;->makePathFromBundle(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljackpal/androidterm/Term;)Ljackpal/androidterm/util/TermSettings;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    return-object v0
.end method

.method static synthetic access$1000(Ljackpal/androidterm/Term;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 78
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doCloseWindow()V

    return-void
.end method

.method static synthetic access$1100(Ljackpal/androidterm/Term;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Ljackpal/androidterm/Term;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Ljackpal/androidterm/Term;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget v0, p0, Ljackpal/androidterm/Term;->mPendingPathBroadcasts:I

    return v0
.end method

.method static synthetic access$210(Ljackpal/androidterm/Term;)I
    .registers 3
    .parameter "x0"

    .prologue
    .line 78
    iget v0, p0, Ljackpal/androidterm/Term;->mPendingPathBroadcasts:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Ljackpal/androidterm/Term;->mPendingPathBroadcasts:I

    return v0
.end method

.method static synthetic access$300(Ljackpal/androidterm/Term;)Ljackpal/androidterm/TermService;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Ljackpal/androidterm/Term;->mTermService:Ljackpal/androidterm/TermService;

    return-object v0
.end method

.method static synthetic access$302(Ljackpal/androidterm/Term;Ljackpal/androidterm/TermService;)Ljackpal/androidterm/TermService;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 78
    iput-object p1, p0, Ljackpal/androidterm/Term;->mTermService:Ljackpal/androidterm/TermService;

    return-object p1
.end method

.method static synthetic access$400(Ljackpal/androidterm/Term;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 78
    invoke-direct {p0}, Ljackpal/androidterm/Term;->populateViewFlipper()V

    return-void
.end method

.method static synthetic access$500(Ljackpal/androidterm/Term;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 78
    invoke-direct {p0}, Ljackpal/androidterm/Term;->populateWindowList()V

    return-void
.end method

.method static synthetic access$600(Ljackpal/androidterm/Term;)Ljackpal/androidterm/TermViewFlipper;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    return-object v0
.end method

.method static synthetic access$700(Ljackpal/androidterm/Term;)Ljackpal/androidterm/compat/ActionBarCompat;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Ljackpal/androidterm/Term;->mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

    return-object v0
.end method

.method static synthetic access$800(Ljackpal/androidterm/Term;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget v0, p0, Ljackpal/androidterm/Term;->mActionBarMode:I

    return v0
.end method

.method static synthetic access$900(Ljackpal/androidterm/Term;IIII)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3, p4}, Ljackpal/androidterm/Term;->doUIToggle(IIII)V

    return-void
.end method

.method private canPaste()Z
    .registers 3

    .prologue
    .line 895
    const-string v1, "clipboard"

    invoke-virtual {p0, v1}, Ljackpal/androidterm/Term;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 896
    .local v0, clip:Landroid/text/ClipboardManager;
    invoke-virtual {v0}, Landroid/text/ClipboardManager;->hasText()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 897
    const/4 v1, 0x1

    .line 899
    :goto_f
    return v1

    :cond_10
    const/4 v1, 0x0

    goto :goto_f
.end method

.method private checkHaveFullHwKeyboard(Landroid/content/res/Configuration;)Z
    .registers 5
    .parameter "c"

    .prologue
    const/4 v0, 0x1

    .line 583
    iget v1, p1, Landroid/content/res/Configuration;->keyboard:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_b

    iget v1, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v1, v0, :cond_b

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private confirmCloseWindow()V
    .registers 5

    .prologue
    .line 663
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 664
    .local v0, b:Landroid/app/AlertDialog$Builder;
    const v2, 0x1080027

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 665
    const v2, 0x7f06005f

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 666
    new-instance v1, Ljackpal/androidterm/Term$6;

    invoke-direct {v1, p0}, Ljackpal/androidterm/Term$6;-><init>(Ljackpal/androidterm/Term;)V

    .line 671
    .local v1, closeWindow:Ljava/lang/Runnable;
    const v2, 0x1040013

    new-instance v3, Ljackpal/androidterm/Term$7;

    invoke-direct {v3, p0, v1}, Ljackpal/androidterm/Term$7;-><init>(Ljackpal/androidterm/Term;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 677
    const v2, 0x1040009

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 678
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 679
    return-void
.end method

.method private createEmulatorView(Ljackpal/androidterm/emulatorview/TermSession;)Ljackpal/androidterm/TermView;
    .registers 5
    .parameter "session"

    .prologue
    .line 445
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 446
    .local v1, metrics:Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 447
    new-instance v0, Ljackpal/androidterm/TermView;

    invoke-direct {v0, p0, p1, v1}, Ljackpal/androidterm/TermView;-><init>(Landroid/content/Context;Ljackpal/androidterm/emulatorview/TermSession;Landroid/util/DisplayMetrics;)V

    .line 449
    .local v0, emulatorView:Ljackpal/androidterm/TermView;
    new-instance v2, Ljackpal/androidterm/Term$EmulatorViewGestureListener;

    invoke-direct {v2, p0, v0}, Ljackpal/androidterm/Term$EmulatorViewGestureListener;-><init>(Ljackpal/androidterm/Term;Ljackpal/androidterm/emulatorview/EmulatorView;)V

    invoke-virtual {v0, v2}, Ljackpal/androidterm/TermView;->setExtGestureListener(Landroid/view/GestureDetector$OnGestureListener;)V

    .line 450
    iget-object v2, p0, Ljackpal/androidterm/Term;->mBackKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v2}, Ljackpal/androidterm/TermView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 451
    invoke-virtual {p0, v0}, Ljackpal/androidterm/Term;->registerForContextMenu(Landroid/view/View;)V

    .line 453
    return-object v0
.end method

.method private createTermSession()Ljackpal/androidterm/emulatorview/TermSession;
    .registers 4

    .prologue
    .line 438
    iget-object v1, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    .line 439
    .local v1, settings:Ljackpal/androidterm/util/TermSettings;
    invoke-virtual {v1}, Ljackpal/androidterm/util/TermSettings;->getInitialCommand()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v1, v2}, Ljackpal/androidterm/Term;->createTermSession(Landroid/content/Context;Ljackpal/androidterm/util/TermSettings;Ljava/lang/String;)Ljackpal/androidterm/emulatorview/TermSession;

    move-result-object v0

    .line 440
    .local v0, session:Ljackpal/androidterm/emulatorview/TermSession;
    iget-object v2, p0, Ljackpal/androidterm/Term;->mTermService:Ljackpal/androidterm/TermService;

    invoke-virtual {v0, v2}, Ljackpal/androidterm/emulatorview/TermSession;->setFinishCallback(Ljackpal/androidterm/emulatorview/TermSession$FinishCallback;)V

    .line 441
    return-object v0
.end method

.method protected static createTermSession(Landroid/content/Context;Ljackpal/androidterm/util/TermSettings;Ljava/lang/String;)Ljackpal/androidterm/emulatorview/TermSession;
    .registers 5
    .parameter "context"
    .parameter "settings"
    .parameter "initialCommand"

    .prologue
    .line 430
    new-instance v0, Ljackpal/androidterm/ShellTermSession;

    invoke-direct {v0, p1, p2}, Ljackpal/androidterm/ShellTermSession;-><init>(Ljackpal/androidterm/util/TermSettings;Ljava/lang/String;)V

    .line 432
    .local v0, session:Ljackpal/androidterm/ShellTermSession;
    const v1, 0x7f060024

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljackpal/androidterm/ShellTermSession;->setProcessExitMessage(Ljava/lang/String;)V

    .line 434
    return-object v0
.end method

.method private doCloseWindow()V
    .registers 5

    .prologue
    .line 682
    iget-object v2, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    if-nez v2, :cond_5

    .line 700
    :cond_4
    :goto_4
    return-void

    .line 686
    :cond_5
    invoke-direct {p0}, Ljackpal/androidterm/Term;->getCurrentEmulatorView()Ljackpal/androidterm/emulatorview/EmulatorView;

    move-result-object v1

    .line 687
    .local v1, view:Ljackpal/androidterm/emulatorview/EmulatorView;
    if-eqz v1, :cond_4

    .line 690
    iget-object v2, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    iget-object v3, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v3}, Ljackpal/androidterm/TermViewFlipper;->getDisplayedChild()I

    move-result v3

    invoke-virtual {v2, v3}, Ljackpal/androidterm/util/SessionList;->remove(I)Ljackpal/androidterm/emulatorview/TermSession;

    move-result-object v0

    .line 691
    .local v0, session:Ljackpal/androidterm/emulatorview/TermSession;
    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/EmulatorView;->onPause()V

    .line 692
    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TermSession;->finish()V

    .line 693
    iget-object v2, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v2, v1}, Ljackpal/androidterm/TermViewFlipper;->removeView(Landroid/view/View;)V

    .line 694
    iget-object v2, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    invoke-virtual {v2}, Ljackpal/androidterm/util/SessionList;->size()I

    move-result v2

    if-nez v2, :cond_31

    .line 695
    const/4 v2, 0x1

    iput-boolean v2, p0, Ljackpal/androidterm/Term;->mStopServiceOnFinish:Z

    .line 696
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->finish()V

    goto :goto_4

    .line 698
    :cond_31
    iget-object v2, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v2}, Ljackpal/androidterm/TermViewFlipper;->showNext()V

    goto :goto_4
.end method

.method private doCopyAll()V
    .registers 3

    .prologue
    .line 944
    const-string v1, "clipboard"

    invoke-virtual {p0, v1}, Ljackpal/androidterm/Term;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 946
    .local v0, clip:Landroid/text/ClipboardManager;
    invoke-direct {p0}, Ljackpal/androidterm/Term;->getCurrentTermSession()Ljackpal/androidterm/emulatorview/TermSession;

    move-result-object v1

    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/TermSession;->getTranscriptText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    .line 947
    return-void
.end method

.method private doCreateNewWindow()V
    .registers 5

    .prologue
    .line 647
    iget-object v2, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    if-nez v2, :cond_c

    .line 648
    const-string v2, "Term"

    const-string v3, "Couldn\'t create new window because mTermSessions == null"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 660
    :goto_b
    return-void

    .line 652
    :cond_c
    invoke-direct {p0}, Ljackpal/androidterm/Term;->createTermSession()Ljackpal/androidterm/emulatorview/TermSession;

    move-result-object v0

    .line 653
    .local v0, session:Ljackpal/androidterm/emulatorview/TermSession;
    iget-object v2, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    invoke-virtual {v2, v0}, Ljackpal/androidterm/util/SessionList;->add(Ljackpal/androidterm/emulatorview/TermSession;)Z

    .line 655
    invoke-direct {p0, v0}, Ljackpal/androidterm/Term;->createEmulatorView(Ljackpal/androidterm/emulatorview/TermSession;)Ljackpal/androidterm/TermView;

    move-result-object v1

    .line 656
    .local v1, view:Ljackpal/androidterm/TermView;
    iget-object v2, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    invoke-virtual {v1, v2}, Ljackpal/androidterm/TermView;->updatePrefs(Ljackpal/androidterm/util/TermSettings;)V

    .line 658
    iget-object v2, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v2, v1}, Ljackpal/androidterm/TermViewFlipper;->addView(Landroid/view/View;)V

    .line 659
    iget-object v2, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    iget-object v3, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v3}, Ljackpal/androidterm/TermViewFlipper;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljackpal/androidterm/TermViewFlipper;->setDisplayedChild(I)V

    goto :goto_b
.end method

.method private doDocumentKeys()V
    .registers 11

    .prologue
    const/4 v2, 0x7

    .line 972
    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-direct {v8, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 973
    .local v8, dialog:Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 974
    .local v3, r:Landroid/content/res/Resources;
    const v0, 0x7f06005a

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 975
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    invoke-virtual {v0}, Ljackpal/androidterm/util/TermSettings;->getControlKeyId()I

    move-result v1

    const v4, 0x7f05000a

    const v5, 0x7f06005b

    const v6, 0x7f06005c

    const-string v7, "CTRLKEY"

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Ljackpal/androidterm/Term;->formatMessage(IILandroid/content/res/Resources;IIILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v0, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    invoke-virtual {v0}, Ljackpal/androidterm/util/TermSettings;->getFnKeyId()I

    move-result v1

    const v4, 0x7f05000b

    const v5, 0x7f06005d

    const v6, 0x7f06005e

    const-string v7, "FNKEY"

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Ljackpal/androidterm/Term;->formatMessage(IILandroid/content/res/Resources;IIILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 985
    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 986
    return-void
.end method

.method private doEmailTranscript()V
    .registers 10

    .prologue
    .line 914
    invoke-direct {p0}, Ljackpal/androidterm/Term;->getCurrentTermSession()Ljackpal/androidterm/emulatorview/TermSession;

    move-result-object v3

    .line 915
    .local v3, session:Ljackpal/androidterm/emulatorview/TermSession;
    if-eqz v3, :cond_6a

    .line 919
    const-string v0, "user@example.com"

    .line 920
    .local v0, addr:Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.intent.action.SENDTO"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mailto:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 924
    .local v2, intent:Landroid/content/Intent;
    const v6, 0x7f060066

    invoke-virtual {p0, v6}, Ljackpal/androidterm/Term;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 925
    .local v4, subject:Ljava/lang/String;
    invoke-virtual {v3}, Ljackpal/androidterm/emulatorview/TermSession;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 926
    .local v5, title:Ljava/lang/String;
    if-eqz v5, :cond_4a

    .line 927
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 929
    :cond_4a
    const-string v6, "android.intent.extra.SUBJECT"

    invoke-virtual {v2, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 930
    const-string v6, "android.intent.extra.TEXT"

    invoke-virtual {v3}, Ljackpal/androidterm/emulatorview/TermSession;->getTranscriptText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 933
    const v6, 0x7f060067

    :try_start_5f
    invoke-virtual {p0, v6}, Ljackpal/androidterm/Term;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljackpal/androidterm/Term;->startActivity(Landroid/content/Intent;)V
    :try_end_6a
    .catch Landroid/content/ActivityNotFoundException; {:try_start_5f .. :try_end_6a} :catch_6b

    .line 941
    .end local v0           #addr:Ljava/lang/String;
    .end local v2           #intent:Landroid/content/Intent;
    .end local v4           #subject:Ljava/lang/String;
    .end local v5           #title:Ljava/lang/String;
    :cond_6a
    :goto_6a
    return-void

    .line 935
    .restart local v0       #addr:Ljava/lang/String;
    .restart local v2       #intent:Landroid/content/Intent;
    .restart local v4       #subject:Ljava/lang/String;
    .restart local v5       #title:Ljava/lang/String;
    :catch_6b
    move-exception v1

    .line 936
    .local v1, e:Landroid/content/ActivityNotFoundException;
    const v6, 0x7f060068

    const/4 v7, 0x1

    invoke-static {p0, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_6a
.end method

.method private doPaste()V
    .registers 7

    .prologue
    .line 950
    const-string v4, "clipboard"

    invoke-virtual {p0, v4}, Ljackpal/androidterm/Term;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 952
    .local v0, clip:Landroid/text/ClipboardManager;
    invoke-virtual {v0}, Landroid/text/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 955
    .local v2, paste:Ljava/lang/CharSequence;
    :try_start_c
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-virtual {v4, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_15
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_c .. :try_end_15} :catch_22

    move-result-object v3

    .line 960
    .local v3, utf8:[B
    invoke-direct {p0}, Ljackpal/androidterm/Term;->getCurrentTermSession()Ljackpal/androidterm/emulatorview/TermSession;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljackpal/androidterm/emulatorview/TermSession;->write(Ljava/lang/String;)V

    .line 961
    .end local v3           #utf8:[B
    :goto_21
    return-void

    .line 956
    :catch_22
    move-exception v1

    .line 957
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    const-string v4, "Term"

    const-string v5, "UTF-8 encoding not found."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_21
.end method

.method private doPreferences()V
    .registers 3

    .prologue
    .line 903
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ljackpal/androidterm/TermPreferences;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Ljackpal/androidterm/Term;->startActivity(Landroid/content/Intent;)V

    .line 904
    return-void
.end method

.method private doResetTerminal()V
    .registers 2

    .prologue
    .line 907
    invoke-direct {p0}, Ljackpal/androidterm/Term;->getCurrentTermSession()Ljackpal/androidterm/emulatorview/TermSession;

    move-result-object v0

    .line 908
    .local v0, session:Ljackpal/androidterm/emulatorview/TermSession;
    if-eqz v0, :cond_9

    .line 909
    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TermSession;->reset()V

    .line 911
    :cond_9
    return-void
.end method

.method private doSendControlKey()V
    .registers 2

    .prologue
    .line 964
    invoke-direct {p0}, Ljackpal/androidterm/Term;->getCurrentEmulatorView()Ljackpal/androidterm/emulatorview/EmulatorView;

    move-result-object v0

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/EmulatorView;->sendControlKey()V

    .line 965
    return-void
.end method

.method private doSendFnKey()V
    .registers 2

    .prologue
    .line 968
    invoke-direct {p0}, Ljackpal/androidterm/Term;->getCurrentEmulatorView()Ljackpal/androidterm/emulatorview/EmulatorView;

    move-result-object v0

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/EmulatorView;->sendFnKey()V

    .line 969
    return-void
.end method

.method private doToggleActionBar()V
    .registers 3

    .prologue
    .line 1028
    iget-object v0, p0, Ljackpal/androidterm/Term;->mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

    .line 1029
    .local v0, bar:Ljackpal/androidterm/compat/ActionBarCompat;
    if-nez v0, :cond_5

    .line 1037
    :goto_4
    return-void

    .line 1032
    :cond_5
    invoke-virtual {v0}, Ljackpal/androidterm/compat/ActionBarCompat;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1033
    invoke-virtual {v0}, Ljackpal/androidterm/compat/ActionBarCompat;->hide()V

    goto :goto_4

    .line 1035
    :cond_f
    invoke-virtual {v0}, Ljackpal/androidterm/compat/ActionBarCompat;->show()V

    goto :goto_4
.end method

.method private doToggleSoftKeyboard()V
    .registers 4

    .prologue
    .line 1003
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Ljackpal/androidterm/Term;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1005
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 1007
    return-void
.end method

.method private doToggleWakeLock()V
    .registers 2

    .prologue
    .line 1010
    iget-object v0, p0, Ljackpal/androidterm/Term;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1011
    iget-object v0, p0, Ljackpal/androidterm/Term;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1015
    :goto_d
    invoke-static {p0}, Ljackpal/androidterm/compat/ActivityCompat;->invalidateOptionsMenu(Landroid/app/Activity;)V

    .line 1016
    return-void

    .line 1013
    :cond_11
    iget-object v0, p0, Ljackpal/androidterm/Term;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto :goto_d
.end method

.method private doToggleWifiLock()V
    .registers 2

    .prologue
    .line 1019
    iget-object v0, p0, Ljackpal/androidterm/Term;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1020
    iget-object v0, p0, Ljackpal/androidterm/Term;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 1024
    :goto_d
    invoke-static {p0}, Ljackpal/androidterm/compat/ActivityCompat;->invalidateOptionsMenu(Landroid/app/Activity;)V

    .line 1025
    return-void

    .line 1022
    :cond_11
    iget-object v0, p0, Ljackpal/androidterm/Term;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    goto :goto_d
.end method

.method private doUIToggle(IIII)V
    .registers 7
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 1040
    iget v0, p0, Ljackpal/androidterm/Term;->mActionBarMode:I

    packed-switch v0, :pswitch_data_3c

    .line 1063
    :cond_5
    :goto_5
    invoke-direct {p0}, Ljackpal/androidterm/Term;->getCurrentEmulatorView()Ljackpal/androidterm/emulatorview/EmulatorView;

    move-result-object v0

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/EmulatorView;->requestFocus()Z

    .line 1064
    :goto_c
    return-void

    .line 1042
    :pswitch_d
    sget v0, Ljackpal/androidterm/compat/AndroidCompat;->SDK:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1f

    iget-boolean v0, p0, Ljackpal/androidterm/Term;->mHaveFullHwKeyboard:Z

    if-nez v0, :cond_1b

    div-int/lit8 v0, p4, 0x2

    if-ge p2, v0, :cond_1f

    .line 1043
    :cond_1b
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->openOptionsMenu()V

    goto :goto_c

    .line 1046
    :cond_1f
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doToggleSoftKeyboard()V

    goto :goto_5

    .line 1050
    :pswitch_23
    iget-boolean v0, p0, Ljackpal/androidterm/Term;->mHaveFullHwKeyboard:Z

    if-nez v0, :cond_5

    .line 1051
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doToggleSoftKeyboard()V

    goto :goto_5

    .line 1055
    :pswitch_2b
    iget-boolean v0, p0, Ljackpal/androidterm/Term;->mHaveFullHwKeyboard:Z

    if-nez v0, :cond_33

    div-int/lit8 v0, p4, 0x2

    if-ge p2, v0, :cond_37

    .line 1056
    :cond_33
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doToggleActionBar()V

    goto :goto_c

    .line 1059
    :cond_37
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doToggleSoftKeyboard()V

    goto :goto_5

    .line 1040
    nop

    :pswitch_data_3c
    .packed-switch 0x0
        :pswitch_d
        :pswitch_23
        :pswitch_2b
    .end packed-switch
.end method

.method private formatMessage(IILandroid/content/res/Resources;IIILjava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "keyId"
    .parameter "disabledKeyId"
    .parameter "r"
    .parameter "arrayId"
    .parameter "enabledId"
    .parameter "disabledId"
    .parameter "regex"

    .prologue
    .line 992
    if-ne p1, p2, :cond_7

    .line 993
    invoke-virtual {p3, p6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 999
    :goto_6
    return-object v2

    .line 995
    :cond_7
    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 996
    .local v1, keyNames:[Ljava/lang/String;
    aget-object v0, v1, p1

    .line 997
    .local v0, keyName:Ljava/lang/String;
    invoke-virtual {p3, p5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 998
    .local v3, template:Ljava/lang/String;
    invoke-virtual {v3, p7, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 999
    .local v2, result:Ljava/lang/String;
    goto :goto_6
.end method

.method private getCurrentEmulatorView()Ljackpal/androidterm/emulatorview/EmulatorView;
    .registers 2

    .prologue
    .line 466
    iget-object v0, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v0}, Ljackpal/androidterm/TermViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Ljackpal/androidterm/emulatorview/EmulatorView;

    return-object v0
.end method

.method private getCurrentTermSession()Ljackpal/androidterm/emulatorview/TermSession;
    .registers 3

    .prologue
    .line 457
    iget-object v0, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    .line 458
    .local v0, sessions:Ljackpal/androidterm/util/SessionList;
    if-nez v0, :cond_6

    .line 459
    const/4 v1, 0x0

    .line 461
    :goto_5
    return-object v1

    :cond_6
    iget-object v1, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v1}, Ljackpal/androidterm/TermViewFlipper;->getDisplayedChild()I

    move-result v1

    invoke-virtual {v0, v1}, Ljackpal/androidterm/util/SessionList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljackpal/androidterm/emulatorview/TermSession;

    goto :goto_5
.end method

.method private makePathFromBundle(Landroid/os/Bundle;)Ljava/lang/String;
    .registers 12
    .parameter "extras"

    .prologue
    .line 325
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Landroid/os/Bundle;->size()I

    move-result v8

    if-nez v8, :cond_b

    .line 326
    :cond_8
    const-string v8, ""

    .line 343
    :goto_a
    return-object v8

    .line 329
    :cond_b
    invoke-virtual {p1}, Landroid/os/Bundle;->size()I

    move-result v8

    new-array v5, v8, [Ljava/lang/String;

    .line 330
    .local v5, keys:[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    .end local v5           #keys:[Ljava/lang/String;
    check-cast v5, [Ljava/lang/String;

    .line 331
    .restart local v5       #keys:[Ljava/lang/String;
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v8}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v1

    .line 332
    .local v1, collator:Ljava/text/Collator;
    invoke-static {v5, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 334
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 335
    .local v7, path:Ljava/lang/StringBuilder;
    move-object v0, v5

    .local v0, arr$:[Ljava/lang/String;
    array-length v6, v0

    .local v6, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_2c
    if-ge v3, v6, :cond_49

    aget-object v4, v0, v3

    .line 336
    .local v4, key:Ljava/lang/String;
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 337
    .local v2, dir:Ljava/lang/String;
    if-eqz v2, :cond_46

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_46

    .line 338
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    :cond_46
    add-int/lit8 v3, v3, 0x1

    goto :goto_2c

    .line 343
    .end local v2           #dir:Ljava/lang/String;
    .end local v4           #key:Ljava/lang/String;
    :cond_49
    const/4 v8, 0x0

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v8

    goto :goto_a
.end method

.method private populateViewFlipper()V
    .registers 10

    .prologue
    .line 347
    iget-object v7, p0, Ljackpal/androidterm/Term;->mTermService:Ljackpal/androidterm/TermService;

    if-eqz v7, :cond_6e

    .line 348
    iget-object v7, p0, Ljackpal/androidterm/Term;->mTermService:Ljackpal/androidterm/TermService;

    invoke-virtual {v7}, Ljackpal/androidterm/TermService;->getSessions()Ljackpal/androidterm/util/SessionList;

    move-result-object v7

    iput-object v7, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    .line 349
    iget-object v7, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    invoke-virtual {v7, p0}, Ljackpal/androidterm/util/SessionList;->addCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)V

    .line 351
    iget-object v7, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    invoke-virtual {v7}, Ljackpal/androidterm/util/SessionList;->size()I

    move-result v7

    if-nez v7, :cond_22

    .line 352
    iget-object v7, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    invoke-direct {p0}, Ljackpal/androidterm/Term;->createTermSession()Ljackpal/androidterm/emulatorview/TermSession;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljackpal/androidterm/util/SessionList;->add(Ljackpal/androidterm/emulatorview/TermSession;)Z

    .line 355
    :cond_22
    iget-object v7, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    invoke-virtual {v7}, Ljackpal/androidterm/util/SessionList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_28
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljackpal/androidterm/emulatorview/TermSession;

    .line 356
    .local v4, session:Ljackpal/androidterm/emulatorview/TermSession;
    invoke-direct {p0, v4}, Ljackpal/androidterm/Term;->createEmulatorView(Ljackpal/androidterm/emulatorview/TermSession;)Ljackpal/androidterm/TermView;

    move-result-object v6

    .line 357
    .local v6, view:Ljackpal/androidterm/emulatorview/EmulatorView;
    iget-object v7, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v7, v6}, Ljackpal/androidterm/TermViewFlipper;->addView(Landroid/view/View;)V

    goto :goto_28

    .line 360
    .end local v4           #session:Ljackpal/androidterm/emulatorview/TermSession;
    .end local v6           #view:Ljackpal/androidterm/emulatorview/EmulatorView;
    :cond_3e
    invoke-direct {p0}, Ljackpal/androidterm/Term;->updatePrefs()V

    .line 362
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 363
    .local v3, intent:Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getFlags()I

    move-result v1

    .line 364
    .local v1, flags:I
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 365
    .local v0, action:Ljava/lang/String;
    const/high16 v7, 0x10

    and-int/2addr v7, v1

    if-nez v7, :cond_69

    if-eqz v0, :cond_69

    .line 367
    const-string v7, "jackpal.androidterm.private.OPEN_NEW_WINDOW"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6f

    .line 368
    iget-object v7, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    iget-object v8, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    invoke-virtual {v8}, Ljackpal/androidterm/util/SessionList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Ljackpal/androidterm/TermViewFlipper;->setDisplayedChild(I)V

    .line 377
    :cond_69
    :goto_69
    iget-object v7, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v7}, Ljackpal/androidterm/TermViewFlipper;->resumeCurrentView()V

    .line 379
    .end local v0           #action:Ljava/lang/String;
    .end local v1           #flags:I
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #intent:Landroid/content/Intent;
    :cond_6e
    return-void

    .line 369
    .restart local v0       #action:Ljava/lang/String;
    .restart local v1       #flags:I
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v3       #intent:Landroid/content/Intent;
    :cond_6f
    const-string v7, "jackpal.androidterm.private.SWITCH_WINDOW"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_69

    .line 370
    const-string v7, "jackpal.androidterm.private.target_window"

    const/4 v8, -0x1

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 371
    .local v5, target:I
    if-ltz v5, :cond_69

    .line 372
    iget-object v7, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v7, v5}, Ljackpal/androidterm/TermViewFlipper;->setDisplayedChild(I)V

    goto :goto_69
.end method

.method private populateWindowList()V
    .registers 6

    .prologue
    .line 382
    iget-object v3, p0, Ljackpal/androidterm/Term;->mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

    if-nez v3, :cond_5

    .line 404
    :cond_4
    :goto_4
    return-void

    .line 387
    :cond_5
    iget-object v3, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    if-eqz v3, :cond_4

    .line 388
    iget-object v3, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v3}, Ljackpal/androidterm/TermViewFlipper;->getDisplayedChild()I

    move-result v1

    .line 389
    .local v1, position:I
    iget-object v0, p0, Ljackpal/androidterm/Term;->mWinListAdapter:Ljackpal/androidterm/WindowListAdapter;

    .line 390
    .local v0, adapter:Ljackpal/androidterm/WindowListAdapter;
    if-nez v0, :cond_36

    .line 391
    new-instance v0, Ljackpal/androidterm/Term$WindowListActionBarAdapter;

    .end local v0           #adapter:Ljackpal/androidterm/WindowListAdapter;
    iget-object v3, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    invoke-direct {v0, p0, v3}, Ljackpal/androidterm/Term$WindowListActionBarAdapter;-><init>(Ljackpal/androidterm/Term;Ljackpal/androidterm/util/SessionList;)V

    .line 392
    .restart local v0       #adapter:Ljackpal/androidterm/WindowListAdapter;
    iput-object v0, p0, Ljackpal/androidterm/Term;->mWinListAdapter:Ljackpal/androidterm/WindowListAdapter;

    .line 394
    iget-object v2, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    .line 395
    .local v2, sessions:Ljackpal/androidterm/util/SessionList;
    invoke-virtual {v2, v0}, Ljackpal/androidterm/util/SessionList;->addCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)V

    .line 396
    invoke-virtual {v2, v0}, Ljackpal/androidterm/util/SessionList;->addTitleChangedListener(Ljackpal/androidterm/emulatorview/UpdateCallback;)V

    .line 397
    iget-object v3, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v3, v0}, Ljackpal/androidterm/TermViewFlipper;->addCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)V

    .line 398
    iget-object v3, p0, Ljackpal/androidterm/Term;->mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

    iget-object v4, p0, Ljackpal/androidterm/Term;->mWinListItemSelected:Ljackpal/androidterm/compat/ActionBarCompat$OnNavigationListener;

    invoke-virtual {v3, v0, v4}, Ljackpal/androidterm/compat/ActionBarCompat;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Ljackpal/androidterm/compat/ActionBarCompat$OnNavigationListener;)V

    .line 402
    .end local v2           #sessions:Ljackpal/androidterm/util/SessionList;
    :goto_30
    iget-object v3, p0, Ljackpal/androidterm/Term;->mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

    invoke-virtual {v3, v1}, Ljackpal/androidterm/compat/ActionBarCompat;->setSelectedNavigationItem(I)V

    goto :goto_4

    .line 400
    :cond_36
    iget-object v3, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    invoke-virtual {v0, v3}, Ljackpal/androidterm/WindowListAdapter;->setSessions(Ljackpal/androidterm/util/SessionList;)V

    goto :goto_30
.end method

.method private restart()V
    .registers 2

    .prologue
    .line 425
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljackpal/androidterm/Term;->startActivity(Landroid/content/Intent;)V

    .line 426
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->finish()V

    .line 427
    return-void
.end method

.method private updatePrefs()V
    .registers 13

    .prologue
    const/16 v10, 0x400

    .line 470
    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    .line 471
    .local v4, metrics:Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v9

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 472
    new-instance v1, Ljackpal/androidterm/emulatorview/ColorScheme;

    iget-object v9, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    invoke-virtual {v9}, Ljackpal/androidterm/util/TermSettings;->getColorScheme()[I

    move-result-object v9

    invoke-direct {v1, v9}, Ljackpal/androidterm/emulatorview/ColorScheme;-><init>([I)V

    .line 474
    .local v1, colorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;
    iget-object v9, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    iget-object v11, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    invoke-virtual {v9, v11}, Ljackpal/androidterm/TermViewFlipper;->updatePrefs(Ljackpal/androidterm/util/TermSettings;)V

    .line 476
    iget-object v9, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v9}, Ljackpal/androidterm/TermViewFlipper;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_2a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_44

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    .local v7, v:Landroid/view/View;
    move-object v9, v7

    .line 477
    check-cast v9, Ljackpal/androidterm/emulatorview/EmulatorView;

    invoke-virtual {v9, v4}, Ljackpal/androidterm/emulatorview/EmulatorView;->setDensity(Landroid/util/DisplayMetrics;)V

    .line 478
    check-cast v7, Ljackpal/androidterm/TermView;

    .end local v7           #v:Landroid/view/View;
    iget-object v9, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    invoke-virtual {v7, v9}, Ljackpal/androidterm/TermView;->updatePrefs(Ljackpal/androidterm/util/TermSettings;)V

    goto :goto_2a

    .line 481
    :cond_44
    iget-object v9, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    if-eqz v9, :cond_62

    .line 482
    iget-object v9, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    invoke-virtual {v9}, Ljackpal/androidterm/util/SessionList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_62

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljackpal/androidterm/emulatorview/TermSession;

    .line 483
    .local v6, session:Ljackpal/androidterm/emulatorview/TermSession;
    check-cast v6, Ljackpal/androidterm/ShellTermSession;

    .end local v6           #session:Ljackpal/androidterm/emulatorview/TermSession;
    iget-object v9, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    invoke-virtual {v6, v9}, Ljackpal/androidterm/ShellTermSession;->updatePrefs(Ljackpal/androidterm/util/TermSettings;)V

    goto :goto_4e

    .line 488
    :cond_62
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->getWindow()Landroid/view/Window;

    move-result-object v8

    .line 489
    .local v8, win:Landroid/view/Window;
    invoke-virtual {v8}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    .line 490
    .local v5, params:Landroid/view/WindowManager$LayoutParams;
    const/16 v0, 0x400

    .line 491
    .local v0, FULLSCREEN:I
    iget-object v9, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    invoke-virtual {v9}, Ljackpal/androidterm/util/TermSettings;->showStatusBar()Z

    move-result v9

    if-eqz v9, :cond_93

    const/4 v2, 0x0

    .line 492
    .local v2, desiredFlag:I
    :goto_75
    iget v9, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v9, v9, 0x400

    if-ne v2, v9, :cond_8b

    sget v9, Ljackpal/androidterm/compat/AndroidCompat;->SDK:I

    const/16 v11, 0xb

    if-lt v9, v11, :cond_92

    iget v9, p0, Ljackpal/androidterm/Term;->mActionBarMode:I

    iget-object v11, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    invoke-virtual {v11}, Ljackpal/androidterm/util/TermSettings;->actionBarMode()I

    move-result v11

    if-eq v9, v11, :cond_92

    .line 493
    :cond_8b
    iget-boolean v9, p0, Ljackpal/androidterm/Term;->mAlreadyStarted:Z

    if-eqz v9, :cond_95

    .line 496
    invoke-direct {p0}, Ljackpal/androidterm/Term;->restart()V

    .line 505
    :cond_92
    :goto_92
    return-void

    .end local v2           #desiredFlag:I
    :cond_93
    move v2, v10

    .line 491
    goto :goto_75

    .line 498
    .restart local v2       #desiredFlag:I
    :cond_95
    invoke-virtual {v8, v2, v10}, Landroid/view/Window;->setFlags(II)V

    .line 499
    iget v9, p0, Ljackpal/androidterm/Term;->mActionBarMode:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_92

    .line 500
    iget-object v9, p0, Ljackpal/androidterm/Term;->mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

    invoke-virtual {v9}, Ljackpal/androidterm/compat/ActionBarCompat;->hide()V

    goto :goto_92
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 8
    .parameter "request"
    .parameter "result"
    .parameter "data"

    .prologue
    const/4 v3, -0x1

    .line 704
    packed-switch p1, :pswitch_data_38

    .line 724
    :cond_4
    :goto_4
    return-void

    .line 706
    :pswitch_5
    if-ne p2, v3, :cond_25

    if-eqz p3, :cond_25

    .line 707
    const-string v1, "jackpal.androidterm.window_id"

    const/4 v2, -0x2

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 708
    .local v0, position:I
    if-ltz v0, :cond_15

    .line 710
    iput v0, p0, Ljackpal/androidterm/Term;->onResumeSelectWindow:I

    goto :goto_4

    .line 711
    :cond_15
    if-ne v0, v3, :cond_4

    .line 712
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doCreateNewWindow()V

    .line 713
    iget-object v1, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    invoke-virtual {v1}, Ljackpal/androidterm/util/SessionList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Ljackpal/androidterm/Term;->onResumeSelectWindow:I

    goto :goto_4

    .line 717
    .end local v0           #position:I
    :cond_25
    iget-object v1, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    if-eqz v1, :cond_31

    iget-object v1, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    invoke-virtual {v1}, Ljackpal/androidterm/util/SessionList;->size()I

    move-result v1

    if-nez v1, :cond_4

    .line 718
    :cond_31
    const/4 v1, 0x1

    iput-boolean v1, p0, Ljackpal/androidterm/Term;->mStopServiceOnFinish:Z

    .line 719
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->finish()V

    goto :goto_4

    .line 704
    :pswitch_data_38
    .packed-switch 0x1
        :pswitch_5
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    .prologue
    .line 589
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 591
    invoke-direct {p0, p1}, Ljackpal/androidterm/Term;->checkHaveFullHwKeyboard(Landroid/content/res/Configuration;)Z

    move-result v1

    iput-boolean v1, p0, Ljackpal/androidterm/Term;->mHaveFullHwKeyboard:Z

    .line 593
    iget-object v1, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v1}, Ljackpal/androidterm/TermViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Ljackpal/androidterm/emulatorview/EmulatorView;

    .line 594
    .local v0, v:Ljackpal/androidterm/emulatorview/EmulatorView;
    if-eqz v0, :cond_17

    .line 595
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljackpal/androidterm/emulatorview/EmulatorView;->updateSize(Z)V

    .line 598
    :cond_17
    iget-object v1, p0, Ljackpal/androidterm/Term;->mWinListAdapter:Ljackpal/androidterm/WindowListAdapter;

    if-eqz v1, :cond_20

    .line 600
    iget-object v1, p0, Ljackpal/androidterm/Term;->mWinListAdapter:Ljackpal/androidterm/WindowListAdapter;

    invoke-virtual {v1}, Ljackpal/androidterm/WindowListAdapter;->notifyDataSetChanged()V

    .line 602
    :cond_20
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 794
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_26

    .line 811
    invoke-super {p0, p1}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_c
    return v0

    .line 796
    :pswitch_d
    invoke-direct {p0}, Ljackpal/androidterm/Term;->getCurrentEmulatorView()Ljackpal/androidterm/emulatorview/EmulatorView;

    move-result-object v1

    invoke-virtual {v1}, Ljackpal/androidterm/emulatorview/EmulatorView;->toggleSelectingText()V

    goto :goto_c

    .line 799
    :pswitch_15
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doCopyAll()V

    goto :goto_c

    .line 802
    :pswitch_19
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doPaste()V

    goto :goto_c

    .line 805
    :pswitch_1d
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doSendControlKey()V

    goto :goto_c

    .line 808
    :pswitch_21
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doSendFnKey()V

    goto :goto_c

    .line 794
    nop

    :pswitch_data_26
    .packed-switch 0x0
        :pswitch_d
        :pswitch_15
        :pswitch_19
        :pswitch_1d
        :pswitch_21
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 17
    .parameter "icicle"

    .prologue
    .line 259
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 260
    const-string v0, "Term"

    const-string v2, "onCreate"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Ljackpal/androidterm/Term;->mPrefs:Landroid/content/SharedPreferences;

    .line 262
    new-instance v0, Ljackpal/androidterm/util/TermSettings;

    invoke-virtual {p0}, Ljackpal/androidterm/Term;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v4, p0, Ljackpal/androidterm/Term;->mPrefs:Landroid/content/SharedPreferences;

    invoke-direct {v0, v2, v4}, Ljackpal/androidterm/util/TermSettings;-><init>(Landroid/content/res/Resources;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    .line 264
    new-instance v1, Landroid/content/Intent;

    const-string v0, "jackpal.androidterm.broadcast.APPEND_TO_PATH"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 265
    .local v1, broadcast:Landroid/content/Intent;
    sget v0, Ljackpal/androidterm/compat/AndroidCompat;->SDK:I

    const/16 v2, 0xc

    if-lt v0, v2, :cond_2f

    .line 266
    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 268
    :cond_2f
    iget v0, p0, Ljackpal/androidterm/Term;->mPendingPathBroadcasts:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljackpal/androidterm/Term;->mPendingPathBroadcasts:I

    .line 269
    const-string v2, "jackpal.androidterm.permission.APPEND_TO_PATH"

    iget-object v3, p0, Ljackpal/androidterm/Term;->mPathReceiver:Landroid/content/BroadcastReceiver;

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Ljackpal/androidterm/Term;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 271
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 272
    .end local v1           #broadcast:Landroid/content/Intent;
    .local v3, broadcast:Landroid/content/Intent;
    const-string v0, "jackpal.androidterm.broadcast.PREPEND_TO_PATH"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    iget v0, p0, Ljackpal/androidterm/Term;->mPendingPathBroadcasts:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljackpal/androidterm/Term;->mPendingPathBroadcasts:I

    .line 274
    const-string v4, "jackpal.androidterm.permission.PREPEND_TO_PATH"

    iget-object v5, p0, Ljackpal/androidterm/Term;->mPathReceiver:Landroid/content/BroadcastReceiver;

    const/4 v6, 0x0

    const/4 v7, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, p0

    invoke-virtual/range {v2 .. v9}, Ljackpal/androidterm/Term;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 276
    new-instance v0, Landroid/content/Intent;

    const-class v2, Ljackpal/androidterm/TermService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Ljackpal/androidterm/Term;->TSIntent:Landroid/content/Intent;

    .line 277
    iget-object v0, p0, Ljackpal/androidterm/Term;->TSIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Ljackpal/androidterm/Term;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 279
    iget-object v0, p0, Ljackpal/androidterm/Term;->TSIntent:Landroid/content/Intent;

    iget-object v2, p0, Ljackpal/androidterm/Term;->mTSConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {p0, v0, v2, v4}, Ljackpal/androidterm/Term;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_7d

    .line 280
    const-string v0, "Term"

    const-string v2, "bind to service failed!"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_7d
    sget v0, Ljackpal/androidterm/compat/AndroidCompat;->SDK:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_8e

    .line 284
    iget-object v0, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    invoke-virtual {v0}, Ljackpal/androidterm/util/TermSettings;->actionBarMode()I

    move-result v11

    .line 285
    .local v11, actionBarMode:I
    iput v11, p0, Ljackpal/androidterm/Term;->mActionBarMode:I

    .line 286
    packed-switch v11, :pswitch_data_104

    .line 296
    .end local v11           #actionBarMode:I
    :cond_8e
    :goto_8e
    const/high16 v0, 0x7f03

    invoke-virtual {p0, v0}, Ljackpal/androidterm/Term;->setContentView(I)V

    .line 297
    const/high16 v0, 0x7f0b

    invoke-virtual {p0, v0}, Ljackpal/androidterm/Term;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Ljackpal/androidterm/TermViewFlipper;

    iput-object v0, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    .line 299
    const-string v0, "power"

    invoke-virtual {p0, v0}, Ljackpal/androidterm/Term;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/os/PowerManager;

    .line 300
    .local v12, pm:Landroid/os/PowerManager;
    const/4 v0, 0x1

    const-string v2, "Term"

    invoke-virtual {v12, v0, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Ljackpal/androidterm/Term;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 301
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Ljackpal/androidterm/Term;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/net/wifi/WifiManager;

    .line 302
    .local v14, wm:Landroid/net/wifi/WifiManager;
    const/4 v13, 0x1

    .line 303
    .local v13, wifiLockMode:I
    sget v0, Ljackpal/androidterm/compat/AndroidCompat;->SDK:I

    const/16 v2, 0xc

    if-lt v0, v2, :cond_be

    .line 304
    const/4 v13, 0x3

    .line 306
    :cond_be
    const-string v0, "Term"

    invoke-virtual {v14, v13, v0}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Ljackpal/androidterm/Term;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 308
    invoke-static {p0}, Ljackpal/androidterm/compat/ActivityCompat;->getActionBar(Landroid/app/Activity;)Ljackpal/androidterm/compat/ActionBarCompat;

    move-result-object v10

    .line 309
    .local v10, actionBar:Ljackpal/androidterm/compat/ActionBarCompat;
    if-eqz v10, :cond_e0

    .line 310
    iput-object v10, p0, Ljackpal/androidterm/Term;->mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

    .line 311
    const/4 v0, 0x1

    invoke-virtual {v10, v0}, Ljackpal/androidterm/compat/ActionBarCompat;->setNavigationMode(I)V

    .line 312
    const/4 v0, 0x0

    const/16 v2, 0x8

    invoke-virtual {v10, v0, v2}, Ljackpal/androidterm/compat/ActionBarCompat;->setDisplayOptions(II)V

    .line 313
    iget v0, p0, Ljackpal/androidterm/Term;->mActionBarMode:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_e0

    .line 314
    invoke-virtual {v10}, Ljackpal/androidterm/compat/ActionBarCompat;->hide()V

    .line 318
    :cond_e0
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Ljackpal/androidterm/Term;->checkHaveFullHwKeyboard(Landroid/content/res/Configuration;)Z

    move-result v0

    iput-boolean v0, p0, Ljackpal/androidterm/Term;->mHaveFullHwKeyboard:Z

    .line 320
    invoke-direct {p0}, Ljackpal/androidterm/Term;->updatePrefs()V

    .line 321
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljackpal/androidterm/Term;->mAlreadyStarted:Z

    .line 322
    return-void

    .line 288
    .end local v10           #actionBar:Ljackpal/androidterm/compat/ActionBarCompat;
    .end local v12           #pm:Landroid/os/PowerManager;
    .end local v13           #wifiLockMode:I
    .end local v14           #wm:Landroid/net/wifi/WifiManager;
    .restart local v11       #actionBarMode:I
    :pswitch_f5
    const v0, 0x7f090001

    invoke-virtual {p0, v0}, Ljackpal/androidterm/Term;->setTheme(I)V

    goto :goto_8e

    .line 291
    :pswitch_fc
    const v0, 0x7f090002

    invoke-virtual {p0, v0}, Ljackpal/androidterm/Term;->setTheme(I)V

    goto :goto_8e

    .line 286
    nop

    :pswitch_data_104
    .packed-switch 0x1
        :pswitch_f5
        :pswitch_fc
    .end packed-switch
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 8
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 780
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 781
    const v0, 0x7f06001c

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 782
    const v0, 0x7f06001d

    invoke-interface {p1, v2, v2, v2, v0}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 783
    const/4 v0, 0x1

    const v1, 0x7f06001e

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 784
    const v0, 0x7f06001f

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 785
    const/4 v0, 0x3

    const v1, 0x7f060020

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 786
    const/4 v0, 0x4

    const v1, 0x7f060021

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 787
    invoke-direct {p0}, Ljackpal/androidterm/Term;->canPaste()Z

    move-result v0

    if-nez v0, :cond_39

    .line 788
    invoke-interface {p1, v3}, Landroid/view/ContextMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 790
    :cond_39
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 5
    .parameter "menu"

    .prologue
    const/4 v2, 0x1

    .line 606
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0a

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 607
    const v0, 0x7f0b0004

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljackpal/androidterm/compat/MenuItemCompat;->setShowAsAction(Landroid/view/MenuItem;I)V

    .line 608
    const v0, 0x7f0b0005

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0, v2}, Ljackpal/androidterm/compat/MenuItemCompat;->setShowAsAction(Landroid/view/MenuItem;I)V

    .line 609
    return v2
.end method

.method public onDestroy()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 408
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 409
    iget-object v0, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v0}, Ljackpal/androidterm/TermViewFlipper;->removeAllViews()V

    .line 410
    iget-object v0, p0, Ljackpal/androidterm/Term;->mTSConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Ljackpal/androidterm/Term;->unbindService(Landroid/content/ServiceConnection;)V

    .line 411
    iget-boolean v0, p0, Ljackpal/androidterm/Term;->mStopServiceOnFinish:Z

    if-eqz v0, :cond_17

    .line 412
    iget-object v0, p0, Ljackpal/androidterm/Term;->TSIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Ljackpal/androidterm/Term;->stopService(Landroid/content/Intent;)Z

    .line 414
    :cond_17
    iput-object v1, p0, Ljackpal/androidterm/Term;->mTermService:Ljackpal/androidterm/TermService;

    .line 415
    iput-object v1, p0, Ljackpal/androidterm/Term;->mTSConnection:Landroid/content/ServiceConnection;

    .line 416
    iget-object v0, p0, Ljackpal/androidterm/Term;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 417
    iget-object v0, p0, Ljackpal/androidterm/Term;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 419
    :cond_28
    iget-object v0, p0, Ljackpal/androidterm/Term;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 420
    iget-object v0, p0, Ljackpal/androidterm/Term;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 422
    :cond_35
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v0, 0x1

    .line 820
    sget v1, Ljackpal/androidterm/compat/AndroidCompat;->SDK:I

    const/4 v2, 0x5

    if-ge v1, v2, :cond_c

    const/4 v1, 0x4

    if-ne p1, v1, :cond_c

    .line 825
    iput-boolean v0, p0, Ljackpal/androidterm/Term;->mBackKeyPressed:Z

    .line 828
    :goto_b
    return v0

    :cond_c
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_b
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 834
    sparse-switch p1, :sswitch_data_58

    .line 868
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_9
    :goto_9
    return v0

    .line 836
    :sswitch_a
    sget v2, Ljackpal/androidterm/compat/AndroidCompat;->SDK:I

    const/4 v3, 0x5

    if-ge v2, v3, :cond_15

    .line 837
    iget-boolean v2, p0, Ljackpal/androidterm/Term;->mBackKeyPressed:Z

    if-eqz v2, :cond_9

    .line 842
    iput-boolean v0, p0, Ljackpal/androidterm/Term;->mBackKeyPressed:Z

    .line 844
    :cond_15
    iget v2, p0, Ljackpal/androidterm/Term;->mActionBarMode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_29

    iget-object v2, p0, Ljackpal/androidterm/Term;->mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

    invoke-virtual {v2}, Ljackpal/androidterm/compat/ActionBarCompat;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_29

    .line 845
    iget-object v0, p0, Ljackpal/androidterm/Term;->mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

    invoke-virtual {v0}, Ljackpal/androidterm/compat/ActionBarCompat;->hide()V

    move v0, v1

    .line 846
    goto :goto_9

    .line 848
    :cond_29
    iget-object v2, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    invoke-virtual {v2}, Ljackpal/androidterm/util/TermSettings;->getBackKeyAction()I

    move-result v2

    packed-switch v2, :pswitch_data_62

    goto :goto_9

    .line 850
    :pswitch_33
    iput-boolean v1, p0, Ljackpal/androidterm/Term;->mStopServiceOnFinish:Z

    .line 852
    :pswitch_35
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->finish()V

    move v0, v1

    .line 853
    goto :goto_9

    .line 855
    :pswitch_3a
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doCloseWindow()V

    move v0, v1

    .line 856
    goto :goto_9

    .line 861
    :sswitch_3f
    iget-object v0, p0, Ljackpal/androidterm/Term;->mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

    if-eqz v0, :cond_52

    iget-object v0, p0, Ljackpal/androidterm/Term;->mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

    invoke-virtual {v0}, Ljackpal/androidterm/compat/ActionBarCompat;->isShowing()Z

    move-result v0

    if-nez v0, :cond_52

    .line 862
    iget-object v0, p0, Ljackpal/androidterm/Term;->mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

    invoke-virtual {v0}, Ljackpal/androidterm/compat/ActionBarCompat;->show()V

    move v0, v1

    .line 863
    goto :goto_9

    .line 865
    :cond_52
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_9

    .line 834
    nop

    :sswitch_data_58
    .sparse-switch
        0x4 -> :sswitch_a
        0x52 -> :sswitch_3f
    .end sparse-switch

    .line 848
    :pswitch_data_62
    .packed-switch 0x0
        :pswitch_33
        :pswitch_3a
        :pswitch_35
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 10
    .parameter "intent"

    .prologue
    .line 728
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v6

    const/high16 v7, 0x10

    and-int/2addr v6, v7

    if-eqz v6, :cond_a

    .line 758
    :cond_9
    :goto_9
    return-void

    .line 733
    :cond_a
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 734
    .local v0, action:Ljava/lang/String;
    if-eqz v0, :cond_9

    .line 738
    const-string v6, "jackpal.androidterm.private.OPEN_NEW_WINDOW"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_34

    .line 740
    iget-object v3, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    .line 741
    .local v3, sessions:Ljackpal/androidterm/util/SessionList;
    if-eqz v3, :cond_9

    .line 745
    invoke-virtual {v3}, Ljackpal/androidterm/util/SessionList;->size()I

    move-result v6

    add-int/lit8 v1, v6, -0x1

    .line 747
    .local v1, position:I
    invoke-virtual {v3, v1}, Ljackpal/androidterm/util/SessionList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljackpal/androidterm/emulatorview/TermSession;

    .line 748
    .local v2, session:Ljackpal/androidterm/emulatorview/TermSession;
    invoke-direct {p0, v2}, Ljackpal/androidterm/Term;->createEmulatorView(Ljackpal/androidterm/emulatorview/TermSession;)Ljackpal/androidterm/TermView;

    move-result-object v5

    .line 750
    .local v5, view:Ljackpal/androidterm/emulatorview/EmulatorView;
    iget-object v6, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v6, v5}, Ljackpal/androidterm/TermViewFlipper;->addView(Landroid/view/View;)V

    .line 751
    iput v1, p0, Ljackpal/androidterm/Term;->onResumeSelectWindow:I

    goto :goto_9

    .line 752
    .end local v1           #position:I
    .end local v2           #session:Ljackpal/androidterm/emulatorview/TermSession;
    .end local v3           #sessions:Ljackpal/androidterm/util/SessionList;
    .end local v5           #view:Ljackpal/androidterm/emulatorview/EmulatorView;
    :cond_34
    const-string v6, "jackpal.androidterm.private.SWITCH_WINDOW"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 753
    const-string v6, "jackpal.androidterm.private.target_window"

    const/4 v7, -0x1

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 754
    .local v4, target:I
    if-ltz v4, :cond_9

    .line 755
    iput v4, p0, Ljackpal/androidterm/Term;->onResumeSelectWindow:I

    goto :goto_9
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 7
    .parameter "item"

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 614
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 615
    .local v0, id:I
    const v2, 0x7f0b0009

    if-ne v0, v2, :cond_1d

    .line 616
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doPreferences()V

    .line 640
    :cond_e
    :goto_e
    iget v2, p0, Ljackpal/androidterm/Term;->mActionBarMode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_18

    .line 641
    iget-object v2, p0, Ljackpal/androidterm/Term;->mActionBar:Ljackpal/androidterm/compat/ActionBarCompat;

    invoke-virtual {v2}, Ljackpal/androidterm/compat/ActionBarCompat;->hide()V

    .line 643
    :cond_18
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    return v2

    .line 617
    :cond_1d
    const v2, 0x7f0b0004

    if-ne v0, v2, :cond_26

    .line 618
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doCreateNewWindow()V

    goto :goto_e

    .line 619
    :cond_26
    const v2, 0x7f0b0005

    if-ne v0, v2, :cond_2f

    .line 620
    invoke-direct {p0}, Ljackpal/androidterm/Term;->confirmCloseWindow()V

    goto :goto_e

    .line 621
    :cond_2f
    const v2, 0x7f0b0006

    if-ne v0, v2, :cond_3f

    .line 622
    new-instance v2, Landroid/content/Intent;

    const-class v3, Ljackpal/androidterm/WindowList;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2, v4}, Ljackpal/androidterm/Term;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_e

    .line 623
    :cond_3f
    const v2, 0x7f0b000a

    if-ne v0, v2, :cond_57

    .line 624
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doResetTerminal()V

    .line 625
    const v2, 0x7f060017

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    .line 626
    .local v1, toast:Landroid/widget/Toast;
    const/16 v2, 0x11

    invoke-virtual {v1, v2, v3, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 627
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_e

    .line 628
    .end local v1           #toast:Landroid/widget/Toast;
    :cond_57
    const v2, 0x7f0b000b

    if-ne v0, v2, :cond_60

    .line 629
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doEmailTranscript()V

    goto :goto_e

    .line 630
    :cond_60
    const v2, 0x7f0b0008

    if-ne v0, v2, :cond_69

    .line 631
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doDocumentKeys()V

    goto :goto_e

    .line 632
    :cond_69
    const v2, 0x7f0b0007

    if-ne v0, v2, :cond_72

    .line 633
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doToggleSoftKeyboard()V

    goto :goto_e

    .line 634
    :cond_72
    const v2, 0x7f0b000c

    if-ne v0, v2, :cond_7b

    .line 635
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doToggleWakeLock()V

    goto :goto_e

    .line 636
    :cond_7b
    const v2, 0x7f0b000d

    if-ne v0, v2, :cond_e

    .line 637
    invoke-direct {p0}, Ljackpal/androidterm/Term;->doToggleWifiLock()V

    goto :goto_e
.end method

.method public onPause()V
    .registers 7

    .prologue
    .line 546
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 548
    iget-object v1, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    .line 549
    .local v1, sessions:Ljackpal/androidterm/util/SessionList;
    iget-object v3, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    .line 551
    .local v3, viewFlipper:Ljackpal/androidterm/TermViewFlipper;
    invoke-virtual {v3}, Ljackpal/androidterm/TermViewFlipper;->onPause()V

    .line 552
    if-eqz v1, :cond_1c

    .line 553
    invoke-virtual {v1, p0}, Ljackpal/androidterm/util/SessionList;->removeCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)Z

    .line 554
    iget-object v0, p0, Ljackpal/androidterm/Term;->mWinListAdapter:Ljackpal/androidterm/WindowListAdapter;

    .line 555
    .local v0, adapter:Ljackpal/androidterm/WindowListAdapter;
    if-eqz v0, :cond_1c

    .line 556
    invoke-virtual {v1, v0}, Ljackpal/androidterm/util/SessionList;->removeCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)Z

    .line 557
    invoke-virtual {v1, v0}, Ljackpal/androidterm/util/SessionList;->removeTitleChangedListener(Ljackpal/androidterm/emulatorview/UpdateCallback;)Z

    .line 558
    invoke-virtual {v3, v0}, Ljackpal/androidterm/TermViewFlipper;->removeCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)V

    .line 562
    .end local v0           #adapter:Ljackpal/androidterm/WindowListAdapter;
    :cond_1c
    sget v4, Ljackpal/androidterm/compat/AndroidCompat;->SDK:I

    const/4 v5, 0x5

    if-ge v4, v5, :cond_24

    .line 566
    const/4 v4, 0x0

    iput-boolean v4, p0, Ljackpal/androidterm/Term;->mBackKeyPressed:Z

    .line 572
    :cond_24
    invoke-virtual {v3}, Ljackpal/androidterm/TermViewFlipper;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    .line 573
    .local v2, token:Landroid/os/IBinder;
    new-instance v4, Ljackpal/androidterm/Term$5;

    invoke-direct {v4, p0, v2}, Ljackpal/androidterm/Term$5;-><init>(Ljackpal/androidterm/Term;Landroid/os/IBinder;)V

    invoke-virtual {v4}, Ljackpal/androidterm/Term$5;->start()V

    .line 580
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 5
    .parameter "menu"

    .prologue
    .line 762
    const v2, 0x7f0b000c

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 763
    .local v0, wakeLockItem:Landroid/view/MenuItem;
    const v2, 0x7f0b000d

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 764
    .local v1, wifiLockItem:Landroid/view/MenuItem;
    iget-object v2, p0, Ljackpal/androidterm/Term;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 765
    const v2, 0x7f060019

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 769
    :goto_1c
    iget-object v2, p0, Ljackpal/androidterm/Term;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_36

    .line 770
    const v2, 0x7f06001b

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 774
    :goto_2a
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v2

    return v2

    .line 767
    :cond_2f
    const v2, 0x7f060018

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_1c

    .line 772
    :cond_36
    const v2, 0x7f06001a

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_2a
.end method

.method public onResume()V
    .registers 8

    .prologue
    .line 509
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 511
    iget-object v2, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    .line 512
    .local v2, sessions:Ljackpal/androidterm/util/SessionList;
    iget-object v4, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    .line 513
    .local v4, viewFlipper:Ljackpal/androidterm/TermViewFlipper;
    if-eqz v2, :cond_19

    .line 514
    invoke-virtual {v2, p0}, Ljackpal/androidterm/util/SessionList;->addCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)V

    .line 515
    iget-object v0, p0, Ljackpal/androidterm/Term;->mWinListAdapter:Ljackpal/androidterm/WindowListAdapter;

    .line 516
    .local v0, adapter:Ljackpal/androidterm/WindowListAdapter;
    if-eqz v0, :cond_19

    .line 517
    invoke-virtual {v2, v0}, Ljackpal/androidterm/util/SessionList;->addCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)V

    .line 518
    invoke-virtual {v2, v0}, Ljackpal/androidterm/util/SessionList;->addTitleChangedListener(Ljackpal/androidterm/emulatorview/UpdateCallback;)V

    .line 519
    invoke-virtual {v4, v0}, Ljackpal/androidterm/TermViewFlipper;->addCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)V

    .line 522
    .end local v0           #adapter:Ljackpal/androidterm/WindowListAdapter;
    :cond_19
    if-eqz v2, :cond_47

    invoke-virtual {v2}, Ljackpal/androidterm/util/SessionList;->size()I

    move-result v5

    invoke-virtual {v4}, Ljackpal/androidterm/TermViewFlipper;->getChildCount()I

    move-result v6

    if-ge v5, v6, :cond_47

    .line 523
    const/4 v1, 0x0

    .local v1, i:I
    :goto_26
    invoke-virtual {v4}, Ljackpal/androidterm/TermViewFlipper;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_47

    .line 524
    invoke-virtual {v4, v1}, Ljackpal/androidterm/TermViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Ljackpal/androidterm/emulatorview/EmulatorView;

    .line 525
    .local v3, v:Ljackpal/androidterm/emulatorview/EmulatorView;
    invoke-virtual {v3}, Ljackpal/androidterm/emulatorview/EmulatorView;->getTermSession()Ljackpal/androidterm/emulatorview/TermSession;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljackpal/androidterm/util/SessionList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_44

    .line 526
    invoke-virtual {v3}, Ljackpal/androidterm/emulatorview/EmulatorView;->onPause()V

    .line 527
    invoke-virtual {v4, v3}, Ljackpal/androidterm/TermViewFlipper;->removeView(Landroid/view/View;)V

    .line 528
    add-int/lit8 v1, v1, -0x1

    .line 523
    :cond_44
    add-int/lit8 v1, v1, 0x1

    goto :goto_26

    .line 533
    .end local v1           #i:I
    .end local v3           #v:Ljackpal/androidterm/emulatorview/EmulatorView;
    :cond_47
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    iput-object v5, p0, Ljackpal/androidterm/Term;->mPrefs:Landroid/content/SharedPreferences;

    .line 534
    iget-object v5, p0, Ljackpal/androidterm/Term;->mSettings:Ljackpal/androidterm/util/TermSettings;

    iget-object v6, p0, Ljackpal/androidterm/Term;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {v5, v6}, Ljackpal/androidterm/util/TermSettings;->readPrefs(Landroid/content/SharedPreferences;)V

    .line 535
    invoke-direct {p0}, Ljackpal/androidterm/Term;->updatePrefs()V

    .line 537
    iget v5, p0, Ljackpal/androidterm/Term;->onResumeSelectWindow:I

    if-ltz v5, :cond_63

    .line 538
    iget v5, p0, Ljackpal/androidterm/Term;->onResumeSelectWindow:I

    invoke-virtual {v4, v5}, Ljackpal/androidterm/TermViewFlipper;->setDisplayedChild(I)V

    .line 539
    const/4 v5, -0x1

    iput v5, p0, Ljackpal/androidterm/Term;->onResumeSelectWindow:I

    .line 541
    :cond_63
    invoke-virtual {v4}, Ljackpal/androidterm/TermViewFlipper;->onResume()V

    .line 542
    return-void
.end method

.method public onUpdate()V
    .registers 6

    .prologue
    .line 874
    iget-object v1, p0, Ljackpal/androidterm/Term;->mTermSessions:Ljackpal/androidterm/util/SessionList;

    .line 875
    .local v1, sessions:Ljackpal/androidterm/util/SessionList;
    if-nez v1, :cond_5

    .line 892
    :cond_4
    :goto_4
    return-void

    .line 879
    :cond_5
    invoke-virtual {v1}, Ljackpal/androidterm/util/SessionList;->size()I

    move-result v3

    if-nez v3, :cond_12

    .line 880
    const/4 v3, 0x1

    iput-boolean v3, p0, Ljackpal/androidterm/Term;->mStopServiceOnFinish:Z

    .line 881
    invoke-virtual {p0}, Ljackpal/androidterm/Term;->finish()V

    goto :goto_4

    .line 882
    :cond_12
    invoke-virtual {v1}, Ljackpal/androidterm/util/SessionList;->size()I

    move-result v3

    iget-object v4, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v4}, Ljackpal/androidterm/TermViewFlipper;->getChildCount()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 883
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1f
    iget-object v3, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v3}, Ljackpal/androidterm/TermViewFlipper;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 884
    iget-object v3, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v3, v0}, Ljackpal/androidterm/TermViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Ljackpal/androidterm/emulatorview/EmulatorView;

    .line 885
    .local v2, v:Ljackpal/androidterm/emulatorview/EmulatorView;
    invoke-virtual {v2}, Ljackpal/androidterm/emulatorview/EmulatorView;->getTermSession()Ljackpal/androidterm/emulatorview/TermSession;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljackpal/androidterm/util/SessionList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_43

    .line 886
    invoke-virtual {v2}, Ljackpal/androidterm/emulatorview/EmulatorView;->onPause()V

    .line 887
    iget-object v3, p0, Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;

    invoke-virtual {v3, v2}, Ljackpal/androidterm/TermViewFlipper;->removeView(Landroid/view/View;)V

    .line 888
    add-int/lit8 v0, v0, -0x1

    .line 883
    :cond_43
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f
.end method
